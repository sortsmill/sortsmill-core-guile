#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2014, 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <stdbool.h>
#include <limits.h>
#include <libintl.h>
#include <libguile.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

static inline size_t
sz_min (size_t a, size_t b)
{
  return (a < b) ? a : b;
}

//-------------------------------------------------------------------------

#define SCM_LIST_MAP_REINDEXED_TO_UNIFORM_VECTOR(NAME, SCM_NAME,        \
                                                 T, TXX,                \
                                                 CONVERT, REINDEX)      \
  VISIBLE SCM                                                           \
  NAME (SCM proc, SCM obj_lst)                                          \
  {                                                                     \
    size_t obj_count = scm_to_size_t (scm_length (obj_lst));            \
    if (obj_count == 0)                                                 \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (SCM_NAME),                         \
          rnrs_c_make_message_condition (_("too few arguments")),       \
          rnrs_make_irritants_condition (SCM_EOL)));                    \
                                                                        \
    scm_dynwind_begin (0);                                              \
                                                                        \
    SCM *lists = scm_malloc (obj_count * sizeof (SCM));                 \
    scm_dynwind_free (lists);                                           \
                                                                        \
    SCM p = obj_lst;                                                    \
    for (size_t i = 0; i < obj_count; i++)                              \
      {                                                                 \
        lists[i] = SCM_CAR (p);                                         \
        p = SCM_CDR (p);                                                \
      }                                                                 \
                                                                        \
    size_t n = scm_to_size_t (scm_length (lists[0]));                   \
    for (size_t i = 1; i < obj_count; i++)                              \
      n = sz_min (n, scm_to_size_t (scm_length (lists[i])));            \
                                                                        \
    T *data = (T *) scm_malloc (n * sizeof (T));                        \
    SCM v = scm_take_##TXX##vector (data, n);                           \
    for (ssize_t j = 0; j < n; j++)                                     \
      {                                                                 \
        SCM args = SCM_EOL;                                             \
        for (size_t i = 0; i < obj_count; i++)                          \
          {                                                             \
            const size_t k = obj_count - i - 1;                         \
            args = scm_cons (SCM_CAR (lists[k]), args);                 \
            lists[k] = SCM_CDR (lists[k]);                              \
          }                                                             \
        data[REINDEX] = (T) CONVERT (scm_apply_0 (proc, args));         \
      }                                                                 \
                                                                        \
    scm_dynwind_end ();                                                 \
                                                                        \
    return v;                                                           \
  }

#define SCM_LIST_MAP_TO_UNIFORM_VECTOR(T, TXX, CONVERT) \
  SCM_LIST_MAP_REINDEXED_TO_UNIFORM_VECTOR              \
  (scm_list_map_to_##TXX##vector,                       \
   "list-map->" #TXX "vector", T, TXX, CONVERT, (j))

VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (int8_t, s8, scm_to_int8);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (int16_t, s16, scm_to_int16);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (int32_t, s32, scm_to_int32);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (int64_t, s64, scm_to_int64);

VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (uint8_t, u8, scm_to_uint8);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (uint16_t, u16, scm_to_uint16);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (uint32_t, u32, scm_to_uint32);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (uint64_t, u64, scm_to_uint64);

VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (float, f32, scm_to_double);
VISIBLE SCM_LIST_MAP_TO_UNIFORM_VECTOR (double, f64, scm_to_double);

//-------------------------------------------------------------------------

#define SCM_UNIFORM_VECTOR_CONCATENATE(T, TXX, ENTRY_SIZE)              \
  VISIBLE SCM                                                           \
  scm_##TXX##vector_concatenate (SCM vec_lst)                           \
  {                                                                     \
    SCM_ASSERT_TYPE ((scm_is_true (scm_list_p (vec_lst))), vec_lst,     \
                     SCM_ARG1, #TXX "vector-concatenate",               \
                     "proper list");                                    \
    size_t entry_count = 0;                                             \
    for (SCM p = vec_lst; !scm_is_null (p); p = SCM_CDR (p))            \
      entry_count +=                                                    \
        scm_to_size_t (scm_##TXX##vector_length (SCM_CAR (p)));         \
                                                                        \
    T *data = scm_malloc (entry_count * (ENTRY_SIZE) * sizeof (T));     \
    SCM vec = scm_take_##TXX##vector (data, entry_count);               \
                                                                        \
    for (SCM p = vec_lst; !scm_is_null (p); p = SCM_CDR (p))            \
      {                                                                 \
        scm_t_array_handle handle;                                      \
        size_t len;                                                     \
        ssize_t inc;                                                    \
        const T *elems =                                                \
          scm_##TXX##vector_elements (SCM_CAR (p), &handle, &len,       \
                                      &inc);                            \
        if ((ENTRY_SIZE) == 1)                                          \
          for (size_t i = 0; i < len; i++)                              \
            {                                                           \
              data[0] = elems[0];                                       \
              data++;                                                   \
              elems += inc;                                             \
            }                                                           \
        else                                                            \
          for (size_t i = 0; i < len; i++)                              \
            {                                                           \
              for (size_t j = 0; j < (ENTRY_SIZE); j++)                 \
                data[j] = elems[j];                                     \
              data += (ENTRY_SIZE);                                     \
              elems += inc;                                             \
            }                                                           \
        scm_array_handle_release (&handle);                             \
      }                                                                 \
                                                                        \
    return vec;                                                         \
  }

VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (int8_t, s8, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (int16_t, s16, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (int32_t, s32, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (int64_t, s64, 1);

VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (uint8_t, u8, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (uint16_t, u16, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (uint32_t, u32, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (uint64_t, u64, 1);

VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (float, f32, 1);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (double, f64, 1);

VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (float, c32, 2);
VISIBLE SCM_UNIFORM_VECTOR_CONCATENATE (double, c64, 2);

//-------------------------------------------------------------------------
