#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTIULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <abstract.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define _PYSCM_MODULE "sortsmill core python pyscms"

//-------------------------------------------------------------------------

static void
pyerr_set_guile_error (void)
{
  // FIXME: Have this include Guile exception information, if
  // possible.  
  PyErr_SetString (PyExc_RuntimeError, "Guile error");
}

static inline void
pyerr_set_assert (bool assertion)
{
  if (!assertion && !PyErr_Occurred ())
    pyerr_set_guile_error ();
}

static inline bool
scm_is_pointer (SCM obj)
{
  return
    scm_is_true (scm_call_1
                 (scm_c_public_ref ("system foreign", "pointer?"), obj));
}

//-------------------------------------------------------------------------

typedef struct
{
  // *INDENT-OFF*
  PyObject_HEAD
  // *INDENT-ON*
  SCM pm_scm;                   // The wrapped Guile object.
} PySCM_Data;

typedef struct
{
  PyObject *self;
  PyObject *args;
  PyObject *kw;
} _self_args_kw;

///////////static void *
static SCM
_PySCM__call_internal (void *datap)
{
  PyObject *_result = NULL;

  _self_args_kw data = *(_self_args_kw *) datap;

  SCM self = PySCM_AsSCM (data.self);

  if (scm_is_true (scm_procedure_p (self)))
    {
      // Python positional and keyword arguments are converted to
      // Guile positional and keyword arguments.
      SCM scm_args;
      if (data.kw != NULL)
        scm_args = scm_c_pymapping_to_keyworded_list (data.kw);
      else
        scm_args = SCM_EOL;
      if (data.args != NULL)
        scm_args = scm_c_pysequence_to_list_with_tail (data.args, scm_args);

      SCM result = scm_apply_0 (self, scm_args);

      if (scm_is_pyobject (result))
        // Success.
        _result = (void *) scm_to_PyObject_ptr (result);
      else
        {
          // Bad return value type.
          // *INDENT-OFF*
          SCM message =
            scm_simple_format
            (SCM_BOOL_F,
             scm_from_utf8_string (_("return value ~S is not a pyobject")),
             scm_list_1 (result));
          // *INDENT-ON*
          char *_message = scm_to_utf8_stringn (message, NULL);
          PyErr_SetString (PyExc_RuntimeError, _message);
          free (_message);
        }
    }
  else
    {
      // The called object is not a procedure.
      // *INDENT-OFF*
      SCM message =
        scm_simple_format
        (SCM_BOOL_F, scm_from_utf8_string (_("~S is not a procedure")),
         scm_list_1 (self));
      // *INDENT-ON*
      char *_message = scm_to_utf8_stringn (message, NULL);
      PyErr_SetString (PyExc_RuntimeError, _message);
      free (_message);
    }

  Py_XINCREF (_result);
  return scm_from_pointer (_result, NULL);
}

typedef struct
{
  SCM key;
  SCM args;
} _reraise_exc_args_t;

static void *
reraise_exc (void *data)
{
  scm_throw (((_reraise_exc_args_t *) data)->key,
             ((_reraise_exc_args_t *) data)->args);
  return NULL;
}

static SCM
func_exc_handler (void *data, SCM key, SCM args)
{
  SCM py_exc_handler = scm_c_private_ref ("sortsmill core python pyexcs",
                                          "with-py-exc-handler-for-c");
  _reraise_exc_args_t my_data = {
    .key = key,
    .args = args
  };
  return scm_call_2 (py_exc_handler, scm_from_pointer (reraise_exc, NULL),
                     scm_from_pointer (&my_data, NULL));
}

static void *
scm_c_call_with_pyerr_handling (SCM (*func) (void *), void *data)
{
  SCM result =
    scm_internal_catch (SCM_BOOL_T, func, data, func_exc_handler, data);
  return scm_to_pointer (result);
}

static PyObject *
PySCM__call (PyObject *self, PyObject *args, PyObject *kw)
{
  _self_args_kw data = {
    .self = self,
    .args = args,
    .kw = kw
  };
  PyObject *result =
    scm_c_call_with_pyerr_handling (_PySCM__call_internal, &data);
  pyerr_set_assert ((result != NULL));
  return result;
}

static bool
scm_is_bytevectorlike (SCM obj)
{
  bool result;
#if SCM_MAJOR_VERSION == 2 && SCM_MINOR_VERSION == 0 && SCM_MICRO_VERSION <= 9
  // FIXME: Is this special case necessary?
  result = (scm_is_uniform_vector (obj) || scm_is_bytevector (obj));
#else
  result = scm_is_bytevector (obj);
#endif
  return result;
}

static bool
scm_is_numberlike (SCM obj)
{
  return (scm_is_number (obj) || scm_is_bool (obj));
}

static bool
scm_is_stringlike (SCM obj)
{
  return (scm_is_string (obj) || scm_is_symbol (obj) || scm_is_keyword (obj));
}

static bool
scm_is_simple (SCM obj)
{
  return (scm_is_numberlike (obj)
          || scm_is_stringlike (obj)
          || scm_is_bytevectorlike (obj)
          || scm_is_true (scm_procedure_p (obj))
          || scm_is_true (scm_variable_p (obj))
          || scm_is_pointer (obj) || scm_is_eq (obj, SCM_UNSPECIFIED));
}

static SCM safer_object (SCM obj, bool undetailed);

static bool
scm_is_proper_list (SCM obj)
{
  return scm_is_true (scm_list_p (obj));
}

static bool
scm_is_dotted_list (SCM obj)
{
  return
    scm_is_true (scm_call_1 (scm_c_public_ref ("srfi srfi-1", "dotted-list?"),
                             obj));
}

static bool
scm_is_circular_list (SCM obj)
{
  return
    scm_is_true (scm_call_1 (scm_c_public_ref ("srfi srfi-1", "circular-list?"),
                             obj));
}

static SCM
undetailed_proper_list (SCM obj)
{
  return scm_c_utf8_sformat ("<proper list 0x~x>",
                             scm_list_1 (scm_object_address (obj)));
}

static SCM
undetailed_dotted_list (SCM obj)
{
  return scm_c_utf8_sformat ("<dotted list 0x~x>",
                             scm_list_1 (scm_object_address (obj)));
}

static SCM
undetailed_circular_list (SCM obj)
{
  return scm_c_utf8_sformat ("<circular list 0x~x>",
                             scm_list_1 (scm_object_address (obj)));
}

static SCM
undetailed_pair (SCM obj)
{
  SCM obj_to_print;
  if (scm_is_proper_list (obj))
    obj_to_print = undetailed_proper_list (obj);
  else if (scm_is_dotted_list (obj))
    obj_to_print = undetailed_dotted_list (obj);
  else
    obj_to_print = undetailed_circular_list (obj);
  return obj_to_print;
}

static SCM
undetailed_vector (SCM obj)
{
  return scm_c_utf8_sformat ("<vector 0x~x>",
                             scm_list_1 (scm_object_address (obj)));
}

static SCM
safer_proper_list (SCM lst)
{
  SCM s = SCM_EOL;
  for (SCM p = lst; !scm_is_null (p); p = SCM_CDR (p))
    s = scm_cons (safer_object (SCM_CAR (p), true), s);
  return scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("(~A)"),
                            scm_list_1 (scm_string_join
                                        (scm_reverse (s),
                                         scm_from_latin1_string (" "),
                                         SCM_UNDEFINED)));
}

static SCM
safer_list (SCM obj)
{
  SCM obj_to_print;
  if (scm_is_dotted_list (obj))
    // FIXME: We can do better than this. We might at least want to
    // print association lists specially.
    obj_to_print = undetailed_dotted_list (obj);
  else if (scm_is_circular_list (obj))
    // FIXME: We can do better than this.
    obj_to_print = undetailed_circular_list (obj);
  else
    obj_to_print = safer_proper_list (obj);
  return obj_to_print;
}

static SCM
safer_vector (SCM v)
{
  size_t n = scm_c_vector_length (v);
  SCM s = SCM_EOL;
  for (size_t i = 0; i < n; i++)
    s = scm_cons (safer_object (scm_c_vector_ref (v, (n - 1) - i), true), s);
  return scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("#(~A)"),
                            scm_list_1 (scm_string_join
                                        (s, scm_from_latin1_string (" "),
                                         SCM_UNDEFINED)));
}

static SCM
safer_object (SCM obj, bool undetailed)
{
  // Try to convert an object to something informative but safely
  // printable, without risk of endless recursions, and with whatever
  // other limitations seem appropriate.
  //
  // FIXME: Improve this routine, generally.
  //
  // FIXME: Add support for arrays.

  SCM safer_obj;
  if (scm_is_pyobject (obj))
    safer_obj = scm_c_utf8_sformat ("<pyobject 0x~x>",
                                    scm_list_1 (scm_object_address (obj)));
  else if (scm_is_simple (obj))
    safer_obj = scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("~S"),
                                   scm_list_1 (obj));
  else if (scm_is_pair (obj))
    safer_obj = undetailed ? undetailed_pair (obj) : safer_list (obj);
  else if (scm_is_true (scm_vector_p (obj)))
    safer_obj = undetailed ? undetailed_vector (obj) : safer_vector (obj);
  else
    scm_c_utf8_sformat ("<object 0x~x>", scm_list_1 (scm_object_address (obj)));
  return safer_obj;
}

static void *
_PySCM__repr_internal (void *datap)
{
  SCM obj = PySCM_AsSCM ((PyObject *) datap);
  SCM s = scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("<scm ~A>"),
                             scm_list_1 (safer_object (obj, false)));
  SCM repr = scm_python_repr (s);
  PyObject *_repr = scm_to_PyObject_ptr (repr);
  Py_INCREF (_repr);
  return (void *) _repr;
}

static PyObject *
PySCM__repr (PyObject *self)
{
  PyObject *repr =
    scm_c_with_continuation_barrier (_PySCM__repr_internal, self);
  pyerr_set_assert ((repr != NULL));
  return repr;
}

typedef struct pm_property_data
{
  SCM property;
  PyObject *self;
  PyObject *value;
} pm_property_data;

static void *
pm_get_string_with_barrier (void *datap)
{
  pm_property_data data = *(pm_property_data *) datap;
  SCM getter = scm_procedure (data.property);
  SCM value = scm_call_1 (getter, PySCM_AsSCM (data.self));
  SCM pyval = (scm_is_true (value)) ? scm_python_str (value) : scm_py_none ();
  PyObject *result = scm_to_PyObject_ptr (pyval);
  Py_INCREF (result);
  return (void *) result;
}

static void *
pm_set_string_with_barrier (void *datap)
{
  pm_property_data data = *(pm_property_data *) datap;
  SCM value;
  if (data.value == Py_None)
    value = SCM_BOOL_F;
  else
    value = scm_pyunicode_to_string (scm_c_incref_make_pyobject (data.value),
                                     SCM_UNDEFINED);
  SCM setter = scm_setter (data.property);
  scm_call_2 (setter, PySCM_AsSCM (data.self), value);

  // Return an arbitrary non-NULL pointer.
  return pm_set_string_with_barrier;
}

static PyObject *
pm_get_string_property (const char *property_name, PyObject *self)
{
  pm_property_data data = {
    .property = scm_c_private_ref (_PYSCM_MODULE, property_name),
    .self = self
  };
  PyObject *value = (PyObject *)
    scm_c_with_continuation_barrier (pm_get_string_with_barrier, &data);
  pyerr_set_assert ((value != NULL));
  return value;
}

static int
pm_set_string_property (const char *property_name,
                        PyObject *self, PyObject *value)
{
  pm_property_data data = {
    .property = scm_c_private_ref (_PYSCM_MODULE, property_name),
    .self = self,
    .value = value
  };
  PyObject *error_indicator = (PyObject *)
    scm_c_with_continuation_barrier (pm_set_string_with_barrier, &data);
  pyerr_set_assert ((error_indicator != NULL));
  return (error_indicator == NULL) ? -1 : 0;
}

static PyObject *
pm_get_name (PyObject *self, void *closure STM_MAYBE_UNUSED)
{
  return pm_get_string_property ("py-name", self);
}

static int
pm_set_name (PyObject *self, PyObject *value, void *closure STM_MAYBE_UNUSED)
{
  return pm_set_string_property ("py-name", self, value);
}

static PyObject *
pm_get_doc (PyObject *self, void *closure STM_MAYBE_UNUSED)
{
  return pm_get_string_property ("py-doc", self);
}

static int
pm_set_doc (PyObject *self, PyObject *value, void *closure STM_MAYBE_UNUSED)
{
  return pm_set_string_property ("py-doc", self, value);
}

static PyGetSetDef PySCM__getseters[] = {
  {
   .name = "__name__",
   .get = pm_get_name,
   .set = pm_set_name,
   .doc = NULL,
   .closure = NULL},
  {
   .name = "__doc__",
   .get = pm_get_doc,
   .set = pm_set_doc,
   .doc = NULL,
   .closure = NULL},
  NULL
};

static PyObject *
PySCM__alloc (PyTypeObject *type, Py_ssize_t nitems STM_MAYBE_UNUSED)
{
  PyObject *p = scm_calloc (sizeof (PySCM_Data));
  p->ob_refcnt = 1;
  p->ob_type = type;
  return p;
}

static void
PySCM__dealloc (PyObject *self)
{
  // This is coded in a way that avoids -Wstrict-aliasing warnings.

  PySCM_Data *_self = (PySCM_Data *) self;
  scm_gc_unprotect_object (_self->pm_scm);
  scm_t_bits *bits = (scm_t_bits *) & (_self->pm_scm);
  *bits = 0;
  free (self);
}

VISIBLE PyTypeObject PySCM_Type = {
  // *INDENT-OFF*
  PyVarObject_HEAD_INIT (NULL, 0)
  // *INDENT-ON*
  .tp_name = "sortsmill_core_guile.scm",
  .tp_basicsize = sizeof (PySCM_Data),
  .tp_flags = Py_TPFLAGS_DEFAULT,
  .tp_doc = NULL,               // FIXME: Write a documentation string.
  .tp_alloc = PySCM__alloc,
  .tp_dealloc = PySCM__dealloc,
  .tp_repr = PySCM__repr,
  .tp_call = PySCM__call,
  .tp_getset = PySCM__getseters
};

VISIBLE PyObject *
PySCM_FromSCM (SCM obj)
{
  PySCM_Data *self = (PySCM_Data *) PySCM__alloc (&PySCM_Type, 0);
  if (self != NULL)
    {
      self->pm_scm = obj;
      scm_gc_protect_object (self->pm_scm);
    }
  return (PyObject *) self;
}

// A referenceable instance of the inline PySCM_Check function.
VISIBLE int PySCM_Check (PyObject *pyobj);

VISIBLE SCM
PySCM_AsSCM (PyObject *pyobj)
{
  return ((PySCM_Data *) pyobj)->pm_scm;
}

VISIBLE bool
scm_is_pyscm (SCM obj)
{
  return (scm_is_pyobject (obj) && PySCM_Check (scm_to_PyObject_ptr (obj)));
}

VISIBLE SCM
scm_pyscm_p (SCM obj)
{
  return scm_from_bool (scm_is_pyscm (obj));
}

VISIBLE SCM
scm_scm_to_pyscm (SCM obj)
{
  PyObject *pyobj = PySCM_FromSCM (obj);
  py_exc_check ((pyobj != NULL), "scm->pyscm", scm_list_1 (obj));
  return scm_c_make_pyobject (pyobj);
}

VISIBLE SCM
scm_pyscm_to_scm (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *pyobj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PySCM_Check (pyobj), obj, SCM_ARG1, "pyscm->scm", "pyscm");
  return PySCM_AsSCM (pyobj);
}

VISIBLE int
PySCM_Ready (void)
{
  return PyType_Ready (&PySCM_Type);
}

//-------------------------------------------------------------------------
