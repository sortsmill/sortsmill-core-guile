include(`m4citrus.m4')dnl
m4_divert(-1)
m4_include([smcoreguile-pkginfo.m4])
m4_include([smcoreguile-dirlayout.m4])
m4_include([smcoreguile-buildinfo.m4])
m4_include([vars.m4])
m4_define([expand_var], [$1=$2])
m4_define([expand_vars_list],
          [m4_ifnblank([$2], [@%:@ $2
])m4_map_sep([expand_var], [m4_newline], m4_split(m4_normalize($1)))])
m4_divert[]# This file was generated with GNU m4.

expand_vars_list([SMCOREGUILE_PKGINFO_LIST], [Package information.])

expand_vars_list([SMCOREGUILE_DIRLAYOUT_LIST], [Directories.])

expand_vars_list([SMCOREGUILE_BUILDINFO_LIST], [Configuration options.])

[GUILE_EFFECTIVE_VERSION=]GUILE_EFFECTIVE_VERSION

# [PYTHON_VERSION_AND_ABI] will be something like `2.7', `3.2', `3.4m', etc.
[PYTHON_VERSION_AND_ABI=]PYTHON_VERSION_AND_ABI

# Actual names of the 'sortsmill-core-guile' and
# 'sortsmill-core-python' programs. This is particularly useful when
# '--program-transform-name' or similar has been used.
sortsmill_core_guile=${bindir}/@core_guile_name@
sortsmill_core_python=${bindir}/@core_python_name@
sortsmill_core_python@PYV@=${bindir}/@core_python_pyv_name@

Name: ${PACKAGE}-${[GUILE_EFFECTIVE_VERSION]}-${[PYTHON_VERSION_AND_ABI]}
Description: ${PACKAGE_NAME} Python ${[PYTHON_VERSION_AND_ABI]} interface for Guile ${[GUILE_EFFECTIVE_VERSION]}
Version: ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
Requires.private: sortsmill-core-guile-${[GUILE_EFFECTIVE_VERSION]} guile-${[GUILE_EFFECTIVE_VERSION]} sortsmill-core
Libs: -L${libdir} -lsortsmill-core-guile-${[GUILE_EFFECTIVE_VERSION]}-py${[PYTHON_VERSION_AND_ABI]} @PYTHON_LIBS@
Cflags: @PYTHON_CFLAGS@ -I${hostincludedir} -I${includedir}
