#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <abstract.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pynumber, PyNumber);
SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pyindex, PyIndex);

VISIBLE SCM_PYOBJECT_BINARY_OP ("py+", scm_pynumber_add, PyNumber_Add);

VISIBLE SCM
scm_pynumber_add_list (SCM obj1, SCM obj_lst)
{
  SCM sum = obj1;

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py+", "pyobject");
      sum = scm_pynumber_add (sum, SCM_CAR (p));
    }

  return sum;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-", scm_pynumber_subtract,
                                PyNumber_Subtract);

VISIBLE SCM
scm_pynumber_minus_list (SCM obj1, SCM obj_lst)
{
  SCM result;

  if (scm_is_null (obj_lst))
    result = scm_pynumber_negative (obj1);
  else
    {
      result = obj1;
      // The following will not work if @var{obj_lst} is cyclic.
      for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
        {
          SCM_ASSERT_TYPE ((scm_is_pair (p)
                            && scm_is_pyobject (SCM_CAR (p))),
                           SCM_CAR (p), SCM_ARGn, "py-", "pyobject");
          result = scm_pynumber_subtract (result, SCM_CAR (p));
        }
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py*", scm_pynumber_multiply,
                                PyNumber_Multiply);

VISIBLE SCM
scm_pynumber_multiply_list (SCM obj1, SCM obj_lst)
{
  SCM product = obj1;

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py*", "pyobject");
      product = scm_pynumber_multiply (product, SCM_CAR (p));
    }

  return product;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-floor/", scm_pynumber_floordivide,
                                PyNumber_FloorDivide);

VISIBLE SCM_PYOBJECT_BINARY_OP ("py/", scm_pynumber_truedivide,
                                PyNumber_TrueDivide);

VISIBLE SCM
scm_pynumber_truedivide_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM quotient = scm_pynumber_truedivide (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py/", "pyobject");
      quotient = scm_pynumber_truedivide (quotient, SCM_CAR (p));
    }

  return quotient;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py%", scm_pynumber_remainder,
                                PyNumber_Remainder);
VISIBLE SCM_PYOBJECT_BINARY_OP ("py-divmod", scm_pynumber_divmod,
                                PyNumber_Divmod);

static SCM_PYOBJECT_TERNARY_OP ("py-power", _scm_pynumber_power,
                                PyNumber_Power);

VISIBLE SCM
scm_pynumber_power (SCM obj1, SCM obj2, SCM obj3)
{
  if (unbndp_or_false (obj3))
    obj3 = scm_py_none ();
  return _scm_pynumber_power (obj1, obj2, obj3);
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("py-", scm_pynumber_negative, PyNumber_Negative);

VISIBLE SCM_PYOBJECT_UNARY_OP ("py-positive", scm_pynumber_positive,
                               PyNumber_Positive);
VISIBLE SCM_PYOBJECT_UNARY_OP ("py-abs", scm_pynumber_absolute,
                               PyNumber_Absolute);
VISIBLE SCM_PYOBJECT_UNARY_OP ("py-bitwise-not", scm_pynumber_invert,
                               PyNumber_Invert);
VISIBLE SCM_PYOBJECT_BINARY_OP ("py-lshift", scm_pynumber_lshift,
                                PyNumber_Lshift);
VISIBLE SCM_PYOBJECT_BINARY_OP ("py-rshift", scm_pynumber_rshift,
                                PyNumber_Rshift);

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-and", scm_pynumber_and,
                                PyNumber_And);

VISIBLE SCM
scm_pynumber_and_list (SCM obj1, SCM obj_lst)
{
  SCM result = obj1;

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-and", "pyobject");
      result = scm_pynumber_and (result, SCM_CAR (p));
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-xor", scm_pynumber_xor,
                                PyNumber_Xor);

VISIBLE SCM
scm_pynumber_xor_list (SCM obj1, SCM obj_lst)
{
  SCM result = obj1;

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-xor", "pyobject");
      result = scm_pynumber_xor (result, SCM_CAR (p));
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-ior", scm_pynumber_or, PyNumber_Or);

VISIBLE SCM
scm_pynumber_or_list (SCM obj1, SCM obj_lst)
{
  SCM result = obj1;

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-ior", "pyobject");
      result = scm_pynumber_or (result, SCM_CAR (p));
    }

  return result;
}

//-------------------------------------------------------------------------

VISIBLE SCM_PYOBJECT_BINARY_OP ("py+!", scm_pynumber_inplaceadd,
                                PyNumber_InPlaceAdd);

VISIBLE SCM
scm_pynumber_inplaceadd_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM sum = scm_pynumber_inplaceadd (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py+!", "pyobject");
      sum = scm_pynumber_inplaceadd (sum, SCM_CAR (p));
    }

  return sum;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-!", scm_pynumber_inplacesubtract,
                                PyNumber_InPlaceSubtract);

VISIBLE SCM
scm_pynumber_inplacesubtract_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM result = scm_pynumber_inplacesubtract (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p)
                        && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-!", "pyobject");
      result = scm_pynumber_inplacesubtract (result, SCM_CAR (p));
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py*!", scm_pynumber_inplacemultiply,
                                PyNumber_InPlaceMultiply);

VISIBLE SCM
scm_pynumber_inplacemultiply_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM product = scm_pynumber_inplacemultiply (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py*!", "pyobject");
      product = scm_pynumber_inplacemultiply (product, SCM_CAR (p));
    }

  return product;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-floor/!", scm_pynumber_inplacefloordivide,
                                PyNumber_InPlaceFloorDivide);

VISIBLE SCM_PYOBJECT_BINARY_OP ("py/!", scm_pynumber_inplacetruedivide,
                                PyNumber_InPlaceTrueDivide);

VISIBLE SCM
scm_pynumber_inplacetruedivide_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM quotient = scm_pynumber_inplacetruedivide (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py/!", "pyobject");
      quotient = scm_pynumber_inplacetruedivide (quotient, SCM_CAR (p));
    }

  return quotient;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py%!", scm_pynumber_inplaceremainder,
                                PyNumber_InPlaceRemainder);

static SCM_PYOBJECT_TERNARY_OP ("py-power!", _scm_pynumber_inplacepower,
                                PyNumber_InPlacePower);

VISIBLE SCM
scm_pynumber_inplacepower (SCM obj1, SCM obj2, SCM obj3)
{
  if (unbndp_or_false (obj3))
    obj3 = scm_py_none ();
  return _scm_pynumber_inplacepower (obj1, obj2, obj3);
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-lshift!", scm_pynumber_inplacelshift,
                                PyNumber_InPlaceLshift);
VISIBLE SCM_PYOBJECT_BINARY_OP ("py-rshift!", scm_pynumber_inplacershift,
                                PyNumber_InPlaceRshift);

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-and!", scm_pynumber_inplaceand,
                                PyNumber_InPlaceAnd);

VISIBLE SCM
scm_pynumber_inplaceand_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM result = scm_pynumber_inplaceand (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-and!", "pyobject");
      result = scm_pynumber_inplaceand (result, SCM_CAR (p));
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-xor!", scm_pynumber_inplacexor,
                                PyNumber_InPlaceXor);

VISIBLE SCM
scm_pynumber_inplacexor_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM result = scm_pynumber_inplacexor (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-xor!", "pyobject");
      result = scm_pynumber_inplacexor (result, SCM_CAR (p));
    }

  return result;
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("py-bitwise-ior!", scm_pynumber_inplaceor,
                                PyNumber_InPlaceOr);

VISIBLE SCM
scm_pynumber_inplaceor_list (SCM obj1, SCM obj2, SCM obj_lst)
{
  SCM result = scm_pynumber_inplaceor (obj1, obj2);

  // The following will not work if @var{obj_lst} is cyclic.
  for (SCM p = obj_lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pyobject (SCM_CAR (p))),
                       SCM_CAR (p), SCM_ARGn, "py-bitwise-ior!", "pyobject");
      result = scm_pynumber_inplaceor (result, SCM_CAR (p));
    }

  return result;
}

//-------------------------------------------------------------------------

// FIXME: In Python 2, we might want to convert this to a Python
// @code{int} rather than a Python @code{long}, if possible.
VISIBLE SCM_PYOBJECT_UNARY_OP ("pynumber->pyinteger", scm_pynumber_integer,
                               PyNumber_Long);

VISIBLE SCM_PYOBJECT_UNARY_OP ("pynumber->pyfloat", scm_pynumber_float,
                               PyNumber_Float);

VISIBLE SCM_PYOBJECT_UNARY_OP ("pynumber->pyindex", scm_pynumber_index,
                               PyNumber_Index);

VISIBLE SCM
scm_c_pynumber_tobase (SCM obj, int base)
{
  // Python 2 allows other bases, but Python 3 does not, and
  // furthermore the call to PyNumber_ToBase may not catch errors.  In
  // any case, we restrict ourselves to the common behavior.
  SCM_ASSERT_TYPE ((base == 2 || base == 8 || base == 10 || base == 16),
                   scm_from_int (base), SCM_ARG2, "pynumber-tobase",
                   "2, 8, 10, or 16");

  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  PyObject *_result = PyNumber_ToBase (_obj, base);
  py_exc_check ((_result != NULL), "pynumber-tobase",
                scm_list_2 (obj, scm_from_int (base)));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pynumber_tobase (SCM obj, SCM base)
{
  base = scm_pyinteger_to_scm (base);
  const int _base = scm_to_int (base);
  return scm_c_pynumber_tobase (obj, _base);
}

//-------------------------------------------------------------------------
