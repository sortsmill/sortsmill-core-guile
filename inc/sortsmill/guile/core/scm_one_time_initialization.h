/*
 * Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_CORE_GUILE_SCM_ONE_TIME_INITIALIZATION_H
#define _SORTSMILL_CORE_GUILE_SCM_ONE_TIME_INITIALIZATION_H

#include <libguile.h>
#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/smcoreguile-buildinfo.h>

void scm_dynwind_pthread_mutex_unlock (void *mutex_ptr);

#if SMCOREGUILE_N_ENABLE_THREADS

#include <pthread.h>

#define STM_SCM_ONE_TIME_INITIALIZE(MODIFIERS, TYPE, NAME,              \
                                    INITIALIZATION)                     \
  STM_ONE_TIME_INITIALIZE (MODIFIERS, TYPE, NAME,                       \
                           static pthread_mutex_t __##NAME##__mutex =   \
                           PTHREAD_MUTEX_INITIALIZER;                   \
                           scm_dynwind_begin (0);                       \
                           pthread_mutex_lock (&__##NAME##__mutex);     \
                           scm_dynwind_pthread_mutex_unlock (&__##NAME##__mutex), \
                           scm_dynwind_end (),                          \
                           INITIALIZATION)

#else

#ifdef __GNUC__
#warning Compiling without threads support.
#endif

#define STM_SCM_ONE_TIME_INITIALIZE(MODIFIERS, TYPE, NAME,           \
                                    INITIALIZATION)                  \
  STM_ONE_TIME_INITIALIZE (MODIFIERS, TYPE, NAME,                    \
                           /* no locking */ ,                        \
                           /* no unlocking */ ,                      \
                           INITIALIZATION)

#endif

#define STM_SCM_DEFINE_UTF8_STRING(MODIFIERS, NAME, STRING)     \
  STM_SCM_ONE_TIME_INITIALIZE (MODIFIERS, SCM, NAME,            \
                               (NAME##__Value =                 \
                                scm_from_utf8_string (STRING)))

#define STM_SCM_DEFINE_UTF8_SYMBOL(MODIFIERS, NAME, STRING)     \
  STM_SCM_ONE_TIME_INITIALIZE (MODIFIERS, SCM, NAME,            \
                               (NAME##__Value =                 \
                                scm_from_utf8_symbol (STRING)))

#define STM_SCM_DEFINE_UTF8_KEYWORD(MODIFIERS, NAME, STRING)            \
  STM_SCM_ONE_TIME_INITIALIZE (MODIFIERS, SCM, NAME,                    \
                               (NAME##__Value =                         \
                                scm_from_utf8_keyword (STRING)))

#define STM_SCM_DEFINE_COMPILED_SCHEME(MODIFIERS, NAME, STRING)         \
  STM_SCM_ONE_TIME_INITIALIZE (MODIFIERS, SCM, NAME,                    \
                               (NAME##__Value =                         \
                                scm_c_compile_eval_utf8_string (STRING)))

#endif /* _SORTSMILL_CORE_GUILE_SCM_ONE_TIME_INITIALIZATION_H */
