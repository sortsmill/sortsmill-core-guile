/*
 * Copyright (C) 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_CORE_RBMAP_H
#define _SORTSMILL_CORE_RBMAP_H

#include <stdint.h>
#include <stdbool.h>
#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#define _SORTSMILL_CORE_RBMAP_DECLS(T, rbmapX)                          \
                                                                        \
  typedef struct                                                        \
  {                                                                     \
    /* `key' and `value' must appear here, at the  */                   \
    /*  head of the struct, in this order.         */                   \
    T key;                                                              \
    SCM value;                                                          \
  } scm_t_##rbmapX##_node_data;                                         \
                                                                        \
  typedef scm_t_##rbmapX##_node_data *scm_t_##rbmapX##_iter;            \
                                                                        \
  inline T scm_##rbmapX##_iter_key (scm_t_##rbmapX##_iter iter);        \
  inline SCM scm_##rbmapX##_iter_value (scm_t_##rbmapX##_iter iter);    \
  inline void scm_##rbmapX##_iter_set_value (scm_t_##rbmapX##_iter      \
                                             iter,                      \
                                             SCM value);                \
                                                                        \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_first (SCM map);               \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_last (SCM map);                \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_nsearch (SCM map, SCM key);    \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_psearch (SCM map, SCM key);    \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_next (SCM map,                 \
                                               scm_t_##rbmapX##_iter    \
                                               iter);                   \
  scm_t_##rbmapX##_iter scm_c_##rbmapX##_prev (SCM map,                 \
                                               scm_t_##rbmapX##_iter    \
                                               iter);                   \
                                                                        \
  bool scm_is_##rbmapX (SCM map);                                       \
  SCM scm_##rbmapX##_p (SCM map);                                       \
                                                                        \
  SCM scm_make_##rbmapX (void);                                         \
  SCM scm_##rbmapX##_set_x (SCM map, SCM key, SCM value);               \
  SCM scm_##rbmapX##_delete_x (SCM map, SCM key);                       \
  SCM scm_##rbmapX##_ref (SCM map, SCM key, SCM default_value);         \
  SCM scm_##rbmapX##_fold_left (SCM proc, SCM init, SCM map,            \
                                SCM start_key);                         \
  SCM scm_##rbmapX##_fold_right (SCM proc, SCM init, SCM map,           \
                                 SCM start_key);                        \
  SCM scm_alist_to_##rbmapX (SCM alist);                                \
  SCM scm_##rbmapX##_to_alist (SCM map);                                \
  SCM scm_plist_to_##rbmapX (SCM plist);                                \
  SCM scm_##rbmapX##_to_plist (SCM map);                                \
  SCM scm_##rbmapX##_keys (SCM map);                                    \
  SCM scm_##rbmapX##_values (SCM map);                                  \
  SCM scm_##rbmapX##_map_to_list (SCM proc, SCM map);                   \
  SCM scm_##rbmapX##_for_each (SCM proc, SCM map);                      \
  SCM scm_##rbmapX##_count (SCM pred, SCM map);                         \
  SCM scm_##rbmapX##_size (SCM map);                                    \
  bool scm_##rbmapX##_is_null (SCM map);                                \
  SCM scm_##rbmapX##_null_p (SCM map);                                  \
                                                                        \
  inline T                                                              \
  scm_##rbmapX##_iter_key (scm_t_##rbmapX##_iter iter)                  \
  {                                                                     \
    return iter->key;                                                   \
  }                                                                     \
                                                                        \
  inline SCM                                                            \
  scm_##rbmapX##_iter_value (scm_t_##rbmapX##_iter iter)                \
  {                                                                     \
    return iter->value;                                                 \
  }                                                                     \
                                                                        \
  inline void                                                           \
  scm_##rbmapX##_iter_set_value (scm_t_##rbmapX##_iter iter, SCM value) \
  {                                                                     \
    iter->value = value;                                                \
  }

/* Store keys as unsigned integers. */
_SORTSMILL_CORE_RBMAP_DECLS (uint8_t, rbmapu8);
_SORTSMILL_CORE_RBMAP_DECLS (uint16_t, rbmapu16);
_SORTSMILL_CORE_RBMAP_DECLS (uint32_t, rbmapu32);
_SORTSMILL_CORE_RBMAP_DECLS (uint64_t, rbmapu64);
_SORTSMILL_CORE_RBMAP_DECLS (unsigned int, rbmapui);
_SORTSMILL_CORE_RBMAP_DECLS (unsigned long int, rbmapul);
_SORTSMILL_CORE_RBMAP_DECLS (size_t, rbmapusz);
_SORTSMILL_CORE_RBMAP_DECLS (uintmax_t, rbmapumx);

/* Store keys as signed integers. */
_SORTSMILL_CORE_RBMAP_DECLS (int8_t, rbmaps8);
_SORTSMILL_CORE_RBMAP_DECLS (int16_t, rbmaps16);
_SORTSMILL_CORE_RBMAP_DECLS (int32_t, rbmaps32);
_SORTSMILL_CORE_RBMAP_DECLS (int64_t, rbmaps64);
_SORTSMILL_CORE_RBMAP_DECLS (int, rbmapsi);
_SORTSMILL_CORE_RBMAP_DECLS (long int, rbmapsl);
_SORTSMILL_CORE_RBMAP_DECLS (ssize_t, rbmapssz);
_SORTSMILL_CORE_RBMAP_DECLS (intmax_t, rbmapsmx);

/* Use SCM objects as keys. The name `rbmapq' is supposed to remind
   one of `eq?' */
_SORTSMILL_CORE_RBMAP_DECLS (scm_t_bits, rbmapq);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_CORE_RBMAP_H */
