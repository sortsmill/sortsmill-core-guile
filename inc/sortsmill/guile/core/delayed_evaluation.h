/*
 * Copyright (C) 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_CORE_DELAYED_EVALUATION_H
#define _SORTSMILL_CORE_DELAYED_EVALUATION_H

#include <stdbool.h>
#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

bool scm_is_delayed (SCM obj);
SCM scm_delayed_p (SCM obj);
bool scm_is_applicable_delayed (SCM obj);
SCM scm_applicable_delayed_p (SCM obj);
bool scm_is_nonapplicable_delayed (SCM obj);
SCM scm_nonapplicable_delayed_p (SCM obj);

SCM scm_delayed_value (SCM thunk);
SCM scm_applicable_delayed_value (SCM thunk);

SCM scm_undelayed (SCM value);
SCM scm_applicable_undelayed (SCM value);

SCM scm_make_delayed (SCM value);
SCM scm_make_applicable_delayed (SCM value);

SCM scm_forced (SCM obj);
bool scm_is_forced (SCM obj);
SCM scm_forced_p (SCM obj);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_CORE_DELAYED_EVALUATION_H */
