/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_GUILE_CORE_POLYNOMIALS_H_
#define SORTSMILL_GUILE_CORE_POLYNOMIALS_H_

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Minimum degree. */
SCM scm_exact_poly_mindegree_mono (SCM poly);
SCM scm_dpoly_mindegree_mono (SCM poly);
SCM scm_dpoly_mindegree_spower (SCM poly);

/* Change degree. */
SCM scm_exact_poly_changedegree_mono (SCM poly, SCM new_degree);
SCM scm_dpoly_changedegree_mono (SCM poly, SCM new_degree);
SCM scm_dpoly_changedegree_bern (SCM poly, SCM new_degree);
SCM scm_dpoly_changedegree_sbern (SCM poly, SCM new_degree);
SCM scm_dpoly_changedegree_spower (SCM poly, SCM new_degree);
SCM scm_dpoly_degreechangeerrorbound_mono (SCM poly, SCM new_degree);
SCM scm_dpoly_degreechangeerrorbound_spower (SCM poly, SCM new_degree);

/* Composition: compute h(t) = f(g(t)). */
SCM scm_exact_poly_compose_mono (SCM f, SCM g);
SCM scm_dpoly_compose_mono (SCM f, SCM g);
SCM scm_dpoly_compose_berndc (SCM f, SCM g);
SCM scm_dpoly_compose_bernh (SCM f, SCM g);
SCM scm_dpoly_compose_sberndc (SCM f, SCM g);
SCM scm_dpoly_compose_sbernh (SCM f, SCM g);
SCM scm_dpoly_compose_spower (SCM f, SCM g);

/* Addition. */
SCM scm_exact_poly_add_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_add_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_add_bern (SCM poly1, SCM poly2);
SCM scm_dpoly_add_sbern (SCM poly1, SCM poly2);
SCM scm_dpoly_add_spower (SCM poly1, SCM poly2);

/* Subtraction. */
SCM scm_exact_poly_sub_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_sub_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_sub_bern (SCM poly1, SCM poly2);
SCM scm_dpoly_sub_sbern (SCM poly1, SCM poly2);
SCM scm_dpoly_sub_spower (SCM poly1, SCM poly2);

/* Negation. */
SCM scm_exact_poly_neg (SCM poly);
SCM scm_dpoly_neg (SCM poly);

/* Additive identity. */
SCM scm_exact_poly_equalszero_p (SCM poly);
SCM scm_dpoly_equalszero_p (SCM poly);

/* Formal differentiation. */
SCM scm_exact_poly_deriv_mono (SCM poly);
SCM scm_dpoly_deriv_mono (SCM poly);
SCM scm_dpoly_deriv_bern (SCM poly);
SCM scm_dpoly_deriv_sbern (SCM poly);
SCM scm_dpoly_deriv_spower (SCM poly);

/* Critical points. */
SCM scm_poly_criticalpoints_mono (SCM poly, SCM a, SCM b);
SCM scm_dpoly_criticalpoints_bernsv (SCM poly, SCM a, SCM b);
SCM scm_dpoly_criticalpoints_berndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_criticalpoints_sbernsv (SCM poly, SCM a, SCM b);
SCM scm_dpoly_criticalpoints_sberndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_criticalpoints_spower (SCM poly, SCM a, SCM b);

/* Division. */
SCM scm_exact_poly_div_mono (SCM poly1, SCM poly2);

/* Greatest common divisor. */
SCM scm_exact_poly_gcd_mono (SCM poly1, SCM poly2);

/* Square-free decomposition. */
SCM scm_exact_poly_squarefree_mono (SCM poly);

/* Evaluation. */
SCM scm_exact_poly_eval_mono (SCM poly, SCM t);
SCM scm_dpoly_eval_mono (SCM poly, SCM t);
SCM scm_dpoly_eval_sbernsv (SCM poly, SCM t);
SCM scm_dpoly_eval_sberndc (SCM poly, SCM t);
SCM scm_dpoly_eval_bernsv (SCM poly, SCM t);
SCM scm_dpoly_eval_berndc (SCM poly, SCM t);
SCM scm_dpoly_eval_spower (SCM poly, SCM t);

/* Multiplication. */
SCM scm_exact_poly_mul_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_mul_mono (SCM poly1, SCM poly2);
SCM scm_dpoly_mul_bern (SCM poly1, SCM poly2);
SCM scm_dpoly_mul_sbern (SCM poly1, SCM poly2);
SCM scm_dpoly_mul_spower (SCM poly1, SCM poly2);

/* Multiplication by a scalar. The basis does not matter. */
SCM scm_exact_poly_scalarmul (SCM poly, SCM r);
SCM scm_dpoly_scalarmul (SCM poly, SCM r);

/* Multiplicative identity. */
SCM scm_exact_poly_equalsone_mono_p (SCM poly);
SCM scm_dpoly_equalsone_mono_p (SCM poly);
SCM scm_dpoly_equalsone_bern_p (SCM poly);
SCM scm_dpoly_equalsone_sbern_p (SCM poly);
SCM scm_dpoly_equalsone_spower_p (SCM poly);

/* Vector cross product in the plane. */
SCM scm_dpoly_crossproduct_mono (SCM ax, SCM ay, SCM bx, SCM by);
SCM scm_dpoly_crossproduct_bern (SCM ax, SCM ay, SCM bx, SCM by);
SCM scm_dpoly_crossproduct_sbern (SCM ax, SCM ay, SCM bx, SCM by);
SCM scm_dpoly_crossproduct_spower (SCM ax, SCM ay, SCM bx, SCM by);

/* Detection of inflections and cusps. */
SCM scm_dpoly_inflections_mono (SCM ax, SCM ay);
SCM scm_dpoly_inflections_bern (SCM ax, SCM ay);
SCM scm_dpoly_inflections_sbern (SCM ax, SCM ay);
SCM scm_dpoly_inflections_spower (SCM ax, SCM ay);

/* Flattening. */
SCM scm_dpoly_flatten_mono (SCM ax, SCM ay, SCM absolute_tolerance,
                            SCM relative_tolerance, SCM max_num_segments);
SCM scm_dpoly_flatten_bern (SCM ax, SCM ay, SCM absolute_tolerance,
                            SCM relative_tolerance, SCM max_num_segments);
SCM scm_dpoly_flatten_sbern (SCM ax, SCM ay, SCM absolute_tolerance,
                             SCM relative_tolerance, SCM max_num_segments);
SCM scm_dpoly_flatten_spower (SCM ax, SCM ay, SCM absolute_tolerance,
                              SCM relative_tolerance, SCM max_num_segments);

/* Instantaneous direction. */
SCM scm_dpoly_direction_bernsv (SCM ax, SCM ay, SCM t);
SCM scm_dpoly_direction_berndc (SCM ax, SCM ay, SCM t);
SCM scm_dpoly_direction_sbernsv (SCM ax, SCM ay, SCM t);
SCM scm_dpoly_direction_sberndc (SCM ax, SCM ay, SCM t);
SCM scm_dpoly_direction_spower (SCM ax, SCM ay, SCM t);

/* Subdivision. */
SCM scm_exact_poly_portion_mono (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_mono (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_berndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_bernh (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_sberndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_sbernh (SCM poly, SCM a, SCM b);
SCM scm_dpoly_portion_spower (SCM poly, SCM a, SCM b);
SCM scm_dpoly_subdiv_bern (SCM poly, SCM t);
SCM scm_dpoly_subdiv_sbern (SCM poly, SCM t);

/* Roots. */
SCM scm_budan_0_1 (SCM poly);   /* Budan’s 0-1 roots test. */
SCM scm_isolate_roots_of_square_free_polynomial (SCM poly, SCM a, SCM b);
SCM scm_dpoly_findroots (SCM mono_poly, SCM a, SCM b, SCM eval_poly);
SCM scm_dpoly_findroots_bernsv (SCM poly, SCM a, SCM b);
SCM scm_dpoly_findroots_berndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_findroots_sbernsv (SCM poly, SCM a, SCM b);
SCM scm_dpoly_findroots_sberndc (SCM poly, SCM a, SCM b);
SCM scm_dpoly_findroots_spower (SCM poly, SCM a, SCM b);

/* Implicitization. */
SCM scm_exact_poly_implicitize_mono (SCM a, SCM b);
SCM scm_dpoly_implicitize_mono (SCM a, SCM b);
SCM scm_dpoly_implicitize_bern (SCM a, SCM b);
SCM scm_dpoly_implicitize_sbern (SCM a, SCM b);
SCM scm_dpoly_implicitize_spower (SCM a, SCM b);

/* Plugging a parametric curve into an implicit curve, for instance to
   find intersections. */
SCM scm_exact_poly_plugintoimplicit_mono (SCM implicit_eq, SCM x, SCM y);
SCM scm_dpoly_plugintoimplicit_mono (SCM implicit_eq, SCM x, SCM y);
SCM scm_dpoly_plugintoimplicit_bern (SCM implicit_eq, SCM x, SCM y);
SCM scm_dpoly_plugintoimplicit_sbern (SCM implicit_eq, SCM x, SCM y);
SCM scm_dpoly_plugintoimplicit_spower (SCM implicit_eq, SCM x, SCM y);

/* Inversion. */
SCM scm_exact_poly_invertdegree1_mono (SCM poly_x, SCM poly_y, SCM x, SCM y);
SCM scm_dpoly_invert_mono (SCM poly_x, SCM poly_y, SCM x, SCM y,
                           SCM ta, SCM tb, SCM absolute_tolerance);

/* Intersections. */
SCM scm_intersect_implicit_with_parametric (SCM implicit_a, SCM bx, SCM by,
                                            SCM t0, SCM t1);
SCM scm_poly_findintersections_mono (SCM ax, SCM ay, SCM implicit_a,
                                     SCM ta0, SCM ta1,
                                     SCM bx, SCM by, SCM implicit_b,
                                     SCM tb0, SCM tb1, SCM tolerance);
SCM scm_dpoly_findintersections_bern (SCM ax, SCM ay, SCM implicit_a,
                                      SCM ta0, SCM ta1,
                                      SCM bx, SCM by, SCM implicit_b,
                                      SCM tb0, SCM tb1, SCM tolerance);
/*
///SCM scm_spoly_findintersections_bernsv (SCM ax, SCM ay, SCM bx, SCM by,
///                                        SCM tolerance);
///SCM scm_spoly_findintersections_berndc (SCM ax, SCM ay, SCM bx, SCM by,
///                                        SCM tolerance);
*/

/* Polynomial coefficient transformations. (Inverses of basis vector
   transformations.) */
SCM scm_exact_poly_mono_to_bern (SCM poly);
SCM scm_exact_poly_bern_to_mono (SCM poly);
SCM scm_exact_poly_mono_to_sbern (SCM poly);
SCM scm_exact_poly_sbern_to_mono (SCM poly);
SCM scm_exact_poly_bern_to_sbern (SCM poly);
SCM scm_exact_poly_sbern_to_bern (SCM poly);
SCM scm_exact_poly_sbern_to_spower (SCM poly);
SCM scm_exact_poly_spower_to_sbern (SCM poly);
SCM scm_exact_poly_bern_to_spower (SCM poly);
SCM scm_exact_poly_spower_to_bern (SCM poly);
SCM scm_exact_poly_mono_to_spower (SCM poly);
SCM scm_exact_poly_spower_to_mono (SCM poly);
SCM scm_dpoly_mono_to_bern (SCM poly);
SCM scm_dpoly_bern_to_mono (SCM poly);
SCM scm_dpoly_mono_to_sbern (SCM poly);
SCM scm_dpoly_sbern_to_mono (SCM poly);
SCM scm_dpoly_bern_to_sbern (SCM poly);
SCM scm_dpoly_sbern_to_bern (SCM poly);
SCM scm_dpoly_sbern_to_spower (SCM poly);
SCM scm_dpoly_spower_to_sbern (SCM poly);
SCM scm_dpoly_bern_to_spower (SCM poly);
SCM scm_dpoly_spower_to_bern (SCM poly);
SCM scm_dpoly_mono_to_spower (SCM poly);
SCM scm_dpoly_spower_to_mono (SCM poly);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_POLYNOMIALS_H_ */
