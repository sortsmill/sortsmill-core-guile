/*
 * Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_GUILE_CORE_VLISTS_H_
#define SORTSMILL_GUILE_CORE_VLISTS_H_

#include <stdbool.h>
#include <stdlib.h>
#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* ...................................................... */
/* Some stuff that users should not use directly ........ */
extern volatile stm_dcl_indicator_t _scm_vlst_null_is_initialized__;
extern SCM _scm_vlst_null__;    /* Use SCM_VLST_NULL instead of this. */
SCM _initialize_scm_vlst_null__ (void);
/* ...................................................... */

/* The public interface follows. */

#define SCM_VLST_NULL                                                   \
  (((bool) stm_dcl_load_indicator (&_scm_vlst_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_vlst_null__) : (_initialize_scm_vlst_null__ ()))

bool scm_is_vlst (SCM obj);
SCM scm_vlst_p (SCM obj);
bool scm_is_vlst_null (SCM obj);
SCM scm_vlst_null_p (SCM obj);
SCM scm_vlst_car (SCM vlst);
SCM scm_vlst_cdr (SCM vlst);
SCM scm_vlst_cons (SCM obj, SCM vlst);
SCM scm_vlst_cons_star (SCM obj, SCM rest);
SCM scm_vlst_acons (SCM key, SCM value, SCM vlst);
size_t scm_c_vlst_length (SCM vlst);
SCM scm_vlst_length (SCM vlst);
SCM scm_c_vlst_ref (SCM vlst, size_t i, SCM dflt);
SCM scm_vlst_ref (SCM vlst, SCM i, SCM dflt);
SCM scm_vlst_copy (SCM vlst);
SCM scm_vlst_acopy (SCM vlst);
SCM scm_vlst_reverse (SCM vlst);
SCM scm_vlst_for_each (SCM proc, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_map_in_order (SCM proc, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_reverse_map (SCM proc, SCM vlst);
SCM scm_vlst_append_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_filter_map (SCM proc, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_count (SCM pred, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_index (SCM pred, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_filter (SCM pred, SCM vlst);
SCM scm_vlst_remp (SCM pred, SCM vlst);
SCM scm_vlst_remove (SCM obj, SCM vlst);
SCM scm_vlst_remv (SCM obj, SCM vlst);
SCM scm_vlst_remq (SCM obj, SCM vlst);
SCM scm_vlst_delete (SCM obj, SCM vlst, SCM pred);
SCM scm_vlst_adelete (SCM key, SCM vlst, SCM pred);
SCM scm_vlst_partition (SCM pred, SCM vlst);
SCM scm_vlst_delete_duplicates (SCM vlst, SCM pred);
SCM scm_vlst_drop (SCM vlst, SCM n, SCM allow_overflow_p);
SCM scm_vlst_take (SCM vlst, SCM n, SCM allow_overflow_p);
SCM scm_vlst_drop_right (SCM vlst, SCM n, SCM allow_overflow_p);
SCM scm_vlst_take_right (SCM vlst, SCM n, SCM allow_overflow_p);
SCM scm_vlst_split_at (SCM vlst, SCM n, SCM allow_overflow_p);
SCM scm_vlst_last_pair (SCM vlst);
SCM scm_vlst_last (SCM vlst);
SCM scm_vlst_append (SCM list_of_vlst);
SCM scm_vlst_append_reverse (SCM vl1, SCM vl2);
SCM scm_vlst_concatenate (SCM vlst_or_list_of_vlst);
SCM scm_vlst_fold_left (SCM proc, SCM init, SCM vlst,
                        SCM another_vlst_or_vlsts);
SCM scm_vlst_fold_right (SCM proc, SCM init, SCM vlst,
                         SCM another_vlst_or_vlsts);
SCM scm_vlst_fold (SCM proc, SCM init, SCM vlst, SCM another_vlst_or_vlsts);
SCM scm_vlst_pair_fold_left (SCM proc, SCM init, SCM vlst,
                             SCM another_vlst_or_vlsts);
SCM scm_vlst_pair_fold_right (SCM proc, SCM init, SCM vlst,
                              SCM another_vlst_or_vlsts);
SCM scm_vlst_pair_fold (SCM proc, SCM init, SCM vlst,
                        SCM another_vlst_or_vlsts);
SCM scm_vlst_sort (SCM before, SCM vlst);
SCM scm_vlst_find (SCM pred, SCM vlst);
SCM scm_vlst_find_tail (SCM pred, SCM vlst);
SCM scm_vlst_drop_while (SCM pred, SCM vlst);
SCM scm_vlst_take_while (SCM pred, SCM vlst);
SCM scm_vlst_span (SCM pred, SCM vlst);
SCM scm_vlst_break (SCM pred, SCM vlst);
SCM scm_vlst_memp (SCM pred, SCM vlst);
SCM scm_vlst_member (SCM obj, SCM vlst, SCM pred);
SCM scm_vlst_memv (SCM obj, SCM vlst);
SCM scm_vlst_memq (SCM obj, SCM vlst);
SCM scm_vlst_assp (SCM pred, SCM vlst);
SCM scm_vlst_assoc (SCM key, SCM vlst, SCM pred);
SCM scm_vlst_assv (SCM key, SCM vlst);
SCM scm_vlst_assq (SCM key, SCM vlst);
SCM scm_vlst_assoc_ref (SCM vlst, SCM key);
SCM scm_vlst_assv_ref (SCM vlst, SCM key);
SCM scm_vlst_assq_ref (SCM vlst, SCM key);
SCM scm_vlst_eq_p (SCM pred, SCM vlsts);
SCM scm_make_vlst (SCM n, SCM fill);
SCM scm_vlst_tabulate (SCM n, SCM proc);
SCM scm_vlst_to_list (SCM vlst);
SCM scm_list_to_vlst (SCM lst);
SCM scm_vlst_to_vector (SCM vlst);
SCM scm_vector_to_vlst (SCM vec);
SCM scm_vlst_1 (SCM a);
SCM scm_vlst_2 (SCM a, SCM b);
SCM scm_vlst_3 (SCM a, SCM b, SCM c);
SCM scm_vlst_4 (SCM a, SCM b, SCM c, SCM d);
SCM scm_vlst_5 (SCM a, SCM b, SCM c, SCM d, SCM e);
SCM scm_vlst_6 (SCM a, SCM b, SCM c, SCM d, SCM e, SCM f);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_VLISTS_H_ */
