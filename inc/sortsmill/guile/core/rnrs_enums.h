/*
 * Copyright (C) 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_RNRS_ENUMS_H
#define _SORTSMILL_GUILE_CORE_RNRS_ENUMS_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM rnrs_make_enumeration (SCM symbol_list);
SCM rnrs_enum_set_universe (SCM enum_set);
SCM rnrs_enum_set_indexer (SCM enum_set);
SCM rnrs_enum_set_constructor (SCM enum_set);
SCM rnrs_enum_set_to_list (SCM enum_set);
SCM rnrs_enum_set_member_p (SCM symbol, SCM enum_set);
SCM rnrs_enum_set_subset_p (SCM enum_set1, SCM enum_set2);
SCM rnrs_enum_set_eq_p (SCM enum_set1, SCM enum_set2);
SCM rnrs_enum_set_union (SCM enum_set1, SCM enum_set2);
SCM rnrs_enum_set_intersection (SCM enum_set1, SCM enum_set2);
SCM rnrs_enum_set_difference (SCM enum_set1, SCM enum_set2);
SCM rnrs_enum_set_complement (SCM enum_set);
SCM rnrs_enum_set_projection (SCM enum_set1, SCM enum_set2);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_RNRS_ENUMS_H */
