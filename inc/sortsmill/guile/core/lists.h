/*
 * Copyright (C) 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_LISTS_H
#define _SORTSMILL_GUILE_LISTS_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

SCM scm_reverse_x_without_checking (SCM lst, SCM tail);

SCM scm_c_reverse_until (SCM lst, size_t n);
SCM scm_reverse_until (SCM lst, SCM n);

SCM scm_c_split_before (SCM lst, size_t n);
SCM scm_split_before (SCM lst, SCM n);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_LISTS_H */
