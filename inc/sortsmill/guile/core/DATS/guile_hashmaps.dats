// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------

#define ATS_DYNLOADFLAG 0       // Avoid ATS’s dynamic loading.

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

staload UN = "prelude/SATS/unsafe.sats"

staload "sortsmill/guile/core/SATS/guile.sats"
staload "sortsmill/guile/core/SATS/guile_hashmaps.sats"

//--------------------------------------------------------------------

%{^
#include <sortsmill/guile/core.h>

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _equal_p_proc,
                             (_equal_p_proc__Value =
                              scm_c_private_ref ("guile", "equal?")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _eq_p_proc,
                             (_eq_p_proc__Value =
                              scm_c_private_ref ("guile", "eq?")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _eqv_p_proc,
                             (_eqv_p_proc__Value =
                              scm_c_private_ref ("guile", "eqv?")));

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _hash_proc,
                             (_hash_proc__Value =
                              scm_c_private_ref ("guile", "hash")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _hashq_proc,
                             (_hashq_proc__Value =
                              scm_c_private_ref ("guile", "hashq")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _hashv_proc,
                             (_hashv_proc__Value =
                              scm_c_private_ref ("guile", "hashv")));
%}

//--------------------------------------------------------------------

local

  fn {} PRETEND_TO_RETURN_SCM () = SCM_UNDEFINED

  staload "sortsmill/core/SATS/hash_maps.sats"
  staload _ = "sortsmill/core/DATS/hash_maps.dats"
  staload _ = "sortsmill/core/DATS/bitstring_maps.dats"

  typedef hash_t = ulint
  typedef key_t = SCM
  typedef value_t = SCM

  fn {} scm_to_hash_t (v : SCM) : hash_t = scm_to_ulong v
  fn {} scm_from_hash_t (h : hash_t) : SCM = scm_from_ulong h

  typedef map_t_map = hash_map (key_t, hash_t, value_t)
  typedef map_t_hashf = key_t -<cloref1> hash_t
  typedef map_t_equalf = (key_t, key_t) -<cloref1> bool
  typedef map_t =
    '{
      map = map_t_map,
      hashf = map_t_hashf,
      equalf = map_t_equalf,
      hash_proc = SCM,
      equal_proc = SCM
    }

  extern castfn scm_t_hashmap_of_map_t : map_t -<> scm_t_hashmap
  extern castfn map_t_of_scm_t_hashmap : scm_t_hashmap -<> map_t

  typedef cursor_t = hash_map_cursor (key_t, hash_t, value_t)

  extern castfn
  scm_t_hashmap_cursor_of_cursor_t : cursor_t -<> scm_t_hashmap_cursor
  extern castfn
  cursor_t_of_scm_t_hashmap_cursor : scm_t_hashmap_cursor -<> cursor_t

  extern fn _equal_p_proc : () -<> SCM = "mac#"
  extern fn _eq_p_proc : () -<> SCM = "mac#"
  extern fn _eqv_p_proc : () -<> SCM = "mac#"
  extern fn _hash_proc : () -<> SCM = "mac#"
  extern fn _hashq_proc : () -<> SCM = "mac#"
  extern fn _hashv_proc : () -<> SCM = "mac#"

  extern fn make_hasher : SCM -> map_t_hashf
  implement make_hasher (hash_proc) =
    let
      val hash_value_max = scm_from_hash_t (lnot ($UN.cast{hash_t} 0))
    in
      if scm_is_eq (hash_proc, _hashq_proc ()) then
        lam (key : key_t) =>
          scm_to_hash_t (scm_hashq (key, hash_value_max))
      else if scm_is_eq (hash_proc, _hashv_proc ()) then
        lam (key : key_t) =>
          scm_to_hash_t (scm_hashv (key, hash_value_max))
      else if scm_is_eq (hash_proc, _hash_proc ()) then
        lam (key : key_t) =>
          scm_to_hash_t (scm_hash (key, hash_value_max))
      else
        lam (key : key_t) =>
          scm_to_hash_t (scm_call_2 (hash_proc, key, hash_value_max))
    end

  extern fn make_key_matcher : SCM -> map_t_equalf
  implement make_key_matcher (equal_proc) =
    if scm_is_eq (equal_proc, _eqv_p_proc ()) then
      lam (key, stored_key : key_t) =>
        scm_is_true (scm_eqv_p (key, stored_key))
    else if scm_is_eq (equal_proc, _eq_p_proc ()) then
      lam (key, stored_key : key_t) =>
        scm_is_eq (key, stored_key)
    else if scm_is_eq (equal_proc, _equal_p_proc ()) then
      lam (key, stored_key : key_t) =>
        scm_is_true (scm_equal_p (key, stored_key))
    else
      lam (key, stored_key : key_t) =>
        scm_is_true (scm_call_2 (equal_proc, key, stored_key))

  extern fn
  _scm_c_make_hashmap__ : (SCM, SCM) -> scm_t_hashmap = "sta#"
  implement
  _scm_c_make_hashmap__ (hash_proc, equal_proc) =
    let
      val hash_proc =
        if SCM_UNBNDP (hash_proc) then _hash_proc () else hash_proc
      val equal_proc =
        if SCM_UNBNDP (equal_proc) then _equal_p_proc () else equal_proc
      val map =
        '{
          map = hash_map_nil,
          hashf = make_hasher hash_proc,
          equalf = make_key_matcher equal_proc,
          hash_proc = hash_proc,
          equal_proc = equal_proc
        }
    in
      scm_t_hashmap_of_map_t map
    end

  extern fn
  _scm_make_hashmap__ : (SCM, SCM) -> SCM = "sta#"
  implement
  _scm_make_hashmap__ (hash_proc, equal_proc) =
    scm_from_scm_t_hashmap (_scm_c_make_hashmap__ (hash_proc, equal_proc))

  extern fn
  _scm_c_make_eq_hashmap__ : () -> scm_t_hashmap = "sta#"
  implement
  _scm_c_make_eq_hashmap__ () =
    _scm_c_make_hashmap__ (_hashq_proc (), _eq_p_proc ())

  extern fn
  _scm_make_eq_hashmap__ : () -> SCM = "sta#"
  implement
  _scm_make_eq_hashmap__ () =
    scm_from_scm_t_hashmap (_scm_c_make_eq_hashmap__ ())

  extern fn
  _scm_c_make_eqv_hashmap__ : () -> scm_t_hashmap = "sta#"
  implement
  _scm_c_make_eqv_hashmap__ () =
    _scm_c_make_hashmap__ (_hashv_proc (), _eqv_p_proc ())

  extern fn
  _scm_make_eqv_hashmap__ : () -> SCM = "sta#"
  implement
  _scm_make_eqv_hashmap__ () =
    scm_from_scm_t_hashmap (_scm_c_make_eqv_hashmap__ ())

  extern fn
  _scm_c_hashmap_hash_function__ : scm_t_hashmap -> SCM = "sta#"
  implement _scm_c_hashmap_hash_function__ map =
    (map_t_of_scm_t_hashmap map).hash_proc

  extern fn
  _scm_hashmap_hash_function__ : SCM -> SCM = "sta#"
  implement _scm_hashmap_hash_function__ map =
    _scm_c_hashmap_hash_function__ (scm_to_scm_t_hashmap map)

  extern fn
  _scm_c_hashmap_equivalence_function__ : scm_t_hashmap -> SCM = "sta#"
  implement _scm_c_hashmap_equivalence_function__ map =
    (map_t_of_scm_t_hashmap map).equal_proc

  extern fn
  _scm_hashmap_equivalence_function__ : SCM -> SCM = "sta#"
  implement _scm_hashmap_equivalence_function__ map =
    _scm_c_hashmap_equivalence_function__ (scm_to_scm_t_hashmap map)

  extern fn
  _scm_hashmap_is_null__ : scm_t_hashmap -> bool = "sta#"
  implement
  _scm_hashmap_is_null__ map =
    hash_map_is_nil (map_t_of_scm_t_hashmap map).map

  extern fn
  _scm_hashmap_null_p__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_null_p__ map =
    scm_from_bool (_scm_hashmap_is_null__ (scm_to_scm_t_hashmap map))

  extern fn
  _scm_c_hashmap_size__ : scm_t_hashmap -> size_t = "sta#"
  implement
  _scm_c_hashmap_size__ map =
    g0ofg1 (hash_map_size (map_t_of_scm_t_hashmap map).map)

  extern fn
  _scm_hashmap_size__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_size__ map =
    scm_from_size_t (_scm_c_hashmap_size__ (scm_to_scm_t_hashmap map))

  extern fn
  _scm_c_hashmap_contains__ : (scm_t_hashmap, SCM) -> bool = "sta#"
  implement
  _scm_c_hashmap_contains__ (map, key) =
    let
      val map = map_t_of_scm_t_hashmap map
      implement hash_map$hash_function<key_t, hash_t> (key) =
        map.hashf (key)
      implement hash_map$keys_match<key_t> (key, stored_key) =
        map.equalf (key, stored_key)
    in
      hash_map_has_key<key_t, hash_t, value_t> (map.map, key)
    end

  extern fn
  _scm_hashmap_contains_p__ : (SCM, SCM) -> SCM = "sta#"
  implement
  _scm_hashmap_contains_p__ (map, key) =
    scm_from_bool (_scm_c_hashmap_contains__ (scm_to_scm_t_hashmap map, key))

  extern fn
  _scm_c_hashmap_ref__ : (scm_t_hashmap, key_t, value_t) -> value_t = "sta#"
  implement
  _scm_c_hashmap_ref__ (map, key, default_value) =
    let
      val map = map_t_of_scm_t_hashmap map
      implement hash_map$hash_function<key_t, hash_t> (key) =
        map.hashf (key)
      implement hash_map$keys_match<key_t> (key, stored_key) =
        map.equalf (key, stored_key)
      var value : value_t?
      val value_found =
        hash_map_get_eff<key_t, hash_t, value_t> (map.map, key, value)
    in
      if value_found then
        opt_unsome_get value
      else
        let
          prval () = opt_unnone value
        in
          if SCM_UNBNDP (default_value) then
            begin
              (* FIXME: We might want to improve this exception,
                 for instance by using R6RS exceptions. *)
              scm_misc_error ("hashmap-ref", "key not found: ~S",
                              scm_list_1 (key));
              PRETEND_TO_RETURN_SCM ()
            end
          else
            default_value
        end
    end

  extern fn
  _scm_hashmap_ref__ : (SCM, key_t, value_t) -> value_t = "sta#"
  implement
  _scm_hashmap_ref__ (map, key, default_value) =
    _scm_c_hashmap_ref__ (scm_to_scm_t_hashmap map, key, default_value)

  extern fn
  _scm_c_hashmap_set__ : (scm_t_hashmap, key_t, value_t) -> scm_t_hashmap = "sta#"
  implement
  _scm_c_hashmap_set__ (map, key, value) =
    let
      val map = map_t_of_scm_t_hashmap map
      implement hash_map$hash_function<key_t, hash_t> (key) =
        map.hashf (key)
      implement hash_map$keys_match<key_t> (key, stored_key) =
        map.equalf (key, stored_key)
      val map =
        '{
          map = hash_map_set<key_t, hash_t, value_t> (map.map, key, value),
          hashf = map.hashf,
          equalf = map.equalf,
          hash_proc = map.hash_proc,
          equal_proc = map.equal_proc
        }
    in
      scm_t_hashmap_of_map_t map
    end
  
  extern fn
  _scm_hashmap_set__ : (SCM, key_t, value_t) -> value_t = "sta#"
  implement
  _scm_hashmap_set__ (map, key, value) =
    let
      val map = _scm_c_hashmap_set__ (scm_to_scm_t_hashmap map, key, value)
    in
      scm_from_scm_t_hashmap map
    end

  extern fn
  _scm_c_hashmap_remove__ : (scm_t_hashmap, key_t) -> scm_t_hashmap = "sta#"
  implement
  _scm_c_hashmap_remove__ (map, key) =
    let
      val map = map_t_of_scm_t_hashmap map
      implement hash_map$hash_function<key_t, hash_t> (key) =
        map.hashf (key)
      implement hash_map$keys_match<key_t> (key, stored_key) =
        map.equalf (key, stored_key)
      val map =
        '{
          map = hash_map_remove<key_t, hash_t, value_t> (map.map, key),
          hashf = map.hashf,
          equalf = map.equalf,
          hash_proc = map.hash_proc,
          equal_proc = map.equal_proc
        }
    in
      scm_t_hashmap_of_map_t map
    end
  
  extern fn
  _scm_hashmap_remove__ : (SCM, key_t) -> value_t = "sta#"
  implement
  _scm_hashmap_remove__ (map, key) =
    let
      val map = _scm_c_hashmap_remove__ (scm_to_scm_t_hashmap map, key)
    in
      scm_from_scm_t_hashmap map
    end

  extern fn
  _scm_c_make_hashmap_cursor__ : scm_t_hashmap -> scm_t_hashmap_cursor = "sta#"
  implement _scm_c_make_hashmap_cursor__ map =
    let
      val map = map_t_of_scm_t_hashmap map
      val cursor = hash_map_first<key_t, hash_t, value_t> map.map
    in
      scm_t_hashmap_cursor_of_cursor_t cursor
    end

  extern fn
  _scm_make_hashmap_cursor__ : SCM -> SCM = "sta#"
  implement
  _scm_make_hashmap_cursor__ map =
    let
      val cursor = _scm_c_make_hashmap_cursor__ (scm_to_scm_t_hashmap map)
    in
      scm_from_scm_t_hashmap_cursor cursor
    end

  extern fn
  _scm_hashmap_cursor_is_null__ : scm_t_hashmap_cursor -> bool = "sta#"
  implement
  _scm_hashmap_cursor_is_null__ cursor =
    hash_map_cursor_is_nil (cursor_t_of_scm_t_hashmap_cursor cursor)

  extern fn
  _scm_hashmap_cursor_null_p__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_cursor_null_p__ cursor =
    let
      val is_null =
        _scm_hashmap_cursor_is_null__ (scm_to_scm_t_hashmap_cursor cursor)
    in
      scm_from_bool is_null
    end

  extern fn
  _scm_c_hashmap_cursor_next__ : scm_t_hashmap_cursor -> scm_t_hashmap_cursor = "sta#"
  implement
  _scm_c_hashmap_cursor_next__ cursor =
    let
      val cursor = cursor_t_of_scm_t_hashmap_cursor cursor
      val cursor = hash_map_next<key_t, hash_t, value_t> cursor
      val cursor = scm_t_hashmap_cursor_of_cursor_t cursor
    in
      cursor
    end

  extern fn
  _scm_hashmap_cursor_next__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_cursor_next__ cursor =
    let
      val cursor = scm_to_scm_t_hashmap_cursor cursor
      val cursor = _scm_c_hashmap_cursor_next__ cursor
      val cursor = scm_from_scm_t_hashmap_cursor cursor
    in
      cursor
    end

  extern fn
  _scm_c_hashmap_cursor_entry__ : scm_t_hashmap_cursor -> SCM = "sta#"
  implement
  _scm_c_hashmap_cursor_entry__ cursor =
    let
      val cursor = cursor_t_of_scm_t_hashmap_cursor cursor
    in
      if hash_map_cursor_is_nil cursor then
        begin
          (* FIXME: We might want to improve this exception,
             for instance by using R6RS exceptions. *)
          scm_misc_error ("hashmap-cursor-entry",
                          "attempt to dereference a null cursor",
                          SCM_EOL);
          PRETEND_TO_RETURN_SCM ()
        end
      else
        let
          val @(key, value) = hash_map_pair<key_t, hash_t, value_t> cursor
        in
          scm_cons (key, value)
        end
    end

  extern fn
  _scm_hashmap_cursor_entry__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_cursor_entry__ cursor =
    _scm_c_hashmap_cursor_entry__ (scm_to_scm_t_hashmap_cursor cursor)

  extern fn
  _scm_c_hashmap_cursor_key__ : scm_t_hashmap_cursor -> SCM = "sta#"
  implement
  _scm_c_hashmap_cursor_key__ cursor =
    let
      val cursor = cursor_t_of_scm_t_hashmap_cursor cursor
    in
      if hash_map_cursor_is_nil cursor then
        begin
          (* FIXME: We might want to improve this exception,
             for instance by using R6RS exceptions. *)
          scm_misc_error ("hashmap-cursor-key",
                          "attempt to dereference a null cursor",
                          SCM_EOL);
          PRETEND_TO_RETURN_SCM ()
        end
      else
        hash_map_key<key_t, hash_t, value_t> cursor
    end

  extern fn
  _scm_hashmap_cursor_key__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_cursor_key__ cursor =
    _scm_c_hashmap_cursor_key__ (scm_to_scm_t_hashmap_cursor cursor)

  extern fn
  _scm_c_hashmap_cursor_value__ : scm_t_hashmap_cursor -> SCM = "sta#"
  implement
  _scm_c_hashmap_cursor_value__ cursor =
    let
      val cursor = cursor_t_of_scm_t_hashmap_cursor cursor
    in
      if hash_map_cursor_is_nil cursor then
        begin
          (* FIXME: We might want to improve this exception,
             for instance by using R6RS exceptions. *)
          scm_misc_error ("hashmap-cursor-value",
                          "attempt to dereference a null cursor",
                          SCM_EOL);
          PRETEND_TO_RETURN_SCM ()
        end
      else
        hash_map_value<key_t, hash_t, value_t> cursor
    end

  extern fn
  _scm_hashmap_cursor_value__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_cursor_value__ cursor =
    _scm_c_hashmap_cursor_value__ (scm_to_scm_t_hashmap_cursor cursor)

  extern fn
  _scm_c_hashmap_to_alist__ : scm_t_hashmap -> SCM = "sta#"
  implement
  _scm_c_hashmap_to_alist__ map =
    let
      fun recursion (cursor : cursor_t, lst : SCM) : SCM =
        if hash_map_cursor_is_nil cursor then
          (* FIXME: Maybe change this to use my
             scm_reverse_x_without_checking() function. *)
          scm_reverse_x (lst, SCM_EOL)
        else
          let
            val @(key, value) = hash_map_pair<key_t, hash_t, value_t> cursor
          in
            recursion (hash_map_next<key_t, hash_t, value_t> cursor,
                       scm_acons (key, value, lst))
          end

      val map = (map_t_of_scm_t_hashmap map).map
      val cursor = hash_map_first<key_t, hash_t, value_t> map
    in
      recursion (cursor, SCM_EOL)
    end

  extern fn
  _scm_hashmap_to_alist__ : SCM -> SCM = "sta#"
  implement
  _scm_hashmap_to_alist__ map =
    _scm_c_hashmap_to_alist__ (scm_to_scm_t_hashmap map)

  extern fn
  _scm_c_hashmap_set_from_alist__ : (scm_t_hashmap, SCM) -> scm_t_hashmap = "sta#"
  implement
  _scm_c_hashmap_set_from_alist__ (map, alist) =
    let
      val map = map_t_of_scm_t_hashmap map
      val hashf = map.hashf
      val equalf = map.equalf
      val hash_proc = map.hash_proc
      val equal_proc = map.equal_proc

      implement hash_map$hash_function<key_t, hash_t> (key) =
        hashf (key)
      implement hash_map$keys_match<key_t> (key, stored_key) =
        equalf (key, stored_key)

      fun recursion (map : hash_map (key_t, hash_t, value_t),
                     alist : SCM) :<cloref1> map_t =
        if scm_is_null alist then
          '{
            map = map,
            hashf = hashf,
            equalf = equalf,
            hash_proc = hash_proc,
            equal_proc = equal_proc
          }
        else
          let
            val head = scm_car alist
            val key = scm_car head
            val value = scm_cdr head
            val map = hash_map_set<key_t, hash_t, value_t> (map, key, value)
            val alist = scm_cdr alist
          in
            recursion (map, alist)
          end
    in
      scm_t_hashmap_of_map_t (recursion (map.map, alist))
    end

  extern fn
  _scm_hashmap_set_from_alist__ : (SCM, SCM) -> SCM = "sta#"
  implement
  _scm_hashmap_set_from_alist__ (map, alist) =
    let
      val map = scm_to_scm_t_hashmap map
      val map = _scm_c_hashmap_set_from_alist__ (map, alist)
      val map = scm_from_scm_t_hashmap map
    in
      map
    end

  extern fn
  _scm_c_alist_to_hashmap__ : (SCM, SCM, SCM) -> scm_t_hashmap = "sta#"
  implement
  _scm_c_alist_to_hashmap__ (alist, hash_proc, equal_proc) =
    let
      val map = _scm_c_make_hashmap__ (hash_proc, equal_proc)
    in
      _scm_c_hashmap_set_from_alist__ (map, alist)
    end

  extern fn
  _scm_alist_to_hashmap__ : (SCM, SCM, SCM) -> SCM = "sta#"
  implement
  _scm_alist_to_hashmap__ (alist, hash_proc, equal_proc) =
    let
      val map = _scm_c_alist_to_hashmap__ (alist, hash_proc, equal_proc)
    in
      scm_from_scm_t_hashmap map
    end

  extern fn
  _scm_c_alist_to_eq_hashmap__ : SCM -> scm_t_hashmap = "sta#"
  implement
  _scm_c_alist_to_eq_hashmap__ alist =
    let
      val map = _scm_c_make_eq_hashmap__ ()
    in
      _scm_c_hashmap_set_from_alist__ (map, alist)
    end

  extern fn
  _scm_alist_to_eq_hashmap__ : SCM -> SCM = "sta#"
  implement
  _scm_alist_to_eq_hashmap__ alist =
    let
      val map = _scm_c_alist_to_eq_hashmap__ (alist)
    in
      scm_from_scm_t_hashmap map
    end

  extern fn
  _scm_c_alist_to_eqv_hashmap__ : SCM -> scm_t_hashmap = "sta#"
  implement
  _scm_c_alist_to_eqv_hashmap__ alist =
    let
      val map = _scm_c_make_eqv_hashmap__ ()
    in
      _scm_c_hashmap_set_from_alist__ (map, alist)
    end

  extern fn
  _scm_alist_to_eqv_hashmap__ : SCM -> SCM = "sta#"
  implement
  _scm_alist_to_eqv_hashmap__ alist =
    let
      val map = _scm_c_alist_to_eqv_hashmap__ (alist)
    in
      scm_from_scm_t_hashmap map
    end

in

  implement scm_c_make_hashmap0 () =
    _scm_c_make_hashmap__ (SCM_UNDEFINED, SCM_UNDEFINED)
  implement scm_c_make_hashmap1 (hash_proc) =
    _scm_c_make_hashmap__ (hash_proc, SCM_UNDEFINED)
  implement scm_c_make_hashmap2 (hash_proc, equal_proc) =
    _scm_c_make_hashmap__ (hash_proc, equal_proc)

  implement scm_make_hashmap0 () =
    _scm_make_hashmap__ (SCM_UNDEFINED, SCM_UNDEFINED)
  implement scm_make_hashmap1 (hash_proc) =
    _scm_make_hashmap__ (hash_proc, SCM_UNDEFINED)
  implement scm_make_hashmap2 (hash_proc, equal_proc) =
    _scm_make_hashmap__ (hash_proc, equal_proc)

  implement scm_c_make_eq_hashmap () =
    _scm_c_make_eq_hashmap__ ()
  implement scm_make_eq_hashmap () =
    _scm_make_eq_hashmap__ ()

  implement scm_c_make_eqv_hashmap () =
    _scm_c_make_eqv_hashmap__ ()
  implement scm_make_eqv_hashmap () =
    _scm_make_eqv_hashmap__ ()

  implement scm_c_hashmap_hash_function map =
    _scm_c_hashmap_hash_function__ map
  implement scm_hashmap_hash_function map =
    _scm_hashmap_hash_function__ map

  implement scm_c_hashmap_equivalence_function map =
    _scm_c_hashmap_equivalence_function__ map
  implement scm_hashmap_equivalence_function map =
    _scm_hashmap_equivalence_function__ map

  implement scm_hashmap_is_null map =
    _scm_hashmap_is_null__ map
  implement scm_hashmap_null_p map =
    _scm_hashmap_null_p__ map

  implement scm_c_hashmap_size map =
    _scm_c_hashmap_size__ map
  implement scm_hashmap_size map =
    _scm_hashmap_size__ map

  implement scm_c_hashmap_contains (map, key) =
    _scm_c_hashmap_contains__ (map, key)
  implement scm_hashmap_contains_p (map, key) =
    _scm_hashmap_contains_p__ (map, key)

  implement scm_c_hashmap_ref2 (map, key) =
    _scm_c_hashmap_ref__ (map, key, SCM_UNDEFINED)
  implement scm_c_hashmap_ref3 (map, key, default_value) =
    _scm_c_hashmap_ref__ (map, key, default_value)

  implement scm_hashmap_ref2 (map, key) =
    _scm_hashmap_ref__ (map, key, SCM_UNDEFINED)
  implement scm_hashmap_ref3 (map, key, default_value) =
    _scm_hashmap_ref__ (map, key, default_value)

  implement scm_c_hashmap_set (map, key, value) =
    _scm_c_hashmap_set__ (map, key, value)
  implement scm_hashmap_set (map, key, value) =
    _scm_hashmap_set__ (map, key, value)

  implement scm_c_hashmap_remove (map, key) =
    _scm_c_hashmap_remove__ (map, key)
  implement scm_hashmap_remove (map, key) =
    _scm_hashmap_remove__ (map, key)

  implement scm_c_make_hashmap_cursor map =
    _scm_c_make_hashmap_cursor__ map
  implement scm_make_hashmap_cursor map =
    _scm_make_hashmap_cursor__ map

  implement scm_hashmap_cursor_is_null cursor =
    _scm_hashmap_cursor_is_null__ cursor
  implement scm_hashmap_cursor_null_p cursor =
    _scm_hashmap_cursor_null_p__ cursor

  implement scm_c_hashmap_cursor_next cursor =
    _scm_c_hashmap_cursor_next__ cursor
  implement scm_hashmap_cursor_next cursor =
    _scm_hashmap_cursor_next__ cursor

  implement scm_c_hashmap_cursor_entry cursor =
    _scm_c_hashmap_cursor_entry__ cursor
  implement scm_hashmap_cursor_entry cursor =
    _scm_hashmap_cursor_entry__ cursor

  implement scm_c_hashmap_cursor_key cursor =
    _scm_c_hashmap_cursor_key__ cursor
  implement scm_hashmap_cursor_key cursor =
    _scm_hashmap_cursor_key__ cursor

  implement scm_c_hashmap_cursor_value cursor =
    _scm_c_hashmap_cursor_value__ cursor
  implement scm_hashmap_cursor_value cursor =
    _scm_hashmap_cursor_value__ cursor

  implement scm_c_hashmap_to_alist map =
    _scm_c_hashmap_to_alist__ map
  implement scm_hashmap_to_alist map =
    _scm_hashmap_to_alist__ map

  implement scm_c_hashmap_set_from_alist (map, alist) =
    _scm_c_hashmap_set_from_alist__ (map, alist)
  implement scm_hashmap_set_from_alist (map, alist) =
    _scm_hashmap_set_from_alist__ (map, alist)

  implement scm_c_alist_to_hashmap1 (alist) =
    _scm_c_alist_to_hashmap__ (alist, SCM_UNDEFINED, SCM_UNDEFINED)
  implement scm_c_alist_to_hashmap2 (alist, hash_proc) =
    _scm_c_alist_to_hashmap__ (alist, hash_proc, SCM_UNDEFINED)
  implement scm_c_alist_to_hashmap3 (alist, hash_proc, equal_proc) =
    _scm_c_alist_to_hashmap__ (alist, hash_proc, equal_proc)

  implement scm_alist_to_hashmap1 (alist) =
    _scm_alist_to_hashmap__ (alist, SCM_UNDEFINED, SCM_UNDEFINED)
  implement scm_alist_to_hashmap2 (alist, hash_proc) =
    _scm_alist_to_hashmap__ (alist, hash_proc, SCM_UNDEFINED)
  implement scm_alist_to_hashmap3 (alist, hash_proc, equal_proc) =
    _scm_alist_to_hashmap__ (alist, hash_proc, equal_proc)

  implement scm_c_alist_to_eq_hashmap alist =
    _scm_c_alist_to_eq_hashmap__ alist
  implement scm_alist_to_eq_hashmap alist =
    _scm_alist_to_eq_hashmap__ alist

  implement scm_c_alist_to_eqv_hashmap alist =
    _scm_c_alist_to_eqv_hashmap__ alist
  implement scm_alist_to_eqv_hashmap alist =
    _scm_alist_to_eqv_hashmap__ alist

end

//--------------------------------------------------------------------

local

  extern fn
  _scm_c_hashmap_is_equal__ : (scm_t_hashmap, scm_t_hashmap, SCM) -> bool = "sta#"
  implement
  _scm_c_hashmap_is_equal__ (x, y, equal_proc) =
    if $UN.cast{ptr} x = $UN.cast{ptr} y then
      // A map is equal to itself. We make this so regardless
      // of the equal_proc, for efficiency. If the equal_proc
      // always returns true if `eq?' would return true, then
      // this special case actually is not a special case.
      true
    else if x.size () != y.size () then
      // Maps of unequal size are unequal.
      false
    else if not (scm_is_eq (x.equivalence_function (),
                            y.equivalence_function ())) then
      // Maps with different equivalence functions are unequal.
      false
    else
      let
        fn {} values_match (x, y : SCM) : bool =
          scm_is_true (if SCM_UNBNDP equal_proc then
                         scm_equal_p (x, y)
                       else
                         scm_call_2 (equal_proc, x, y))

        fun loop (xcursor : scm_t_hashmap_cursor) :<cloref1> bool =
          if xcursor.is_null () then
            // The maps contain the same set of keys, with
            // the same equivalence function, and the value
            // paired with a given key in x is equal to the
            // value paired with that key in y. Therefore
            // the maps are equal.
            true
          else if not (y.contains (xcursor.key ())) then
            // The two maps have different sets of keys.
            // Therefore the maps are unequal.
            false
          else if not (values_match (xcursor.value (),
                                     y.ref (xcursor.key ()))) then
            // The same key is paired with unequal values.
            // Therefore the maps are unequal.
            false
          else
            loop (xcursor.next ())
      in
        loop (x.cursor ())
      end

  extern fn
  _scm_hashmap_is_equal__ : (SCM, SCM, SCM) -> bool = "sta#"
  implement
  _scm_hashmap_is_equal__ (x, y, equal_proc) =
    _scm_c_hashmap_is_equal__ (scm_to_scm_t_hashmap x,
                               scm_to_scm_t_hashmap y,
                               equal_proc)

  extern fn
  _scm_hashmap_equal_p__ : (SCM, SCM, SCM) -> SCM = "sta#"
  implement
  _scm_hashmap_equal_p__ (x, y, equal_proc) =
    scm_from_bool (_scm_hashmap_is_equal__ (x, y, equal_proc))

in

  implement scm_c_hashmap_is_equal = _scm_c_hashmap_is_equal__
  implement scm_hashmap_is_equal = _scm_hashmap_is_equal__
  implement scm_hashmap_equal_p = _scm_hashmap_equal_p__

end

//--------------------------------------------------------------------

%{$

#include <sortsmill/guile/core/hashmaps.h>

scm_t_hashmap
scm_c_make_hashmap (SCM hash_proc, SCM equal_proc)
{
  return _scm_c_make_hashmap__ (hash_proc, equal_proc);
}

SCM
scm_make_hashmap (SCM hash_proc, SCM equal_proc)
{
  return _scm_make_hashmap__ (hash_proc, equal_proc);
}

scm_t_hashmap
scm_c_make_eq_hashmap (void)
{
  return _scm_c_make_eq_hashmap__ ();
}

SCM
scm_make_eq_hashmap (void)
{
  return _scm_make_eq_hashmap__ ();
}

scm_t_hashmap
scm_c_make_eqv_hashmap (void)
{
  return _scm_c_make_eqv_hashmap__ ();
}

SCM
scm_make_eqv_hashmap (void)
{
  return _scm_make_eqv_hashmap__ ();
}

SCM
scm_c_hashmap_hash_function (scm_t_hashmap map)
{
  return _scm_c_hashmap_hash_function__ (map);
}

SCM
scm_hashmap_hash_function (SCM map)
{
  return _scm_hashmap_hash_function__ (map);
}

SCM
scm_c_hashmap_equivalence_function (scm_t_hashmap map)
{
  return _scm_c_hashmap_equivalence_function__ (map);
}

SCM
scm_hashmap_equivalence_function (SCM map)
{
  return _scm_hashmap_equivalence_function__ (map);
}

bool
scm_hashmap_is_null (scm_t_hashmap map)
{
  return _scm_hashmap_is_null__ (map);
}

SCM
scm_hashmap_null_p (SCM map)
{
  return _scm_hashmap_null_p__ (map);
}

size_t
scm_c_hashmap_size (scm_t_hashmap map)
{
  return _scm_c_hashmap_size__ (map);
}

SCM
scm_hashmap_size (SCM map)
{
  return _scm_hashmap_size__ (map);
}

bool
scm_c_hashmap_contains (scm_t_hashmap map, SCM key)
{
  return _scm_c_hashmap_contains__ (map, key);
}

SCM
scm_hashmap_contains_p (SCM map, SCM key)
{
  return _scm_hashmap_contains_p__ (map, key);
}

SCM
scm_c_hashmap_ref (scm_t_hashmap map, SCM key, SCM default_value)
{
  return _scm_c_hashmap_ref__ (map, key, default_value);
}

SCM
scm_hashmap_ref (SCM map, SCM key, SCM default_value)
{
  return _scm_hashmap_ref__ (map, key, default_value);
}

scm_t_hashmap
scm_c_hashmap_set (scm_t_hashmap map, SCM key, SCM value)
{
  return _scm_c_hashmap_set__ (map, key, value);
}

SCM
scm_hashmap_set (SCM map, SCM key, SCM value)
{
  return _scm_hashmap_set__ (map, key, value);
}

scm_t_hashmap
scm_c_hashmap_remove (scm_t_hashmap map, SCM key)
{
  return _scm_c_hashmap_remove__ (map, key);
}

SCM
scm_hashmap_remove (SCM map, SCM key)
{
  return _scm_hashmap_remove__ (map, key);
}

bool
scm_hashmap_cursor_is_null (scm_t_hashmap_cursor cursor)
{
  return _scm_hashmap_cursor_is_null__ (cursor);
}

SCM
scm_hashmap_cursor_null_p (SCM cursor)
{
  return _scm_hashmap_cursor_null_p__ (cursor);
}

scm_t_hashmap_cursor
scm_c_make_hashmap_cursor (scm_t_hashmap map)
{
  return _scm_c_make_hashmap_cursor__ (map);
}

SCM
scm_make_hashmap_cursor (SCM map)
{
  return _scm_make_hashmap_cursor__ (map);
}

scm_t_hashmap_cursor
scm_c_hashmap_cursor_next (scm_t_hashmap_cursor cursor)
{
  return _scm_c_hashmap_cursor_next__ (cursor);
}

SCM
scm_hashmap_cursor_next (SCM cursor)
{
  return _scm_hashmap_cursor_next__ (cursor);
}

SCM
scm_c_hashmap_cursor_entry (scm_t_hashmap_cursor cursor)
{
  return _scm_c_hashmap_cursor_entry__ (cursor);
}

SCM
scm_hashmap_cursor_entry (SCM cursor)
{
  return _scm_hashmap_cursor_entry__ (cursor);
}

SCM
scm_c_hashmap_cursor_key (scm_t_hashmap_cursor cursor)
{
  return _scm_c_hashmap_cursor_key__ (cursor);
}

SCM
scm_hashmap_cursor_key (SCM cursor)
{
  return _scm_hashmap_cursor_key__ (cursor);
}

SCM
scm_c_hashmap_cursor_value (scm_t_hashmap_cursor cursor)
{
  return _scm_c_hashmap_cursor_value__ (cursor);
}

SCM
scm_hashmap_cursor_value (SCM cursor)
{
  return _scm_hashmap_cursor_value__ (cursor);
}

SCM
scm_c_hashmap_to_alist (scm_t_hashmap map)
{
  return _scm_c_hashmap_to_alist__ (map);
}

SCM
scm_hashmap_to_alist (SCM map)
{
  return _scm_hashmap_to_alist__ (map);
}

scm_t_hashmap
scm_c_hashmap_set_from_alist (scm_t_hashmap map, SCM alist)
{
  return _scm_c_hashmap_set_from_alist__ (map, alist);
}

SCM
scm_hashmap_set_from_alist (SCM map, SCM alist)
{
  return _scm_hashmap_set_from_alist__ (map, alist);
}

scm_t_hashmap
scm_c_alist_to_hashmap (SCM alist, SCM hash_proc, SCM equal_proc)
{
  return _scm_c_alist_to_hashmap__ (alist, hash_proc, equal_proc);
}

SCM
scm_alist_to_hashmap (SCM alist, SCM hash_proc, SCM equal_proc)
{
  return _scm_alist_to_hashmap__ (alist, hash_proc, equal_proc);
}

scm_t_hashmap
scm_c_alist_to_eq_hashmap (SCM alist)
{
  return _scm_c_alist_to_eq_hashmap__ (alist);
}

SCM
scm_alist_to_eq_hashmap (SCM alist)
{
  return _scm_alist_to_eq_hashmap__ (alist);
}

scm_t_hashmap
scm_c_alist_to_eqv_hashmap (SCM alist)
{
  return _scm_c_alist_to_eqv_hashmap__ (alist);
}

SCM
scm_alist_to_eqv_hashmap (SCM alist)
{
  return _scm_alist_to_eqv_hashmap__ (alist);
}

bool
scm_c_hashmap_is_equal (scm_t_hashmap x, scm_t_hashmap y,
                        SCM equal_proc)
{
  return _scm_c_hashmap_is_equal__ (x, y, equal_proc);
}

bool
scm_hashmap_is_equal (SCM x, SCM y, SCM equal_proc)
{
  return _scm_hashmap_is_equal__ (x, y, equal_proc);
}

SCM
scm_hashmap_equal_p (SCM x, SCM y, SCM equal_proc)
{
  return _scm_hashmap_equal_p__ (x, y, equal_proc);
}

%}

//--------------------------------------------------------------------
