/*
 * Copyright (C) 2017 Khaled Hosny and Barry Schwartz
 * Copyright (C) 2014 Free Software Foundation, Inc.
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

/* This code is based loosely on the foreign objects implementation in
   Guile 2.2.2. */

#ifndef SORTSMILL_GUILE_CORE_EXOTIC_OBJECTS_H__
#define SORTSMILL_GUILE_CORE_EXOTIC_OBJECTS_H__

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

#include <libguile.h>

/* ------------------------------------------------------------------- */
/*
 * FOR INTERNAL USE BY THE LIBRARY.
 */

void scm_raise_exotic_object_type_assertion_failure__ (SCM type, SCM val);

#if defined (SCM_IS_A_P)

inline bool exotic_objects_scm_is_a_p__ (SCM val, SCM type);

inline bool
exotic_objects_scm_is_a_p__ (SCM val, SCM type)
{
  return SCM_IS_A_P (val, type);
}

#else                           /* !defined (SCM_IS_A_P) */

/*
 * This branch probably never is taken, but SCM_IS_A_P is an
 * undocumented macro, so let us be cautious.
 */

bool exotic_objects_scm_is_a_p__ (SCM val, SCM type);

#endif /* !defined (SCM_IS_A_P) */

/* ------------------------------------------------------------------- */
/*
 * PUBLIC API.
 */

/*
 * WARNING WARNING WARNING:
 *
 * In general, it is left as a responsibility of the programmer to
 * ensure that ‘type’ arguments are valid exotic object types. The
 * design is this way for efficiency.
 *
 * You can call corresponding Scheme procedures in
 * (sortsmill core exotic-objects) if you want more thorough checking.
 */

SCM scm_c_utf8_make_exotic_object_type (const char *name);
SCM scm_make_exotic_object_type (SCM name);

inline bool scm_is_exotic_object (SCM val, SCM type);
inline SCM scm_exotic_object_p (SCM val, SCM type);
inline void scm_assert_exotic_object_type (SCM type, SCM val);
inline void *scm_c_exotic_object_ref (SCM type, SCM obj);
inline void scm_c_exotic_object_set_x (SCM type, SCM obj, const void *val);
inline void *scm_c_unchecked_exotic_object_ref (SCM obj);
inline void scm_c_unchecked_exotic_object_set_x (SCM obj, const void *val);
inline SCM scm_exotic_object_ref (SCM type, SCM obj);
inline void scm_exotic_object_set_x (SCM type, SCM obj, SCM val);
inline SCM scm_unchecked_exotic_object_ref (SCM type, SCM obj);
inline void scm_unchecked_exotic_object_set_x (SCM type, SCM obj, SCM val);

SCM scm_c_make_exotic_object (SCM type, const void *val);
SCM scm_make_exotic_object (SCM type, SCM val);

inline bool
scm_is_exotic_object (SCM val, SCM type)
{
  return exotic_objects_scm_is_a_p__ (val, type);
}

inline SCM
scm_exotic_object_p (SCM val, SCM type)
{
  return scm_from_bool (exotic_objects_scm_is_a_p__ (val, type));
}

inline void
scm_assert_exotic_object_type (SCM type, SCM val)
{
  if (!exotic_objects_scm_is_a_p__ (val, type))
    scm_raise_exotic_object_type_assertion_failure__ (type, val);
}

inline void *
scm_c_exotic_object_ref (SCM type, SCM obj)
{
  scm_assert_exotic_object_type (type, obj);
  return (void *) ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0];
}

inline void
scm_c_exotic_object_set_x (SCM type, SCM obj, const void *val)
{
  scm_assert_exotic_object_type (type, obj);
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
}

inline void *
scm_c_unchecked_exotic_object_ref (SCM obj)
{
  return (void *) ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0];
}

inline void
scm_c_unchecked_exotic_object_set_x (SCM obj, const void *val)
{
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
}

inline SCM
scm_exotic_object_ref (SCM type, SCM obj)
{
  scm_assert_exotic_object_type (type, obj);
  return (SCM) ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0];
}

inline void
scm_exotic_object_set_x (SCM type, SCM obj, SCM val)
{
  scm_assert_exotic_object_type (type, obj);
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
}

inline SCM
scm_unchecked_exotic_object_ref (SCM type, SCM obj)
{
  return (SCM) ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0];
}

inline void
scm_unchecked_exotic_object_set_x (SCM type, SCM obj, SCM val)
{
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_EXOTIC_OBJECTS_H__ */
