/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_BINCOEF_H
#define _SORTSMILL_GUILE_CORE_BINCOEF_H

#include <libguile.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Binary coefficients C(n,k). */
SCM scm_c_bincoef (uintmax_t n, uintmax_t k);
SCM scm_bincoef (SCM n, SCM k);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_BINCOEF_H */
