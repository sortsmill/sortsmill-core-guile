/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_LINEAR_SYSTEMS_H
#define _SORTSMILL_GUILE_CORE_LINEAR_SYSTEMS_H

#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* Solve AX=B by gaussian elimination with partial pivoting. Returns X
   (or #f on failure to solve) and an informational integer. FIXME:
   Document more thoroughly. */
SCM scm_sgausselimsolve (SCM A, SCM B); /* single precision */
SCM scm_dgausselimsolve (SCM A, SCM B); /* double precision */

/* Solve AX=B (matrix RHS) or Ax*=b* (vector RHS) by gaussian
   elimination with partial pivoting, where A is tridiagonal. Returns
   X or x* (or #f on failure to solve), and an informational
   integer. FIXME: Document more thoroughly. */
SCM scm_stridiagsolve (SCM subdiag, SCM diag, SCM superdiag, SCM B);
SCM scm_dtridiagsolve (SCM subdiag, SCM diag, SCM superdiag, SCM B);

/* Solve AX=B (matrix RHS) or Ax*=b* (vector RHS) by gaussian
   elimination with partial pivoting, where A is cyclic (periodic)
   tridiagonal. Returns X or x* (or #f on failure to solve), and an
   informational integer. FIXME: Document more thoroughly. */
SCM scm_scyclictridiagsolve (SCM subdiag, SCM diag, SCM superdiag,
                             SCM lower_left, SCM upper_right, SCM B);
SCM scm_dcyclictridiagsolve (SCM subdiag, SCM diag, SCM superdiag,
                             SCM lower_left, SCM upper_right, SCM B);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_LINEAR_SYSTEMS_H */
