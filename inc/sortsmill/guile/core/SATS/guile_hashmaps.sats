// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

//--------------------------------------------------------------------
//
// Immutable hash maps for Guile.
//

#define ATS_PACKNAME "SMCOREGUILE.guile_hashmaps"
#define ATS_EXTERN_PREFIX "_smcoreguile_guile_hashmaps__"

%{#
#include "sortsmill/guile/core/CATS/guile_hashmaps.cats"
%}

staload "sortsmill/guile/core/SATS/guile.sats"

//--------------------------------------------------------------------
//
// Maps.
//

typedef scm_t_hashmap = $extype"scm_t_hashmap"

fn scm_is_hashmap : SCM -> bool = "mac#%"
fn scm_hashmap_p : SCM -> SCM = "mac#%"
fn scm_to_scm_t_hashmap : SCM -> scm_t_hashmap = "mac#%"
fn scm_from_scm_t_hashmap : scm_t_hashmap -> SCM = "mac#%"
overload .to_scm_t_hashmap with scm_to_scm_t_hashmap
overload .to_scm with scm_from_scm_t_hashmap

// The first optional argument is a hash procedure.
// The second is an equality or equivalence procedure
// that takes the already-stored key as the second argument.
fn scm_c_make_hashmap0 : () -> scm_t_hashmap
fn scm_c_make_hashmap1 : (SCM) -> scm_t_hashmap
fn scm_c_make_hashmap2 : (SCM, SCM) -> scm_t_hashmap
overload scm_c_make_hashmap with scm_c_make_hashmap0
overload scm_c_make_hashmap with scm_c_make_hashmap1
overload scm_c_make_hashmap with scm_c_make_hashmap2

// The first optional argument is a hash procedure.
// The second is an equality or equivalence procedure
// that takes the already-stored key as the second argument.
fn scm_make_hashmap0 : () -> SCM
fn scm_make_hashmap1 : (SCM) -> SCM
fn scm_make_hashmap2 : (SCM, SCM) -> SCM
overload scm_make_hashmap with scm_make_hashmap0
overload scm_make_hashmap with scm_make_hashmap1
overload scm_make_hashmap with scm_make_hashmap2

fn scm_c_make_eq_hashmap : () -> scm_t_hashmap
fn scm_make_eq_hashmap : () -> SCM

fn scm_c_make_eqv_hashmap : () -> scm_t_hashmap
fn scm_make_eqv_hashmap : () -> SCM

(* Similar to what is in SRFI-69 and R6RS. *)
fn scm_c_hashmap_hash_function : scm_t_hashmap -> SCM
fn scm_hashmap_hash_function : SCM -> SCM
overload .hash_function with scm_c_hashmap_hash_function

(* The key-matcher is called `equivalence function' here
   for similarity to SRFI-69 and R6RS. *)
fn scm_c_hashmap_equivalence_function : scm_t_hashmap -> SCM
fn scm_hashmap_equivalence_function : SCM -> SCM
overload .equivalence_function with scm_c_hashmap_equivalence_function

fn scm_hashmap_is_null : scm_t_hashmap -> bool
fn scm_hashmap_null_p : SCM -> SCM
overload .is_null with scm_hashmap_is_null

fn scm_c_hashmap_size : scm_t_hashmap -> size_t
fn scm_hashmap_size : SCM -> SCM
overload .size with scm_c_hashmap_size

fn scm_c_hashmap_contains : (scm_t_hashmap, SCM) -> bool
fn scm_hashmap_contains_p : (SCM, SCM) -> SCM
overload .contains with scm_c_hashmap_contains

fn scm_c_hashmap_ref2 : (scm_t_hashmap, SCM) -> SCM
fn scm_c_hashmap_ref3 : (scm_t_hashmap, SCM, SCM) -> SCM
overload scm_c_hashmap_ref with scm_c_hashmap_ref2
overload scm_c_hashmap_ref with scm_c_hashmap_ref3
overload .ref with scm_c_hashmap_ref2
overload .ref with scm_c_hashmap_ref3

fn scm_hashmap_ref2 : (SCM, SCM) -> SCM
fn scm_hashmap_ref3 : (SCM, SCM, SCM) -> SCM
overload scm_hashmap_ref with scm_hashmap_ref2
overload scm_hashmap_ref with scm_hashmap_ref3

fn scm_c_hashmap_set : (scm_t_hashmap, SCM, SCM) -> scm_t_hashmap
fn scm_hashmap_set : (SCM, SCM, SCM) -> SCM
overload .set with scm_c_hashmap_set

fn scm_c_hashmap_remove : (scm_t_hashmap, SCM) -> scm_t_hashmap
fn scm_hashmap_remove : (SCM, SCM) -> SCM
overload .remove with scm_c_hashmap_remove

//--------------------------------------------------------------------
//
// Cursors.
//

typedef scm_t_hashmap_cursor = $extype"scm_t_hashmap_cursor"

fn scm_is_hashmap_cursor : SCM -> bool = "mac#%"
fn scm_hashmap_cursor_p : SCM -> SCM = "mac#%"
fn scm_to_scm_t_hashmap_cursor : SCM -> scm_t_hashmap_cursor = "mac#%"
fn scm_from_scm_t_hashmap_cursor : scm_t_hashmap_cursor -> SCM = "mac#%"
overload .to_scm_t_hashmap_cursor with scm_to_scm_t_hashmap_cursor
overload .to_scm with scm_from_scm_t_hashmap_cursor

fn scm_c_make_hashmap_cursor : scm_t_hashmap -> scm_t_hashmap_cursor
fn scm_make_hashmap_cursor : SCM -> SCM
overload .cursor with scm_c_make_hashmap_cursor

fn scm_hashmap_cursor_is_null : scm_t_hashmap_cursor -> bool
fn scm_hashmap_cursor_null_p : SCM -> SCM
overload .is_null with scm_hashmap_cursor_is_null

fn scm_c_hashmap_cursor_next : scm_t_hashmap_cursor -> scm_t_hashmap_cursor
fn scm_hashmap_cursor_next : SCM -> SCM
overload .next with scm_c_hashmap_cursor_next

fn scm_c_hashmap_cursor_entry : scm_t_hashmap_cursor -> SCM
fn scm_hashmap_cursor_entry : SCM -> SCM
overload .entry with scm_c_hashmap_cursor_entry

fn scm_c_hashmap_cursor_key : scm_t_hashmap_cursor -> SCM
fn scm_hashmap_cursor_key : SCM -> SCM
overload .key with scm_c_hashmap_cursor_key

fn scm_c_hashmap_cursor_value : scm_t_hashmap_cursor -> SCM
fn scm_hashmap_cursor_value : SCM -> SCM
overload .value with scm_c_hashmap_cursor_value

//--------------------------------------------------------------------
//
// Convenience functions.
//

fn scm_c_hashmap_to_alist : scm_t_hashmap -> SCM
fn scm_hashmap_to_alist : SCM -> SCM

fn scm_c_hashmap_set_from_alist : (scm_t_hashmap, SCM) -> scm_t_hashmap
fn scm_hashmap_set_from_alist : (SCM, SCM) -> SCM

fn scm_c_alist_to_hashmap1 : (SCM) -> scm_t_hashmap
fn scm_c_alist_to_hashmap2 : (SCM, SCM) -> scm_t_hashmap
fn scm_c_alist_to_hashmap3 : (SCM, SCM, SCM) -> scm_t_hashmap
overload scm_c_alist_to_hashmap with scm_c_alist_to_hashmap1
overload scm_c_alist_to_hashmap with scm_c_alist_to_hashmap2
overload scm_c_alist_to_hashmap with scm_c_alist_to_hashmap3

fn scm_alist_to_hashmap1 : (SCM) -> SCM
fn scm_alist_to_hashmap2 : (SCM, SCM) -> SCM
fn scm_alist_to_hashmap3 : (SCM, SCM, SCM) -> SCM
overload scm_alist_to_hashmap with scm_alist_to_hashmap1
overload scm_alist_to_hashmap with scm_alist_to_hashmap2
overload scm_alist_to_hashmap with scm_alist_to_hashmap3

fn scm_c_alist_to_eq_hashmap : SCM -> scm_t_hashmap
fn scm_alist_to_eq_hashmap : SCM -> SCM

fn scm_c_alist_to_eqv_hashmap : SCM -> scm_t_hashmap
fn scm_alist_to_eqv_hashmap : SCM -> SCM

//--------------------------------------------------------------------
//
// Support for an `equal?' extension.
//

// The third argument is an optional `equivalence function' procedure.
// If the argument is SCM_UNDEFINED, then `equal?' is used.
fn scm_c_hashmap_is_equal : (scm_t_hashmap, scm_t_hashmap, SCM) -> bool
fn scm_hashmap_is_equal : (SCM, SCM, SCM) -> bool
fn scm_hashmap_equal_p : (SCM, SCM, SCM) -> SCM

//--------------------------------------------------------------------
