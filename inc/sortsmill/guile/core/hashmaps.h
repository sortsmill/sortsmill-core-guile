/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_GUILE_CORE_HASHMAPS_H_
#define SORTSMILL_GUILE_CORE_HASHMAPS_H_

#include <stdbool.h>
#include <stdlib.h>
#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* *INDENT-OFF*                                                      */
/* ................................................................. */
/* Some stuff that users should not use directly ................... */
struct _scm_t_hashmap__;        /* Use scm_t_hashmap instead.        */
struct _scm_t_hashmap_cursor__; /* Use scm_t_hashmap_cursor instead. */
/* ................................................................. */
/* *INDENT-ON*                                                       */

/* The public interface follows. */

typedef struct _scm_t_hashmap__ *scm_t_hashmap;
typedef struct _scm_t_hashmap_cursor__ *scm_t_hashmap_cursor;

bool scm_is_hashmap (SCM obj);
SCM scm_hashmap_p (SCM obj);

scm_t_hashmap scm_to_scm_t_hashmap (SCM obj);
SCM scm_from_scm_t_hashmap (scm_t_hashmap p);

scm_t_hashmap scm_c_make_hashmap (SCM hash_proc, SCM equal_proc);
SCM scm_make_hashmap (SCM hash_proc, SCM equal_proc);

scm_t_hashmap scm_c_make_eq_hashmap (void);
SCM scm_make_eq_hashmap (void);

scm_t_hashmap scm_c_make_eqv_hashmap (void);
SCM scm_make_eqv_hashmap (void);

SCM scm_c_hashmap_hash_function (scm_t_hashmap map);
SCM scm_hashmap_hash_function (SCM map);

SCM scm_c_hashmap_equivalence_function (scm_t_hashmap map);
SCM scm_hashmap_hash_function (SCM map);

bool scm_hashmap_is_null (scm_t_hashmap map);
SCM scm_hashmap_null_p (SCM map);

size_t scm_c_hashmap_size (scm_t_hashmap map);
SCM scm_hashmap_size (SCM map);

bool scm_c_hashmap_contains (scm_t_hashmap map, SCM key);
SCM scm_hashmap_contains_p (SCM map, SCM key);

SCM scm_c_hashmap_ref (scm_t_hashmap map, SCM key, SCM default_value);
SCM scm_hashmap_ref (SCM map, SCM key, SCM default_value);

scm_t_hashmap scm_c_hashmap_set (scm_t_hashmap map, SCM key, SCM value);
SCM scm_hashmap_set (SCM map, SCM key, SCM value);

scm_t_hashmap scm_c_hashmap_remove (scm_t_hashmap map, SCM key);
SCM scm_hashmap_remove (SCM map, SCM key);

bool scm_is_hashmap_cursor (SCM obj);
SCM scm_hashmap_cursor_p (SCM obj);

scm_t_hashmap_cursor scm_to_scm_t_hashmap_cursor (SCM obj);
SCM scm_from_scm_t_hashmap_cursor (scm_t_hashmap_cursor p);

scm_t_hashmap_cursor scm_c_make_hashmap_cursor (scm_t_hashmap map);
SCM scm_make_hashmap_cursor (SCM map);

bool scm_hashmap_cursor_is_null (scm_t_hashmap_cursor cursor);
SCM scm_hashmap_cursor_null_p (SCM cursor);

scm_t_hashmap_cursor scm_c_hashmap_cursor_next (scm_t_hashmap_cursor cursor);
SCM scm_hashmap_cursor_next (SCM cursor);

SCM scm_c_hashmap_cursor_entry (scm_t_hashmap_cursor cursor);
SCM scm_hashmap_cursor_entry (SCM cursor);

SCM scm_c_hashmap_cursor_key (scm_t_hashmap_cursor cursor);
SCM scm_hashmap_cursor_key (SCM cursor);

SCM scm_c_hashmap_cursor_value (scm_t_hashmap_cursor cursor);
SCM scm_hashmap_cursor_value (SCM cursor);

SCM scm_c_hashmap_to_alist (scm_t_hashmap map);
SCM scm_hashmap_to_alist (SCM map);

scm_t_hashmap scm_c_hashmap_set_from_alist (scm_t_hashmap map, SCM alist);
SCM scm_hashmap_set_from_alist (SCM map, SCM alist);

scm_t_hashmap scm_c_alist_to_hashmap (SCM alist, SCM hash_proc, SCM equal_proc);
SCM scm_alist_to_hashmap (SCM alist, SCM hash_proc, SCM equal_proc);

scm_t_hashmap scm_c_alist_to_eq_hashmap (SCM alist);
SCM scm_alist_to_eq_hashmap (SCM alist);

scm_t_hashmap scm_c_alist_to_eqv_hashmap (SCM alist);
SCM scm_alist_to_eqv_hashmap (SCM alist);

bool scm_c_hashmap_is_equal (scm_t_hashmap x, scm_t_hashmap y, SCM equal_proc);
bool scm_hashmap_is_equal (SCM x, SCM y, SCM equal_proc);
SCM scm_hashmap_equal_p (SCM x, SCM y, SCM equal_proc);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_HASHMAPS_H_ */
