/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_COMPLEX_H
#define _SORTSMILL_GUILE_CORE_COMPLEX_H

#include <complex.h>
#include <libguile.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

inline float complex scm_to_float_complex (SCM);
inline double complex scm_to_double_complex (SCM);
inline SCM scm_from_float_complex (float complex);
inline SCM scm_from_double_complex (double complex);

inline float complex
scm_to_float_complex (SCM z)
{
  return (float) scm_c_real_part (z) + (float) scm_c_imag_part (z) * I;
}

inline double complex
scm_to_double_complex (SCM z)
{
  return scm_c_real_part (z) + scm_c_imag_part (z) * I;
}

inline SCM
scm_from_float_complex (float complex z)
{
  return scm_c_make_rectangular (crealf (z), cimagf (z));
}

inline SCM
scm_from_double_complex (double complex z)
{
  return scm_c_make_rectangular (creal (z), cimag (z));
}

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_GUILE_CORE_COMPLEX_H */
