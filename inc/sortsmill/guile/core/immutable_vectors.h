/*
 * Copyright (C) 2015 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SORTSMILL_GUILE_CORE_IMMUTABLE_VECTORS_H_
#define SORTSMILL_GUILE_CORE_IMMUTABLE_VECTORS_H_

#include <stdbool.h>
#include <stdlib.h>
#include <libguile.h>
#include <sortsmill/core.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

/* ...................................................... */
/* Some stuff that users should not use directly ........ */
extern volatile stm_dcl_indicator_t _scm_ivect_null_is_initialized__;
extern SCM _scm_ivect_null__;   /* Use SCM_IVECT_NULL instead of this. */
extern SCM _scm_u8ivect_null__; /* Use SCM_U8IVECT_NULL instead of this. */
extern SCM _scm_u16ivect_null__;        /* Use SCM_U16IVECT_NULL instead of this. */
extern SCM _scm_u32ivect_null__;        /* Use SCM_U32IVECT_NULL instead of this. */
extern SCM _scm_u64ivect_null__;        /* Use SCM_U64IVECT_NULL instead of this. */
extern SCM _scm_s8ivect_null__; /* Use SCM_S8IVECT_NULL instead of this. */
extern SCM _scm_s16ivect_null__;        /* Use SCM_S16IVECT_NULL instead of this. */
extern SCM _scm_s32ivect_null__;        /* Use SCM_S32IVECT_NULL instead of this. */
extern SCM _scm_s64ivect_null__;        /* Use SCM_S64IVECT_NULL instead of this. */
extern SCM _scm_f32ivect_null__;        /* Use SCM_F32IVECT_NULL instead of this. */
extern SCM _scm_f64ivect_null__;        /* Use SCM_F64IVECT_NULL instead of this. */
extern SCM _scm_c32ivect_null__;        /* Use SCM_C32IVECT_NULL instead of this. */
extern SCM _scm_c64ivect_null__;        /* Use SCM_C64IVECT_NULL instead of this. */
SCM _initialize_scm_ivect_null__ (void);
/* ...................................................... */

/* The public interface follows. */


SCM scm_ivect_type (SCM ivect);

#define SCM_IVECT_NULL                                                  \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_U8IVECT_NULL                                                \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_u8ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_U16IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_u16ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_U32IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_u32ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_U64IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_u64ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_S8IVECT_NULL                                                \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_s8ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_S16IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_s16ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_S32IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_s32ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_S64IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_s64ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_F32IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_f32ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_F64IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_f64ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_C32IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_c32ivect_null__) : (_initialize_scm_ivect_null__ ()))
#define SCM_C64IVECT_NULL                                               \
  (((bool) stm_dcl_load_indicator (&_scm_ivect_null_is_initialized__)) ? \
   (*(const SCM *) &_scm_c64ivect_null__) : (_initialize_scm_ivect_null__ ()))

bool scm_is_any_ivect (SCM obj);
SCM scm_any_ivect_p (SCM obj);
bool scm_is_any_ivect_null (SCM obj);
SCM scm_any_ivect_null_p (SCM obj);
size_t scm_c_any_ivect_length (SCM ivect);
SCM scm_any_ivect_length (SCM ivect);

bool scm_is_ivect (SCM obj);
SCM scm_ivect_p (SCM obj);
bool scm_is_ivect_null (SCM obj);
SCM scm_ivect_null_p (SCM obj);
size_t scm_c_ivect_length (SCM ivect);
SCM scm_ivect_length (SCM ivect);
SCM scm_c_ivect_ref (SCM ivect, size_t i);
SCM scm_ivect_ref (SCM ivect, SCM i);
SCM scm_c_ivect_set (SCM ivect, size_t i, SCM x);
SCM scm_ivect_set (SCM ivect, SCM i, SCM x);
SCM scm_ivect_push (SCM ivect, SCM obj, SCM more_objs);
SCM scm_c_ivect_pop (SCM ivect, size_t count);
SCM scm_ivect_pop (SCM ivect, SCM count);
SCM scm_c_ivect_slice_ref (SCM ivect, size_t start, size_t end);
SCM scm_ivect_slice_ref (SCM ivect, SCM start, SCM end);
SCM scm_c_ivect_slice_set (SCM ivect, size_t start, SCM source);
SCM scm_ivect_slice_set (SCM ivect, SCM start, SCM source);
SCM scm_c_ivect_reverse (SCM ivect, size_t start, size_t end);
SCM scm_ivect_reverse (SCM ivect, SCM start, SCM end);
SCM scm_ivect_concatenate (SCM ivects);
SCM scm_ivect_to_list (SCM ivect, SCM start, SCM end);
SCM scm_list_to_ivect (SCM lst);
SCM scm_ivect_to_vlst (SCM ivect, SCM start, SCM end);
SCM scm_vlst_to_ivect (SCM lst);
SCM scm_ivect_to_vector (SCM ivect, SCM start, SCM end);
SCM scm_vector_to_ivect (SCM vect);
SCM scm_ivect_map (SCM proc, SCM ivect, SCM another_ivect_or_ivects);
SCM scm_ivect_map_in_order (SCM proc, SCM ivect, SCM another_ivect_or_ivects);

#define SMCOREGUILE_TYPED_IVECT_DECLARATIONS__(T)                       \
  bool scm_is_##T##ivect (SCM obj);                                     \
  SCM scm_##T##ivect_p (SCM obj);                                       \
  bool scm_is_##T##ivect_null (SCM obj);                                \
  SCM scm_##T##ivect_null_p (SCM obj);                                  \
  size_t scm_c_##T##ivect_length (SCM ivect);                           \
  SCM scm_##T##ivect_length (SCM ivect);                                \
  SCM scm_c_##T##ivect_ref (SCM ivect, size_t i);                       \
  SCM scm_##T##ivect_ref (SCM ivect, SCM i);                            \
  SCM scm_c_##T##ivect_set (SCM ivect, size_t i, SCM x);                \
  SCM scm_##T##ivect_set (SCM ivect, SCM i, SCM x);                     \
  SCM scm_##T##ivect_push (SCM ivect, SCM obj, SCM more_objs);          \
  SCM scm_c_##T##ivect_pop (SCM ivect, size_t count);                   \
  SCM scm_##T##ivect_pop (SCM ivect, SCM count);                        \
  SCM scm_c_##T##ivect_slice_ref (SCM ivect, size_t start, size_t end); \
  SCM scm_##T##ivect_slice_ref (SCM ivect, SCM start, SCM end);         \
  SCM scm_c_##T##ivect_slice_set (SCM ivect, size_t start, SCM source); \
  SCM scm_##T##ivect_slice_set (SCM ivect, SCM start, SCM source);      \
  SCM scm_c_##T##ivect_reverse (SCM ivect, size_t start, size_t end);   \
  SCM scm_##T##ivect_reverse (SCM ivect, SCM start, SCM end);           \
  SCM scm_##T##ivect_concatenate (SCM ivects);                          \
  SCM scm_##T##ivect_to_list (SCM ivect, SCM start, SCM end);           \
  SCM scm_list_to_##T##ivect (SCM lst);                                 \
  SCM scm_##T##ivect_to_vlst (SCM ivect, SCM start, SCM end);           \
  SCM scm_vlst_to_##T##ivect (SCM lst);                                 \
  SCM scm_##T##ivect_to_vector (SCM ivect, SCM start, SCM end);         \
  SCM scm_##T##ivect_to_##T##vector (SCM ivect, SCM start, SCM end);    \
  SCM scm_vector_to_##T##ivect (SCM vect);                              \
  SCM scm_##T##vector_to_##T##ivect (SCM vect);

SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (u8);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (u16);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (u32);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (u64);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (s8);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (s16);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (s32);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (s64);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (f32);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (f64);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (c32);
SMCOREGUILE_TYPED_IVECT_DECLARATIONS__ (c64);

/* C interfaces to the data structures. */
DECLARE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (, scmivect, SCM, 5);
scmivect_t scm_to_scmivect_t (SCM);
u8ivect_t scm_to_u8ivect_t (SCM);
u16ivect_t scm_to_u16ivect_t (SCM);
u32ivect_t scm_to_u32ivect_t (SCM);
u64ivect_t scm_to_u64ivect_t (SCM);
s8ivect_t scm_to_s8ivect_t (SCM);
s16ivect_t scm_to_s16ivect_t (SCM);
s32ivect_t scm_to_s32ivect_t (SCM);
s64ivect_t scm_to_s64ivect_t (SCM);
f32ivect_t scm_to_f32ivect_t (SCM);
f64ivect_t scm_to_f64ivect_t (SCM);
c32ivect_t scm_to_c32ivect_t (SCM);
c64ivect_t scm_to_c64ivect_t (SCM);
SCM scm_from_scmivect_t (scmivect_t);
SCM scm_from_u8ivect_t (u8ivect_t);
SCM scm_from_u16ivect_t (u16ivect_t);
SCM scm_from_u32ivect_t (u32ivect_t);
SCM scm_from_u64ivect_t (u64ivect_t);
SCM scm_from_s8ivect_t (s8ivect_t);
SCM scm_from_s16ivect_t (s16ivect_t);
SCM scm_from_s32ivect_t (s32ivect_t);
SCM scm_from_s64ivect_t (s64ivect_t);
SCM scm_from_f32ivect_t (f32ivect_t);
SCM scm_from_f64ivect_t (f64ivect_t);
SCM scm_from_c32ivect_t (c32ivect_t);
SCM scm_from_c64ivect_t (c64ivect_t);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* SORTSMILL_GUILE_CORE_IMMUTABLE_VECTORS_H_ */
