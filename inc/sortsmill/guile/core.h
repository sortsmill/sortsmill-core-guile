/*
 * Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
 *
 * This file is part of Sorts Mill Core Guile.
 * 
 * Sorts Mill Core Guile is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sorts Mill Core Guile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_GUILE_CORE_H
#define _SORTSMILL_GUILE_CORE_H

/* <sortsmill/guile/core/python.h> is left out on purpose, so the
   troublesome header <Python.h> is not included if you are not using
   Python. */

#include <sortsmill/smcoreguile-pkginfo.h>

#include <libguile.h>
#include <sortsmill/core.h>
#include <sortsmill/guile/core/alists_of_groups.h>
#include <sortsmill/guile/core/arrays.h>
#include <sortsmill/guile/core/atomic_ops.h>
#include <sortsmill/guile/core/bincoef.h>
#include <sortsmill/guile/core/bivariate_polynomials.h>
#include <sortsmill/guile/core/brentroot.h>
#include <sortsmill/guile/core/complex.h>
#include <sortsmill/guile/core/delayed_evaluation.h>
#include <sortsmill/guile/core/eval.h>
#include <sortsmill/guile/core/exotic_objects.h>
#include <sortsmill/guile/core/format.h>
#include <sortsmill/guile/core/gmp.h>
#include <sortsmill/guile/core/hashmaps.h>
#include <sortsmill/guile/core/hobby.h>
#include <sortsmill/guile/core/immutable_vectors.h>
#include <sortsmill/guile/core/linear_systems.h>
#include <sortsmill/guile/core/lists.h>
#include <sortsmill/guile/core/modules.h>
#include <sortsmill/guile/core/polynomials.h>
#include <sortsmill/guile/core/popcount.h>
#include <sortsmill/guile/core/postscript.h>
#include <sortsmill/guile/core/rbmap.h>
#include <sortsmill/guile/core/rnrs_conditions.h>
#include <sortsmill/guile/core/rnrs_enums.h>
#include <sortsmill/guile/core/scm_one_time_initialization.h>
#include <sortsmill/guile/core/uniform_vectors.h>
#include <sortsmill/guile/core/vlists.h>
#include <sortsmill/guile/core/wrap.h>

#endif /* _SORTSMILL_GUILE_CORE_H */
