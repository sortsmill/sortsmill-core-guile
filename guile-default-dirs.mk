# -*- coding: utf-8; tab-width: 4 -*-
#
# Copyright (C) 2013 Khaled Hosny and Barry Schwartz
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

#-------------------------------------------------------------------------
#
# Try to compute default values for `guiledir' and `guileobjdir' from
# values that were compiled into Guile. We want to replace the Guile
# installation’s prefixes (for instance, /usr/local/...) with our own.

guile_site_dir := $(abspath $(shell $(GUILE) -c '(display (%site-dir))'))
guile_datadir := $(abspath $(shell $(GUILE) -c '(display (assq-ref %guile-build-info (quote datadir)))'))

# `guile_site_subdir' is set to the subdirectory of Guile’s datadir
# that is (%site-dir), or to an empty string if (%site-dir) is not a
# subdirectory of the datadir.
guile_site_subdir := \
	$(patsubst $(guile_datadir)/%,%,$(filter $(guile_datadir)/%,$(guile_site_dir)))

guile_site_ccache_dir := $(abspath $(shell $(GUILE) -c '(display (%site-ccache-dir))'))
guile_libdir := $(abspath $(shell $(GUILE) -c '(display (assq-ref %guile-build-info (quote libdir)))'))

# `guile_site_ccache_subdir' is set to the subdirectory of Guile’s
# libdir that is (%site-ccache-dir), or to an empty string if
# (%site-dir) is not a subdirectory of the libdir.
guile_site_ccache_subdir := \
	$(patsubst $(guile_libdir)/%,%,$(filter $(guile_libdir)/%,$(guile_site_ccache_dir)))

# Set the default `guiledir' to a subdirectory we computed or, if that
# is empty, to a fallback value.
guiledir_default := $(datarootdir)/$(or $(guile_site_subdir),guile/site/$(GUILE_EFFECTIVE_VERSION))

# Set the default `guileobjdir' to a subdirectory we computed or, if
# that is empty, to a fallback value.
guileobjdir_default := $(libdir)/$(or $(guile_site_ccache_subdir),guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache)

#-------------------------------------------------------------------------
