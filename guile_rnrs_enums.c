#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/guile/core/rnrs_enums.h>
#include <sortsmill/guile/core/wrap.h>

#define _ENUMS_MODULE "rnrs enums"

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_make_enumeration, _ENUMS_MODULE,
                           "make-enumeration");

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_enum_set_universe, _ENUMS_MODULE,
                           "enum-set-universe");

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_enum_set_indexer, _ENUMS_MODULE,
                           "enum-set-indexer");

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_enum_set_constructor, _ENUMS_MODULE,
                           "enum-set-constructor");

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_enum_set_to_list, _ENUMS_MODULE,
                           "enum-set->list");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_member_p, _ENUMS_MODULE,
                           "enum-set-member?");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_subset_p, _ENUMS_MODULE,
                           "enum-set-subset?");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_eq_p, _ENUMS_MODULE, "enum-set=?");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_union, _ENUMS_MODULE,
                           "enum-set-union");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_intersection, _ENUMS_MODULE,
                           "enum-set-intersection");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_difference, _ENUMS_MODULE,
                           "enum-set-difference");

VISIBLE C_WRAP_SCM_CALL_1 (rnrs_enum_set_complement, _ENUMS_MODULE,
                           "enum-set-complement");

VISIBLE C_WRAP_SCM_CALL_2 (rnrs_enum_set_projection, _ENUMS_MODULE,
                           "enum-set-projection");
