#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Library.
// 
// Sorts Mill Core Library is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>

#include "pats_ccomp_basics.h"

#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

VISIBLE void
atsruntime_raise (void *exn)
{
  //
  // Raise a Guile exception rather than an ATS exception.
  //
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_c_make_message_condition (_("ATS exception in " __FILE__)),
      rnrs_make_irritants_condition (SCM_EOL)));
}

VISIBLE void
atsruntime_handle_unmatchedval (char *msg0)
{
  //
  // Raise a Guile exception rather than an ATS exception.
  //
  SCM msg0_ = scm_from_utf8_string (msg0);      /* Assume msg0 is not
                                                   localized. */
  SCM msg = scm_c_utf8_sformat (_("ATS unmatched val: ~s"), scm_list_1 (msg0_));
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_make_message_condition (msg),
      rnrs_make_irritants_condition (SCM_EOL)));
}
