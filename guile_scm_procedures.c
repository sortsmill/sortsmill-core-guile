#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libguile.h>
#include <sortsmill/core.h>

#ifndef SCM_GSUBR_MAX
#error SCM_GSUBR_MAX is not defined. This module needs to be modified due to changes in Guile.
#endif

//-------------------------------------------------------------------------

static SCM missing_argument_indicator = SCM_UNDEFINED;
static SCM scm_undefined_as_pointer = SCM_UNDEFINED;
static SCM scm_to_pointer_proc = SCM_UNDEFINED;

static SCM
_scm_prepare_optional_argument_for_c (SCM arg)
{
  return (scm_is_eq (arg, missing_argument_indicator)) ?
    scm_undefined_as_pointer : (scm_call_1 (scm_to_pointer_proc, arg));
}

//-------------------------------------------------------------------------

void init_sortsmill_core_guile_scm_procedures (void);

VISIBLE void
init_sortsmill_core_guile_scm_procedures (void)
{
  // Precomputed values.
  missing_argument_indicator =
    scm_make_symbol (scm_from_utf8_string ("<missing-argument>"));
  scm_undefined_as_pointer =
    scm_call_1 (scm_c_public_ref ("system foreign", "make-pointer"),
                scm_from_uintmax ((scm_t_bits) (SCM_UNPACK (SCM_UNDEFINED))));
  scm_to_pointer_proc = scm_c_public_ref ("system foreign", "scm->pointer");

  // The variable *missing-argument* contains an uninterned symbol.
  // That symbol is used as the default value for #:optional arguments
  // to lambda*-scm-procedure.
  scm_c_define ("*missing-argument*", missing_argument_indicator);

  // The procedure prepare-optional-argument-for-c is like
  // scm->pointer, but handles *missing-argument* specially,
  // converting it to the ‘Guile pointer’ equivalent of SCM_UNDEFINED.
  scm_c_define_gsubr ("prepare-optional-argument-for-c", 1, 0, 0,
                      _scm_prepare_optional_argument_for_c);

  scm_c_define ("SCM_GSUBR_MAX", scm_from_size_t (SCM_GSUBR_MAX));
}

//-------------------------------------------------------------------------
