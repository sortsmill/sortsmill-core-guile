#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <float.h>
#include <libguile.h>

#include "miscellaneous_data.h"

static void
analyze_floating_point_constant (double constant, SCM *mant,
                                 SCM *radix, SCM *expon)
{
  // Only binary is supported, as yet, and probably always.
  const int _radix = 2;

  double _mant;
  int _expon;
  _mant = frexp (constant, &_expon);
  if (_mant == 0.5 && DBL_MIN_EXP < _expon)
    {
      _mant = 1;
      _expon -= 1;
    }

  *mant = scm_inexact_to_exact (scm_from_double (_mant));
  *radix = scm_from_int (_radix);
  *expon = scm_from_int (_expon);
}

void init_sortsmill_core_guile_machine (void);

VISIBLE void
init_sortsmill_core_guile_machine (void)
{
  scm_c_define ("SIZEOF__BOOL", scm_from_size_t (sizeof (bool)));
  scm_c_define ("SIZEOF_SHORT", scm_from_size_t (sizeof (short)));
  scm_c_define ("SIZEOF_USHORT", scm_from_size_t (sizeof (unsigned short)));
  scm_c_define ("SIZEOF_INTPTR_T", scm_from_size_t (sizeof (intptr_t)));
  scm_c_define ("SIZEOF_UINTPTR_T", scm_from_size_t (sizeof (uintptr_t)));

  SCM mant;
  SCM radix;
  SCM expon;
  analyze_floating_point_constant (FLT_EPSILON, &mant, &radix, &expon);
  scm_c_define ("FLT_EPSILON_FRACTION", mant);
  scm_c_define ("FLT_EPSILON_EXPONENT_BASE", radix);
  scm_c_define ("FLT_EPSILON_EXPONENT", expon);
  analyze_floating_point_constant (DBL_EPSILON, &mant, &radix, &expon);
  scm_c_define ("DBL_EPSILON_FRACTION", mant);
  scm_c_define ("DBL_EPSILON_EXPONENT_BASE", radix);
  scm_c_define ("DBL_EPSILON_EXPONENT", expon);

  scm_c_define ("PI_DIGITS", scm_from_latin1_string (PI_DIGITS));
}
