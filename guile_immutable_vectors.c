#include <config.h>

// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define IVECT_MODULE__ "sortsmill core immutable-vectors"

static inline size_t
szmin__ (size_t a, size_t b)
{
  return (a < b) ? a : b;
}

DEFINE_SMCORE_IMMUTABLE_VECTOR_DATATYPE (VISIBLE, scmivect, SCM, 5);

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_u8,
                             (_symbol_u8__Value =
                              scm_from_latin1_symbol ("u8")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_u16,
                             (_symbol_u16__Value =
                              scm_from_latin1_symbol ("u16")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_u32,
                             (_symbol_u32__Value =
                              scm_from_latin1_symbol ("u32")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_u64,
                             (_symbol_u64__Value =
                              scm_from_latin1_symbol ("u64")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_s8,
                             (_symbol_s8__Value =
                              scm_from_latin1_symbol ("s8")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_s16,
                             (_symbol_s16__Value =
                              scm_from_latin1_symbol ("s16")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_s32,
                             (_symbol_s32__Value =
                              scm_from_latin1_symbol ("s32")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_s64,
                             (_symbol_s64__Value =
                              scm_from_latin1_symbol ("s64")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_f32,
                             (_symbol_f32__Value =
                              scm_from_latin1_symbol ("f32")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_f64,
                             (_symbol_f64__Value =
                              scm_from_latin1_symbol ("f64")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_c32,
                             (_symbol_c32__Value =
                              scm_from_latin1_symbol ("c32")));
STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, _symbol_c64,
                             (_symbol_c64__Value =
                              scm_from_latin1_symbol ("c64")));

typedef enum
{
  _scm,
  _u8,
  _u16,
  _u32,
  _u64,
  _s8,
  _s16,
  _s32,
  _s64,
  _f32,
  _f64,
  _c32,
  _c64
} _type_enum_t;

static _type_enum_t
_type_enum (SCM type)
{
  _type_enum_t e;
  if (scm_is_eq (type, SCM_BOOL_T))
    e = _scm;
  else if (scm_is_eq (type, _symbol_u8 ()))
    e = _u8;
  else if (scm_is_eq (type, _symbol_u16 ()))
    e = _u16;
  else if (scm_is_eq (type, _symbol_u32 ()))
    e = _u32;
  else if (scm_is_eq (type, _symbol_u64 ()))
    e = _u64;
  else if (scm_is_eq (type, _symbol_s8 ()))
    e = _s8;
  else if (scm_is_eq (type, _symbol_s16 ()))
    e = _s16;
  else if (scm_is_eq (type, _symbol_s32 ()))
    e = _s32;
  else if (scm_is_eq (type, _symbol_s64 ()))
    e = _s64;
  else if (scm_is_eq (type, _symbol_f32 ()))
    e = _f32;
  else if (scm_is_eq (type, _symbol_f64 ()))
    e = _f64;
  else if (scm_is_eq (type, _symbol_c32 ()))
    e = _c32;
  else if (scm_is_eq (type, _symbol_c64 ()))
    e = _c64;
  else
    rnrs_raise_condition
      (scm_list_3
       (rnrs_make_assertion_violation (),
        rnrs_c_make_message_condition (_("unexpected value for ivect type")),
        rnrs_make_irritants_condition (scm_list_1 (type))));
  return e;
}

static void
_ivect_i_n (const char *who, size_t vlen, SCM start, SCM end,
            size_t *i_, size_t *n_)
{
  const size_t xi_ = (SCM_UNBNDP (start)) ? 0 : (scm_to_size_t (start));
  const size_t xend_ = (SCM_UNBNDP (end)) ? vlen : (scm_to_size_t (end));
  if (xend_ < xi_)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the end index must not be less than the start index")),
        rnrs_make_irritants_condition
        (scm_list_2 (scm_from_size_t (xi_), scm_from_size_t (xend_)))));
  const size_t xn_ = xend_ - xi_;

  if (vlen < xi_)
    scm_out_of_range (who, start);

  if (vlen - xi_ < xn_)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("the ivect is not long enough")),
        rnrs_make_irritants_condition (scm_list_2 (scm_from_size_t (xi_),
                                                   scm_from_size_t (xend_)))));

  *i_ = xi_;
  *n_ = xn_;
}

VISIBLE SCM guile_ivect_equal_p__ (SCM, SCM);
VISIBLE SCM guile_ivect_write__ (SCM, SCM);
void guile_init_smcoreguile_ivects (void);
static void initialize_ivect_null_variables__ (void);

//----------------------------------------------------------------------

volatile stm_dcl_indicator_t _scm_ivect_null_is_initialized__ = false;
SCM _scm_ivect_null__;          /* Use SCM_IVECT_NULL instead of this. */
SCM _scm_u8ivect_null__;        /* Use SCM_U8IVECT_NULL instead of this. */
SCM _scm_u16ivect_null__;       /* Use SCM_U16IVECT_NULL instead of this. */
SCM _scm_u32ivect_null__;       /* Use SCM_U32IVECT_NULL instead of this. */
SCM _scm_u64ivect_null__;       /* Use SCM_U64IVECT_NULL instead of this. */
SCM _scm_s8ivect_null__;        /* Use SCM_S8IVECT_NULL instead of this. */
SCM _scm_s16ivect_null__;       /* Use SCM_S16IVECT_NULL instead of this. */
SCM _scm_s32ivect_null__;       /* Use SCM_S32IVECT_NULL instead of this. */
SCM _scm_s64ivect_null__;       /* Use SCM_S64IVECT_NULL instead of this. */
SCM _scm_f32ivect_null__;       /* Use SCM_F32IVECT_NULL instead of this. */
SCM _scm_f64ivect_null__;       /* Use SCM_F64IVECT_NULL instead of this. */
SCM _scm_c32ivect_null__;       /* Use SCM_C32IVECT_NULL instead of this. */
SCM _scm_c64ivect_null__;       /* Use SCM_C64IVECT_NULL instead of this. */

// *INDENT-OFF*

STM_SCM_ONE_TIME_INITIALIZE (static, SCM, _load_the_guile_module,
                             { // Load the module by looking up an
                               // arbitrary variable in it.
                               _load_the_guile_module__Value =
                                 scm_c_private_lookup (IVECT_MODULE__,
                                                       "ivect-null");
                               stm_dcl_store_indicator
                                 (&_scm_ivect_null_is_initialized__, true); })

// *INDENT-ON*

VISIBLE SCM
_initialize_scm_ivect_null__ (void)
{
  (void) _load_the_guile_module ();
  return _scm_ivect_null__;
}

static void
expected_untyped_ivect__ (SCM obj)
{
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_c_make_message_condition (_("expected an untyped ivect")),
      rnrs_make_irritants_condition (scm_list_1 (obj))));
}

static void
_expected_typed_ivect (SCM obj, SCM type)
{
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_make_message_condition
      (scm_c_utf8_sformat (_("expected a ~sivect"), scm_list_1 (type))),
      rnrs_make_irritants_condition (scm_list_1 (obj))));
}

//----------------------------------------------------------------------

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, scm_ivect_type__,
                             scm_ivect_type____Value =
                             scm_c_utf8_make_exotic_object_type ("<ivect>"));

typedef struct
{
  void *pointer;
  SCM type;
} scm_ivect_header__;

static inline void
scm_assert_ivect__ (SCM obj)
{
  scm_assert_exotic_object_type (scm_ivect_type__ (), obj);
  const scm_ivect_header__ *header =
    scm_c_exotic_object_ref (scm_ivect_type__ (), obj);
  const SCM type = header->type;
  if (!scm_is_eq (type, SCM_BOOL_T))
    expected_untyped_ivect__ (obj);
}

static inline void
scm_assert_typed_ivect__ (SCM obj, SCM type)
{
  scm_assert_exotic_object_type (scm_ivect_type__ (), obj);
  if (!SCM_UNBNDP (type))
    {
      const scm_ivect_header__ *header =
        scm_c_exotic_object_ref (scm_ivect_type__ (), obj);
      const SCM type1 = header->type;
      if (!scm_is_eq (type1, type))
        _expected_typed_ivect (obj, type);
    }
}

static inline void
scm_assert_any_ivect__ (SCM obj)
{
  scm_assert_exotic_object_type (scm_ivect_type__ (), obj);
}

VISIBLE SCM
scm_ivect_type (SCM ivect)
{
  scm_assert_exotic_object_type (scm_ivect_type__ (), ivect);
  const scm_ivect_header__ *header =
    scm_c_exotic_object_ref (scm_ivect_type__ (), ivect);
  return header->type;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static inline void *
scm_to_any_ivect__ (SCM obj)
{
  scm_assert_any_ivect__ (obj);
  const scm_ivect_header__ *header =
    scm_c_exotic_object_ref (scm_ivect_type__ (), obj);
  return header->pointer;
}

VISIBLE scmivect_t
scm_to_scmivect_t (SCM obj)
{
  scm_assert_ivect__ (obj);
  const scm_ivect_header__ *header =
    scm_c_exotic_object_ref (scm_ivect_type__ (), obj);
  return (scmivect_t) header->pointer;
}

VISIBLE SCM
scm_from_scmivect_t (scmivect_t p)
{
  scm_ivect_header__ *header = scm_gc_malloc (sizeof (scm_ivect_header__), "");
  header->pointer = p;
  header->type = SCM_BOOL_T;
  return scm_c_make_exotic_object (scm_ivect_type__ (), header);
}

#define SCM_TO_IVECT_T__(T)                                     \
  VISIBLE T##ivect_t                                            \
  scm_to_##T##ivect_t (SCM obj)                                 \
  {                                                             \
    scm_assert_typed_ivect__ (obj, _symbol_##T ());             \
    const scm_ivect_header__ *header =                          \
      scm_c_exotic_object_ref (scm_ivect_type__ (), obj);       \
    return (T##ivect_t) header->pointer;                        \
  }

#define SCM_FROM_IVECT_T__(T)                                           \
  VISIBLE SCM                                                           \
  scm_from_##T##ivect_t (T##ivect_t p)                                  \
  {                                                                     \
    scm_ivect_header__ *header =                                        \
      scm_gc_malloc (sizeof (scm_ivect_header__), "");                  \
    header->pointer = p;                                                \
    header->type = _symbol_##T ();                                      \
    return scm_c_make_exotic_object (scm_ivect_type__ (), header);      \
  }

SCM_TO_IVECT_T__ (u8);
SCM_TO_IVECT_T__ (u16);
SCM_TO_IVECT_T__ (u32);
SCM_TO_IVECT_T__ (u64);
SCM_TO_IVECT_T__ (s8);
SCM_TO_IVECT_T__ (s16);
SCM_TO_IVECT_T__ (s32);
SCM_TO_IVECT_T__ (s64);
SCM_TO_IVECT_T__ (f32);
SCM_TO_IVECT_T__ (f64);
SCM_TO_IVECT_T__ (c32);
SCM_TO_IVECT_T__ (c64);

SCM_FROM_IVECT_T__ (u8);
SCM_FROM_IVECT_T__ (u16);
SCM_FROM_IVECT_T__ (u32);
SCM_FROM_IVECT_T__ (u64);
SCM_FROM_IVECT_T__ (s8);
SCM_FROM_IVECT_T__ (s16);
SCM_FROM_IVECT_T__ (s32);
SCM_FROM_IVECT_T__ (s64);
SCM_FROM_IVECT_T__ (f32);
SCM_FROM_IVECT_T__ (f64);
SCM_FROM_IVECT_T__ (c32);
SCM_FROM_IVECT_T__ (c64);

#define FAST_SCM_TO_IVECT_T__(T)                                \
  static inline T##ivect_t                                      \
  _fast_scm_to_##T##ivect_t (SCM obj)                           \
  {                                                             \
    const scm_ivect_header__ *header =                          \
      scm_c_exotic_object_ref (scm_ivect_type__ (), obj);       \
    return (T##ivect_t) header->pointer;                        \
  }

FAST_SCM_TO_IVECT_T__ (scm);
FAST_SCM_TO_IVECT_T__ (u8);
FAST_SCM_TO_IVECT_T__ (u16);
FAST_SCM_TO_IVECT_T__ (u32);
FAST_SCM_TO_IVECT_T__ (u64);
FAST_SCM_TO_IVECT_T__ (s8);
FAST_SCM_TO_IVECT_T__ (s16);
FAST_SCM_TO_IVECT_T__ (s32);
FAST_SCM_TO_IVECT_T__ (s64);
FAST_SCM_TO_IVECT_T__ (f32);
FAST_SCM_TO_IVECT_T__ (f64);
FAST_SCM_TO_IVECT_T__ (c32);
FAST_SCM_TO_IVECT_T__ (c64);

static inline void *
fast_scm_to_any_ivect____ (SCM obj)
{
  const scm_ivect_header__ *header =
    scm_c_exotic_object_ref (scm_ivect_type__ (), obj);
  return (void *) header->pointer;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

VISIBLE bool
scm_is_any_ivect (SCM obj)
{
  return scm_is_exotic_object (obj, scm_ivect_type__ ());
}

VISIBLE SCM
scm_any_ivect_p (SCM obj)
{
  return scm_exotic_object_p (obj, scm_ivect_type__ ());
}

VISIBLE bool
scm_is_ivect (SCM obj)
{
  return (scm_is_exotic_object (obj, scm_ivect_type__ ()) &&
          (scm_is_eq
           (SCM_BOOL_T,
            ((scm_ivect_header__ *)
             scm_c_exotic_object_ref (scm_ivect_type__ (), obj))->type)));
}

VISIBLE SCM
scm_ivect_p (SCM obj)
{
  return scm_from_bool (scm_is_ivect (obj));
}

#define IVECT_P__(T)                                                    \
  VISIBLE bool                                                          \
  scm_is_##T##ivect (SCM obj)                                           \
  {                                                                     \
    return                                                              \
      (scm_is_exotic_object (obj, scm_ivect_type__ ()) &&               \
       (scm_is_eq                                                       \
        (_symbol_##T (),                                                \
         ((scm_ivect_header__ *)                                        \
          scm_c_exotic_object_ref (scm_ivect_type__ (), obj))->type))); \
  }                                                                     \
  VISIBLE SCM                                                           \
  scm_##T##ivect_p (SCM obj)                                            \
  {                                                                     \
    return scm_from_bool (scm_is_##T##ivect (obj));                     \
  }

IVECT_P__ (u8);
IVECT_P__ (u16);
IVECT_P__ (u32);
IVECT_P__ (u64);
IVECT_P__ (s8);
IVECT_P__ (s16);
IVECT_P__ (s32);
IVECT_P__ (s64);
IVECT_P__ (f32);
IVECT_P__ (f64);
IVECT_P__ (c32);
IVECT_P__ (c64);

VISIBLE void
guile_init_smcoreguile_ivects (void)
{
  scm_c_define ("<ivect>", scm_ivect_type__ ());
  initialize_ivect_null_variables__ ();
}

//----------------------------------------------------------------------

VISIBLE bool
scm_is_any_ivect_null (SCM obj)
{
  return (scm_is_any_ivect (obj) && fast_scm_to_any_ivect____ (obj) == NULL);
}

VISIBLE SCM
scm_any_ivect_null_p (SCM obj)
{
  return scm_from_bool (scm_is_any_ivect_null (obj));
}

VISIBLE bool
scm_is_ivect_null (SCM obj)
{
  return (scm_is_ivect (obj) && _fast_scm_to_scmivect_t (obj) == NULL);
}

VISIBLE SCM
scm_ivect_null_p (SCM obj)
{
  return scm_from_bool (scm_is_ivect_null (obj));
}

#define IVECT_NULL_P__(T)                                       \
  VISIBLE bool                                                  \
  scm_is_##T##ivect_null (SCM obj)                              \
  {                                                             \
    return ((scm_is_##T##ivect (obj)                            \
             && _fast_scm_to_##T##ivect_t (obj) == NULL));      \
  }                                                             \
  VISIBLE SCM                                                   \
  scm_##T##ivect_null_p (SCM obj)                               \
  {                                                             \
    return scm_from_bool (scm_is_##T##ivect_null (obj));        \
  }

IVECT_NULL_P__ (u8);
IVECT_NULL_P__ (u16);
IVECT_NULL_P__ (u32);
IVECT_NULL_P__ (u64);
IVECT_NULL_P__ (s8);
IVECT_NULL_P__ (s16);
IVECT_NULL_P__ (s32);
IVECT_NULL_P__ (s64);
IVECT_NULL_P__ (f32);
IVECT_NULL_P__ (f64);
IVECT_NULL_P__ (c32);
IVECT_NULL_P__ (c64);

//----------------------------------------------------------------------

VISIBLE size_t
scm_c_any_ivect_length (SCM ivect)
{
  return smcore_immutable_vector_length (scm_to_any_ivect__ (ivect));
}

VISIBLE SCM
scm_any_ivect_length (SCM ivect)
{
  return scm_from_size_t (scm_c_any_ivect_length (ivect));
}

VISIBLE size_t
scm_c_ivect_length (SCM ivect)
{
  return scmivect_length (scm_to_scmivect_t (ivect));
}

VISIBLE SCM
scm_ivect_length (SCM ivect)
{
  return scm_from_size_t (scm_c_ivect_length (ivect));
}

#define IVECT_LENGTH__(T)                                       \
  VISIBLE size_t                                                \
  scm_c_##T##ivect_length (SCM ivect)                           \
  {                                                             \
    return T##ivect_length (scm_to_##T##ivect_t (ivect));       \
  }                                                             \
  VISIBLE SCM                                                   \
  scm_##T##ivect_length (SCM ivect)                             \
  {                                                             \
    return scm_from_size_t (scm_c_##T##ivect_length (ivect));   \
  }

IVECT_LENGTH__ (u8);
IVECT_LENGTH__ (u16);
IVECT_LENGTH__ (u32);
IVECT_LENGTH__ (u64);
IVECT_LENGTH__ (s8);
IVECT_LENGTH__ (s16);
IVECT_LENGTH__ (s32);
IVECT_LENGTH__ (s64);
IVECT_LENGTH__ (f32);
IVECT_LENGTH__ (f64);
IVECT_LENGTH__ (c32);
IVECT_LENGTH__ (c64);

//----------------------------------------------------------------------

VISIBLE SCM
scm_c_ivect_ref (SCM ivect, size_t i)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  if (scmivect_length (v) <= i)
    scm_out_of_range ("ivect-ref", scm_from_size_t (i));
  return scmivect_ref (v, i);
}

VISIBLE SCM
scm_ivect_ref (SCM ivect, SCM i)
{
  return scm_c_ivect_ref (ivect, scm_to_size_t (i));
}

#define IVECT_REF__(T, CONVERT)                                 \
  VISIBLE SCM                                                   \
  scm_c_##T##ivect_ref (SCM ivect, size_t i)                    \
  {                                                             \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    if (T##ivect_length (v) <= i)                               \
      scm_out_of_range (#T "ivect-ref", scm_from_size_t (i));   \
    return CONVERT (T##ivect_ref (v, i));                       \
  }                                                             \
  VISIBLE SCM                                                   \
  scm_##T##ivect_ref (SCM ivect, SCM i)                         \
  {                                                             \
    return scm_c_##T##ivect_ref (ivect, scm_to_size_t (i));     \
  }

IVECT_REF__ (u8, scm_from_uint8);
IVECT_REF__ (u16, scm_from_uint16);
IVECT_REF__ (u32, scm_from_uint32);
IVECT_REF__ (u64, scm_from_uint64);
IVECT_REF__ (s8, scm_from_int8);
IVECT_REF__ (s16, scm_from_int16);
IVECT_REF__ (s32, scm_from_int32);
IVECT_REF__ (s64, scm_from_int64);
IVECT_REF__ (f32, scm_from_double);
IVECT_REF__ (f64, scm_from_double);
IVECT_REF__ (c32, scm_from_float_complex);
IVECT_REF__ (c64, scm_from_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_c_ivect_set (SCM ivect, size_t i, SCM x)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  if (scmivect_length (v) <= i)
    scm_out_of_range ("ivect-set", scm_from_size_t (i));
  return scm_from_scmivect_t (scmivect_set (v, i, x));
}

VISIBLE SCM
scm_ivect_set (SCM ivect, SCM i, SCM x)
{
  return scm_c_ivect_set (ivect, scm_to_size_t (i), x);
}

#define IVECT_SET__(T, CONVERT)                                 \
  VISIBLE SCM                                                   \
  scm_c_##T##ivect_set (SCM ivect, size_t i, SCM x)             \
  {                                                             \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    if (T##ivect_length (v) <= i)                               \
      scm_out_of_range (#T "ivect-set", scm_from_size_t (i));   \
    return (scm_from_##T##ivect_t                               \
            (T##ivect_set (v, i, CONVERT (x))));                \
  }                                                             \
  VISIBLE SCM                                                   \
  scm_##T##ivect_set (SCM ivect, SCM i, SCM x)                  \
  {                                                             \
    return scm_c_##T##ivect_set (ivect, scm_to_size_t (i), x);  \
  }

IVECT_SET__ (u8, scm_to_uint8);
IVECT_SET__ (u16, scm_to_uint16);
IVECT_SET__ (u32, scm_to_uint32);
IVECT_SET__ (u64, scm_to_uint64);
IVECT_SET__ (s8, scm_to_int8);
IVECT_SET__ (s16, scm_to_int16);
IVECT_SET__ (s32, scm_to_int32);
IVECT_SET__ (s64, scm_to_int64);
IVECT_SET__ (f32, scm_to_double);
IVECT_SET__ (f64, scm_to_double);
IVECT_SET__ (c32, scm_to_float_complex);
IVECT_SET__ (c64, scm_to_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_ivect_push (SCM ivect, SCM obj, SCM more_objs)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  if (!SCM_UNBNDP (obj))
    {
      if (SCM_UNBNDP (more_objs))
        v = scmivect_push (v, obj);
      else
        {
          const size_t n = scm_to_size_t (scm_length (more_objs)) + 1;
          SCM *a = scm_gc_malloc (n * sizeof (SCM), "");
          a[0] = obj;
          for (size_t i = 1; i < n; i++)
            {
              a[i] = SCM_CAR (more_objs);
              more_objs = SCM_CDR (more_objs);
            }
          v = scmivect_pushes (v, n, a);
        }
    }
  return scm_from_scmivect_t (v);
}

#define IVECT_PUSH__(T, ENTRY_T, CONVERT)                               \
  VISIBLE SCM                                                           \
  scm_##T##ivect_push (SCM ivect, SCM obj, SCM more_objs)               \
  {                                                                     \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    if (!SCM_UNBNDP (obj))                                              \
      {                                                                 \
        if (SCM_UNBNDP (more_objs))                                     \
          v = T##ivect_push (v, CONVERT (obj));                         \
        else                                                            \
          {                                                             \
            const size_t n =                                            \
              scm_to_size_t (scm_length (more_objs)) + 1;               \
            ENTRY_T *a = scm_gc_malloc (n * sizeof (ENTRY_T), "");      \
            a[0] = CONVERT (obj);                                       \
            for (size_t i = 1; i < n; i++)                              \
              {                                                         \
                a[i] = CONVERT (SCM_CAR (more_objs));                   \
                more_objs = SCM_CDR (more_objs);                        \
              }                                                         \
            v = T##ivect_pushes (v, n, a);                              \
          }                                                             \
      }                                                                 \
    return scm_from_##T##ivect_t (v);                                   \
  }

IVECT_PUSH__ (u8, uint8_t, scm_to_uint8);
IVECT_PUSH__ (u16, uint16_t, scm_to_uint16);
IVECT_PUSH__ (u32, uint32_t, scm_to_uint32);
IVECT_PUSH__ (u64, uint64_t, scm_to_uint64);
IVECT_PUSH__ (s8, int8_t, scm_to_int8);
IVECT_PUSH__ (s16, int16_t, scm_to_int16);
IVECT_PUSH__ (s32, int32_t, scm_to_int32);
IVECT_PUSH__ (s64, int64_t, scm_to_int64);
IVECT_PUSH__ (f32, float, scm_to_double);
IVECT_PUSH__ (f64, double, scm_to_double);
IVECT_PUSH__ (c32, float complex, scm_to_float_complex);
IVECT_PUSH__ (c64, double complex, scm_to_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_c_ivect_pop (SCM ivect, size_t count)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  if (scmivect_length (v) < count)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("ivect-pop"),
        rnrs_c_make_message_condition
        (_("an attempt to pop more entries than there are")),
        rnrs_make_irritants_condition
        (scm_list_2 (ivect, scm_from_size_t (count)))));
  switch (count)
    {
    case 0:
      // Do nothing.
      break;
    case 1:
      v = scmivect_pop (v);
      break;
    default:
      v = scmivect_pops (v, count);
      break;
    }
  return scm_from_scmivect_t (v);
}

VISIBLE SCM
scm_ivect_pop (SCM ivect, SCM count)
{
  const size_t n = (SCM_UNBNDP (count)) ? 1 : (scm_to_size_t (count));
  return scm_c_ivect_pop (ivect, n);
}

#define IVECT_POP__(T)                                          \
  VISIBLE SCM                                                   \
  scm_c_##T##ivect_pop (SCM ivect, size_t count)                \
  {                                                             \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    if (T##ivect_length (v) < count)                            \
      rnrs_raise_condition                                      \
        (scm_list_4                                             \
         (rnrs_make_assertion_violation (),                     \
          rnrs_c_make_who_condition (#T "ivect-pop"),           \
          rnrs_c_make_message_condition                         \
          (_("an attempt to pop more entries than there are")), \
          rnrs_make_irritants_condition                         \
          (scm_list_2 (ivect, scm_from_size_t (count)))));      \
    switch (count)                                              \
      {                                                         \
      case 0:                                                   \
        /* Do nothing. */                                       \
        break;                                                  \
      case 1:                                                   \
        v = T##ivect_pop (v);                                   \
        break;                                                  \
      default:                                                  \
        v = T##ivect_pops (v, count);                           \
        break;                                                  \
      }                                                         \
    return scm_from_##T##ivect_t (v);                           \
  }                                                             \
  VISIBLE SCM                                                   \
  scm_##T##ivect_pop (SCM ivect, SCM count)                     \
  {                                                             \
    const size_t n =                                            \
      (SCM_UNBNDP (count)) ? 1 : (scm_to_size_t (count));       \
    return scm_c_##T##ivect_pop (ivect, n);                     \
  }

IVECT_POP__ (u8);
IVECT_POP__ (u16);
IVECT_POP__ (u32);
IVECT_POP__ (u64);
IVECT_POP__ (s8);
IVECT_POP__ (s16);
IVECT_POP__ (s32);
IVECT_POP__ (s64);
IVECT_POP__ (f32);
IVECT_POP__ (f64);
IVECT_POP__ (c32);
IVECT_POP__ (c64);

//----------------------------------------------------------------------

static SCM
_ivect_slice_ref (scmivect_t v, size_t start, size_t end)
{
  const char *who = "ivect-slice-ref";

  const size_t length = scmivect_length (v);

  if (length < end)
    scm_out_of_range (who, scm_from_size_t (end));
  if (end < start)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the end index must not be less than the start index")),
        rnrs_make_irritants_condition
        (scm_list_2 (scm_from_size_t (start), scm_from_size_t (end)))));

  return scm_from_scmivect_t (scmivect_slice (v, start, end));
}

VISIBLE SCM
scm_c_ivect_slice_ref (SCM ivect, size_t start, size_t end)
{
  return _ivect_slice_ref (scm_to_scmivect_t (ivect), start, end);
}

VISIBLE SCM
scm_ivect_slice_ref (SCM ivect, SCM start, SCM end)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t length = scmivect_length (v);

  const size_t start_ = scm_to_size_t (start);
  const size_t end_ = (SCM_UNBNDP (end)) ? length : (scm_to_size_t (end));

  return _ivect_slice_ref (v, start_, end_);
}

#define IVECT_SLICE_REF__(T)                                            \
                                                                        \
  static SCM                                                            \
  _##T##ivect_slice_ref (T##ivect_t v, size_t start, size_t end)        \
  {                                                                     \
   const char *who = #T "ivect-slice-ref";                              \
                                                                        \
   const size_t length = T##ivect_length (v);                           \
                                                                        \
   if (length < end)                                                    \
     scm_out_of_range (who, scm_from_size_t (end));                     \
   if (end < start)                                                     \
     rnrs_raise_condition                                               \
       (scm_list_4                                                      \
        (rnrs_make_assertion_violation (),                              \
           rnrs_c_make_who_condition (who),                             \
           rnrs_c_make_message_condition                                \
           (_("the end index must not be less than the start index")),  \
           rnrs_make_irritants_condition                                \
           (scm_list_2 (scm_from_size_t (start),                        \
                        scm_from_size_t (end)))));                      \
                                                                        \
   return scm_from_##T##ivect_t (T##ivect_slice (v, start, end));       \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_c_##T##ivect_slice_ref (SCM ivect, size_t start, size_t end)      \
  {                                                                     \
    return                                                              \
      _##T##ivect_slice_ref (scm_to_##T##ivect_t (ivect), start, end);  \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##T##ivect_slice_ref (SCM ivect, SCM start, SCM end)              \
  {                                                                     \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t length = T##ivect_length (v);                          \
                                                                        \
    const size_t start_ = scm_to_size_t (start);                        \
    const size_t end_ = (SCM_UNBNDP (end)) ?                            \
      length : (scm_to_size_t (end));                                   \
                                                                        \
    return _##T##ivect_slice_ref (v, start_, end_);                     \
  }

IVECT_SLICE_REF__ (u8);
IVECT_SLICE_REF__ (u16);
IVECT_SLICE_REF__ (u32);
IVECT_SLICE_REF__ (u64);
IVECT_SLICE_REF__ (s8);
IVECT_SLICE_REF__ (s16);
IVECT_SLICE_REF__ (s32);
IVECT_SLICE_REF__ (s64);
IVECT_SLICE_REF__ (f32);
IVECT_SLICE_REF__ (f64);
IVECT_SLICE_REF__ (c32);
IVECT_SLICE_REF__ (c64);

//----------------------------------------------------------------------

static void
_check_slice_set_lengths (size_t start, size_t u_length, size_t v_length,
                          SCM ivect, SCM source)
{
  const char *who = "ivect-slice-set";
  if (v_length < start)
    scm_out_of_range (who, scm_from_size_t (start));
  if (v_length - start < u_length)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (), rnrs_c_make_who_condition (who),
        /* FIXME: Come up with a better message. */
        rnrs_c_make_message_condition
        (_("there are not enough entries for the new values")),
        rnrs_make_irritants_condition
        (scm_list_3 (ivect, scm_from_size_t (start), source))));
}

static SCM
_ivect_slice_set_from_ivect (SCM ivect, size_t start, SCM source)
{

  scmivect_t u = scm_to_scmivect_t (source);
  scmivect_t v = scm_to_scmivect_t (ivect);

  const size_t u_length = scmivect_length (u);
  const size_t v_length = scmivect_length (v);
  _check_slice_set_lengths (start, u_length, v_length, ivect, source);

  const size_t chunk_size = 256;
  SCM chunk[chunk_size];

  size_t i = 0;
  while (i != u_length)
    {
      const size_t j =
        (u_length - i < chunk_size) ? u_length : (i + chunk_size);
      scmivect_refs (u, i, j - i, chunk);
      v = scmivect_sets (v, start + i, j - i, chunk);
      i = j;
    }
  return scm_from_scmivect_t (v);
}

static SCM
_ivect_slice_set_from_vector (SCM ivect, size_t start, SCM source)
{
  scm_t_array_handle handle;
  size_t u_length;
  ssize_t u_stride;
  const SCM *u = scm_vector_elements (source, &handle, &u_length, &u_stride);

  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t v_length = scmivect_length (v);
  _check_slice_set_lengths (start, u_length, v_length, ivect, source);

  const size_t chunk_size = 256;
  SCM chunk[chunk_size];

  size_t i = 0;
  while (i != u_length)
    {
      const size_t j =
        (u_length - i < chunk_size) ? u_length : (i + chunk_size);
      for (ssize_t k = i; k < j; k++)
        chunk[k - i] = u[k * u_stride];
      v = scmivect_sets (v, start + i, j - i, chunk);
      i = j;
    }

  scm_array_handle_release (&handle);

  return scm_from_scmivect_t (v);
}

static SCM
_ivect_slice_set_from_list (SCM ivect, size_t start, SCM source)
{
  SCM u = source;
  scmivect_t v = scm_to_scmivect_t (ivect);

  const size_t u_length = scm_to_size_t (scm_length (u));
  const size_t v_length = scmivect_length (v);
  _check_slice_set_lengths (start, u_length, v_length, ivect, source);

  const size_t chunk_size = 256;
  SCM chunk[chunk_size];

  size_t i = 0;
  while (i != u_length)
    {
      const size_t j =
        (u_length - i < chunk_size) ? u_length : (i + chunk_size);
      for (size_t k = 0; k < j - i; k++)
        {
          chunk[k] = SCM_CAR (u);
          u = SCM_CDR (u);
        }
      v = scmivect_sets (v, start + i, j - i, chunk);
      i = j;
    }
  return scm_from_scmivect_t (v);
}

VISIBLE SCM
scm_c_ivect_slice_set (SCM ivect, size_t start, SCM source)
{
  SCM result;
  if (scm_is_ivect (source))
    result = _ivect_slice_set_from_ivect (ivect, start, source);
  else if (scm_is_vector (source))
    result = _ivect_slice_set_from_vector (ivect, start, source);
  else if (scm_is_true (scm_list_p (source)))
    result = _ivect_slice_set_from_list (ivect, start, source);
  else
    {
      SCM_ASSERT_TYPE (false, source, SCM_ARG3, "ivect-slice-set",
                       "ivect, vector, or proper list");
      result = SCM_UNDEFINED;
    }
  return result;
}

VISIBLE SCM
scm_ivect_slice_set (SCM ivect, SCM start, SCM source)
{
  return scm_c_ivect_slice_set (ivect, scm_to_size_t (start), source);
}

#define IVECT_SLICE_SET_COMMON__(T, ENTRY_T, CONVERT)                   \
                                                                        \
  static SCM                                                            \
  _##T##ivect_slice_set_from_##T##ivect (SCM ivect, size_t start,       \
                                         SCM source)                    \
  {                                                                     \
                                                                        \
    T##ivect_t u = scm_to_##T##ivect_t (source);                        \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
                                                                        \
    const size_t u_length = T##ivect_length (u);                        \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    const size_t chunk_size = 256;                                      \
    ENTRY_T chunk[chunk_size];                                          \
                                                                        \
    size_t i = 0;                                                       \
    while (i != u_length)                                               \
      {                                                                 \
        const size_t j =                                                \
          (u_length - i < chunk_size) ? u_length : (i + chunk_size);    \
        T##ivect_refs (u, i, j - i, chunk);                             \
        v = T##ivect_sets (v, start + i, j - i, chunk);                 \
        i = j;                                                          \
      }                                                                 \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
                                                                        \
  static SCM                                                            \
  _##T##ivect_slice_set_from_ivect (SCM ivect, size_t start,            \
                                    SCM source)                         \
  {                                                                     \
                                                                        \
    scmivect_t u = scm_to_scmivect_t (source);                          \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
                                                                        \
    const size_t u_length = scmivect_length (u);                        \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    const size_t chunk_size = 256;                                      \
    SCM scm_chunk[chunk_size];                                          \
    ENTRY_T chunk[chunk_size];                                          \
                                                                        \
    size_t i = 0;                                                       \
    while (i != u_length)                                               \
      {                                                                 \
        const size_t j =                                                \
          (u_length - i < chunk_size) ? u_length : (i + chunk_size);    \
        scmivect_refs (u, i, j - i, scm_chunk);                         \
        for (size_t k = 0; k < chunk_size; k++)                         \
          chunk[k] = CONVERT (scm_chunk[k]);                            \
        v = T##ivect_sets (v, start + i, j - i, chunk);                 \
        i = j;                                                          \
      }                                                                 \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
                                                                        \
  static SCM                                                            \
  _##T##ivect_slice_set_from_vector (SCM ivect, size_t start,           \
                                     SCM source)                        \
  {                                                                     \
    scm_t_array_handle handle;                                          \
    size_t u_length;                                                    \
    ssize_t u_stride;                                                   \
    const SCM *u = scm_vector_elements (source, &handle, &u_length,     \
                                        &u_stride);                     \
                                                                        \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    const size_t chunk_size = 256;                                      \
    ENTRY_T chunk[chunk_size];                                          \
                                                                        \
    size_t i = 0;                                                       \
    while (i != u_length)                                               \
      {                                                                 \
        const size_t j =                                                \
          (u_length - i < chunk_size) ? u_length : (i + chunk_size);    \
        for (ssize_t k = i; k < j; k++)                                 \
          chunk[k - i] = CONVERT (u[k * u_stride]);                     \
        v = T##ivect_sets (v, start + i, j - i, chunk);                 \
        i = j;                                                          \
      }                                                                 \
                                                                        \
    scm_array_handle_release (&handle);                                 \
                                                                        \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
                                                                        \
  static SCM                                                            \
  _##T##ivect_slice_set_from_list (SCM ivect, size_t start, SCM source) \
  {                                                                     \
    SCM u = source;                                                     \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
                                                                        \
    const size_t u_length = scm_to_size_t (scm_length (u));             \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    const size_t chunk_size = 256;                                      \
    ENTRY_T chunk[chunk_size];                                          \
                                                                        \
    size_t i = 0;                                                       \
    while (i != u_length)                                               \
      {                                                                 \
        const size_t j =                                                \
          (u_length - i < chunk_size) ? u_length : (i + chunk_size);    \
        for (size_t k = 0; k < j - i; k++)                              \
          {                                                             \
            chunk[k] = CONVERT (SCM_CAR (u));                           \
            u = SCM_CDR (u);                                            \
          }                                                             \
        v = T##ivect_sets (v, start + i, j - i, chunk);                 \
        i = j;                                                          \
      }                                                                 \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_c_##T##ivect_slice_set (SCM ivect, size_t start, SCM source)      \
  {                                                                     \
    SCM result;                                                         \
    if (scm_is_##T##ivect (source))                                     \
      result =                                                          \
        _##T##ivect_slice_set_from_##T##ivect (ivect, start, source);   \
    else if (scm_is_ivect (source))                                     \
      result = _##T##ivect_slice_set_from_ivect (ivect, start, source); \
    else if (scm_is_true (scm_##T##vector_p (source)))                  \
      result =                                                          \
        _##T##ivect_slice_set_from_##T##vector (ivect, start, source);  \
    else if (scm_is_vector (source))                                    \
      result =                                                          \
        _##T##ivect_slice_set_from_vector (ivect, start, source);       \
    else if (scm_is_true (scm_list_p (source)))                         \
      result = _##T##ivect_slice_set_from_list (ivect, start, source);  \
    else                                                                \
      {                                                                 \
        SCM_ASSERT (false, source, SCM_ARG3, #T "ivect-slice-set");     \
        result = SCM_UNDEFINED;                                         \
      }                                                                 \
    return result;                                                      \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##T##ivect_slice_set (SCM ivect, SCM start, SCM source)           \
  {                                                                     \
    return scm_c_##T##ivect_slice_set (ivect, scm_to_size_t (start),    \
                                       source);                         \
  }

#define IVECT_SLICE_SET__(T, ENTRY_T, CONVERT)                          \
  static SCM                                                            \
  _##T##ivect_slice_set_from_##T##vector (SCM ivect, size_t start,      \
                                          SCM source)                   \
  {                                                                     \
    scm_t_array_handle handle;                                          \
    size_t u_length;                                                    \
    ssize_t u_stride;                                                   \
    const ENTRY_T *u =                                                  \
      scm_##T##vector_elements (source, &handle, &u_length, &u_stride); \
                                                                        \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    if (u_stride == 1)                                                  \
      v = T##ivect_sets (v, start, u_length, u);                        \
    else                                                                \
      {                                                                 \
        const size_t chunk_size = 256;                                  \
        ENTRY_T chunk[chunk_size];                                      \
                                                                        \
        size_t i = 0;                                                   \
        while (i != u_length)                                           \
          {                                                             \
            const size_t j = (u_length - i < chunk_size) ?              \
              u_length : (i + chunk_size);                              \
            for (ssize_t k = i; k < j; k++)                             \
              chunk[k - i] = u[k * u_stride];                           \
            v = T##ivect_sets (v, start + i, j - i, chunk);             \
            i = j;                                                      \
          }                                                             \
      }                                                                 \
                                                                        \
    scm_array_handle_release (&handle);                                 \
                                                                        \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
  IVECT_SLICE_SET_COMMON__ (T, ENTRY_T, CONVERT)

IVECT_SLICE_SET__ (u8, uint8_t, scm_to_uint8);
IVECT_SLICE_SET__ (u16, uint16_t, scm_to_uint16);
IVECT_SLICE_SET__ (u32, uint32_t, scm_to_uint32);
IVECT_SLICE_SET__ (u64, uint64_t, scm_to_uint64);
IVECT_SLICE_SET__ (s8, int8_t, scm_to_int8);
IVECT_SLICE_SET__ (s16, int16_t, scm_to_int16);
IVECT_SLICE_SET__ (s32, int32_t, scm_to_int32);
IVECT_SLICE_SET__ (s64, int64_t, scm_to_int64);
IVECT_SLICE_SET__ (f32, float, scm_to_double);
IVECT_SLICE_SET__ (f64, double, scm_to_double);

#define COMPLEX_IVECT_SLICE_SET__(T, FLOAT, CONVERT)                    \
  static SCM                                                            \
  _##T##ivect_slice_set_from_##T##vector (SCM ivect, size_t start,      \
                                          SCM source)                   \
  {                                                                     \
    scm_t_array_handle handle;                                          \
    size_t u_length;                                                    \
    ssize_t u_stride;                                                   \
    const FLOAT *u =                                                    \
      scm_##T##vector_elements (source, &handle, &u_length, &u_stride); \
                                                                        \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t v_length = T##ivect_length (v);                        \
    _check_slice_set_lengths (start, u_length, v_length, ivect,         \
                              source);                                  \
                                                                        \
    const size_t chunk_size = 256;                                      \
    FLOAT complex chunk[chunk_size];                                    \
                                                                        \
    size_t i = 0;                                                       \
    while (i != u_length)                                               \
      {                                                                 \
        const size_t j =                                                \
          (u_length - i < chunk_size) ? u_length : (i + chunk_size);    \
        for (ssize_t k = i; k < j; k++)                                 \
          {                                                             \
            const ssize_t m = 2 * k * u_stride;                         \
            chunk[k - i] = u[m] + u[m + u_stride] * I;                  \
          }                                                             \
        v = T##ivect_sets (v, start + i, j - i, chunk);                 \
        i = j;                                                          \
      }                                                                 \
                                                                        \
    scm_array_handle_release (&handle);                                 \
                                                                        \
    return scm_from_##T##ivect_t (v);                                   \
  }                                                                     \
  IVECT_SLICE_SET_COMMON__ (T, FLOAT complex, CONVERT)

COMPLEX_IVECT_SLICE_SET__ (c32, float, scm_to_float_complex);
COMPLEX_IVECT_SLICE_SET__ (c64, double, scm_to_double_complex);

//----------------------------------------------------------------------

static SCM
_ivect_reverse (scmivect_t v, size_t start, size_t end)
{
  const char *who = "ivect-reverse";

  const size_t length = scmivect_length (v);

  if (length < end)
    scm_out_of_range (who, scm_from_size_t (end));
  if (end < start)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the end index must not be less than the start index")),
        rnrs_make_irritants_condition
        (scm_list_2 (scm_from_size_t (start), scm_from_size_t (end)))));

  const size_t buffer_size = 128;
  SCM buffer1[buffer_size];
  SCM buffer2[buffer_size];

  const size_t n = end - start;
  const size_t q = n / buffer_size;
  const size_t r = n % buffer_size;

  scmivect_t w = NULL;

  if (r != 0)
    {
      scmivect_refs (v, end - r, r, buffer1);
      for (size_t k = 0; k < r; k++)
        buffer2[r - 1 - k] = buffer1[k];
      w = scmivect_pushes (w, r, buffer2);
    }
  for (size_t i1 = q; 0 < i1; i1--)
    {
      const size_t i = i1 - 1;
      scmivect_refs (v, start + i * buffer_size, buffer_size, buffer1);
      for (size_t k = 0; k < buffer_size; k++)
        buffer2[buffer_size - 1 - k] = buffer1[k];
      w = scmivect_pushes (w, buffer_size, buffer2);
    }

  return scm_from_scmivect_t (w);
}

VISIBLE SCM
scm_c_ivect_reverse (SCM ivect, size_t start, size_t end)
{
  return _ivect_reverse (scm_to_scmivect_t (ivect), start, end);
}

VISIBLE SCM
scm_ivect_reverse (SCM ivect, SCM start, SCM end)
{
  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t length = scmivect_length (v);

  const size_t start_ =
    (SCM_UNBNDP (start)) ? ((size_t) 0) : (scm_to_size_t (start));
  const size_t end_ = (SCM_UNBNDP (end)) ? length : (scm_to_size_t (end));

  return _ivect_reverse (v, start_, end_);
}

#define IVECT_REVERSE__(T, ENTRY_T)                                     \
                                                                        \
  static SCM                                                            \
  _##T##ivect_reverse (T##ivect_t v, size_t start, size_t end)          \
  {                                                                     \
    const char *who = #T "ivect-reverse";                               \
                                                                        \
    const size_t length = T##ivect_length (v);                          \
                                                                        \
    if (length < end)                                                   \
      scm_out_of_range (who, scm_from_size_t (end));                    \
    if (end < start)                                                    \
      rnrs_raise_condition                                              \
        (scm_list_4                                                     \
         (rnrs_make_assertion_violation (),                             \
          rnrs_c_make_who_condition (who),                              \
          rnrs_c_make_message_condition                                 \
          (_("the end index must not be less than the start index")),   \
          rnrs_make_irritants_condition                                 \
          (scm_list_2 (scm_from_size_t (start),                         \
                       scm_from_size_t (end)))));                       \
                                                                        \
                                                                        \
    const size_t buffer_size = 128;                                     \
    ENTRY_T buffer1[buffer_size];                                       \
    ENTRY_T buffer2[buffer_size];                                       \
                                                                        \
    const size_t n = end - start;                                       \
    const size_t q = n / buffer_size;                                   \
    const size_t r = n % buffer_size;                                   \
                                                                        \
    T##ivect_t w = NULL;                                                \
                                                                        \
    if (r != 0)                                                         \
      {                                                                 \
        T##ivect_refs (v, end - r, r, buffer1);                         \
        for (size_t k = 0; k < r; k++)                                  \
          buffer2[r - 1 - k] = buffer1[k];                              \
        w = T##ivect_pushes (w, r, buffer2);                            \
      }                                                                 \
    for (size_t i1 = q; 0 < i1; i1--)                                   \
      {                                                                 \
        const size_t i = i1 - 1;                                        \
        T##ivect_refs (v, start + i * buffer_size, buffer_size,         \
                       buffer1);                                        \
        for (size_t k = 0; k < buffer_size; k++)                        \
          buffer2[buffer_size - 1 - k] = buffer1[k];                    \
        w = T##ivect_pushes (w, buffer_size, buffer2);                  \
      }                                                                 \
                                                                        \
    return scm_from_##T##ivect_t (w);                                   \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_c_##T##ivect_reverse (SCM ivect, size_t start, size_t end)        \
  {                                                                     \
    return                                                              \
      _##T##ivect_reverse (scm_to_##T##ivect_t (ivect), start, end);    \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##T##ivect_reverse (SCM ivect, SCM start, SCM end)                \
  {                                                                     \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t length = T##ivect_length (v);                          \
                                                                        \
    const size_t start_ = (SCM_UNBNDP (start)) ?                        \
      ((size_t) 0) : (scm_to_size_t (start));                           \
    const size_t end_ = (SCM_UNBNDP (end)) ?                            \
      length : (scm_to_size_t (end));                                   \
                                                                        \
    return _##T##ivect_reverse (v, start_, end_);                       \
  }

IVECT_REVERSE__ (u8, uint8_t);
IVECT_REVERSE__ (u16, uint16_t);
IVECT_REVERSE__ (u32, uint32_t);
IVECT_REVERSE__ (u64, uint64_t);
IVECT_REVERSE__ (s8, int8_t);
IVECT_REVERSE__ (s16, int16_t);
IVECT_REVERSE__ (s32, int32_t);
IVECT_REVERSE__ (s64, int64_t);
IVECT_REVERSE__ (f32, float);
IVECT_REVERSE__ (f64, double);
IVECT_REVERSE__ (c32, float complex);
IVECT_REVERSE__ (c64, double complex);

//----------------------------------------------------------------------

static SCM
_ivect_concatenate_from_ivect (SCM ivects)
{
  SCM concat;
  scmivect_t ivects_ = scm_to_scmivect_t (ivects);
  if (ivects_ == NULL)
    concat = SCM_IVECT_NULL;
  else
    {
      scmivect_t v = scm_to_scmivect_t (scmivect_ref (ivects_, 0));
      const size_t n = scmivect_length (ivects_);
      for (size_t i = 1; i < n; i++)
        v = scmivect_append (v, (scm_to_scmivect_t
                                 (scmivect_ref (ivects_, i))));
      concat = scm_from_scmivect_t (v);
    }
  return concat;
}

static SCM
_ivect_concatenate_from_list (SCM ivects)
{
  SCM concat;
  if (scm_is_null (ivects))
    concat = SCM_IVECT_NULL;
  else
    {
      scmivect_t v = scm_to_scmivect_t (SCM_CAR (ivects));
      for (SCM p = SCM_CDR (ivects); !scm_is_null (p); p = SCM_CDR (p))
        v = scmivect_append (v, scm_to_scmivect_t (SCM_CAR (p)));
      concat = scm_from_scmivect_t (v);
    }
  return concat;
}

VISIBLE SCM
scm_ivect_concatenate (SCM ivects)
{
  SCM concat;
  if (scm_is_ivect (ivects))
    concat = _ivect_concatenate_from_ivect (ivects);
  else if (scm_is_true (scm_list_p (ivects)))
    concat = _ivect_concatenate_from_list (ivects);
  else
    {
      SCM_ASSERT_TYPE (false, ivects, SCM_ARG1, "ivect-concatenate",
                       "ivect or proper list");
      concat = SCM_UNDEFINED;
    }
  return concat;
}

#define IVECT_CONCATENATE__(T, T_IN_CAPS)                               \
                                                                        \
  static SCM                                                            \
  _##T##ivect_concatenate_from_ivect (SCM ivects)                       \
  {                                                                     \
    SCM concat;                                                         \
    scmivect_t ivects_ = scm_to_scmivect_t (ivects);                    \
    if (ivects_ == NULL)                                                \
      concat = SCM_##T_IN_CAPS##IVECT_NULL;                             \
    else                                                                \
      {                                                                 \
        T##ivect_t v = scm_to_##T##ivect_t (scmivect_ref (ivects_, 0)); \
        const size_t n = scmivect_length (ivects_);                     \
        for (size_t i = 1; i < n; i++)                                  \
          v = T##ivect_append (v, (scm_to_##T##ivect_t                  \
                                   (scmivect_ref (ivects_, i))));       \
        concat = scm_from_##T##ivect_t (v);                             \
      }                                                                 \
    return concat;                                                      \
  }                                                                     \
                                                                        \
  static SCM                                                            \
  _##T##ivect_concatenate_from_list (SCM ivects)                        \
  {                                                                     \
    SCM concat;                                                         \
    if (scm_is_null (ivects))                                           \
      concat = SCM_##T_IN_CAPS##IVECT_NULL;                             \
    else                                                                \
      {                                                                 \
        T##ivect_t v = scm_to_##T##ivect_t (SCM_CAR (ivects));          \
        for (SCM p = SCM_CDR (ivects); !scm_is_null (p);                \
             p = SCM_CDR (p))                                           \
          v = T##ivect_append (v, scm_to_##T##ivect_t (SCM_CAR (p)));   \
        concat = scm_from_##T##ivect_t (v);                             \
      }                                                                 \
    return concat;                                                      \
  }                                                                     \
                                                                        \
  VISIBLE SCM                                                           \
  scm_##T##ivect_concatenate (SCM ivects)                               \
  {                                                                     \
    SCM concat;                                                         \
    if (scm_is_ivect (ivects))                                          \
      concat = _##T##ivect_concatenate_from_ivect (ivects);             \
    else if (scm_is_true (scm_list_p (ivects)))                         \
      concat = _##T##ivect_concatenate_from_list (ivects);              \
    else                                                                \
      {                                                                 \
        SCM_ASSERT_TYPE (false, ivects,                                 \
                         SCM_ARG1, #T "ivect-concatenate",              \
                         "ivect or proper list");                       \
        concat = SCM_UNDEFINED;                                         \
      }                                                                 \
    return concat;                                                      \
  }

IVECT_CONCATENATE__ (u8, U8);
IVECT_CONCATENATE__ (u16, U16);
IVECT_CONCATENATE__ (u32, U32);
IVECT_CONCATENATE__ (u64, U64);
IVECT_CONCATENATE__ (s8, S8);
IVECT_CONCATENATE__ (s16, S16);
IVECT_CONCATENATE__ (s32, S32);
IVECT_CONCATENATE__ (s64, S64);
IVECT_CONCATENATE__ (f32, F32);
IVECT_CONCATENATE__ (f64, F64);
IVECT_CONCATENATE__ (c32, C32);
IVECT_CONCATENATE__ (c64, C64);

//----------------------------------------------------------------------

VISIBLE SCM
scm_ivect_to_list (SCM ivect, SCM start, SCM end)
{
  const char *who = "ivect->list";

  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t vlen = scmivect_length (v);

  size_t i_;
  size_t n_;
  _ivect_i_n (who, vlen, start, end, &i_, &n_);

  SCM lst = SCM_EOL;
  if (n_ != 0)
    {
      size_t k = i_ + n_;
      const SCM *p = scmivect_ptr (v, k - 1);
      while (k != i_)
        {
          lst = scm_cons (*p, lst);
          p = scmivect_prev (v, k - 1, p);
          k--;
        }
    }
  return lst;
}

#define IVECT_TO_LIST__(T, ENTRY_T, CONVERT)                    \
  VISIBLE SCM                                                   \
  scm_##T##ivect_to_list (SCM ivect, SCM start, SCM end)        \
  {                                                             \
    const char *who = #T "ivect->list";                         \
                                                                \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    const size_t vlen = T##ivect_length (v);                    \
                                                                \
    size_t i_;                                                  \
    size_t n_;                                                  \
    _ivect_i_n (who, vlen, start, end, &i_, &n_);               \
                                                                \
    SCM lst = SCM_EOL;                                          \
    if (n_ != 0)                                                \
      {                                                         \
        size_t k = i_ + n_;                                     \
        const ENTRY_T *p = T##ivect_ptr (v, k - 1);             \
        while (k != i_)                                         \
          {                                                     \
            lst = scm_cons (CONVERT (*p), lst);                 \
            p = T##ivect_prev (v, k - 1, p);                    \
            k--;                                                \
          }                                                     \
      }                                                         \
    return lst;                                                 \
  }

IVECT_TO_LIST__ (u8, uint8_t, scm_from_uint8);
IVECT_TO_LIST__ (u16, uint16_t, scm_from_uint16);
IVECT_TO_LIST__ (u32, uint32_t, scm_from_uint32);
IVECT_TO_LIST__ (u64, uint64_t, scm_from_uint64);
IVECT_TO_LIST__ (s8, int8_t, scm_from_int8);
IVECT_TO_LIST__ (s16, int16_t, scm_from_int16);
IVECT_TO_LIST__ (s32, int32_t, scm_from_int32);
IVECT_TO_LIST__ (s64, int64_t, scm_from_int64);
IVECT_TO_LIST__ (f32, float, scm_from_double);
IVECT_TO_LIST__ (f64, double, scm_from_double);
IVECT_TO_LIST__ (c32, float complex, scm_from_float_complex);
IVECT_TO_LIST__ (c64, double complex, scm_from_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_list_to_ivect (SCM lst)
{
  const size_t buf_size = 128;
  SCM a[buf_size];

  const size_t n = scm_to_size_t (scm_length (lst));
  const size_t q = n / buf_size;
  const size_t r = n % buf_size;

  scmivect_t v = NULL;

  for (size_t i = 0; i < q; i++)
    {
      for (size_t j = 0; j < buf_size; j++)
        {
          a[j] = SCM_CAR (lst);
          lst = SCM_CDR (lst);
        }
      v = scmivect_pushes (v, buf_size, a);
    }
  if (r != 0)
    {
      for (size_t j = 0; j < r; j++)
        {
          a[j] = SCM_CAR (lst);
          lst = SCM_CDR (lst);
        }
      v = scmivect_pushes (v, r, a);
    }

  return scm_from_scmivect_t (v);
}

#define LIST_TO_IVECT__(T, ENTRY_T, CONVERT)            \
  VISIBLE SCM                                           \
  scm_list_to_##T##ivect (SCM lst)                      \
  {                                                     \
    const size_t buf_size = 128;                        \
    ENTRY_T a[buf_size];                                \
                                                        \
    const size_t n = scm_to_size_t (scm_length (lst));  \
    const size_t q = n / buf_size;                      \
    const size_t r = n % buf_size;                      \
                                                        \
    T##ivect_t v = NULL;                                \
                                                        \
    for (size_t i = 0; i < q; i++)                      \
      {                                                 \
        for (size_t j = 0; j < buf_size; j++)           \
          {                                             \
            a[j] = CONVERT (SCM_CAR (lst));             \
            lst = SCM_CDR (lst);                        \
          }                                             \
        v = T##ivect_pushes (v, buf_size, a);           \
      }                                                 \
    if (r != 0)                                         \
      {                                                 \
        for (size_t j = 0; j < r; j++)                  \
          {                                             \
            a[j] = CONVERT (SCM_CAR (lst));             \
            lst = SCM_CDR (lst);                        \
          }                                             \
        v = T##ivect_pushes (v, r, a);                  \
      }                                                 \
                                                        \
    return scm_from_##T##ivect_t (v);                   \
  }

LIST_TO_IVECT__ (u8, uint8_t, scm_to_uint8);
LIST_TO_IVECT__ (u16, uint16_t, scm_to_uint16);
LIST_TO_IVECT__ (u32, uint32_t, scm_to_uint32);
LIST_TO_IVECT__ (u64, uint64_t, scm_to_uint64);
LIST_TO_IVECT__ (s8, int8_t, scm_to_int8);
LIST_TO_IVECT__ (s16, int16_t, scm_to_int16);
LIST_TO_IVECT__ (s32, int32_t, scm_to_int32);
LIST_TO_IVECT__ (s64, int64_t, scm_to_int64);
LIST_TO_IVECT__ (f32, float, scm_to_double);
LIST_TO_IVECT__ (f64, double, scm_to_double);
LIST_TO_IVECT__ (c32, float complex, scm_to_float_complex);
LIST_TO_IVECT__ (c64, double complex, scm_to_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_ivect_to_vlst (SCM ivect, SCM start, SCM end)
{
  const char *who = "ivect->vlst";

  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t vlen = scmivect_length (v);

  size_t i_;
  size_t n_;
  _ivect_i_n (who, vlen, start, end, &i_, &n_);

  SCM lst = SCM_VLST_NULL;
  if (n_ != 0)
    {
      size_t k = i_ + n_;
      const SCM *p = scmivect_ptr (v, k - 1);
      while (k != i_)
        {
          lst = scm_vlst_cons (*p, lst);
          p = scmivect_prev (v, k - 1, p);
          k--;
        }
    }
  return lst;
}

#define IVECT_TO_VLST__(T, ENTRY_T, CONVERT)                    \
  VISIBLE SCM                                                   \
  scm_##T##ivect_to_vlst (SCM ivect, SCM start, SCM end)        \
  {                                                             \
    const char *who = #T "ivect->vlst";                         \
                                                                \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    const size_t vlen = T##ivect_length (v);                    \
                                                                \
    size_t i_;                                                  \
    size_t n_;                                                  \
    _ivect_i_n (who, vlen, start, end, &i_, &n_);               \
                                                                \
    SCM lst = SCM_VLST_NULL;                                    \
    if (n_ != 0)                                                \
      {                                                         \
        size_t k = i_ + n_;                                     \
        const ENTRY_T *p = T##ivect_ptr (v, k - 1);             \
        while (k != i_)                                         \
          {                                                     \
            lst = scm_vlst_cons (CONVERT (*p), lst);            \
            p = T##ivect_prev (v, k - 1, p);                    \
            k--;                                                \
          }                                                     \
      }                                                         \
    return lst;                                                 \
  }

IVECT_TO_VLST__ (u8, uint8_t, scm_from_uint8);
IVECT_TO_VLST__ (u16, uint16_t, scm_from_uint16);
IVECT_TO_VLST__ (u32, uint32_t, scm_from_uint32);
IVECT_TO_VLST__ (u64, uint64_t, scm_from_uint64);
IVECT_TO_VLST__ (s8, int8_t, scm_from_int8);
IVECT_TO_VLST__ (s16, int16_t, scm_from_int16);
IVECT_TO_VLST__ (s32, int32_t, scm_from_int32);
IVECT_TO_VLST__ (s64, int64_t, scm_from_int64);
IVECT_TO_VLST__ (f32, float, scm_from_double);
IVECT_TO_VLST__ (f64, double, scm_from_double);
IVECT_TO_VLST__ (c32, float complex, scm_from_float_complex);
IVECT_TO_VLST__ (c64, double complex, scm_from_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_vlst_to_ivect (SCM lst)
{
  const size_t buf_size = 128;
  SCM a[buf_size];

  const size_t n = scm_c_vlst_length (lst);
  const size_t q = n / buf_size;
  const size_t r = n % buf_size;

  scmivect_t v = NULL;

  for (size_t i = 0; i < q; i++)
    {
      for (size_t j = 0; j < buf_size; j++)
        {
          a[j] = scm_vlst_car (lst);
          lst = scm_vlst_cdr (lst);
        }
      v = scmivect_pushes (v, buf_size, a);
    }
  if (r != 0)
    {
      for (size_t j = 0; j < r; j++)
        {
          a[j] = scm_vlst_car (lst);
          lst = scm_vlst_cdr (lst);
        }
      v = scmivect_pushes (v, r, a);
    }

  return scm_from_scmivect_t (v);
}

#define VLST_TO_IVECT__(T, ENTRY_T, CONVERT)    \
  VISIBLE SCM                                   \
  scm_vlst_to_##T##ivect (SCM lst)              \
  {                                             \
   const size_t buf_size = 128;                 \
   ENTRY_T a[buf_size];                         \
                                                \
   const size_t n = scm_c_vlst_length (lst);    \
   const size_t q = n / buf_size;               \
   const size_t r = n % buf_size;               \
                                                \
   T##ivect_t v = NULL;                         \
                                                \
   for (size_t i = 0; i < q; i++)               \
     {                                          \
      for (size_t j = 0; j < buf_size; j++)     \
        {                                       \
         a[j] = CONVERT (scm_vlst_car (lst));   \
         lst = scm_vlst_cdr (lst);              \
         }                                      \
      v = T##ivect_pushes (v, buf_size, a);     \
      }                                         \
   if (r != 0)                                  \
     {                                          \
      for (size_t j = 0; j < r; j++)            \
        {                                       \
         a[j] = CONVERT (scm_vlst_car (lst));   \
         lst = scm_vlst_cdr (lst);              \
         }                                      \
      v = T##ivect_pushes (v, r, a);            \
      }                                         \
                                                \
   return scm_from_##T##ivect_t (v);            \
   }

VLST_TO_IVECT__ (u8, uint8_t, scm_to_uint8);
VLST_TO_IVECT__ (u16, uint16_t, scm_to_uint16);
VLST_TO_IVECT__ (u32, uint32_t, scm_to_uint32);
VLST_TO_IVECT__ (u64, uint64_t, scm_to_uint64);
VLST_TO_IVECT__ (s8, int8_t, scm_to_int8);
VLST_TO_IVECT__ (s16, int16_t, scm_to_int16);
VLST_TO_IVECT__ (s32, int32_t, scm_to_int32);
VLST_TO_IVECT__ (s64, int64_t, scm_to_int64);
VLST_TO_IVECT__ (f32, float, scm_to_double);
VLST_TO_IVECT__ (f64, double, scm_to_double);
VLST_TO_IVECT__ (c32, float complex, scm_to_float_complex);
VLST_TO_IVECT__ (c64, double complex, scm_to_double_complex);

//----------------------------------------------------------------------

VISIBLE SCM
scm_ivect_to_vector (SCM ivect, SCM start, SCM end)
{
  const char *who = "ivect->vector";

  scm_t_array_handle handle;
  size_t _len;
  ssize_t _inc;

  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t vlen = scmivect_length (v);

  size_t i_;
  size_t n_;
  _ivect_i_n (who, vlen, start, end, &i_, &n_);

  SCM vect = scm_c_make_vector (n_, SCM_UNDEFINED);
  SCM *elems = scm_vector_writable_elements (vect, &handle, &_len, &_inc);
  assert (_len == n_);
  assert (_inc == 1);
  scmivect_refs (v, i_, n_, elems);
  scm_array_handle_release (&handle);
  return vect;
}

#define IVECT_TO_VECTOR__(T, ENTRY_T, CONVERT)                          \
  VISIBLE SCM                                                           \
  scm_##T##ivect_to_vector (SCM ivect, SCM start, SCM end)              \
  {                                                                     \
    const char *who = #T "ivect->vector";                               \
                                                                        \
    scm_t_array_handle handle;                                          \
    size_t _len;                                                        \
    ssize_t _inc;                                                       \
                                                                        \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                         \
    const size_t vlen = T##ivect_length (v);                            \
                                                                        \
    size_t i_;                                                          \
    size_t n_;                                                          \
    _ivect_i_n (who, vlen, start, end, &i_, &n_);                       \
                                                                        \
    SCM vect = scm_c_make_vector (n_, SCM_UNDEFINED);                   \
    SCM *elems =                                                        \
      scm_vector_writable_elements (vect, &handle, &_len, &_inc);       \
    assert (_len == n_);                                                \
    assert (_inc == 1);                                                 \
    size_t k = 0;                                                       \
    const ENTRY_T *p = T##ivect_ptr (v, i_);                            \
    while (p != NULL)                                                   \
      {                                                                 \
        elems[k] = CONVERT (*p);                                        \
        p = T##ivect_next (v, k, p);                                    \
        k++;                                                            \
      }                                                                 \
    scm_array_handle_release (&handle);                                 \
    return vect;                                                        \
  }

IVECT_TO_VECTOR__ (u8, uint8_t, scm_from_uint8);
IVECT_TO_VECTOR__ (u16, uint16_t, scm_from_uint16);
IVECT_TO_VECTOR__ (u32, uint32_t, scm_from_uint32);
IVECT_TO_VECTOR__ (u64, uint64_t, scm_from_uint64);
IVECT_TO_VECTOR__ (s8, int8_t, scm_from_int8);
IVECT_TO_VECTOR__ (s16, int16_t, scm_from_int16);
IVECT_TO_VECTOR__ (s32, int32_t, scm_from_int32);
IVECT_TO_VECTOR__ (s64, int64_t, scm_from_int64);
IVECT_TO_VECTOR__ (f32, float, scm_from_double);
IVECT_TO_VECTOR__ (f64, double, scm_from_double);
IVECT_TO_VECTOR__ (c32, float complex, scm_from_float_complex);
IVECT_TO_VECTOR__ (c64, double complex, scm_from_double_complex);

#define IVECT_TO_TYPED_VECTOR__(T, ENTRY_T, TAKE)               \
  VISIBLE SCM                                                   \
  scm_##T##ivect_to_##T##vector (SCM ivect, SCM start, SCM end) \
  {                                                             \
    const char *who = #T "ivect->" #T "vector";                 \
                                                                \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    const size_t vlen = T##ivect_length (v);                    \
                                                                \
    size_t i_;                                                  \
    size_t n_;                                                  \
    _ivect_i_n (who, vlen, start, end, &i_, &n_);               \
                                                                \
    ENTRY_T *elems = scm_malloc (n_ * sizeof (ENTRY_T));        \
    T##ivect_refs (v, i_, n_, elems);                           \
    return TAKE (elems, n_);                                    \
  }

IVECT_TO_TYPED_VECTOR__ (u8, uint8_t, scm_take_u8vector);
IVECT_TO_TYPED_VECTOR__ (u16, uint16_t, scm_take_u16vector);
IVECT_TO_TYPED_VECTOR__ (u32, uint32_t, scm_take_u32vector);
IVECT_TO_TYPED_VECTOR__ (u64, uint64_t, scm_take_u64vector);
IVECT_TO_TYPED_VECTOR__ (s8, int8_t, scm_take_s8vector);
IVECT_TO_TYPED_VECTOR__ (s16, int16_t, scm_take_s16vector);
IVECT_TO_TYPED_VECTOR__ (s32, int32_t, scm_take_s32vector);
IVECT_TO_TYPED_VECTOR__ (s64, int64_t, scm_take_s64vector);
IVECT_TO_TYPED_VECTOR__ (f32, float, scm_take_f32vector);
IVECT_TO_TYPED_VECTOR__ (f64, double, scm_take_f64vector);

#define IVECT_TO_COMPLEX_VECTOR__(T, FLOAT, RE, IM, TAKE)       \
  VISIBLE SCM                                                   \
  scm_##T##ivect_to_##T##vector (SCM ivect, SCM start, SCM end) \
  {                                                             \
    const char *who = #T "ivect->" #T "vector";                 \
                                                                \
    T##ivect_t v = scm_to_##T##ivect_t (ivect);                 \
    const size_t vlen = T##ivect_length (v);                    \
                                                                \
    size_t i_;                                                  \
    size_t n_;                                                  \
    _ivect_i_n (who, vlen, start, end, &i_, &n_);               \
                                                                \
    FLOAT *elems = scm_malloc (n_ * 2 * sizeof (FLOAT));        \
    size_t k = 0;                                               \
    const FLOAT complex *p = T##ivect_ptr (v, i_);              \
    while (p != NULL)                                           \
      {                                                         \
        elems[2 * k] = RE (*p);                                 \
        elems[2 * k + 1] = IM (*p);                             \
        p = T##ivect_next (v, k, p);                            \
        k++;                                                    \
      }                                                         \
    return TAKE (elems, n_);                                    \
  }

IVECT_TO_COMPLEX_VECTOR__ (c32, float, crealf, cimagf, scm_take_c32vector);
IVECT_TO_COMPLEX_VECTOR__ (c64, double, creal, cimag, scm_take_c64vector);

//----------------------------------------------------------------------

VISIBLE SCM
scm_vector_to_ivect (SCM vect)
{
  const size_t buf_size = 128;
  SCM a[buf_size];

  scm_t_array_handle handle;
  size_t n;
  ssize_t stride;

  const SCM *elems = scm_vector_elements (vect, &handle, &n, &stride);

  const size_t q = n / buf_size;
  const size_t r = n % buf_size;

  scmivect_t v = NULL;

  for (size_t i = 0; i < q; i++)
    {
      for (size_t j = 0; j < buf_size; j++)
        {
          a[j] = *elems;
          elems += stride;
        }
      v = scmivect_pushes (v, buf_size, a);
    }
  if (r != 0)
    {
      for (size_t j = 0; j < r; j++)
        {
          a[j] = *elems;
          elems += stride;
        }
      v = scmivect_pushes (v, r, a);
    }

  scm_array_handle_release (&handle);

  return scm_from_scmivect_t (v);
}

#define VECTOR_TO_IVECT__(T, ENTRY_T, CONVERT)          \
  VISIBLE SCM                                           \
  scm_vector_to_##T##ivect (SCM vect)                   \
  {                                                     \
    const size_t buf_size = 128;                        \
    ENTRY_T a[buf_size];                                \
                                                        \
    scm_t_array_handle handle;                          \
    size_t n;                                           \
    ssize_t stride;                                     \
                                                        \
    const SCM *elems =                                  \
      scm_vector_elements (vect, &handle, &n, &stride); \
                                                        \
    const size_t q = n / buf_size;                      \
    const size_t r = n % buf_size;                      \
                                                        \
    T##ivect_t v = NULL;                                \
                                                        \
    for (size_t i = 0; i < q; i++)                      \
      {                                                 \
        for (size_t j = 0; j < buf_size; j++)           \
          {                                             \
            a[j] = CONVERT (*elems);                    \
            elems += stride;                            \
          }                                             \
        v = T##ivect_pushes (v, buf_size, a);           \
      }                                                 \
    if (r != 0)                                         \
      {                                                 \
        for (size_t j = 0; j < r; j++)                  \
          {                                             \
            a[j] = CONVERT (*elems);                    \
            elems += stride;                            \
          }                                             \
        v = T##ivect_pushes (v, r, a);                  \
      }                                                 \
                                                        \
    scm_array_handle_release (&handle);                 \
                                                        \
    return scm_from_##T##ivect_t (v);                   \
  }

VECTOR_TO_IVECT__ (u8, uint8_t, scm_to_uint8);
VECTOR_TO_IVECT__ (u16, uint16_t, scm_to_uint16);
VECTOR_TO_IVECT__ (u32, uint32_t, scm_to_uint32);
VECTOR_TO_IVECT__ (u64, uint64_t, scm_to_uint64);
VECTOR_TO_IVECT__ (s8, int8_t, scm_to_int8);
VECTOR_TO_IVECT__ (s16, int16_t, scm_to_int16);
VECTOR_TO_IVECT__ (s32, int32_t, scm_to_int32);
VECTOR_TO_IVECT__ (s64, int64_t, scm_to_int64);
VECTOR_TO_IVECT__ (f32, float, scm_to_double);
VECTOR_TO_IVECT__ (f64, double, scm_to_double);
VECTOR_TO_IVECT__ (c32, float complex, scm_to_float_complex);
VECTOR_TO_IVECT__ (c64, double complex, scm_to_double_complex);

#define TYPED_VECTOR_TO_IVECT__(T, ENTRY_T)                     \
  VISIBLE SCM                                                   \
  scm_##T##vector_to_##T##ivect (SCM vect)                      \
  {                                                             \
   const size_t buf_size = 128;                                 \
   ENTRY_T a[buf_size];                                         \
                                                                \
   scm_t_array_handle handle;                                   \
   size_t n;                                                    \
   ssize_t stride;                                              \
                                                                \
   const ENTRY_T *elems =                                       \
     scm_##T##vector_elements (vect, &handle, &n, &stride);     \
                                                                \
   const size_t q = n / buf_size;                               \
   const size_t r = n % buf_size;                               \
                                                                \
   T##ivect_t v = NULL;                                         \
                                                                \
   for (size_t i = 0; i < q; i++)                               \
     {                                                          \
      for (size_t j = 0; j < buf_size; j++)                     \
        {                                                       \
         a[j] = *elems;                                         \
         elems += stride;                                       \
         }                                                      \
      v = T##ivect_pushes (v, buf_size, a);                     \
      }                                                         \
   if (r != 0)                                                  \
     {                                                          \
      for (size_t j = 0; j < r; j++)                            \
        {                                                       \
          a[j] = *elems;                                        \
          elems += stride;                                      \
        }                                                       \
      v = T##ivect_pushes (v, r, a);                            \
     }                                                          \
                                                                \
   scm_array_handle_release (&handle);                          \
                                                                \
   return scm_from_##T##ivect_t (v);                            \
  }

TYPED_VECTOR_TO_IVECT__ (u8, uint8_t);
TYPED_VECTOR_TO_IVECT__ (u16, uint16_t);
TYPED_VECTOR_TO_IVECT__ (u32, uint32_t);
TYPED_VECTOR_TO_IVECT__ (u64, uint64_t);
TYPED_VECTOR_TO_IVECT__ (s8, int8_t);
TYPED_VECTOR_TO_IVECT__ (s16, int16_t);
TYPED_VECTOR_TO_IVECT__ (s32, int32_t);
TYPED_VECTOR_TO_IVECT__ (s64, int64_t);
TYPED_VECTOR_TO_IVECT__ (f32, float);
TYPED_VECTOR_TO_IVECT__ (f64, double);

#define COMPLEX_VECTOR_TO_IVECT__(T, FLOAT)                     \
  VISIBLE SCM                                                   \
  scm_##T##vector_to_##T##ivect (SCM vect)                      \
  {                                                             \
    const size_t buf_size = 128;                                \
    FLOAT complex a[buf_size];                                  \
                                                                \
    scm_t_array_handle handle;                                  \
    size_t n;                                                   \
    ssize_t stride;                                             \
                                                                \
    const FLOAT *elems =                                        \
      scm_##T##vector_elements (vect, &handle, &n, &stride);    \
                                                                \
    const size_t q = n / buf_size;                              \
    const size_t r = n % buf_size;                              \
                                                                \
    T##ivect_t v = NULL;                                        \
                                                                \
    for (size_t i = 0; i < q; i++)                              \
      {                                                         \
        for (size_t j = 0; j < buf_size; j++)                   \
          {                                                     \
            a[j] = elems[0] + elems[stride] * I;                \
            elems += 2 * stride;                                \
          }                                                     \
        v = T##ivect_pushes (v, buf_size, a);                   \
      }                                                         \
    if (r != 0)                                                 \
      {                                                         \
        for (size_t j = 0; j < r; j++)                          \
          {                                                     \
            a[j] = elems[0] + elems[stride] * I;                \
            elems += 2 * stride;                                \
          }                                                     \
        v = T##ivect_pushes (v, r, a);                          \
      }                                                         \
                                                                \
    scm_array_handle_release (&handle);                         \
                                                                \
    return scm_from_##T##ivect_t (v);                           \
  }

COMPLEX_VECTOR_TO_IVECT__ (c32, float);
COMPLEX_VECTOR_TO_IVECT__ (c64, double);

//----------------------------------------------------------------------

static SCM
_scm_ivect_map_one (SCM proc, SCM ivect)
{
  // This implementation maps the ivect entries in left-to-right
  // order.

  const size_t buffer_size = 256;
  SCM buffer[buffer_size];

  scmivect_t v = scm_to_scmivect_t (ivect);
  const size_t vlen = scmivect_length (v);
  const size_t q = vlen / buffer_size;
  const size_t r = vlen % buffer_size;

  scmivect_t w = NULL;

  for (size_t i = 0; i < q; i++)
    {
      scmivect_refs (v, i * buffer_size, buffer_size, buffer);
      for (size_t j = 0; j < buffer_size; j++)
        buffer[j] = scm_call_1 (proc, buffer[j]);
      w = scmivect_pushes (w, buffer_size, buffer);
    }
  scmivect_refs (v, q * buffer_size, r, buffer);
  for (size_t j = 0; j < r; j++)
    buffer[j] = scm_call_1 (proc, buffer[j]);
  w = scmivect_pushes (w, r, buffer);

  return scm_from_scmivect_t (w);
}

static SCM
_scm_ivect_map_two (SCM proc, SCM ivect1, SCM ivect2)
{
  // This implementation maps the ivect entries in left-to-right order.

  const size_t buffer_size = 256;
  SCM buffer1[buffer_size];
  SCM buffer2[buffer_size];

  scmivect_t v1 = scm_to_scmivect_t (ivect1);
  const size_t v1len = scmivect_length (v1);
  scmivect_t v2 = scm_to_scmivect_t (ivect2);
  const size_t v2len = scmivect_length (v2);
  const size_t vlen = szmin__ (v1len, v2len);
  const size_t q = vlen / buffer_size;
  const size_t r = vlen % buffer_size;

  scmivect_t w = NULL;

  for (size_t i = 0; i < q; i++)
    {
      scmivect_refs (v1, i * buffer_size, buffer_size, buffer1);
      scmivect_refs (v2, i * buffer_size, buffer_size, buffer2);
      for (size_t j = 0; j < buffer_size; j++)
        buffer1[j] = scm_call_2 (proc, buffer1[j], buffer2[j]);
      w = scmivect_pushes (w, buffer_size, buffer1);
    }
  scmivect_refs (v1, q * buffer_size, r, buffer1);
  scmivect_refs (v2, q * buffer_size, r, buffer2);
  for (size_t j = 0; j < r; j++)
    buffer1[j] = scm_call_2 (proc, buffer1[j], buffer2[j]);
  w = scmivect_pushes (w, r, buffer1);

  return scm_from_scmivect_t (w);
}

static SCM
_scm_ivect_map_many (SCM proc, SCM ivect1, SCM more_ivects)
{
  // This implementation maps the ivect entries in left-to-right order.

  const size_t buffer_size = 256;
  SCM buffer[buffer_size];

  const size_t arg_count = scm_to_size_t (scm_length (more_ivects)) + 1;
  scmivect_t args[arg_count];
  size_t vlen = SIZE_MAX;
  args[0] = scm_to_scmivect_t (ivect1);
  vlen = szmin__ (vlen, scmivect_length (args[0]));
  for (size_t i = 1; i < arg_count; i++)
    {
      args[i] = scm_to_scmivect_t (SCM_CAR (more_ivects));
      vlen = szmin__ (vlen, scmivect_length (args[i]));
      more_ivects = SCM_CDR (more_ivects);
    }
  const size_t q = vlen / buffer_size;
  const size_t r = vlen % buffer_size;

  scmivect_t w = NULL;

  for (size_t i = 0; i < q; i++)
    {
      const size_t chunk_start = i * buffer_size;
      for (size_t j = 0; j < buffer_size; j++)
        {
          SCM lst = SCM_EOL;
          for (size_t k = arg_count; k != 0; k--)
            lst = scm_cons (scmivect_ref (args[k - 1], chunk_start + j), lst);
          buffer[j] = scm_apply_0 (proc, lst);
        }
      w = scmivect_pushes (w, buffer_size, buffer);
    }
  const size_t chunk_start = q * buffer_size;
  for (size_t j = 0; j < r; j++)
    {
      SCM lst = SCM_EOL;
      for (size_t k = arg_count; k != 0; k--)
        lst = scm_cons (scmivect_ref (args[k - 1], chunk_start + j), lst);
      buffer[j] = scm_apply_0 (proc, lst);
    }
  w = scmivect_pushes (w, r, buffer);

  return scm_from_scmivect_t (w);
}

VISIBLE SCM
scm_ivect_map (SCM proc, SCM ivect, SCM another_ivect_or_ivects)
{
  SCM result;
  if (SCM_UNBNDP (another_ivect_or_ivects))
    result = _scm_ivect_map_one (proc, ivect);
  else if (scm_is_ivect (another_ivect_or_ivects))
    result = _scm_ivect_map_two (proc, ivect, another_ivect_or_ivects);
  else
    result = _scm_ivect_map_many (proc, ivect, another_ivect_or_ivects);
  return result;
}

VISIBLE SCM
scm_ivect_map_in_order (SCM proc, SCM ivect, SCM another_ivect_or_ivects)
{
  return scm_ivect_map (proc, ivect, another_ivect_or_ivects);
}

//----------------------------------------------------------------------

static void
initialize_ivect_null_variables__ (void)
{
  _scm_ivect_null__ = scm_from_scmivect_t (NULL);
  scm_c_define ("ivect-null", _scm_ivect_null__);
  _scm_u8ivect_null__ = scm_from_u8ivect_t (NULL);
  scm_c_define ("u8ivect-null", _scm_u8ivect_null__);
  _scm_u16ivect_null__ = scm_from_u16ivect_t (NULL);
  scm_c_define ("u16ivect-null", _scm_u16ivect_null__);
  _scm_u32ivect_null__ = scm_from_u32ivect_t (NULL);
  scm_c_define ("u32ivect-null", _scm_u32ivect_null__);
  _scm_u64ivect_null__ = scm_from_u64ivect_t (NULL);
  scm_c_define ("u64ivect-null", _scm_u64ivect_null__);
  _scm_s8ivect_null__ = scm_from_s8ivect_t (NULL);
  scm_c_define ("s8ivect-null", _scm_s8ivect_null__);
  _scm_s16ivect_null__ = scm_from_s16ivect_t (NULL);
  scm_c_define ("s16ivect-null", _scm_s16ivect_null__);
  _scm_s32ivect_null__ = scm_from_s32ivect_t (NULL);
  scm_c_define ("s32ivect-null", _scm_s32ivect_null__);
  _scm_s64ivect_null__ = scm_from_s64ivect_t (NULL);
  scm_c_define ("s64ivect-null", _scm_s64ivect_null__);
  _scm_f32ivect_null__ = scm_from_f32ivect_t (NULL);
  scm_c_define ("f32ivect-null", _scm_f32ivect_null__);
  _scm_f64ivect_null__ = scm_from_f64ivect_t (NULL);
  scm_c_define ("f64ivect-null", _scm_f64ivect_null__);
  _scm_c32ivect_null__ = scm_from_c32ivect_t (NULL);
  scm_c_define ("c32ivect-null", _scm_c32ivect_null__);
  _scm_c64ivect_null__ = scm_from_c64ivect_t (NULL);
  scm_c_define ("c64ivect-null", _scm_c64ivect_null__);
}

//----------------------------------------------------------------------

static SCM
guile_scmivect_equal_p__ (SCM a, SCM b)
{
  scmivect_t a_ = scm_to_scmivect_t (a);
  scmivect_t b_ = scm_to_scmivect_t (b);
  SCM equal_p;
  if (a_ == b_)
    // The vectors are the same (including the case where both are
    // empty, in which case a_ == NULL, b_ == NULL).
    equal_p = SCM_BOOL_T;
  else if (scmivect_length (a_) == scmivect_length (b_))
    {
      size_t i = 0;
      const SCM *p = scmivect_ptr (a_, 0);
      const SCM *q = scmivect_ptr (b_, 0);
      while (p != NULL && scm_is_true (scm_equal_p (*p, *q)))
        {
          p = scmivect_next (a_, i, p);
          q = scmivect_next (b_, i, q);
          i++;
        }
      equal_p = scm_from_bool (p == NULL);
    }
  else
    equal_p = SCM_BOOL_F;
  return equal_p;
}

#define GUILE_IVECT_EQUAL_P__(T, ENTRY_T)                               \
  static SCM                                                            \
  guile_##T##ivect_equal_p__ (SCM a, SCM b)                             \
  {                                                                     \
    T##ivect_t a_ = scm_to_##T##ivect_t (a);                            \
    T##ivect_t b_ = scm_to_##T##ivect_t (b);                            \
    SCM equal_p;                                                        \
    if (a_ == b_)                                                       \
      /* The vectors are the same (including the case where both are */ \
      /* empty, in which case a_ == NULL, b_ == NULL).               */ \
      equal_p = SCM_BOOL_T;                                             \
    else if (T##ivect_length (a_) == T##ivect_length (b_))              \
      {                                                                 \
        size_t i = 0;                                                   \
        const ENTRY_T *p = T##ivect_ptr (a_, 0);                        \
        const ENTRY_T *q = T##ivect_ptr (b_, 0);                        \
        while (p != NULL && *p == *q)                                   \
          {                                                             \
            p = T##ivect_next (a_, i, p);                               \
            q = T##ivect_next (b_, i, q);                               \
            i++;                                                        \
          }                                                             \
        equal_p = scm_from_bool (p == NULL);                            \
      }                                                                 \
    else                                                                \
      equal_p = SCM_BOOL_F;                                             \
    return equal_p;                                                     \
  }

GUILE_IVECT_EQUAL_P__ (u8, uint8_t) GUILE_IVECT_EQUAL_P__ (u16, uint16_t);
GUILE_IVECT_EQUAL_P__ (u32, uint32_t);
GUILE_IVECT_EQUAL_P__ (u64, uint64_t);
GUILE_IVECT_EQUAL_P__ (s8, int8_t);
GUILE_IVECT_EQUAL_P__ (s16, int16_t);
GUILE_IVECT_EQUAL_P__ (s32, int32_t);
GUILE_IVECT_EQUAL_P__ (s64, int64_t);
GUILE_IVECT_EQUAL_P__ (f32, float);
GUILE_IVECT_EQUAL_P__ (f64, double);
GUILE_IVECT_EQUAL_P__ (c32, float complex);
GUILE_IVECT_EQUAL_P__ (c64, double complex);

     VISIBLE SCM guile_ivect_equal_p__ (SCM a, SCM b)
{
  SCM equal_p;

  SCM a_type = scm_ivect_type (a);
  SCM b_type = scm_ivect_type (b);

  if (!scm_is_eq (a_type, b_type))
    equal_p = SCM_BOOL_F;
  else
    switch (_type_enum (a_type))
      {
      case _scm:
        equal_p = guile_scmivect_equal_p__ (a, b);
        break;
      case _u8:
        equal_p = guile_u8ivect_equal_p__ (a, b);
        break;
      case _u16:
        equal_p = guile_u16ivect_equal_p__ (a, b);
        break;
      case _u32:
        equal_p = guile_u32ivect_equal_p__ (a, b);
        break;
      case _u64:
        equal_p = guile_u64ivect_equal_p__ (a, b);
        break;
      case _s8:
        equal_p = guile_s8ivect_equal_p__ (a, b);
        break;
      case _s16:
        equal_p = guile_s16ivect_equal_p__ (a, b);
        break;
      case _s32:
        equal_p = guile_s32ivect_equal_p__ (a, b);
        break;
      case _s64:
        equal_p = guile_s64ivect_equal_p__ (a, b);
        break;
      case _f32:
        equal_p = guile_f32ivect_equal_p__ (a, b);
        break;
      case _f64:
        equal_p = guile_f64ivect_equal_p__ (a, b);
        break;
      case _c32:
        equal_p = guile_c32ivect_equal_p__ (a, b);
        break;
      case _c64:
        equal_p = guile_c64ivect_equal_p__ (a, b);
        break;
      default:
        assert (false);
      }

  return equal_p;
}

//----------------------------------------------------------------------

VISIBLE SCM
guile_ivect_write__ (SCM ivect, SCM port)
{
  SCM (*ref) (SCM, size_t);
  const SCM type = scm_ivect_type (ivect);
  const _type_enum_t e = _type_enum (type);
  switch (e)
    {
    case _scm:
      ref = scm_c_ivect_ref;
      break;
    case _u8:
      ref = scm_c_u8ivect_ref;
      break;
    case _u16:
      ref = scm_c_u16ivect_ref;
      break;
    case _u32:
      ref = scm_c_u32ivect_ref;
      break;
    case _u64:
      ref = scm_c_u64ivect_ref;
      break;
    case _s8:
      ref = scm_c_s8ivect_ref;
      break;
    case _s16:
      ref = scm_c_s16ivect_ref;
      break;
    case _s32:
      ref = scm_c_s32ivect_ref;
      break;
    case _s64:
      ref = scm_c_s64ivect_ref;
      break;
    case _f32:
      ref = scm_c_f32ivect_ref;
      break;
    case _f64:
      ref = scm_c_f64ivect_ref;
      break;
    case _c32:
      ref = scm_c_c32ivect_ref;
      break;
    case _c64:
      ref = scm_c_c64ivect_ref;
      break;
    default:
      assert (false);
      ref = NULL;
    }
  if (e == _scm)
    scm_display (scm_from_utf8_string ("#<ivect"), port);
  else
    scm_simple_format (port, scm_from_utf8_string ("#<~aivect"),
                       scm_list_1 (type));
  const size_t n = scm_c_any_ivect_length (ivect);
  for (size_t i = 0; i < n; i++)
    {
      scm_display (scm_from_utf8_string (" "), port);
      scm_write (ref (ivect, i), port);
    }
  scm_display (scm_from_utf8_string (">"), port);
  return SCM_UNSPECIFIED;
}

//----------------------------------------------------------------------
