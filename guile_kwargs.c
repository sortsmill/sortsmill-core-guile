#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libintl.h>
#include <libguile.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

static SCM
_scm_split_at_the_first_keyword (SCM lst, SCM max_positionals)
{
  // An internal routine that does not do enough error-checking for
  // general use.

  SCM positionals = SCM_EOL;
  size_t n = 0;
  while (!scm_is_null (lst) && !scm_is_keyword (SCM_CAR (lst)))
    {
      positionals = scm_cons (SCM_CAR (lst), positionals);
      n++;
      lst = SCM_CDR (lst);
    }

  if (scm_to_size_t (max_positionals) < n)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("lambda/kwargs"),
        rnrs_c_make_message_condition (_("too many positional arguments")),
        rnrs_make_irritants_condition (scm_list_1 (lst))));

  SCM values[2] = {
    scm_reverse_x_without_checking (positionals, SCM_EOL),
    lst
  };
  return scm_c_values (values, 2);
}

//-------------------------------------------------------------------------

void init_sortsmill_core_guile_kwargs (void);

VISIBLE void
init_sortsmill_core_guile_kwargs (void)
{
  scm_c_define_gsubr ("split-at-the-first-keyword%", 2, 0, 0,
                      _scm_split_at_the_first_keyword);
}

//-------------------------------------------------------------------------
