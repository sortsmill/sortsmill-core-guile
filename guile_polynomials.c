#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <string.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#ifndef SCM_ARG8
#define SCM_ARG8 8
#endif

//----------------------------------------------------------------------

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _symbol_f64,
                             (_symbol_f64__Value =
                              scm_from_latin1_symbol ("f64")));

static bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

static inline size_t
szmin (size_t a, size_t b)
{
  return (a < b) ? a : b;
}

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

typedef struct
{
  size_t degree;
  mpq_t *poly;
} _dynwind_data_for_clearing_mpq_poly_t;

static void
dynwind_handler_for_clearing_mpq_poly (void *p)
{
  _dynwind_data_for_clearing_mpq_poly_t *data = p;
  for (size_t i = 0; i <= data->degree; i++)
    mpq_clear (data->poly[i]);
  free (data);
}

static void
mpq_dynwind_init_poly (size_t degree, mpq_t *poly)
{
  for (size_t i = 0; i <= degree; i++)
    mpq_init (poly[i]);

  _dynwind_data_for_clearing_mpq_poly_t *data =
    scm_malloc (sizeof (_dynwind_data_for_clearing_mpq_poly_t));
  data->degree = degree;
  data->poly = poly;
  scm_dynwind_unwind_handler (dynwind_handler_for_clearing_mpq_poly,
                              data, SCM_F_WIND_EXPLICITLY);
}

typedef struct
{
  size_t degree;
  mpq_t *bipoly;
} _dynwind_data_for_clearing_mpq_bipoly_t;

static void
dynwind_handler_for_clearing_mpq_bipoly (void *p)
{
  _dynwind_data_for_clearing_mpq_bipoly_t *data = p;
  mpq_t (*bipoly)[data->degree + 1] =
    (mpq_t (*)[data->degree + 1]) data->bipoly;
  for (size_t i = 0; i <= data->degree; i++)
    for (size_t j = 0; j <= data->degree - i; j++)
      mpq_clear (bipoly[i][j]);
  free (data);
}

static void
mpq_dynwind_init_bipoly (size_t degree, mpq_t (*bipoly)[degree + 1])
{
  for (size_t i = 0; i <= degree; i++)
    for (size_t j = 0; j <= degree - i; j++)
      mpq_init (bipoly[i][j]);

  _dynwind_data_for_clearing_mpq_bipoly_t *data =
    scm_malloc (sizeof (_dynwind_data_for_clearing_mpq_bipoly_t));
  data->degree = degree;
  data->bipoly = (mpq_t *) bipoly;
  scm_dynwind_unwind_handler (dynwind_handler_for_clearing_mpq_bipoly,
                              data, SCM_F_WIND_EXPLICITLY);
}

static size_t
poly_degree (const char *who, unsigned int arg_pos, SCM poly)
{
  size_t n;
  if (scm_is_vector (poly))
    n = scm_c_vector_length (poly);
  else if (scm_is_pair (poly))
    n = scm_to_size_t (scm_length (poly));
  else if (scm_is_array (poly) && scm_c_array_rank (poly) == 1)
    n = scm_c_array_length (poly);
  else
    n = 0;
  SCM_ASSERT_TYPE ((n != 0), poly, arg_pos, who, _("non-empty vector or list"));
  return (n - 1);
}

static void
poly_scm_vector_to_mpq (size_t degree, SCM poly_scm, mpq_t *poly_mpq)
{
  scm_dynwind_begin (0);
  scm_t_array_handle handle;
  size_t n;
  ssize_t stride;
  const SCM *p = scm_vector_elements (poly_scm, &handle, &n, &stride);
  scm_dynwind_array_handle_release (&handle);
  for (size_t i = 0; i <= degree; i++)
    {
      scm_to_mpq (scm_inexact_to_exact (*p), poly_mpq[i]);
      p += stride;
    }
  scm_dynwind_end ();
}

static void
poly_scm_list_to_mpq (size_t degree, SCM poly_scm, mpq_t *poly_mpq)
{
  for (size_t i = 0; i <= degree; i++)
    {
      assert (scm_is_pair (poly_scm));
      scm_to_mpq (scm_inexact_to_exact (SCM_CAR (poly_scm)), poly_mpq[i]);
      poly_scm = SCM_CDR (poly_scm);
    }
}

static void
poly_scm_to_mpq (size_t degree, SCM poly_scm, mpq_t *poly_mpq)
{
  if (scm_is_vector (poly_scm))
    poly_scm_vector_to_mpq (degree, poly_scm, poly_mpq);
  else if (scm_is_pair (poly_scm))
    poly_scm_list_to_mpq (degree, poly_scm, poly_mpq);
  else if (scm_is_array (poly_scm) && scm_c_array_rank (poly_scm) == 1)
    poly_scm_list_to_mpq (degree, scm_array_to_list (poly_scm), poly_mpq);
  else
    assert (false);
}

static SCM
poly_mpq_to_scm (size_t degree, mpq_t *poly_mpq)
{
  SCM vec = scm_c_make_vector (degree + 1, SCM_UNDEFINED);

  scm_dynwind_begin (0);

  scm_t_array_handle handle;
  size_t len;
  ssize_t inc;
  SCM *elems = scm_vector_writable_elements (vec, &handle, &len, &inc);
  scm_dynwind_array_handle_release (&handle);
  for (size_t i = 0; i <= degree; i++)
    elems[i] = scm_from_mpq (poly_mpq[i]);

  scm_dynwind_end ();

  return vec;
}

static void
poly_scm_to_double (size_t degree, SCM poly_scm, double *poly_d)
{
  scm_t_array_handle handle;
  size_t n;
  ssize_t stride;
  const double *p = scm_f64vector_elements (scm_any_to_f64vector (poly_scm),
                                            &handle, &n, &stride);
  for (size_t i = 0; i <= degree; i++)
    {
      poly_d[i] = *p;
      p += stride;
    }
  scm_array_handle_release (&handle);
}

///static void
///poly_scm_to_float (size_t degree, SCM poly_scm, float *poly_d)
///{
///  scm_t_array_handle handle;
///  size_t n;
///  ssize_t stride;
///  const float *p = scm_f32vector_elements (scm_any_to_f32vector (poly_scm),
///                                           &handle, &n, &stride);
///  for (size_t i = 0; i <= degree; i++)
///    {
///      poly_d[i] = *p;
///      p += stride;
///    }
///  scm_array_handle_release (&handle);
///}

static void
_assert_poly_degrees_equal (size_t degree_ax, size_t degree_ay,
                            const char *who, SCM ax, SCM ay)
{
  if (degree_ax != degree_ay)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("polynomials of degrees that must be but are not equal")),
        rnrs_make_irritants_condition (scm_list_2 (ax, ay))));
}

static size_t
_parametric_poly_degree (size_t degree_ax, size_t degree_ay,
                         const char *who, SCM ax, SCM ay)
{
  _assert_poly_degrees_equal (degree_ax, degree_ay, who, ax, ay);
  return degree_ax;
}

//----------------------------------------------------------------------

static inline size_t
add_size_t (size_t a, size_t b)
{
  return (a + b);
}

static inline size_t
mul_size_t (size_t a, size_t b)
{
  return (a * b);
}

static inline size_t
max_size_t (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

typedef void exact_poly_poly_to_poly_t (size_t degree_a, ssize_t stride_a,
                                        mpq_t *a, size_t degree_b,
                                        ssize_t stride_b, mpq_t *b,
                                        ssize_t stride_c, mpq_t *c);
typedef void dpoly_poly_to_poly_t (size_t degree_a, ssize_t stride_a,
                                   const double *a, size_t degree_b,
                                   ssize_t stride_b, const double *b,
                                   ssize_t stride_c, double *c);

static SCM
exact_poly_poly_to_poly (const char *who, SCM poly1, SCM poly2,
                         size_t (*degree_func) (size_t, size_t),
                         exact_poly_poly_to_poly_t *proc)
{
  const size_t degree1 = poly_degree (who, SCM_ARG1, poly1);
  const size_t degree2 = poly_degree (who, SCM_ARG2, poly2);
  const size_t degree3 = degree_func (degree1, degree2);

  scm_dynwind_begin (0);

  mpq_t *poly1_mpq = scm_malloc ((degree1 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly1_mpq);
  mpq_dynwind_init_poly (degree1, poly1_mpq);
  poly_scm_to_mpq (degree1, poly1, poly1_mpq);

  mpq_t *poly2_mpq = scm_malloc ((degree2 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly2_mpq);
  mpq_dynwind_init_poly (degree2, poly2_mpq);
  poly_scm_to_mpq (degree2, poly2, poly2_mpq);

  mpq_t *poly3_mpq = scm_malloc ((degree3 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly3_mpq);
  mpq_dynwind_init_poly (degree3, poly3_mpq);

  proc (degree1, 1, poly1_mpq, degree2, 1, poly2_mpq, 1, poly3_mpq);
  const SCM poly3 = poly_mpq_to_scm (degree3, poly3_mpq);

  scm_dynwind_end ();

  return poly3;
}

static SCM
dpoly_poly_to_poly (const char *who, SCM poly1, SCM poly2,
                    size_t (*degree_func) (size_t, size_t),
                    dpoly_poly_to_poly_t *proc)
{
  const size_t degree1 = poly_degree (who, SCM_ARG1, poly1);
  const size_t degree2 = poly_degree (who, SCM_ARG2, poly2);
  const size_t degree3 = degree_func (degree1, degree2);

  double poly1_d[degree1 + 1];
  poly_scm_to_double (degree1, poly1, poly1_d);

  double poly2_d[degree2 + 1];
  poly_scm_to_double (degree2, poly2, poly2_d);

  double *poly3_d = scm_malloc ((degree3 + 1) * sizeof (double));

  proc (degree1, 1, poly1_d, degree2, 1, poly2_d, 1, poly3_d);

  return scm_take_f64vector (poly3_d, degree3 + 1);
}

//----------------------------------------------------------------------

static size_t
derivative_degree (size_t deg)
{
  return (deg == 0) ? 0 : (deg - 1);
}

typedef void exact_poly_to_poly_t (size_t degree, ssize_t stride, mpq_t *poly,
                                   ssize_t result_stride, mpq_t *result);
typedef void dpoly_to_poly_t (size_t degree, ssize_t stride, const double *poly,
                              ssize_t result_stride, double *result);

static SCM
exact_poly_to_poly (const char *who, SCM poly,
                    size_t (*degree_func) (size_t), exact_poly_to_poly_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree =
    (degree_func == NULL) ? degree : (degree_func (degree));

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t *result_poly_mpq = scm_malloc ((result_degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (result_poly_mpq);
  mpq_dynwind_init_poly (result_degree, result_poly_mpq);

  proc (degree, 1, poly_mpq, 1, result_poly_mpq);
  const SCM result = poly_mpq_to_scm (result_degree, result_poly_mpq);

  scm_dynwind_end ();

  return result;
}

static SCM
dpoly_to_poly (const char *who, SCM poly,
               size_t (*degree_func) (size_t), dpoly_to_poly_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree =
    (degree_func == NULL) ? degree : (degree_func (degree));

  scm_dynwind_begin (0);

  double *poly_d = scm_malloc ((degree + 1) * sizeof (double));
  scm_dynwind_free (poly_d);
  poly_scm_to_double (degree, poly, poly_d);

  double *result_poly_d = scm_malloc ((result_degree + 1) * sizeof (double));
  proc (degree, 1, poly_d, 1, result_poly_d);
  const SCM result = scm_take_f64vector (result_poly_d, result_degree + 1);

  scm_dynwind_end ();

  return result;
}

//----------------------------------------------------------------------

typedef bool exact_poly_to_bool_t (size_t degree, ssize_t stride, mpq_t *poly);
typedef bool dpoly_to_bool_t (size_t degree, ssize_t stride,
                              const double *poly);

static bool
exact_poly_to_bool (const char *who, SCM poly, exact_poly_to_bool_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  const bool result = proc (degree, 1, poly_mpq);

  scm_dynwind_end ();

  return result;
}

static bool
dpoly_to_bool (const char *who, SCM poly, dpoly_to_bool_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return proc (degree, 1, poly_d);
}

//----------------------------------------------------------------------

typedef size_t exact_poly_to_size_t_t (size_t degree, ssize_t stride,
                                       mpq_t *poly);
typedef size_t dpoly_to_size_t_t (size_t degree, ssize_t stride,
                                  const double *poly);

static size_t
exact_poly_to_size_t (const char *who, SCM poly, exact_poly_to_size_t_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  const size_t result = proc (degree, 1, poly_mpq);

  scm_dynwind_end ();

  return result;
}

static size_t
dpoly_to_size_t (const char *who, SCM poly, dpoly_to_size_t_t *proc)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return proc (degree, 1, poly_d);
}

//----------------------------------------------------------------------
//
// Polynomial addition.

VISIBLE SCM
scm_exact_poly_add_mono (SCM poly1, SCM poly2)
{
  return exact_poly_poly_to_poly ("exact-poly-add-mono", poly1, poly2,
                                  max_size_t, mpq_poly_add_mono);
}

VISIBLE SCM
scm_dpoly_add_mono (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-add-mono", poly1, poly2,
                             max_size_t, dpoly_add_mono);
}

VISIBLE SCM
scm_dpoly_add_bern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-add-bern", poly1, poly2,
                             max_size_t, dpoly_add_bern);
}

VISIBLE SCM
scm_dpoly_add_sbern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-add-sbern", poly1, poly2,
                             max_size_t, dpoly_add_sbern);
}

VISIBLE SCM
scm_dpoly_add_spower (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-add-spower", poly1, poly2,
                             max_size_t, dpoly_add_spower);
}

//----------------------------------------------------------------------
//
// Polynomial subtraction.

VISIBLE SCM
scm_exact_poly_sub_mono (SCM poly1, SCM poly2)
{
  return exact_poly_poly_to_poly ("exact-poly-sub-mono", poly1, poly2,
                                  max_size_t, mpq_poly_sub_mono);
}

VISIBLE SCM
scm_dpoly_sub_mono (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-sub-mono", poly1, poly2,
                             max_size_t, dpoly_sub_mono);
}

VISIBLE SCM
scm_dpoly_sub_bern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-sub-bern", poly1, poly2,
                             max_size_t, dpoly_sub_bern);
}

VISIBLE SCM
scm_dpoly_sub_sbern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-sub-sbern", poly1, poly2,
                             max_size_t, dpoly_sub_sbern);
}

VISIBLE SCM
scm_dpoly_sub_spower (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-sub-spower", poly1, poly2,
                             max_size_t, dpoly_sub_spower);
}

//----------------------------------------------------------------------
//
// Polynomial negation.

VISIBLE SCM
scm_exact_poly_neg (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-neg", poly, NULL, mpq_poly_neg);
}

VISIBLE SCM
scm_dpoly_neg (SCM poly)
{
  return dpoly_to_poly ("dpoly-neg", poly, NULL, dpoly_neg);
}

//----------------------------------------------------------------------
//
// Polynomial additive identity.

VISIBLE SCM
scm_exact_poly_equalszero_p (SCM poly)
{
  const bool equalszero = exact_poly_to_bool ("exact-poly-equalszero", poly,
                                              mpq_poly_equalszero);
  return scm_from_bool (equalszero);
}

VISIBLE SCM
scm_dpoly_equalszero_p (SCM poly)
{
  const bool equalszero = dpoly_to_bool ("dpoly-equalszero", poly,
                                         dpoly_equalszero);
  return scm_from_bool (equalszero);
}

//----------------------------------------------------------------------
//
// Polynomial minimum degree.

VISIBLE SCM
scm_exact_poly_mindegree_mono (SCM poly)
{
  const size_t mindegree =
    exact_poly_to_size_t ("exact-poly-mindegree-mono", poly,
                          mpq_poly_mindegree_mono);
  return scm_from_size_t (mindegree);
}

VISIBLE SCM
scm_dpoly_mindegree_mono (SCM poly)
{
  const size_t mindegree =
    dpoly_to_size_t ("dpoly-mindegree-mono", poly, dpoly_mindegree_mono);
  return scm_from_size_t (mindegree);
}

VISIBLE SCM
scm_dpoly_mindegree_spower (SCM poly)
{
  const size_t mindegree =
    dpoly_to_size_t ("dpoly-mindegree-spower", poly, dpoly_mindegree_spower);
  return scm_from_size_t (mindegree);
}

//----------------------------------------------------------------------
//
// Raise or lower polynomial degree.

VISIBLE SCM
scm_exact_poly_changedegree_mono (SCM poly, SCM new_degree)
{
  const char *who = "exact-poly-changedegree-mono";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree = scm_to_size_t (new_degree);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t *result_poly_mpq = scm_malloc ((result_degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (result_poly_mpq);
  mpq_dynwind_init_poly (result_degree, result_poly_mpq);

  mpq_poly_changedegree_mono (degree, 1, poly_mpq,
                              result_degree, 1, result_poly_mpq);
  const SCM result = poly_mpq_to_scm (result_degree, result_poly_mpq);

  scm_dynwind_end ();

  return result;
}

VISIBLE SCM
scm_dpoly_changedegree_mono (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-changedegree-mono";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree = scm_to_size_t (new_degree);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *result_poly_d = scm_malloc ((result_degree + 1) * sizeof (double));
  dpoly_changedegree_mono (degree, 1, poly_d, result_degree, 1, result_poly_d);

  return scm_take_f64vector (result_poly_d, result_degree + 1);
}

VISIBLE SCM
scm_dpoly_changedegree_bern (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-changedegree-bern";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree = scm_to_size_t (new_degree);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *result_poly_d = scm_malloc ((result_degree + 1) * sizeof (double));
  dpoly_changedegree_bern (degree, 1, poly_d, result_degree, 1, result_poly_d);

  return scm_take_f64vector (result_poly_d, result_degree + 1);
}

VISIBLE SCM
scm_dpoly_changedegree_sbern (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-changedegree-sbern";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree = scm_to_size_t (new_degree);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *result_poly_d = scm_malloc ((result_degree + 1) * sizeof (double));
  dpoly_changedegree_sbern (degree, 1, poly_d, result_degree, 1, result_poly_d);

  return scm_take_f64vector (result_poly_d, result_degree + 1);
}

VISIBLE SCM
scm_dpoly_changedegree_spower (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-changedegree-spower";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  const size_t result_degree = scm_to_size_t (new_degree);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *result_poly_d = scm_malloc ((result_degree + 1) * sizeof (double));
  dpoly_changedegree_spower (degree, 1, poly_d, result_degree, 1,
                             result_poly_d);

  return scm_take_f64vector (result_poly_d, result_degree + 1);
}

VISIBLE SCM
scm_dpoly_degreechangeerrorbound_mono (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-degreechangeerrorbound-mono";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  const double error_bound =
    dpoly_degreechangeerrorbound_mono (degree, 1, poly_d,
                                       scm_to_size_t (new_degree));
  return scm_from_double (error_bound);
}

VISIBLE SCM
scm_dpoly_degreechangeerrorbound_spower (SCM poly, SCM new_degree)
{
  const char *who = "dpoly-degreechangeerrorbound-spower";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  const double error_bound =
    dpoly_degreechangeerrorbound_spower (degree, 1, poly_d,
                                         scm_to_size_t (new_degree));
  return scm_from_double (error_bound);
}

//----------------------------------------------------------------------
//
// Polynomial composition.

VISIBLE SCM
scm_exact_poly_compose_mono (SCM poly1, SCM poly2)
{
  return exact_poly_poly_to_poly ("exact-poly-compose-mono", poly1, poly2,
                                  mul_size_t, mpq_poly_compose_mono);
}

VISIBLE SCM
scm_dpoly_compose_mono (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-mono", poly1, poly2,
                             mul_size_t, dpoly_compose_mono);
}

VISIBLE SCM
scm_dpoly_compose_berndc (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-berndc", poly1, poly2,
                             mul_size_t, dpoly_compose_berndc);
}

VISIBLE SCM
scm_dpoly_compose_bernh (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-bernh", poly1, poly2,
                             mul_size_t, dpoly_compose_bernh);
}

VISIBLE SCM
scm_dpoly_compose_sberndc (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-sberndc", poly1, poly2,
                             mul_size_t, dpoly_compose_sberndc);
}

VISIBLE SCM
scm_dpoly_compose_sbernh (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-sbernh", poly1, poly2,
                             mul_size_t, dpoly_compose_sbernh);
}

VISIBLE SCM
scm_dpoly_compose_spower (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-compose-spower", poly1, poly2,
                             mul_size_t, dpoly_compose_spower);
}

//----------------------------------------------------------------------
//
// Polynomial division.

VISIBLE SCM
scm_exact_poly_div_mono (SCM poly1, SCM poly2)
{
  const char *who = "exact-poly-div-mono";

  const size_t degree1 = poly_degree (who, SCM_ARG1, poly1);
  const size_t degree2 = poly_degree (who, SCM_ARG2, poly2);

  scm_dynwind_begin (0);

  mpq_t *poly1_mpq = scm_malloc ((degree1 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly1_mpq);
  mpq_dynwind_init_poly (degree1, poly1_mpq);
  poly_scm_to_mpq (degree1, poly1, poly1_mpq);

  mpq_t *poly2_mpq = scm_malloc ((degree2 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly2_mpq);
  mpq_dynwind_init_poly (degree2, poly2_mpq);
  poly_scm_to_mpq (degree2, poly2, poly2_mpq);

  mpq_t *polyq_mpq = scm_malloc ((degree1 + 1) * sizeof (mpq_t));
  scm_dynwind_free (polyq_mpq);
  mpq_dynwind_init_poly (degree1, polyq_mpq);

  mpq_t *polyr_mpq = scm_malloc ((degree1 + 1) * sizeof (mpq_t));
  scm_dynwind_free (polyr_mpq);
  mpq_dynwind_init_poly (degree1, polyr_mpq);

  size_t degreeq;
  size_t degreer;
  bool division_by_zero;
  mpq_poly_div_mono (degree1, 1, poly1_mpq, degree2, 1, poly2_mpq,
                     &degreeq, 1, polyq_mpq, &degreer, 1, polyr_mpq,
                     &division_by_zero);
  if (division_by_zero)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("division by zero")),
        rnrs_make_irritants_condition (scm_list_2 (poly1, poly2))));

  const SCM q = poly_mpq_to_scm (degreeq, polyq_mpq);
  const SCM r = poly_mpq_to_scm (degreer, polyr_mpq);

  scm_dynwind_end ();

  SCM values[2] = { q, r };
  return scm_c_values (values, 2);
}

//----------------------------------------------------------------------
//
// Polynomial gcd.

VISIBLE SCM
scm_exact_poly_gcd_mono (SCM poly1, SCM poly2)
{
  const char *who = "exact-poly-gcd-mono";

  const size_t degree1 = poly_degree (who, SCM_ARG1, poly1);
  const size_t degree2 = poly_degree (who, SCM_ARG2, poly2);
  const size_t degree_max = szmax (degree1, degree2);

  scm_dynwind_begin (0);

  mpq_t *poly1_mpq = scm_malloc ((degree1 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly1_mpq);
  mpq_dynwind_init_poly (degree1, poly1_mpq);
  poly_scm_to_mpq (degree1, poly1, poly1_mpq);

  mpq_t *poly2_mpq = scm_malloc ((degree2 + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly2_mpq);
  mpq_dynwind_init_poly (degree2, poly2_mpq);
  poly_scm_to_mpq (degree2, poly2, poly2_mpq);

  mpq_t *poly3_mpq = scm_malloc ((degree_max + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly3_mpq);
  mpq_dynwind_init_poly (degree_max, poly3_mpq);

  size_t degree3;
  mpq_poly_gcd_mono (degree1, 1, poly1_mpq, degree2, 1, poly2_mpq, &degree3, 1,
                     poly3_mpq);
  const SCM poly3 = poly_mpq_to_scm (degree3, poly3_mpq);

  scm_dynwind_end ();

  return poly3;
}

//----------------------------------------------------------------------
//
// Square-free decomposition of a polynomial.

static void
handler_for_clear_polynomial_list (void *decomp)
{
  clear_polynomial_list (decomp);
}

VISIBLE SCM
scm_exact_poly_squarefree_mono (SCM poly)
{
  const char *who = "exact-poly-squarefree-mono";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  polynomial_list_entry_t *decomp;
  mpq_t lead_coef;
  mpq_init (lead_coef);
  scm_dynwind_mpq_clear (lead_coef);
  mpq_poly_squarefree_mono (degree, 1, poly_mpq, &decomp, lead_coef);
  scm_dynwind_unwind_handler (handler_for_clear_polynomial_list, decomp,
                              SCM_F_WIND_EXPLICITLY);

  const SCM lead_coef_scm = scm_from_mpq (lead_coef);

  SCM vec = scm_c_make_vector (polynomial_list_length (decomp), SCM_UNDEFINED);
  size_t i = 0;
  for (polynomial_list_entry_t *p = decomp; p != NULL; p = p->next)
    {
      SCM_SIMPLE_VECTOR_SET (vec, i, poly_mpq_to_scm (p->degree, p->poly));
      i++;
    }

  scm_dynwind_end ();

  SCM values[2] = { vec, lead_coef_scm };
  return scm_c_values (values, 2);
}

//----------------------------------------------------------------------
//
// Polynomial formal differentiation.

VISIBLE SCM
scm_exact_poly_deriv_mono (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-deriv-mono", poly,
                             derivative_degree, mpq_poly_deriv_mono);
}

VISIBLE SCM
scm_dpoly_deriv_mono (SCM poly)
{
  return dpoly_to_poly ("dpoly-deriv-mono", poly,
                        derivative_degree, dpoly_deriv_mono);
}

VISIBLE SCM
scm_dpoly_deriv_bern (SCM poly)
{
  return dpoly_to_poly ("dpoly-deriv-bern", poly,
                        derivative_degree, dpoly_deriv_bern);
}

VISIBLE SCM
scm_dpoly_deriv_sbern (SCM poly)
{
  return dpoly_to_poly ("dpoly-deriv-sbern", poly,
                        derivative_degree, dpoly_deriv_sbern);
}

VISIBLE SCM
scm_dpoly_deriv_spower (SCM poly)
{
  return dpoly_to_poly ("dpoly-deriv-spower", poly,
                        derivative_degree, dpoly_deriv_spower);
}

//----------------------------------------------------------------------
//
// Critical points of univariate polynomials.

VISIBLE SCM
scm_poly_criticalpoints_mono (SCM poly, SCM a, SCM b)
{
  const char *who = "poly-criticalpoints-mono";

  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t a_mpq;
  mpq_init (a_mpq);
  scm_dynwind_mpq_clear (a_mpq);
  if (SCM_UNBNDP (a))
    mpq_set_ui (a_mpq, 0, 1);   // a ← 0
  else
    scm_to_mpq (a, a_mpq);

  mpq_t b_mpq;
  mpq_init (b_mpq);
  scm_dynwind_mpq_clear (b_mpq);
  if (SCM_UNBNDP (b))
    mpq_set_ui (b_mpq, 1, 1);   // b ← 1
  else
    scm_to_mpq (b, b_mpq);

  size_t num_points;
  double *t_data = scm_malloc (szmax (degree, 1) * sizeof (double));
  poly_criticalpoints_mono (degree, 1, poly_mpq, a_mpq, b_mpq,
                            &num_points, t_data);

  scm_dynwind_end ();

  t_data = scm_realloc (t_data, szmax (num_points, 1) * sizeof (double));
  return scm_take_f64vector (t_data, num_points);
}

static SCM
_scm_dpoly_criticalpoints (const char *who,
                           void (*criticalpoints) (size_t degree,
                                                   ssize_t, const double *,
                                                   double, double, size_t *,
                                                   double t[degree - 1]),
                           SCM poly, SCM a, SCM b)
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  const double a_d = (SCM_UNBNDP (a)) ? 0.0 : (scm_to_double (a));
  const double b_d = (SCM_UNBNDP (b)) ? 1.0 : (scm_to_double (b));

  size_t num_points;
  double *t_data = scm_malloc (szmax (degree, 1) * sizeof (double));
  criticalpoints (degree, 1, poly_d, a_d, b_d, &num_points, t_data);

  t_data = scm_realloc (t_data, szmax (num_points, 1) * sizeof (double));
  return scm_take_f64vector (t_data, num_points);
}

VISIBLE SCM
scm_dpoly_criticalpoints_bernsv (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_criticalpoints ("dpoly-criticalpoints-bernsv",
                                    dpoly_criticalpoints_bernsv, poly, a, b);
}

VISIBLE SCM
scm_dpoly_criticalpoints_berndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_criticalpoints ("dpoly-criticalpoints-berndc",
                                    dpoly_criticalpoints_berndc, poly, a, b);
}

VISIBLE SCM
scm_dpoly_criticalpoints_sbernsv (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_criticalpoints ("dpoly-criticalpoints-sbernsv",
                                    dpoly_criticalpoints_sbernsv, poly, a, b);
}

VISIBLE SCM
scm_dpoly_criticalpoints_sberndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_criticalpoints ("dpoly-criticalpoints-sberndc",
                                    dpoly_criticalpoints_sberndc, poly, a, b);
}

VISIBLE SCM
scm_dpoly_criticalpoints_spower (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_criticalpoints ("dpoly-criticalpoints-spower",
                                    dpoly_criticalpoints_spower, poly, a, b);
}

//----------------------------------------------------------------------
//
// Polynomial evaluation.

VISIBLE SCM
scm_exact_poly_eval_mono (SCM poly, SCM t)
{
  const char *who = "exact-poly-eval-mono";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t t_mpq;
  mpq_init (t_mpq);
  scm_dynwind_mpq_clear (t_mpq);
  scm_to_mpq (scm_inexact_to_exact (t), t_mpq);

  mpq_t value_mpq;
  mpq_init (value_mpq);
  mpq_poly_eval_mono (degree, 1, poly_mpq, t_mpq, value_mpq);
  const SCM value = scm_from_mpq (value_mpq);
  mpq_clear (value_mpq);

  scm_dynwind_end ();

  return value;
}

VISIBLE SCM
scm_dpoly_eval_mono (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-mono";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_mono (degree, 1, poly_d, scm_to_double (t)));
}

VISIBLE SCM
scm_dpoly_eval_sbernsv (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-sbern-schumaker-volk";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_sbernsv (degree, 1, poly_d, scm_to_double (t)));
}

VISIBLE SCM
scm_dpoly_eval_sberndc (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-sbern-de-casteljau";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_sberndc (degree, 1, poly_d, scm_to_double (t)));
}

VISIBLE SCM
scm_dpoly_eval_bernsv (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-bern-schumaker-volk";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_bernsv (degree, 1, poly_d, scm_to_double (t)));
}

VISIBLE SCM
scm_dpoly_eval_berndc (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-bern-de-casteljau";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_berndc (degree, 1, poly_d, scm_to_double (t)));
}


VISIBLE SCM
scm_dpoly_eval_spower (SCM poly, SCM t)
{
  const char *who = "dpoly-eval-spower";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);
  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);
  return
    scm_from_double (dpoly_eval_spower (degree, 1, poly_d, scm_to_double (t)));
}

//----------------------------------------------------------------------
//
// Polynomial multiplication.

VISIBLE SCM
scm_exact_poly_mul_mono (SCM poly1, SCM poly2)
{
  return exact_poly_poly_to_poly ("exact-poly-mul-mono", poly1, poly2,
                                  add_size_t, mpq_poly_mul_mono);
}

VISIBLE SCM
scm_dpoly_mul_mono (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-mul-mono", poly1, poly2,
                             add_size_t, dpoly_mul_mono);
}

VISIBLE SCM
scm_dpoly_mul_bern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-mul-bern", poly1, poly2,
                             add_size_t, dpoly_mul_bern);
}

VISIBLE SCM
scm_dpoly_mul_sbern (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-mul-sbern", poly1, poly2,
                             add_size_t, dpoly_mul_sbern);
}

VISIBLE SCM
scm_dpoly_mul_spower (SCM poly1, SCM poly2)
{
  return dpoly_poly_to_poly ("dpoly-mul-spower", poly1, poly2,
                             add_size_t, dpoly_mul_spower);
}

//----------------------------------------------------------------------
//
// Scalar multiplication of a polynomial.

VISIBLE SCM
scm_exact_poly_scalarmul (SCM poly, SCM r)
{
  const size_t degree = poly_degree ("exact-poly-scalarmul", SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t r_mpq;
  mpq_init (r_mpq);
  scm_dynwind_mpq_clear (r_mpq);
  scm_to_mpq (scm_inexact_to_exact (r), r_mpq);

  mpq_poly_scalarmul (degree, 1, poly_mpq, r_mpq, 1, poly_mpq);
  const SCM result = poly_mpq_to_scm (degree, poly_mpq);

  scm_dynwind_end ();

  return result;
}

VISIBLE SCM
scm_dpoly_scalarmul (SCM poly, SCM r)
{
  const size_t degree = poly_degree ("exact-poly-scalarmul", SCM_ARG1, poly);

  const double r_d = scm_to_double (r);

  // Use separate buffers for input and result, so the input can be
  // collected in case of a Guile exception.

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *result = scm_malloc ((degree + 1) * sizeof (double));

  dpoly_scalarmul (degree, 1, poly_d, r_d, 1, result);

  return scm_take_f64vector (result, degree + 1);
}

//----------------------------------------------------------------------
//
// Polynomial multiplicative identity.

VISIBLE SCM
scm_exact_poly_equalsone_mono_p (SCM poly)
{
  const bool equalsone = exact_poly_to_bool ("exact-poly-equalsone-mono", poly,
                                             mpq_poly_equalsone_mono);
  return scm_from_bool (equalsone);
}

VISIBLE SCM
scm_dpoly_equalsone_mono_p (SCM poly)
{
  const bool equalsone = dpoly_to_bool ("dpoly-equalsone-mono", poly,
                                        dpoly_equalsone_mono);
  return scm_from_bool (equalsone);
}

VISIBLE SCM
scm_dpoly_equalsone_bern_p (SCM poly)
{
  const bool equalsone = dpoly_to_bool ("dpoly-equalsone-bern", poly,
                                        dpoly_equalsone_bern);
  return scm_from_bool (equalsone);
}

VISIBLE SCM
scm_dpoly_equalsone_sbern_p (SCM poly)
{
  const bool equalsone = dpoly_to_bool ("dpoly-equalsone-sbern", poly,
                                        dpoly_equalsone_sbern);
  return scm_from_bool (equalsone);
}

VISIBLE SCM
scm_dpoly_equalsone_spower_p (SCM poly)
{
  const bool equalsone = dpoly_to_bool ("dpoly-equalsone-spower", poly,
                                        dpoly_equalsone_spower);
  return scm_from_bool (equalsone);
}

//----------------------------------------------------------------------
//
// Polynomial cross products.

static SCM
_scm_dpoly_crossproduct (const char *who,
                         void (*crossproduct) (size_t, ssize_t, const double *,
                                               ssize_t, const double *,
                                               size_t, ssize_t, const double *,
                                               ssize_t, const double *,
                                               ssize_t, double *),
                         SCM ax, SCM ay, SCM bx, SCM by)
{
  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);
  const size_t degree_b =
    _parametric_poly_degree (poly_degree (who, SCM_ARG3, bx),
                             poly_degree (who, SCM_ARG4, by),
                             who, bx, by);

  double ax_[degree_a + 1];
  double ay_[degree_a + 1];
  double bx_[degree_b + 1];
  double by_[degree_b + 1];

  poly_scm_to_double (degree_a, ax, ax_);
  poly_scm_to_double (degree_a, ay, ay_);
  poly_scm_to_double (degree_b, bx, bx_);
  poly_scm_to_double (degree_b, by, by_);

  double *cross_ = scm_malloc ((degree_a + degree_b + 1) * sizeof (double));
  crossproduct (degree_a, 1, ax_, 1, ay_, degree_b, 1, bx_, 1, by_, 1, cross_);

  return scm_take_f64vector (cross_, degree_a + degree_b + 1);
}

VISIBLE SCM
scm_dpoly_crossproduct_mono (SCM ax, SCM ay, SCM bx, SCM by)
{
  return _scm_dpoly_crossproduct ("dpoly-crossproduct-mono",
                                  dpoly_crossproduct_mono, ax, ay, bx, by);
}

VISIBLE SCM
scm_dpoly_crossproduct_bern (SCM ax, SCM ay, SCM bx, SCM by)
{
  return _scm_dpoly_crossproduct ("dpoly-crossproduct-bern",
                                  dpoly_crossproduct_bern, ax, ay, bx, by);
}

VISIBLE SCM
scm_dpoly_crossproduct_sbern (SCM ax, SCM ay, SCM bx, SCM by)
{
  return _scm_dpoly_crossproduct ("dpoly-crossproduct-sbern",
                                  dpoly_crossproduct_sbern, ax, ay, bx, by);
}

VISIBLE SCM
scm_dpoly_crossproduct_spower (SCM ax, SCM ay, SCM bx, SCM by)
{
  return _scm_dpoly_crossproduct ("dpoly-crossproduct-spower",
                                  dpoly_crossproduct_spower, ax, ay, bx, by);
}

//----------------------------------------------------------------------
//
// Detection of inflections and cusps in polynomial splines.

static SCM
_scm_dpoly_inflections (const char *who,
                        void (*find_inflections)
                        (size_t degree_a, ssize_t stride_ax, const double *ax,
                         ssize_t stride_ay, const double *ay,
                         size_t *num_inflections,
                         double inflections
                         [(degree_a <= 2) ? 0 : 2 * degree_a - 4]),
                        SCM ax, SCM ay)
{
  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);

  SCM inflections;

  if (degree_a <= 2)
    inflections = scm_make_f64vector (scm_from_int (0), SCM_UNSPECIFIED);
  else
    {
      double ax_[degree_a + 1];
      poly_scm_to_double (degree_a, ax, ax_);

      double ay_[degree_a + 1];
      poly_scm_to_double (degree_a, ay, ay_);

      size_t num_inflections;
      double inflections_[2 * degree_a - 4];
      find_inflections (degree_a, 1, ax_, 1, ay_,
                        &num_inflections, inflections_);

      if (num_inflections == 0)
        inflections = scm_make_f64vector (scm_from_int (0), SCM_UNSPECIFIED);
      else
        {
          double *inflections_data =
            scm_malloc (szmax (num_inflections, 1) * sizeof (double));
          memcpy (inflections_data, inflections_,
                  num_inflections * sizeof (double));
          inflections = scm_take_f64vector (inflections_data, num_inflections);
        }
    }

  return inflections;
}

VISIBLE SCM
scm_dpoly_inflections_mono (SCM ax, SCM ay)
{
  return _scm_dpoly_inflections ("dpoly-inflections-mono",
                                 dpoly_inflections_mono, ax, ay);
}

VISIBLE SCM
scm_dpoly_inflections_bern (SCM ax, SCM ay)
{
  return _scm_dpoly_inflections ("dpoly-inflections-bern",
                                 dpoly_inflections_bern, ax, ay);
}

VISIBLE SCM
scm_dpoly_inflections_sbern (SCM ax, SCM ay)
{
  return _scm_dpoly_inflections ("dpoly-inflections-sbern",
                                 dpoly_inflections_sbern, ax, ay);
}

VISIBLE SCM
scm_dpoly_inflections_spower (SCM ax, SCM ay)
{
  return _scm_dpoly_inflections ("dpoly-inflections-spower",
                                 dpoly_inflections_spower, ax, ay);
}

//----------------------------------------------------------------------
//
// Flatten a polynomial spline in the plane, yielding a piecewise
// curve of line segments.

static SCM
_scm_dpoly_flatten (const char *who,
                    void (*flatten) (size_t, ssize_t, const double *,
                                     ssize_t, const double *,
                                     double, double, size_t,
                                     size_t *, double *, double *, double *,
                                     int *),
                    SCM ax, SCM ay, SCM absolute_tolerance,
                    SCM relative_tolerance, SCM max_num_points)
{
  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);

  double ax_[degree_a + 1];
  poly_scm_to_double (degree_a, ax, ax_);

  double ay_[degree_a + 1];
  poly_scm_to_double (degree_a, ay, ay_);

  const double abs_tol = scm_to_double (absolute_tolerance);
  const double rel_tol = scm_to_double (relative_tolerance);
  const size_t max_num_pts = scm_to_size_t (max_num_points);

  size_t num_pts;
  double *x_ = scm_malloc (szmax (max_num_pts, 1) * sizeof (double));
  double *y_ = scm_malloc (szmax (max_num_pts, 1) * sizeof (double));
  double *t_ = scm_malloc (szmax (max_num_pts, 1) * sizeof (double));
  int info;
  flatten (degree_a, 1, ax_, 1, ay_, abs_tol, rel_tol, max_num_pts,
           &num_pts, x_, y_, t_, &info);

  if (info != 0)
    {
      free (x_);
      free (y_);
      free (t_);

      switch (info)
        {
        case SMCORE__FLATTENING_MAX_NUM_POINTS_EXCEEDED:
          rnrs_raise_condition
            (scm_list_4
             (rnrs_make_error (),
              rnrs_c_make_who_condition (who),
              rnrs_c_make_message_condition
              (_("maximum number of points exceeded")),
              rnrs_make_irritants_condition
              (scm_list_5 (ax, ay, absolute_tolerance, relative_tolerance,
                           max_num_points))));
          break;
        default:
          rnrs_raise_condition
            (scm_list_4
             (rnrs_make_error (),
              rnrs_c_make_who_condition (who),
              rnrs_c_make_message_condition
              ("unspecified error (SOMEONE PLEASE FIX THIS)"),
              rnrs_make_irritants_condition
              (scm_list_5 (ax, ay, absolute_tolerance, relative_tolerance,
                           max_num_points))));
          break;
        }
    }

  SCM values[3] = {
    scm_take_f64vector (x_, num_pts),
    scm_take_f64vector (y_, num_pts),
    scm_take_f64vector (t_, num_pts)
  };
  return scm_c_values (values, 3);
}

VISIBLE SCM
scm_dpoly_flatten_mono (SCM ax, SCM ay, SCM absolute_tolerance,
                        SCM relative_tolerance, SCM max_num_points)
{
  return _scm_dpoly_flatten ("dpoly-flatten-mono", dpoly_flatten_mono,
                             ax, ay, absolute_tolerance, relative_tolerance,
                             max_num_points);
}

VISIBLE SCM
scm_dpoly_flatten_bern (SCM ax, SCM ay, SCM absolute_tolerance,
                        SCM relative_tolerance, SCM max_num_points)
{
  return _scm_dpoly_flatten ("dpoly-flatten-bern", dpoly_flatten_bern,
                             ax, ay, absolute_tolerance, relative_tolerance,
                             max_num_points);
}

VISIBLE SCM
scm_dpoly_flatten_sbern (SCM ax, SCM ay, SCM absolute_tolerance,
                         SCM relative_tolerance, SCM max_num_points)
{
  return _scm_dpoly_flatten ("dpoly-flatten-sbern", dpoly_flatten_sbern,
                             ax, ay, absolute_tolerance, relative_tolerance,
                             max_num_points);
}

VISIBLE SCM
scm_dpoly_flatten_spower (SCM ax, SCM ay, SCM absolute_tolerance,
                          SCM relative_tolerance, SCM max_num_points)
{
  return _scm_dpoly_flatten ("dpoly-flatten-spower", dpoly_flatten_spower,
                             ax, ay, absolute_tolerance, relative_tolerance,
                             max_num_points);
}

//----------------------------------------------------------------------
//
// The instantaneous direction of a polynomial spline.

static SCM
_scm_dpoly_direction (const char *who,
                      double complex (*direction) (size_t degree_a,
                                                   ssize_t stride_ax,
                                                   const double *ax,
                                                   ssize_t stride_ay,
                                                   const double *ay, double t),
                      SCM ax, SCM ay, SCM t)
{
  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);

  double ax_[degree_a + 1];
  poly_scm_to_double (degree_a, ax, ax_);

  double ay_[degree_a + 1];
  poly_scm_to_double (degree_a, ay, ay_);

  return scm_from_double_complex (direction (degree_a, 1, ax_, 1, ay_,
                                             scm_to_double (t)));
}

VISIBLE SCM
scm_dpoly_direction_bernsv (SCM ax, SCM ay, SCM t)
{
  return _scm_dpoly_direction ("dpoly-direction-bernsv",
                               dpoly_direction_bernsv, ax, ay, t);
}

VISIBLE SCM
scm_dpoly_direction_berndc (SCM ax, SCM ay, SCM t)
{
  return _scm_dpoly_direction ("dpoly-direction-berndc",
                               dpoly_direction_berndc, ax, ay, t);
}

VISIBLE SCM
scm_dpoly_direction_sbernsv (SCM ax, SCM ay, SCM t)
{
  return _scm_dpoly_direction ("dpoly-direction-sbernsv",
                               dpoly_direction_sbernsv, ax, ay, t);
}

VISIBLE SCM
scm_dpoly_direction_sberndc (SCM ax, SCM ay, SCM t)
{
  return _scm_dpoly_direction ("dpoly-direction-sberndc",
                               dpoly_direction_sberndc, ax, ay, t);
}

VISIBLE SCM
scm_dpoly_direction_spower (SCM ax, SCM ay, SCM t)
{
  return _scm_dpoly_direction ("dpoly-direction-spower",
                               dpoly_direction_spower, ax, ay, t);
}

//----------------------------------------------------------------------
//
// Polynomial subdivision.

VISIBLE SCM
scm_exact_poly_portion_mono (SCM poly, SCM a, SCM b)
{
  const char *who = "exact-poly-portion-mono";
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  mpq_t a_mpq;
  mpq_init (a_mpq);
  scm_dynwind_mpq_clear (a_mpq);
  scm_to_mpq (scm_inexact_to_exact (a), a_mpq);

  mpq_t b_mpq;
  mpq_init (b_mpq);
  scm_dynwind_mpq_clear (b_mpq);
  scm_to_mpq (scm_inexact_to_exact (b), b_mpq);

  mpq_t *poly3_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly3_mpq);
  mpq_dynwind_init_poly (degree, poly3_mpq);

  mpq_poly_portion_mono (degree, 1, poly_mpq, a_mpq, b_mpq, 1, poly3_mpq);
  const SCM poly3 = poly_mpq_to_scm (degree, poly3_mpq);

  scm_dynwind_end ();

  return poly3;
}

static SCM
_scm_dpoly_portion (SCM poly, SCM a, SCM b, const char *who,
                    void (*portion) (size_t degree, ssize_t stride,
                                     const double *poly,
                                     double a, double b,
                                     ssize_t result_stride, double *result))
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  const double a_ = scm_to_double (a);
  const double b_ = scm_to_double (b);

  double *poly3_d = scm_malloc ((degree + 1) * sizeof (double));
  portion (degree, 1, poly_d, a_, b_, 1, poly3_d);

  return scm_take_f64vector (poly3_d, degree + 1);
}

VISIBLE SCM
scm_dpoly_portion_mono (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-mono",
                             dpoly_portion_mono);
}

VISIBLE SCM
scm_dpoly_portion_berndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-berndc",
                             dpoly_portion_berndc);
}

VISIBLE SCM
scm_dpoly_portion_bernh (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-bernh",
                             dpoly_portion_bernh);
}

VISIBLE SCM
scm_dpoly_portion_sberndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-sberndc",
                             dpoly_portion_sberndc);
}

VISIBLE SCM
scm_dpoly_portion_sbernh (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-sbernh",
                             dpoly_portion_sbernh);
}

VISIBLE SCM
scm_dpoly_portion_spower (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_portion (poly, a, b, "dpoly-portion-spower",
                             dpoly_portion_spower);
}

static SCM
_scm_dpoly_subdiv (SCM poly, SCM t, const char *who,
                   void (*subdiv) (size_t degree, ssize_t stride,
                                   const double *poly, double t,
                                   ssize_t stride_a, double *a,
                                   ssize_t stride_b, double *b))
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  const double t_ = scm_to_double (t);

  double *a_data = scm_malloc ((degree + 1) * sizeof (double));
  SCM a = scm_take_f64vector (a_data, degree + 1);
  double *b_data = scm_malloc ((degree + 1) * sizeof (double));
  SCM b = scm_take_f64vector (b_data, degree + 1);
  subdiv (degree, 1, poly_d, t_, 1, a_data, 1, b_data);

  SCM values[2] = { a, b };
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_dpoly_subdiv_bern (SCM poly, SCM t)
{
  return _scm_dpoly_subdiv (poly, t, "dpoly-subdiv-bern", dpoly_subdiv_bern);
}

VISIBLE SCM
scm_dpoly_subdiv_sbern (SCM poly, SCM t)
{
  return _scm_dpoly_subdiv (poly, t, "dpoly-subdiv-sbern", dpoly_subdiv_sbern);
}

//----------------------------------------------------------------------
//
// Polynomial roots.

VISIBLE SCM
scm_budan_0_1 (SCM poly)
{
  const size_t degree = poly_degree ("budan-0_1", SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);
  const size_t result = mpq_budan_0_1 (degree, 1, poly_mpq);

  scm_dynwind_end ();

  return scm_from_size_t (result);
}

VISIBLE SCM
scm_isolate_roots_of_square_free_polynomial (SCM poly, SCM a, SCM b)
{
  const size_t degree = poly_degree ("isolate-roots-of-square-free-polynomial",
                                     SCM_ARG1, poly);

  scm_dynwind_begin (0);

  mpq_t a_mpq;
  mpq_init (a_mpq);
  scm_dynwind_mpq_clear (a_mpq);
  scm_to_mpq (scm_inexact_to_exact (a), a_mpq);

  mpq_t b_mpq;
  mpq_init (b_mpq);
  scm_dynwind_mpq_clear (b_mpq);
  scm_to_mpq (scm_inexact_to_exact (b), b_mpq);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, poly, poly_mpq);

  polynomial_root_interval_t *intervals =
    isolate_roots_of_square_free_polynomial (degree, 1, poly_mpq, a_mpq, b_mpq);

  SCM vec = scm_c_make_vector (polynomial_root_interval_list_length (intervals),
                               SCM_UNDEFINED);
  size_t i = 0;
  for (polynomial_root_interval_t *p = intervals; p != NULL; p = p->next)
    {
      const SCM t1 = scm_from_mpq (p->a);
      if (p->root_is_exact)
        SCM_SIMPLE_VECTOR_SET (vec, i, t1);
      else
        {
          const SCM t2 = scm_from_mpq (p->b);
          SCM_SIMPLE_VECTOR_SET (vec, i, scm_cons (t1, t2));
        }
      i++;
    }

  clear_polynomial_root_interval_list (intervals);

  scm_dynwind_end ();

  return vec;
}

static double
call_dfunc (double x, void *func_p)
{
  SCM func = *(SCM *) func_p;
  return scm_to_double (scm_call_1 (func, scm_from_double (x)));
}

VISIBLE SCM
scm_dpoly_findroots (SCM mono_poly, SCM a, SCM b, SCM eval_poly)
{
  const char *who = "dpoly-findroots";

  const size_t degree = poly_degree (who, SCM_ARG1, mono_poly);

  scm_dynwind_begin (0);

  mpq_t *poly_mpq = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_mpq);
  mpq_dynwind_init_poly (degree, poly_mpq);
  poly_scm_to_mpq (degree, mono_poly, poly_mpq);

  mpq_t a_mpq;
  mpq_init (a_mpq);
  scm_dynwind_mpq_clear (a_mpq);
  scm_to_mpq (scm_inexact_to_exact (a), a_mpq);

  mpq_t b_mpq;
  mpq_init (b_mpq);
  scm_dynwind_mpq_clear (b_mpq);
  scm_to_mpq (scm_inexact_to_exact (b), b_mpq);

  //
  // FIXME FIXME FIXME FIXME FIXME FIXME FIXME: Support eval_poly.
  //
  size_t root_count;
  double *roots = scm_malloc (szmax (degree, 1) * sizeof (double));
  scm_dynwind_free (roots);
  size_t *multiplicities = scm_malloc (szmax (degree, 1) * sizeof (size_t));
  scm_dynwind_free (multiplicities);
  bool identically_zero;
  if (unbndp_or_false (eval_poly))
    dpoly_findroots (degree, 1, poly_mpq, a_mpq, b_mpq, NULL, NULL,
                     &identically_zero, &root_count, roots, multiplicities);
  else
    dpoly_findroots (degree, 1, poly_mpq, a_mpq, b_mpq, call_dfunc, &eval_poly,
                     &identically_zero, &root_count, roots, multiplicities);
  if (identically_zero)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("the polynomial is identically zero")),
        rnrs_make_irritants_condition (scm_list_1 (mono_poly))));

  double *roots_data = scm_malloc (szmax (root_count, 1) * sizeof (double));
  memcpy (roots_data, roots, root_count * sizeof (double));
  SCM roots_scm = scm_take_f64vector (roots_data, root_count);

  SCM multiplicities_scm = scm_c_make_vector (root_count, SCM_UNDEFINED);
  for (size_t i = 0; i < root_count; i++)
    SCM_SIMPLE_VECTOR_SET (multiplicities_scm, i,
                           scm_from_size_t (multiplicities[i]));

  scm_dynwind_end ();

  SCM values[2] = { roots_scm, multiplicities_scm };
  return scm_c_values (values, 2);
}

static SCM
_scm_dpoly_findroots_in_some_basis (SCM poly, SCM a, SCM b,
                                    const char *who,
                                    void (*findroots)
                                    (size_t degree, ssize_t stride,
                                     const double *poly, double a, double b,
                                     bool *identically_zero,
                                     size_t *root_count, double *roots))
{
  const size_t degree = poly_degree (who, SCM_ARG1, poly);

  double poly_d[degree + 1];
  poly_scm_to_double (degree, poly, poly_d);

  double *roots = scm_malloc (szmax (degree, 1) * sizeof (double));

  bool identically_zero;
  size_t root_count;
  findroots (degree, 1, poly_d, scm_to_double (a), scm_to_double (b),
             &identically_zero, &root_count, roots);

  if (identically_zero)
    {
      free (roots);
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          (_("the polynomial is identically zero")),
          rnrs_make_irritants_condition (scm_list_1 (poly))));
    }

  return scm_take_f64vector (scm_realloc (roots, root_count * sizeof (double)),
                             root_count);
}

VISIBLE SCM
scm_dpoly_findroots_bernsv (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_findroots_in_some_basis (poly, a, b,
                                             "dpoly-findroots-bernsv",
                                             dpoly_findroots_bernsv);
}

VISIBLE SCM
scm_dpoly_findroots_berndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_findroots_in_some_basis (poly, a, b,
                                             "dpoly-findroots-berndc",
                                             dpoly_findroots_berndc);
}

VISIBLE SCM
scm_dpoly_findroots_sbernsv (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_findroots_in_some_basis (poly, a, b,
                                             "dpoly-findroots-sbernsv",
                                             dpoly_findroots_sbernsv);
}

VISIBLE SCM
scm_dpoly_findroots_sberndc (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_findroots_in_some_basis (poly, a, b,
                                             "dpoly-findroots-sberndc",
                                             dpoly_findroots_sberndc);
}

VISIBLE SCM
scm_dpoly_findroots_spower (SCM poly, SCM a, SCM b)
{
  return _scm_dpoly_findroots_in_some_basis (poly, a, b,
                                             "dpoly-findroots-spower",
                                             dpoly_findroots_spower);
}

//----------------------------------------------------------------------

VISIBLE SCM
scm_exact_poly_implicitize_mono (SCM a, SCM b)
{
  const char *who = "exact-poly-implicitize-mono";

  const size_t degree = _parametric_poly_degree (poly_degree (who, SCM_ARG1, a),
                                                 poly_degree (who, SCM_ARG2, b),
                                                 who, a, b);

  scm_dynwind_begin (0);

  mpq_t *a_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (a_);
  mpq_dynwind_init_poly (degree, a_);
  poly_scm_to_mpq (degree, a, a_);

  mpq_t *b_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (b_);
  mpq_dynwind_init_poly (degree, b_);
  poly_scm_to_mpq (degree, b, b_);

  mpq_t result_[degree + 1][degree + 1];
  mpq_dynwind_init_bipoly (degree, result_);

  int info;
  mpq_poly_implicitize_mono (degree, 1, a_, 1, b_, result_, &info);
  if (info == -1)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("this polynomial degree is not supported")),
        rnrs_make_irritants_condition (scm_list_1 (scm_from_size_t (degree)))));
  if (info != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("an error occurred during implicitization")),
        rnrs_make_irritants_condition (scm_list_2 (a, b))));

  SCM size = scm_from_size_t (degree + 1);
  SCM result = scm_make_array (scm_from_int (0), scm_list_2 (size, size));
  scm_t_array_handle handle;
  scm_array_get_handle (result, &handle);
  scm_dynwind_array_handle_release (&handle);

  SCM *elems = scm_array_handle_writable_elements (&handle);
  for (ssize_t i = 0; i <= degree; i++)
    for (size_t j = 0; j <= degree - i; j++)
      elems[i * (degree + 1) + j] = scm_from_mpq (result_[i][j]);

  scm_dynwind_end ();

  return result;
}

// FIXME: This is reusable.
static size_t
bipoly_degree (const char *who, SCM bipoly, unsigned int arg_pos,
               scm_t_array_handle *handlep)
{
  const scm_t_array_dim *dims = scm_array_handle_dims (handlep);
  SCM_ASSERT_TYPE ((scm_array_handle_rank (handlep) == 2
                    && (dims[0].ubnd - dims[0].lbnd
                        == dims[1].ubnd - dims[1].lbnd)),
                   bipoly, arg_pos, who, _("square rank-2 array"));
  return (size_t) (dims[0].ubnd - dims[0].lbnd);
}

VISIBLE SCM
scm_exact_poly_plugintoimplicit_mono (SCM implicit_eq, SCM x, SCM y)
{
  const char *who = "exact-poly-plugintoimplicit-mono";

  const size_t degree = _parametric_poly_degree (poly_degree (who, SCM_ARG2, x),
                                                 poly_degree (who, SCM_ARG3, y),
                                                 who, x, y);

  scm_dynwind_begin (0);

  scm_t_array_handle handle_eq;
  scm_array_get_handle (implicit_eq, &handle_eq);
  scm_dynwind_array_handle_release (&handle_eq);

  const size_t degree_eq =
    bipoly_degree (who, implicit_eq, SCM_ARG1, &handle_eq);
  const SCM *elems = scm_array_handle_elements (&handle_eq);
  ssize_t stridex = scm_array_handle_dims (&handle_eq)[0].inc;
  ssize_t stridey = scm_array_handle_dims (&handle_eq)[1].inc;
  mpq_t (*eq_)[degree_eq + 1] =
    scm_malloc ((degree_eq + 1) * (degree_eq + 1) * sizeof (mpq_t));
  scm_dynwind_free (eq_);
  mpq_dynwind_init_bipoly (degree_eq, eq_);
  for (ssize_t i = 0; i <= degree_eq; i++)
    for (ssize_t j = 0; j <= degree_eq - i; j++)
      scm_to_mpq (scm_inexact_to_exact (elems[i * stridex + j * stridey]),
                  eq_[i][j]);

  mpq_t *x_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (x_);
  mpq_dynwind_init_poly (degree, x_);
  poly_scm_to_mpq (degree, x, x_);

  mpq_t *y_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (y_);
  mpq_dynwind_init_poly (degree, y_);
  poly_scm_to_mpq (degree, y, y_);

  mpq_t *result_ = scm_malloc ((degree * degree_eq + 1) * sizeof (mpq_t));
  scm_dynwind_free (result_);
  mpq_dynwind_init_poly (degree * degree_eq, result_);

  mpq_poly_plugintoimplicit_mono (degree_eq, eq_, degree, 1, x_, 1, y_,
                                  1, result_);

  const SCM result = poly_mpq_to_scm (degree * degree_eq, result_);

  scm_dynwind_end ();

  return result;
}

static SCM
_scm_dpoly_implicitize (const char *who,
                        void (*implicitize)
                        (size_t degree,
                         ssize_t stride_x, const double *x,
                         ssize_t stride_y, const double *y,
                         double implicit_equation[degree + 1][degree + 1],
                         int *info), SCM a, SCM b)
{
  const size_t degree = _parametric_poly_degree (poly_degree (who, SCM_ARG1, a),
                                                 poly_degree (who, SCM_ARG2, b),
                                                 who, a, b);

  double a_[degree + 1];
  poly_scm_to_double (degree, a, a_);

  double b_[degree + 1];
  poly_scm_to_double (degree, b, b_);

  SCM size = scm_from_size_t (degree + 1);
  SCM result = scm_make_typed_array (_symbol_f64 (), SCM_UNSPECIFIED,
                                     scm_list_2 (size, size));
  scm_t_array_handle handle;
  scm_array_get_handle (result, &handle);

  double (*result_)[degree + 1] =
    (double (*)[degree + 1]) scm_array_handle_f64_writable_elements (&handle);
  int info;
  implicitize (degree, 1, a_, 1, b_, result_, &info);

  scm_array_handle_release (&handle);

  switch (info)
    {
    case 0:
      break;

    case -1:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          (_("this polynomial degree is not supported")),
          rnrs_make_irritants_condition (scm_list_1
                                         (scm_from_size_t (degree)))));
      break;

    default:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          (_("an error occurred during implicitization")),
          rnrs_make_irritants_condition (scm_list_2 (a, b))));
      break;
    }

  return result;
}

VISIBLE SCM
scm_dpoly_implicitize_mono (SCM a, SCM b)
{
  return _scm_dpoly_implicitize ("dpoly-implicitize-mono",
                                 dpoly_implicitize_mono, a, b);
}

VISIBLE SCM
scm_dpoly_implicitize_bern (SCM a, SCM b)
{
  return _scm_dpoly_implicitize ("dpoly-implicitize-bern",
                                 dpoly_implicitize_bern, a, b);
}

VISIBLE SCM
scm_dpoly_implicitize_sbern (SCM a, SCM b)
{
  return _scm_dpoly_implicitize ("dpoly-implicitize-sbern",
                                 dpoly_implicitize_sbern, a, b);
}

VISIBLE SCM
scm_dpoly_implicitize_spower (SCM a, SCM b)
{
  return _scm_dpoly_implicitize ("dpoly-implicitize-spower",
                                 dpoly_implicitize_spower, a, b);
}

static SCM
_scm_dpoly_plugintoimplicit (const char *who,
                             void (*pluginto)
                             (size_t degree1,
                              double implicit_eq[degree1 + 1][degree1 + 1],
                              size_t degree2,
                              ssize_t stride_x, const double *x,
                              ssize_t stride_y, const double *y,
                              ssize_t result_stride, double *result),
                             SCM implicit_eq, SCM x, SCM y)
{
  const size_t degree = _parametric_poly_degree (poly_degree (who, SCM_ARG2, x),
                                                 poly_degree (who, SCM_ARG3, y),
                                                 who, x, y);

  scm_dynwind_begin (0);

  scm_t_array_handle handle_eq;
  scm_array_get_handle (implicit_eq, &handle_eq);
  scm_dynwind_array_handle_release (&handle_eq);

  const size_t degree_eq =
    bipoly_degree (who, implicit_eq, SCM_ARG1, &handle_eq);
  const double *elems = scm_array_handle_f64_elements (&handle_eq);
  ssize_t stridex = scm_array_handle_dims (&handle_eq)[0].inc;
  ssize_t stridey = scm_array_handle_dims (&handle_eq)[1].inc;
  double eq_[degree_eq + 1][degree_eq + 1];
  for (ssize_t i = 0; i <= degree_eq; i++)
    for (ssize_t j = 0; j <= degree_eq - i; j++)
      eq_[i][j] = elems[i * stridex + j * stridey];

  double *x_ = scm_malloc ((degree + 1) * sizeof (double));
  scm_dynwind_free (x_);
  poly_scm_to_double (degree, x, x_);

  double *y_ = scm_malloc ((degree + 1) * sizeof (double));
  scm_dynwind_free (y_);
  poly_scm_to_double (degree, y, y_);

  double *result_ = scm_malloc ((degree * degree_eq + 1) * sizeof (double));
  pluginto (degree_eq, eq_, degree, 1, x_, 1, y_, 1, result_);
  const SCM result = scm_take_f64vector (result_, degree * degree_eq + 1);

  scm_dynwind_end ();

  return result;
}

VISIBLE SCM
scm_dpoly_plugintoimplicit_mono (SCM implicit_eq, SCM x, SCM y)
{
  return _scm_dpoly_plugintoimplicit ("dpoly-plugintoimplicit-mono",
                                      dpoly_plugintoimplicit_mono,
                                      implicit_eq, x, y);
}

VISIBLE SCM
scm_dpoly_plugintoimplicit_bern (SCM implicit_eq, SCM x, SCM y)
{
  return _scm_dpoly_plugintoimplicit ("dpoly-plugintoimplicit-bern",
                                      dpoly_plugintoimplicit_bern,
                                      implicit_eq, x, y);
}

VISIBLE SCM
scm_dpoly_plugintoimplicit_sbern (SCM implicit_eq, SCM x, SCM y)
{
  return _scm_dpoly_plugintoimplicit ("dpoly-plugintoimplicit-sbern",
                                      dpoly_plugintoimplicit_sbern,
                                      implicit_eq, x, y);
}

VISIBLE SCM
scm_dpoly_plugintoimplicit_spower (SCM implicit_eq, SCM x, SCM y)
{
  return _scm_dpoly_plugintoimplicit ("dpoly-plugintoimplicit-spower",
                                      dpoly_plugintoimplicit_spower,
                                      implicit_eq, x, y);
}

//----------------------------------------------------------------------
//
// Inversion of polynomial splines.

VISIBLE SCM
scm_exact_poly_invertdegree1_mono (SCM poly_x, SCM poly_y, SCM x, SCM y)
{
  const char *who = "exact-poly-invertdegree1-mono";

  const size_t degree =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, poly_x),
                             poly_degree (who, SCM_ARG2, poly_y),
                             who, poly_x, poly_y);

  scm_dynwind_begin (0);

  mpq_t *poly_x_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_x_);
  mpq_dynwind_init_poly (degree, poly_x_);
  poly_scm_to_mpq (degree, poly_x, poly_x_);
  if (1 < mpq_poly_mindegree_mono (degree, 1, poly_x_))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("degree greater than one")),
        rnrs_make_irritants_condition (scm_list_1 (poly_x))));

  mpq_t *poly_y_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_y_);
  mpq_dynwind_init_poly (degree, poly_y_);
  poly_scm_to_mpq (degree, poly_y, poly_y_);
  if (1 < mpq_poly_mindegree_mono (degree, 1, poly_y_))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("degree greater than one")),
        rnrs_make_irritants_condition (scm_list_1 (poly_y))));

  mpq_t x_;
  mpq_init (x_);
  scm_dynwind_mpq_clear (x_);
  scm_to_mpq (scm_inexact_to_exact (x), x_);

  mpq_t y_;
  mpq_init (y_);
  scm_dynwind_mpq_clear (y_);
  scm_to_mpq (scm_inexact_to_exact (y), y_);

  mpq_t t_;
  mpq_init (t_);
  scm_dynwind_mpq_clear (t_);

  int info;
  mpq_poly_invertdegree1_mono (1, poly_x_, 1, poly_y_, x_, y_, t_, &info);

  switch (info)
    {
    case 0:
      break;
    case SMCORE__INVERSION_PARAMETER_IS_ARBITRARY:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          (_("there are infinitely many solutions")),
          rnrs_make_irritants_condition (scm_list_4 (poly_x, poly_y, x, y))));
      break;
    default:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          ("an unexpected error occurred (PLEASE SOMEONE FIX ME)"),
          rnrs_make_irritants_condition (scm_list_4 (poly_x, poly_y, x, y))));
      break;
    }

  SCM t = scm_from_mpq (t_);

  scm_dynwind_end ();

  return t;
}

VISIBLE SCM
scm_dpoly_invert_mono (SCM poly_x, SCM poly_y, SCM x, SCM y,
                       SCM ta, SCM tb, SCM absolute_tolerance)
{
  const char *who = "dpoly-invert-mono";

  const size_t degree =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, poly_x),
                             poly_degree (who, SCM_ARG2, poly_y),
                             who, poly_x, poly_y);
  const size_t max_num_solutions = degree;

  scm_dynwind_begin (0);

  mpq_t *poly_x_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_x_);
  mpq_dynwind_init_poly (degree, poly_x_);
  poly_scm_to_mpq (degree, poly_x, poly_x_);

  mpq_t *poly_y_ = scm_malloc ((degree + 1) * sizeof (mpq_t));
  scm_dynwind_free (poly_y_);
  mpq_dynwind_init_poly (degree, poly_y_);
  poly_scm_to_mpq (degree, poly_y, poly_y_);

  mpq_t x_;
  mpq_init (x_);
  scm_dynwind_mpq_clear (x_);
  scm_to_mpq (scm_inexact_to_exact (x), x_);

  mpq_t y_;
  mpq_init (y_);
  scm_dynwind_mpq_clear (y_);
  scm_to_mpq (scm_inexact_to_exact (y), y_);

  mpq_t ta_;
  mpq_init (ta_);
  scm_dynwind_mpq_clear (ta_);
  scm_to_mpq (scm_inexact_to_exact (ta), ta_);

  mpq_t tb_;
  mpq_init (tb_);
  scm_dynwind_mpq_clear (tb_);
  scm_to_mpq (scm_inexact_to_exact (tb), tb_);

  const double tol_ = scm_to_double (absolute_tolerance);

  int info;
  size_t num_solutions;
  double *t_ = scm_malloc (szmax (1, max_num_solutions) * sizeof (double));
  scm_dynwind_free (t_);
  dpoly_invert_mono (degree, 1, poly_x_, 1, poly_y_, x_, y_, ta_, tb_, tol_,
                     &num_solutions, t_, &info);
  switch (info)
    {
    case 0:
      break;
    case SMCORE__INVERSION_PARAMETER_IS_ARBITRARY:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          (_("there are infinitely many solutions")),
          rnrs_make_irritants_condition (scm_list_4 (poly_x, poly_y, x, y))));
      break;
    default:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition
          ("unexpected error condition (PLEASE SOMEBODY FIX ME)"),
          rnrs_make_irritants_condition (scm_list_n (poly_x, poly_y, x, y,
                                                     ta, tb,
                                                     absolute_tolerance))));
      break;
    }

  double *t_data = scm_malloc (szmax (1, num_solutions) * sizeof (double));
  memcpy (t_data, t_, num_solutions * sizeof (double));
  SCM t = scm_take_f64vector (t_data, num_solutions);

  scm_dynwind_end ();

  return t;
}

//----------------------------------------------------------------------

VISIBLE SCM
scm_intersect_implicit_with_parametric (SCM implicit_a, SCM bx, SCM by,
                                        SCM t0, SCM t1)
{
  const char *who = "intersect-implicit-with-parametric";

  const size_t degree_b =
    _parametric_poly_degree (poly_degree (who, SCM_ARG2, bx),
                             poly_degree (who, SCM_ARG3, by),
                             who, bx, by);

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (implicit_a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, implicit_a, SCM_ARG1, &handle_a);
  const SCM *elems = scm_array_handle_elements (&handle_a);
  ssize_t stride_ax = scm_array_handle_dims (&handle_a)[0].inc;
  ssize_t stride_ay = scm_array_handle_dims (&handle_a)[1].inc;
  mpq_t (*implicit_a_)[degree_a + 1] =
    scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (implicit_a_);
  mpq_dynwind_init_bipoly (degree_a, implicit_a_);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      scm_to_mpq (scm_inexact_to_exact (elems[i * stride_ax + j * stride_ay]),
                  implicit_a_[i][j]);

  mpq_t *bx_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (bx_);
  mpq_dynwind_init_poly (degree_b, bx_);
  poly_scm_to_mpq (degree_b, bx, bx_);

  mpq_t *by_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (by_);
  mpq_dynwind_init_poly (degree_b, by_);
  poly_scm_to_mpq (degree_b, by, by_);

  mpq_t t0_;
  mpq_init (t0_);
  scm_dynwind_mpq_clear (t0_);
  scm_to_mpq (scm_inexact_to_exact (t0), t0_);

  mpq_t t1_;
  mpq_init (t1_);
  scm_dynwind_mpq_clear (t1_);
  scm_to_mpq (scm_inexact_to_exact (t1), t1_);

  bool the_curves_are_the_same;
  size_t num_intersections;
  double *t_ = scm_malloc (szmax (degree_a * degree_b, 1) * sizeof (double));
  scm_dynwind_free (t_);
  intersect_implicit_with_parametric (degree_a, implicit_a_,
                                      degree_b, 1, bx_, 1, by_, t0_, t1_,
                                      &the_curves_are_the_same,
                                      &num_intersections, t_);
  SCM result = scm_c_make_vector (num_intersections, SCM_UNDEFINED);
  for (size_t i = 0; i < num_intersections; i++)
    SCM_SIMPLE_VECTOR_SET (result, i, scm_from_double (t_[i]));

  scm_dynwind_end ();

  return result;
}

static void
_check_implicit_equation_degree (const char *who,
                                 size_t degree, size_t degree_impl,
                                 SCM x, SCM y, SCM implicit)
{
  if (degree_impl != degree)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition ("polynomial degrees do not match"),
        rnrs_make_irritants_condition (scm_list_3 (x, y, implicit))));
}

// FIXME: This could be used in other places.
static void
_scm_data_to_bipoly (size_t degree, const SCM *elems,
                     ssize_t stridex, ssize_t stridey,
                     mpq_t bipoly[degree + 1][degree + 1])
{
  for (ssize_t i = 0; i <= degree; i++)
    for (ssize_t j = 0; j <= degree - i; j++)
      scm_to_mpq (scm_inexact_to_exact (elems[i * stridex + j * stridey]),
                  bipoly[i][j]);
}

VISIBLE SCM
scm_poly_findintersections_mono (SCM ax, SCM ay, SCM implicit_a,
                                 SCM ta0, SCM ta1,
                                 SCM bx, SCM by, SCM implicit_b,
                                 SCM tb0, SCM tb1, SCM tolerance)
{
  const char *who = "poly-findintersections-mono";

  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);
  const size_t degree_b =
    _parametric_poly_degree (poly_degree (who, SCM_ARG6, bx),
                             poly_degree (who, SCM_ARG7, by),
                             who, bx, by);

  const double tol_ = (unbndp_or_false (tolerance)) ?
    (-1.0) : (scm_to_double (tolerance));

  scm_dynwind_begin (0);

  mpq_t *ax_ = scm_malloc ((degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (ax_);
  mpq_dynwind_init_poly (degree_a, ax_);
  poly_scm_to_mpq (degree_a, ax, ax_);

  mpq_t *ay_ = scm_malloc ((degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (ay_);
  mpq_dynwind_init_poly (degree_a, ay_);
  poly_scm_to_mpq (degree_a, ay, ay_);

  mpq_t (*impl_a_)[degree_a + 1];
  if (unbndp_or_false (implicit_a))
    impl_a_ = NULL;
  else
    {
      scm_t_array_handle handle_impl_a;
      scm_array_get_handle (implicit_a, &handle_impl_a);
      scm_dynwind_array_handle_release (&handle_impl_a);

      const size_t degree_impl_a =
        bipoly_degree (who, implicit_a, SCM_ARG3, &handle_impl_a);
      _check_implicit_equation_degree (who, degree_a, degree_impl_a,
                                       ax, ay, implicit_a);
      const SCM *elems = scm_array_handle_elements (&handle_impl_a);
      ssize_t stridex = scm_array_handle_dims (&handle_impl_a)[0].inc;
      ssize_t stridey = scm_array_handle_dims (&handle_impl_a)[1].inc;
      impl_a_ = scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
      scm_dynwind_free (impl_a_);
      mpq_dynwind_init_bipoly (degree_a, impl_a_);
      _scm_data_to_bipoly (degree_a, elems, stridex, stridey, impl_a_);
    }

  mpq_t *bx_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (bx_);
  mpq_dynwind_init_poly (degree_b, bx_);
  poly_scm_to_mpq (degree_b, bx, bx_);

  mpq_t *by_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (by_);
  mpq_dynwind_init_poly (degree_b, by_);
  poly_scm_to_mpq (degree_b, by, by_);

  mpq_t ta0_;
  mpq_init (ta0_);
  scm_dynwind_mpq_clear (ta0_);
  scm_to_mpq (scm_inexact_to_exact (ta0), ta0_);

  mpq_t ta1_;
  mpq_init (ta1_);
  scm_dynwind_mpq_clear (ta1_);
  scm_to_mpq (scm_inexact_to_exact (ta1), ta1_);

  mpq_t (*impl_b_)[degree_b + 1];
  if (unbndp_or_false (implicit_b))
    impl_b_ = NULL;
  else
    {
      scm_t_array_handle handle_impl_b;
      scm_array_get_handle (implicit_b, &handle_impl_b);
      scm_dynwind_array_handle_release (&handle_impl_b);

      const size_t degree_impl_b =
        bipoly_degree (who, implicit_b, SCM_ARG8, &handle_impl_b);
      _check_implicit_equation_degree (who, degree_b, degree_impl_b,
                                       ax, ay, implicit_b);
      const SCM *elems = scm_array_handle_elements (&handle_impl_b);
      ssize_t stridex = scm_array_handle_dims (&handle_impl_b)[0].inc;
      ssize_t stridey = scm_array_handle_dims (&handle_impl_b)[1].inc;
      impl_b_ = scm_malloc ((degree_b + 1) * (degree_b + 1) * sizeof (mpq_t));
      scm_dynwind_free (impl_b_);
      mpq_dynwind_init_bipoly (degree_b, impl_b_);
      _scm_data_to_bipoly (degree_b, elems, stridex, stridey, impl_b_);
    }

  mpq_t tb0_;
  mpq_init (tb0_);
  scm_dynwind_mpq_clear (tb0_);
  scm_to_mpq (scm_inexact_to_exact (tb0), tb0_);

  mpq_t tb1_;
  mpq_init (tb1_);
  scm_dynwind_mpq_clear (tb1_);
  scm_to_mpq (scm_inexact_to_exact (tb1), tb1_);

  plane_intersections_t *intersections =
    poly_findintersections_mono (degree_a, 1, ax_, 1, ay_, impl_a_, ta0_, ta1_,
                                 degree_b, 1, bx_, 1, by_, impl_b_, tb0_, tb1_,
                                 tol_);

  scm_dynwind_end ();

  SCM result = scm_c_make_vector (intersections->num_xy, SCM_UNDEFINED);
  for (size_t i = 0; i < intersections->num_xy; i++)
    {
      SCM entry = scm_c_make_vector (4, SCM_UNDEFINED);
      SCM_SIMPLE_VECTOR_SET (entry, 0,
                             scm_from_double (intersections->xy[i].x));
      SCM_SIMPLE_VECTOR_SET (entry, 1,
                             scm_from_double (intersections->xy[i].y));

      SCM ta = scm_c_make_vector (intersections->xy[i].num_ta, SCM_UNDEFINED);
      for (size_t j = 0; j < intersections->xy[i].num_ta; j++)
        SCM_SIMPLE_VECTOR_SET (ta, j,
                               scm_from_double (intersections->xy[i].ta[j]));
      SCM_SIMPLE_VECTOR_SET (entry, 2, ta);

      SCM tb = scm_c_make_vector (intersections->xy[i].num_tb, SCM_UNDEFINED);
      for (size_t j = 0; j < intersections->xy[i].num_tb; j++)
        SCM_SIMPLE_VECTOR_SET (tb, j,
                               scm_from_double (intersections->xy[i].tb[j]));
      SCM_SIMPLE_VECTOR_SET (entry, 3, tb);

      SCM_SIMPLE_VECTOR_SET (result, i, entry);
    }

  return result;
}

VISIBLE SCM
scm_dpoly_findintersections_bern (SCM ax, SCM ay, SCM implicit_a,
                                  SCM ta0, SCM ta1,
                                  SCM bx, SCM by, SCM implicit_b,
                                  SCM tb0, SCM tb1, SCM tolerance)
{
  const char *who = "dpoly-findintersections-bern";

  const size_t degree_a =
    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
                             poly_degree (who, SCM_ARG2, ay),
                             who, ax, ay);
  const size_t degree_b =
    _parametric_poly_degree (poly_degree (who, SCM_ARG6, bx),
                             poly_degree (who, SCM_ARG7, by),
                             who, bx, by);

  const double tol_ = (unbndp_or_false (tolerance)) ?
    (-1.0) : (scm_to_double (tolerance));

  scm_dynwind_begin (0);

  double *ax_ = scm_malloc ((degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (ax_);
  poly_scm_to_double (degree_a, ax, ax_);

  double *ay_ = scm_malloc ((degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (ay_);
  poly_scm_to_double (degree_a, ay, ay_);

  mpq_t (*impl_a_)[degree_a + 1];
  if (unbndp_or_false (implicit_a))
    impl_a_ = NULL;
  else
    {
      scm_t_array_handle handle_impl_a;
      scm_array_get_handle (implicit_a, &handle_impl_a);
      scm_dynwind_array_handle_release (&handle_impl_a);

      const size_t degree_impl_a =
        bipoly_degree (who, implicit_a, SCM_ARG3, &handle_impl_a);
      _check_implicit_equation_degree (who, degree_a, degree_impl_a,
                                       ax, ay, implicit_a);
      const SCM *elems = scm_array_handle_elements (&handle_impl_a);
      ssize_t stridex = scm_array_handle_dims (&handle_impl_a)[0].inc;
      ssize_t stridey = scm_array_handle_dims (&handle_impl_a)[1].inc;
      impl_a_ = scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
      scm_dynwind_free (impl_a_);
      mpq_dynwind_init_bipoly (degree_a, impl_a_);
      _scm_data_to_bipoly (degree_a, elems, stridex, stridey, impl_a_);
    }

  double *bx_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (bx_);
  poly_scm_to_double (degree_b, bx, bx_);

  double *by_ = scm_malloc ((degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (by_);
  poly_scm_to_double (degree_b, by, by_);

  const double ta0_ = scm_to_double (ta0);
  const double ta1_ = scm_to_double (ta1);

  mpq_t (*impl_b_)[degree_b + 1];
  if (unbndp_or_false (implicit_b))
    impl_b_ = NULL;
  else
    {
      scm_t_array_handle handle_impl_b;
      scm_array_get_handle (implicit_b, &handle_impl_b);
      scm_dynwind_array_handle_release (&handle_impl_b);

      const size_t degree_impl_b =
        bipoly_degree (who, implicit_b, SCM_ARG8, &handle_impl_b);
      _check_implicit_equation_degree (who, degree_b, degree_impl_b,
                                       ax, ay, implicit_b);
      const SCM *elems = scm_array_handle_elements (&handle_impl_b);
      ssize_t stridex = scm_array_handle_dims (&handle_impl_b)[0].inc;
      ssize_t stridey = scm_array_handle_dims (&handle_impl_b)[1].inc;
      impl_b_ = scm_malloc ((degree_b + 1) * (degree_b + 1) * sizeof (mpq_t));
      scm_dynwind_free (impl_b_);
      mpq_dynwind_init_bipoly (degree_b, impl_b_);
      _scm_data_to_bipoly (degree_b, elems, stridex, stridey, impl_b_);
    }

  const double tb0_ = scm_to_double (tb0);
  const double tb1_ = scm_to_double (tb1);

  plane_intersections_t *intersections =
    dpoly_findintersections_bern (degree_a, 1, ax_, 1, ay_, impl_a_, ta0_, ta1_,
                                  degree_b, 1, bx_, 1, by_, impl_b_, tb0_, tb1_,
                                  tol_);

  scm_dynwind_end ();

  SCM result = scm_c_make_vector (intersections->num_xy, SCM_UNDEFINED);
  for (size_t i = 0; i < intersections->num_xy; i++)
    {
      SCM entry = scm_c_make_vector (4, SCM_UNDEFINED);
      SCM_SIMPLE_VECTOR_SET (entry, 0,
                             scm_from_double (intersections->xy[i].x));
      SCM_SIMPLE_VECTOR_SET (entry, 1,
                             scm_from_double (intersections->xy[i].y));

      SCM ta = scm_c_make_vector (intersections->xy[i].num_ta, SCM_UNDEFINED);
      for (size_t j = 0; j < intersections->xy[i].num_ta; j++)
        SCM_SIMPLE_VECTOR_SET (ta, j,
                               scm_from_double (intersections->xy[i].ta[j]));
      SCM_SIMPLE_VECTOR_SET (entry, 2, ta);

      SCM tb = scm_c_make_vector (intersections->xy[i].num_tb, SCM_UNDEFINED);
      for (size_t j = 0; j < intersections->xy[i].num_tb; j++)
        SCM_SIMPLE_VECTOR_SET (tb, j,
                               scm_from_double (intersections->xy[i].tb[j]));
      SCM_SIMPLE_VECTOR_SET (entry, 3, tb);

      SCM_SIMPLE_VECTOR_SET (result, i, entry);
    }

  return result;
}

///static SCM
///_scm_spoly_findintersections (const char *who,
///                              float_plane_intersection_t *
///                              (*findintersections)
///                              (size_t degree_a,
///                               ssize_t stride_ax, const float *ax,
///                               ssize_t stride_ay, const float *ay,
///                               size_t degree_b,
///                               ssize_t stride_bx, const float *bx,
///                               ssize_t stride_by, const float *by,
///                               float tolerance),
///                              SCM ax, SCM ay, SCM bx, SCM by, SCM tolerance)
///{
///  const size_t degree_a =
///    _parametric_poly_degree (poly_degree (who, SCM_ARG1, ax),
///                             poly_degree (who, SCM_ARG2, ay),
///                             who, ax, ay);
///  const size_t degree_b =
///    _parametric_poly_degree (poly_degree (who, SCM_ARG4, bx),
///                             poly_degree (who, SCM_ARG5, by),
///                             who, bx, by);
///
///  const float tol = (unbndp_or_false (tolerance)) ?
///    -1.0 : (scm_to_double (tolerance));
///
///  float ax_[degree_a + 1];
///  float ay_[degree_a + 1];
///  float bx_[degree_b + 1];
///  float by_[degree_b + 1];
///  poly_scm_to_float (degree_a, ax, ax_);
///  poly_scm_to_float (degree_a, ay, ay_);
///  poly_scm_to_float (degree_b, bx, bx_);
///  poly_scm_to_float (degree_b, by, by_);
///
///  float_plane_intersection_t *intersections =
///    findintersections (degree_a, 1, ax_, 1, ay_, degree_b, 1, bx_, 1, by_,
///                       tol);
///
///  size_t num_intersections =
///    float_plane_intersection_list_length (intersections);
///  SCM result = scm_c_make_vector (num_intersections, SCM_UNDEFINED);
///  float_plane_intersection_t *p = intersections;
///  for (size_t i = 0; i < num_intersections; i++)
///    {
///      float *data = scm_malloc (4 * sizeof (float));
///      SCM entry = scm_take_f32vector (data, 4);
///      data[0] = p->t1;
///      data[1] = p->t2;
///      data[2] = p->x;
///      data[3] = p->y;
///      SCM_SIMPLE_VECTOR_SET (result, i, entry);
///      p = p->next;
///    }
///
///  return result;
///}
///
///VISIBLE SCM
///scm_spoly_findintersections_bernsv (SCM ax, SCM ay, SCM bx, SCM by,
///                                    SCM tolerance)
///{
///  return _scm_spoly_findintersections ("spoly-findintersections-bernsv",
///                                       spoly_findintersections_bernsv,
///                                       ax, ay, bx, by, tolerance);
///}
///
///VISIBLE SCM
///scm_spoly_findintersections_berndc (SCM ax, SCM ay, SCM bx, SCM by,
///                                    SCM tolerance)
///{
///  return _scm_spoly_findintersections ("spoly-findintersections-berndc",
///                                       spoly_findintersections_berndc,
///                                       ax, ay, bx, by, tolerance);
///}

//----------------------------------------------------------------------

VISIBLE SCM
scm_exact_poly_mono_to_bern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-mono-to-bern", poly, NULL,
                             mpq_poly_mono_to_bern);
}

VISIBLE SCM
scm_exact_poly_bern_to_mono (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-bern-to-mono", poly, NULL,
                             mpq_poly_bern_to_mono);
}

VISIBLE SCM
scm_exact_poly_mono_to_sbern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-mono-to-sbern", poly, NULL,
                             mpq_poly_mono_to_sbern);
}

VISIBLE SCM
scm_exact_poly_sbern_to_mono (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-sbern-to-mono", poly, NULL,
                             mpq_poly_sbern_to_mono);
}

VISIBLE SCM
scm_exact_poly_bern_to_sbern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-bern-to-sbern", poly, NULL,
                             mpq_poly_bern_to_sbern);
}

VISIBLE SCM
scm_exact_poly_sbern_to_bern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-sbern-to-bern", poly, NULL,
                             mpq_poly_sbern_to_bern);
}

VISIBLE SCM
scm_exact_poly_sbern_to_spower (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-sbern-to-spower", poly, NULL,
                             mpq_poly_sbern_to_spower);
}

VISIBLE SCM
scm_exact_poly_spower_to_sbern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-spower-to-sbern", poly, NULL,
                             mpq_poly_spower_to_sbern);
}

VISIBLE SCM
scm_exact_poly_bern_to_spower (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-bern-to-spower", poly, NULL,
                             mpq_poly_bern_to_spower);
}

VISIBLE SCM
scm_exact_poly_spower_to_bern (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-spower-to-bern", poly, NULL,
                             mpq_poly_spower_to_bern);
}

VISIBLE SCM
scm_exact_poly_mono_to_spower (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-mono-to-spower", poly, NULL,
                             mpq_poly_mono_to_spower);
}

VISIBLE SCM
scm_exact_poly_spower_to_mono (SCM poly)
{
  return exact_poly_to_poly ("exact-poly-spower-to-mono", poly, NULL,
                             mpq_poly_spower_to_mono);
}

//----------------------------------------------------------------------

VISIBLE SCM
scm_dpoly_mono_to_bern (SCM poly)
{
  return dpoly_to_poly ("dpoly-mono-to-bern", poly, NULL, dpoly_mono_to_bern);
}

VISIBLE SCM
scm_dpoly_bern_to_mono (SCM poly)
{
  return dpoly_to_poly ("dpoly-bern-to-mono", poly, NULL, dpoly_bern_to_mono);
}

VISIBLE SCM
scm_dpoly_mono_to_sbern (SCM poly)
{
  return dpoly_to_poly ("dpoly-mono-to-sbern", poly, NULL, dpoly_mono_to_sbern);
}

VISIBLE SCM
scm_dpoly_sbern_to_mono (SCM poly)
{
  return dpoly_to_poly ("dpoly-sbern-to-mono", poly, NULL, dpoly_sbern_to_mono);
}

VISIBLE SCM
scm_dpoly_bern_to_sbern (SCM poly)
{
  return dpoly_to_poly ("dpoly-bern-to-sbern", poly, NULL, dpoly_bern_to_sbern);
}

VISIBLE SCM
scm_dpoly_sbern_to_bern (SCM poly)
{
  return dpoly_to_poly ("dpoly-sbern-to-bern", poly, NULL, dpoly_sbern_to_bern);
}

VISIBLE SCM
scm_dpoly_sbern_to_spower (SCM poly)
{
  return dpoly_to_poly ("dpoly-sbern-to-spower", poly, NULL,
                        dpoly_sbern_to_spower);
}

VISIBLE SCM
scm_dpoly_spower_to_sbern (SCM poly)
{
  return dpoly_to_poly ("dpoly-spower-to-sbern", poly, NULL,
                        dpoly_spower_to_sbern);
}

VISIBLE SCM
scm_dpoly_bern_to_spower (SCM poly)
{
  return dpoly_to_poly ("dpoly-bern-to-spower", poly, NULL,
                        dpoly_bern_to_spower);
}

VISIBLE SCM
scm_dpoly_spower_to_bern (SCM poly)
{
  return dpoly_to_poly ("dpoly-spower-to-bern", poly, NULL,
                        dpoly_spower_to_bern);
}

VISIBLE SCM
scm_dpoly_mono_to_spower (SCM poly)
{
  return dpoly_to_poly ("dpoly-mono-to-spower", poly, NULL,
                        dpoly_mono_to_spower);
}

VISIBLE SCM
scm_dpoly_spower_to_mono (SCM poly)
{
  return dpoly_to_poly ("dpoly-spower-to-mono", poly, NULL,
                        dpoly_spower_to_mono);
}

//----------------------------------------------------------------------
