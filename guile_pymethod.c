#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

////////#if PY_VERSION_HEX < 0x03040000
////////#define _CONST_CHAR_PTR_WORKAROUND(x) ((char *) (x))
////////#else
////////#define _CONST_CHAR_PTR_WORKAROUND(x) (x)
////////#endif

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pymethod, PyMethod);

#if PY_MAJOR_VERSION <= 2

VISIBLE bool
scm_is_pyinstancemethod (SCM obj STM_MAYBE_UNUSED)
{
  return false;
}

VISIBLE SCM
scm_pyinstancemethod_p (SCM obj STM_MAYBE_UNUSED)
{
  return SCM_BOOL_F;
}

#else // !(PY_MAJOR_VERSION <= 2)

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pyinstancemethod, PyInstanceMethod);

#endif // !(PY_MAJOR_VERSION <= 2)

#if PY_MAJOR_VERSION <= 2

VISIBLE SCM
scm_pyinstancemethod_new (SCM func, SCM class)
{
  // In Python 2, this actually returns an old-style ‘method’ rather
  // than an ‘instance method’.

  scm_assert_pyobject (func);
  PyObject *_func = scm_to_PyObject_ptr (func);
  SCM_ASSERT_TYPE ((PyCallable_Check (_func)), func, SCM_ARG1,
                   "pyinstancemethod-new", "pycallable");

  scm_assert_pyobject (class);
  PyObject *_class = scm_to_PyObject_ptr (class);
  SCM_ASSERT_TYPE ((PyType_Check (_class)), class, SCM_ARG2,
                   "pyinstancemethod-new", "pytype");

  PyObject *_method = PyMethod_New (_func, NULL, _class);
  py_exc_check ((_method != NULL), "pyinstancemethod-new",
                scm_list_2 (func, class));

  return scm_c_make_pyobject (_method);
}

#else // !(PY_MAJOR_VERSION <= 2)

VISIBLE SCM
scm_pyinstancemethod_new (SCM func, SCM class STM_MAYBE_UNUSED)
{
  scm_assert_pyobject (func);
  PyObject *_func = scm_to_PyObject_ptr (func);
  SCM_ASSERT_TYPE ((PyCallable_Check (_func)), func, SCM_ARG1,
                   "pyinstancemethod-new", "pycallable");

  PyObject *_method = PyInstanceMethod_New (_func);
  py_exc_check ((_method != NULL), "pyinstancemethod-new", scm_list_1 (func));

  return scm_c_make_pyobject (_method);
}

#endif // !(PY_MAJOR_VERSION <= 2)

//-------------------------------------------------------------------------
