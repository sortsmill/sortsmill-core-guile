#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

#if PY_VERSION_HEX < 0x03040000
#define _CONST_CHAR_PTR_WORKAROUND(x) ((char *) (x))
#else
#define _CONST_CHAR_PTR_WORKAROUND(x) (x)
#endif

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pymodule, PyModule);

VISIBLE SCM
scm_c_pymodule_new (const char *name)
{
  PyObject *obj = PyModule_New (name);
  py_exc_check ((obj != NULL), "pymodule-new",
                scm_list_1 (scm_from_utf8_string (name)));
  return scm_c_make_pyobject (obj);
}

VISIBLE SCM
scm_pymodule_new (SCM name)
{
  scm_dynwind_begin (0);
  char *_name = scm_to_utf8_stringn (scm_pyunicode_to_scm (name), NULL);
  scm_dynwind_free (_name);
  SCM obj = scm_c_pymodule_new (_name);
  scm_dynwind_end ();
  return obj;
}

VISIBLE SCM
scm_pymodule_getdict (SCM module)
{
  scm_assert_pyobject (module);
  PyObject *_module = scm_to_PyObject_ptr (module);
  PyObject *_dict = PyModule_GetDict (_module);
  return scm_c_make_borrowed_pyobject (_dict, module);
}

VISIBLE const char *
scm_c_pymodule_getname (SCM module)
{
  scm_assert_pyobject (module);
  PyObject *_module = scm_to_PyObject_ptr (module);

  const char *name = PyModule_GetName (_module);
  py_exc_check ((name != NULL), "pymodule-getname", scm_list_1 (module));

  return name;
}

VISIBLE SCM
scm_pymodule_getname (SCM module)
{
  return scm_from_utf8_string (scm_c_pymodule_getname (module));
}

#if PY_VERSION_HEX < 0x03020000

VISIBLE SCM
scm_pymodule_getfilename (SCM module)
{
  scm_assert_pyobject (module);
  PyObject *_module = scm_to_PyObject_ptr (module);

  const char *_name = PyModule_GetFilename (_module);
  py_exc_check ((_name != NULL), "pymodule-getfilename", scm_list_1 (module));

  // FIXME: Is the scm_string_copy here necessary?
  return scm_string_copy (scm_from_utf8_string (_name));
}

#else // !(PY_VERSION_HEX < 0x03020000)

VISIBLE SCM
scm_pymodule_getfilename (SCM module)
{
  scm_assert_pyobject (module);
  PyObject *_module = scm_to_PyObject_ptr (module);

  PyObject *_name = PyModule_GetFilenameObject (_module);
  py_exc_check ((_name != NULL), "pymodule-getfilename", scm_list_1 (module));

  // FIXME: scm_pyunicode_to_string is not guaranteed to preserve
  // Python ‘strings’ not encodable in UTF-8 or the like. On the other
  // hand, if a module has such a ‘non-Unicode’ file name, something
  // elsewhere is awry and probably ought to be dealt with more
  // directly.
  return scm_pyunicode_to_string (scm_c_make_pyobject (_name), SCM_UNDEFINED);
}

#endif // !(PY_VERSION_HEX < 0x03020000)

VISIBLE void
scm_c_pymodule_addobject (SCM module, const char *name, SCM v)
{
  scm_assert_pyobject (module);
  PyObject *_module = scm_to_PyObject_ptr (module);

  scm_assert_pyobject (v);
  PyObject *_v = scm_to_PyObject_ptr (v);

  Py_INCREF (_v);
  int errval = PyModule_AddObject (_module, name, _v);
  py_exc_check ((errval != -1), "pymodule-addobject!",
                scm_list_3 (module, scm_from_utf8_string (name), v));
}

VISIBLE SCM
scm_pymodule_addobject_x (SCM module, SCM name, SCM v)
{
  scm_dynwind_begin (0);
  char *_name = scm_to_utf8_stringn (scm_pyunicode_to_scm (name), NULL);
  scm_dynwind_free (_name);
  scm_c_pymodule_addobject (module, _name, v);
  scm_dynwind_end ();
  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_c_pyimport_importmodule (const char *name, SCM globals, SCM locals,
                             SCM fromlist, int level)
{
  PyObject *_globals;
  if (unbndp_or_false (globals))
    _globals = NULL;
  else
    {
      scm_assert_pyobject (globals);
      _globals = scm_to_PyObject_ptr (globals);
    }

  PyObject *_locals;
  if (unbndp_or_false (locals))
    _locals = NULL;
  else
    {
      scm_assert_pyobject (locals);
      _locals = scm_to_PyObject_ptr (locals);
    }

  if (unbndp_or_false (fromlist))
    fromlist = scm_list_to_pytuple (SCM_EOL);
  else
    scm_assert_pyobject (fromlist);
  PyObject *_fromlist = scm_to_PyObject_ptr (fromlist);

  PyObject *_module =
    PyImport_ImportModuleLevel (_CONST_CHAR_PTR_WORKAROUND (name),
                                _globals, _locals, _fromlist, level);
  py_exc_check ((_module != NULL), "pyimport-importmodule",
                scm_list_5 (scm_from_utf8_string (name), globals, locals,
                            fromlist, scm_from_int (level)));

  return scm_c_make_pyobject (_module);
}

VISIBLE SCM
scm_pyimport_importmodule (SCM name, SCM globals, SCM locals, SCM fromlist,
                           SCM level)
{
  const int _level = (unbndp_or_false (level)) ? 0 : scm_to_int (level);

  scm_dynwind_begin (0);

  char *_name = scm_to_utf8_stringn (scm_pyunicode_to_scm (name), NULL);
  scm_dynwind_free (_name);

  SCM module =
    scm_c_pyimport_importmodule (_name, globals, locals, fromlist, _level);

  scm_dynwind_end ();

  return module;
}

VISIBLE SCM
scm_pyimport_import (SCM name)
{
  SCM pyname = scm_scm_to_pyunicode (name);

  scm_assert_pyobject (pyname);
  PyObject *_name = scm_to_PyObject_ptr (pyname);

  PyObject *_module = PyImport_Import (_name);
  py_exc_check ((_module != NULL), "pyimport-import", scm_list_1 (name));

  return scm_c_make_pyobject (_module);
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pyimport-reloadmodule",
                               scm_pyimport_reloadmodule,
                               PyImport_ReloadModule);

VISIBLE SCM
scm_c_pyimport_addmodule (const char *name)
{
  PyObject *_module = PyImport_AddModule (name);
  py_exc_check ((_module != NULL), "pyimport-addmodule",
                scm_list_1 (scm_from_utf8_string (name)));
  return scm_c_incref_make_pyobject (_module);
}

VISIBLE SCM
scm_pyimport_addmodule_x (SCM name)
{
  scm_dynwind_begin (0);
  char *_name = scm_to_utf8_stringn (scm_pyunicode_to_scm (name), NULL);
  scm_dynwind_free (_name);
  SCM module = scm_c_pyimport_addmodule (_name);
  scm_dynwind_end ();
  return module;
}

VISIBLE SCM
scm_pyimport_getmoduledict (void)
{
  PyObject *_dict = PyImport_GetModuleDict ();
  py_exc_check ((_dict != NULL), "pyimport-getmoduledict", SCM_EOL);
  return scm_c_incref_make_pyobject (_dict);
}

//-------------------------------------------------------------------------

// Convert a Python module name to the name of a Guile module. For
// example: @code{"a.b.c"} yields @code{'(a b c)}.
VISIBLE SCM
scm_pymodule_name_to_module_name (SCM pymodule_name)
{
  // The @code{#\.} character.
  SCM period = scm_integer_to_char (scm_from_int (0x2E));

  // To get the Guile module name, split at @code{#\.} characters
  // and convert the resulting list elements to symbols.
  SCM module_name_reversed = SCM_EOL;
  SCM p = scm_string_split (pymodule_name, period);
  while (!scm_is_null (p))
    {
      module_name_reversed =
        scm_cons (scm_string_to_symbol (SCM_CAR (p)), module_name_reversed);
      p = SCM_CDR (p);
    }
  return scm_reverse_x_without_checking (module_name_reversed, SCM_EOL);
}

// Convert a Python module name to the name of a Guile initialization
// procedure. For example: @code{"a.b.c"} yields @code{'PyInit-a.b.c}.
VISIBLE SCM
scm_pyinit_name (SCM pymodule_name)
{
  return
    scm_string_to_symbol (scm_string_append
                          (scm_list_2 (scm_from_latin1_string ("PyInit-"),
                                       pymodule_name)));
}

// Return the variable that contains the Guile initialization
// procedure for a Python module; but return @code{#f} if the variable
// cannot be found.
VISIBLE SCM
scm_pyinit_variable (SCM module_name, SCM pyinit_name)
{
  SCM module = scm_call_5 (scm_c_public_ref ("guile", "resolve-module"),
                           module_name, SCM_BOOL_T, SCM_BOOL_F,
                           scm_from_latin1_keyword ("ensure"), SCM_BOOL_F);
  return (scm_is_true (module)) ?
    scm_module_variable (module, pyinit_name) : SCM_BOOL_F;
}

// Return the variable that contains the Guile initialization
// procedure for a Python module; but return @code{#f} if the variable
// cannot be found.
VISIBLE SCM
scm_pymodule_pyinit_variable (SCM pymodule_name)
{
  return scm_pyinit_variable (scm_pymodule_name_to_module_name (pymodule_name),
                              scm_pyinit_name (pymodule_name));
}

// Return the Guile initialization procedure for a Python module; but
// return @code{#f} if the procedure cannot be found.
VISIBLE SCM
scm_pyinit_procedure (SCM module_name, SCM pyinit_name)
{
  SCM variable = scm_pyinit_variable (module_name, pyinit_name);
  return (scm_is_true (variable)) ? scm_variable_ref (variable) : SCM_BOOL_F;
}

// Return the Guile initialization procedure for a Python module; but
// return @code{#f} if the procedure cannot be found.
VISIBLE SCM
scm_pymodule_pyinit_procedure (SCM pymodule_name)
{
  return scm_pyinit_procedure (scm_pymodule_name_to_module_name (pymodule_name),
                               scm_pyinit_name (pymodule_name));
}

//-------------------------------------------------------------------------
