#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

#if PY_VERSION_HEX < 0x03040000
#define _CONST_CHAR_PTR_WORKAROUND(x) ((char *) (x))
#else
#define _CONST_CHAR_PTR_WORKAROUND(x) (x)
#endif

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_py_initialize (SCM initsigs)
{
  if (!Py_IsInitialized ())
    {
      if (unbndp_or_false (initsigs))
        Py_Initialize ();
      else
        Py_InitializeEx (scm_to_int (initsigs));
    }
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_py_initialized_p (void)
{
  return scm_from_bool (Py_IsInitialized ());
}

VISIBLE SCM
scm_py_finalize (void)
{
  if (Py_IsInitialized ())
    {
      scm_gc ();
      Py_Finalize ();
    }
  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_raise_py_uninitialized (void)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_error (),
      rnrs_c_make_who_condition ("make-pyobject"),
      rnrs_c_make_message_condition (_("Python is not initialized")),
      rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

VISIBLE void scm_c_assert_py_initialized (void);

VISIBLE SCM
scm_assert_py_initialized (void)
{
  scm_c_assert_py_initialized ();
  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------
