#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <frameobject.h>

#include <assert.h>
#include <stdlib.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>

#if PY_VERSION_HEX < 0x02070000

static inline int
PyFrame_GetLineNumber (const PyFrameObject *frame)
{
  return frame->f_lineno;
}

#endif // !(PY_VERSION_HEX < 0x02070000)

VISIBLE SCM
scm_pyeval_getbuiltins (void)
{
  PyObject *obj = PyEval_GetBuiltins ();
  return scm_c_incref_make_pyobject (obj);
}

VISIBLE SCM
scm_pyeval_getlocals (void)
{
  PyObject *obj = PyEval_GetLocals ();
  return (obj == NULL) ? SCM_BOOL_F : scm_c_incref_make_pyobject (obj);
}

VISIBLE SCM
scm_pyeval_getglobals (void)
{
  PyObject *obj = PyEval_GetGlobals ();
  return (obj == NULL) ? SCM_BOOL_F : scm_c_incref_make_pyobject (obj);
}

VISIBLE SCM
scm_pyeval_getframe (void)
{
  PyObject *obj = (PyObject *) PyEval_GetFrame ();
  return (obj == NULL) ? SCM_BOOL_F : scm_c_incref_make_pyobject (obj);
}

VISIBLE SCM
scm_pyframe_getlinenumber (SCM frame)
{
  scm_assert_pyobject (frame);
  PyObject *_frame = scm_to_PyObject_ptr (frame);
  SCM_ASSERT_TYPE (PyFrame_Check (_frame), frame, SCM_ARG1,
                   "pyframe-getlinenumber", "Python frame object");
  return scm_from_int (PyFrame_GetLineNumber ((PyFrameObject *) _frame));
}

VISIBLE SCM
scm_pyframe_getfilename (SCM frame)
{
  // Get file name, given a Python execution frame. The data is
  // extracted from the frame structures directly rather than through
  // a CPython API function, so beware.

  scm_assert_pyobject (frame);
  PyObject *_frame = scm_to_PyObject_ptr (frame);
  SCM_ASSERT_TYPE (PyFrame_Check (_frame), frame, SCM_ARG1,
                   "pyframe-getfilename", "Python frame object");
  PyFrameObject *f = (PyFrameObject *) _frame;
  return (f->f_code->co_filename != NULL) ?
    scm_pyunicode_to_string (scm_c_make_pyobject (f->f_code->co_filename),
                             SCM_UNDEFINED) : scm_py_none ();
}

VISIBLE SCM
scm_pyeval_getfuncname (SCM func)
{
  scm_assert_pyobject (func);
  PyObject *_func = scm_to_PyObject_ptr (func);
  const char *s = PyEval_GetFuncName (_func);
  // FIXME: Is (s == NULL) possible? I suspect it is not.
  return (s == NULL) ? SCM_BOOL_F : scm_from_locale_string (s);
}

VISIBLE SCM
scm_pyeval_getfuncdesc (SCM func)
{
  scm_assert_pyobject (func);
  PyObject *_func = scm_to_PyObject_ptr (func);
  const char *s = PyEval_GetFuncDesc (_func);
  // FIXME: Is (s == NULL) possible? I suspect it is not.
  return (s == NULL) ? SCM_BOOL_F : scm_from_locale_string (s);
}

/////////////VISIBLE SCM
/////////////scm_pytraceback_here (SCM frame)
/////////////{
/////////////  // Warning: PyTraceBack_Here seems to be undocumented.
/////////////
/////////////  scm_assert_pyobject (frame);
/////////////  PyObject *_frame = scm_to_PyObject_ptr (frame);
/////////////  SCM_ASSERT_TYPE (PyFrame_Check (_frame), frame, SCM_ARG1,
/////////////                   "pytraceback-here", "Python frame object");
/////////////  int errval = PyTraceBack_Here (_frame);
/////////////  py_exc_check ((errval != -1), "pytraceback-here", scm_list_1 (frame));
/////////////  return SCM_UNSPECIFIED;
/////////////}
