#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libintl.h>
#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>

#define _HOBBY_MODULE "sortsmill core hobby"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

VISIBLE SCM
scm_hobby_tensions_to_control_points (SCM perhaps_increase_tensions_p,
                                      SCM p0, SCM dir0, SCM tension0,
                                      SCM tension3, SCM dir3, SCM p3)
{
  const bool _perhaps_increase_tensions_p =
    scm_is_true (perhaps_increase_tensions_p);
  const double complex p0_ = scm_to_double_complex (p0);
  const double complex p3_ = scm_to_double_complex (p3);
  const double complex dir0_ = scm_to_double_complex (dir0);
  const double complex dir3_ = scm_to_double_complex (dir3);
  const double tension0_ = scm_to_double (tension0);
  const double tension3_ = scm_to_double (tension3);
  double complex p1_;
  double complex p2_;
  hobby_tensions_to_control_points (_perhaps_increase_tensions_p,
                                    p0_, dir0_, tension0_, tension3_,
                                    dir3_, p3_, &p1_, &p2_);

  SCM values[2] = {
    scm_from_double_complex (p1_),
    scm_from_double_complex (p2_)
  };
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_control_points_to_hobby_tensions (SCM p0, SCM p1, SCM p2, SCM p3)
{
  const double complex p0_ = scm_to_double_complex (p0);
  const double complex p1_ = scm_to_double_complex (p1);
  const double complex p2_ = scm_to_double_complex (p2);
  const double complex p3_ = scm_to_double_complex (p3);
  double tension0_;
  double tension3_;
  control_points_to_hobby_tensions (p0_, p1_, p2_, p3_, &tension0_, &tension3_);
  SCM values[2] = {
    scm_from_double (tension0_),
    scm_from_double (tension3_)
  };
  return scm_c_values (values, 2);
}

static size_t
get_system_size (SCM a_tensions, SCM b_tensions, SCM points)
{
  const char *who = "hobby-guess-directions";

  const size_t n_a_tensions = scm_to_size_t (scm_f64vector_length (a_tensions));
  const size_t n_b_tensions = scm_to_size_t (scm_f64vector_length (b_tensions));
  const size_t n_points = scm_to_size_t (scm_c64vector_length (points));

  if (n_points < 2)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("too few points")),
        rnrs_make_irritants_condition (scm_list_1 (points))));
  if (n_a_tensions != n_points - 1)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("wrong number of first tensions")),
        rnrs_make_irritants_condition (scm_list_1 (a_tensions))));
  if (n_b_tensions != n_points - 1)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("wrong number of second tensions")),
        rnrs_make_irritants_condition (scm_list_1 (b_tensions))));

  return n_points;
}

VISIBLE SCM
scm_hobby_guess_directions (SCM start_dir, SCM end_dir,
                            SCM start_curl, SCM end_curl,
                            SCM a_tensions, SCM b_tensions, SCM points)
{
  const char *who = "hobby-guess-directions";

  // Accept any combination of lists, vectors, and uniform vectors.
  a_tensions = scm_any_to_f64vector (a_tensions);
  b_tensions = scm_any_to_f64vector (b_tensions);
  points = scm_any_to_c64vector (points);

  // A vanishing direction is equivalent to #f.
  const double complex _start_dir =
    (scm_is_true (start_dir)) ? scm_to_double_complex (start_dir) : 0;
  const double complex _end_dir =
    (scm_is_true (end_dir)) ? scm_to_double_complex (end_dir) : 0;

  // Curl defaults to 1.
  const double _start_curl =
    (scm_is_true (start_curl)) ? scm_to_double (start_curl) : 1;
  const double _end_curl =
    (scm_is_true (end_curl)) ? scm_to_double (end_curl) : 1;

  const size_t n = get_system_size (a_tensions, b_tensions, points);
  double *_a_tensions =
    scm_gc_malloc_pointerless ((n - 1) * sizeof (double), "");
  double *_b_tensions =
    scm_gc_malloc_pointerless ((n - 1) * sizeof (double), "");
  double complex *_points =
    scm_gc_malloc_pointerless (n * sizeof (double complex), "");
  double complex *_directions =
    scm_gc_malloc_pointerless (n * sizeof (double complex), "");

  for (size_t _i = 0; _i < n - 1; _i++)
    {
      // NOTE: This loop probably can be sped up by using array
      // handles.
      const SCM i = scm_from_size_t (_i);
      _a_tensions[_i] = scm_to_double (scm_f64vector_ref (a_tensions, i));
      _b_tensions[_i] = scm_to_double (scm_f64vector_ref (b_tensions, i));
    }

  for (size_t _i = 0; _i < n; _i++)
    {
      // NOTE: This loop probably can be sped up by using array
      // handles.
      const SCM i = scm_from_size_t (_i);
      _points[_i] = scm_to_double_complex (scm_c64vector_ref (points, i));
    }

  int info;
  const char *info_message;
  hobby_guess_directions (_start_dir, _end_dir, _start_curl, _end_curl,
                          n, _a_tensions, _b_tensions, _points, _directions,
                          &info, &info_message);
  if (info != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (info_message),
        rnrs_make_irritants_condition (scm_list_n (start_dir, end_dir,
                                                   start_curl, end_curl,
                                                   a_tensions, b_tensions,
                                                   points, SCM_UNDEFINED))));

  double *dir_data = scm_calloc (2 * n * sizeof (double));
  SCM directions = scm_take_c64vector (dir_data, n);
  for (size_t i = 0; i < n; i++)
    {
      const double complex z = _directions[i];
      dir_data[2 * i] = creal (z);
      dir_data[2 * i + 1] = cimag (z);
    }

  return directions;
}

static size_t
get_periodic_system_size (SCM a_tensions, SCM b_tensions, SCM points)
{
  const char *who = "hobby-periodic-guess-directions";

  const size_t n_a_tensions = scm_to_size_t (scm_f64vector_length (a_tensions));
  const size_t n_b_tensions = scm_to_size_t (scm_f64vector_length (b_tensions));
  const size_t n_points = scm_to_size_t (scm_c64vector_length (points));

  if (n_points < 2)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("too few points")),
        rnrs_make_irritants_condition (scm_list_1 (points))));
  if (n_a_tensions != n_points)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("wrong number of first tensions")),
        rnrs_make_irritants_condition (scm_list_1 (a_tensions))));
  if (n_b_tensions != n_points)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("wrong number of second tensions")),
        rnrs_make_irritants_condition (scm_list_1 (b_tensions))));

  return n_points;
}

VISIBLE SCM
scm_hobby_periodic_guess_directions (SCM a_tensions, SCM b_tensions, SCM points)
{
  const char *who = "hobby-periodic-guess-directions";

  // Accept any combination of lists, vectors, and uniform vectors.
  a_tensions = scm_any_to_f64vector (a_tensions);
  b_tensions = scm_any_to_f64vector (b_tensions);
  points = scm_any_to_c64vector (points);

  const size_t n = get_periodic_system_size (a_tensions, b_tensions, points);
  double *_a_tensions =
    scm_gc_malloc_pointerless ((n - 1) * sizeof (double), "");
  double *_b_tensions =
    scm_gc_malloc_pointerless ((n - 1) * sizeof (double), "");
  double complex *_points =
    scm_gc_malloc_pointerless (n * sizeof (double complex), "");
  double complex *_directions =
    scm_gc_malloc_pointerless (n * sizeof (double complex), "");

  for (size_t _i = 0; _i < n; _i++)
    {
      // NOTE: This loop probably can be sped up by using array
      // handles.
      const SCM i = scm_from_size_t (_i);
      _a_tensions[_i] = scm_to_double (scm_f64vector_ref (a_tensions, i));
      _b_tensions[_i] = scm_to_double (scm_f64vector_ref (b_tensions, i));
      _points[_i] = scm_to_double_complex (scm_c64vector_ref (points, i));
    }

  int info;
  const char *info_message;
  hobby_periodic_guess_directions (n, _a_tensions, _b_tensions, _points,
                                   _directions, &info, &info_message);
  if (info != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (info_message),
        rnrs_make_irritants_condition (scm_list_3 (a_tensions, b_tensions,
                                                   points))));

  double *dir_data = scm_calloc (2 * n * sizeof (double));
  SCM directions = scm_take_c64vector (dir_data, n);
  for (size_t i = 0; i < n; i++)
    {
      const double complex z = _directions[i];
      dir_data[2 * i] = creal (z);
      dir_data[2 * i + 1] = cimag (z);
    }

  return directions;
}

static SCM
rnrs_make_hobby_guide_tokens_status_condition (SCM tokens, SCM info,
                                               SCM bad_token_index)
{
  return
    scm_call_3 (scm_c_public_ref (_HOBBY_MODULE,
                                  "make-hobby-guide-tokens-status-condition"),
                tokens, info, bad_token_index);
}

VISIBLE SCM
scm_solve_hobby_guide_tokens (SCM input_tokens)
{
  const char *who = "solve-hobby-guide";

  scm_t_array_handle handle;
  size_t len;
  ssize_t inc;
  const double *_input_tokens =
    scm_f64vector_elements (scm_any_to_f64vector (input_tokens), &handle, &len,
                            &inc);
  const size_t n = len / 3;
  if (inc != 1)
    {
      double *_new_array =
        scm_gc_malloc_pointerless (3 * n * sizeof (double), "");
      for (size_t i = 0; i < 3 * n; i++)
        {
          _new_array[i] = *_input_tokens;
          _input_tokens++;
        }
      _input_tokens = _new_array;
    }

  double *_output_tokens = scm_malloc (((9 * n) / 2) * sizeof (double));
  size_t n_output;
  int info;
  const char *info_message;
  size_t bad_token_index = 0;
  solve_hobby_guide_tokens (n, _input_tokens, &n_output, _output_tokens, &info,
                            &info_message, &bad_token_index);

  scm_array_handle_release (&handle);

  if (info != 0)
    {
      free (_output_tokens);
      rnrs_raise_condition
        (scm_list_5
         (rnrs_make_error (),
          rnrs_c_make_who_condition (who),
          rnrs_c_make_message_condition (info_message),
          rnrs_make_irritants_condition (scm_list_1 (input_tokens)),
          rnrs_make_hobby_guide_tokens_status_condition
          (input_tokens, scm_from_int (info),
           scm_from_size_t (bad_token_index))));
    }

  return
    scm_take_f64vector (scm_realloc (_output_tokens,
                                     3 * n_output * sizeof (double)),
                        3 * n_output);
}
