#! /usr/bin/nawk -f

# Copyright (C) 2013 Khaled Hosny and Barry Schwartz
#
# This file is part of the Sorts Mill Core Guile.
# 
# Sorts Mill Core Guile is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill Core Guile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

BEGIN {
    scm_module_name = ARGV[1]
    ARGV[1] = ""

    num_exports = 0
    num_entries = 0
}

# Convert `#[[:space:]]+define' to `#define', so it will count as just
# one field.
/^[[:space:]]*#[[:space:]]+define[[:space:]]+/ {
    sub (/#[[:space:]]+define/, "#define")
}

/^[[:space:]]*#define[[:space:]]+/ {
    if ($3 ~ /^"/)
	{
	    # A quoted string. Convert it to a comment.
	    $0 = ";; "$0
	}
    else if ($2 ~ /_IN_UTF8$/ && $3 ~ /^{/)
	{
	    # The UTF-8-encoded initializers become Scheme strings.
	    sub(/_IN_UTF8$/, "", $2)
	    gsub(/,/, " ")
	    sub(/{/, "(utf8->string #vu8(", $3)
	    if ($(NF - 1) ~ /^0$/) $(NF - 1) = ""
	    sub(/}/, "))", $NF)
	    gsub(/[[:space:]]+/, " ")

	    # Collect the variable name, for export.
	    num_exports++
	    exports[num_exports] = $2
	}
    else
	{
	    # An unquoted entry. Collect the variable name, for
	    # export.
	    num_exports++
	    exports[num_exports] = $2
	}
    sub(/#define/, "(define")
    sub(/$/, ")")
    num_entries++
    entries[num_entries] = $0
    next
}

# Anything remaining, unless it is a blank line, becomes a comment.
$0 !~ /^[[:space:]]*$/ {
    sub(/^/, ";; ")
}

{
    num_entries++
    entries[num_entries] = $0
}

END {
    print ";; -*- coding: utf-8 -*-\n"
    print "(library ("scm_module_name")\n"
    if (num_exports == 1)
	print "  (export "exports[1]")\n"
    else if (1 < num_exports)
	{
	    print "  (export "exports[1]
	    for (i = 2; i < num_exports; i++)
		print "          "exports[i]
	    print "          "exports[num_exports]")\n"
	}
    print "  (import (rnrs base)"
    print "          (rnrs bytevectors))\n"
    for (i = 1; i < num_entries; i++)
	print "  "entries[i]
    print "  "entries[i]")"
}
