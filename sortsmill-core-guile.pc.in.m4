include(`m4citrus.m4')dnl
m4_divert(-1)
m4_include([smcoreguile-pkginfo.m4])
m4_include([smcoreguile-dirlayout.m4])
m4_include([smcoreguile-buildinfo.m4])
m4_include([vars.m4])
m4_define([expand_var], [$1=$2])
m4_define([expand_vars_list],
          [m4_ifnblank([$2], [@%:@ $2
])m4_map_sep([expand_var], [m4_newline], m4_split(m4_normalize($1)))])
m4_divert[]# This file was generated with GNU m4.

expand_vars_list([SMCOREGUILE_PKGINFO_LIST], [Package information.])

expand_vars_list([SMCOREGUILE_DIRLAYOUT_LIST], [Directories.])

expand_vars_list([SMCOREGUILE_BUILDINFO_LIST], [Configuration options.])

# Actual name of the 'sortsmill-core-guile' program. This is
# particularly useful when '--program-transform-name' or similar has
# been used.
sortsmill_core_guile=${bindir}/@core_guile_name@

[GUILE_EFFECTIVE_VERSION=]GUILE_EFFECTIVE_VERSION

Name: ${PACKAGE}-${[GUILE_EFFECTIVE_VERSION]}
Description: ${PACKAGE_NAME} for Guile ${[GUILE_EFFECTIVE_VERSION]}
Version: ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}
Requires.private: guile-${[GUILE_EFFECTIVE_VERSION]} sortsmill-core
Libs: -L${libdir} -lsortsmill-core-guile-${[GUILE_EFFECTIVE_VERSION]}
Cflags: -I${hostincludedir} -I${includedir}
