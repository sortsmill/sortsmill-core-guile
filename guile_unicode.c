#include <config.h>

// Copyright (C) 2013 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libguile.h>
#include <sortsmill/core.h>

void init_sortsmill_core_guile_unicode (void);

VISIBLE void
init_sortsmill_core_guile_unicode (void)
{
  scm_c_define ("UNINAME_MAX", scm_from_size_t (UNINAME_MAX));
  scm_c_define ("UNINAME_INVALID", scm_from_uint32 (UNINAME_INVALID));
}
