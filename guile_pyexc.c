#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define _PYEXCS_MODULE "sortsmill core python pyexcs"

//-------------------------------------------------------------------------

#if PY_VERSION_HEX < 0x03040000
#define _CONST_CHAR_PTR_WORKAROUND(x) ((char *) (x))
#else
#define _CONST_CHAR_PTR_WORKAROUND(x) (x)
#endif

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_pyerr_print_x (SCM set_sys_last_vars_p)
{
  if (PyErr_Occurred ())
    PyErr_PrintEx (!unbndp_or_false (set_sys_last_vars_p));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyerr_occurred_p (void)
{
  return scm_from_bool (PyErr_Occurred ());
}

VISIBLE SCM
scm_pyerr_exceptionmatches_p (SCM exc)
{
  int matches;
  if (PyErr_Occurred ())
    {
      scm_assert_pyobject (exc);
      PyObject *_exc = scm_to_PyObject_ptr (exc);
      matches = PyErr_ExceptionMatches (_exc);
      py_exc_check ((matches != -1), "pyerr-exceptionmatches",
                    scm_list_1 (exc));
    }
  else
    matches = false;
  return scm_from_bool (matches);
}

VISIBLE SCM
scm_pyerr_givenexceptionmatches_p (SCM given, SCM exc)
{
  scm_assert_pyobject (given);
  PyObject *_given = scm_to_PyObject_ptr (given);

  scm_assert_pyobject (exc);
  PyObject *_exc = scm_to_PyObject_ptr (exc);

  const int matches = PyErr_GivenExceptionMatches (_given, _exc);
  py_exc_check ((matches != -1), "pyerr-givenexceptionmatches",
                scm_list_2 (given, exc));

  return scm_from_bool (matches);
}

VISIBLE SCM
scm_pyerr_normalizeexception_x (SCM type, SCM value, SCM traceback)
{
  PyObject *_type = NULL;
  PyObject *_value = NULL;
  PyObject *_traceback = NULL;

  if (scm_is_true (type))
    {
      scm_assert_pyobject (type);
      _type = scm_to_PyObject_ptr (type);
      Py_INCREF (_type);
    }
  if (scm_is_true (value))
    {
      scm_assert_pyobject (value);
      _value = scm_to_PyObject_ptr (value);
      Py_INCREF (_value);
    }
  if (scm_is_true (traceback))
    {
      scm_assert_pyobject (traceback);
      _traceback = scm_to_PyObject_ptr (traceback);
      Py_INCREF (_traceback);
    }

  PyErr_NormalizeException (&_type, &_value, &_traceback);

  SCM values[3] = {
    ((_type == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (_type)),
    ((_value == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (_value)),
    ((_traceback == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (_traceback))
  };
  return scm_c_values (values, 3);
}

VISIBLE SCM
scm_pyerr_clear_x (void)
{
  PyErr_Clear ();
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyerr_fetch_x (void)
{
  PyObject *type;
  PyObject *value;
  PyObject *traceback;
  PyErr_Fetch (&type, &value, &traceback);
  SCM values[3] = {
    ((type == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (type)),
    ((value == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (value)),
    ((traceback == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (traceback))
  };
  return scm_c_values (values, 3);
}

VISIBLE SCM
scm_pyerr_restore_x (SCM type, SCM value, SCM traceback)
{
  PyObject *_type = NULL;
  PyObject *_value = NULL;
  PyObject *_traceback = NULL;

  if (scm_is_true (type))
    {
      scm_assert_pyobject (type);
      _type = scm_to_PyObject_ptr (type);
      Py_INCREF (_type);

      if (scm_is_true (value))
        {
          scm_assert_pyobject (value);
          _value = scm_to_PyObject_ptr (value);
          Py_INCREF (_value);
        }
      if (scm_is_true (traceback))
        {
          scm_assert_pyobject (traceback);
          _traceback = scm_to_PyObject_ptr (traceback);
          Py_INCREF (_traceback);
        }
    }

  PyErr_Restore (_type, _value, _traceback);

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyerr_set_x (SCM exc, SCM obj)
{
  // Let @var{obj} be a Guile string.
  SCM pyobj = scm_scm_to_pyunicode (obj);

  scm_assert_pyobject (exc);
  PyObject *_exc = scm_to_PyObject_ptr (exc);

  scm_assert_pyobject (pyobj);
  PyObject *_pyobj = scm_to_PyObject_ptr (pyobj);

  PyErr_SetObject (_exc, _pyobj);

  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------

#define _PYEXC_FUNC(GUILENAME, CNAME)           \
  SCM                                           \
  scm_pyexc_##GUILENAME (void)                  \
  {                                             \
    Py_INCREF (PyExc_##CNAME);                  \
    return scm_c_make_pyobject (PyExc_##CNAME); \
  }

// FIXME: Give these procedures documentation strings.
#define _DEF_PYEXC(GUILENAME)                                           \
  scm_c_define_gsubr ("pyexc-" #GUILENAME, 0, 0, 0, scm_pyexc_##GUILENAME)

static _PYEXC_FUNC (baseexception, BaseException);
static _PYEXC_FUNC (exception, Exception);
#if PY_MAJOR_VERSION < 3
static _PYEXC_FUNC (standarderror, StandardError);
#endif
static _PYEXC_FUNC (arithmeticerror, ArithmeticError);
static _PYEXC_FUNC (lookuperror, LookupError);
static _PYEXC_FUNC (assertionerror, AssertionError);
static _PYEXC_FUNC (attributeerror, AttributeError);
static _PYEXC_FUNC (eoferror, EOFError);
static _PYEXC_FUNC (environmenterror, EnvironmentError);
static _PYEXC_FUNC (floatingpointerror, FloatingPointError);
static _PYEXC_FUNC (ioerror, IOError);
static _PYEXC_FUNC (importerror, ImportError);
static _PYEXC_FUNC (indexerror, IndexError);
static _PYEXC_FUNC (keyerror, KeyError);
static _PYEXC_FUNC (keyboardinterrupt, KeyboardInterrupt);
static _PYEXC_FUNC (memoryerror, MemoryError);
static _PYEXC_FUNC (nameerror, NameError);
static _PYEXC_FUNC (notimplementederror, NotImplementedError);
static _PYEXC_FUNC (oserror, OSError);
static _PYEXC_FUNC (overflowerror, OverflowError);
static _PYEXC_FUNC (referenceerror, ReferenceError);
static _PYEXC_FUNC (runtimeerror, RuntimeError);
static _PYEXC_FUNC (syntaxerror, SyntaxError);
static _PYEXC_FUNC (systemerror, SystemError);
static _PYEXC_FUNC (systemexit, SystemExit);
static _PYEXC_FUNC (typeerror, TypeError);
static _PYEXC_FUNC (valueerror, ValueError);
#ifdef MS_WINDOWS
static _PYEXC_FUNC (windowserror, WindowsError);
#endif
static _PYEXC_FUNC (zerodivisionerror, ZeroDivisionError);

#if 0x03030000 <= PY_VERSION_HEX
static _PYEXC_FUNC (blockingioerror, BlockingIOError);
static _PYEXC_FUNC (brokenpipeerror, BrokenPipeError);
static _PYEXC_FUNC (childprocesserror, ChildProcessError);
static _PYEXC_FUNC (connectionerror, ConnectionError);
static _PYEXC_FUNC (connectionabortederror, ConnectionAbortedError);
static _PYEXC_FUNC (connectionrefusederror, ConnectionRefusedError);
static _PYEXC_FUNC (connectionreseterror, ConnectionResetError);
static _PYEXC_FUNC (fileexistserror, FileExistsError);
static _PYEXC_FUNC (filenotfounderror, FileNotFoundError);
static _PYEXC_FUNC (interruptederror, InterruptedError);
static _PYEXC_FUNC (isadirectoryerror, IsADirectoryError);
static _PYEXC_FUNC (notadirectoryerror, NotADirectoryError);
static _PYEXC_FUNC (permissionerror, PermissionError);
static _PYEXC_FUNC (processlookuperror, ProcessLookupError);
static _PYEXC_FUNC (timeouterror, TimeoutError);
#endif // !(0x03030000 <= PY_VERSION_HEX)

//-------------------------------------------------------------------------

void init_guile_pyexc (void);

VISIBLE void
init_guile_pyexc (void)
{
  _DEF_PYEXC (baseexception);
  _DEF_PYEXC (exception);
#if PY_MAJOR_VERSION < 3
  _DEF_PYEXC (standarderror);
#endif
  _DEF_PYEXC (arithmeticerror);
  _DEF_PYEXC (lookuperror);
  _DEF_PYEXC (assertionerror);
  _DEF_PYEXC (attributeerror);
  _DEF_PYEXC (eoferror);
  _DEF_PYEXC (environmenterror);
  _DEF_PYEXC (floatingpointerror);
  _DEF_PYEXC (ioerror);
  _DEF_PYEXC (importerror);
  _DEF_PYEXC (indexerror);
  _DEF_PYEXC (keyerror);
  _DEF_PYEXC (keyboardinterrupt);
  _DEF_PYEXC (memoryerror);
  _DEF_PYEXC (nameerror);
  _DEF_PYEXC (notimplementederror);
  _DEF_PYEXC (oserror);
  _DEF_PYEXC (overflowerror);
  _DEF_PYEXC (referenceerror);
  _DEF_PYEXC (runtimeerror);
  _DEF_PYEXC (syntaxerror);
  _DEF_PYEXC (systemerror);
  _DEF_PYEXC (systemexit);
  _DEF_PYEXC (typeerror);
  _DEF_PYEXC (valueerror);
#ifdef MS_WINDOWS
  _DEF_PYEXC (windowserror);
#endif
  _DEF_PYEXC (zerodivisionerror);

#if 0x03030000 <= PY_VERSION_HEX
  _DEF_PYEXC (blockingioerror);
  _DEF_PYEXC (brokenpipeerror);
  _DEF_PYEXC (childprocesserror);
  _DEF_PYEXC (connectionerror);
  _DEF_PYEXC (connectionabortederror);
  _DEF_PYEXC (connectionrefusederror);
  _DEF_PYEXC (connectionreseterror);
  _DEF_PYEXC (fileexistserror);
  _DEF_PYEXC (filenotfounderror);
  _DEF_PYEXC (interruptederror);
  _DEF_PYEXC (isadirectoryerror);
  _DEF_PYEXC (notadirectoryerror);
  _DEF_PYEXC (permissionerror);
  _DEF_PYEXC (processlookuperror);
  _DEF_PYEXC (timeouterror);
#endif // !(0x03030000 <= PY_VERSION_HEX)
}

//-------------------------------------------------------------------------

VISIBLE SCM
rnrs_make_py_exc_info_condition (SCM type, SCM value, SCM traceback)
{
  return scm_call_3 (scm_c_private_ref (_PYEXCS_MODULE, "make-py-exc-info"),
                     type, value, traceback);
}

VISIBLE SCM
rnrs_c_make_py_exc_info_condition (PyObject *type, PyObject *value,
                                   PyObject *traceback)
{
  SCM scmtype = (type == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (type);
  SCM scmvalue = (value == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (value);
  SCM scmtraceback =
    (traceback == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (traceback);
  return rnrs_make_py_exc_info_condition (scmtype, scmvalue, scmtraceback);
}

VISIBLE SCM
rnrs_fetch_py_exc_info_condition (void)
{
  PyObject *type;
  PyObject *value;
  PyObject *traceback;
  PyErr_Fetch (&type, &value, &traceback);
  return rnrs_c_make_py_exc_info_condition (type, value, traceback);
}

VISIBLE void
scm_c_py_exc_rnrs_raise_condition (const char *who, const char *message,
                                   SCM irritants)
{
  rnrs_raise_condition
    (scm_list_5
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition (who),
      rnrs_c_make_message_condition (message),
      rnrs_make_irritants_condition (irritants),
      rnrs_fetch_py_exc_info_condition ()));
}

VISIBLE SCM
scm_py_exc_rnrs_raise_condition (SCM who, SCM message, SCM irritants)
{
  rnrs_raise_condition
    (scm_list_5
     (rnrs_make_assertion_violation (),
      rnrs_make_who_condition (who),
      rnrs_make_message_condition (message),
      rnrs_make_irritants_condition (irritants),
      rnrs_fetch_py_exc_info_condition ()));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_py_exc_check_violation (SCM who, SCM message, SCM irritants)
{
  rnrs_raise_condition
    (scm_list_5
     (rnrs_make_assertion_violation (),
      rnrs_make_who_condition (who),
      rnrs_make_message_condition (message),
      rnrs_make_irritants_condition (irritants),
      rnrs_fetch_py_exc_info_condition ()));
  return SCM_UNSPECIFIED;
}

VISIBLE void
scm_c_py_exc_check_violation (const char *who, const char *message,
                              SCM irritants)
{
  scm_py_exc_check_violation (scm_from_utf8_string (who),
                              scm_from_locale_string (message), irritants);
}

//-------------------------------------------------------------------------
