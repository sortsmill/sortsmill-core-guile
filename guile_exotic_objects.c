#include <config.h>

// Copyright (C) 2017 Khaled Hosny and Barry Schwartz
// Copyright (C) 2014 Free Software Foundation, Inc.
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

// This code is based loosely on the foreign objects implementation in
// Guile 2.2.2.

#include <stdbool.h>
#include <libguile.h>
#include <sortsmill/guile/core/exotic_objects.h>
#include <sortsmill/guile/core/scm_one_time_initialization.h>

#define EXOTIC_OBJECTS_MODULE__ "sortsmill core exotic-objects"

STM_SCM_ONE_TIME_INITIALIZE (static, SCM, make_exotic_object_type_,
                             make_exotic_object_type___Value =
                             scm_c_private_lookup (EXOTIC_OBJECTS_MODULE__,
                                                   "make-exotic-object-type"));
#if defined (SCM_IS_A_P)

VISIBLE bool exotic_objects_scm_is_a_p__ (SCM val, SCM type);

#else /* !defined (SCM_IS_A_P) */

#warning The SCM_IS_A_P macro was not found, and we are using a slow workaround.

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, scm_is_a_p__,
                             (scm_is_a_p____Value =
                              scm_c_public_ref ("oop goops", "is-a?")));

VISIBLE bool
exotic_objects_scm_is_a_p__ (SCM val, SCM type)
{
  return scm_is_true (scm_call_2 (scm_is_a_p__ (), val, type));
}

#endif /* !defined (SCM_IS_A_P) */

// Addressable instances of inline functions.
VISIBLE bool scm_is_exotic_object (SCM val, SCM type);
VISIBLE SCM scm_exotic_object_p (SCM val, SCM type);
VISIBLE void scm_assert_exotic_object_type (SCM type, SCM val);
VISIBLE void *scm_c_exotic_object_ref (SCM type, SCM obj);
VISIBLE void scm_c_exotic_object_set_x (SCM type, SCM obj, const void *val);
VISIBLE void *scm_c_unchecked_exotic_object_ref (SCM obj);
VISIBLE void scm_c_unchecked_exotic_object_set_x (SCM obj, const void *val);
VISIBLE SCM scm_exotic_object_ref (SCM type, SCM obj);
VISIBLE void scm_exotic_object_set_x (SCM type, SCM obj, SCM val);
VISIBLE SCM scm_unchecked_exotic_object_ref (SCM type, SCM obj);
VISIBLE void scm_unchecked_exotic_object_set_x (SCM type, SCM obj, SCM val);

VISIBLE void
scm_raise_exotic_object_type_assertion_failure__ (SCM type, SCM val)
{
  scm_error (scm_arg_type_key, NULL, "Wrong type (expecting ~A): ~S",
             scm_list_2 (scm_class_name (type), val), scm_list_1 (val));
}

VISIBLE SCM
scm_c_utf8_make_exotic_object_type (const char *name)
{
  return scm_make_exotic_object_type (scm_from_utf8_symbol (name));
}

VISIBLE SCM
scm_make_exotic_object_type (SCM name)
{
  return scm_call_1 (scm_variable_ref (make_exotic_object_type_ ()), name);
}

VISIBLE SCM
scm_c_make_exotic_object (SCM type, const void *val)
{
  SCM_ASSERT (scm_is_true (scm_struct_vtable_p (type)), type, SCM_ARG1,
              "make-exotic-object");
  SCM obj = scm_c_make_structv (type, 0, 0, NULL);
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
  return obj;
}

VISIBLE SCM
scm_make_exotic_object (SCM type, SCM val)
{
  SCM_ASSERT (scm_is_true (scm_struct_vtable_p (type)), type, SCM_ARG1,
              "make-exotic-object");
  SCM obj = scm_c_make_structv (type, 0, 0, NULL);
  ((scm_t_bits *) SCM_CELL_WORD_1 (obj))[0] = (scm_t_bits) val;
  return obj;
}
