#include <config.h>

// Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <assert.h>
#include <sortsmill/guile/core.h>

void guile_init_smcoreguile_hashmaps (void);

//----------------------------------------------------------------------

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM, scm_hashmap_type__,
                             scm_hashmap_type____Value =
                             scm_c_utf8_make_exotic_object_type ("<hashmap>"));

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM,
                             scm_hashmap_cursor_type__,
                             scm_hashmap_cursor_type____Value =
                             scm_c_utf8_make_exotic_object_type
                             ("<hashmap-cursor>"));

static inline void
scm_assert_hashmap__ (SCM obj)
{
  scm_assert_exotic_object_type (scm_hashmap_type__ (), obj);
}

VISIBLE scm_t_hashmap
scm_to_scm_t_hashmap (SCM obj)
{
  scm_assert_hashmap__ (obj);
  return scm_c_exotic_object_ref (scm_hashmap_type__ (), obj);
}

VISIBLE SCM
scm_from_scm_t_hashmap (scm_t_hashmap p)
{
  return scm_c_make_exotic_object (scm_hashmap_type__ (), (void *) p);
}

static inline void
scm_assert_hashmap_cursor__ (SCM obj)
{
  scm_assert_exotic_object_type (scm_hashmap_cursor_type__ (), obj);
}

VISIBLE scm_t_hashmap_cursor
scm_to_scm_t_hashmap_cursor (SCM obj)
{
  scm_assert_hashmap_cursor__ (obj);
  return scm_c_exotic_object_ref (scm_hashmap_cursor_type__ (), obj);
}

VISIBLE SCM
scm_from_scm_t_hashmap_cursor (scm_t_hashmap_cursor p)
{
  return scm_c_make_exotic_object (scm_hashmap_cursor_type__ (), (void *) p);
}

VISIBLE bool
scm_is_hashmap (SCM obj)
{
  return scm_is_exotic_object (obj, scm_hashmap_type__ ());
}

VISIBLE SCM
scm_hashmap_p (SCM obj)
{
  return scm_exotic_object_p (obj, scm_hashmap_cursor_type__ ());
}

VISIBLE bool
scm_is_hashmap_cursor (SCM obj)
{
  return scm_is_exotic_object (obj, scm_hashmap_cursor_type__ ());
}

VISIBLE SCM
scm_hashmap_cursor_p (SCM obj)
{
  return scm_exotic_object_p (obj, scm_hashmap_cursor_type__ ());
}

VISIBLE void
guile_init_smcoreguile_hashmaps (void)
{
  scm_c_define ("<hashmap>", scm_hashmap_type__ ());
  scm_c_define ("<hashmap-cursor>", scm_hashmap_cursor_type__ ());
}

//----------------------------------------------------------------------
