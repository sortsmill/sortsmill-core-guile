// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

VISIBLE SCM
SCM_CYCLICTRIDIAGSOLVE (SCM subdiag, SCM diag, SCM superdiag,
                        SCM lower_left, SCM upper_right, SCM B)
{
  const char *who = CYCLICTRIDIAGSOLVE_STR;

  REAL lower_left_ = scm_to_double (lower_left);
  REAL upper_right_ = scm_to_double (upper_right);

  scm_dynwind_begin (0);

  scm_t_array_handle handle_subdiag;
  scm_t_array_handle handle_diag;
  scm_t_array_handle handle_superdiag;
  scm_t_array_handle handle_B;

  scm_array_get_handle (subdiag, &handle_subdiag);
  scm_dynwind_array_handle_release (&handle_subdiag);
  scm_array_get_handle (diag, &handle_diag);
  scm_dynwind_array_handle_release (&handle_diag);
  scm_array_get_handle (superdiag, &handle_superdiag);
  scm_dynwind_array_handle_release (&handle_superdiag);
  scm_array_get_handle (B, &handle_B);
  scm_dynwind_array_handle_release (&handle_B);

  int n;
  int nrhs;
  size_t rank_of_B;
  ssize_t row_inc;
  ssize_t col_inc;
  get_tridiag_argument_dimensions (who, subdiag, diag, superdiag, B,
                                   &handle_subdiag, &handle_diag,
                                   &handle_superdiag, &handle_B,
                                   &n, &nrhs, &rank_of_B, &row_inc, &col_inc);
  check_cyclic_tridiag_dimensions (who, n, nrhs, subdiag, diag, superdiag, B);

  REAL *subdiag_ = scm_malloc (_MAX (1, n - 1) * sizeof (REAL));
  scm_dynwind_free (subdiag_);
  REAL *diag_ = scm_malloc (n * sizeof (REAL));
  scm_dynwind_free (diag_);
  REAL *superdiag_ = scm_malloc (_MAX (1, n - 1) * sizeof (REAL));
  scm_dynwind_free (superdiag_);
  REAL *B_ = scm_malloc (_MAX (1, n * (nrhs + 1) * sizeof (REAL)));
  scm_dynwind_free (B_);

  const scm_t_array_dim *dims_subdiag = scm_array_handle_dims (&handle_subdiag);
  const scm_t_array_dim *dims_diag = scm_array_handle_dims (&handle_diag);
  const scm_t_array_dim *dims_superdiag =
    scm_array_handle_dims (&handle_superdiag);

  const REAL *subdiag_data = READONLY_ELEMENTS (&handle_subdiag);
  const REAL *diag_data = READONLY_ELEMENTS (&handle_diag);
  const REAL *superdiag_data = READONLY_ELEMENTS (&handle_superdiag);
  const REAL *B_data = READONLY_ELEMENTS (&handle_B);

  for (int i = 0; i < n - 1; i++)
    {
      subdiag_[i] = *subdiag_data;
      subdiag_data += dims_subdiag[0].inc;
      superdiag_[i] = *superdiag_data;
      superdiag_data += dims_superdiag[0].inc;
    }

  for (int i = 0; i < n; i++)
    {
      diag_[i] = *diag_data;
      diag_data += dims_diag[0].inc;
    }

  for (int j = 0; j < nrhs; j++)
    for (int i = 0; i < n; i++)
      B_[j * n + i] = B_data[(ssize_t) i * row_inc + (ssize_t) j * col_inc];

  int info;
  CYCLICTRIDIAGSOLVE (n, nrhs, subdiag_, diag_, superdiag_,
                      lower_left_, upper_right_, (REAL (*)[n]) B_, &info);

  SCM X;
  if (info != 0)
    X = SCM_BOOL_F;
  else if (rank_of_B == 1)
    {
      REAL *data = scm_malloc (n * sizeof (double));
      memcpy (data, B_, n * sizeof (double));
      X = TAKE_VECTOR (data, n);
    }
  else
    {
      X = scm_make_typed_array (SYMBOL_Fxx (), SCM_UNSPECIFIED,
                                scm_list_2 (scm_from_int (n),
                                            scm_from_int (nrhs)));
      scm_t_array_handle handle;
      scm_array_get_handle (X, &handle);
      REAL *data = WRITABLE_ELEMENTS (&handle);
      for (int i = 0; i < n; i++)
        for (int j = 0; j < nrhs; j++)
          {
            *data = B_[j * n + i];
            data++;
          }
      scm_array_handle_release (&handle);
    }

  scm_dynwind_end ();

  SCM values[2] = { X, scm_from_int (info) };
  return scm_c_values (values, 2);
}
