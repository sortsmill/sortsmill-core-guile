#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <stdbool.h>
#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

VISIBLE SCM
scm_reverse_x_without_checking (SCM lst, SCM tail)
{
  SCM q = (SCM_UNBNDP (tail)) ? SCM_EOL : tail;
  while (!scm_is_null (lst))
    {
      SCM p = SCM_CDR (lst);
      SCM_SETCDR (lst, q);
      q = lst;
      lst = p;
    }
  return q;
}

static void
reverse_and_split (SCM lst, unsigned int lst_position, const char *who,
                   size_t n, SCM *left, SCM *right)
{
  SCM left_side = SCM_EOL;
  SCM p = lst;
  for (size_t i = 0; i < n; i++)
    {
      if (scm_is_null (p))
        rnrs_raise_condition
          (scm_list_4
           (rnrs_make_assertion_violation (),
            rnrs_c_make_who_condition (who),
            rnrs_c_make_message_condition (_("the list is too short")),
            rnrs_make_irritants_condition (scm_list_2 (lst,
                                                       scm_from_size_t (n)))));
      SCM_ASSERT_TYPE ((scm_is_pair (p)), p, SCM_ARG1, who, "list");
      left_side = scm_cons (SCM_CAR (p), left_side);
      p = SCM_CDR (p);
    }
  *left = left_side;
  *right = p;
}

VISIBLE SCM
scm_c_reverse_until (SCM lst, size_t n)
{
  SCM values[2];
  reverse_and_split (lst, SCM_ARG1, "reverse-until", n, &values[0], &values[1]);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_reverse_until (SCM lst, SCM n)
{
  return scm_c_reverse_until (lst, scm_to_size_t (n));
}

VISIBLE SCM
scm_c_split_before (SCM lst, size_t n)
{
  SCM values[2];
  SCM initial_part;
  reverse_and_split (lst, SCM_ARG1, "split-before", n,
                     &initial_part, &values[1]);
  values[0] = scm_reverse_x_without_checking (initial_part, SCM_EOL);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_split_before (SCM lst, SCM n)
{
  return scm_c_split_before (lst, scm_to_size_t (n));
}
