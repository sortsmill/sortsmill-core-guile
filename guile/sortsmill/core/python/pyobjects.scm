;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014, 2017 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pyobjects)

  (export
   ;; (py<SOMETHING>-type) → pytype
   pyscm-type
   pybaseobject-type
   pysuper-type
   pytype-type
   pybytes-type
   pyunicode-type
   pybool-type
   pyint-type ;; Supported in Python 2, only.
   pylong-type
   pyfloat-type
   pycomplex-type
   pytuple-type
   pylist-type
   pydict-type
   pydictproxy-type
   pyset-type
   pyfrozenset-type
   pyrange-type
   pyslice-type
   pyellipsis-type
   pycode-type
   pyframe-type
   pytraceback-type
   pymethod-type
   pyinstancemethod-type
   pymodule-type
   pyproperty-type
   pycapsule-type
   pybytearray-type
   pymemoryview-type
   pycell-type

   py-incref!
   py-decref!
   py-refcount

   make-pyobject            ;; Steals the reference. Use with extreme care.
   incref-make-pyobject     ;; Use with extreme care.
   make-borrowed-pyobject   ;; Use with extreme care.
   pyobject->pointer        ;; Borrows the reference. Use with extreme care.
   incref-pyobject->pointer ;; Use with extreme care.
   pyobject?
   py-notimplemented
   py-notimplemented?
   pynone? ;; (pynone? obj) → boolean  [You also can write (equal? obj (py None))]
   py-none ;; (py-none) → obj  [You also can write (py None)]
   pybytes?
   pybytes-exact?
   bytevector->pybytes
   pybytes->bytevector
   pyunicode?
   pyunicode-exact?
   string->pyunicode
   pyunicode->string
   pyunicode->scm
   scm->pyunicode
   python-repr ;; (python-repr string) → pybytes-or-pyunicode
   python-str  ;; (python-str string) → pybytes-or-pyunicode
   pybool?
   pybool
   pyobject-true?
   pyfloat?
   pyfloat-exact?
   real->pyfloat
   pyfloat->real
   pycomplex?
   pycomplex-exact?
   number->pycomplex
   pycomplex->number
   pyinteger?
   pyinteger-exact?
   integer->pyinteger
   pyinteger->integer
   pyreal?
   pyreal->real

   pyobject-hasattr? ;; (pyobject-hasattr? obj attr-name) → boolean
   pyobject-getattr  ;; (pyobject-getattr obj attr-name) → obj
   pyobject-setattr! ;; (pyobject-setattr! obj attr-name v) → *unspecified*
   pyobject-delattr! ;; (pyobject-delattr! obj attr-name) → *unspecified*
   pyobject<?        ;; (pyobject<? obj1 obj2) → boolean
   pyobject<=?       ;; (pyobject<=? obj1 obj2) → boolean
   pyobject=?        ;; (pyobject=? obj1 obj2) → boolean
   pyobject<>?       ;; (pyobject<>? obj1 obj2) → boolean
   pyobject>?        ;; (pyobject>? obj1 obj2) → boolean
   pyobject>=?       ;; (pyobject>=? obj1 obj2) → boolean
   pyobject-repr     ;; (pyobject-repr obj) → obj
   pyobject-unicode  ;; (pyobject-unicode obj) → obj
   pyobject-ascii    ;; (pyobject-ascii obj) → obj
   pyobject-bytes    ;; (pyobject-bytes obj) → obj
   pyobject-isinstance? ;; (pyobject-isinstance inst cls) → boolean
   pyobject-issubclass? ;; (pyobject-issubclass inst cls) → boolean
   pycallable?          ;; (pycallable? obj) → boolean
   pyobject-call        ;; (pyobject-call callable [[args] kw]) → obj
   pyobject-callobject  ;; (pyobject-callobject callable [args]) → obj
   pyobject-callfunctionobjargs ;; (pyobject-callfunctionobjargs callable arg1 arg2 ...) → obj
   pyobject-callmethodobjargs ;; (pyobject-callmethodobjargs obj method-name arg1 arg2 ...) → obj
   pyobject->procedure  ;; (pyobject->procedure callable) → procedure
   pyobject-method->procedure ;; (pyobject-method->procedure obj method-name) → procedure
   pyobject-hash        ;; (pyobject-hash obj) → integer
   pyobject-not         ;; (pyobject-not obj) → Python-boolean
   pyobject-type        ;; (pyobject-type obj) → type-obj
   pyobject-length      ;; (pyobject-length obj) → integer
   pyobject-getitem     ;; (pyobject-getitem obj key) → obj
   pyobject-setitem!    ;; (pyobject-setitem! obj key v) → *unspecified*
   pyobject-delitem!    ;; (pyobject-delitem! obj key) → *unspecified*
   pyobject-asfiledescriptor ;; (pyobject-asfiledescriptor obj) → integer
   pyobject-dir              ;; (pyobject-dir [obj]) → obj
   pyobject-getiter          ;; (pyobject-getiter obj) → obj

   ;; Converts only pyintegers; other objects are returned unchanged.
   pyinteger->scm
   )

  (import (except (rnrs)
                  ;; We want to redefine the following with GOOPS;
                  ;; importing them from (rnrs) might cause trouble.
                  write equal?)
          (only (srfi :26) cut)
          (except (guile) error)
          (oop goops)
          (system foreign)
          (ice-9 match)
          (ice-9 format)
          (sortsmill core python pysequences)
          (sortsmill core python pymappings)
          (sortsmill core python version)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pyscm_type
                          scm_pybaseobject_type
                          scm_pysuper_type
                          scm_pytype_type
                          scm_pybytes_type
                          scm_pyunicode_type
                          scm_pybool_type
                          scm_pyint_type
                          scm_pylong_type
                          scm_pyfloat_type
                          scm_pycomplex_type
                          scm_pytuple_type
                          scm_pylist_type
                          scm_pydict_type
                          scm_pydictproxy_type
                          scm_pyset_type
                          scm_pyfrozenset_type
                          scm_pyrange_type
                          scm_pyslice_type
                          scm_pyellipsis_type
                          scm_pycode_type
                          scm_pyframe_type
                          scm_pytraceback_type
                          scm_pymethod_type
                          scm_pymodule_type
                          scm_pyproperty_type
                          scm_pybytearray_type
                          scm_pycell_type

                          scm_py_incref_x
                          scm_py_decref_x
                          scm_py_refcount

                          scm_make_pyobject
                          scm_incref_make_pyobject
                          scm_make_borrowed_pyobject
                          scm_pyobject_to_pointer
                          scm_incref_pyobject_to_pointer
                          scm_pyobject_p
                          scm_py_notimplemented
                          scm_py_notimplemented_p
                          scm_pynone_p
                          scm_py_none
                          scm_pybytes_p
                          scm_pybytes_exact_p
                          scm_bytevector_to_pybytes
                          scm_pybytes_to_bytevector
                          scm_pyunicode_p
                          scm_pyunicode_exact_p
                          scm_string_to_pyunicode
                          scm_pyunicode_to_string
                          scm_pyunicode_to_scm
                          scm_scm_to_pyunicode
                          scm_python_repr
                          scm_python_str
                          scm_pybool_p
                          scm_pybool
                          scm_pyobject_true_p
                          scm_pyfloat_p
                          scm_pyfloat_exact_p
                          scm_real_to_pyfloat
                          scm_pyfloat_to_real
                          scm_pycomplex_p
                          scm_pycomplex_exact_p
                          scm_number_to_pycomplex
                          scm_pycomplex_to_number
                          scm_pyinteger_p
                          scm_pyinteger_exact_p
                          scm_integer_to_pyinteger
                          scm_pyinteger_to_integer
                          scm_pyinteger_to_scm
                          scm_pyreal_p
                          scm_pyreal_to_real
                          scm_pyobject_hasattr_p
                          scm_pyobject_getattr
                          scm_pyobject_setattr_x
                          scm_pyobject_delattr_x
                          scm_pyobject_less_p
                          scm_pyobject_leq_p
                          scm_pyobject_eq_p
                          scm_pyobject_neq_p
                          scm_pyobject_gr_p
                          scm_pyobject_geq_p
                          scm_pyobject_repr
                          scm_pyobject_unicode
                          scm_pyobject_ascii
                          scm_pyobject_bytes
                          scm_pyobject_isinstance_p
                          scm_pyobject_issubclass_p
                          scm_pycallable_p
                          scm_pyobject_call
                          scm_pyobject_callobject
                          scm_pyobject_callfunctionobjargs
                          scm_pyobject_callmethodobjargs
                          scm_pyobject_hash
                          scm_pyobject_not
                          scm_pyobject_type
                          scm_pyobject_length
                          scm_pyobject_getitem
                          scm_pyobject_setitem_x
                          scm_pyobject_delitem_x
                          scm_pyobject_asfiledescriptor
                          scm_pyobject_dir
                          scm_pyobject_getiter

                          scm_pyinstancemethod_type

                          scm_pycapsule_type
                          scm_pymemoryview_type

                          guile_init_pyobjects))

  (eval-when (compile load eval)
    ((pointer->procedure void guile_init_pyobjects '())))

  ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  (define-generic write)
  (define-method (write [obj <pyobject>] port)
    (write-pyobject obj port))

  (define-generic equal?)
  (define-method (equal? [a <pyobject>] [b <pyobject>])
    (pyobject=? a b))

  ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  (define-syntax define-pytype-proc
    (lambda (stx)
      (syntax-case stx ()
        [(_ t) (identifier? #'t)
         (let* ([t^ (syntax->datum #'t)]
                [proc-name (string->symbol (format #f "py~a-type" t^))]
                [func-name (string->symbol (format #f "scm_py~a_type" t^))]
                [doc-string (format #f "Return a Guile-wrapped type object for the py~a type." t^)])
           #`(define-scm-procedure (#,(datum->syntax stx proc-name))
               #,(datum->syntax stx doc-string)
               #,(datum->syntax stx func-name)))])))

  (define-pytype-proc scm)
  (define-pytype-proc baseobject)
  (define-pytype-proc super)
  (define-pytype-proc type)
  (define-pytype-proc bytes)
  (define-pytype-proc unicode)
  (define-pytype-proc bool)
  (define-pytype-proc int) ;; Supported only in Python 2.
  (define-pytype-proc long)
  (define-pytype-proc float)
  (define-pytype-proc complex)
  (define-pytype-proc tuple)
  (define-pytype-proc list)
  (define-pytype-proc dict)
  (define-pytype-proc dictproxy)
  (define-pytype-proc set)
  (define-pytype-proc frozenset)
  (define-pytype-proc range)
  (define-pytype-proc slice)
  (define-pytype-proc ellipsis)
  (define-pytype-proc code)
  (define-pytype-proc frame)
  (define-pytype-proc traceback)
  (define-pytype-proc method)
  (define-pytype-proc module)
  (define-pytype-proc property)
  (define-pytype-proc bytearray)
  (define-pytype-proc cell)
  (define-pytype-proc instancemethod) ;; Not supported in Python 2.x.
  (define-pytype-proc capsule)        ;; Not supported in Python 2.6.
  (define-pytype-proc memoryview)     ;; Not supported in Python 2.6.

  (define-scm-procedure (py-incref! obj)
    "A wrapper around @code{Py_INCREF}. Use this procedure with
extreme care!"
    scm_py_incref_x)

  (define-scm-procedure (py-decref! obj)
    "A wrapper around @code{Py_DECREF}. Use this procedure with
extreme care!"
    scm_py_decref_x)

  (define-scm-procedure (py-refcount obj)
    "Return the reference count of a Guile-wrapped Python object."
    scm_py_refcount)

  (define-scm-procedure (make-pyobject address)
    "Steal a reference to a Python object, turning the reference into
a Guile object. The argument may be either an integer address or a
Guile pointer. As a special case (because it has been found useful),
if the argument is @code{NULL} then the return value is @{#f}."
    scm_make_pyobject)

  (define-scm-procedure (incref-make-pyobject address)
    "FIXME: Document this."
    scm_incref_make_pyobject)

  (define-scm-procedure (make-borrowed-pyobject child parent)
    "FIXME: Document this."
    scm_make_borrowed_pyobject)

  (define-scm-procedure (pyobject->pointer obj)
    "Convert a @code{pyobject} to a Guile pointer equal to the wrapped
@code{PyObject*}. The reference is borrowed."
    scm_pyobject_to_pointer)

  (define-scm-procedure (incref-pyobject->pointer obj)
    "Convert a @code{pyobject} to a Guile pointer equal to the
wrapped @code{PyObject*}. The reference count is incremented."
    scm_incref_pyobject_to_pointer)

  (define-scm-procedure (pyobject? obj)
    "Is the argument a @code{pyobject}?"
    scm_pyobject_p)

  (define-scm-procedure (py-notimplemented)
    "Return a Guile-wrapped reference to @code{Py_NotImplemented}."
    scm_py_notimplemented)

  (define-scm-procedure (py-notimplemented? obj)
    "Is the argument a reference to @code{Py_NotImplemented}?"
    scm_py_notimplemented_p)

  (define-scm-procedure (pynone? obj)
    "Is the argument a Guile-wrapped Python @code{None} object?"
    scm_pynone_p)

  (define-scm-procedure (py-none)
    "Return a Guile-wrapped reference to @code{Py_None}."
    scm_py_none)

  (define-scm-procedure (pybytes? obj)
    "Is the argument a @code{pybytes}?"
    scm_pybytes_p)

  (define-scm-procedure (pybytes-exact? obj)
    "Is the argument a @code{pybytes} but not a member of a subtype
of @code{pybytes}?"
    scm_pybytes_exact_p)

  (define-scm-procedure (bytevector->pybytes bv)
    "Convert a Guile bytevector to a Guile-wrapped @code{PyBytes}
object."
    scm_bytevector_to_pybytes)

  (define-scm-procedure (pybytes->bytevector pybytes)
    "Convert a Guile-wrapped @code{PyBytes} object to a bytevector."
    scm_pybytes_to_bytevector)

  (define-scm-procedure (pyunicode? obj)
    "Is the argument a @code{pyunicode}?"
    scm_pyunicode_p)

  (define-scm-procedure (pyunicode-exact? obj)
    "Is the argument a @code{pyunicode} but not a member of a subtype
of @code{pyunicode}?"
    scm_pyunicode_exact_p)

  (define*-scm-procedure (string->pyunicode str #:optional errors)
    "Convert a Guile string to a Guile-wrapped @code{PyUnicode}
object. The optional argument is the @code{errors} argument to the
Python codec."
    scm_string_to_pyunicode)

  (define*-scm-procedure (pyunicode->string pyuni #:optional errors)
    "Convert a Guile-wrapped @code{PyUnicode} object to a Guile
string. The optional argument is the @code{errors} argument to the
Python codec."
    scm_pyunicode_to_string)

  (define-scm-procedure (pyunicode->scm str)
    "Convert Guile-wrapped @code{PyUnicode} object to a Guile string,
with default codec error handling. (FIXME: Should I say `strict' error
handling?) Other objects are returned unchanged."
    scm_pyunicode_to_scm)

  (define-scm-procedure (scm->pyunicode str)
    "Convert a Guile string to a Guile-wrapped @code{PyUnicode}
object, with default codec error handling. (FIXME: Should I say
`strict' error handling?) Other objects are returned unchanged."
    scm_scm_to_pyunicode)

  (define-scm-procedure (python-repr str)
    "Convert a Guile string to a Guile-wrapped @code{PyBytes} (if
Python version is 2) or @code{PyUnicode} object (if Python version is
greater than 2). For Python 2, the conversion is to ASCII. This
procedure is suitable for use in the definitions of @code{__tp_repr__}
methods."
    scm_python_repr)

  (define-scm-procedure (python-str str)
    "Convert a Guile string to a Guile-wrapped @code{PyBytes} (if
Python version is 2) or @code{PyUnicode} object (if Python version is
greater than 2). For Python 2, the conversion is to locale
encoding. This procedure is suitable for use in the definitions of
@code{__tp_str__} methods."
    scm_python_str)

  (define-scm-procedure (pybool? obj)
    "Is the argument a @code{pybool}?"
    scm_pybool_p)

  (define-scm-procedure (pybool obj)
    "Convert a Guile value to a Guile-wrapped @code{PyBool} object."
    scm_pybool)

  (define-scm-procedure (pyobject-true? pyobj)
    "A wrapper around @code{PyObject_IsTrue}."
    scm_pyobject_true_p)
  
  (define-scm-procedure (pyfloat? obj)
    "Is the argument a @code{pyfloat}?"
    scm_pyfloat_p)

  (define-scm-procedure (pyfloat-exact? obj)
    "Is the argument a @code{pyfloat} but not a member of a subtype
of @code{pyfloat}?"
    scm_pyfloat_exact_p)

  (define-scm-procedure (real->pyfloat real-number)
    "Convert a Guile value to a Guile-wrapped @code{PyFloat} object."
    scm_real_to_pyfloat)

  (define-scm-procedure (pyfloat->real pyfloat)
    "Convert a Guile-wrapped @code{PyFloat} object to an `inexact'
real."
    scm_pyfloat_to_real)

  (define-scm-procedure (pycomplex? obj)
    "Is the argument a @code{pycomplex}?"
    scm_pycomplex_p)

  (define-scm-procedure (pycomplex-exact? obj)
    "Is the argument a @code{pycomplex} but not a member of a subtype
of @code{pycomplex}?"
    scm_pycomplex_exact_p)

  (define-scm-procedure (number->pycomplex complex-number)
    "Convert a Guile value to a Guile-wrapped @code{PyComplex}
object."
    scm_number_to_pycomplex)

  (define-scm-procedure (pycomplex->number pycomplex)
    "Convert a Guile-wrapped @code{PyComplex} object to an `inexact'
number."
    scm_pycomplex_to_number)

  (define-scm-procedure (pyinteger? obj)
    "Is the argument a @code{pyinteger} (that is, a Guile-wrapped
Python @code{int} or Python @code{long})?"
    scm_pyinteger_p)

  (define-scm-procedure (pyinteger-exact? obj)
    "Is the argument a @code{pyinteger} but not a member of a
subtype (that is, a Guile-wrapped Python @code{int} or Python
@code{long} but not a subtype of either)?"
    scm_pyinteger_exact_p)

  (define-scm-procedure (integer->pyinteger integer-number)
    "Convert a Guile value to a Guile-wrapped @code{PyInt} or
@code{PyLong} object."
    scm_integer_to_pyinteger)

  (define-scm-procedure (pyinteger->integer pyinteger)
    "Convert a Guile-wrapped @code{PyInt} or @code{PyLong} object to a
Guile integer."
    scm_pyinteger_to_integer)

  (define-scm-procedure (pyinteger->scm obj)
    "Convert a Guile-wrapped @code{PyInt} or @code{PyLong} object to a
Guile integer. Other objects are returned unchanged."
    scm_pyinteger_to_scm)

  (define-scm-procedure (pyreal? obj)
    "Is the argument a Guile-wrapped @code{PyInt}, @code{PyLong}, or
@code{PyFloat}?"
    scm_pyreal_p)

  (define-scm-procedure (pyreal->real obj)
    "Convert a Guile-wrapped @code{PyInt}, @code{PyLong}, or
@code{PyFloat} object to a Guile real (which may be either exact or
inexact)."
    scm_pyreal_to_real)

  (define-scm-procedure (pyobject-hasattr? obj attr-name)
    "A wrapper around @code{PyObject_HasAttr}. The @var{attr-name}
argument may be either a Python or a Guile string."
    scm_pyobject_hasattr_p)

  (define-scm-procedure (pyobject-getattr obj attr-name)
    "A wrapper around @code{PyObject_GetAttr}. The @var{attr-name}
argument may be either a Python or a Guile string."
    scm_pyobject_getattr)

  (define-scm-procedure (pyobject-setattr! obj attr-name v)
    "A wrapper around @code{PyObject_SetAttr}. The @var{attr-name}
argument may be either a Python or a Guile string."
    scm_pyobject_setattr_x)

  (define-scm-procedure (pyobject-delattr! obj attr-name)
    "A wrapper around @code{PyObject_DelAttr}. The @var{attr-name}
argument may be either a Python or a Guile string."
    scm_pyobject_delattr_x)

  (define-scm-procedure (pyobject<? obj1 obj2)
    "Python `less-than', returning a Guile boolean."
    scm_pyobject_less_p)

  (define-scm-procedure (pyobject<=? obj1 obj2)
    "Python `less-than-or-equal', returning a Guile boolean."
    scm_pyobject_leq_p)

  (define-scm-procedure (pyobject=? obj1 obj2)
    "Python `equal', returning a Guile boolean."
    scm_pyobject_eq_p)

  (define-scm-procedure (pyobject<>? obj1 obj2)
    "Python `not-equal', returning a Guile boolean."
    scm_pyobject_neq_p)

  (define-scm-procedure (pyobject>? obj1 obj2)
    "Python `greater-than', returning a Guile boolean."
    scm_pyobject_gr_p)

  (define-scm-procedure (pyobject>=? obj1 obj2)
    "Python `greater-than-or-equal', returning a Guile boolean."
    scm_pyobject_geq_p)

  (define-scm-procedure (pyobject-repr obj)
    "A wrapper around @code{PyObject_Repr}."
    scm_pyobject_repr)

  (define-scm-procedure (pyobject-unicode obj)
    "A wrapper around @code{PyObject_Unicode}, for Python 2, or
@code{PyObject_Str}, for Python 3."
    scm_pyobject_unicode)

  (define-scm-procedure (pyobject-ascii obj)
    "A wrapper around @code{PyObject_Repr}, for Python 2, or
@code{PyObject_ASCII}, for Python 3."
    scm_pyobject_ascii)

  (define-scm-procedure (pyobject-bytes obj)
    "For Python 3, this is a wrapper around @code{PyObject_Bytes}. For
Python 2, it is a procedure with similar behavior. (It is @emph{not} a
wrapper around Python 2's @code{PyObject_Bytes}, which is just a
synonym for @code{PyObject_Str}.)"
    scm_pyobject_bytes)

  (define-scm-procedure (pyobject-isinstance? inst cls)
    "A wrapper around @code{PyObject_IsInstance}."
    scm_pyobject_isinstance_p)

  (define-scm-procedure (pyobject-issubclass? inst cls)
    "A wrapper around @code{PyObject_IsSubclass}."
    scm_pyobject_issubclass_p)

  (define-scm-procedure (pycallable? obj)
    "A wrapper around @code{PyCallable_Check}."
    scm_pycallable_p)

  (define*-scm-procedure (pyobject-call callable #:optional args keywords)
    "A wrapper around @code{PyObject_Call}, with the @var{args} and
@var{keywords} arguments optional."
    scm_pyobject_call)

  (define*-scm-procedure (pyobject-callobject callable #:optional args)
    "A wrapper around @code{PyObject_CallObject}, with the @var{args}
argument optional."
    scm_pyobject_callobject)

  (define-scm-procedure (pyobject-callfunctionobjargs callable . args)
    "Similar to (though not based on)
@code{PyObject_CallFunctionObjArgs}. Handles keyword arguments. FIXME:
Write better documentation."
    scm_pyobject_callfunctionobjargs)

  (define-scm-procedure (pyobject-callmethodobjargs obj method-name . args)
    "Similar to (though not based on)
@code{PyObject_CallMethodObjArgs}. The @var{method-name} argument can
be either a Python or a Guile string. Handles keyword
arguments. FIXME: Write better documentation."
    scm_pyobject_callmethodobjargs)

  (define (pyobject->procedure callable)
    "Convert a Guile-wrapped Python callable to a Guile
procedure. Handles keyword arguments. FIXME: Write better
documentation."
    (lambda args
      (apply pyobject-callfunctionobjargs callable args)))

  (define (pyobject-method->procedure obj method-name)
    "FIXME: Document this."
    (lambda args
      (apply pyobject-callmethodobjargs obj method-name args)))

  (define-scm-procedure (pyobject-hash obj)
    "A wrapper around @code{PyObject_Hash}."
    scm_pyobject_hash)

  (define-scm-procedure (pyobject-not obj)
    "A wrapper around @code{PyObject_Not}. Returns a Python
boolean (as opposed to a Guile boolean)."
    scm_pyobject_not)

  (define-scm-procedure (pyobject-type obj)
    "A wrapper around @code{PyObject_Type}."
    scm_pyobject_type)

  (define-scm-procedure (pyobject-length obj)
    "A wrapper around @code{PyObject_Length}."
    scm_pyobject_length)

  (define-scm-procedure (pyobject-getitem obj key)
    "A wrapper around @code{PyObject_GetItem}."
    scm_pyobject_getitem)

  (define-scm-procedure (pyobject-setitem! obj key v)
    "A wrapper around @code{PyObject_SetItem}."
    scm_pyobject_setitem_x)

  (define-scm-procedure (pyobject-delitem! obj key)
    "A wrapper around @code{PyObject_DelItem}."
    scm_pyobject_delitem_x)

  (define-scm-procedure (pyobject-asfiledescriptor obj)
    "A wrapper around @code{PyObject_AsFileDescriptor}."
    scm_pyobject_asfiledescriptor)

  (define*-scm-procedure (pyobject-dir #:optional obj)
    "A wrapper around @code{PyObject_Dir}."
    scm_pyobject_dir)

  (define-scm-procedure (pyobject-getiter obj)
    "A wrapper around @code{PyObject_GetIter}."
    scm_pyobject_getiter)

  ) ;; end of library.
