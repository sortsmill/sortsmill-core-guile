;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python very-high-level)

  (export
   py-main            ;; (py-main params-lst) → integer
   pyrun-simplestring ;; (pyrun-simplestring string [int-flags]) → int-flags
   pyrun-anyfile ;; (pyrun-anyfile [port [int-flags]] [#:file-name fn]) → int-flags
   pyrun-interactiveone ;; (pyrun-interactiveone [port [int-flags]] [#:file-name fn]) → int-flags
   pyrun-string ;; (pyrun-string string start globals locals [int-flags]) → pyobj, int-flags
   pyrun-file ;; (pyrun-file port start globals locals [int-flags] [#:file-name fn]) → pyobj, int-flags
   py-compilestring ;; (py-compilestring string file-name start [int-flags [optimize]]) → pycode, int-flags */
   pyeval-evalcode ;; (pyeval-evalcode pycode globals locals [args [keywords [defaults [closure]]]]) → pyobj
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_py_main
                          scm_pyrun_simplestring
                          scm_pyrun_anyfile
                          scm_pyrun_interactiveone
                          scm_pyrun_string
                          scm_pyrun_file
                          scm_py_compilestring
                          scm_pyeval_evalcode))

  (define-scm-procedure (py-main program-params)
    "Call @code{Py_Main} with the given program parameters."
    scm_py_main)

  (define*-scm-procedure (pyrun-simplestring command #:optional flags)
    "Call @code{PyRun_SimpleStringFlags}, returning the final value of
the @code{cf_flags} field. The default flags are zero."
    scm_pyrun_simplestring)

  (define pyrun-anyfile
    (let ([proc (lambda-scm-procedure (port flags file-name)
                  scm_pyrun_anyfile)])
      (lambda* (#:optional port flags #:key file-name)
        "Call @code{PyRun_AnyFileExFlags} on a Guile input
port (assuming it can be converted to a C @code{(FILE*)}), returning
the final value of the @code{cf_flags} field. The default port is
@code{(current-input-port)}. The default flags are zero. The file name
can be overridden with the optional keyword argument
@code{file-name}."
        (proc port flags file-name))))

  (define pyrun-interactiveone
    (let ([proc (lambda-scm-procedure (port flags file-name)
                  scm_pyrun_interactiveone)])
      (lambda* (#:optional port flags #:key file-name)
        "Call @code{PyRun_InteractiveOneFlags} on a Guile input
port (assuming it can be converted to a C @code{(FILE*)}), returning
two values: the final value of the @code{cf_flags} field, and the
return value of the call to @code{PyRun_InteractiveOneFlags} (if it
was not @code{-1}, in which case an exception is raised). The default
port is @code{(current-input-port)}. The default flags are zero. The
file name can be overridden with the optional keyword argument
@code{file-name}."
        (proc port flags file-name))))

  (define*-scm-procedure (pyrun-string string start globals locals
                                       #:optional flags)
    "Call @code{PyRun_StringFlags}, returning a Python object and the
final value of the @code{cf_flags} field. Required arguments:
@var{string} is a Guile string; @var{start} is @code{'Py_eval_input},
@code{'Py_file_input}, or @code{'Py_single_input}; @var{globals} is a
Python globals object, @code{#f} for an empty dictionary, or @code{#t}
for the builtins dictionary; @var{locals} is a Python locals object,
or @code{#f} for an empty dictionary. The default flags are zero."
    scm_pyrun_string)

  (define pyrun-file
    (let ([proc (lambda-scm-procedure (port start globals locals flags file-name)
                  scm_pyrun_file)])
      (lambda* (port start globals locals #:optional flags #:key file-name)
        "Call @code{PyRun_FileExFlags}, returning a Python object and
the final value of the @code{cf_flags} field. Required arguments:
@var{port} is a Guile input port, or @code{#f} for the current input
port; @var{start} is @code{'Py_eval_input}, @code{'Py_file_input}, or
@code{'Py_single_input}; @var{globals} is a Python globals object,
@code{#f} for an empty dictionary, or @code{#t} for the builtins
dictionary; @var{locals} is a Python locals object, or @code{#f} for
an empty dictionary. The default flags are zero. The file name can be
overridden with the optional keyword argument @code{file-name}."
        (proc port start globals locals flags file-name))))

  (define*-scm-procedure (py-compilestring string file-name start
                                           #:optional flags optimize)
    "Call @code{Py_CompileStringExFlags} or
@code{Py_CompileStringFlags}, returning a Python code object and the
final value of the @code{cf_flags} field. Required arguments:
@var{string} and @var{file-name} are Guile strings; @var{start} is
@code{'Py_eval_input}, @code{'Py_file_input}, or
@code{'Py_single_input}. The default flags are zero. The default
optimization level is @code{-1}; if the Python version is less than
3.2, the optimization level is ignored."
    scm_py_compilestring)

  (define*-scm-procedure (pyeval-evalcode pycode globals locals #:optional
                                          args keywords defaults closure)
    "Call @code{PyEval_EvalCodeEx}. FIXME: Write more documentation
for this procedure."
    scm_pyeval_evalcode)

  ) ;; end of library.
