;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pyscms)

  (export
   pyscm?      ;; (pyscm? obj) → boolean
   scm->pyscm  ;; (scm->pyscm obj) → obj
   scm->pyscm* ;; (scm->pyscm* obj [#:name string-or-#f] [#:doc string-or-#f]) → obj
   pyscm->scm  ;; (pyscm->scm obj) → obj

   py-name     ;; An object property for __name__ attributes.
   py-doc      ;; An object property for __doc__ attributes.
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core python pyobjects))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pyscm_p
                          scm_scm_to_pyscm
                          scm_pyscm_to_scm
                          ))

  (define py-name (make-object-property))
  (define py-doc (make-object-property))

  (define-scm-procedure (pyscm? obj)
    "Is the argument a @code{pyscm}?"
    scm_pyscm_p)

  (define-scm-procedure (scm->pyscm obj)
    "Wrap a Guile object for use in Python code."
    scm_scm_to_pyscm)

  (define* (scm->pyscm* obj #:key
                        [name (if (procedure? obj) (procedure-name obj) #f)]
                        [doc (if (procedure? obj) (procedure-documentation obj) #f)])
    "Wrap a Guile object for use in Python code, possibly adding
@code{__name__} and/or @code{__doc__} attributes. FIXME: More thorough
documentation."
    (set! (py-name obj) (cond [(pynone? name) #f]
                              [(symbol? name) (symbol->string name)]
                              [else name]))
    (set! (py-doc obj) (if (pynone? doc) #f doc))
    (scm->pyscm obj))

  (define-scm-procedure (pyscm->scm obj)
    "Unwrap a Guile object that has been wrapped by
@code{scm->pyscm}."
    scm_pyscm_to_scm)

  ) ;; end of library.
