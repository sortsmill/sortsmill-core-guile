;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python friendlier)

  (export
   py-local-ref
   py-global-ref
   py-builtin-ref
   py-var-ref
   py-local-ref?
   py-global-ref?
   py-builtin-ref?
   py-var-ref?
   py-module-var-ref

   py-local-set!
   py-global-set!
   py-var-set!

   py-local-del!
   py-global-del!
   py-var-del!

   py-attr-ref
   py-attr-ref?
   py-attr-set!
   py-attr-del!

   py-item-ref

   ;; Procedures with setters.
   py-var        ;; py-var-ref, py-var-set!
   py-attr       ;; py-attr-ref, py-attr-set!
   py-item       ;; py-item-ref, py-item-set!

   del!          ;; FIXME: THIS IS A HACK. Improve it, please.

   py-call       ;; pyobject-callfunctionobjargs or pyobject-callmethodobjargs
   py-meth       ;; pyobject-callmethodobjargs

   py-type
   py-property
   py-classmethod
   py-staticmethod

   py-len        ;; pyobject-length

   py-car        ;; pysequence-car
   py-cdr        ;; pysequence-cdr
   py-caar       ;; pysequence-caar
   py-cadr       ;; pysequence-cadr
   py-cdar       ;; pysequence-cdar
   py-cddr       ;; pysequence-cddr
   py-take       ;; pysequence-take
   py-drop       ;; pysequence-drop
   py-take-right ;; pysequence-take-right
   py-drop-right ;; pysequence-drop-right
   )

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (system foreign)
          (sortsmill core python pyobjects)
          (sortsmill core python pyscms)
          (sortsmill core python pysequences)
          (sortsmill core python pymappings)
          (sortsmill core python pymethods)
          (sortsmill core python reflection)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core kwargs)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo)
          )

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          init_guile_pyfriendlier
                          scm_py_local_ref
                          scm_py_local_ref_p
                          scm_py_global_ref
                          scm_py_global_ref_p
                          scm_py_builtin_ref
                          scm_py_builtin_ref_p
                          scm_py_var_ref
                          scm_py_var_ref_p
                          scm_py_module_var_ref
                          scm_py_item_ref
                          scm_py_item_set_x
                          scm_py_item_del_x)
    ((pointer->procedure void init_guile_pyfriendlier '())))

  ;;----------------------------------------------------------------------

  (define-scm-procedure (py-local-ref key)
    "FIXME: Document this."
    scm_py_local_ref)

  (define-scm-procedure (py-global-ref key)
    "FIXME: Document this."
    scm_py_global_ref)

  (define-scm-procedure (py-builtin-ref key)
    "FIXME: Document this."
    scm_py_builtin_ref)

  (define-scm-procedure (py-local-ref? key)
    "FIXME: Document this."
    scm_py_local_ref_p)

  (define-scm-procedure (py-global-ref? key)
    "FIXME: Document this."
    scm_py_global_ref_p)

  (define-scm-procedure (py-builtin-ref? key)
    "FIXME: Document this."
    scm_py_builtin_ref_p)

  (define-scm-procedure (py-var-ref key)
    "FIXME: Document this."
    scm_py_var_ref)

  (define-scm-procedure (py-var-ref? key)
    "FIXME: Document this."
    scm_py_var_ref_p)

  (define-scm-procedure (py-module-var-ref module-key variable-key)
    "FIXME: Document this."
    scm_py_module_var_ref)

  ;;----------------------------------------------------------------------

  (define-scm-procedure (py-item-ref obj key)
    "FIXME: Document this."
    scm_py_item_ref)

  (define-scm-procedure (py-item-set! obj key value)
    "FIXME: Document this."
    scm_py_item_set_x)

  (define-scm-procedure (py-item-del! obj key)
    "FIXME: Document this."
    scm_py_item_del_x)

  ;;----------------------------------------------------------------------

  (define (py-local-set! key value)
    "FIXME: Document this."
    (let ([locals (pyeval-getlocals)])
      (if locals
          (pyobject-setitem! locals (prepare-item-key key) value)
          (raise-no-execution-frame-error 'py-local-set!))))

  (define (py-global-set! key value)
    "FIXME: Document this."
    (let ([globals (pyeval-getglobals)])
      (if globals
          (pyobject-setitem! globals (prepare-item-key key) value)
          (raise-no-execution-frame-error 'py-global-set!))))

  (define py-var-set! py-local-set!)

  ;;----------------------------------------------------------------------

  (define (py-local-del! key)
    "FIXME: Document this."
    (let ([locals (pyeval-getlocals)])
      (if locals
          (pyobject-delitem! locals (prepare-item-key key))
          (raise-no-execution-frame-error 'py-local-del!))))

  (define (py-global-del! key)
    "FIXME: Document this."
    (let ([globals (pyeval-getglobals)])
      (if globals
          (pyobject-delitem! globals (prepare-item-key key))
          (raise-no-execution-frame-error 'py-global-del!))))

  (define py-var-del! py-local-del!)

  ;;----------------------------------------------------------------------

  (define (py-attr-ref obj key)
    "FIXME: Document this."
    (pyobject-getattr obj (prepare-attr-key key)))

  (define (py-attr-ref? obj key)
    "FIXME: Document this."
    (let ([k (prepare-attr-key key)])
      (if (pyobject-hasattr? obj k)
          (pyobject-getattr obj k)
          #f)))

  (define (py-attr-set! obj key value)
    "FIXME: Document this."
    (pyobject-setattr! obj (prepare-attr-key key) value))

  (define (py-attr-del! obj key)
    "FIXME: Document this."
    (pyobject-delattr! obj (prepare-attr-key key)))

  ;;----------------------------------------------------------------------

  (define py-var (make-procedure-with-setter py-var-ref py-var-set!))
  (define py-attr (make-procedure-with-setter py-attr-ref py-attr-set!))
  (define py-item (make-procedure-with-setter py-item-ref py-item-set!))

  ;; FIXME: THIS IS A COMPLETE HACK to make a ‘del!’ that behaves like
  ;; ‘set!’. See if we can come up with a better and more general way.
  ;;
  ;; For now, the ‘py-var’, ‘py-attr’, or ‘py-item’ must appear
  ;; literally.
  ;;
  ;; The implementation of ‘set!’ at
  ;; http://srfi.schemers.org/srfi-17/srfi-17.html is not very
  ;; ‘Schemey’, and also will not work verbatim in Guile. Something
  ;; like it could work, and could allow a ‘real’ argument, but I am
  ;; not yet ready to commit to such an implementation.
  ;;
  ;; The way ‘set!’ works in Guile perhaps involves the special
  ;; support, built into psyntax, for ‘set!’ and variable
  ;; transformers. I am not sure, yet. It _does_ involve ‘applicable
  ;; structs’, which are merely _mentioned_ in the Guile 2.0.9
  ;; documentation. See
  ;; http://lists.gnu.org/archive/html/guile-user/2010-08/msg00072.html
  (define-syntax del!
    (lambda (stx)
      (syntax-case stx (py-var py-attr py-item)
        [(del! (py-var key))      #'(py-var-del! key)]
        [(del! (py-attr obj key)) #'(py-attr-del! obj key)]
        [(del! (py-item obj key)) #'(py-item-del! obj key)]
        [_ (syntax-violation
            'del! (_ "illegal del! form") (syntax->datum stx))])))

  ;;----------------------------------------------------------------------

  (define py-type ;; FIXME: Add a documentation string.
    (case-lambda
      [(obj) (pyobject-type obj)]
      [(name bases . dict)
       (let ([type-function (py-builtin-ref (string->pyunicode "type"))]
             [obj-bases (cond [(pytuple? bases) bases]
                              [(list? bases) (list->pytuple bases)]
                              [(pysequence? bases)
                               (list->pytuple (pysequence->list bases))]
                              [else (assertion-violation
                                     'py-type
                                     (_ "bases must be a list or pysequence"))])]
             [obj-dict (if (and (= 1 (length dict)) (pydict? (car dict)))
                           (car dict)
                           (alist->pydict dict))])
         (py-call type-function (scm->pyunicode name) obj-bases obj-dict))]))

  (define (proc->py who proc)
    (cond [(or (not proc) (pynone? proc)) (py-none)]
          [(procedure? proc) (scm->pyscm proc)]
          [(pyscm? proc) proc]
          [assertion-violation who (_ "unexpected type") proc]))

  (define (str->py str)
    (cond [(or (not str) (pynone? str)) (py-none)]
          [_ (scm->pyunicode str)]))

  (define/kwargs (py-property get set del doc)
    "FIXME: Document this."
    (py-call (py-builtin-ref (string->pyunicode "property"))
             (proc->py 'py-property get)
             (proc->py 'py-property set)
             (proc->py 'py-property del)
             (str->py doc)))

  (define/kwargs (py-classmethod function)
    "FIXME: Document this."
    (py-call (py-builtin-ref (string->pyunicode "classmethod"))
             (proc->py 'py-classmethod function)))

  (define/kwargs (py-staticmethod function)
    "FIXME: Document this."
    (py-call (py-builtin-ref (string->pyunicode "staticmethod"))
             (proc->py 'py-classmethod function)))

  ;;----------------------------------------------------------------------

  (define (py-call f . args)
    "@code{(py-call callable arg1 arg2 ...)} or
@code{(py-call \"method\" object arg1 arg2 ...)}.
FIXME: Write better documentation for this."
    (cond [(pycallable? f) (apply pyobject-callfunctionobjargs f args)]
          [(not (null? args))
           (apply pyobject-callmethodobjargs
                  (car args)
                  (prepare-attr-key f)
                  (cdr args))]
          [else (apply assertion-violation
                       'py-call
                       (_ "invalid arguments to py-call")
                       f args)]))

  ;; Call a Python method with the method name after the object.
  ;; @code{(py-meth obj "method" ...)} is equivalent to
  ;; @code{(py-call "method" obj ...)}.
  (define py-meth       pyobject-callmethodobjargs)

  (define py-len        pyobject-length)

  (define py-car        pysequence-car)
  (define py-cdr        pysequence-cdr)
  (define py-caar       pysequence-caar)
  (define py-cadr       pysequence-cadr)
  (define py-cdar       pysequence-cdar)
  (define py-cddr       pysequence-cddr)
  (define py-take       pysequence-take)
  (define py-drop       pysequence-drop)
  (define py-take-right pysequence-take-right)
  (define py-drop-right pysequence-drop-right)

  ;;----------------------------------------------------------------------

  ) ;; end of library.
