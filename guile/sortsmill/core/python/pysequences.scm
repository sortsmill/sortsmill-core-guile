;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pysequences)

  (export
   pysequence?              ;; (pysequence? obj) → boolean
   pylist?                  ;; (pylist? obj) → boolean
   pylist-exact?            ;; (pylist-exact? obj) → boolean
   pytuple?                 ;; (pytuple? obj) → boolean
   pytuple-exact?           ;; (pytuple-exact? obj) → boolean
   pysequence-length        ;; (pysequence-length obj) → integer
   pysequence-concat        ;; (pysequence-concat obj1 obj2 ...) → obj
   pysequence-repeat        ;; (pysequence-repeat obj count) → obj
   pysequence-inplaceconcat! ;; (pysequence-inplaceconcat! obj1 obj2 ...) → obj
   pysequence-inplacerepeat! ;; (pysequence-inplacerepeat! obj count) → obj
   pysequence-getitem       ;; (pysequence-getitem obj i) → obj
   pysequence-getslice      ;; (pysequence-getslice obj i1 i2) → obj
   pysequence-setitem!      ;; (pysequence-setitem! obj i v) → *unspecified*
   pysequence-delitem!      ;; (pysequence-delitem! obj i) → *unspecified*
   pysequence-setslice!     ;; (pysequence-setslice! obj i1 i2 v) → *unspecified*
   pysequence-delslice!     ;; (pysequence-delslice! obj i1 i2) → *unspecified*
   pysequence-count         ;; (pysequence-count obj v) → integer
   pysequence-contains?     ;; (pysequence-contains obj v) → boolean
   pysequence-index         ;; (pysequence-index obj v) → integer
   pysequence-list          ;; (pysequence-list obj) → obj
   pysequence-tuple         ;; (pysequence-tuple obj) → obj

   pysequence->list         ;; (pysequence->list obj [tail]) → list
   pysequence-reverse->list ;; (pysequence-reverse->list obj) → list
   pysequence-reverse->pylist ;; (pysequence-reverse->list obj) → pylist
   pysequence-reverse->pytuple ;; (pysequence-reverse->list obj) → pytuple
   pysequence-map->list     ;; (pysequence-map->list proc obj1 obj2 ...) → list
   pysequence->vector       ;; (pysequence->vector obj) → vector
   pysequence-reverse->vector ;; (pysequence-reverse->vector obj) → vector
   pysequence-map->vector   ;; (pysequence-map->vector proc obj1 obj2 ...) → vector

   pysequence-for-each      ;; (pysequence-for-each proc obj1 obj2 ...) → *unspecified*
   pysequence-fold-left     ;; (pysequence-fold-left proc init obj1 obj2 ...) → obj
   pysequence-fold-right    ;; (pysequence-fold-right proc init obj1 obj2 ...) → obj
   pysequence-for-all       ;; (pysequence-for-all proc obj1 obj2 ...) → #f-or-obj
   pysequence-exists        ;; (pysequence-exists proc obj1 obj2 ...) → #f-or-obj

   list->pylist             ;; (list->pylist list) → obj
   list->pytuple            ;; (list->pytuple list) → obj
   list-reverse->pylist     ;; (list-reverse->pylist list) → obj
   list-reverse->pytuple    ;; (list-reverse->pytuple list) → obj

   vector->pylist           ;; (vector->pylist vector) → obj
   vector->pytuple          ;; (vector->pytuple vector) → obj
   vector-reverse->pylist   ;; (vector-reverse->pylist vector) → obj
   vector-reverse->pytuple  ;; (vector-reverse->pytuple vector) → obj

   list-map->pylist         ;; (list-map->pylist proc list1 list2 ...) → obj
   list-map->pytuple        ;; (list-map->pytuple proc list1 list2 ...) → obj
   vector-map->pylist       ;; (vector-map->pylist proc vector1 vector2 ...) → obj
   vector-map->pytuple      ;; (vector-map->pytuple proc vector1 vector2 ...) → obj

   pysequence->s8vector     ;; (pysequence->s8vector obj) → s8vector
   pysequence->s16vector    ;; etc.
   pysequence->s32vector
   pysequence->u8vector
   pysequence->u16vector
   pysequence->u32vector
   pysequence->f32vector
   pysequence->f64vector

   pysequence-reverse->s8vector  ;; (pysequence-reverse->s8vector obj) → s8vector
   pysequence-reverse->s16vector ;; etc.
   pysequence-reverse->s32vector
   pysequence-reverse->u8vector
   pysequence-reverse->u16vector
   pysequence-reverse->u32vector
   pysequence-reverse->f32vector
   pysequence-reverse->f64vector

   s8vector->pylist         ;; (s8vector->pylist s8vector) → obj
   s16vector->pylist        ;; (s16vector->pylist s16vector) → obj
   s32vector->pylist        ;; (s32vector->pylist s32vector) → obj
   u8vector->pylist         ;; (u8vector->pylist u8vector) → obj
   u16vector->pylist        ;; (u16vector->pylist u16vector) → obj
   u32vector->pylist        ;; (u32vector->pylist u32vector) → obj
   f32vector->pylist        ;; (f32vector->pylist f32vector) → obj
   f64vector->pylist        ;; (f64vector->pylist f64vector) → obj

   s8vector-reverse->pylist  ;; (s8vector-reverse->pylist s8vector) → obj
   s16vector-reverse->pylist ;; (s16vector-reverse->pylist s16vector) → obj
   s32vector-reverse->pylist ;; (s32vector-reverse->pylist s32vector) → obj
   u8vector-reverse->pylist  ;; (u8vector-reverse->pylist u8vector) → obj
   u16vector-reverse->pylist ;; (u16vector-reverse->pylist u16vector) → obj
   u32vector-reverse->pylist ;; (u32vector-reverse->pylist u32vector) → obj
   f32vector-reverse->pylist ;; (f32vector-reverse->pylist f32vector) → obj
   f64vector-reverse->pylist ;; (f64vector-reverse->pylist f64vector) → obj

   s8vector->pytuple        ;; (s8vector->pytuple s8vector) → obj
   s16vector->pytuple       ;; (s16vector->pytuple s16vector) → obj
   s32vector->pytuple       ;; (s32vector->pytuple s32vector) → obj
   u8vector->pytuple        ;; (u8vector->pytuple u8vector) → obj
   u16vector->pytuple       ;; (u16vector->pytuple u16vector) → obj
   u32vector->pytuple       ;; (u32vector->pytuple u32vector) → obj
   f32vector->pytuple       ;; (f32vector->pytuple f32vector) → obj
   f64vector->pytuple       ;; (f64vector->pytuple f64vector) → obj

   s8vector-reverse->pytuple  ;; (s8vector-reverse->pytuple s8vector) → obj
   s16vector-reverse->pytuple ;; (s16vector-reverse->pytuple s16vector) → obj
   s32vector-reverse->pytuple ;; (s32vector-reverse->pytuple s32vector) → obj
   u8vector-reverse->pytuple  ;; (u8vector-reverse->pytuple u8vector) → obj
   u16vector-reverse->pytuple ;; (u16vector-reverse->pytuple u16vector) → obj
   u32vector-reverse->pytuple ;; (u32vector-reverse->pytuple u32vector) → obj
   f32vector-reverse->pytuple ;; (f32vector-reverse->pytuple f32vector) → obj
   f64vector-reverse->pytuple ;; (f64vector-reverse->pytuple f64vector) → obj

   pysequence-map->s8vector  ;; (pysequence-map->s8vector proc pyseq1 pyseq2 ...) → s8vector
   pysequence-map->s16vector ;; etc.
   pysequence-map->s32vector
   pysequence-map->s64vector
   pysequence-map->u8vector
   pysequence-map->u16vector
   pysequence-map->u32vector
   pysequence-map->u64vector
   pysequence-map->f32vector
   pysequence-map->f64vector

   pysequence-car           ;; (pysequence-car obj) → obj
   pysequence-cdr           ;; (pysequence-cdr obj) → obj
   pysequence-caar          ;; (pysequence-caar obj) → obj
   pysequence-cadr          ;; (pysequence-cadr obj) → obj
   pysequence-cdar          ;; (pysequence-cdar obj) → obj
   pysequence-cddr          ;; (pysequence-cddr obj) → obj
   pysequence-take          ;; (pysequence-take obj integer) → obj
   pysequence-drop          ;; (pysequence-drop obj integer) → obj
   pysequence-take-right    ;; (pysequence-take-right obj integer) → obj
   pysequence-drop-right    ;; (pysequence-drop-right obj integer) → obj

   pylist-insert!  ;; (pylist-insert! pylist i item) → *unspecified*
   pylist-append!  ;; (pylist-append! pylist item) → *unspecified*
   pylist-sort!    ;; (pylist-sort! pylist) → *unspecified*
   pylist-reverse! ;; (pylist-reverse! pylist) → *unspecified*
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pysequence_p
                          scm_pylist_p
                          scm_pylist_exact_p
                          scm_pytuple_p
                          scm_pytuple_exact_p
                          scm_pysequence_length
                          scm_pysequence_concat_list
                          scm_pysequence_repeat
                          scm_pysequence_inplaceconcat_list_x
                          scm_pysequence_inplacerepeat_x
                          scm_pysequence_getitem
                          scm_pysequence_getslice
                          scm_pysequence_setitem_x
                          scm_pysequence_delitem_x
                          scm_pysequence_setslice_x
                          scm_pysequence_delslice_x
                          scm_pysequence_count
                          scm_pysequence_contains_p
                          scm_pysequence_index
                          scm_pysequence_list
                          scm_pysequence_tuple

                          scm_pysequence_to_list_with_tail
                          scm_pysequence_to_list
                          scm_pysequence_reverse_to_list
                          scm_pysequence_reverse_to_pylist
                          scm_pysequence_reverse_to_pytuple
                          scm_pysequence_map_to_list

                          scm_pysequence_to_vector
                          scm_pysequence_reverse_to_vector
                          scm_pysequence_map_to_vector

                          scm_pysequence_for_each
                          scm_pysequence_fold_left
                          scm_pysequence_fold_right
                          scm_pysequence_for_all
                          scm_pysequence_exists

                          scm_pysequence_to_s8vector
                          scm_pysequence_to_s16vector
                          scm_pysequence_to_s32vector
                          scm_pysequence_to_u8vector
                          scm_pysequence_to_u16vector
                          scm_pysequence_to_u32vector
                          scm_pysequence_to_f32vector
                          scm_pysequence_to_f64vector

                          scm_pysequence_reverse_to_s8vector
                          scm_pysequence_reverse_to_s16vector
                          scm_pysequence_reverse_to_s32vector
                          scm_pysequence_reverse_to_u8vector
                          scm_pysequence_reverse_to_u16vector
                          scm_pysequence_reverse_to_u32vector
                          scm_pysequence_reverse_to_f32vector
                          scm_pysequence_reverse_to_f64vector

                          scm_list_to_pylist
                          scm_list_to_pytuple
                          scm_list_reverse_to_pylist
                          scm_list_reverse_to_pytuple

                          scm_list_map_to_pylist
                          scm_list_map_to_pytuple
                          scm_vector_map_to_pylist
                          scm_vector_map_to_pytuple

                          scm_vector_to_pylist
                          scm_vector_to_pytuple
                          scm_vector_reverse_to_pylist
                          scm_vector_reverse_to_pytuple

                          scm_s8vector_to_pylist
                          scm_s16vector_to_pylist
                          scm_s32vector_to_pylist
                          scm_u8vector_to_pylist
                          scm_u16vector_to_pylist
                          scm_u32vector_to_pylist
                          scm_f32vector_to_pylist
                          scm_f64vector_to_pylist

                          scm_s8vector_reverse_to_pylist
                          scm_s16vector_reverse_to_pylist
                          scm_s32vector_reverse_to_pylist
                          scm_u8vector_reverse_to_pylist
                          scm_u16vector_reverse_to_pylist
                          scm_u32vector_reverse_to_pylist
                          scm_f32vector_reverse_to_pylist
                          scm_f64vector_reverse_to_pylist

                          scm_s8vector_to_pytuple
                          scm_s16vector_to_pytuple
                          scm_s32vector_to_pytuple
                          scm_u8vector_to_pytuple
                          scm_u16vector_to_pytuple
                          scm_u32vector_to_pytuple
                          scm_f32vector_to_pytuple
                          scm_f64vector_to_pytuple

                          scm_s8vector_reverse_to_pytuple
                          scm_s16vector_reverse_to_pytuple
                          scm_s32vector_reverse_to_pytuple
                          scm_u8vector_reverse_to_pytuple
                          scm_u16vector_reverse_to_pytuple
                          scm_u32vector_reverse_to_pytuple
                          scm_f32vector_reverse_to_pytuple
                          scm_f64vector_reverse_to_pytuple

                          scm_pysequence_map_to_s8vector
                          scm_pysequence_map_to_s16vector
                          scm_pysequence_map_to_s32vector
                          scm_pysequence_map_to_s64vector
                          scm_pysequence_map_to_u8vector
                          scm_pysequence_map_to_u16vector
                          scm_pysequence_map_to_u32vector
                          scm_pysequence_map_to_u64vector
                          scm_pysequence_map_to_f32vector
                          scm_pysequence_map_to_f64vector

                          scm_pysequence_car
                          scm_pysequence_cdr
                          scm_pysequence_caar
                          scm_pysequence_cadr
                          scm_pysequence_cdar
                          scm_pysequence_cddr
                          scm_pysequence_take
                          scm_pysequence_drop
                          scm_pysequence_take_right
                          scm_pysequence_drop_right

                          scm_pylist_insert_x
                          scm_pylist_append_x
                          scm_pylist_sort_x
                          scm_pylist_reverse_x
                          ))

  (define-scm-procedure (pysequence? obj)
    "Is the argument a Guile-wrapped Python object supporting Python's
sequence protocol?"
    scm_pysequence_p)

  (define-scm-procedure (pylist? obj)
    "Is the argument a @code{pylist}?"
    scm_pylist_p)

  (define-scm-procedure (pylist-exact? obj)
    "Is the argument a @code{pylist} but not a member of a subtype of
@code{pylist}?"
    scm_pylist_exact_p)

  (define-scm-procedure (pytuple? obj)
    "Is the argument a @code{pytuple}?"
    scm_pytuple_p)

  (define-scm-procedure (pytuple-exact? obj)
    "Is the argument a @code{pytuple} but not a member of a subtype of
@code{pytuple}?"
    scm_pytuple_exact_p)

  (define-scm-procedure (pysequence-length obj)
    "A wrapper around @code{PySequence_Length}."
    scm_pysequence_length)

  (define-scm-procedure (pysequence-concat . obj-list)
    "A wrapper around @code{PySequence_Concat}. Accepts one or more
arguments."
    scm_pysequence_concat_list)

  (define-scm-procedure (pysequence-repeat obj count)
    "A wrapper around @code{PySequence_Repeat}. The @var{count}
argument may be either a Guile or a Python integer."
    scm_pysequence_repeat)

  (define-scm-procedure (pysequence-inplaceconcat! . obj-list)
    "A wrapper around @code{PySequence_InPlaceConcat}. Accepts one or
more arguments."
    scm_pysequence_inplaceconcat_list_x)

  (define-scm-procedure (pysequence-inplacerepeat obj count)
    "A wrapper around @code{PySequence_InPlaceRepeat}. The @var{count}
argument may be either a Guile or a Python integer."
    scm_pysequence_inplacerepeat_x)

  (define-scm-procedure (pysequence-getitem obj i)
    "A wrapper around @code{PySequence_GetItem}. The @var{i}
argument may be either a Guile or a Python integer."
    scm_pysequence_getitem)

  (define-scm-procedure (pysequence-getslice obj i1 i2)
    "A wrapper around @code{PySequence_GetSlice}. The @var{i1} and
@var{i2} arguments may be Guile or Python integers, respectively."
    scm_pysequence_getslice)

  (define-scm-procedure (pysequence-setitem! obj i v)
    "A wrapper around @code{PySequence_SetItem}. The @var{i}
argument may be either a Guile or a Python integer."
    scm_pysequence_setitem_x)

  (define-scm-procedure (pysequence-delitem! obj i)
    "A wrapper around @code{PySequence_DelItem}. The @var{i}
argument may be either a Guile or a Python integer."
    scm_pysequence_delitem_x)

  (define-scm-procedure (pysequence-setslice! obj i1 i2 v)
    "A wrapper around @code{PySequence_SetSlice}. The @var{i1} and
@var{i2} arguments may be either Guile or Python integers,
respectively."
    scm_pysequence_setslice_x)

  (define-scm-procedure (pysequence-delslice! obj i1 i2)
    "A wrapper around @code{PySequence_DelSlice}. The @var{i1} and
@var{i2} arguments may be either Guile or Python integers,
respectively."
    scm_pysequence_delslice_x)

  (define-scm-procedure (pysequence-count obj v)
    "A wrapper around @code{PySequence_Count}. Returns a Guile
integer (as opposed to a Python integer)."
    scm_pysequence_count)

  (define-scm-procedure (pysequence-contains? obj v)
    "A wrapper around @code{PySequence_Contains}. Returns a Guile
boolean (as opposed to a Python boolean)."
    scm_pysequence_contains_p)

  (define-scm-procedure (pysequence-index obj v)
    "A wrapper around @code{PySequence_Index}. Returns a Guile
integer (as opposed to a Python integer)."
    scm_pysequence_index)

  (define-scm-procedure (pysequence-list obj)
    "A wrapper around @code{PySequence_List}."
    scm_pysequence_list)

  (define-scm-procedure (pysequence-tuple obj)
    "A wrapper around @code{PySequence_Tuple}."
    scm_pysequence_tuple)

  (define*-scm-procedure (pysequence->list obj #:optional tail)
    "Convert a Guile-wrapped Python sequence to a Guile list of
Guile-wrapped Python objects. Optionally provide a `tail' other than
an empty list."
    scm_pysequence_to_list_with_tail)

  (define-scm-procedure (pysequence-reverse->list obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile
list of Guile-wrapped Python objects. (This procedure is about as fast
as @code{pysequence->list}.)"
    scm_pysequence_reverse_to_list)

  (define-scm-procedure (pysequence-reverse->pylist obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order
Guile-wrapped Python list."
    scm_pysequence_reverse_to_pylist)

  (define-scm-procedure (pysequence-reverse->pylist obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order
Guile-wrapped Python list."
    scm_pysequence_reverse_to_pylist)

  (define-scm-procedure (pysequence-reverse->pytuple obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order
Guile-wrapped Python tuple."
    scm_pysequence_reverse_to_pytuple)

  (define-scm-procedure (pysequence-map->list proc . obj-list)
    "A `map' procedure for PySequence objects, returning the results
as a Guile list. The order of application is unspecified. FIXME: Write
better documentation for this procedure."
    scm_pysequence_map_to_list)

  (define-scm-procedure (pysequence->vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile vector of
Guile-wrapped Python objects."
    scm_pysequence_to_vector)

  (define-scm-procedure (pysequence-reverse->vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile
vector of Guile-wrapped Python objects."
    scm_pysequence_reverse_to_vector)

  (define-scm-procedure (pysequence-map->vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_vector)

  (define-scm-procedure (pysequence-for-each proc . obj-list)
    "A `for-each' procedure for PySequence objects. The order of
application is left-to-right. FIXME: Write better documentation for
this procedure."
    scm_pysequence_for_each)

  (define-scm-procedure (pysequence-fold-left proc init . obj-list)
    "A `fold-left' procedure for PySequence objects. FIXME: Write
better documentation for this procedure."
    scm_pysequence_fold_left)

  (define-scm-procedure (pysequence-fold-right proc init . obj-list)
    "A `fold-right' procedure for PySequence objects. FIXME: Write
better documentation for this procedure."
    scm_pysequence_fold_right)

  (define-scm-procedure (pysequence-for-all proc . obj-list)
    "A `for-all' procedure for PySequence objects. FIXME: Write better
documentation for this procedure."
    scm_pysequence_for_all)

  (define-scm-procedure (pysequence-exists proc . obj-list)
    "An `exists' procedure for PySequence objects. FIXME: Write better
documentation for this procedure."
    scm_pysequence_exists)

  (define-scm-procedure (list->pylist lst)
    "Convert an acyclic list of Guile-wrapped Python objects to a
Guile-wrapped Python list. The behavior is unspecified if the argument
is a cyclic list."
    scm_list_to_pylist)

  (define-scm-procedure (list->pytuple lst)
    "Convert an acyclic list of Guile-wrapped Python objects to a
Guile-wrapped Python tuple. The behavior is unspecified if the
argument is a cyclic list."
    scm_list_to_pytuple)

  (define-scm-procedure (list-reverse->pylist lst)
    "Convert an acyclic list of Guile-wrapped Python objects to a
Guile-wrapped Python list having the same elements but in the reverse
order. The behavior is unspecified if the argument is a cyclic
list. (This procedure is about as fast as @code{list->pylist}.)"
    scm_list_reverse_to_pylist)

  (define-scm-procedure (list-reverse->pytuple lst)
    "Convert an acyclic list of Guile-wrapped Python objects to a
Guile-wrapped Python tuple having the same elements but in the reverse
order. The behavior is unspecified if the argument is a cyclic
list. (This procedure is about as fast as @code{list->pytuple}.)"
    scm_list_reverse_to_pytuple)

  (define-scm-procedure (list-map->pylist proc . lists)
    "Map acyclic lists of Guile objects to a Guile-wrapped Python
list. The behavior is unspecified if the arguments include any cyclic
lists. The shortest input list determines the size of the returned
Python list."
    scm_list_map_to_pylist)

  (define-scm-procedure (list-map->pytuple proc . lists)
    "Map acyclic lists of Guile objects to a Guile-wrapped Python
tuple. The behavior is unspecified if the arguments include any cyclic
lists. The shortest input list determines the size of the returned
Python tuple."
    scm_list_map_to_pytuple)

  (define-scm-procedure (vector-map->pylist proc . vectors)
    "Map vectors of Guile objects to a Guile-wrapped Python list. The
shortest input vector determines the size of the returned Python
list."
    scm_vector_map_to_pylist)

  (define-scm-procedure (vector-map->pytuple proc . vectors)
    "Map vectors of Guile objects to a Guile-wrapped Python tuple. The
shortest input vector determines the size of the returned Python
tuple."
    scm_vector_map_to_pytuple)

  (define-scm-procedure (vector->pylist v)
    "Convert an vector of Guile-wrapped Python objects to a
Guile-wrapped Python list."
    scm_vector_to_pylist)

  (define-scm-procedure (vector->pytuple v)
    "Convert an vector of Guile-wrapped Python objects to a
Guile-wrapped Python tuple."
    scm_vector_to_pytuple)

  (define-scm-procedure (vector-reverse->pylist v)
    "Convert an vector of Guile-wrapped Python objects to a
reversed-order Guile-wrapped Python list."
    scm_vector_reverse_to_pylist)

  (define-scm-procedure (vector-reverse->pytuple v)
    "Convert an vector of Guile-wrapped Python objects to a
reversed-order Guile-wrapped Python tuple."
    scm_vector_reverse_to_pytuple)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (pysequence->s8vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile s8vector."
    scm_pysequence_to_s8vector)

  (define-scm-procedure (pysequence->s16vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile s16vector."
    scm_pysequence_to_s16vector)

  (define-scm-procedure (pysequence->s32vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile s32vector."
    scm_pysequence_to_s32vector)

  (define-scm-procedure (pysequence->u8vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile u8vector."
    scm_pysequence_to_u8vector)

  (define-scm-procedure (pysequence->u16vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile u16vector."
    scm_pysequence_to_u16vector)

  (define-scm-procedure (pysequence->u32vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile u32vector."
    scm_pysequence_to_u32vector)

  (define-scm-procedure (pysequence->f32vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile f32vector."
    scm_pysequence_to_f32vector)

  (define-scm-procedure (pysequence->f64vector obj)
    "Convert a Guile-wrapped Python sequence to a Guile f64vector."
    scm_pysequence_to_f64vector)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (pysequence-reverse->s8vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile s8vector."
    scm_pysequence_reverse_to_s8vector)

  (define-scm-procedure (pysequence-reverse->s16vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile s16vector."
    scm_pysequence_reverse_to_s16vector)

  (define-scm-procedure (pysequence-reverse->s32vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile s32vector."
    scm_pysequence_reverse_to_s32vector)

  (define-scm-procedure (pysequence-reverse->u8vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile u8vector."
    scm_pysequence_reverse_to_u8vector)

  (define-scm-procedure (pysequence-reverse->u16vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile u16vector."
    scm_pysequence_reverse_to_u16vector)

  (define-scm-procedure (pysequence-reverse->u32vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile u32vector."
    scm_pysequence_reverse_to_u32vector)

  (define-scm-procedure (pysequence-reverse->f32vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile f32vector."
    scm_pysequence_reverse_to_f32vector)

  (define-scm-procedure (pysequence-reverse->f64vector obj)
    "Convert a Guile-wrapped Python sequence to a reversed-order Guile f64vector."
    scm_pysequence_reverse_to_f64vector)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (s8vector->pylist v)
    "Convert a Guile s8vector to a Guile-wrapped Python list."
    scm_s8vector_to_pylist)

  (define-scm-procedure (s16vector->pylist v)
    "Convert a Guile s16vector to a Guile-wrapped Python list."
    scm_s16vector_to_pylist)

  (define-scm-procedure (s32vector->pylist v)
    "Convert a Guile s32vector to a Guile-wrapped Python list."
    scm_s32vector_to_pylist)

  (define-scm-procedure (u8vector->pylist v)
    "Convert a Guile u8vector to a Guile-wrapped Python list."
    scm_u8vector_to_pylist)

  (define-scm-procedure (u16vector->pylist v)
    "Convert a Guile u16vector to a Guile-wrapped Python list."
    scm_u16vector_to_pylist)

  (define-scm-procedure (u32vector->pylist v)
    "Convert a Guile u32vector to a Guile-wrapped Python list."
    scm_u32vector_to_pylist)

  (define-scm-procedure (f32vector->pylist v)
    "Convert a Guile f32vector to a Guile-wrapped Python list."
    scm_f32vector_to_pylist)

  (define-scm-procedure (f64vector->pylist v)
    "Convert a Guile f64vector to a Guile-wrapped Python list."
    scm_f64vector_to_pylist)

  ;;-----------------------------------------------------------------------
  
  (define-scm-procedure (s8vector-reverse->pylist v)
    "Convert a Guile s8vector to a reversed-order Guile-wrapped Python list."
    scm_s8vector_reverse_to_pylist)

  (define-scm-procedure (s16vector-reverse->pylist v)
    "Convert a Guile s16vector to a reversed-order Guile-wrapped Python list."
    scm_s16vector_reverse_to_pylist)

  (define-scm-procedure (s32vector-reverse->pylist v)
    "Convert a Guile s32vector to a reversed-order Guile-wrapped Python list."
    scm_s32vector_reverse_to_pylist)

  (define-scm-procedure (u8vector-reverse->pylist v)
    "Convert a Guile u8vector to a reversed-order Guile-wrapped Python list."
    scm_u8vector_reverse_to_pylist)

  (define-scm-procedure (u16vector-reverse->pylist v)
    "Convert a Guile u16vector to a reversed-order Guile-wrapped Python list."
    scm_u16vector_reverse_to_pylist)

  (define-scm-procedure (u32vector-reverse->pylist v)
    "Convert a Guile u32vector to a reversed-order Guile-wrapped Python list."
    scm_u32vector_reverse_to_pylist)

  (define-scm-procedure (f32vector-reverse->pylist v)
    "Convert a Guile f32vector to a reversed-order Guile-wrapped Python list."
    scm_f32vector_reverse_to_pylist)

  (define-scm-procedure (f64vector-reverse->pylist v)
    "Convert a Guile f64vector to a reversed-order Guile-wrapped Python list."
    scm_f64vector_reverse_to_pylist)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (s8vector->pytuple v)
    "Convert a Guile s8vector to a Guile-wrapped Python tuple."
    scm_s8vector_to_pytuple)

  (define-scm-procedure (s16vector->pytuple v)
    "Convert a Guile s16vector to a Guile-wrapped Python tuple."
    scm_s16vector_to_pytuple)

  (define-scm-procedure (s32vector->pytuple v)
    "Convert a Guile s32vector to a Guile-wrapped Python tuple."
    scm_s32vector_to_pytuple)

  (define-scm-procedure (u8vector->pytuple v)
    "Convert a Guile u8vector to a Guile-wrapped Python tuple."
    scm_u8vector_to_pytuple)

  (define-scm-procedure (u16vector->pytuple v)
    "Convert a Guile u16vector to a Guile-wrapped Python tuple."
    scm_u16vector_to_pytuple)

  (define-scm-procedure (u32vector->pytuple v)
    "Convert a Guile u32vector to a Guile-wrapped Python tuple."
    scm_u32vector_to_pytuple)

  (define-scm-procedure (f32vector->pytuple v)
    "Convert a Guile f32vector to a Guile-wrapped Python tuple."
    scm_f32vector_to_pytuple)

  (define-scm-procedure (f64vector->pytuple v)
    "Convert a Guile f64vector to a Guile-wrapped Python tuple."
    scm_f64vector_to_pytuple)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (s8vector-reverse->pytuple v)
    "Convert a Guile s8vector to a reversed-order Guile-wrapped Python tuple."
    scm_s8vector_reverse_to_pytuple)

  (define-scm-procedure (s16vector-reverse->pytuple v)
    "Convert a Guile s16vector to a reversed-order Guile-wrapped Python tuple."
    scm_s16vector_reverse_to_pytuple)

  (define-scm-procedure (s32vector-reverse->pytuple v)
    "Convert a Guile s32vector to a reversed-order Guile-wrapped Python tuple."
    scm_s32vector_reverse_to_pytuple)

  (define-scm-procedure (u8vector-reverse->pytuple v)
    "Convert a Guile u8vector to a reversed-order Guile-wrapped Python tuple."
    scm_u8vector_reverse_to_pytuple)

  (define-scm-procedure (u16vector-reverse->pytuple v)
    "Convert a Guile u16vector to a reversed-order Guile-wrapped Python tuple."
    scm_u16vector_reverse_to_pytuple)

  (define-scm-procedure (u32vector-reverse->pytuple v)
    "Convert a Guile u32vector to a reversed-order Guile-wrapped Python tuple."
    scm_u32vector_reverse_to_pytuple)

  (define-scm-procedure (f32vector-reverse->pytuple v)
    "Convert a Guile f32vector to a reversed-order Guile-wrapped Python tuple."
    scm_f32vector_reverse_to_pytuple)

  (define-scm-procedure (f64vector-reverse->pytuple v)
    "Convert a Guile f64vector to a reversed-order Guile-wrapped Python tuple."
    scm_f64vector_reverse_to_pytuple)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (pysequence-map->s8vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the
results as a Guile s8vector. The order of application is
unspecified. FIXME: Write better documentation for this procedure."
    scm_pysequence_map_to_s8vector)

  (define-scm-procedure (pysequence-map->s16vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile s16vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_s16vector)

  (define-scm-procedure (pysequence-map->s32vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile s32vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_s32vector)

  (define-scm-procedure (pysequence-map->s64vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile s64vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_s64vector)

  (define-scm-procedure (pysequence-map->u8vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile u8vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_u8vector)

  (define-scm-procedure (pysequence-map->u16vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile u16vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_u16vector)

  (define-scm-procedure (pysequence-map->u32vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile u32vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_u32vector)

  (define-scm-procedure (pysequence-map->u64vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile u64vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_u64vector)

  (define-scm-procedure (pysequence-map->f32vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile f32vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_f32vector)

  (define-scm-procedure (pysequence-map->f64vector proc . obj-lst)
    "A `map' procedure for PySequence objects, returning the results
as a Guile f64vector. The order of application is unspecified. FIXME:
Write better documentation for this procedure."
    scm_pysequence_map_to_f64vector)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (pysequence-car obj)
    "Return the first item in a Guile-wrapped PySequence object. The
behavior is unspecified if the sequence length is zero."
    scm_pysequence_car)

  (define-scm-procedure (pysequence-cdr obj)
    "Drop the first item in a Guile-wrapped PySequence object. The
behavior is unspecified if the sequence length is zero."
    scm_pysequence_cdr)

  (define-scm-procedure (pysequence-caar obj)
    "FIXME: Document this."
    scm_pysequence_caar)

  (define-scm-procedure (pysequence-cadr obj)
    "Return the second item in a Guile-wrapped PySequence object. The
behavior is unspecified if the sequence length is less than two."
    scm_pysequence_cadr)

  (define-scm-procedure (pysequence-cdar obj)
    "FIXME: Document this."
    scm_pysequence_cdar)

  (define-scm-procedure (pysequence-cddr obj)
    "FIXME: Document this."
    scm_pysequence_cddr)

  (define-scm-procedure (pysequence-take obj n)
    "Take the first @var{n} items of a Python sequence. The behavior
is unspecified for @var{n} negative or greater than the length of the
sequence."
    scm_pysequence_take)

  (define-scm-procedure (pysequence-drop obj n)
    "Drop the first @var{n} items of a Python sequence. The behavior
is unspecified for @var{n} negative or greater than the length of the
sequence."
    scm_pysequence_drop)

  (define-scm-procedure (pysequence-take-right obj n)
    "Take the last @var{n} items of a Python sequence. The behavior is
unspecified for @var{n} negative or greater than the length of the
sequence."
    scm_pysequence_take_right)

  (define-scm-procedure (pysequence-drop-right obj n)
    "Drop the last @var{n} items of a Python sequence. The behavior is
unspecified for @var{n} negative or greater than the length of the
sequence."
    scm_pysequence_drop_right)

  (define-scm-procedure (pylist-insert! pylist i item)
    "A wrapper around @code{PyList_Insert}. The index argument @var{i}
may be either a Python or a Guile integer."
    scm_pylist_insert_x)

  (define-scm-procedure (pylist-append! pylist item)
    "A wrapper around @code{PyList_Append}."
    scm_pylist_append_x)

  (define-scm-procedure (pylist-sort! pylist)
    "A wrapper around @code{PyList_Sort}."
    scm_pylist_sort_x)

  (define-scm-procedure (pylist-reverse! pylist)
    "A wrapper around @code{PyList_Reverse}."
    scm_pylist_reverse_x)

   ) ;; end of library.
