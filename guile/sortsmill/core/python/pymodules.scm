;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pymodules)

  (export
   pymodule?             ;; (pymodule? obj) → boolean
   pymodule-new          ;; (pymodule-new name) → obj
   pymodule-getdict      ;; (pymodule-getdict obj) → obj
   pymodule-getname      ;; (pymodule-getname obj) → string
   pymodule-getfilename  ;; (pymodule-getfilename obj) → string
   pymodule-addobject!   ;; (pymodule-addobject! obj string value) → *unspecified*

   pyimport-importmodule  ;; (pyimport-importmodule name [globals [locals [fromlist [level]]]]) → obj
   pyimport-import        ;; (pyimport-import name) → obj
   pyimport-reloadmodule  ;; (pyimport-reloadmodule obj) → obj
   pyimport-addmodule!    ;; (pyimport-addmodule! name) → obj
   pyimport-getmoduledict ;; (pyimport-getmoduledict) → obj
   py-sys.modules         ;; (py-sys.modules) → obj  [a synonym for pyimport-getmoduledict]

   pymodule-name->module-name ;; (pymodule-name->module-name string) → list-of-symbols
   pyinit-name                ;; (pyinit-name string) → symbol
   pyinit-variable            ;; (pyinit-variable module-name pyinit-name) → variable
   pymodule-pyinit-variable   ;; (pymodule-pyinit-variable module-name pyinit-name) → variable
   pyinit-procedure           ;; (pyinit-procedure module-name pyinit-name) → procedure
   pymodule-pyinit-procedure  ;; (pymodule-pyinit-procedure module-name pyinit-name) → procedure
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core kwargs)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          )

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pymodule_p
                          scm_pymodule_new
                          scm_pymodule_getdict
                          scm_pymodule_getname
                          scm_pymodule_getfilename
                          scm_pymodule_addobject_x
                          scm_pyimport_importmodule
                          scm_pyimport_import
                          scm_pyimport_reloadmodule
                          scm_pyimport_addmodule_x
                          scm_pyimport_getmoduledict
                          scm_pymodule_name_to_module_name
                          scm_pyinit_name
                          scm_pyinit_variable
                          scm_pymodule_pyinit_variable
                          scm_pyinit_procedure
                          scm_pymodule_pyinit_procedure
                          ))

  (define-scm-procedure (pymodule? obj)
    "Is the argument a Guile-wrapped Python module?"
    scm_pymodule_p)

  (define-scm-procedure (pymodule-new name)
    "A wrapper around @code{PyModule_New}. The argument may be either
a Guile or a Python string."
    scm_pymodule_new)

  (define-scm-procedure (pymodule-getdict module)
    "A wrapper around @code{PyModule_GetDict}."
    scm_pymodule_getdict)

  (define-scm-procedure (pymodule-getname module)
    "A wrapper around @code{PyModule_GetName}."
    scm_pymodule_getname)

  (define-scm-procedure (pymodule-getfilename module)
    "A wrapper around @code{PyModule_GetFilename}."
    scm_pymodule_getfilename)

  (define-scm-procedure (pymodule-addobject! module key value)
    "A wrapper around @code{PyModule_AddObject}."
    scm_pymodule_addobject_x)

  (define pyimport-importmodule
    (let ([proc (lambda-scm-procedure (name globals locals fromlist level)
                  scm_pyimport_importmodule)])
      (lambda/kwargs (name globals locals fromlist level)
        "A wrapper around @code{PyModule_ImportModuleLevel}. All the
arguments except @var{name} are optional, and all the arguments can be
referred to by keyword if one prefers."
        (assert name)
        (proc name globals locals fromlist level))))

  (define-scm-procedure (pyimport-import name)
    "A wrapper around @code{PyImport_Import}."
    scm_pyimport_import)

  (define-scm-procedure (pyimport-reloadmodule module)
    "A wrapper around @code{PyImport_ReloadModule}."
    scm_pyimport_reloadmodule)

  (define-scm-procedure (pyimport-addmodule! name)
    "A wrapper around @code{PyImport_AddModule}."
    scm_pyimport_addmodule_x)

  (define-scm-procedure (pyimport-getmoduledict)
    "A wrapper around @code{PyImport_GetModuleDict}. What this
procedure returns is a reference to the (per interpreter)
@code{sys.modules} dictionary."
    scm_pyimport_getmoduledict)

  (define py-sys.modules pyimport-getmoduledict)

  (define-scm-procedure (pymodule-name->module-name pymodule-name)
    "Convert a Python module name to the name of a Guile module. For
example: @code{\"a.b.c\"} yields @code{'(a b c)}."
    scm_pymodule_name_to_module_name)

  (define-scm-procedure (pyinit-name pymodule-name)
    "Convert a Python module name to the name of a Guile
initialization procedure. For example: @code{\"a.b.c\"} yields
@code{'PyInit-a.b.c}."
    scm_pyinit_name)

  (define-scm-procedure (pyinit-variable guile-module-name pyinit-name)
    "Return the variable that contains the Guile initialization
procedure for a Python module; but return @code{#f} if the variable
cannot be found."
    scm_pyinit_variable)

  (define-scm-procedure (pyinit-variable guile-module-name pyinit-name)
    "Return the variable that contains the Guile initialization
procedure for a Python module; but return @code{#f} if the variable
cannot be found."
    scm_pyinit_variable)

  (define-scm-procedure (pymodule-pyinit-variable pymodule-name)
    "Return the variable that contains the Guile initialization
procedure for a Python module; but return @code{#f} if the variable
cannot be found."
    scm_pymodule_pyinit_variable)

  (define-scm-procedure (pyinit-procedure guile-module-name pyinit-name)
    "Return the Guile initialization procedure for a Python module;
but return @code{#f} if the procedure cannot be found."
    scm_pyinit_procedure)

  (define-scm-procedure (pymodule-pyinit-procedure pymodule-name)
    "Return the Guile initialization procedure for a Python module;
but return @code{#f} if the procedure cannot be found."
    scm_pymodule_pyinit_procedure)

  ) ;; end of library.
