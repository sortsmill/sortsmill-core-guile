;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pymethods)

  (export
   pymethod?             ;; (pymethod? obj) → boolean
   pyinstancemethod?     ;; (pyinstancemethod? obj) → boolean
   pyinstancemethod-new  ;; (pyinstancemethod-new func class) → obj
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core python version)
          (sortsmill core kwargs)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pymethod_p
                          scm_pyinstancemethod_new
                          scm_pyinstancemethod_p
                          ))

  (define-scm-procedure (pymethod? obj)
    "Is the argument a Guile-wrapped Python method?"
    scm_pymethod_p)

  (define-scm-procedure (pyinstancemethod? obj)
     "Is the argument a Guile-wrapped Python 3 instance method? (For
Python 2, this procedure always returns @code{#f}.)"
     scm_pyinstancemethod_p)

  (define-scm-procedure (pyinstancemethod-new func class)
    "For Python 2, a wrapper around @code{PyMethod_New}. For Python 3,
a wrapper around @code{PyInstanceMethod_New} (in which case the
@var{class} argument is ignored). FIXME: Improve this documentation."
    scm_pyinstancemethod_new)

  ) ;; end of library.
