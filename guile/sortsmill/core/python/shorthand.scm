;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python shorthand)

  (export py)

  (import (rnrs)
          (only (srfi :1) drop-right last)
          (only (srfi :26) cut)
          (except (guile) error)
          (ice-9 match)
          (ice-9 format)
          (sortsmill core python pyobjects)
          (sortsmill core python pynumbers)
          (sortsmill core python pysequences)
          (sortsmill core python pymappings)
          (sortsmill core python pymodules)
          (sortsmill core python pyscms)
          (sortsmill core python pysets)
          (sortsmill core python friendlier)
          (sortsmill core python version)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  ;; Python shorthand.
  ;;
  ;; (py 3) → Python-integer (int or long, according to circumstances)
  ;; (py 3.0) → Python-float
  ;; (py 3+4i) → Python-complex
  ;; (py "string") → Python-unicode
  ;; (py (#:bytes "string")) → Python-bytes
  ;; (py (#:bytes 3 5 6 1 255 0)) → Python-bytes
  ;; (py #f) → Python-False
  ;; (py #t) → Python-True
  ;; (py False) → Python-False
  ;; (py True) → Python-True
  ;; (py None) → Python-None
  ;; (py (#:tuple 3 #f None)) → Python-tuple
  ;; (py (#:list 3 #f None)) → Python-list
  ;; (py (#:dict (3 . #f) (None . None)) → Python-dict
  ;; (py (#:set 3 4 ("a" "b" "c") 5)) → Python-set
  ;; (py (#:frozenset "abcde" 3 (1 2 3))) → Python-frozenset
  ;;
  ;; etc.
  ;;
  ;; FIXME: Document this feature better and more completely.
  ;;

  (define (py-syntax-violation . args)
    (apply syntax-violation 'py (_ "py expression syntax error")
           (map syntax->datum args)))

  (define (py-exact-arg-count-violation who count stx expr)
    (syntax-violation
     'py
     (format #f
             (_ "~0@*~a expects exactly this many arguments: ~1@*~a")
             who count)
     (syntax->datum stx) (syntax->datum expr)))

  (define (py-min-arg-count-violation who min stx expr)
    (syntax-violation
     'py
     (format #f
             (_ "~0@*~a expects at least this many arguments: ~1@*~a")
             who min)
     (syntax->datum stx) (syntax->datum expr)))

  (define (py-max-arg-count-violation who max stx expr)
    (syntax-violation
     'py
     (format #f
             (_ "~0@*~a expects at most this many arguments: ~1@*~a")
             who max)
     (syntax->datum stx) (syntax->datum expr)))

  (define (py-min-max-arg-count-violation who min max stx expr)
    (syntax-violation
     'py
     (format #f
             (_ "~0@*~a expects a number of arguments within this range: [~1@*~a,~2@*~a]")
             who min max)
     (syntax->datum stx) (syntax->datum expr)))

  (define (py-identifier-violation who stx expr)
    (syntax-violation 'py (format #f (_ "~0@*~a: expected a Python identifier")
                                  who)
                      (syntax->datum stx) (syntax->datum expr)))

  (define (py-key-value-pair-violation who stx expr)
    (syntax-violation 'py (format #f (_ "~0@*~a: expected a key-value pair") who)
                      (syntax->datum stx) (syntax->datum expr)))

  (define (py-bad-bytes-element-violation who elements-lst stx expr)
    (let ([bad-values (filter (negate byte?) elements-lst)])
      (syntax-violation 'py (format #f (_ "~0@*~a: bad bytes elements: ~1@*~a")
                                    who bad-values)
                        (syntax->datum stx) (syntax->datum expr))))

  (define (py-bad-str-violation who stx expr)
    (syntax-violation 'py (format #f (_ "~0@*~a: expected a string") who)
                      (syntax->datum stx) (syntax->datum expr)))

  (define-syntax py
    (lambda (stx)
      "FIXME: Document this."
      (syntax-case stx ()
        [(_ expr) (expr->python stx #'expr)]
        [_ (py-syntax-violation stx)])))

  (define (exact-integer? n)
    (let ([n (syntax->datum n)])
      (and (integer? n) (exact? n))))

  (define (byte? n)
    (let ([n (syntax->datum n)])
      (and (exact-integer? n) (<= 0 n 255))))

  (define (inexact-real? n)
    (let ([n (syntax->datum n)])
      (and (real? n) (inexact? n))))

  (define (inexact-complex? n)
    (let ([n (syntax->datum n)])
      (and (complex? n) (inexact? n))))

  (define (string^? s)
    (let ([s (syntax->datum s)])
      (string? s)))

  (define (bytevector^? b)
    (let ([b (syntax->datum b)])
      (bytevector? b)))

  (define (non-keyword? e)
    (let ([e (syntax->datum e)])
      (not (keyword? e))))

  (define (py-identifier-start-char? c)
    ;; FIXME: Add support for ‘Other_ID_Start’ characters.
    (cond [(char=? c #\_) #t]
          [else (match (char-general-category c)
                  [(or 'Lu 'Ll 'Lt 'Lm 'Lo 'Nl) #t]
                  [_ #f])]))

  (define (py-identifier-continuation-char? c)
    ;; FIXME: Add support for ‘Other_ID_Continue’ characters.
    (cond [(py-identifier-start-char? c) #t]
          [else (match (char-general-category c)
                  [(or 'Mn 'Mc 'Nd 'Pc) #t]
                  [_ #f])]))

  (define (py-identifier-symbol? s)
    ;; FIXME: Currently this is only a subset of legal Python
    ;; identifiers, because ‘Other_ID_Start’ and ‘Other_ID_Continue’
    ;; are not considered.
    ;;
    ;; Keep in mind that most of the possible identifiers are not
    ;; legal in Python 2.
    ;;
    (let ([s (syntax->datum s)])
      (and (symbol? s)
           (let ([t (string-normalize-nfkc (symbol->string s))])
             (and (not (string-null? t))
                  (py-identifier-start-char? (string-ref t 0))
                  (string-every py-identifier-continuation-char? t))))))

  (define (py-identifier-symbol-sequence? s)
    (let ([s (syntax->datum s)])
      (and (symbol? s)
           (let* ([t (string-normalize-nfkc (symbol->string s))]
                  [components (string-split t #\.)])
             (for-all (compose py-identifier-symbol? string->symbol)
                      components)))))

  (define (split-py-identifier-symbol-sequence s)
    (let* ([t (string-normalize-nfkc (symbol->string (syntax->datum s)))]
           [components (string-split t #\.)]
           [module-key (string-join (drop-right components 1) ".")]
           [variable-key (last components)])
      (values module-key variable-key)))

  (define (expr->python stx expr)

    (define (x->py elem) (expr->python stx elem))
    (define (x-map->py elems) (map x->py elems))

    (define (pair->py who pair)
      (syntax-case pair ()
        [(k . v) #`(cons #,(x->py #'k) #,(x->py #'v))]
        [_ (py-key-value-pair-violation who stx pair)]))

    (define (pair-map->py who pairs)
      (map (cut pair->py who <>) pairs))

    (syntax-case expr (False True None + - * floor/ / %)

      ;; Numeric literals.
      [n (exact-integer? #'n)   #'(integer->pyinteger n)]
      [n (inexact-real? #'n)    #'(real->pyfloat n)]
      [n (inexact-complex? #'n) #'(number->pycomplex n)]

      ;; ‘Unicode’ string literals.
      [s (string^? #'s)         #'(string->pyunicode s)]

      ;; ‘Bytes’ objects (Python 2 strings) from literal
      ;; strings. Encoding is UTF-8.
      [(#:bytes s)
       (string^? #'s)           #`(bytevector->pybytes
                                   #,(datum->syntax
                                      stx (string->utf8 (syntax->datum #'s))))]

      ;; ‘Bytes’ objects (Python 2 strings) from literal runs of
      ;; integers between 0 and 255, inclusive.
      [(#:bytes e ...)
       (for-all byte? #'(e ...)) #`(bytevector->pybytes
                                    #,(datum->syntax
                                       stx (u8-list->bytevector
                                            (map syntax->datum #'(e ...)))))]

      [(#:bytes e ...)          (py-bad-bytes-element-violation
                                 "#:bytes" (syntax->datum #'(e ...)) stx expr)]

      ;; ‘Bytes’ objects if running Python 2; otherwise ‘Unicode’
      ;; objects.
      [(#:str s) (string^? #'s) (if (<= PY_MAJOR_VERSION 2)
                                    #`(bytevector->pybytes
                                       #,(datum->syntax
                                          stx (string->utf8 (syntax->datum #'s))))
                                    #'(string->pyunicode s))]
      [(#:str e ...)            (py-bad-str-violation "#:str" stx expr)]

      ;; Literal True, False, and None.
      ;;
      ;; For efficiency, ‘False’, ‘True’, and ‘None’ are computed
      ;; directly, rather than fetched from
      ;; dictionaries. Consequently, changes in or ‘shadowing’ of
      ;; their bindings will not be propagated. Such changes and
      ;; ‘shadowing’ of these symbols would be a Very Bad Idea,
      ;; anyway.
      [#f                       #'(pybool #f)]
      [False                    #'(pybool #f)]
      [#t                       #'(pybool #t)]
      [True                     #'(pybool #t)]
      [None                     #'(py-none)]

      ;; ‘Arithmetic’ operations. (In Python these can mean many
      ;; things besides actual arithmetic operations.)
      [(+ e e* ...)             #`(py+ #,(x->py #'e) #,@(x-map->py #'(e* ...)))]
      [(- e e* ...)             #`(py- #,(x->py #'e) #,@(x-map->py #'(e* ...)))]
      [(* e e* ...)             #`(py* #,(x->py #'e) #,@(x-map->py #'(e* ...)))]
      [(floor/ e1 e2)           #`(py-floor/ #,(x->py #'e1) #,(x->py #'e2))]
      [(/ e1 e2 e* ...)         #`(py/ #,(x->py #'e1) #,(x->py #'e2)
                                       #,@(x-map->py #'(e* ...)))]
      [(% e1 e2)                #`(py% #,(x->py #'e1) #,(x->py #'e2))]
      [(+ e ...)                (py-min-arg-count-violation '+ 1 stx expr)]
      [(- e ...)                (py-min-arg-count-violation '- 1 stx expr)]
      [(* e ...)                (py-min-arg-count-violation '* 1 stx expr)]
      [(floor/ e ...)           (py-exact-arg-count-violation 'floor/ 2 stx expr)]
      [(/ e ...)                (py-min-arg-count-violation '/ 2 stx expr)]
      [(% e ...)                (py-exact-arg-count-violation '% 2 stx expr)]

      ;; Dereferencing a Python variable.
      [s (py-identifier-symbol? #'s) #`(py-var-ref
                                        (string->pyunicode
                                         #,(symbol->string (syntax->datum #'s))))]

      ;; Dereferencing a Python variable in a module. Importation is
      ;; automatic.
      [s (py-identifier-symbol-sequence? #'s)
         (let-values ([(module-key variable-key)
                       (split-py-identifier-symbol-sequence #'s)])
           #`(py-module-var-ref #,module-key #,variable-key))]

      ;; Dereferencing an item in a Python object.
      [(#:item obj key)         #`(py-item-ref #,(x->py #'obj) #,(x->py #'key))]
      [(#:item e ...)           (py-min-max-arg-count-violation "#:item" 1 2 stx expr)]

      ;; Dereferencing one of an object’s attributes.
      [(#:attr obj key)         #`(py-attr-ref #,(x->py #'obj) #,(x->py #'key))]
      [(#:attr e ...)           (py-exact-arg-count-violation "#:attr" 2 stx expr)]

      ;; Calling (applying) a function or other Python callable, or a
      ;; method with the method name given before the ‘self’
      ;; object. You can leave out the #:call keyword, in which case
      ;; the expression looks like Scheme procedure application.
      [(#:call f arg ...)       #`(py-call #,(x->py #'f) #,@(x-map->py #'(arg ...)))]
      [(f arg ...) (non-keyword? #'f) #`(py-call #,(x->py #'f)
                                                 #,@(x-map->py #'(arg ...)))]

      ;; Evaluating Guile expressions, returning a pyobject. The
      ;; behavior is unspecified if the last expression does not
      ;; return a pyobject.
      [(#:scm e e* ...)         #'(begin e e* ...)]
      [(#:scm e ...)            (py-min-arg-count-violation "#:scm" 1 stx expr)]

      ;; Evaluating Guile expressions, returning an object that is
      ;; converted to a pyscm – that is, to a Guile value wrapped for
      ;; Python. As a special case, a Guile procedure is converted to
      ;; a Python callable.
      [(#:pyscm e e* ...)       #'(scm->pyscm (begin e e* ...))]
      [(#:pyscm e ...)          (py-min-arg-count-violation "#:pyscm" 1 stx expr)]

      ;; Like #:pyscm, except accepts only one expression to specify
      ;; the object to wrap, and may set the __name__ and __doc__
      ;; attributes of the Python object. FIXME: Write less confusing
      ;; documentation.
      [(#:pyscm* e)             #'(scm->pyscm* e)]
      [(#:pyscm* e name)        #'(scm->pyscm* e #:name name)]
      [(#:pyscm* e name doc)    #'(scm->pyscm* e #:name name #:doc doc)]
      [(#:pyscm* e ...)         (py-min-arg-count-violation "#:pyscm*" 1 stx expr)]

      ;; Returning the type of a Python value.
      [(#:type e)               #`(py-type #,(x->py #'e))]
      [(#:type e ...)           (py-exact-arg-count-violation "#:type" 1 stx expr)]

      ;; Constructing a Python ‘property’ object.
      [(#:property)             #`(py-property)]
      [(#:property get)         #`(py-property #,(x->py #'get))]
      [(#:property get set)     #`(py-property #,(x->py #'get) #,(x->py #'set))]
      [(#:property get set del) #`(py-property #,(x->py #'get) #,(x->py #'set)
                                               #,(x->py #'del))]
      [(#:property get set del doc) #`(py-property #,(x->py #'get) #,(x->py #'set)
                                                   #,(x->py #'del) #,(x->py #'doc))]
      [(#:property e ...)       (py-max-arg-count-violation "#:property" 4 stx expr)]

      ;; Constructing a Python classmethod.
      [(#:classmethod func)     #`(py-classmethod #,(x->py #'func))]
      [(#:classmethod e ...)    (py-exact-arg-count-violation "#:classmethod"
                                                              1 stx expr)]

      ;; Constructing a Python staticmethod.
      [(#:staticmethod func)    #`(py-staticmethod #,(x->py #'func))]
      [(#:staticmethod e ...)   (py-exact-arg-count-violation "#:staticmethod"
                                                              1 stx expr)]

      ;; Length of a Python object.
      [(#:len obj)              #`(py-len #,(x->py #'obj))]
      [(#:len e ...)            (py-exact-arg-count-violation "#:len" 1 stx expr)]

      ;; Python tuples from given items.
      [(#:tuple e ...)          #`(list->pytuple (list #,@(x-map->py #'(e ...))))]

      ;; Python lists from given items.
      [(#:list e ...)           #`(list->pylist (list #,@(x-map->py #'(e ...))))]

      ;; Python dictionaries from given key-value pairs.
      [(#:dict pair ...)        #`(alist->pydict
                                   (list #,@(pair-map->py "#:dict" #'(pair ...))))]

      ;; Python sets from given items.
      [(#:set e ...)            #`(list->pyset (list #,@(x-map->py #'(e ...))))]
      [(#:frozenset e ...)      #`(list->pyfrozenset (list #,@(x-map->py #'(e ...))))]

      [_ (py-syntax-violation stx expr)]))

  ) ;; end of library.
