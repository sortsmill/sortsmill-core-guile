;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pytypes)

  (export
   pytype?            ;; (pytype? obj) → boolean
   pytype-exact?      ;; (pytype-exact? obj) → boolean
   pytype-clearcache! ;; (pytype-clearcache!) → integer
   pytype-getflags    ;; (pytype-getflags pytype) → list-of-symbols
   pytype-modified!   ;; (pytype-modified! pytype) → *unspecified*
   pytype-hasfeature? ;; (pytype-hasfeature? pytype integer-or-list) → boolean
   pytype-issubtype?  ;; (pytype-issubtype? pytype1 pytype2) → boolean

   tpflags-symbols->integer ;; (tpflags-symbols->integer list) → integer
   integer->tpflags-symbols ;; (integer->tpflags-symbols integer) → list
   Py_TPFLAGS_HAVE_GETCHARBUFFER ;; integer
   Py_TPFLAGS_HAVE_SEQUENCE_IN   ;; integer
   Py_TPFLAGS_HAVE_INPLACEOPS    ;; etc.
   Py_TPFLAGS_CHECKTYPES
   Py_TPFLAGS_HAVE_RICHCOMPARE
   Py_TPFLAGS_HAVE_WEAKREFS
   Py_TPFLAGS_HAVE_ITER
   Py_TPFLAGS_HAVE_CLASS
   Py_TPFLAGS_HEAPTYPE
   Py_TPFLAGS_BASETYPE
   Py_TPFLAGS_READY
   Py_TPFLAGS_READYING
   Py_TPFLAGS_HAVE_GC
   Py_TPFLAGS_HAVE_STACKLESS_EXTENSION
   Py_TPFLAGS_HAVE_INDEX
   Py_TPFLAGS_HAVE_VERSION_TAG
   Py_TPFLAGS_VALID_VERSION_TAG
   Py_TPFLAGS_IS_ABSTRACT
   Py_TPFLAGS_HAVE_NEWBUFFER
   Py_TPFLAGS_INT_SUBCLASS
   Py_TPFLAGS_LONG_SUBCLASS
   Py_TPFLAGS_LIST_SUBCLASS
   Py_TPFLAGS_TUPLE_SUBCLASS
   Py_TPFLAGS_BYTES_SUBCLASS
   Py_TPFLAGS_UNICODE_SUBCLASS
   Py_TPFLAGS_DICT_SUBCLASS
   Py_TPFLAGS_BASE_EXC_SUBCLASS
   Py_TPFLAGS_TYPE_SUBCLASS
   Py_TPFLAGS_DEFAULT
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          init_guile_pytype
                          scm_pytype_p
                          scm_pytype_exact_p
                          scm_pytype_clearcache_x
                          scm_pytype_getflags
                          scm_pytype_modified_x
                          scm_pytype_hasfeature_p
                          scm_pytype_issubtype_p
                          )
    ((pointer->procedure void init_guile_pytype '())))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  ;;----------------------------------------------------------------------

  (define-scm-procedure (pytype? obj)
    "Is the argument a @code{pytype}?"
    scm_pytype_p)

  (define-scm-procedure (pytype-exact? obj)
    "Is the argument a @code{pytype} but not a member of a subtype of
@code{pytype}?"
    scm_pytype_exact_p)

  (define-scm-procedure (pytype-clearcache!)
    "A wrapper around @code{PyType_ClearCache}."
    scm_pytype_clearcache_x)

  (define-scm-procedure (pytype-getflags pytype)
    "A wrapper around @code{PyType_GetFlags}, or the equivalent
accessing the @code{tp_flags} field directly. The returned value is a
list of symbols. FIXME: Write better documentation for this."
    scm_pytype_getflags)

  (define-scm-procedure (pytype-modified! pytype)
    "A wrapper around @code{PyType_Modified}."
    scm_pytype_modified_x)

  (define-scm-procedure (pytype-hasfeature? pytype features)
    "A wrapper around @code{PyType_HasFeature}. The @var{features}
argument may be either an integer or a list of symbols. WARNING: The
Python documentation implies that only one bit at a time should be
tested, though the implementation (as of this writing) acts like an
inclusive-or of multiple feature tests. FIXME: Write better
documentation for this."
    scm_pytype_hasfeature_p)

(define-scm-procedure (pytype-issubtype? pytype1 pytype2)
    "A wrapper around @code{PyType_IsSubtype}."
    scm_pytype_issubtype_p)

  ;;----------------------------------------------------------------------

  (define tpflags-pairs
    `((have-getcharbuffer . ,Py_TPFLAGS_HAVE_GETCHARBUFFER)
      (have-sequence-in . ,Py_TPFLAGS_HAVE_SEQUENCE_IN)
      (have-inplaceops . ,Py_TPFLAGS_HAVE_INPLACEOPS)
      (checktypes . ,Py_TPFLAGS_CHECKTYPES)
      (have-richcompare . ,Py_TPFLAGS_HAVE_RICHCOMPARE)
      (have-weakrefs . ,Py_TPFLAGS_HAVE_WEAKREFS)
      (have-iter . ,Py_TPFLAGS_HAVE_ITER)
      (have-class . ,Py_TPFLAGS_HAVE_CLASS)
      (heaptype . ,Py_TPFLAGS_HEAPTYPE)
      (basetype . ,Py_TPFLAGS_BASETYPE)
      (ready . ,Py_TPFLAGS_READY)
      (readying . ,Py_TPFLAGS_READYING)
      (have-gc . ,Py_TPFLAGS_HAVE_GC)
      (have-stackless-extension . ,Py_TPFLAGS_HAVE_STACKLESS_EXTENSION)
      (have-index . ,Py_TPFLAGS_HAVE_INDEX)
      (have-version-tag . ,Py_TPFLAGS_HAVE_VERSION_TAG)
      (valid-version-tag . ,Py_TPFLAGS_VALID_VERSION_TAG)
      (is-abstract . ,Py_TPFLAGS_IS_ABSTRACT)
      (have-newbuffer . ,Py_TPFLAGS_HAVE_NEWBUFFER)
      (int-subclass . ,Py_TPFLAGS_INT_SUBCLASS)
      (long-subclass . ,Py_TPFLAGS_LONG_SUBCLASS)
      (list-subclass . ,Py_TPFLAGS_LIST_SUBCLASS)
      (tuple-subclass . ,Py_TPFLAGS_TUPLE_SUBCLASS)
      (bytes-subclass . ,Py_TPFLAGS_BYTES_SUBCLASS)
      (unicode-subclass . ,Py_TPFLAGS_UNICODE_SUBCLASS)
      (dict-subclass . ,Py_TPFLAGS_DICT_SUBCLASS)
      (base-exc-subclass . ,Py_TPFLAGS_BASE_EXC_SUBCLASS)
      (type-subclass . ,Py_TPFLAGS_TYPE_SUBCLASS)
      (default . ,Py_TPFLAGS_DEFAULT)))

  (define tpflags-symbols (map car tpflags-pairs))

  (define (tpflags-symbols->integer symbols)
    (fold-left (lambda (prior symb)
                 (let ([n (assq-ref tpflags-pairs symb)])
                   (unless n
                     (assertion-violation
                      'tpflags-symbols->integer
                      (format #f (_ "expected symbols from ~a") tpflags-symbols)
                      symb))
                   (+ prior n)))
               0 symbols))

  (define (integer->tpflags-symbols n)
    (assert (integer? n))
    (reverse
     (fold-left (lambda (prior pair)
                  (let ([symb (car pair)]
                        [value (cdr pair)])
                    (if (and (not (zero? value))
                             (= (bitwise-ior value n) n))
                        (cons symb prior)
                        prior)))
                '() tpflags-pairs)))

  ;;----------------------------------------------------------------------

  ) ;; end of library.
