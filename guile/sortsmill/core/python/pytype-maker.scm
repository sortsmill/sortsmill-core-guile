;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pytype-maker)

  (export make-pytype)

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (sortsmill smcoreguile-pkginfo)
          (sortsmill core i18n)
          (sortsmill core python pyobjects)
          (sortsmill core python pysequences)
          (sortsmill core python pymappings)
          (sortsmill core python pymethods)
          (sortsmill core python pyscms)
          (sortsmill core python pytypes)
          (sortsmill core python friendlier)
          (sortsmill core python shorthand))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (define (prepare-string who keyword s)
    (cond [(string? s) s]
          [(symbol? s) (symbol->string s)]
          [(pyunicode? s) (pyunicode->string s)]
          [(pybytes? s) (utf8->string (pybytes->bytevector s))]
          [else (assertion-violation
                 who (_ "expected a string, symbol, pyunicode, or pybytes")
                 keyword s)]))

  (define (prepare-method who keyword proc)
    (cond [(procedure? proc) (scm->pyscm* proc)]
          [(pycallable? proc) proc]
          [else (assertion-violation
                 who (_ "expected a procedure or pycallable") keyword proc)]))

  (define (prepare-method* who keyword proc)
    (cond [(pynone? proc) (py-none)]
          [else (prepare-method who keyword proc)]))

  (define (parse-pytype-arguments who arguments table)
    (match arguments
      [() *unspecified*]

      [(#:name name . more-args)
       (hashtable-set! table #:name (prepare-string who #:name name))
       (parse-pytype-arguments who more-args table)]

      [(#:base base-type . more-args)
       (unless (pytype? base-type)
         (assertion-violation who (_ "expected a base class") base-type))
       (hashtable-update! table #:base
                          (lambda (bases) (append bases (list base-type)))
                          '())
       (parse-pytype-arguments who more-args table)]

      [(#:dict dict . more-args)
       (unless (pydict? dict)
         (assertion-violation who (_ "expected a pydict") dict))
       (hashtable-set! table #:dict dict)
       (parse-pytype-arguments who more-args table)]

      [(#:attribute attr-name attr-value . more-args)
       (let ([name (prepare-string who #:method attr-name)]
             [value (if (pyobject? attr-value)
                        attr-value
                        (scm->pyscm attr-value))])
         (hashtable-update! table #:attribute
                            (lambda (attrs)
                              (append attrs (list (list #:attribute name value))))
                            '()))]

      [(#:method method-name method-proc . more-args)
       (let ([name (prepare-string who #:method method-name)]
             [func (prepare-method who #:method method-proc)])
         (hashtable-update! table #:attribute
                            (lambda (methods)
                              (append methods (list (list #:method name func))))
                            '()))
       (parse-pytype-arguments who more-args table)]

      [(#:classmethod method-name method-proc . more-args)
       (let ([name (prepare-string who #:classmethod method-name)]
             [func (prepare-method who #:classmethod method-proc)])
         (hashtable-update! table #:attribute
                            (lambda (methods)
                              (append methods (list (list #:classmethod name func))))
                            '()))
       (parse-pytype-arguments who more-args table)]

      [(#:staticmethod method-name method-proc . more-args)
       (let ([name (prepare-string who #:staticmethod method-name)]
             [func (prepare-method who #:staticmethod method-proc)])
         (hashtable-update! table #:attribute
                            (lambda (methods)
                              (append methods (list (list #:staticmethod name func))))
                            '()))
       (parse-pytype-arguments who more-args table)]

      [(#:property name
                   (? (negate keyword?) get)
                   (? (negate keyword?) set)
                   (? (negate keyword?) del)
                   (? (negate keyword?) doc)
                   . more-args)
       (hashtable-update! table #:attribute
                          (lambda (methods)
                            (append methods
                                    (list (list #:property name
                                                (prepare-method* who #:property get)
                                                (prepare-method* who #:property set)
                                                (prepare-method* who #:property del)
                                                doc))))
                          '())
       (parse-pytype-arguments who more-args table)]
      [(#:property name
                   (? (negate keyword?) get)
                   (? (negate keyword?) set)
                   (? (negate keyword?) del)
                   . more-args)
       (parse-pytype-arguments who
                               (cons* #:property name
                                      get set del (py None) more-args)
                               table)]
      [(#:property name
                   (? (negate keyword?) get)
                   (? (negate keyword?) set)
                   . more-args)
       (parse-pytype-arguments who
                               (cons* #:property name
                                      get set (py None) (py None)
                                      more-args)
                               table)]
      [(#:property name
                   (? (negate keyword?) get)
                   . more-args)
       (parse-pytype-arguments who
                               (cons* #:property name
                                      get (py None) (py None) (py None)
                                      more-args)
                               table)]
      [(#:property name
                   . more-args)
       (parse-pytype-arguments who
                               (cons* #:property name
                                      (py None) (py None) (py None) (py None)
                                      more-args)
                               table)]

      [_ (assertion-violation who (_ "unexpected arguments") arguments)]))

  (define (make-pytype . arguments)
    "Make a new Python type. FIXME: Document this."
    (let ([table (make-eq-hashtable)])
      (parse-pytype-arguments 'make-pytype arguments table)
      (let ([new-type
             (py (type (#:scm (python-str (hashtable-ref table #:name
                                                         "unnamed_type")))
                       (#:scm (list->pytuple (hashtable-ref
                                              table #:base
                                              (list (pybaseobject-type)))))
                       (#:scm (hashtable-ref table #:dict (py (#:dict))))))])
        (for-each (match-lambda
                   [(#:attribute name value)
                    (pyobject-setattr! new-type (python-str name) value)
                    (pytype-modified! new-type)]
                   [(#:method name func)
                    (pyobject-setattr! new-type (python-str name)
                                       (pyinstancemethod-new func new-type))
                    (pytype-modified! new-type)]
                   [(#:classmethod name func)
                    (pyobject-setattr! new-type (python-str name)
                                       (py-classmethod func))
                    (pytype-modified! new-type)]
                   [(#:staticmethod name func)
                    (pyobject-setattr! new-type (python-str name)
                                       (py-staticmethod func))
                    (pytype-modified! new-type)]
                   [(#:property name get set del doc)
                    (pyobject-setattr! new-type (python-str name)
                                       (py-property get set del doc))
                    (pytype-modified! new-type)])
                  (hashtable-ref table #:attribute '()))
        new-type)))

  ) ;; end of library.
