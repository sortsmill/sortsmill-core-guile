;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python initialization)

  (export py-initialize
          py-initialized?
          py-finalize
          raise-py-uninitialized
          assert-py-initialized)

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core python version))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_py_initialize
                          scm_py_initialized_p
                          scm_py_finalize
                          scm_raise_py_uninitialized
                          scm_assert_py_initialized))

  (define*-scm-procedure (py-initialize #:optional initsigs)
    "If the embedded Python interpreter is not already initialized,
call @code{Py_Initialize} or @code{Py_InitializeEx}."
    scm_py_initialize)

  (define-scm-procedure (py-initialized?)
    "Call @code{Py_IsInitialized} and return the result as a Guile
boolean."
    scm_py_initialized_p)

  (define-scm-procedure (py-finalize)
    "If the embedded Python interpreter is running, run the Guile
garbage collecter, then call @code{Py_Finalize}."
    scm_py_finalize)

  (define-scm-procedure (raise-py-uninitialized)
    "Raise an exception to indicate that Python is not initialized."
    scm_raise_py_uninitialized)

  (define-scm-procedure (assert-py-initialized)
    "If Python is not initialized, raise an exception to indicate that
problem."
    scm_assert_py_initialized)

  ) ;; end of library.
