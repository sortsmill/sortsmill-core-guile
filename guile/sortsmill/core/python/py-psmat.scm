;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python py-psmat)

  ;;
  ;; PostScript matrices as Python six-tuples.
  ;;

  (export
   py-psmat-identity    ;; (py-psmat-identity) → pytuple
   py-psmat-compose     ;; (py-psmat-compose pytuple1 ...) → pytuple
   py-psmat-inverse     ;; (py-psmat-inverse pytuple) → pytuple
   py-psmat-rotate      ;; (py-psmat-rotate θ) → pytuple
   py-psmat-scale       ;; (py-psmat-scale x [y = x]) → pytuple
   py-psmat-skew        ;; (py-psmat-skew θ) → pytuple
   py-psmat-translate   ;; (py-psmat-translate x y) → pytuple
   make-py-psmat-module ;; (make-py-psmat-module name) → pymodule
   )

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (sortsmill smcoreguile-pkginfo)
          (sortsmill core i18n)
          (sortsmill core python pyobjects)
          (sortsmill core python pysequences)
          (sortsmill core python pyscms)
          (sortsmill core python pymodules)
          (sortsmill core python shorthand))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (define-inlinable (n->inexact n)
    (exact->inexact (pyfloat->real n)))

  (define (n->exact n)
    (inexact->exact (pyfloat->real n)))

  (define (py-psmat-identity)
    "Return a PostScript identity matrix as a six-element tuple of
pyfloats."
    (py (#:tuple 1.0 0.0 0.0 1.0 0.0 0.0)))

  (define* (py-psmat-compose mat1 #:optional [mat2 (py None)] #:rest rest)
    "Return a PostScript matrix that is the composition of one or more
input transformations. The arguments and return values all are
six-element pytuple of pyfloats."
    (cond [(pynone? mat2)
           (assert (null? rest))
           mat1]
          [else (fold-left compose-two-matrices
                           (compose-two-matrices mat1 mat2)
                           rest)]))

  (define (compose-two-matrices mat1 mat2)
    (match (pysequence-map->vector n->inexact mat1)
      [#(m10 m11 m12 m13 m14 m15)
       (match (pysequence-map->vector n->inexact mat2)
         [#(m20 m21 m22 m23 m24 m25)
          (let ([a0 (+ (* m10 m20) (* m11 m22))]
                [a1 (+ (* m10 m21) (* m11 m23))]
                [a2 (+ (* m12 m20) (* m13 m22))]
                [a3 (+ (* m12 m21) (* m13 m23))]
                [a4 (+ (* m14 m20) (* m15 m22) m24)]
                [a5 (+ (* m14 m21) (* m15 m23) m25)])
            (py (#:tuple (#:scm (real->pyfloat a0))
                         (#:scm (real->pyfloat a1))
                         (#:scm (real->pyfloat a2))
                         (#:scm (real->pyfloat a3))
                         (#:scm (real->pyfloat a4))
                         (#:scm (real->pyfloat a5)))))]
         [_ (error 'py-psmat-compose
                   (_ "second argument: expected a Python six-tuple")
                   mat2)])]
      [_ (error 'py-psmat-compose
                (_ "first argument: expected a Python six-tuple")
                mat1)]))

  (define (py-psmat-inverse mat)
    "Invert a PostScript matrix. For simplicity, use Cramer's rule
in exact arithmetic."
    (match (pysequence-map->vector n->exact mat)
      [#(a0 a1 a2 a3 a4 a5)

       ;; Below, the letter ‘A’ represents the matrix:
       ;;
       ;; A = a0 a1
       ;;     a2 a3

       ;; Compute:
       ;;
       ;;    The determinant –
       ;;
       ;;        det A = (a0 * a3) - (a1 * a2)
       ;;
       ;;    The adjugate –
       ;;
       ;;        adj A =  a3 -a1 = g0 g1
       ;;                -a2  a0   g2 g3
       ;;
       (let ([determinant (- (* a0 a3) (* a1 a2))]
             [g0 a3]
             [g1 (- a1)]
             [g2 (- a2)]
             [g3 a0])
         
         ;; Compute:
         ;;
         ;;    The inverse –
         ;;
         ;;        inv A = adj A / det A = v0 v1 = A⁻¹
         ;;                                v2 v3
         ;;
         (let ([v0 (/ g0 determinant)]
               [v1 (/ g1 determinant)]
               [v2 (/ g2 determinant)]
               [v3 (/ g3 determinant)])

           ;; Compute the offset in the plane.
           (let ([d0 (- (+ (* a4 v0) (* a5 v2)))]
                 [d1 (- (+ (* a4 v1) (* a5 v3)))])

             ;; The result.
             (py (#:tuple (#:scm (real->pyfloat v0))
                          (#:scm (real->pyfloat v1))
                          (#:scm (real->pyfloat v2))
                          (#:scm (real->pyfloat v3))
                          (#:scm (real->pyfloat d0))
                          (#:scm (real->pyfloat d1)))))))]

      [_ (error 'py-psmat-inverse
                (_ "expected a Python six-tuple")
                mat)]))

  (define (py-psmat-rotate theta)
    "Return a matrix that will rotate by an angle expressed in
radians."
    (let ([t (n->inexact theta)])
      (let ([cosine (cos t)]
            [sine (sin t)])
        (let ([py-cosine (real->pyfloat cosine)]
              [py-sine (real->pyfloat sine)]
              [py-minus-sine (real->pyfloat (- sine))])
          (py (#:tuple (#:scm py-cosine)
                       (#:scm py-sine)
                       (#:scm py-minus-sine)
                       (#:scm py-cosine)
                       0.0 0.0))))))

  (define* (py-psmat-scale x #:optional [y x])
    "Return a matrix that will scale by x horizontally and y vertically.
If y is omitted, the matrix will scale by x in both directions."
    (let ([x (n->inexact x)]
          [y (n->inexact y)])
      (py (#:tuple (#:scm (real->pyfloat x))
                   0.0 0.0
                   (#:scm (real->pyfloat y))
                   0.0 0.0))))

  (define (py-psmat-skew theta)
    "Return a matrix that will skew by an angle (to produce an oblique
font). The angle is expressed in radians."
    (let ([t (n->inexact theta)])
      (let ([tangent (tan t)])
        (py (#:tuple 1.0 0.0 (#:scm (real->pyfloat tangent)) 1.0 0.0 0.0)))))

  (define (py-psmat-translate x y)
    "Return a matrix that will translate by x horizontally and y
vertically."
    (let ([x (n->inexact x)]
          [y (n->inexact y)])
      (py (#:tuple 1.0 0.0 0.0 1.0
                   (#:scm (real->pyfloat x))
                   (#:scm (real->pyfloat y))))))

  (define (make-py-psmat-module module-name)
    "Create a clone of the original FontForge's psMat Python
extension."
    (let ([m (pymodule-new module-name)])
      (pymodule-addobject! m "identity" (scm->pyscm py-psmat-identity))
      (pymodule-addobject! m "compose" (scm->pyscm py-psmat-compose))
      (pymodule-addobject! m "inverse" (scm->pyscm py-psmat-inverse))
      (pymodule-addobject! m "rotate" (scm->pyscm py-psmat-rotate))
      (pymodule-addobject! m "scale" (scm->pyscm py-psmat-scale))
      (pymodule-addobject! m "skew" (scm->pyscm py-psmat-skew))
      (pymodule-addobject! m "translate" (scm->pyscm py-psmat-translate))
      m))

  ) ;; end of library.
