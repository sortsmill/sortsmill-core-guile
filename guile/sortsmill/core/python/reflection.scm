;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python reflection)

  (export
   pyeval-getbuiltins
   pyeval-getlocals
   pyeval-getglobals
   pyeval-getframe
   pyframe-getlinenumber
   pyframe-getfilename
   pyeval-getfuncname
   pyeval-getfuncdesc
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          )

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          scm_pyeval_getbuiltins
                          scm_pyeval_getlocals
                          scm_pyeval_getglobals
                          scm_pyeval_getframe
                          scm_pyframe_getlinenumber
                          scm_pyframe_getfilename
                          scm_pyeval_getfuncname
                          scm_pyeval_getfuncdesc
;;;;;;;;;;;;                          scm_pytraceback_here
                          ))

  (define-scm-procedure (pyeval-getbuiltins)
    "Call @code{PyEval_GetBuiltins}, returning the result as a
Guile-wrapped Python object."
    scm_pyeval_getbuiltins)

  (define-scm-procedure (pyeval-getlocals)
    "Call @code{PyEval_GetLocals}, returning the result as a
Guile-wrapped Python object, or @code{#f} if the return value was
@code{NULL}."
    scm_pyeval_getlocals)

  (define-scm-procedure (pyeval-getglobals)
    "Call @code{PyEval_GetGlobals}, returning the result as a
Guile-wrapped Python object, or @code{#f} if the return value was
@code{NULL}."
    scm_pyeval_getglobals)

  (define-scm-procedure (pyeval-getframe)
    "Call @code{PyEval_GetFrame}, returning the result as a
Guile-wrapped Python object, or @code{#f} if the return value was
@code{NULL}."
    scm_pyeval_getframe)

  (define-scm-procedure (pyframe-getlinenumber frame)
    "Call @code{PyFrame_GetLineNumber}, taking a Guile-wrapped Python
object as argument, and returning the result as a Guile integer."
    scm_pyframe_getlinenumber)

  (define-scm-procedure (pyframe-getfilename frame)
    "Get file name, given a Python execution frame. The data is
extracted from the frame structures directly rather than through a
CPython API function, so beware."
    scm_pyframe_getfilename)

  (define-scm-procedure (pyeval-getfuncname func)
    "Call @code{PyFrame_GetFuncName}, taking a Guile-wrapped Python
object as argument, and returning the result as a Guile string."
    scm_pyeval_getfuncname)

  (define-scm-procedure (pyeval-getfuncdesc func)
    "Call @code{PyFrame_GetFuncDesc}, taking a Guile-wrapped Python
object as argument, and returning the result as a Guile string."
    scm_pyeval_getfuncdesc)

;;;;;;;;  (define-scm-procedure (pytraceback-here frame)
;;;;;;;;    "Call @code{PyTraceBack_Here}, given a Guile-wrapped Python
;;;;;;;;object, which must be a pyframe. The traceback object is placed in the
;;;;;;;;Python thread-state's @code{curexc_traceback} field, rather than
;;;;;;;;returned."
;;;;;;;;    scm_pytraceback_here)

  ) ;; end of library.
