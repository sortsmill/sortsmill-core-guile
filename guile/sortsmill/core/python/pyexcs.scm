;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core python pyexcs)

  (export
   pyerr-print!    ;; (pyerr-print! [set-sys-last-vars?]) → *unspecified*
   pyerr-occurred? ;; (pyerr-occurred?) → boolean
   pyerr-exceptionmatches? ;; (pyerr-exceptionmatches? exc) → boolean
   pyerr-givenexceptionmatches? ;; (pyerr-givenexceptionmatches? given exc) → boolean
   pyerr-normalizeexception! ;; (pyerr-normalizeexception! type value traceback) → type, value, traceback
   pyerr-clear!    ;; (pyerr-clear!) → *unspecified*
   pyerr-fetch!    ;; (pyerr-fetch!) → exc-type, exc-value, exc-traceback
   pyerr-restore!  ;; (pyerr-restore! exc-type exc-value exc-traceback) → *unspecified*
   pyerr-set!      ;; (pyerr-set! exc pyobj-or-string) → *unspecified*

   ;; (pyexc-<SOMETHING>) → pyobj
   pyexc-baseexception
   pyexc-exception
   pyexc-standarderror
   pyexc-arithmeticerror
   pyexc-lookuperror
   pyexc-assertionerror
   pyexc-attributeerror
   pyexc-eoferror
   pyexc-environmenterror
   pyexc-floatingpointerror
   pyexc-ioerror
   pyexc-importerror
   pyexc-indexerror
   pyexc-keyerror
   pyexc-keyboardinterrupt
   pyexc-memoryerror
   pyexc-nameerror
   pyexc-notimplementederror
   pyexc-oserror
   pyexc-overflowerror
   pyexc-referenceerror
   pyexc-runtimeerror
   pyexc-syntaxerror
   pyexc-systemerror
   pyexc-systemexit
   pyexc-typeerror
   pyexc-valueerror
   pyexc-windowserror
   pyexc-zerodivisionerror
   pyexc-blockingioerror
   pyexc-brokenpipeerror
   pyexc-childprocesserror
   pyexc-connectionerror
   pyexc-connectionabortederror
   pyexc-connectionrefusederror
   pyexc-connectionreseterror
   pyexc-fileexistserror
   pyexc-filenotfounderror
   pyexc-interruptederror
   pyexc-isadirectoryerror
   pyexc-notadirectoryerror
   pyexc-permissionerror
   pyexc-processlookuperror
   pyexc-timeouterror

   &py-exc-info
   make-py-exc-info
   py-exc-info?
   py-exc-info-type
   py-exc-info-value
   py-exc-info-traceback
   py-exc-check-violation
   raise-pyexc ;; (raise-pyexc exc pyobj-or-string)
   py-exceptioncheck ;; (py-exceptioncheck assertion exception exception-value)
   py-typecheck      ;; (py-typecheck assertion exception-value)
   py-valuecheck     ;; (py-valuecheck assertion exception-value)
   py-indexcheck     ;; (py-indexcheck assertion exception-value)
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill core python pyobjects)
          (sortsmill core python pyscms)
          (sortsmill core python reflection)
          (sortsmill core python shorthand)
          (sortsmill smcoreguile-pkginfo)
          )

  (eval-when (compile load eval)
    (define-gettext-for-domain _ SMCOREGUILE_PACKAGE))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile-python)
                          init_guile_pyexc
                          scm_pyerr_print_x
                          scm_pyerr_occurred_p
                          scm_pyerr_exceptionmatches_p
                          scm_pyerr_givenexceptionmatches_p
                          scm_pyerr_normalizeexception_x
                          scm_pyerr_clear_x
                          scm_pyerr_fetch_x
                          scm_pyerr_restore_x
                          scm_pyerr_set_x
                          scm_py_exc_check_violation
                          )
    ((pointer->procedure void init_guile_pyexc '())))

  ;;----------------------------------------------------------------------

  (define*-scm-procedure (pyerr-print! #:optional set-sys-last-vars?)
    "A wrapper around @code{PyErr_PrintEx}, except it does
nothing (except waste some time) if the Python error indicator is not
set. The @var{set-sys-last-vars?} argument is optional."
    scm_pyerr_print_x)

  (define-scm-procedure (pyerr-occurred?)
    "A wrapper around @code{PyErr_Occurred}. Returns a Guile
boolean (not a reference to the exception indicator)."
    scm_pyerr_occurred_p)

  (define-scm-procedure (pyerr-exceptionmatches? exc)
    "A wrapper around @code{PyErr_ExceptionMatches}. Returns @code{#f}
if the Python error indicator is not set."
    scm_pyerr_exceptionmatches_p)

  (define-scm-procedure (pyerr-givenexceptionmatches? given exc)
    "A wrapper around @code{PyErr_GivenExceptionMatches}. Returns
@code{#f} if the Python error indicator is not set."
    scm_pyerr_givenexceptionmatches_p)

  (define-scm-procedure (pyerr-normalizeexception! type value traceback)
    "FIXME: Document this procedure."
    scm_pyerr_normalizeexception_x)

  (define-scm-procedure (pyerr-clear!)
    "A wrapper around @code{PyErr_Clear}."
    scm_pyerr_clear_x)

  (define-scm-procedure (pyerr-fetch!)
    "A wrapper around @code{PyErr_Fetch}. Returns three values:
@code{type}, @code{value}, and @code{traceback}, respectively. Any of
these may equal @code{#f}."
    scm_pyerr_fetch_x)

  (define-scm-procedure (pyerr-restore! type value traceback)
    "A wrapper around @code{PyErr_Restore}. Any or all of the
arguments may equal @code{#f}. The arguments are @emph{not} checked
very carefully for correctness, so be careful."
    scm_pyerr_restore_x)

  (define-scm-procedure (pyerr-set! exc obj)
    "A wrapper around @code{PyErr_SetObject}. The @var{obj} may be
either a Guile-wrapped Python object or a Guile string."
    scm_pyerr_set_x)

  ;;----------------------------------------------------------------------

  (define-syntax possibly-unavailable-pyexc
    (lambda (stx)
      (syntax-case stx ()
        [(_ exc) (identifier? #'exc)
         (let ([exc^ (syntax->datum #'exc)])
           (if (defined? exc^)
               #'*unspecified* ;; Already defined as a variable.
               #'(define-syntax exc
                   (make-variable-transformer
                    (lambda (stx)
                      (syntax-case stx ()
                        [_ #'(error 'exc (_ "Python exception not supported"))]))))))])))

  (possibly-unavailable-pyexc pyexc-baseexception)
  (possibly-unavailable-pyexc pyexc-exception)
  (possibly-unavailable-pyexc pyexc-standarderror)
  (possibly-unavailable-pyexc pyexc-arithmeticerror)
  (possibly-unavailable-pyexc pyexc-lookuperror)
  (possibly-unavailable-pyexc pyexc-assertionerror)
  (possibly-unavailable-pyexc pyexc-attributeerror)
  (possibly-unavailable-pyexc pyexc-eoferror)
  (possibly-unavailable-pyexc pyexc-environmenterror)
  (possibly-unavailable-pyexc pyexc-floatingpointerror)
  (possibly-unavailable-pyexc pyexc-ioerror)
  (possibly-unavailable-pyexc pyexc-importerror)
  (possibly-unavailable-pyexc pyexc-indexerror)
  (possibly-unavailable-pyexc pyexc-keyerror)
  (possibly-unavailable-pyexc pyexc-keyboardinterrupt)
  (possibly-unavailable-pyexc pyexc-memoryerror)
  (possibly-unavailable-pyexc pyexc-nameerror)
  (possibly-unavailable-pyexc pyexc-notimplementederror)
  (possibly-unavailable-pyexc pyexc-oserror)
  (possibly-unavailable-pyexc pyexc-overflowerror)
  (possibly-unavailable-pyexc pyexc-referenceerror)
  (possibly-unavailable-pyexc pyexc-runtimeerror)
  (possibly-unavailable-pyexc pyexc-syntaxerror)
  (possibly-unavailable-pyexc pyexc-systemerror)
  (possibly-unavailable-pyexc pyexc-systemexit)
  (possibly-unavailable-pyexc pyexc-typeerror)
  (possibly-unavailable-pyexc pyexc-valueerror)
  (possibly-unavailable-pyexc pyexc-windowserror)
  (possibly-unavailable-pyexc pyexc-zerodivisionerror)
  (possibly-unavailable-pyexc pyexc-blockingioerror)
  (possibly-unavailable-pyexc pyexc-brokenpipeerror)
  (possibly-unavailable-pyexc pyexc-childprocesserror)
  (possibly-unavailable-pyexc pyexc-connectionerror)
  (possibly-unavailable-pyexc pyexc-connectionabortederror)
  (possibly-unavailable-pyexc pyexc-connectionrefusederror)
  (possibly-unavailable-pyexc pyexc-connectionreseterror)
  (possibly-unavailable-pyexc pyexc-fileexistserror)
  (possibly-unavailable-pyexc pyexc-filenotfounderror)
  (possibly-unavailable-pyexc pyexc-interruptederror)
  (possibly-unavailable-pyexc pyexc-isadirectoryerror)
  (possibly-unavailable-pyexc pyexc-notadirectoryerror)
  (possibly-unavailable-pyexc pyexc-permissionerror)
  (possibly-unavailable-pyexc pyexc-processlookuperror)
  (possibly-unavailable-pyexc pyexc-timeouterror)

  ;;----------------------------------------------------------------------

  (define-condition-type &py-exc-info
    &condition 
    make-py-exc-info
    py-exc-info?
    (type py-exc-info-type)
    (value py-exc-info-value)
    (traceback py-exc-info-traceback))

  (define-scm-procedure (py-exc-check-violation who message . irritants)
    "Raise an R6RS-style exception containing a @symbol{&py-exc-info}
condition holding information from the Python exception
indicator. Clears the indicator."
    scm_py_exc_check_violation)

  (define (raise-pyexc exc obj)
    "Raise an R6RS-style Guile exception that contains Python
exception data in the form of a @code{&py-exc-info} condition.

In the intended contexts, calling @code{raise-pyexc} results in the
given Python exception being raised within Python."
    (let ([value (cond [(pyobject? obj) obj]
                       [(string? obj) (string->pyunicode obj)]
                       [else (scm->pyscm obj)])])
      (let* ([frame (pyeval-getframe)]
             [stack (if frame
                        (py (traceback.extract_stack (#:scm (pyeval-getframe))))
                        #f)])
        (raise (condition (make-error)
                          (make-who-condition 'raise-pyexc)
                          (make-message-condition "Python exception")
                          (make-irritants-condition exc obj)
                          (make-py-exc-info exc value stack))))))

  (define-syntax-rule (py-exceptioncheck assertion exception exception-value)
    (unless assertion
      (let ([value (cond [(pyobject? exception-value) exception-value]
                         [(string? exception-value)
                          (string->pyunicode exception-value)]
                         [else (scm->pyscm exception-value)])])
        (raise-pyexc exception value))))

  (define-syntax-rule (py-typecheck assertion exception-value)
    (py-exceptioncheck assertion (pyexc-typeerror) exception-value))

  (define-syntax-rule (py-valuecheck assertion exception-value)
    (py-exceptioncheck assertion (pyexc-valueerror) exception-value))

  (define-syntax-rule (py-indexcheck assertion exception-value)
    (py-exceptioncheck assertion (pyexc-indexerror) exception-value))

  (define (with-py-exc-handler thunk)
    ;; Currently this is not exported, but is called from C.
    (catch #t
      (lambda ()
        (guard (exc [(py-exc-info? exc)
                     (pyerr-restore! (py-exc-info-type exc)
                                     (py-exc-info-value exc)
                                     (py-exc-info-traceback exc))
                     %null-pointer])
          (thunk)))
      (lambda (key . args)
        (unless (pyerr-occurred?)
          (pyerr-set! (py sortsmill_core_guile.GuileException)
                      (string->pyunicode
                       (call-with-output-string
                        (lambda (port) (print-exception port #f key args))))))
        %null-pointer)))

  (define (with-py-exc-handler-for-c func-ptr data-ptr)
    ;; Currently this is not exported, but is called from C.
    (let ([proc (pointer->procedure '* func-ptr '(*))])
      (with-py-exc-handler (lambda () (proc data-ptr)))))

  ;;----------------------------------------------------------------------

  ) ;; end of library.
