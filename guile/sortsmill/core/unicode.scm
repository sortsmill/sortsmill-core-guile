;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core unicode)

  (export
   UNINAME_MAX             ; UNINAME_MAX → integer
   unicode-code-point-name ; (unicode-code-point-name integer) → string
   unicode-character-name  ; (unicode-character-name char) → string
   unicode-name-code-point ; (unicode-name-code-point string) → integer
   unicode-name-character  ; (unicode-name-character string) → char
   )

  (import (sortsmill core helpers)
          (sortsmill core xgc)
          (rnrs)
          (except (guile) error)
          (system foreign))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libunistring")
                          unicode_character_name
                          unicode_name_character)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          init_sortsmill_core_guile_unicode)
    ((pointer->procedure void init_sortsmill_core_guile_unicode '())))

  (define unicode-code-point-name
    (let ([get-name (pointer->procedure '* unicode_character_name
                                        `(,uint32 *))])
      (lambda (code-point)
        "Return the Unicode character name corresponding to the integer
@var{code-point}. If there is no such name, then return @code{#f}
instead."
        (let* ([buf (x-gc-malloc-atomic UNINAME_MAX)]
               [name (get-name code-point buf)])
          (if (null-pointer? name)
              #f
              (pointer->string buf -1 "ISO-8859-1"))))))

  (define (unicode-character-name c)
    "Return the Unicode character name of the character @var{c}. If
there is no such name, then return @code{#f} instead."
    (unicode-code-point-name (char->integer c)))

  (define unicode-name-code-point
    (let ([get-code-point (pointer->procedure uint32
                                              unicode_name_character
                                              '(*))])
      (lambda (name)
        "Return the integer code point corresponding to the Unicode
name string @var{name}, which is examined case-insensitively. If there
is no such character, return @char{#f} instead."
        (let ([code-point
               (get-code-point (bytevector->pointer (string->utf8 name)))])
          (if (= code-point UNINAME_INVALID)
              #f
              code-point)))))

  (define (unicode-name-character name)
    "Return the character corresponding to the Unicode name string
@var{name}, which is examined case-insensitively. If there is no such
character, return @char{#f} instead."
    (let ([code-point (unicode-name-code-point name)])
      (if code-point
          (integer->char code-point)
          #f)))

  ) ;; end of library.
