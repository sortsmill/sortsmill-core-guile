;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core misc-math-functions)

  (export
   bincoef ;; (bincoef n k) → binomial coefficient C(n,k)
   popcount ;; (popcount i) → population count (binary hamming weight)
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_bincoef
                          scm_popcount))

  ;;--------------------------------------------------------------------

  (define-scm-procedure (bincoef n k)
    "Return the binomial coefficient C(n,k) of the two arguments."
    scm_bincoef)

  (define-scm-procedure (popcount i)
    "Return the population count (binary hamming weight) of a
non-negative exact integer."
    scm_popcount)

  ;;--------------------------------------------------------------------

  ) ;; end of library.
