;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core delayed-evaluation)

  ;; Delayed evaluation, using libatomic_ops to reduce the use of
  ;; mutexes.

  (export
   ;; Syntax for delayed objects.
   ;;
   ;; An applicable delayed object can be forced in the usual way (by
   ;; calling @code{forced}), or by calling it as a thunk.
   ;;
   delayed ;; (delayed expression) → delayed-object
   applicable-delayed ;; (applicable-delayed expression) → applicable-delayed-object

   ;; Procedural versions of ‘delayed’ and ‘applicable-delayed’.
   ;;
   ;; An applicable delayed object can be forced in the usual way (by
   ;; calling @code{forced}), or by calling it as a thunk.
   ;;
   delayed-value ;; (delayed-value thunk) → delayed-object
   applicable-delayed-value ;; (applicable-delayed-value thunk) → applicable-delayed-object

   ;; Creation of delayed objects that are in the ‘forced’ state from
   ;; the beginning.
   ;;
   ;; (undelayed value) → delayed-object
   ;; (applicable-undelayed value) → applicable-delayed-object
   ;;
   ;; An applicable delayed object can be forced in the usual way (by
   ;; calling @code{forced}), or by calling it as a thunk.
   ;;
   undelayed
   applicable-undelayed

   ;; Make a applicable/nonapplicable delayed object if the argument
   ;; is not already a delayed object of the same applicability;
   ;; otherwise just return the argument.
   ;;
   ;; (make-delayed value) → delayed-object
   ;; (make-applicable-delayed value) → applicable-delayed-object
   ;;
   ;; An applicable delayed object can be forced in the usual way (by
   ;; calling @code{forced}), or by calling it as a thunk.
   ;;
   make-delayed
   make-applicable-delayed

   ;; Force evaluation of a delayed object.
   forced ;; (forced delayed-object) → value

   ;; Is the argument a delayed object?
   delayed? ;; (delayed? obj) → boolean

   ;; Is the argument an applicable delayed object?
   applicable-delayed? ;; (applicable-delayed? obj)

   ;; Is the argument an delayed object but not an applicable delayed
   ;; object?
   nonapplicable-delayed? ;; (nonapplicable-delayed? obj)

   ;; Is the argument already in the ‘forced’ state? (The behavior is
   ;; unspecified if the argument is not a delayed object.)
   forced? ;; (forced? delayed-object) → boolean
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_delayed_value
                          scm_applicable_delayed_value
                          scm_undelayed
                          scm_applicable_undelayed
                          scm_make_delayed
                          scm_make_applicable_delayed
                          scm_delayed_p
                          scm_applicable_delayed_p
                          scm_nonapplicable_delayed_p
                          scm_forced
                          scm_forced_p))

  (define <delayed-vtable>
    (make-struct <applicable-struct-vtable> 0 'pwpwpwpw))

  (define (null-proc . args)
    (error #f (_ "attempt to apply a non-applicable delayed object")))

  (define-scm-procedure (delayed-value thunk)
    "Create a nonapplicable delayed value initialized by a given
thunk."
    scm_delayed_value)

  (define-scm-procedure (applicable-delayed-value thunk)
    "Create an applicable delayed value initialized by a given thunk."
    scm_applicable_delayed_value)

  (define-scm-procedure (undelayed value)
    "Create an `already forced' delayed value. The argument is the
desired value, rather than a thunk."
    scm_undelayed)

  (define-scm-procedure (applicable-undelayed value)
    "Create an `already forced' applicable delayed value. The argument
is the desired value, rather than a thunk."
    scm_applicable_undelayed)

  (define-scm-procedure (make-delayed value)
    "If the argument is a delayed object, return it. Otherwise return
an `undelayed' object made from the argument."
    scm_make_delayed)

  (define-scm-procedure (make-applicable-delayed value)
    "If the argument is an applicable delayed object, return
it. Otherwise return an applicable `undelayed' object made from the
argument."
    scm_make_applicable_delayed)

  (define-scm-procedure (delayed? obj)
    "Is the argument a delayed (or `undelayed') object?"
    scm_delayed_p)

  (define-scm-procedure (applicable-delayed? obj)
    "Is the argument an applicable delayed (or applicable `undelayed')
object?"
    scm_applicable_delayed_p)

  (define-scm-procedure (nonapplicable-delayed? obj)
    "Is the argument a delayed (or `undelayed') object that @emph{is
not} applicable?"
    scm_nonapplicable_delayed_p)

  (define-syntax-rule (delayed expression)
    (delayed-value (lambda () expression)))

  (define-syntax-rule (applicable-delayed expression)
    (applicable-delayed-value (lambda () expression)))

  (define-scm-procedure (forced obj)
    "Force evaluation of a delayed (or `undelayed') object."
    scm_forced)

  (define-scm-procedure (forced? obj)
    "Return @code{#t} if the delayed object been forced at least once,
or if it is `undelayed'; otherwise return @code{#f}. The behavior of
@code{forced?} is unspecified if the argument is not a delayed (or
`undelayed') object."
    scm_forced_p)

  ) ;; end of library.
