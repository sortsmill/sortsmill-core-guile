;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.
;;

(library (sortsmill core extensible-procedures)

;;;
;;; FIXME: Document this module.
;;;
;;; FIXME: Improve the handling of names of things in error messages.
;;;

  (export
   extensible-procedure-part?  ;; (extensible-procedure-part? obj) → boolean
   pp/guarded                  ;; Make a procedure part with pattern and guard.
   pp/pattern                  ;; Make a procedure part with pattern.

   extensible-procedure?       ;; (extensible-procedure? obj) → boolean
   make-extensible-procedure   ;; Make one without binding a variable.
   modify-extensible-procedure ;; Make a new one with changes.
   merge-extensible-procedures ;; Make a new one by merging others.

   define-extensible-procedure ;; Define a variable, containing an empty extensible procedure.
   define/fallback             ;; Set or change fallback procedures.
   define/pp                   ;; Add a procedure part.
   define/guarded              ;; Add a part with pattern and guard.
   define/pattern              ;; Add a part with pattern.

   procedure-parts
   setter-parts
   fallback-procedure
   fallback-setter
   )

  (import (rnrs)
          (only (srfi :1) delete-duplicates filter-map)
          (srfi :26)
          (srfi :42)
          (except (guile) error)
          (ice-9 match)
          (sortsmill core kwargs)
          (sortsmill smcoreguile-pkginfo))

  (eval-when (compile load eval)

    (define (_ string) (gettext string SMCOREGUILE_PACKAGE))

    ;;----------------------------------------------------------------------

    (define exten-proc-name (make-object-property))
    (define proc-parts (make-object-property))
    (define setr-parts (make-object-property))
    (define fb-proc (make-object-property))
    (define fb-setr (make-object-property))
    (define exten-part (make-object-property))

    (define (procedure-parts obj)
      (proc-parts obj))

    (define (setter-parts obj)
      (setr-parts obj))

    (define (fallback-procedure obj)
      (fb-proc obj))

    (define (fallback-setter obj)
      (fb-setr obj))

    (define (extensible-procedure? obj)
      (and (procedure-with-setter? obj)
           (list? (proc-parts obj))
           (list? (setr-parts obj))
           (let ([fb-proc^ (fb-proc obj)])
             (or (not fb-proc^) (procedure? fb-proc^)))
           (let ([fb-setter^ (fb-setr obj)])
             (or (not fb-setter^) (procedure? fb-setter^)))
           #t))

    (define (assert-is-extensible-procedure obj who)
      (unless (extensible-procedure? obj)
        (assertion-violation who (_ "not an extensible procedure") obj)))

    (define (extensible-procedure-part? obj)
      (and (procedure? obj) (exten-part obj) #t))

    (define (assert-is-extensible-procedure-part obj who)
      (unless (extensible-procedure-part? obj)
        (assertion-violation who (_ "not an extensible procedure part")
                             obj)))

    (define (assert-are-extensible-procedure-parts obj who)
      (for-each (cut assert-is-extensible-procedure-part <> who) obj))

    (define/kwargs (make-extensible-procedure [procedure-parts '()]
                                              [setter-parts '()]
                                              fallback-procedure
                                              fallback-setter
                                              name)
      (assert-are-extensible-procedure-parts procedure-parts
                                             'make-extensible-procedure)
      (assert-are-extensible-procedure-parts setter-parts
                                             'make-extensible-procedure)
      (assert (or (not fallback-procedure)
                  (procedure? fallback-procedure)))
      (assert (or (not fallback-setter)
                  (procedure? fallback-setter)))
      (let ([new-proc
             (make-procedure-with-setter
              (make-chained-procedure procedure-parts
                                      fallback-procedure name)
              (make-chained-procedure setter-parts
                                      fallback-setter name))])
        (set! (proc-parts new-proc) procedure-parts)
        (set! (setr-parts new-proc) setter-parts)
        (set! (fb-proc new-proc) fallback-procedure)
        (set! (fb-setr new-proc) fallback-procedure)
        (set! (exten-proc-name new-proc) name)
        new-proc))

    (define (make-chained-procedure parts fallback who)
      (match parts
        [()
         (or fallback
             (lambda arguments
               (arguments-not-matched who arguments)))]
        [(first-part . more-parts)
         (lambda arguments
           (first-part arguments more-parts fallback))]))

    (define-syntax pp/guarded
      (syntax-rules ()
        [(_ pattern (#:name name) guard body body* ...)
         (pp/guarded guard (#:name name) body body* ...)]
        [(_ pattern #t (#:name name) body body* ...)
         (let ([part
                (lambda (arguments more-parts fallback)
                  (match arguments
                    [pattern (begin body body* ...)]
                    [_ (do-part more-parts fallback arguments name)]))])
           (set! (exten-part part) #t)
           (set! (exten-proc-name part) name)
           part)]
        [(_ pattern guard (#:name name) body body* ...)
         (let ([part
                (lambda (arguments more-parts fallback)
                  (match arguments
                    [pattern
                     (if guard
                         (begin body body* ...)
                         (do-part more-parts fallback arguments name))]
                    [_ (do-part more-parts fallback arguments name)]))])
           (set! (exten-part part) #t)
           (set! (exten-proc-name part) name)
           part)]
        [(_ pattern guard body body* ...)
         (pp/guarded pattern guard (#:name #f) body body* ...)]))

    (define-syntax pp/pattern
      (syntax-rules ()
        [(_ pattern (#:name name) body body* ...)
         (pp/guarded pattern #t (#:name name) body body* ...)]
        [(_ pattern body body* ...)
         (pp/guarded pattern #t body body* ...)]))

    (define-inlinable (do-part parts fallback arguments who)
      (if (null? parts)
          (if fallback
              (apply fallback arguments)
              (arguments-not-matched who arguments))
          ((car parts) arguments (cdr parts) fallback)))

    (define (arguments-not-matched who arguments)
      (apply assertion-violation who
             (_ "arguments not matched") arguments))

    (define no-arg (make-symbol "<no-arg>"))

    (define (opt-arg arg dflt)
      (if (eq? arg no-arg) dflt arg))

    (define/kwargs (cons-procedure-part procedure procedure-part)
      (let ([parts (cons procedure-part (proc-parts procedure))])
        (modify-extensible-procedure #:procedure procedure
                                     #:procedure-parts parts)))

    (define/kwargs (append-procedure-part procedure procedure-part)
      (let ([parts (append (proc-parts procedure) (list procedure-part))])
        (modify-extensible-procedure #:procedure procedure
                                     #:procedure-parts parts)))

    (define/kwargs (cons-setter-part procedure setter-part)
      (let ([parts (cons setter-part (setr-parts procedure))])
        (modify-extensible-procedure #:procedure procedure
                                     #:setter-parts parts)))

    (define/kwargs (append-setter-part procedure setter-part)
      (let ([parts (append (setr-parts procedure) (list setter-part))])
        (modify-extensible-procedure #:procedure procedure
                                     #:setter-parts parts)))

    (define/kwargs (modify-extensible-procedure procedure
                                                [procedure-parts no-arg]
                                                [setter-parts no-arg]
                                                [fallback-procedure no-arg]
                                                [fallback-setter no-arg]
                                                [name no-arg])
      (assert-is-extensible-procedure procedure
                                      'modify-extensible-procedure)
      (let ([procedure-parts
             (opt-arg procedure-parts (proc-parts procedure))]
            [setter-parts
             (opt-arg setter-parts (setr-parts procedure))]
            [fallback-procedure
             (opt-arg fallback-procedure (fb-proc procedure))]
            [fallback-setter
             (opt-arg fallback-setter (fb-setr procedure))]
            [my-name
             (opt-arg name (exten-proc-name procedure))])
        (make-extensible-procedure #:procedure-parts procedure-parts
                                   #:setter-parts setter-parts
                                   #:fallback-procedure fallback-procedure
                                   #:fallback-setter fallback-setter
                                   #:name my-name)))

    (define/kwargs (merge-extensible-procedures components name)
      (let ([proced-parts
             (delete-duplicates (apply append (map proc-parts components))
                                eq?)]
            [setter-parts
             (delete-duplicates (apply append (map setr-parts components))
                                eq?)]
            [fb-procs
             (delete-duplicates (filter-map fb-proc components) eq?)]
            [fb-setters
             (delete-duplicates (filter-map fb-setr components) eq?)])
        (when (< 1 (length fb-procs))
          (when (< 1 (length fb-setters))
            (assertion-violation
             'merge-extensible-procedures
             (_ "conflicting fallback procedures and setters")))
          (assertion-violation 'merge-extensible-procedures
                               (_ "conflicting fallback procedures")))
        (when (< 1 (length fb-setters))
          (assertion-violation 'merge-extensible-procedures
                               (_ "conflicting fallback setters")))
        (let ([fback-proc (if (null? fb-procs) #f (car fb-procs))]
              [fback-setter (if (null? fb-setters) #f (car fb-setters))])
          (make-extensible-procedure #:procedure-parts proced-parts
                                     #:setter-parts setter-parts
                                     #:fallback-procedure fback-proc
                                     #:fallback-setter fback-setter
                                     #:name name))))

    (define-syntax define-extensible-procedure
      (syntax-rules ()
        [(_ name)
         (eval-when (compile load eval)
           (module-define! (current-module) 'name
                           (make-extensible-procedure #:name 'name)))]))

    (define-syntax define/fallback
      (syntax-rules (setter)
        [(_ (setter name) fallback-setter)
         (eval-when (compile load eval)
           (let* ([old-proc (ensure-exten-proc (current-module) 'name)]
                  [new-proc (modify-extensible-procedure
                             #:procedure old-proc
                             #:fallback-setter fallback-setter)])
             (set-extensible-procedure! #:name 'name
                                        #:procedure new-proc
                                        #:caller-name 'define/fallback)))]
        [(_ name fallback-procedure)
         (eval-when (compile load eval)
           (let* ([old-proc (ensure-exten-proc (current-module) 'name)]
                  [new-proc (modify-extensible-procedure
                             #:procedure old-proc
                             #:fallback-procedure fallback-procedure)])
             (set-extensible-procedure! #:name 'name
                                        #:procedure new-proc
                                        #:caller-name 'define/fallback)))]))

    (define (set-extensible-procedure-clause! insert-part name part)
      (when (and name (not (exten-proc-name part)))
        (set! (exten-proc-name part) name))
      (let* ([old-proc (ensure-exten-proc (current-module) name)]
             [new-proc (insert-part old-proc part)])
        (set-extensible-procedure! #:name name
                                   #:procedure new-proc
                                   #:caller-name 'define/pp)))

    (define-syntax set-clause!
      (syntax-rules ()
        [(_ insert-part name part)
         (eval-when (compile load eval)
           (set-extensible-procedure-clause! insert-part 'name part))]))

    (define-syntax define/pp
      (syntax-rules (override setter)
        [(_ (override setter name) part)
         (set-clause! cons-setter-part name part)]
        [(_ (override name) part)
         (set-clause! cons-procedure-part name part)]
        [(_ (setter name) part)
         (set-clause! append-setter-part name part)]
        [(_ name part)
         (set-clause! append-procedure-part name part)]))

    (define-syntax define/guarded
      (syntax-rules ()
        [(_ (name-expr . pattern) guard body body* ...)
         (define/pp name-expr
           (pp/guarded pattern guard
                       (#:name (extract-name name-expr))
                       (begin body body* ...)))]))

    (define-syntax define/pattern
      (syntax-rules ()
        [(_ (name-expr . pattern) body body* ...)
         (define/pp name-expr
           (pp/pattern pattern
                       (#:name (extract-name name-expr))
                       (begin body body* ...)))]))

    (define-syntax extract-name
      (syntax-rules (override setter)
        [(_ (override setter name)) 'name]
        [(_ (override name)) 'name]
        [(_ (setter name)) 'name]
        [(_ name) 'name]))

    ;;----------------------------------------------------------------------

    (define/kwargs (set-extensible-procedure! name procedure caller-name)
      (assert-is-extensible-procedure procedure caller-name)
      (module-define! (current-module) name procedure))

    (define (append-element element lst)
      (append lst (list element)))

    (define (get-exten-proc m v)
      (let ([var (module-variable m v)])
        (if var
            (if (defined? v m)
                (let ([procedure (variable-ref var)])
                  (if (extensible-procedure? procedure) procedure #f))
                #f)
            #f)))

    (define (ensure-exten-proc m v)
      (or (get-exten-proc m v) (make-extensible-procedure #:name v)))

    ;;----------------------------------------------------------------------

    (define/fallback merge-extensibles (lambda _ #f))

    (define/pattern (merge-extensibles importing-module conflicting-name
                                       interface1
                                       (? extensible-procedure? value1)
                                       interface2
                                       (? extensible-procedure? value2)
                                       previous-var previous-value)
      (make-variable (merge-extensible-procedures
                      (if (and previous-var
                               (extensible-procedure? previous-value))
;;; FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
;;; FIXME FIXME FIXME: Just what _is_ the role of
;;; previous-var/previous-value???
                          `(,previous-value ,value1 ,value2)
                          `(,value1 ,value2)))))

    (module-define! duplicate-handlers 'merge-extensibles merge-extensibles)

    ;; Make ‘merge-extensibles’ a default, because it corresponds to
    ;; the intended use of extensible procedures.
    (default-duplicate-binding-handler
      `(merge-extensibles ,@(default-duplicate-binding-handler)))

    ;;----------------------------------------------------------------------
    ) ;; end of eval-when.
  ) ;; end of library.
