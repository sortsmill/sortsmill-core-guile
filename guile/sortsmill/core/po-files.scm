;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core po-files)

  ;; An interface to libgettextpo.

  ;; FIXME: Write all the documentation strings.
  ;;
  ;; FIXME: Add support for PO file modification and writing.

  (export
   po_file_t?
   po_message_iterator_t?
   po_message_t?
   po_filepos_t?
   po-file-read
   po-file-free
   call-with-po-file
   po-file-encoding
   current-po-file-encoding
   po-file-domains
   po-file-domain-header
   po-header-field
   po-message-iterator
   po-message-iterator-free
   po-next-message
   po-file-messages-fold
   po-file-messages-map->list
   po-file-messages-for-each
   po-message-msgctxt
   po-message-msgid
   po-message-msgid-plural
   po-message-msgstr
   po-message-msgstr-plural
   po-message-comments
   po-message-extracted-comments
   po-message-filepos
   po-message-prev-msgctxt
   po-message-prev-msgid
   po-message-prev-msgid-plural
   po-message-is-obsolete?
   po-message-is-fuzzy?
   po-message-is-format?
   po-message-range
   po-filepos-file
   po-filepos-start-line
   po-format-list
   po-format-pretty-name
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (ice-9 format)
          (only (web http) parse-header)
          (sortsmill core helpers)
          (sortsmill core i18n)
          (sortsmill core xgc)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libgettextpo")
                          po_file_read
                          po_file_free
                          po_file_domains
                          po_file_domain_header
                          po_header_field
                          po_message_iterator
                          po_message_iterator_free
                          po_next_message
                          po_message_msgctxt
                          po_message_msgid
                          po_message_msgid_plural
                          po_message_msgstr
                          po_message_msgstr_plural
                          po_message_comments
                          po_message_extracted_comments
                          po_message_filepos
                          po_message_prev_msgctxt
                          po_message_prev_msgid
                          po_message_prev_msgid_plural
                          po_message_is_obsolete
                          po_message_is_fuzzy
                          po_message_is_format
                          po_message_is_range
                          po_filepos_file
                          po_filepos_start_line
                          po_format_list
                          po_format_pretty_name
                          ))

  (define-wrapped-pointer-type po_file_t
    po_file_t?
    pointer->po_file_t
    po_file_t->pointer
    (lambda (obj port)
      (format port "#<po_file_t 0x~x>"
              (pointer-address (po_file_t->pointer obj)))))

  (set-procedure-property! po_file_t? 'documentation
                           "Return @code{#t} if the argument is a
@code{po_file_t} handle. Otherwise return @code{#f}.")

  (define-wrapped-pointer-type po_message_iterator_t
    po_message_iterator_t?
    pointer->po_message_iterator_t
    po_message_iterator_t->pointer
    (lambda (obj port)
      (format port "#<po_message_iterator_t 0x~x>"
              (pointer-address (po_message_iterator_t->pointer obj)))))

  (set-procedure-property! po_message_iterator_t? 'documentation
                           "Return @code{#t} if the argument is a
@code{po_message_iterator_t} handle. Otherwise return @code{#f}.")

  (define-wrapped-pointer-type po_message_t
    po_message_t?
    pointer->po_message_t
    po_message_t->pointer
    (lambda (obj port)
      (format port "#<po_message_t 0x~x>"
              (pointer-address (po_message_t->pointer obj)))))

  (set-procedure-property! po_message_t? 'documentation
                           "Return @code{#t} if the argument is a
@code{po_message_t} handle. Otherwise return @code{#f}.")

  (define-wrapped-pointer-type po_filepos_t
    po_filepos_t?
    pointer->po_filepos_t
    po_filepos_t->pointer
    (lambda (obj port)
      (format port "#<po_filepos_t 0x~x>"
              (pointer-address (po_filepos_t->pointer obj)))))

  (set-procedure-property! po_filepos_t? 'documentation
                           "Return @code{#t} if the argument is a
@code{po_filepos_t} handle. Otherwise return @code{#f}.")

  (define po-file-read
    (let ([proc (pointer->procedure '* po_file_read '(*))])
      (lambda (file-name)
        "Read the PO file @var{file-name}, returning a
@code{po_file_t} handle."
        (let ([f (proc (string->pointer file-name))])
          (if (null-pointer? f)
              (error 'po-file-read
                     (_ "failed to read the specified PO file")
                     file-name)
              (pointer->po_file_t f))))))

  (define po-file-free
    (let ([proc (pointer->procedure void po_file_free '(*))])
      (lambda (po-file)
        "Free the memory associated with the given @code{po_file_t}
handle."
        (proc (po_file_t->pointer po-file)))))

  (define (call-with-po-file file-name proc)
    "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."    
    (let ([po-file (po-file-read file-name)])
      (parameterize ([current-po-file-encoding po-file])
        (dynamic-wind
          (lambda () *unspecified*)
          (lambda () (proc po-file))
          (lambda () (po-file-free po-file))))))

  (define* (po-file-encoding po-file #:optional [domain #f])
    "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
    (let ([header (po-file-domain-header po-file "ISO-8859-1" domain)])
      (if header
          (let ([content-type (po-header-field header "Content-Type")])
            (if content-type
                (assq-ref (cdr (parse-header 'content-type content-type))
                          'charset)
                #f))
          #f)))

  (define current-po-file-encoding
    (make-parameter
     "ASCII" (lambda (val)
               (cond [(string? val) val]
                     [(po_file_t? val) (or (po-file-encoding val) "ASCII")]
                     [else (assertion-violation 'current-po-file-encoding
                                                (_ "illegal initializer")
                                                val)]))))

  (define po-file-domains
    (let ([proc (pointer->procedure '* po_file_domains '(*))])
      (lambda (po-file)
        "Given a @code{po_file_t} handle, return a list of strings
representing the domains contained in the PO file. If the PO file
contains no `domain' directive, the return value will equal
@code{(list \"messages\")}."
        (let collect-domains
            ([domain-pointers (proc (po_file_t->pointer po-file))]
             [domains '()])
          (let ([domain-ptr (dereference-pointer domain-pointers)])
            (if (null-pointer? domain-ptr)
                (reverse domains)
                (collect-domains
                 (make-pointer (+ (sizeof '*)
                                  (pointer-address domain-pointers)))
                 (cons (pointer->string domain-ptr) domains))))))))

  (define po-file-domain-header
    (let ([proc (pointer->procedure '* po_file_domain_header '(* *))])
      (lambda* (po-file #:optional [encoding (current-po-file-encoding)] [domain #f])
        "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
        (let ([p (proc (po_file_t->pointer po-file)
                       (if domain (string->pointer domain) %null-pointer))])
          (if (null-pointer? p) #f (pointer->string p -1 encoding))))))

  (define po-header-field
    (let ([proc (pointer->procedure '* po_header_field '(* *))])
      (lambda (header field)
        "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring.

Useful information: @code{`Content-Type'} header fields can be parsed
by importing @code{(web http)} and calling something like
@code{(parse-header 'content-type
my-content-type-header-field-string)}."
        (let ([p (proc (string->pointer header "UTF-8")
                       (string->pointer field "UTF-8"))])
          (if (null-pointer? p)
              #f
              (pointer->string (x-gc-grabstr p) -1 "UTF-8"))))))

  (define po-message-iterator
    (let ([proc (pointer->procedure '* po_message_iterator '(* *))])
      (lambda* (po-file #:optional [domain #f])
        "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
        (let ([p (proc (po_file_t->pointer po-file)
                       (if domain (string->pointer domain) %null-pointer))])
          (if (null-pointer? p)
              (error 'po-message-iterator
                     (_ "failed to create an iterator")
                     po-file domain)
              (pointer->po_message_iterator_t p))))))

  (define po-message-iterator-free
    (let ([proc (pointer->procedure void po_message_iterator_free '(*))])
      (lambda (iter)
        "Free the memory associated with the given
@code{po_message_iterator_t} handle."
        (proc (po_message_iterator_t->pointer iter)))))

  (define po-next-message
    (let ([proc (pointer->procedure '* po_next_message '(*))])
      (lambda (iter)
        "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
        (let ([p (proc (po_message_iterator_t->pointer iter))])
          (if (null-pointer? p) #f (pointer->po_message_t p))))))

  (define* (po-file-messages-fold proc start po-file #:optional [domain #f])
    "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
    (let ([iter (po-message-iterator po-file domain)])
      (dynamic-wind
        (lambda () *unspecified*)
        (lambda () (let loop ([prior start])
                     (let ([msg (po-next-message iter)])
                       (if msg
                           (loop (proc prior msg))
                           prior))))
        (lambda () (po-message-iterator-free iter)))))

  (define* (po-file-messages-map->list proc po-file #:optional [domain #f])
    "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
    (reverse (po-file-messages-fold (lambda (prior msg)
                                      (cons (proc msg) prior))
                                    '() po-file domain)))

  (define* (po-file-messages-for-each proc po-file #:optional [domain #f])
    "FIXME FIXME FIXME FIXME FIXME FIXME: Write this docstring."
    (po-file-messages-fold (lambda (_ignored_ msg)
                             (proc msg)
                             *unspecified*)
                           *unspecified* po-file domain))

  (define-syntax define-po-message-field
    (lambda (stx)
      (syntax-case stx ()
        [(_ proc-name doc-string) (and (identifier? #'proc-name)
                                       (string? (syntax->datum #'doc-string)))
         (let ([c-func
                ;; Convert proc-name to the same symbol, except with
                ;; #\- mapped to #\_
                (datum->syntax
                 stx (string->symbol
                      (string-map (lambda (c) (if (char=? c #\-) #\_ c))
                                  (symbol->string
                                   (syntax->datum #'proc-name)))))])
           #`(define proc-name
               (let ([proc (pointer->procedure '* #,c-func '(*))])
                 (lambda* (msg #:optional
                               [encoding (current-po-file-encoding)])
                   doc-string
                   (let ([p (proc (po_message_t->pointer msg))])
                     (if (null-pointer? p)
                         #f
                         (pointer->string
                          p -1 encoding)))))))] )))

  (define-po-message-field po-message-msgctxt
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`msgctxt' of the message, if it exists; otherwise return @code{#f}.")

  (define-po-message-field po-message-msgid
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`msgid' of the message.")

  (define-po-message-field po-message-msgid-plural
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`msgid_plural' of the message, if that field exists; otherwise return
@code{#f}.")

  (define-po-message-field po-message-msgstr
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`msgstr' of the message. If there is no translation, the return value
is an empty string.")

  (define po-message-msgstr-plural
    (let ([proc (pointer->procedure '* po_message_msgstr_plural `(* ,int))])
      (lambda* (msg index #:optional [encoding (current-po-file-encoding)])
        "Given a @code{po_message_t} handle, an integer @code{index},
and optionally an encoding name such as @code{\"UTF-8\"} or
@code{\"ISO-8859-1\"}, return the `msgstr[index]' of the message, if
it exists; otherwise return @code{#f}."
        (let ([p (proc (po_message_t->pointer msg) index)])
          (if (null-pointer? p) #f (pointer->string p -1 encoding))))))

  (define-po-message-field po-message-comments
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
comments of the message, if they exist; otherwise return an empty
string.")

  (define-po-message-field po-message-extracted-comments
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
extracted comments of the message, if they exist; otherwise return an
empty string.")

  (define po-message-filepos
    (let ([proc (pointer->procedure '* po_message_filepos `(* ,int))])
      (lambda (msg i)
        "Given a @code{po_message_t} handle and an integer index
@var{i}, return a @code{po_filepos_t} handle for the @var{i}th source
file position, if such a file position is marked in the PO file;
otherwise return @code{#f}."
        (let ([fp (proc (po_message_t->pointer msg) i)])
          (if (null-pointer? fp) #f (pointer->po_filepos_t fp))))))

  (define-po-message-field po-message-prev-msgctxt
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`previous msgctxt' of the message, if it exists; otherwise return
@code{#f}.")

  (define-po-message-field po-message-prev-msgid
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`previous msgid' of the message, if it exists; otherwise return
@code{#f}.")

  (define-po-message-field po-message-prev-msgid-plural
    "Given a @code{po_message_t} handle and optionally an encoding
name such as @code{\"UTF-8\"} or @code{\"ISO-8859-1\"}, return the
`previous msgid_plural' of the message, if that field exists;

otherwise return @code{#f}.")

  (define po-message-is-obsolete?
    (let ([proc (pointer->procedure int po_message_is_obsolete '(*))])
      (lambda (msg)
        "Is the message associated with the given @code{po_message_t}
handle marked obsolete?"
        (not (zero? (proc (po_message_t->pointer msg)))))))

  (define po-message-is-fuzzy?
    (let ([proc (pointer->procedure int po_message_is_fuzzy '(*))])
      (lambda (msg)
        "Is the message associated with the given @code{po_message_t}
handle marked fuzzy?"
        (not (zero? (proc (po_message_t->pointer msg)))))))

  (define po-message-is-format?
    (let ([proc (pointer->procedure int po_message_is_format '(* *))])
      (lambda (msg format-name)
        "Is the message associated with the given @code{po_message_t}
handle marked as being a format string of kind named by the string
@var{format-name}, for instance @code{\"c-format\"} or
@code{\"scheme-format\"}?"
        (not (zero? (proc (po_message_t->pointer msg)
                          (string->pointer format-name "UTF-8")))))))

  (define po-message-range
    (let ([proc (pointer->procedure int po_message_is_range '(* * *))]
          [sz (sizeof int)])
      (lambda (msg)
        "If the message associated with the given @code{po_message_t}
handle has a numeric range, return the min and max as multiple values;
otherwise return @code{#f} for both values."
        (let* ([bv (make-bytevector (+ sz sz))]
               [has-range?
                (not (zero? (proc (po_message_t->pointer msg)
                                  (bytevector->pointer bv)
                                  (bytevector->pointer bv sz))))])
          (if has-range?
              (values (bytevector-sint-ref bv 0 (native-endianness) sz)
                      (bytevector-sint-ref bv sz (native-endianness) sz))
              (values #f #f))))))

  (define po-filepos-file
    (let ([proc (pointer->procedure '* po_filepos_file '(*))])
      (lambda (pos)
        ;; FIXME: Is it possible for the return value of
        ;; po_filepos_file to be a null pointer, so that there is no
        ;; file name?
        "Given a @code{po_filepos_t} handle, return the file name."
        (let ([p (proc (po_filepos_t->pointer pos))])
          ;; FIXME: Is a null pointer possible here?
          (if (null-pointer? p) #f (pointer->string p))))))

  (define po-filepos-start-line
    (let* ([proc (pointer->procedure size_t po_filepos_start_line '(*))]
           [bv (make-bytevector (sizeof size_t))]
           [_ignored_ (bytevector-sint-set! bv 0 -1 (native-endianness)
                                            (sizeof size_t))]
           [size_t_of_-1 (bytevector-uint-ref bv 0 (native-endianness)
                                              (sizeof size_t))])
      (lambda (pos)
        "Given a @code{po_filepos_t} handle, return the line number
where the strings starts, if this line number is available; otherwise
return @code{#f}."
        (let ([n (proc (po_filepos_t->pointer pos))])
          (if (= n size_t_of_-1) #f n)))))

  (define po-format-list
    ;; FIXME: Would it be better to build the list just once, at
    ;; startup?
    (let ([proc (pointer->procedure '* po_format_list '())])
      (lambda ()
        "Return a list of strings specifying the supported format
flags."
        (let collect-formats
            ([format-pointers (proc)]
             [formats '()])
          (let ([format-ptr (dereference-pointer format-pointers)])
            (if (null-pointer? format-ptr)
                (reverse formats)
                (collect-formats
                 (make-pointer (+ (sizeof '*)
                                  (pointer-address format-pointers)))
                 (cons (pointer->string format-ptr) formats))))))))

  (define po-format-pretty-name
    (let ([proc (pointer->procedure '* po_format_pretty_name '(*))])
      (lambda (format-name)
        "Given a format flag name, such as @code{\"csharp-format\"},
return a corresponding `pretty' name, such as @code{\"C#\"}. If the
format flag is not supported, return @code{#f}."
        (let ([p (proc (string->pointer format-name "UTF-8"))])
          (if (null-pointer? p) #f (pointer->string p -1 "UTF-8"))))))

  ) ;; end of library.
