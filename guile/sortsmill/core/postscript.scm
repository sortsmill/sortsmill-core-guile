;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core postscript)

  (export
   postscript-integer?      ; (postscript-integer? string) → boolean
   postscript-real?         ; (postscript-real? string) → boolean
   postscript-radix-number? ; (postscript-radix-number? string) → boolean
   postscript-number?       ; (postscript-number? string) → boolean
   postscript->number       ; (postscript->number string) → real
   )

  (import (sortsmill core helpers)
          (sortsmill core machine)
          (rnrs)
          (except (guile) error)
          (system foreign))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libsortsmill-core")
                          is_postscript_integer
                          is_postscript_real
                          is_postscript_radix_number
                          is_postscript_number)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_postscript_to_number))

  (define is-postscript-integer
    (pointer->procedure _Bool is_postscript_integer '(*)))

  (define is-postscript-real
    (pointer->procedure _Bool is_postscript_real '(*)))

  (define is-postscript-radix-number
    (pointer->procedure _Bool is_postscript_radix_number '(*)))

  (define is-postscript-number
    (pointer->procedure _Bool is_postscript_number '(*)))

  (define scm-postscript-to-number
    (pointer->procedure '* scm_postscript_to_number '(*)))

  (define (postscript-integer? s)
    "Test if the string @var{s} represents a PostScript integer, such
as `@code{555}', `@code{+034}', or `@code{-1234}'. Return a boolean."
    (not (zero? (is-postscript-integer (string->pointer s "UTF-8")))))

  (define (postscript-real? s)
    "Test if the string @var{s} represents a PostScript real, such as
`@code{555.}', `@code{+.034}', `@code{-01234E56}', or
`@code{1.9e+23}'. Return a boolean."
    (not (zero? (is-postscript-real (string->pointer s "UTF-8")))))

  (define (postscript-radix-number? s)
    "Test if the string @var{s} represents a PostScript radix number,
such as `@code{2#01011}', `@code{008#555}', or
`@code{16#9ABCdef0}'. Return a boolean."
    (not (zero? (is-postscript-radix-number (string->pointer s "UTF-8")))))

  (define (postscript-number? s)
    "Test if the string @var{s} represents a PostScript integer, real,
or radix number. Return a boolean."
    (not (zero? (is-postscript-number (string->pointer s "UTF-8")))))

  (define (postscript->number s)
    "Convert the string @var{s}, which represents a PostScript integer, real,
or radix number, to a Guile (real) number. Raises an R⁶RS assertion
violation if the conversion fails."
    (pointer->scm (scm-postscript-to-number (scm->pointer s))))

  ) ;; end of library.
