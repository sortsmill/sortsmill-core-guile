;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core miscellaneous-macros)

  (export
   pred-or  ;; Combine predicates in ‘or’ fashion.
   pred-and ;; Combine predicates in ‘and’ fashion.
   )

  (import (rnrs)
          (except (guile) error))

  (define-syntax pred-or
    (syntax-rules ()
      [(_) (const #f)]
      [(_ pred ...) (lambda args (or (apply pred args) ...))]))

  (define-syntax pred-and
    (syntax-rules ()
      [(_) (const #t)]
      [(_ pred ...) (lambda args (and (apply pred args) ...))]))

  ) ;; end of library.
