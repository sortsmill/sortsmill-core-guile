;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core brent-root-finder)

  (export sbrentroot f32brentroot     ; Synonyms for 32-bit brentroot.
          dbrentroot f64brentroot     ; Synonyms for 64-bit brentroot.
          brentroot)             ; Scheme implementation of brentroot.

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core machine)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_sbrentroot
                          scm_dbrentroot))

  ;;--------------------------------------------------------------------

  (define*-scm-procedure (sbrentroot t0 t1 f #:optional tolerance epsilon)
    "Single-precision Brent’s method root-finder, implemented in C."
    scm_sbrentroot)

  (define*-scm-procedure (dbrentroot t0 t1 f #:optional tolerance epsilon)
    "Double-precision Brent’s method root-finder, implemented in C."
    scm_dbrentroot)

  ;; Synonyms.
  (define f32brentroot sbrentroot)
  (define f64brentroot dbrentroot)

  ;;--------------------------------------------------------------------

  (define* (brentroot t0 t1 f
                      #:optional
                      [tolerance 0]
                      [epsilon dbl-epsilon] ; An exact rational!
                      #:key
                      [debugging-port #f])

    "A Brent’s method root-finder that does not force the values to
floating point. The current implementation is purely in Scheme.

It is recommended to use @var{sbrentroot} or @var{dbrentroot} instead,
if possible."

    (define (half x) (/ x 2))

    (define (apply-sign x y)
      ;; Apply the sign of x to y.  We do not need to deal with x=0.
      (if (negative? x) (- y) y))

    (define (bracketed? fx fy)
      (or (and (negative? fx) (positive? fy))
          (and (positive? fx) (negative? fy))))

    (define (iteration a b c fa fb fc step0 step1)

      (define (linear-interpolation fa fb half-interval tol1)
        (let ([s (/ fb fa)])
          (let ([p (* 2 half-interval s)]
                [q (- 1 s)])
            (if (positive? p)
                (decide-step p (- q) half-interval tol1
                             step0 step1)
                (decide-step (- p) q half-interval tol1
                             step0 step1)))))

      (define (inverse-quadratic-interpolation fa fb fc half-interval tol1)
        (let ([q (/ fa fc)]
              [r (/ fb fc)]
              [s (/ fb fa)])
          (let ([q-1 (1- q)]
                [r-1 (1- r)]
                [s-1 (1- s)])
            (let ([pp (* s (- (* 2 half-interval q (- q r))
                              (* (- b a) r-1)))]
                  [qq (* q-1 r-1 s-1)])
              (if (positive? pp)
                  (decide-step pp (- qq) half-interval tol1
                               step0 step1)
                  (decide-step (- pp) qq half-interval tol1
                               step0 step1))))))

      (define (decide-step p q half-interval tol1 step0 step1)
        ;; Revert to bisection if necessary.
        (let ([two_p (+ p p)])
          (if (and (< two_p (- (* 3 half-interval q) (abs (* tol1 q))))
                   (< two_p (abs (* step1 q))))
              (values (/ p q) step0)
              (begin
                (when debugging-port
                  (format debugging-port " revert to bisection\n"))
                (values half-interval half-interval)))))

      (define (actual-step step half-interval tol1)
        ;; Move the bracket by at least tol1.
        (if (< tol1 (abs step))
            step
            (apply-sign half-interval tol1)))

      (let ([tol1 (+ (* 2 epsilon (abs b)) (half tolerance))]
            [half-interval (half (- c b))])
        (if (or (<= (abs half-interval) tol1) (zero? fb))
            (begin
              (when debugging-port
                (if (inexact? b)
                    (format debugging-port " result =~26,17,2,1E\n" b)
                    (format debugging-port " result = ~a\n" b)))
              b)
            (let-values
                ([(step0^ step1^)
                  (if (and (<= tol1 (abs step1))
                           (< (abs fb) (abs fa)))
                      ;; Interpolation.
                      (if (= a c)
                          (begin
                            (when debugging-port
                              (format debugging-port
                                      " linear interpolation\n"))
                            (linear-interpolation fa fb half-interval
                                                  tol1))
                          (begin
                            (when debugging-port
                              (format debugging-port
                                      " inverse quadratic interpolation\n"))
                            (inverse-quadratic-interpolation fa fb fc
                                                             half-interval
                                                             tol1)))
                      (begin
                        (when debugging-port
                          (format debugging-port " bisection\n"))
                        (values half-interval half-interval)))])
              (let* ([delta (actual-step step0^ half-interval tol1)]
                     [b^ (+ b delta)]
                     [fb^ (f b^)])
                (when debugging-port
                  (format debugging-port " (abs (delta) == tol1) ?   ~1d\n"
                          (if (= (abs delta) tol1) 1 0))
                  (if (inexact? delta)
                      (format debugging-port " delta =~27,17,2,1E\n" delta)
                      (format debugging-port " delta =  ~a\n" delta)))
                (if (bracketed? fb^ fc)
                    (if (<= (abs fb^) (abs fc))
                        (iteration b b^ c fb fb^ fc step0^ step1^)
                        (iteration b^ c b^ fb^ fc fb^ step0^ step1^))
                    (let
                        ;; Recompute delta to account for roundoff.
                        ;; (FIXME: Is this necessary?)
                        ([delta (- b^ b)])
                      (if (<= (abs fb^) (abs fb))
                          (iteration b b^ b fb fb^ fb delta delta)
                          (iteration b^ b b^ fb^ fb fb^ delta delta)))))))))

    (let ([ft0 (f t0)]
          [ft1 (f t1)])
      (unless (bracketed? ft0 ft1)
        (assertion-violation
         'brentroot (_ "the root is not bracketed between these points")
         t0 t1))
      (let ([t1-t0 (- t1 t0)])
        (if (< (abs ft0) (abs ft1))
            (iteration t1 t0 t1 ft1 ft0 ft1 t1-t0 t1-t0)
            (iteration t0 t1 t0 ft0 ft1 ft0 t1-t0 t1-t0)))))

  ;;--------------------------------------------------------------------

  ) ;; end of library.
