;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core psmat)

  ;;
  ;; PostScript matrices as Guile vectors of length six. These are
  ;; analogous to the Python six-tuples employed in (sortsmill core
  ;; python py-psmat) and in the original FontForge’s psMat module.
  ;;

  (export
   psmat-identity  ;; (psmat-identity) → vector
   psmat-compose   ;; (psmat-compose vector1 ...) → vector
   psmat-inverse   ;; (psmat-inverse vector) → vector
   psmat-rotate    ;; (psmat-rotate θ) → vector
   psmat-scale     ;; (psmat-scale x [y = x]) → vector
   psmat-skew      ;; (psmat-skew θ) → vector
   psmat-translate ;; (psmat-translate x y) → vector
   )

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (sortsmill smcoreguile-pkginfo)
          (sortsmill core i18n))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (define (psmat-identity)
    "Return a PostScript identity matrix as a six-element vector of
real numbers."
    #(1 0 0 1 0 0))

  (define* (psmat-compose mat1 #:optional mat2 #:rest rest)
    "Return a PostScript matrix that is the composition of one or more
input transformations. The arguments and return value all are
six-element vectors of real numbers."
    (cond [(not mat2)
           (assert (null? rest))
           mat1]
          [else (fold-left compose-two-matrices
                           (compose-two-matrices mat1 mat2)
                           rest)]))

  (define (compose-two-matrices mat1 mat2)
    (match mat1
      [#(m10 m11 m12 m13 m14 m15)
       (match mat2
         [#(m20 m21 m22 m23 m24 m25)
          (let ([a0 (+ (* m10 m20) (* m11 m22))]
                [a1 (+ (* m10 m21) (* m11 m23))]
                [a2 (+ (* m12 m20) (* m13 m22))]
                [a3 (+ (* m12 m21) (* m13 m23))]
                [a4 (+ (* m14 m20) (* m15 m22) m24)]
                [a5 (+ (* m14 m21) (* m15 m23) m25)])
            (list->vector (list a0 a1 a2 a3 a4 a5)))]
         [_ (assertion-violation 'psmat-compose
                                 (_ "second argument: expected a six-element vector")
                                 mat2)])]
      [_ (assertion-violation 'psmat-compose
                              (_ "first argument: expected a six-element vector")
                              mat1)]))

  (define (psmat-inverse mat)
    "Invert a PostScript matrix. For simplicity, use Cramer's rule in
exact arithmetic. The arguments and return value both are six-element
vectors of real numbers."
    (match (vector-map inexact->exact mat)
      [#(a0 a1 a2 a3 a4 a5)

       ;; Below, the letter ‘A’ represents the matrix:
       ;;
       ;; A = a0 a1
       ;;     a2 a3

       ;; Compute:
       ;;
       ;;    The determinant –
       ;;
       ;;        det A = (a0 * a3) - (a1 * a2)
       ;;
       ;;    The adjugate –
       ;;
       ;;        adj A =  a3 -a1 = g0 g1
       ;;                -a2  a0   g2 g3
       ;;
       (let ([determinant (- (* a0 a3) (* a1 a2))]
             [g0 a3]
             [g1 (- a1)]
             [g2 (- a2)]
             [g3 a0])
         
         ;; Compute:
         ;;
         ;;    The inverse –
         ;;
         ;;        inv A = adj A / det A = v0 v1 = A⁻¹
         ;;                                v2 v3
         ;;
         (let ([v0 (/ g0 determinant)]
               [v1 (/ g1 determinant)]
               [v2 (/ g2 determinant)]
               [v3 (/ g3 determinant)])

           ;; Compute the offset in the plane.
           (let ([d0 (- (+ (* a4 v0) (* a5 v2)))]
                 [d1 (- (+ (* a4 v1) (* a5 v3)))])

             ;; The result.
             (list->vector (list v0 v1 v2 v3 d0 d1)))))]

      [_ (assertion-violation 'psmat-inverse
                              (_ "expected a six-element vector")
                              mat)]))

  (define (psmat-rotate theta)
    "Return a PostScript matrix that will rotate by an angle expressed
in radians. The matrix is represented by a six-element vector of real
numbers."
    (let ([cosine (cos theta)]
          [sine (sin theta)])
      (list->vector (list cosine sine (- sine) cosine 0 0))))

  (define* (psmat-scale x #:optional [y x])
    "Return a PostScript matrix that will scale by x horizontally and
y vertically. If y is omitted, the matrix will scale by x in both
directions. The matrix is represented by a six-element vector of real
numbers."
    (list->vector (list x 0 0 y 0 0)))

  (define (psmat-skew theta)
    "Return a PostScript matrix that will skew by an angle (to produce
an oblique font). The angle is expressed in radians. The matrix is
represented by a six-element vector of real numbers."
    (list->vector (list 1 0 (tan theta) 1 0 0)))

  (define (psmat-translate x y)
    "Return a PostScript matrix that will translate by x horizontally
and y vertically.  The matrix is represented by a six-element vector
of real numbers."
    (list->vector (list 1 0 0 1 x y)))

  ) ;; end of library.
