;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core libunicodenames)

  ;; FIXME: Add support for ‘blocks DB’.

  (export
   uninm-names-db?      ; (uninm-names-db? obj) → boolean
   uninm-find-names-db  ; (uninm-find-names-db [locale-base]) → string
   uninm-names-db-open  ; (uninm-names-db-open file-name) → uninm-names-db
   uninm-names-db-close ; (uninm-names-db-close uninm-names-db) → *unspecified*
   call-with-uninm-names-db ; (call-with-uninm-names-db file-name proc) → result-of-proc
   uninm-name       ; (uninm-name uninm-names-db char-or-integer) → string-or-#f
   uninm-annotation ; (uninm-annotation uninm-names-db char-or-integer) → string-or-#f
   )

  (import (sortsmill core helpers)
          (sortsmill core xgc)
          (sortsmill smcoreguile-pkginfo)
          (rnrs)
          (except (guile) error)
          (ice-9 format)
          (ice-9 i18n)
          (system foreign))

  (define (_ string) (gettext string SMCOREGUILE_PACKAGE))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-ltdlopened "libunicodenames")
                          uninm_find_names_db
                          uninm_names_db_open
                          uninm_names_db_close
                          uninm_name
                          uninm_annotation))

  (define-wrapped-pointer-type uninm-names-db
    uninm-names-db?
    pointer->uninm-names-db
    uninm-names-db->pointer
    (lambda (obj port)
      ;; WARNING: Think carefully before modifying this procedure to
      ;; do more than print the @code{pointer-address}. It should
      ;; succeed even if the uninm-names-db object is `closed'.
      (format port "#<uninm-names-db 0x~x>"
              (pointer-address (uninm-names-db->pointer obj)))))

  (set-procedure-property! uninm-names-db? 'documentation
                           "Return @code{#t} if the argument is a
`uninm-names-db' handle. Otherwise return @code{#f}.")

  (define uninm-find-names-db
    (let ([find-db (pointer->procedure '* uninm_find_names_db '(*))])
      (lambda* (#:optional [locale-base #f])
        "Return the full path name of `names database', as a
string. Optionally look for gettext files under the directory given by
the string @var{locale-base}. This procedure calls the C function
uninm_find_names_db(3), and raises R6RS-style `error' if the C
function fails."
        (let ([file-name (find-db (if locale-base
                                      (string->pointer locale-base)
                                      %null-pointer))])
          (if (null-pointer? file-name)
              (error 'uninm-find-names-db
                     (_ "failed to find a names db")
                     locale-base)
              (pointer->string (x-gc-grabstr file-name)))))))

  (define uninm-names-db-open
    (let ([db-open (pointer->procedure '* uninm_names_db_open '(*))])
      (lambda (file-name)
        "Open a `names database' file and return a `uninm-names-db'
handle. This procedure calls the C function uninm_names_db_open(3),
and raises R6RS-style `error' if the C function fails.

The open names database @emph{is not} closed automatically when the
handle is destroyed."
        (let ([db (db-open (string->pointer file-name))])
          (if (null-pointer? db)
              (error 'uninm-find-names-db
                     (_ "failed to open a names db")
                     file-name)
              (pointer->uninm-names-db db))))))

  (define uninm-names-db-close
    (let ([db-close (pointer->procedure void uninm_names_db_close '(*))])
      (lambda (uninm-names-db)
        "Close a `names database', which must be opened with the given
handle @var{uninm-names-db}. Afterwards the handle is meaningless and
must not be used for anything. The return value is unspecified.

This procedure calls the C function uninm_names_db_close(3), and is
dangerous to call directly, because the C function does not do error
checking. Be careful."
        (if (uninm-names-db? uninm-names-db)
            (db-close (uninm-names-db->pointer uninm-names-db))
            (assertion-violation 'uninm-names-db-close
                                 (_ "expected a uninm-names-db handle")
                                 uninm-names-db)))))

  (define (call-with-uninm-names-db file-name proc)
    "Call @code{uninm-names-db-open} to open @var{file-name}. Then, if
@code{uninm-names-db-open} succeeds, call the procedure @var{proc}, in
a dynamic wind, passing the `uninm-names-db' handle as argument. On
exit from the dynamic wind, call @code{uninm-names-db-close} to close
the database. Return the result of the call to @var{proc}.

Exceptions raised by @code{uninm-names-db-open} are passed along,
unaltered.

Be careful:

@itemize @bullet
  @item
  You must not call @code{uninm-names-db-close} on the
  handle. Let the dynamic wind close it.

  @item
  If @var{proc} returns the `uninm-names-db' handle, it is
  meaningless after exit from the dynamic wind.
@end itemize"
    (let ([handle (uninm-names-db-open file-name)])
      (dynamic-wind
        (lambda () *unspecified*)
        (lambda () (proc handle))
        (lambda () (uninm-names-db-close handle)))))

  (define uninm-name
    (let ([get-name (pointer->procedure '* uninm_name `(* ,unsigned-int))])
      (lambda (uninm-names-db c)
        "Given an open `uninm-names-db' handle and a character or
Unicode code point (an integer between 0 and #xFFFFFFFF), return a
Unicode name string (or its translation into another language), or
@code{#f} if no such string is found. The name string may contain any
Unicode characters, although the default `en' strings happen to be
ASCII without lowercase, as found in the standard Unicode names list."
        (if (uninm-names-db? uninm-names-db)
            (cond [(char? c) (uninm-name uninm-names-db (char->integer c))]
                  [(and (integer? c) (<= 0 c #xFFFFFFFF))
                   (let ([name
                          (get-name (uninm-names-db->pointer uninm-names-db) c)])
                     (if (null-pointer? name)
                         #f
                         (pointer->string name -1 "UTF-8")))]
                  [else
                   (assertion-violation
                    'uninm-name
                    (_ "expected a character or integer between 0 and #xFFFFFFFF")
                    c)])
            (assertion-violation 'uninm-name
                                 (_ "expected a uninm-names-db handle")
                                 uninm-names-db)))))

  (define uninm-annotation
    (let ([get-annotation
           (pointer->procedure '* uninm_annotation `(* ,unsigned-int))])
      (lambda (uninm-names-db c)
        "Given an open `uninm-names-db' handle and a character or
Unicode code point (an integer between 0 and #xFFFFFFFF), return a
Unicode annotation string (or its translation into another language),
or @code{#f} if no such string is found. The annotation string may
contain any Unicode characters."
        (if (uninm-names-db? uninm-names-db)
            (cond [(char? c)
                   (uninm-annotation uninm-names-db (char->integer c))]
                  [(and (integer? c) (<= 0 c #xFFFFFFFF))
                   (let ([annotation
                          (get-annotation
                           (uninm-names-db->pointer uninm-names-db) c)])
                     (if (null-pointer? annotation)
                         #f
                         (pointer->string annotation -1 "UTF-8")))]
                  [else
                   (assertion-violation
                    'uninm-annotation
                    (_ "expected a character or integer between 0 and #xFFFFFFFF")
                    c)])
            (assertion-violation 'uninm-annotation
                                 (_ "expected a uninm-names-db handle")
                                 uninm-names-db)))))

  ) ;; end of library.
