;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core helpers)

  (export dynamic-link-ltdlopened
          dynamic-link-sortsmill-core-guile
          dynamic-link-sortsmill-core-guile-python
          expose-foreign-funcs
          foreign-func-searchfirst-list
          foreign-func-searchlast-list)

  (import (rnrs)
          (except (guile) error)
          (ice-9 match)
          (ice-9 i18n)
          (ice-9 format)
          (system foreign)
          (sortsmill smcoreguile-pkginfo))

  (define (_ string) (gettext string SMCOREGUILE_PACKAGE))

  (define (python-support-is-linked-in?)
    (catch #t
      (lambda ()
        (not (not (dynamic-pointer "scm_c_make_pyobject" (dynamic-link)))))
      (lambda ignored-args #f)))

  (define (python-extension-is-loaded?)
    (not (not (resolve-module '(__smcoreguile__python_extension_data__)
                              #f #:ensure #f))))

  (define (python-extension-major-minor)
    (let* ([m (resolve-module '(__smcoreguile__python_extension_data__) #f)]
           [major (module-ref m 'python-major-version)]
           [minor (module-ref m 'python-minor-version)])
      (values major minor)))

  (define (dynamic-link-ltdlopened lib)
    "If the C (or similar) library @var{lib} can be loaded
dynamically, load it. Otherwise assume it is already linked into the
program (possibly as a static library). Return the result of
@code{dynamic-link}."
    (catch #t
      (lambda () (dynamic-link lib))
      (lambda unused-args (dynamic-link))))

  (define (dynamic-link-sortsmill-core-guile)
    "Call @code{dynamic-link-ltdlopened} on the Sorts Mill Core Guile
main library."
    (dynamic-link-ltdlopened (string-append "libsortsmill-core-guile-"
                                            (effective-version))))

  (define (dynamic-link-sortsmill-core-guile-python)
    "Call @code{dynamic-link-ltdlopened} on the Sorts Mill Core Guile
Python-support library."
    (cond
     [(python-support-is-linked-in?)
      ;; We end up here if the program is interpreted by a program that has
      ;; been linked with the Sorts Mill Core Guile Python-support library.
      (dynamic-link)]
     [(python-extension-is-loaded?)
      ;; We end up here if the program is interpreted by the Python
      ;; extension (sortsmill-core-guile.so).
      (let-values ([(major minor) (python-extension-major-minor)])
        (dynamic-link-ltdlopened
         (format #f "libsortsmill-core-guile-~a-py~d.~d"
                 (effective-version) major minor)))]
     [(getenv "SORTSMILL_PYTHON_VERSION") =>
      ;; We end up here if the program is interpreted, for instance,
      ;; by the stock ‘guile’ program; but only if also there is an
      ;; environment variable that tells us what version of Python to
      ;; use.
      (lambda (env-value)
        (dynamic-link-ltdlopened
         (format #f "libsortsmill-core-guile-~a-py~a"
                 (effective-version) env-value)))]
     [else
      ;; We end up here if the program is interpreted, for instance,
      ;; by the stock ‘guile’ program, but we do not know what version
      ;; of Python to use.
      ;;
      ;; FIXME Handle this situation better.
      ;;
      (error 'dynamic-link-sortsmill-core-guile-python
             (_ "Please set the environment variable SORTSMILL_PYTHON_VERSION to one of \"2.6\", \"2.7\", \"3.2\", \"3.3\", etc."))]))

  (define this-program (dynamic-link))

  (define foreign-func-searchfirst-list
    ;; By default, check the program’s own exported symbols first.
    (make-parameter (list this-program)))

  (define foreign-func-searchlast-list
    (make-parameter '()))

  (define (check-for-func dll func)
    (catch #t
      (lambda ()
        (let ([result (dynamic-func (symbol->string func) dll)])
          (when (and (not (eq? dll this-program))
                     (getenv "SORTSMILL_SHOW_DYNLINKED_SYMBOLS"))
            ;; A debugging aid, mostly for Makefile development. Also
            ;; could be used for testing.
            (format (current-error-port) ">>> DYNLINKED SYMBOL: '~s'\n" func))
          result))
      (lambda ignored-args #f)))

  (define (find-func dll-list func)
    (and (not (null? dll-list))
         (or (check-for-func (car dll-list) func)
             (find-func (cdr dll-list) func))))

  (define (make-dll-list dll-or-list)
    (if (list? dll-or-list)
        (begin (assert (for-all dynamic-object? dll-or-list))
               (append (foreign-func-searchfirst-list)
                       dll-or-list
                       (foreign-func-searchlast-list)))
        (begin (assert (dynamic-object? dll-or-list))
               (make-dll-list (list dll-or-list)))))

  (define-syntax expose-one-foreign-func
    (lambda (stx)
      (syntax-case stx ()
        [(_ dll func) (identifier? #'func)
         #'(define func
             (or (find-func (make-dll-list dll) 'func)
                 (error 'expose-foreign-funcs
                        (_ "foreign function not found")
                        'func)))])))

  (define-syntax expose-foreign-funcs
    (lambda (stx)
      "FIXME FIXME FIXME (THIS DOCUMENTATION IS
OBSOLETE; NOW IT SEARCHES A LIST):
(expose-foreign-funcs DLL FUNC ...); does the equivalent of trying
first (define func (dynamic-func \"FUNC\" (dynamic-link))) and
then (define func (dynamic-func \"FUNC\" DLL)), for each FUNC."
      (syntax-case stx ()
        [(_ dll func ...) (for-all identifier? #'(func ...))
         #'(begin (expose-one-foreign-func dll func) ...)] )))

  ) ;; end of library.
