;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core machine) ;;; Low-level stuff, common
                                  ;;; constants, etc.

  (export _Bool              ; Foreign type for ‘_Bool’
          short              ; Foreign type for ‘short int’
          unsigned-short     ; Foreign type for ‘unsigned short int’
          intptr             ; Foreign type for ‘intptr_t’
          uintptr            ; Foreign type for ‘uintptr_t’
          float-at-size      ; (float-at-size n) → foreign-type
          int-at-size        ; (int-at-size n) → foreign-type
          uint-at-size       ; (uint-at-size n) → foreign-type
          float_t-at-size    ; (float_t-at-size n) → symbol
          int_t-at-size      ; (int_t-at-size n) → symbol
          uint_t-at-size     ; (uint_t-at-size n) → symbol
          bytevector-address-native-set! ; (bytevector-address-native-set! bv index integer) → *unspecified*
          bytevector-address-native-ref ; (bytevector-address-native-ref bv index) → integer
          bytevector-pointer-native-set! ; (bytevector-pointer-native-set! bv index pointer) → *unspecified*
          bytevector-pointer-native-ref ; (bytevector-pointer-native-ref bv index) → pointer
          pointer-set!       ; (pointer-set! bytevector-or-pointer pointer) → *unspecified*
          pointer-ref        ; (pointer-ref bytevector-or-pointer) → pointer
          bytevector-SCM-set! ; (bytevector-SCM-set! bv index guile-object) → *unspecified*
          bytevector-SCM-ref ; (bytevector-SCM-ref bv index) → guile-object
          SCM-set!           ; (SCM-set! bytevector-or-pointer guile-object) → *unspecified*
          SCM-ref            ; (SCM-ref bytevector-or-pointer) → guile-object
          bytevector-bool-int-native-set! ; (bytevector-bool-int-native-set! bv index integer) → *unspecified*
          bytevector-bool-int-native-ref ; (bytevector-bool-int-native-ref bv index) → integer
          bytevector-short-native-set! ; (bytevector-short-native-set! bv index integer) → *unspecified*
          bytevector-short-native-ref ; (bytevector-short-native-ref bv index) → integer
          bytevector-unsigned-short-native-set! ; (bytevector-unsigned-short-native-set! bv index integer) → *unspecified*
          bytevector-unsigned-short-native-ref ; (bytevector-unsigned-short-native-ref bv index) → integer
          bytevector-int-native-set! ; (bytevector-int-native-set! bv index integer) → *unspecified*
          bytevector-int-native-ref ; (bytevector-int-native-ref bv index) → integer
          bytevector-unsigned-int-native-set! ; (bytevector-unsigned-int-native-set! bv index integer) → *unspecified*
          bytevector-unsigned-int-native-ref ; (bytevector-unsigned-int-native-ref bv index) → integer
          bytevector-long-native-set! ; (bytevector-long-native-set! bv index integer) → *unspecified*
          bytevector-long-native-ref ; (bytevector-long-native-ref bv index) → integer
          bytevector-unsigned-long-native-set! ; (bytevector-unsigned-long-native-set! bv index integer) → *unspecified*
          bytevector-unsigned-long-native-ref ; (bytevector-unsigned-long-native-ref bv index) → integer
          bytevector-intptr-native-set! ; (bytevector-intptr-native-set! bv index integer) → *unspecified*
          bytevector-intptr-native-ref ; (bytevector-intptr-native-ref bv index) → integer
          bytevector-uintptr-native-set! ; (bytevector-uintptr-native-set! bv index integer) → *unspecified*
          bytevector-uintptr-native-ref ; (bytevector-uintptr-native-ref bv index) → integer
          bytevector-ssize_t-native-set! ; (bytevector-ssize_t-native-set! bv index integer) → *unspecified*
          bytevector-ssize_t-native-ref ; (bytevector-ssize_t-native-ref bv index) → integer
          bytevector-size_t-native-set! ; (bytevector-size_t-native-set! bv index integer) → *unspecified*
          bytevector-size_t-native-ref ; (bytevector-size_t-native-ref bv index) → integer
          bytevector-ptrdiff_t-native-set! ; (bytevector-ptrdiff_t-native-set! bv index integer) → *unspecified*
          bytevector-ptrdiff_t-native-ref ; (bytevector-ptrdiff_t-native-ref bv index) → integer
          flt-epsilon        ; Epsilon of ‘float’ (an exact rational)
          dbl-epsilon        ; Epsilon of ‘double’ (an exact rational)
          pi                 ; Value of π
          pi/2               ; Value of π/2
          pi/3               ; Value of π/3
          pi/4               ; Value of π/4
          pi/5               ; Value of π/5
          pi/6               ; Value of π/6
          /pi                ; Value of 1/π
          /pi/2              ; Value of 2/π
          /pi/3              ; Value of 3/π
          /pi/4              ; Value of 4/π
          /pi/5              ; Value of 5/π
          /pi/6              ; Value of 6/π
          1/pi               ; Value of 1/π (non-standard Scheme syntax)
          2/pi               ; Value of 2/π (non-standard Scheme syntax)
          3/pi               ; Value of 3/π (non-standard Scheme syntax)
          4/pi               ; Value of 4/π (non-standard Scheme syntax)
          5/pi               ; Value of 5/π (non-standard Scheme syntax)
          6/pi               ; Value of 6/π (non-standard Scheme syntax)
          )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (ice-9 i18n)
          (sortsmill core helpers)
          (sortsmill smcoreguile-pkginfo))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          init_sortsmill_core_guile_machine)
    ((pointer->procedure void init_sortsmill_core_guile_machine '())))


  (define (_ msg) (gettext msg SMCOREGUILE_PACKAGE))
  (define (unrecognized-size-msg) (_ "unrecognized size"))

  (define-syntax define-foreign-int-type
    (lambda (stx)
      (syntax-case stx ()
        [(_ name size) (and (identifier? #'name)
                            (integer? (primitive-eval (syntax->datum #'size)))
                            (positive? (primitive-eval (syntax->datum #'size))))
         #'(define name
             (case size
               [(1) int8]
               [(2) int16]
               [(4) int32]
               [(8) int64]
               [else (assertion-violation
                      'name (unrecognized-size-msg) size)]))])))

  (define-syntax define-foreign-uint-type
    (lambda (stx)
      (syntax-case stx ()
        [(_ name size) (and (identifier? #'name)
                            (integer? (primitive-eval (syntax->datum #'size)))
                            (positive? (primitive-eval (syntax->datum #'size))))
         #'(define name
             (case size
               [(1) uint8]
               [(2) uint16]
               [(4) uint32]
               [(8) uint64]
               [else (assertion-violation
                      'name (unrecognized-size-msg) size)]))])))

  (eval-when (compile load eval) ;;; Try to catch problems at compile
                                 ;;; time.
    (define-foreign-uint-type _Bool SIZEOF__BOOL)
    (define-foreign-int-type short SIZEOF_SHORT)
    (define-foreign-uint-type unsigned-short SIZEOF_USHORT)
    (define-foreign-int-type intptr SIZEOF_INTPTR_T)
    (define-foreign-uint-type uintptr SIZEOF_UINTPTR_T))

  (define (float-at-size size)
    (let ([float-size (sizeof float)]
          [double-size (sizeof double)])
      (lambda (size)
        "Return the foreign type @code{float} or @code{double},
depending on the value of the argument @var{size}."
        (cond
         [(= size float-size) float]
         [(= size double-size) double]
         [else (assertion-violation
                'float-at-size (unrecognized-size-msg) size)]))))

  (define (int-at-size size)
    "Return the foreign type @code{int8}, @code{int16},
@code{int32}, or @code{int64}, depending on the value of the
argument @var{size}."
    (case size
      [(1) int8]
      [(2) int16]
      [(4) int32]
      [(8) int64]
      [else (assertion-violation
             'int-at-size (unrecognized-size-msg) size)]))

  (define (uint-at-size size)
    "Return the foreign type @code{uint8}, @code{uint16},
@code{uint32}, or @code{uint64}, depending on the value of the
argument @var{size}."
    (case size
      [(1) uint8]
      [(2) uint16]
      [(4) uint32]
      [(8) uint64]
      [else (assertion-violation
             'uint-at-size (unrecognized-size-msg) size)]))

  (define float_t-at-size
    (let ([float-size (sizeof float)]
          [double-size (sizeof double)])
      (lambda (size)
        "Return the symbol @code{float} or @code{double},
depending on the value of the argument @var{size}."
        (cond
         [(= size float-size) 'float]
         [(= size double-size) 'double]
         [else (assertion-violation
                'float-at-size (unrecognized-size-msg) size)]))))

  (define (int_t-at-size size)
    "Return the symbol @code{int8_t}, @code{int16_t},
@code{int32_t}, or @code{int64_t}, depending on the value of the
argument @var{size}."
    (case size
      [(1) 'int8_t]
      [(2) 'int16_t]
      [(4) 'int32_t]
      [(8) 'int64_t]
      [else (assertion-violation
             'int-at-size (unrecognized-size-msg) size)]))

  (define (uint_t-at-size size)
    "Return the foreign type @code{uint8_t}, @code{uint16_t},
@code{uint32_t}, or @code{uint64_t}, depending on the value of the
argument @var{size}."
    (case size
      [(1) 'uint8_t]
      [(2) 'uint16_t]
      [(4) 'uint32_t]
      [(8) 'uint64_t]
      [else (assertion-violation
             'uint-at-size (unrecognized-size-msg) size)]))

  (eval-when (compile load eval)
    (define bytevector-address-native-set!
      (let ([bv-set!
             (case (sizeof '*)
               [(4) bytevector-u32-native-set!]
               [(8) bytevector-u64-native-set!]
               [else (assertion-violation
                      'bytevector-address-native-set!
                      (unrecognized-size-msg) (sizeof '*))])])
        (lambda (bv index address)
          "Store the value of the `pointer-sized' integer
@var{address} in the bytevector @var{bv} at the given @var{index}."
          (bv-set! bv index address)))))

  (eval-when (compile load eval)
    (define bytevector-address-native-ref
      (let ([bv-ref
             (case (sizeof '*)
               [(4) bytevector-u32-native-ref]
               [(8) bytevector-u64-native-ref]
               [else (assertion-violation
                      'bytevector-address-native-ref
                      (unrecognized-size-msg) (sizeof '*))])])
        (lambda (bv index)
          "Return the value of the `pointer-sized' integer that is
stored in the bytevector @var{bv} at the given @var{index}."
          (bv-ref bv index)))))
  
  (define (bytevector-pointer-native-set! bv index p)
    "Store the value of pointer @var{p} in the bytevector @var{bv} at
the given @var{index}."
    (bytevector-address-native-set! bv index (pointer-address p)))

  (define (bytevector-pointer-native-ref bv index)
    "Return as a pointer the address value that is stored in the
bytevector @var{bv} at the given @var{index}."
    (make-pointer (bytevector-address-native-ref bv index)))

  (define pointer-set!
    (let ([size (sizeof '*)])
      (lambda (location p)
        "Store the value of pointer @var{p} in the memory referred to
by the bytevector or pointer @var{location}."
        (cond [(pointer? location)
               (pointer-set! (pointer->bytevector location size) p)]
              [else (bytevector-pointer-native-set! location 0 p)]))))

  (define pointer-ref
    (let ([size (sizeof '*)])
      (lambda (location)
        "Return as a pointer the address value that is stored in the
memory referred to by the bytevector or pointer @var{location}."
        (cond [(pointer? location)
               (pointer-ref (pointer->bytevector location size))]
              [else (bytevector-pointer-native-ref location 0)]))))

  (define (bytevector-SCM-set! bv index guile-object)
    "Store the @code{object-address} of @var{guile-object} in the
bytevector @var{bv} at the given @var{index}."
    (bytevector-pointer-native-set! bv index (scm->pointer guile-object)))

  (define (bytevector-SCM-ref bv index)
    "Return the Guile object whose @code{object-address} is stored in
the memory referred to by the bytevector or pointer @var{location}."
    (pointer->scm (bytevector-pointer-native-ref bv index)))

  (define (SCM-set! location guile-object)
    "Store the @code{object-address} of @var{guile-object} in the
memory referred to by the bytevector or pointer @var{location}."
    (pointer-set! location (scm->pointer guile-object)))

  (define (SCM-ref location)
    "Return the Guile object whose @code{object-address} is stored in
the memory referred to by the bytevector or pointer @var{location}."
    (pointer->scm (pointer-ref location)))

  (define (bytevector-signed-integer-setter int-type)
    (case (sizeof int-type)
      [(1) bytevector-s8-set!]
      [(2) bytevector-s16-native-set!]
      [(4) bytevector-s32-native-set!]
      [(8) bytevector-s64-native-set!]
      [else (assertion-violation
             'bytevector-signed-integer-setter
             (unrecognized-size-msg) (sizeof int-type))]))

  (define (bytevector-signed-integer-reffer int-type)
    (case (sizeof int-type)
      [(1) bytevector-s8-ref]
      [(2) bytevector-s16-native-ref]
      [(4) bytevector-s32-native-ref]
      [(8) bytevector-s64-native-ref]
      [else (assertion-violation
             'bytevector-signed-integer-reffer
             (unrecognized-size-msg) (sizeof int-type))]))

  (define (bytevector-unsigned-integer-setter int-type)
    (case (sizeof int-type)
      [(1) bytevector-u8-set!]
      [(2) bytevector-u16-native-set!]
      [(4) bytevector-u32-native-set!]
      [(8) bytevector-u64-native-set!]
      [else (assertion-violation
             'bytevector-unsigned-integer-setter
             (unrecognized-size-msg) (sizeof int-type))]))

  (define (bytevector-unsigned-integer-reffer int-type)
    (case (sizeof int-type)
      [(1) bytevector-u8-ref]
      [(2) bytevector-u16-native-ref]
      [(4) bytevector-u32-native-ref]
      [(8) bytevector-u64-native-ref]
      [else (assertion-violation
             'bytevector-unsigned-integer-reffer
             (unrecognized-size-msg) (sizeof int-type))]))

  (define bytevector-bool-int-native-set!
    (let ([setter (bytevector-unsigned-integer-setter _Bool)])
      (lambda (bv index value)
        "FIXME: Document this."
        (setter bv index (if (zero? value) 0 1)))))

  ;; FIXME: These could use documentation strings.
  ;;
  ;; FIXME FIXME FIXME FIXME FIXME FIXME: These need inspection and testing.
  (define bytevector-bool-int-native-ref (bytevector-unsigned-integer-reffer _Bool))
  (define bytevector-short-native-set! (bytevector-signed-integer-setter short))
  (define bytevector-short-native-ref (bytevector-signed-integer-reffer short))
  (define bytevector-unsigned-short-native-set! (bytevector-unsigned-integer-setter unsigned-short))
  (define bytevector-unsigned-short-native-ref (bytevector-unsigned-integer-reffer unsigned-short))
  (define bytevector-int-native-set! (bytevector-signed-integer-setter int))
  (define bytevector-int-native-ref (bytevector-signed-integer-reffer int))
  (define bytevector-unsigned-int-native-set! (bytevector-unsigned-integer-setter unsigned-int))
  (define bytevector-unsigned-int-native-ref (bytevector-unsigned-integer-reffer unsigned-int))
  (define bytevector-long-native-set! (bytevector-signed-integer-setter long))
  (define bytevector-long-native-ref (bytevector-signed-integer-reffer long))
  (define bytevector-unsigned-long-native-set! (bytevector-unsigned-integer-setter unsigned-long))
  (define bytevector-unsigned-long-native-ref (bytevector-unsigned-integer-reffer unsigned-long))
  (define bytevector-intptr-native-set! (bytevector-signed-integer-setter intptr))
  (define bytevector-intptr-native-ref (bytevector-signed-integer-reffer intptr))
  (define bytevector-uintptr-native-set! (bytevector-unsigned-integer-setter uintptr))
  (define bytevector-uintptr-native-ref (bytevector-unsigned-integer-reffer uintptr))
  (define bytevector-ssize_t-native-set! (bytevector-signed-integer-setter ssize_t))
  (define bytevector-ssize_t-native-ref (bytevector-signed-integer-reffer ssize_t))
  (define bytevector-size_t-native-set! (bytevector-unsigned-integer-setter size_t))
  (define bytevector-size_t-native-ref (bytevector-unsigned-integer-reffer size_t))
  (define bytevector-ptrdiff_t-native-set! (bytevector-signed-integer-setter ptrdiff_t))
  (define bytevector-ptrdiff_t-native-ref (bytevector-signed-integer-reffer ptrdiff_t))

  (eval-when (compile load eval)
    (define flt-epsilon
      (* FLT_EPSILON_FRACTION
         (expt FLT_EPSILON_EXPONENT_BASE FLT_EPSILON_EXPONENT)))
    (when (inexact? flt-epsilon)
      (assertion-violation 'flt-epsilon
                           "expected flt-epsilon to be an exact rational"
                           flt-epsilon)))

  (eval-when (compile load eval)
    (define dbl-epsilon
      (* DBL_EPSILON_FRACTION
         (expt DBL_EPSILON_EXPONENT_BASE DBL_EPSILON_EXPONENT)))
    (when (inexact? dbl-epsilon)
      (assertion-violation 'dbl-epsilon
                           "expected dbl-epsilon to be an exact rational"
                           dbl-epsilon)))

  ;; Fun with π (though so far no roots or powers).
  (define pi-fraction (/ (string->number (substring PI_DIGITS 0 25))
                         (expt 10 24)))
  (define pi (exact->inexact pi-fraction))
  (define pi/2 (exact->inexact (/ pi-fraction 2)))
  (define pi/3 (exact->inexact (/ pi-fraction 3)))
  (define pi/4 (exact->inexact (/ pi-fraction 4)))
  (define pi/5 (exact->inexact (/ pi-fraction 5)))
  (define pi/6 (exact->inexact (/ pi-fraction 6)))
  (define /pi (exact->inexact (/ pi-fraction)))
  (define /pi/2 (exact->inexact (/ 2 pi-fraction)))
  (define /pi/3 (exact->inexact (/ 3 pi-fraction)))
  (define /pi/4 (exact->inexact (/ 4 pi-fraction)))
  (define /pi/5 (exact->inexact (/ 5 pi-fraction)))
  (define /pi/6 (exact->inexact (/ 6 pi-fraction)))
  (define 1/pi /pi)   ; A symbol beginning with a digit is non-standard.
  (define 2/pi /pi/2) ; A symbol beginning with a digit is non-standard.
  (define 3/pi /pi/3) ; A symbol beginning with a digit is non-standard.    
  (define 4/pi /pi/4) ; A symbol beginning with a digit is non-standard.    
  (define 5/pi /pi/5) ; A symbol beginning with a digit is non-standard.
  (define 6/pi /pi/6) ; A symbol beginning with a digit is non-standard.

  ) ;; end of library.
