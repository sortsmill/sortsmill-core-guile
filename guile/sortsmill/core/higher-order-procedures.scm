;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core higher-order-procedures)

  (export
   ;;
   ;; Some of these procedures were a whimsical aside from actual
   ;; programming, and perhaps are of little use. Even the useful ones
   ;; may be written in a whimsical fashion.
   ;;
   pick-arg      ;; Pick an argument.
   slice-args    ;; Pick a slice of arguments.
   rev-args      ;; Reverse arguments.
   rl-args       ;; Rotate arguments left.
   rr-args       ;; Rotate arguments right.
   dropl-args    ;; Drop arguments from the left.
   dropr-args    ;; Drop arguments from the right.
   predicate-or  ;; Combine predicates in ‘or’ fashion.
   predicate-and ;; Combine predicates in ‘and’ fashion.
   )

  (import (rnrs)
          (only (srfi :1) append-reverse! drop drop-right take)
          (srfi :42)
          (except (guile) error)
          (ice-9 format)
          (ice-9 match)
          (sortsmill core lists)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (define* (pick-arg n #:optional proc)
    "Return a procedure that picks a particular argument, and
optionally applies a procedure to it."
    (define (too-few-args args)
      (assertion-violation
       'pick-arg
       (format #f (_ "expected at least this many arguments: ~d") n)
       args))
    (if proc
        (case n
          [(1) (case-lambda
                 [(a . _) (proc a)]
                 [else (too-few-args (list))])]
          [(2) (case-lambda
                 [(a b . _) (proc b)]
                 [else (too-few-args (list))])]
          [(3) (case-lambda
                 [(a b c . _) (proc c)]
                 [else (too-few-args (list))])]
          [(4) (case-lambda
                 [(a b c d . _) (proc d)]
                 [else (too-few-args (list))])]
          [(5) (case-lambda
                 [(a b c d e . _) (proc e)]
                 [else (too-few-args (list))])]
          [(6) (case-lambda
                 [(a b c d e f . _) (proc f)]
                 [else (too-few-args (list))])]
          [(7) (case-lambda
                 [(a b c d e f g . _) (proc g)]
                 [else (too-few-args (list))])]
          [(8) (case-lambda
                 [(a b c d e f g h . _) (proc h)]
                 [else (too-few-args (list))])]
          [(9) (case-lambda
                 [(a b c d e f g h i . _) (proc i)]
                 [else (too-few-args (list))])]
          [else (lambda args
                  (proc (list-ref args (1- n))))])
        (case n
          [(1) (case-lambda
                 [(a . _) a]
                 [else (too-few-args (list))])]
          [(2) (case-lambda
                 [(a b . _) b]
                 [else (too-few-args (list))])]
          [(3) (case-lambda
                 [(a b c . _) c]
                 [else (too-few-args (list))])]
          [(4) (case-lambda
                 [(a b c d . _) d]
                 [else (too-few-args (list))])]
          [(5) (case-lambda
                 [(a b c d e . _) e]
                 [else (too-few-args (list))])]
          [(6) (case-lambda
                 [(a b c d e f . _) f]
                 [else (too-few-args (list))])]
          [(7) (case-lambda
                 [(a b c d e f g . _) g]
                 [else (too-few-args (list))])]
          [(8) (case-lambda
                 [(a b c d e f g h . _) h]
                 [else (too-few-args (list))])]
          [(9) (case-lambda
                 [(a b c d e f g h i . _) i]
                 [else (too-few-args (list))])]
          [else (lambda args
                  (list-ref args (1- n)))])))

  (define* (slice-args n count #:optional proc)
    "Return a procedure that picks a slice of arguments, and
optionally applies a procedure to it."
    (define (too-few-args args)
      (assertion-violation
       'slice-args
       (format #f (_ "expected at least this many arguments: ~d") n)
       args))
    (if proc
        (lambda args
          (let ([lst (drop args (1- n))])
            (apply proc
                   (if (< count (length lst)) (take lst count) lst))))
        (lambda args
          (let ([lst (drop args (1- n))])
            (if (< count (length lst)) (take lst count) lst)))))

  (define* (rev-args #:optional proc)
    "Return a procedure that calls another procedure with the
arguments reversed; or, if no procedure is specified, returns a list
of the arguments reversed."
    (if proc
        (case-lambda
          [(a b) (proc b a)]
          [(a) (proc a)]
          [() (proc)]
          [args (apply proc (reverse args))])
        (case-lambda
          [(a b) (list b a)]
          [(a) (list a)]
          [() (list)]
          [args (reverse args)])))

  (define rl-args
    (case-lambda
      [(n proc)
       (lambda args
         (let-values ([(a b) (split-before args n)])
           (apply proc (append b a))))]
      [(n-or-proc)
       (if (exact-integer? n-or-proc)
           (if (= n-or-proc 1)
               (lambda args
                 (reverse! (cons (car args) (reverse (cdr args)))))
               (lambda args
                 (let-values ([(a b) (split-before args n-or-proc)])
                   (append b a))))
           (lambda args
             (apply n-or-proc
                    (reverse! (cons (car args) (reverse (cdr args)))))))]
      [()
       (lambda args
         (reverse! (cons (car args) (reverse (cdr args)))))]))
  ;;
  (set-procedure-property! rl-args 'documentation
                           "Return a procedure that rotates its
arguments left by some number of places, and then either returns the
rotated arguments as a list or applies a procedure to them.")

  (define rr-args
    (case-lambda
      [(n proc)
       (lambda args
         (let-values ([(a b) (split-before args (- (length args) n))])
           (apply proc (append b a))))]
      [(n-or-proc)
       (if (exact-integer? n-or-proc)
           (if (= n-or-proc 1)
               (lambda args
                 (reverse! (cons (car args) (reverse (cdr args)))))
               (lambda args
                 (let-values ([(a b)
                               (split-before args (- (length args) n-or-proc))])
                   (append b a))))
           (lambda args
             (apply n-or-proc (reverse! (cons (car args)
                                              (reverse (cdr args)))))))]
      [()
       (lambda args
         (reverse! (cons (car args) (reverse (cdr args)))))]))
  ;;
  (set-procedure-property! rr-args 'documentation
                           "Return a procedure that rotates its
arguments right by some number of places, and then either returns the
rotated arguments as a list or applies a procedure to them.")

  (define* (dropl-args n #:optional proc)
    "Return a procedure drops some of its arguments from the left, and
either returns the remaining arguments as a list or applies a
procedure to them."
    (if proc
        (lambda args
          (apply proc (drop args n)))
        (lambda args
          (drop args n))))

  (define* (dropr-args n #:optional proc)
    "Return a procedure drops some of its arguments from the right, and
either returns the remaining arguments as a list or applies a
procedure to them."
    (if proc
        (lambda args
          (apply proc (drop-right args n)))
        (lambda args
          (drop-right args n))))

  (define (predicate-or . predicates)
    "Combine predicates in `or' fashion."
    (case-lambda
      [(x) (any?-ec (:list pred predicates) (pred x))]
      [args (any?-ec (:list pred predicates) (apply pred args))]))

  (define (predicate-and . predicates)
    "Combine predicates in `and' fashion."
    (case-lambda
      [(x) (every?-ec (:list pred predicates) (pred x))]
      [args (every?-ec (:list pred predicates) (apply pred args))]))

  ) ;; end of library.
