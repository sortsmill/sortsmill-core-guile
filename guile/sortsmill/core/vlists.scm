;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

;; Some of the code in this file is based on the implementation of
;; SRFI-1 for Guile, which is released under the LGPL:
;;
;; 	Copyright (C) 2001, 2002, 2003, 2004, 2005, 2006, 2009, 2010, 2011 Free Software Foundation, Inc.
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;; 
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Some parts from the reference implementation, which is
;;; Copyright (c) 1998, 1999 by Olin Shivers. You may do as you please with
;;; this code as long as you do not remove this copyright notice or
;;; hold me liable for its use.

;;; Author: Martin Grabmueller <mgrabmue@cs.tu-berlin.de>
;;; Date: 2001-06-06

(library (sortsmill core vlists)

  (export vlst?
          vlst-null
          vlst-null?
          vlst-car vlst-head
          vlst-cdr vlst-tail
          vlst-car+cdr
          vlst-cons
          vlst-cons*
          vlst-acons                 ; Analogous to SRFI-1 alist-cons.
          vlst-length
          vlst-ref
          vlst-first
          vlst-second
          vlst-third
          vlst-fourth
          vlst-fifth
          vlst-sixth
          vlst-seventh
          vlst-eighth
          vlst-ninth
          vlst-tenth
          vlst-caar
          vlst-cadr
          vlst-cdar
          vlst-cddr
          vlst-caaar
          vlst-caadr
          vlst-cadar
          vlst-caddr
          vlst-cdaar
          vlst-cdadr
          vlst-cddar
          vlst-cdddr
          vlst-caaaar
          vlst-caaadr
          vlst-caadar
          vlst-caaddr
          vlst-cadaar
          vlst-cadadr
          vlst-caddar
          vlst-cadddr
          vlst-cdaaar
          vlst-cdaadr
          vlst-cdadar
          vlst-cdaddr
          vlst-cddaar
          vlst-cddadr
          vlst-cdddar
          vlst-cddddr
          vlst-copy
          vlst-acopy                 ; Analogous to SRFI-1 alist-copy.
          vlst-reverse
          vlst-for-each
          vlst-map
          vlst-map-in-order
          vlst-reverse-map
          vlst-append-map
          vlst-filter-map
          vlst-count
          vlst-index                 ; Analogous to SRFI-1 list-index.
          vlst-filter
          vlst-remp
          vlst-remove
          vlst-delete
          vlst-adelete             ; Analogous to SRFI-1 alist-delete.
          vlst-remv vlst-delv
          vlst-remq vlst-delq
          vlst-partition
          vlst-delete-duplicates
          vlst-drop
          vlst-take
          vlst-drop-right
          vlst-take-right
          vlst-split-at
          vlst-last-pair
          vlst-last
          vlst-append
          vlst-append-reverse
          vlst-concatenate   ; Accepts either a list or vlst argument.
          vlst-fold-left     ; Like fold-left from (rnrs).
          vlst-fold-right    ; Like fold-right from (rnrs).
          vlst-fold          ; Like fold from (guile).
          vlst-reduce-left   ; A variation upon reduce from SRFI-1.
          vlst-reduce-right  ; Like reduce-right from SRFI-1.
          vlst-reduce        ; Like reduce from SRFI-1.
          vlst-pair-fold-left
          vlst-pair-fold-right
          vlst-pair-fold
          vlst-unfold        ; Like unfold from SRFI-1.
          vlst-unfold-right  ; Like unfold-right from SRFI-1.
          vlst-sort
          vlst-find
          vlst-find-tail
          vlst-drop-while
          vlst-take-while
          vlst-span
          vlst-break
          vlst-memp
          vlst-member ; Supports an optional arg, the way SRFI-1 `member' does.
          vlst-memv
          vlst-memq
          vlst-assp
          vlst-assoc ; Supports an optional arg, the way SRFI-1 `assoc' does.
          vlst-assv
          vlst-assq
          vlst-assoc-ref
          vlst-assv-ref
          vlst-assq-ref
          vlst-for-all vlst-every
          vlst-exists vlst-any
          vlst=                         ; Analogous to SRFI-1 ‘list=’.
          vlst-zip
          vlst-unzip1 vlst-unzip2 vlst-unzip3 vlst-unzip4 vlst-unzip5
          vlst-unzipn
          make-vlst
          vlst-tabulate
          vlst-iota
          vlst->list
          list->vlst
          vlst->vector
          vector->vlst
          vlst)

  (import (except (rnrs)
                  ;; We want to redefine the following with GOOPS;
                  ;; importing them from (rnrs) might cause trouble.
                  write equal?)
          (except (guile) error)
          (oop goops)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          guile_init_smcoreguile_vlsts
                          scm_vlst_p
                          scm_vlst_null_p
                          scm_vlst_car
                          scm_vlst_cdr
                          scm_vlst_cons
                          scm_vlst_cons_star
                          scm_vlst_acons
                          scm_vlst_length
                          scm_vlst_ref
                          scm_vlst_copy
                          scm_vlst_acopy
                          scm_vlst_reverse
                          scm_vlst_for_each
                          scm_vlst_map
                          scm_vlst_map_in_order
                          scm_vlst_reverse_map
                          scm_vlst_append_map
                          scm_vlst_filter_map
                          scm_vlst_count
                          scm_vlst_index
                          scm_vlst_filter
                          scm_vlst_remp
                          scm_vlst_remove
                          scm_vlst_remv
                          scm_vlst_remq
                          scm_vlst_delete
                          scm_vlst_adelete
                          scm_vlst_partition
                          scm_vlst_delete_duplicates
                          scm_vlst_drop
                          scm_vlst_take
                          scm_vlst_drop_right
                          scm_vlst_take_right
                          scm_vlst_split_at
                          scm_vlst_last_pair
                          scm_vlst_last
                          scm_vlst_append
                          scm_vlst_append_reverse
                          scm_vlst_concatenate
                          scm_vlst_fold_left
                          scm_vlst_fold_right
                          scm_vlst_fold
                          scm_vlst_pair_fold_left
                          scm_vlst_pair_fold_right
                          scm_vlst_pair_fold
                          scm_vlst_sort
                          scm_vlst_find
                          scm_vlst_find_tail
                          scm_vlst_drop_while
                          scm_vlst_take_while
                          scm_vlst_span
                          scm_vlst_break
                          scm_vlst_memp
                          scm_vlst_member
                          scm_vlst_memv
                          scm_vlst_memq
                          scm_vlst_assp
                          scm_vlst_assoc
                          scm_vlst_assv
                          scm_vlst_assq
                          scm_vlst_assoc_ref
                          scm_vlst_assv_ref
                          scm_vlst_assq_ref
                          scm_vlst_eq_p
                          scm_make_vlst
                          scm_vlst_tabulate
                          scm_vlst_to_list
                          scm_list_to_vlst
                          scm_vlst_to_vector
                          scm_vector_to_vlst
                          scm_vlst_1 scm_vlst_2 scm_vlst_3
                          scm_vlst_4 scm_vlst_5)
    ((pointer->procedure void guile_init_smcoreguile_vlsts '())))

  (define-generic write)
  (define-method (write [obj <vlst>] port)
    (display "#<vlst" port)
    (let loop ([lst obj])
      (unless (vlst-null? lst)
        (display " " port)
        (write (vlst-car lst) port)
        (loop (vlst-cdr lst))))
    (display ">" port))

  (define-generic equal?)
  (define-method (equal? [a <vlst>] [b <vlst>])
    (vlst-equal? a b))

  (define-scm-procedure (vlst? obj)
    "Is the argument a @code{vlst}?"
    scm_vlst_p)

  (define-scm-procedure (vlst-null? obj)
    "Is the argument a null @code{vlst}?"
    scm_vlst_null_p)

  (define-scm-procedure (vlst-car vlst)
    scm_vlst_car)

  ;; For similarity to (ice-9 vlist).
  (define vlst-head vlst-car)

  (define-scm-procedure (vlst-cdr vlst)
    scm_vlst_cdr)

  ;; For similarity to (ice-9 vlist).
  (define vlst-tail vlst-cdr)

  (define (vlst-car+cdr vlst)
    (values (vlst-car vlst) (vlst-cdr vlst)))

  (define-scm-procedure (vlst-cons obj vlst)
    scm_vlst_cons)

  (define-scm-procedure (vlst-cons* obj . rest)
    scm_vlst_cons_star)

  (define-scm-procedure (vlst-acons key value vlst)
    "Cons the (ordinary) pair @code{(key . value)} onto a vlst."
    scm_vlst_acons)

  (define-scm-procedure (vlst-length vlst)
    scm_vlst_length)

  (define*-scm-procedure (vlst-ref vlst i #:optional dflt)
    scm_vlst_ref)

  (define vlst-first vlst-car)
  (define vlst-second (lambda (vl) (vlst-ref vl 1)))
  (define vlst-third (lambda (vl) (vlst-ref vl 2)))
  (define vlst-fourth (lambda (vl) (vlst-ref vl 3)))
  (define vlst-fifth (lambda (vl) (vlst-ref vl 4)))
  (define vlst-sixth (lambda (vl) (vlst-ref vl 5)))
  (define vlst-seventh (lambda (vl) (vlst-ref vl 6)))
  (define vlst-eighth (lambda (vl) (vlst-ref vl 7)))
  (define vlst-ninth (lambda (vl) (vlst-ref vl 8)))
  (define vlst-tenth (lambda (vl) (vlst-ref vl 9)))

  (define (vlst-caar obj) (vlst-car (vlst-car obj)))
  (define vlst-cadr vlst-second)
  (define (vlst-cdar obj) (vlst-cdr (vlst-car obj)))
  (define (vlst-cddr obj) (vlst-cdr (vlst-cdr obj)))

  (define (vlst-caaar obj) (vlst-caar (vlst-car obj)))
  (define (vlst-caadr obj) (vlst-caar (vlst-cdr obj)))
  (define (vlst-cadar obj) (vlst-cadr (vlst-car obj)))
  (define vlst-caddr vlst-third)
  (define (vlst-cdaar obj) (vlst-cdar (vlst-car obj)))
  (define (vlst-cdadr obj) (vlst-cdar (vlst-cdr obj)))
  (define (vlst-cddar obj) (vlst-cddr (vlst-car obj)))
  (define (vlst-cdddr obj) (vlst-cddr (vlst-cdr obj)))

  (define (vlst-caaaar obj) (vlst-caaar (vlst-car obj)))
  (define (vlst-caaadr obj) (vlst-caaar (vlst-cdr obj)))
  (define (vlst-caadar obj) (vlst-caadr (vlst-car obj)))
  (define (vlst-caaddr obj) (vlst-caadr (vlst-cdr obj)))
  (define (vlst-cadaar obj) (vlst-cadar (vlst-car obj)))
  (define (vlst-cadadr obj) (vlst-cadar (vlst-cdr obj)))
  (define (vlst-caddar obj) (vlst-caddr (vlst-car obj)))
  (define vlst-cadddr vlst-fourth)
  (define (vlst-cdaaar obj) (vlst-cdaar (vlst-car obj)))
  (define (vlst-cdaadr obj) (vlst-cdaar (vlst-cdr obj)))
  (define (vlst-cdadar obj) (vlst-cdadr (vlst-car obj)))
  (define (vlst-cdaddr obj) (vlst-cdadr (vlst-cdr obj)))
  (define (vlst-cddaar obj) (vlst-cddar (vlst-car obj)))
  (define (vlst-cddadr obj) (vlst-cddar (vlst-cdr obj)))
  (define (vlst-cdddar obj) (vlst-cdddr (vlst-car obj)))
  (define (vlst-cddddr obj) (vlst-cdddr (vlst-cdr obj)))

  (define-scm-procedure (vlst-copy vlst)
    scm_vlst_copy)

  (define-scm-procedure (vlst-acopy vlst)
    "Copy an `association vlst', making copies of the association
pairs as well as the list spine."
    scm_vlst_acopy)

  (define-scm-procedure (vlst-reverse vlst)
    scm_vlst_reverse)

  (define vlst-for-each
    (let ([for-each1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                       scm_vlst_for_each)]
          [for-each2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                       scm_vlst_for_each)])
      (case-lambda
        [(proc vlst) (for-each1 proc vlst)]
        [(proc vlst1 vlst2) (for-each1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (for-each2 proc vlst1 more-vlsts)])))

  (define vlst-map
    (let ([map1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                  scm_vlst_map)]
          [map2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                  scm_vlst_map)])
      (case-lambda
        [(proc vlst) (map1 proc vlst)]
        [(proc vlst1 vlst2) (map1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (map2 proc vlst1 more-vlsts)])))

  (define vlst-map-in-order
    (let ([map1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                  scm_vlst_map_in_order)]
          [map2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                  scm_vlst_map_in_order)])
      (case-lambda
        [(proc vlst) (map1 proc vlst)]
        [(proc vlst1 vlst2) (map1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (map2 proc vlst1 more-vlsts)])))

  (define-scm-procedure (vlst-reverse-map proc vlst)
    scm_vlst_reverse_map)

  (define vlst-append-map
    (let ([map1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                  scm_vlst_append_map)]
          [map2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                  scm_vlst_append_map)])
      (case-lambda
        [(proc vlst) (map1 proc vlst)]
        [(proc vlst1 vlst2) (map1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (map2 proc vlst1 more-vlsts)])))

  (define vlst-filter-map
    (let ([map1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                  scm_vlst_filter_map)]
          [map2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                  scm_vlst_filter_map)])
      (case-lambda
        [(proc vlst) (map1 proc vlst)]
        [(proc vlst1 vlst2) (map1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (map2 proc vlst1 more-vlsts)])))

  (define vlst-count
    (let ([count1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                    scm_vlst_count)]
          [count2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                    scm_vlst_count)])
      (case-lambda
        [(proc vlst) (count1 proc vlst)]
        [(proc vlst1 vlst2) (count1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (count2 proc vlst1 more-vlsts)])))

  (define vlst-index
    (let ([index1 (lambda*-scm-procedure (proc vlst #:optional vlst2)
                    scm_vlst_index)]
          [index2 (lambda-scm-procedure (proc vlst1 more-vlsts)
                    scm_vlst_index)])
      (case-lambda
        [(proc vlst) (index1 proc vlst)]
        [(proc vlst1 vlst2) (index1 proc vlst1 vlst2)]
        [(proc vlst1 . more-vlsts) (index2 proc vlst1 more-vlsts)])))

  (define-scm-procedure (vlst-filter pred vlst)
    scm_vlst_filter)

  (define-scm-procedure (vlst-remp pred vlst)
    "A procedure for vlst analogous to R6RS @code{remp} and SRFI-1
@code{remove}."
    scm_vlst_remp)

  (define-scm-procedure (vlst-remove obj vlst)
    "A procedure for vlst analogous to R6RS @code{remove}. (For a
procedure analogous to SRFI-1 @code{remove}, see @code{vlst-remp}."
    scm_vlst_remove)

  (define*-scm-procedure (vlst-delete obj vlst #:optional pred)
    "A procedure for vlst analogous to SRFI-1 @code{delete}."
    scm_vlst_delete)

  (define*-scm-procedure (vlst-adelete key vlst #:optional pred)
    "A procedure for vlst analogous to SRFI-1 @code{alist-delete}."
    scm_vlst_adelete)

  (define-scm-procedure (vlst-remv obj vlst)
    "A procedure for vlst analogous to R6RS @code{remv}."
    scm_vlst_remv)

  (define vlst-delv vlst-remv)

  (define-scm-procedure (vlst-remq obj vlst)
    "A procedure for vlst analogous to R6RS @code{remq}."
    scm_vlst_remq)

  (define vlst-delq vlst-remq)

  (define-scm-procedure (vlst-partition pred vlst)
    scm_vlst_partition)

  (define*-scm-procedure (vlst-delete-duplicates vlst #:optional pred)
    scm_vlst_delete_duplicates)

  (define*-scm-procedure (vlst-drop vlst n #:optional allow-overflow?)
    scm_vlst_drop)

  (define*-scm-procedure (vlst-take vlst n #:optional allow-overflow?)
    scm_vlst_take)

  (define*-scm-procedure (vlst-drop-right vlst n #:optional allow-overflow?)
    scm_vlst_drop_right)

  (define*-scm-procedure (vlst-take-right vlst n #:optional allow-overflow?)
    scm_vlst_take_right)

  (define*-scm-procedure (vlst-split-at vlst n #:optional allow-overflow?)
    scm_vlst_split_at)

  (define-scm-procedure (vlst-last-pair vlst)
    "Return the last `pseudopair' (the sublist comprising only the
last element of the vlst)."
    scm_vlst_last_pair)

  (define-scm-procedure (vlst-last vlst)
    "Return the last element of the vlst."
    scm_vlst_last)

  (define-scm-procedure (vlst-append . vlsts)
    scm_vlst_append)

  (define-scm-procedure (vlst-append-reverse vl1 vl2)
    scm_vlst_append_reverse)

  (define-scm-procedure (vlst-concatenate vlsts)
    "A procedure analogous to SRFI-1 `concatenate'. It takes a list or
vlst as argument, the members of which should be of type vlst; and it
returns the concatenation of those members."
    scm_vlst_concatenate)

  (define vlst-fold-left
    (let ([fold-left1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold_left)]
          [fold-left2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_fold_left)])
      (case-lambda
        [(proc init vlst) (fold-left1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold-left1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold-left2 proc init vlst1 more-vlsts)])))

  (define vlst-fold-right
    (let ([fold-right1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold_right)]
          [fold-right2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_fold_right)])
      (case-lambda
        [(proc init vlst) (fold-right1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold-right1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold-right2 proc init vlst1 more-vlsts)])))

  (define vlst-fold
    (let ([fold1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold)]
          [fold2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_fold)])
      (case-lambda
        [(proc init vlst) (fold1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold2 proc init vlst1 more-vlsts)])))

  (define vlst-reduce-left
    (let ([fold-left1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold_left)])
      (lambda (proc left-identity vlst)
        "A procedure for vlst, analogous to SRFI-1 `reduce', except
that the called procedure expects arguments in the same order as does
the called procedure in R6RS `fold-left'."
        (if (vlst-null? vlst)
            left-identity
            (fold-left1 proc (vlst-car vlst) (vlst-cdr vlst))))))

  (define vlst-reduce-right
    (let ([fold-right1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold_right)])
      (lambda (proc right-identity vlst)
        "A procedure for vlst, analogous to SRFI-1 `reduce-right'."
        (if (vlst-null? vlst)
            right-identity
            (fold-right1 proc (vlst-car vlst) (vlst-cdr vlst))))))

  (define vlst-reduce
    (let ([fold1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_fold)])
      (lambda (proc right-identity vlst)
        "A procedure for vlst, analogous to SRFI-1 `reduce'."
        (if (vlst-null? vlst)
            right-identity
            (fold1 proc (vlst-car vlst) (vlst-cdr vlst))))))

  (define vlst-pair-fold-left
    (let ([fold-left1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_pair_fold_left)]
          [fold-left2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_pair_fold_left)])
      (case-lambda
        [(proc init vlst) (fold-left1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold-left1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold-left2 proc init vlst1 more-vlsts)])))

  (define vlst-pair-fold-right
    (let ([fold-right1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_pair_fold_right)]
          [fold-right2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_pair_fold_right)])
      (case-lambda
        [(proc init vlst) (fold-right1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold-right1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold-right2 proc init vlst1 more-vlsts)])))

  (define vlst-pair-fold
    (let ([fold1
           (lambda*-scm-procedure (proc init vlst #:optional vlst2)
             scm_vlst_pair_fold)]
          [fold2
           (lambda-scm-procedure (proc init vlst1 more-vlsts)
             scm_vlst_pair_fold)])
      (case-lambda
        [(proc init vlst) (fold1 proc init vlst)]
        [(proc init vlst1 vlst2) (fold1 proc init vlst1 vlst2)]
        [(proc init vlst1 . more-vlsts)
         (fold2 proc init vlst1 more-vlsts)])))

  (define* (vlst-unfold seed-means-done? seed-to-elem seed-to-seed seed
                        #:optional [seed-to-tail (lambda (x) vlst-null)])
    "`Unfold' to build a vlst, the way SRFI-1 @code{unfold} builds a
list."
    (let do-unfold ([sd seed])
      (if (seed-means-done? sd)
          (seed-to-tail sd)
          (vlst-cons (seed-to-elem sd)
                     (do-unfold (seed-to-seed sd))))))

  (define* (vlst-unfold-right seed-means-done? seed-to-elem seed-to-seed seed
                              #:optional [tail vlst-null])
    "`Unfold' to build a vlst, the way SRFI-1 @code{unfold-right}
builds a list."
    (let do-unfold-right ([prior tail]
                          [sd seed])
      (if (seed-means-done? sd)
          prior
          (do-unfold-right (vlst-cons (seed-to-elem sd) prior)
                           (seed-to-seed sd)))))

  (define-scm-procedure (vlst-sort before? vlst)
    "A stable sort analogous to R6RS list-sort and vector-sort."
    scm_vlst_sort)

  (define-scm-procedure (vlst-find pred vlst)
    scm_vlst_find)

  (define-scm-procedure (vlst-find-tail pred vlst)
    scm_vlst_find_tail)

  (define-scm-procedure (vlst-drop-while pred vlst)
    scm_vlst_drop_while)

  (define-scm-procedure (vlst-take-while pred vlst)
    scm_vlst_take_while)

  (define-scm-procedure (vlst-span pred vlst)
    scm_vlst_span)

  (define-scm-procedure (vlst-break pred vlst)
    scm_vlst_break)

  (define-scm-procedure (vlst-memp pred vlst)
    scm_vlst_memp)

  (define-scm-procedure (vlst-memv obj vlst)
    scm_vlst_memv)

  (define-scm-procedure (vlst-memq obj vlst)
    scm_vlst_memq)

  (define*-scm-procedure (vlst-member obj vlst #:optional =)
    scm_vlst_member)

  (define-scm-procedure (vlst-assp pred vlst)
    scm_vlst_assp)

  (define-scm-procedure (vlst-assv key vlst)
    scm_vlst_assv)

  (define-scm-procedure (vlst-assq key vlst)
    scm_vlst_assq)

  (define*-scm-procedure (vlst-assoc key vlst #:optional =)
    scm_vlst_assoc)

  (define-scm-procedure (vlst-assoc-ref vlst key)
    scm_vlst_assoc_ref)

  (define-scm-procedure (vlst-assv-ref vlst key)
    scm_vlst_assv_ref)

  (define-scm-procedure (vlst-assq-ref vlst key)
    scm_vlst_assq_ref)

  ;; vlst-any: based on the Guile implementation of SRFI-1 ‘any’.
  (define (vlst-any pred ls . lists)
    (if (null? lists)
        (vlst-any1 pred ls)
        (let lp ((lists (cons ls lists)))
          (cond ((vlst-any1 vlst-null? lists) #f)
                ((vlst-any1 vlst-null? (map vlst-cdr lists))
                 (apply pred (map vlst-car lists)))
                (else (or (apply pred (map vlst-car lists))
                          (lp (map vlst-cdr lists))))))))

  (define (vlst-any1 pred ls)
    (let lp ((ls ls))
      (cond ((vlst-null? ls) #f)
            ((vlst-null? (vlst-cdr ls)) (pred (vlst-car ls)))
            (else (or (pred (vlst-car ls))
                      (lp (vlst-cdr ls)))))))

  ;; vlst-every: based on the Guile implementation of SRFI-1 ‘every’.
  (define (vlst-every pred ls . lists)
    (if (null? lists)
        (vlst-every1 pred ls)
        (let lp ((lists (cons ls lists)))
          (cond ((vlst-any1 vlst-null? lists) #t)
                ((vlst-any1 vlst-null? (map vlst-cdr lists))
                 (apply pred (map vlst-car lists)))
                (else (and (apply pred (map vlst-car lists))
                           (lp (map vlst-cdr lists))))))))

  (define (vlst-every1 pred ls)
    (let lp ((ls ls))
      (cond ((vlst-null? ls) #t)
            ((vlst-null? (vlst-cdr ls)) (pred (vlst-car ls)))
            (else (and (pred (vlst-car ls))
                       (lp (vlst-cdr ls)))))))

  (define vlst-for-all vlst-every)      ; R⁶RS compatibility.
  (define vlst-exists vlst-any)         ; R⁶RS compatibility.

  (define-scm-procedure (vlst= pred . vlsts)
    scm_vlst_eq_p)

  (define (vlst-zip vl1 . the-rest)
    (apply vlst-map vlst vl1 the-rest))

  (define (vlst-unzip1 vl)
    (vlst-map vlst-first vl))

  (define (vlst-unzip2 vl)
    (values (vlst-map vlst-first vl)
            (vlst-map vlst-second vl)))

  (define (vlst-unzip3 vl)
    (values (vlst-map vlst-first vl)
            (vlst-map vlst-second vl)
            (vlst-map vlst-third vl)))

  (define (vlst-unzip4 vl)
    (values (vlst-map vlst-first vl)
            (vlst-map vlst-second vl)
            (vlst-map vlst-third vl)
            (vlst-map vlst-fourth vl)))

  (define (vlst-unzip5 vl)
    (values (vlst-map vlst-first vl)
            (vlst-map vlst-second vl)
            (vlst-map vlst-third vl)
            (vlst-map vlst-fourth vl)
            (vlst-map vlst-fifth vl)))

  (define (vlst-unzipn n vl)
    "Unzip a vlst into a given number of vlst values."
    (let ([ith-members
           (lambda (i) (vlst-map (lambda (x) (vlst-ref x i)) vl))])
      (apply values (vlst->list (vlst-tabulate n ith-members)))))

  (define*-scm-procedure (make-vlst n #:optional fill)
    scm_make_vlst)

  (define-scm-procedure (vlst-tabulate n proc)
    scm_vlst_tabulate)

  (define* (vlst-iota n #:optional (start 0) (step 1))
    ;; Unlike SRFI-1, we will require that n be an _exact_ integer.
    (unless (and (exact-integer? n) (not (negative? n)))
      (assertion-violation 'vlst-iota
                           _("expected a non-negative exact integer") n))
    (let loop ([reversed-result vlst-null]
               [i 0])
      (if (= i n)
          (vlst-reverse reversed-result)
          (loop (vlst-cons (+ start (* i step)) reversed-result)
                (1+ i)))))

  (define-scm-procedure (vlst->list vlst)
    scm_vlst_to_list)

  (define-scm-procedure (list->vlst lst)
    scm_list_to_vlst)

  (define-scm-procedure (vlst->vector vlst)
    scm_vlst_to_vector)

  (define-scm-procedure (vector->vlst vec)
    scm_vector_to_vlst)

  (define vlst
    (let ([vlst1 (lambda-scm-procedure (a) scm_vlst_1)]
          [vlst2 (lambda-scm-procedure (a b) scm_vlst_2)]
          [vlst3 (lambda-scm-procedure (a b c) scm_vlst_3)]
          [vlst4 (lambda-scm-procedure (a b c d) scm_vlst_4)]
          [vlst5 (lambda-scm-procedure (a b c d e) scm_vlst_5)])
      (case-lambda
        [() vlst-null]
        [(a) (vlst1 a)]
        [(a b) (vlst2 a b)]
        [(a b c) (vlst3 a b c)]
        [(a b c d) (vlst4 a b c d)]
        [(a b c d e) (vlst5 a b c d e)]
        [(. args) (list->vlst args)])))

  ) ;; end of library.
