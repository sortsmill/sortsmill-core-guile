;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core i18n)

  ;;
  ;; Gettext idioms.
  ;;

  (export
   ;; (define-gettext-for-domain name "domain") → (name string1) → string2
   ;;
   ;; Typically ‘name’ is set to ‘_’
   ;;
   define-gettext-for-domain

   ;; (define-contextual-gettext-for-domain name "domain") → (name context string1) → string2
   ;;
   ;; Typically ‘name’ is set to ‘C_’
   ;;
   define-contextual-gettext-for-domain

   ;; Reëxport @var{format} from @code{(ice-9 format)}, to help ensure
   ;; that localizers can use the full @var{format} functionality. We
   ;; do _not_ want the version of @var{format} that is just
   ;; @var{simple-format} by another name.
   format)

  (import (rnrs)
          (except (guile) error)
          (ice-9 format)
          (ice-9 i18n))

  (define-syntax define-gettext-for-domain
    (lambda (stx)
      (syntax-case stx ()
        [(_ name domain) (identifier? #'name)
         (let* ([domain-string (syntax->datum #'domain)]
                [doc-string
                 (format #f
                         "String localization for the ~s text domain: `@code{(~a \"string\")}'.  Use the xgettext flag `--keyword=_'."
                         domain-string
                         (syntax->datum #'name))])
           #`(define (name str)
               #,(datum->syntax stx doc-string)
               (gettext str #,(datum->syntax stx domain-string))))] )))

  (define-syntax define-contextual-gettext-for-domain
    (lambda (stx)
      (syntax-case stx ()
        [(_ name domain) (identifier? #'name)
         (let* ([domain-string (syntax->datum #'domain)]
                [doc-string
                 (format #f
                         "String localization for the ~s text domain, with context: `@code{(~a \"context\" \"string\")}'.  Use the xgettext flag `--keyword=C_:1c,2'."
                         domain-string
                         (syntax->datum #'name))])
           #`(define name
               (let ([context-separator (string #\eot)])
                 (lambda (context str)
                   #,(datum->syntax stx doc-string)
                   (let* ([original (string-append context context-separator str)]
                          [translation
                           (gettext original #,(datum->syntax stx domain-string))])
                     (if (string=? translation original)
                         str
                         translation))))))] )))

  ) ;; end of library.
