;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core linear-systems)

  (export
   sgausselimsolve f32gausselimsolve
   dgausselimsolve f64gausselimsolve
   stridiagsolve f32tridiagsolve
   dtridiagsolve f64tridiagsolve
   scyclictridiagsolve f32cyclictridiagsolve
   dcyclictridiagsolve f64cyclictridiagsolve
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_sgausselimsolve
                          scm_dgausselimsolve
                          scm_stridiagsolve
                          scm_dtridiagsolve
                          scm_scyclictridiagsolve
                          scm_dcyclictridiagsolve))

  ;;--------------------------------------------------------------------

  (define-scm-procedure (sgausselimsolve A B)
    "Solve a linear system by gaussian elimination with partial
pivoting. Single precision version. FIXME: Document this more
thoroughly."
    scm_sgausselimsolve)

  (define-scm-procedure (dgausselimsolve A B)
    "Solve a linear system by gaussian elimination with partial
pivoting. Double precision version. FIXME: Document this more
thoroughly."
    scm_dgausselimsolve)

  (define-scm-procedure (stridiagsolve subdiag diag superdiag B)
    "Solve a tridiagonal linear system by gaussian elimination with
partial pivoting. Single precision version. FIXME: Document this more
thoroughly."
    scm_stridiagsolve)

  (define-scm-procedure (dtridiagsolve subdiag diag superdiag B)
    "Solve a tridiagonal linear system by gaussian elimination with
partial pivoting. Double precision version. FIXME: Document this more
thoroughly."
    scm_dtridiagsolve)

  (define-scm-procedure (scyclictridiagsolve subdiag diag superdiag
                                             lower-left upper-right B)
    "Solve a cyclic tridiagonal linear system by gaussian elimination
with partial pivoting. Single precision version. FIXME: Document this
more thoroughly."
    scm_scyclictridiagsolve)

  (define-scm-procedure (dcyclictridiagsolve subdiag diag superdiag
                                             lower-left upper-right B)
    "Solve a cyclic tridiagonal linear system by gaussian elimination
with partial pivoting. Double precision version. FIXME: Document this
more thoroughly."
    scm_dcyclictridiagsolve)

  (define f32gausselimsolve sgausselimsolve)
  (define f64gausselimsolve dgausselimsolve)
  (define f32tridiagsolve stridiagsolve)
  (define f64tridiagsolve dtridiagsolve)
  (define f32cyclictridiagsolve scyclictridiagsolve)
  (define f64cyclictridiagsolve dcyclictridiagsolve)

  ;;--------------------------------------------------------------------

  ) ;; end of library.
