;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core atomic-ops)

  (export
   make-AO                 ;; (make-AO [integer]) → ao-object [unspecified type]
   AO-load                 ;; (AO-load ao) → integer
   AO-load-acquire-read    ;; (AO-load-acquire-read ao) → integer
   AO-store-release-write! ;; (AO-store-release-write! ao integer) → *unspecified*
   )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_make_AO
                          scm_AO_load
                          scm_AO_load_acquire_read
                          scm_AO_store_release_write_x
                          ))

  (define-scm-procedure (make-AO initial-value)
    "FIXME: Document this."
    scm_make_AO)

  (define-scm-procedure (AO-load ao)
    "FIXME: Document this."
    scm_AO_load)

  (define-scm-procedure (AO-load-acquire-read ao)
    "FIXME: Document this."
    scm_AO_load_acquire_read)

  (define-scm-procedure (AO-store-release-write! ao value)
    "FIXME: Document this."
    scm_AO_store_release_write_x)

  ) ;; end of library.
