;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core polynomials)

  (export exact-poly-div-mono
          exact-poly-gcd-mono
          exact-poly-squarefree-mono

          exact-poly-mindegree-mono
          f64poly-mindegree-mono dpoly-mindegree-mono
          f64poly-mindegree-spower dpoly-mindegree-spower

          exact-poly-changedegree-mono
          f64poly-changedegree-mono dpoly-changedegree-mono
          f64poly-changedegree-bern dpoly-changedegree-bern
          f64poly-changedegree-sbern dpoly-changedegree-sbern
          f64poly-changedegree-spower dpoly-changedegree-spower
          f64poly-degreechangeerrorbound-mono dpoly-degreechangeerrorbound-mono
          f64poly-degreechangeerrorbound-spower dpoly-degreechangeerrorbound-spower

          exact-poly-equalszero?
          f64poly-equalszero? dpoly-equalszero?

          exact-poly-equalsone-mono?
          f64poly-equalsone-mono? dpoly-equalsone-mono?
          f64poly-equalsone-bern? dpoly-equalsone-bern?
          f64poly-equalsone-sbern? dpoly-equalsone-sbern?
          f64poly-equalsone-spower? dpoly-equalsone-spower?

          exact-poly-neg
          f64poly-neg dpoly-neg

          exact-poly-add-mono
          f64poly-add-mono dpoly-add-mono
          f64poly-add-bern dpoly-add-bern
          f64poly-add-sbern dpoly-add-sbern
          f64poly-add-spower dpoly-add-spower

          exact-poly-sub-mono
          f64poly-sub-mono dpoly-sub-mono
          f64poly-sub-bern dpoly-sub-bern
          f64poly-sub-sbern dpoly-sub-sbern
          f64poly-sub-spower dpoly-sub-spower

          exact-poly-scalarmul
          f64poly-scalarmul dpoly-scalarmul

          exact-poly-mul-mono
          f64poly-mul-mono dpoly-mul-mono
          f64poly-mul-bern dpoly-mul-bern
          f64poly-mul-sbern dpoly-mul-sbern
          f64poly-mul-spower dpoly-mul-spower

          f64poly-crossproduct-mono dpoly-crossproduct-mono
          f64poly-crossproduct-bern dpoly-crossproduct-bern
          f64poly-crossproduct-sbern dpoly-crossproduct-sbern
          f64poly-crossproduct-spower dpoly-crossproduct-spower

          f64poly-inflections-mono dpoly-inflections-mono
          f64poly-inflections-bern dpoly-inflections-bern
          f64poly-inflections-sbern dpoly-inflections-sbern
          f64poly-inflections-spower dpoly-inflections-spower

          f64poly-flatten-spower dpoly-flatten-mono
          f64poly-flatten-spower dpoly-flatten-bern
          f64poly-flatten-spower dpoly-flatten-sbern
          f64poly-flatten-spower dpoly-flatten-spower

          f64poly-direction-bernsv dpoly-direction-bernsv
          f64poly-direction-berndc dpoly-direction-berndc
          f64poly-direction-sbernsv dpoly-direction-sbernsv
          f64poly-direction-sberndc dpoly-direction-sberndc
          f64poly-direction-spower dpoly-direction-spower

          exact-poly-deriv-mono
          f64poly-deriv-mono dpoly-deriv-mono
          f64poly-deriv-bern dpoly-deriv-bern
          f64poly-deriv-sbern dpoly-deriv-sbern
          f64poly-deriv-spower dpoly-deriv-spower

          poly-criticalpoints-mono ;; Mixed exact and double arith.
          f64poly-criticalpoints-bernsv dpoly-criticalpoints-bernsv
          f64poly-criticalpoints-berndc dpoly-criticalpoints-berndc
          f64poly-criticalpoints-sbernsv dpoly-criticalpoints-sbernsv
          f64poly-criticalpoints-sberndc dpoly-criticalpoints-sberndc
          f64poly-criticalpoints-spower dpoly-criticalpoints-spower

          exact-poly-eval-mono
          f64poly-eval-mono dpoly-eval-mono
          f64poly-eval-bernsv dpoly-eval-bernsv
          f64poly-eval-berndc dpoly-eval-berndc
          f64poly-eval-sbernsv dpoly-eval-sbernsv
          f64poly-eval-sberndc dpoly-eval-sberndc
          f64poly-eval-spower dpoly-eval-spower

          exact-poly-compose-mono
          f64poly-compose-mono dpoly-compose-mono
          f64poly-compose-berndc dpoly-compose-berndc
          f64poly-compose-bernh dpoly-compose-bernh
          f64poly-compose-sberndc dpoly-compose-sberndc
          f64poly-compose-sbernh dpoly-compose-sbernh
          f64poly-compose-spower dpoly-compose-spower

          exact-poly-portion-mono
          f64poly-portion-mono dpoly-portion-mono
          f64poly-portion-berndc dpoly-portion-berndc
          f64poly-portion-bernh dpoly-portion-bernh
          f64poly-portion-sberndc dpoly-portion-sberndc
          f64poly-portion-sbernh dpoly-portion-sbernh
          f64poly-portion-spower dpoly-portion-spower
          f64poly-subdiv-bern dpoly-subdiv-bern
          f64poly-subdiv-sbern dpoly-subdiv-sbern

          budan-0_1
          isolate-roots-of-square-free-polynomial
          f64poly-findroots dpoly-findroots
          f64poly-findroots-bernsv dpoly-findroots-bernsv
          f64poly-findroots-berndc dpoly-findroots-berndc
          f64poly-findroots-sbernsv dpoly-findroots-sbernsv
          f64poly-findroots-sberndc dpoly-findroots-sberndc
          f64poly-findroots-spower dpoly-findroots-spower

          exact-poly-implicitize-mono
          f64poly-implicitize-mono dpoly-implicitize-mono
          f64poly-implicitize-bern dpoly-implicitize-bern
          f64poly-implicitize-sbern dpoly-implicitize-sbern
          f64poly-implicitize-spower dpoly-implicitize-spower

          exact-poly-plugintoimplicit-mono
          f64poly-plugintoimplicit-mono dpoly-plugintoimplicit-mono
          f64poly-plugintoimplicit-bern dpoly-plugintoimplicit-bern
          f64poly-plugintoimplicit-sbern dpoly-plugintoimplicit-sbern
          f64poly-plugintoimplicit-spower dpoly-plugintoimplicit-spower

          exact-poly-invertdegree1-mono
          f64poly-invert-mono dpoly-invert-mono

          intersect-implicit-with-parametric
          poly-findintersections-mono
          f64poly-findintersections-bern dpoly-findintersections-bern
;;;          f32poly-findintersections-bernsv spoly-findintersections-bernsv
;;;          f32poly-findintersections-berndc spoly-findintersections-berndc

          exact-poly-mono-to-bern
          exact-poly-bern-to-mono
          exact-poly-mono-to-sbern
          exact-poly-sbern-to-mono
          exact-poly-bern-to-sbern
          exact-poly-sbern-to-bern
          exact-poly-sbern-to-spower
          exact-poly-spower-to-sbern
          exact-poly-bern-to-spower
          exact-poly-spower-to-bern
          exact-poly-mono-to-spower
          exact-poly-spower-to-mono

          f64poly-mono-to-bern dpoly-mono-to-bern
          f64poly-bern-to-mono dpoly-bern-to-mono
          f64poly-mono-to-sbern dpoly-mono-to-sbern
          f64poly-sbern-to-mono dpoly-sbern-to-mono
          f64poly-bern-to-sbern dpoly-bern-to-sbern
          f64poly-sbern-to-bern dpoly-sbern-to-bern
          f64poly-sbern-to-spower dpoly-sbern-to-spower
          f64poly-spower-to-sbern dpoly-spower-to-sbern
          f64poly-bern-to-spower dpoly-bern-to-spower
          f64poly-spower-to-bern dpoly-spower-to-bern
          f64poly-mono-to-spower dpoly-mono-to-spower
          f64poly-spower-to-mono dpoly-spower-to-mono
          )

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_exact_poly_div_mono
                          scm_exact_poly_gcd_mono
                          scm_exact_poly_squarefree_mono

                          scm_exact_poly_mindegree_mono
                          scm_dpoly_mindegree_mono
                          scm_dpoly_mindegree_spower

                          scm_exact_poly_changedegree_mono
                          scm_dpoly_changedegree_mono
                          scm_dpoly_changedegree_bern
                          scm_dpoly_changedegree_sbern
                          scm_dpoly_changedegree_spower
                          scm_dpoly_degreechangeerrorbound_mono
                          scm_dpoly_degreechangeerrorbound_spower

                          scm_exact_poly_equalszero_p
                          scm_dpoly_equalszero_p

                          scm_exact_poly_equalsone_mono_p
                          scm_dpoly_equalsone_mono_p
                          scm_dpoly_equalsone_bern_p
                          scm_dpoly_equalsone_sbern_p
                          scm_dpoly_equalsone_spower_p

                          scm_exact_poly_neg
                          scm_dpoly_neg

                          scm_exact_poly_add_mono
                          scm_dpoly_add_mono
                          scm_dpoly_add_bern
                          scm_dpoly_add_sbern
                          scm_dpoly_add_spower

                          scm_exact_poly_sub_mono
                          scm_dpoly_sub_mono
                          scm_dpoly_sub_bern
                          scm_dpoly_sub_sbern
                          scm_dpoly_sub_spower

                          scm_exact_poly_scalarmul
                          scm_dpoly_scalarmul

                          scm_exact_poly_mul_mono
                          scm_dpoly_mul_mono
                          scm_dpoly_mul_bern
                          scm_dpoly_mul_sbern
                          scm_dpoly_mul_spower

                          scm_dpoly_crossproduct_mono
                          scm_dpoly_crossproduct_bern
                          scm_dpoly_crossproduct_sbern
                          scm_dpoly_crossproduct_spower

                          scm_dpoly_inflections_mono
                          scm_dpoly_inflections_bern
                          scm_dpoly_inflections_sbern
                          scm_dpoly_inflections_spower

                          scm_dpoly_flatten_mono
                          scm_dpoly_flatten_bern
                          scm_dpoly_flatten_sbern
                          scm_dpoly_flatten_spower

                          scm_dpoly_direction_bernsv
                          scm_dpoly_direction_berndc
                          scm_dpoly_direction_sbernsv
                          scm_dpoly_direction_sberndc
                          scm_dpoly_direction_spower

                          scm_exact_poly_deriv_mono
                          scm_dpoly_deriv_mono
                          scm_dpoly_deriv_bern
                          scm_dpoly_deriv_sbern
                          scm_dpoly_deriv_spower

                          scm_poly_criticalpoints_mono
                          scm_dpoly_criticalpoints_bernsv
                          scm_dpoly_criticalpoints_berndc
                          scm_dpoly_criticalpoints_sbernsv
                          scm_dpoly_criticalpoints_sberndc
                          scm_dpoly_criticalpoints_spower

                          scm_exact_poly_eval_mono
                          scm_dpoly_eval_mono
                          scm_dpoly_eval_bernsv
                          scm_dpoly_eval_berndc
                          scm_dpoly_eval_sbernsv
                          scm_dpoly_eval_sberndc
                          scm_dpoly_eval_spower

                          scm_exact_poly_compose_mono
                          scm_dpoly_compose_mono
                          scm_dpoly_compose_berndc
                          scm_dpoly_compose_bernh
                          scm_dpoly_compose_sberndc
                          scm_dpoly_compose_sbernh
                          scm_dpoly_compose_spower

                          scm_exact_poly_portion_mono
                          scm_dpoly_portion_mono
                          scm_dpoly_portion_berndc
                          scm_dpoly_portion_bernh
                          scm_dpoly_portion_sberndc
                          scm_dpoly_portion_sbernh
                          scm_dpoly_portion_spower
                          scm_dpoly_subdiv_bern
                          scm_dpoly_subdiv_sbern

                          scm_budan_0_1
                          scm_isolate_roots_of_square_free_polynomial
                          scm_dpoly_findroots
                          scm_dpoly_findroots_bernsv
                          scm_dpoly_findroots_berndc
                          scm_dpoly_findroots_sbernsv
                          scm_dpoly_findroots_sberndc
                          scm_dpoly_findroots_spower

                          scm_exact_poly_implicitize_mono
                          scm_dpoly_implicitize_mono
                          scm_dpoly_implicitize_bern
                          scm_dpoly_implicitize_sbern
                          scm_dpoly_implicitize_spower

                          scm_exact_poly_plugintoimplicit_mono
                          scm_dpoly_plugintoimplicit_mono
                          scm_dpoly_plugintoimplicit_bern
                          scm_dpoly_plugintoimplicit_sbern
                          scm_dpoly_plugintoimplicit_spower

                          scm_exact_poly_invertdegree1_mono
                          scm_dpoly_invert_mono

                          scm_intersect_implicit_with_parametric
                          scm_poly_findintersections_mono
                          scm_dpoly_findintersections_bern
;;;                          scm_spoly_findintersections_bernsv
;;;                          scm_spoly_findintersections_berndc

                          scm_exact_poly_mono_to_bern
                          scm_exact_poly_bern_to_mono
                          scm_exact_poly_mono_to_sbern
                          scm_exact_poly_sbern_to_mono
                          scm_exact_poly_bern_to_sbern
                          scm_exact_poly_sbern_to_bern
                          scm_exact_poly_sbern_to_spower
                          scm_exact_poly_spower_to_sbern
                          scm_exact_poly_bern_to_spower
                          scm_exact_poly_spower_to_bern
                          scm_exact_poly_mono_to_spower
                          scm_exact_poly_spower_to_mono

                          scm_dpoly_mono_to_bern
                          scm_dpoly_bern_to_mono
                          scm_dpoly_mono_to_sbern
                          scm_dpoly_sbern_to_mono
                          scm_dpoly_bern_to_sbern
                          scm_dpoly_sbern_to_bern
                          scm_dpoly_sbern_to_spower
                          scm_dpoly_spower_to_sbern
                          scm_dpoly_bern_to_spower
                          scm_dpoly_spower_to_bern
                          scm_dpoly_mono_to_spower
                          scm_dpoly_spower_to_mono
                          ))

  (define-scm-procedure (exact-poly-mindegree-mono poly)
    "Return the minimum degree (the actual degree) of the given
polynomial. (For a zero polynomial, the return value is zero.)"
    scm_exact_poly_mindegree_mono)

  (define-scm-procedure (dpoly-mindegree-mono poly)
    "Return the minimum degree (the actual degree) of the given
polynomial. (For a zero polynomial, the return value is zero.)"
    scm_dpoly_mindegree_mono)

  (define-scm-procedure (dpoly-mindegree-spower poly)
    "Return the minimum degree (the actual degree) of the given
polynomial. (For a zero polynomial, the return value is zero.)"
    scm_dpoly_mindegree_spower)

  (define f64poly-mindegree-mono dpoly-mindegree-mono)
  (define f64poly-mindegree-spower dpoly-mindegree-spower)

  (define-scm-procedure (exact-poly-changedegree-mono poly new-degree)
    "Raise degree by appending zeros, or lower degree by truncating
terms. (WARNING: truncating nonzero terms in the monomial basis is not
equivalent to other methods of lowering degree, such as truncating
terms in symmetric power basis.)"
    scm_exact_poly_changedegree_mono)

  (define-scm-procedure (dpoly-changedegree-mono poly new-degree)
    scm_dpoly_changedegree_mono)

  (define-scm-procedure (dpoly-changedegree-bern poly new-degree)
    scm_dpoly_changedegree_bern)

  (define-scm-procedure (dpoly-changedegree-sbern poly new-degree)
    scm_dpoly_changedegree_sbern)

  (define-scm-procedure (dpoly-changedegree-spower poly new-degree)
    scm_dpoly_changedegree_spower)

  (define-scm-procedure (dpoly-degreechangeerrorbound-mono poly new-degree)
    scm_dpoly_degreechangeerrorbound_mono)

  (define-scm-procedure (dpoly-degreechangeerrorbound-spower poly new-degree)
    scm_dpoly_degreechangeerrorbound_spower)

  (define f64poly-changedegree-mono dpoly-changedegree-mono)
  (define f64poly-changedegree-bern dpoly-changedegree-bern)
  (define f64poly-changedegree-sbern dpoly-changedegree-sbern)
  (define f64poly-changedegree-spower dpoly-changedegree-spower)
  (define f64poly-degreechangeerrorbound-mono dpoly-degreechangeerrorbound-mono)
  (define f64poly-degreechangeerrorbound-spower dpoly-degreechangeerrorbound-spower)

  (define-scm-procedure (exact-poly-add-mono poly1 poly2)
    "Add two real polynomials in monomial basis, using exact arithmetic."
    scm_exact_poly_add_mono)

  (define-scm-procedure (dpoly-add-mono poly1 poly2)
    "Add two real polynomials in monomial basis, using double
precision arithmetic."
    scm_dpoly_add_mono)

  (define-scm-procedure (dpoly-add-bern poly1 poly2)
    scm_dpoly_add_bern)

  (define-scm-procedure (dpoly-add-sbern poly1 poly2)
    scm_dpoly_add_sbern)

  (define-scm-procedure (dpoly-add-spower poly1 poly2)
    scm_dpoly_add_spower)

  (define f64poly-add-mono dpoly-add-mono)
  (define f64poly-add-bern dpoly-add-bern)
  (define f64poly-add-sbern dpoly-add-sbern)
  (define f64poly-add-spower dpoly-add-spower)

  (define-scm-procedure (exact-poly-sub-mono poly1 poly2)
    "Given two real polynomials in monomial basis, subtract the second
from the first, using exact arithmetic."
    scm_exact_poly_sub_mono)

  (define-scm-procedure (dpoly-sub-mono poly1 poly2)
    "Given two real polynomials in monomial basis, subtract the second
from the first, using double precision arithmetic."
    scm_dpoly_sub_mono)

  (define-scm-procedure (dpoly-sub-bern poly1 poly2)
    scm_dpoly_sub_bern)

  (define-scm-procedure (dpoly-sub-sbern poly1 poly2)
    scm_dpoly_sub_sbern)

  (define-scm-procedure (dpoly-sub-spower poly1 poly2)
    scm_dpoly_sub_spower)

  (define f64poly-sub-mono dpoly-sub-mono)
  (define f64poly-sub-bern dpoly-sub-bern)
  (define f64poly-sub-sbern dpoly-sub-sbern)
  (define f64poly-sub-spower dpoly-sub-spower)

  (define-scm-procedure (exact-poly-neg poly)
    "Negate a real polynomial (in any basis), using exact arithmetic."
    scm_exact_poly_neg)

  (define-scm-procedure (dpoly-neg poly)
    "Negate a real polynomial (in any basis), using double precision
arithmetic."
    scm_dpoly_neg)

  (define f64poly-neg dpoly-neg)

  (define-scm-procedure (exact-poly-equalszero? poly)
    "Is the given real polynomial (in any basis) equal to the
additive identity?"
    scm_exact_poly_equalszero_p)

  (define-scm-procedure (dpoly-equalszero? poly)
    "Is the given real polynomial (in any basis) equal to the
additive identity?"
    scm_dpoly_equalszero_p)

  (define f64poly-equalszero? dpoly-equalszero?)

  (define-scm-procedure (exact-poly-compose-mono f g)
    "Compose two real polynomials in monomial basis, using exact
arithmetic. In particular, @code{(exact-poly-compose-mono f g)}
returns the coefficients of h(t) = f(g(t))."
    scm_exact_poly_compose_mono)

  (define-scm-procedure (dpoly-compose-mono f g)
    scm_dpoly_compose_mono)

  (define-scm-procedure (dpoly-compose-berndc f g)
    scm_dpoly_compose_berndc)

  (define-scm-procedure (dpoly-compose-bernh f g)
    scm_dpoly_compose_bernh)

  (define-scm-procedure (dpoly-compose-sberndc f g)
    scm_dpoly_compose_sberndc)

  (define-scm-procedure (dpoly-compose-sbernh f g)
    scm_dpoly_compose_sbernh)

  (define-scm-procedure (dpoly-compose-spower f g)
    scm_dpoly_compose_spower)

  (define f64poly-compose-mono dpoly-compose-mono)
  (define f64poly-compose-berndc dpoly-compose-berndc)
  (define f64poly-compose-bernh dpoly-compose-bernh)
  (define f64poly-compose-sberndc dpoly-compose-sberndc)
  (define f64poly-compose-sbernh dpoly-compose-sbernh)
  (define f64poly-compose-spower dpoly-compose-spower)

  (define-scm-procedure (exact-poly-deriv-mono poly)
    "Given a polynomial in monomial basis, return its formal
derivative, using exact arithmetic."
    scm_exact_poly_deriv_mono)

  (define-scm-procedure (dpoly-deriv-mono poly)
    scm_dpoly_deriv_mono)

  (define-scm-procedure (dpoly-deriv-bern poly)
    scm_dpoly_deriv_bern)

  (define-scm-procedure (dpoly-deriv-sbern poly)
    scm_dpoly_deriv_sbern)

  (define-scm-procedure (dpoly-deriv-spower poly)
    scm_dpoly_deriv_spower)

  (define f64poly-deriv-mono dpoly-deriv-mono)
  (define f64poly-deriv-bern dpoly-deriv-bern)
  (define f64poly-deriv-sbern dpoly-deriv-sbern)
  (define f64poly-deriv-spower dpoly-deriv-spower)

  (define*-scm-procedure (poly-criticalpoints-mono poly #:optional a b)
    scm_poly_criticalpoints_mono)

  (define*-scm-procedure (dpoly-criticalpoints-bernsv poly #:optional a b)
    scm_dpoly_criticalpoints_bernsv)

  (define*-scm-procedure (dpoly-criticalpoints-berndc poly #:optional a b)
    scm_dpoly_criticalpoints_berndc)

  (define*-scm-procedure (dpoly-criticalpoints-sbernsv poly #:optional a b)
    scm_dpoly_criticalpoints_sbernsv)

  (define*-scm-procedure (dpoly-criticalpoints-sberndc poly #:optional a b)
    scm_dpoly_criticalpoints_sberndc)

  (define*-scm-procedure (dpoly-criticalpoints-spower poly #:optional a b)
    scm_dpoly_criticalpoints_spower)

  (define f64poly-criticalpoints-bernsv dpoly-criticalpoints-bernsv)
  (define f64poly-criticalpoints-berndc dpoly-criticalpoints-berndc)
  (define f64poly-criticalpoints-sbernsv dpoly-criticalpoints-sbernsv)
  (define f64poly-criticalpoints-sberndc dpoly-criticalpoints-sberndc)
  (define f64poly-criticalpoints-spower dpoly-criticalpoints-spower)

  (define-scm-procedure (exact-poly-div-mono poly1 poly2)
    "Divide @var{poly1} by @var{poly2}, which are given in monomial
basis. Use exact arithmetic. Return two results, the quotient and the
remainder, but raise an exception if @var{poly2} is identically zero."
    scm_exact_poly_div_mono)

  (define-scm-procedure (exact-poly-gcd-mono poly1 poly2)
    "Return the (monic, if non-zero) greatest common divisor two real
polynomials in monomial basis, using exact arithmetic. If both
polynomials are zero then the gcd also is zero."
    scm_exact_poly_gcd_mono)

  (define-scm-procedure (exact-poly-squarefree-mono poly)
    "Return two values:

        * the square-free decomposition as a vector of monic
polynomials indexed by their multiplicity

        * the lead coefficient.

If the input polynomial is zero the return values are @code{#(#(0))}
and @code{0}, respectively."
    scm_exact_poly_squarefree_mono)

  (define-scm-procedure (exact-poly-eval-mono poly t)
    "Evaluate a real polynomial in monomial basis, using exact
arithmetic."
    scm_exact_poly_eval_mono)

  (define-scm-procedure (dpoly-eval-mono poly t)
    "Evaluate a real polynomial in monomial basis, using
double-precision arithmetic."
    scm_dpoly_eval_mono)

  (define-scm-procedure (dpoly-eval-bernsv poly t)
    scm_dpoly_eval_bernsv)

  (define-scm-procedure (dpoly-eval-berndc poly t)
    scm_dpoly_eval_berndc)

  (define-scm-procedure (dpoly-eval-sbernsv poly t)
    scm_dpoly_eval_sbernsv)

  (define-scm-procedure (dpoly-eval-sberndc poly t)
    scm_dpoly_eval_sberndc)

  (define-scm-procedure (dpoly-eval-spower poly t)
    scm_dpoly_eval_spower)

  (define f64poly-eval-mono dpoly-eval-mono)
  (define f64poly-eval-bernsv dpoly-eval-bernsv)
  (define f64poly-eval-berndc dpoly-eval-berndc)
  (define f64poly-eval-sbernsv dpoly-eval-sbernsv)
  (define f64poly-eval-sberndc dpoly-eval-sberndc)
  (define f64poly-eval-spower dpoly-eval-spower)

  (define-scm-procedure (exact-poly-mul-mono poly1 poly2)
    "Multiply two real polynomials in monomial basis, using exact
arithmetic."
    scm_exact_poly_mul_mono)

  (define-scm-procedure (dpoly-mul-mono poly1 poly2)
    scm_dpoly_mul_mono)

  (define-scm-procedure (dpoly-mul-bern poly1 poly2)
    scm_dpoly_mul_bern)

  (define-scm-procedure (dpoly-mul-sbern poly1 poly2)
    scm_dpoly_mul_sbern)

  (define-scm-procedure (dpoly-mul-spower poly1 poly2)
    scm_dpoly_mul_spower)

  (define f64poly-mul-mono dpoly-mul-mono)
  (define f64poly-mul-bern dpoly-mul-bern)
  (define f64poly-mul-sbern dpoly-mul-sbern)
  (define f64poly-mul-spower dpoly-mul-spower)

  (define-scm-procedure (exact-poly-equalsone-mono? poly)
    "Is the given real polynomial, in monomial basis, equal to the
multiplicative identity?"
    scm_exact_poly_equalsone_mono_p)

  (define-scm-procedure (dpoly-equalsone-mono? poly)
    scm_dpoly_equalsone_mono_p)

  (define-scm-procedure (dpoly-equalsone-bern? poly)
    scm_dpoly_equalsone_bern_p)

  (define-scm-procedure (dpoly-equalsone-sbern? poly)
    scm_dpoly_equalsone_sbern_p)

  (define-scm-procedure (dpoly-equalsone-spower? poly)
    scm_dpoly_equalsone_spower_p)

  (define f64poly-equalsone-mono? dpoly-equalsone-mono?)
  (define f64poly-equalsone-bern? dpoly-equalsone-bern?)
  (define f64poly-equalsone-sbern? dpoly-equalsone-sbern?)
  (define f64poly-equalsone-spower? dpoly-equalsone-spower?)

  (define-scm-procedure (dpoly-crossproduct-mono ax ay bx by)
    scm_dpoly_crossproduct_mono)

  (define-scm-procedure (dpoly-crossproduct-bern ax ay bx by)
    scm_dpoly_crossproduct_bern)

  (define-scm-procedure (dpoly-crossproduct-sbern ax ay bx by)
    scm_dpoly_crossproduct_sbern)

  (define-scm-procedure (dpoly-crossproduct-spower ax ay bx by)
    scm_dpoly_crossproduct_spower)

  (define f64poly-crossproduct-mono dpoly-crossproduct-mono)
  (define f64poly-crossproduct-bern dpoly-crossproduct-bern)
  (define f64poly-crossproduct-sbern dpoly-crossproduct-sbern)
  (define f64poly-crossproduct-spower dpoly-crossproduct-spower)

  (define-scm-procedure (dpoly-inflections-mono ax ay)
    scm_dpoly_inflections_mono)

  (define-scm-procedure (dpoly-inflections-bern ax ay)
    scm_dpoly_inflections_bern)

  (define-scm-procedure (dpoly-inflections-sbern ax ay)
    scm_dpoly_inflections_sbern)

  (define-scm-procedure (dpoly-inflections-spower ax ay)
    scm_dpoly_inflections_spower)

  (define f64poly-inflections-mono dpoly-inflections-mono)
  (define f64poly-inflections-bern dpoly-inflections-bern)
  (define f64poly-inflections-sbern dpoly-inflections-sbern)
  (define f64poly-inflections-spower dpoly-inflections-spower)

  (define-scm-procedure (dpoly-flatten-mono ax ay absolute_tolerance
                                            relative_tolerance
                                            max_num_segments)
    scm_dpoly_flatten_mono)

  (define-scm-procedure (dpoly-flatten-bern ax ay absolute_tolerance
                                            relative_tolerance
                                            max_num_segments)
    scm_dpoly_flatten_bern)

  (define-scm-procedure (dpoly-flatten-sbern ax ay absolute_tolerance
                                             relative_tolerance
                                             max_num_segments)
    scm_dpoly_flatten_sbern)

  (define-scm-procedure (dpoly-flatten-spower ax ay absolute_tolerance
                                              relative_tolerance
                                              max_num_segments)
    scm_dpoly_flatten_spower)

  (define f64poly-flatten-spower dpoly-flatten-mono)
  (define f64poly-flatten-spower dpoly-flatten-bern)
  (define f64poly-flatten-spower dpoly-flatten-sbern)
  (define f64poly-flatten-spower dpoly-flatten-spower)

  (define-scm-procedure (dpoly-direction-bernsv ax ay t)
    scm_dpoly_direction_bernsv)

  (define-scm-procedure (dpoly-direction-berndc ax ay t)
    scm_dpoly_direction_berndc)

  (define-scm-procedure (dpoly-direction-sbernsv ax ay t)
    scm_dpoly_direction_sbernsv)

  (define-scm-procedure (dpoly-direction-sberndc ax ay t)
    scm_dpoly_direction_sberndc)

  (define-scm-procedure (dpoly-direction-spower ax ay t)
    scm_dpoly_direction_spower)

  (define f64poly-direction-bernsv dpoly-direction-bernsv)
  (define f64poly-direction-berndc dpoly-direction-berndc)
  (define f64poly-direction-sbernsv dpoly-direction-sbernsv)
  (define f64poly-direction-sberndc dpoly-direction-sberndc)
  (define f64poly-direction-spower dpoly-direction-spower)

  (define-scm-procedure (exact-poly-portion-mono poly a b)
    "Subdivide out a portion of the given polynomial on [0,1], using
exact arithmetic, and obtaining a polynomial on [0,1], of the same
degree. The polynomial is given in the ordinary monomial basis."
    scm_exact_poly_portion_mono)

  (define-scm-procedure (dpoly-portion-mono poly a b)
    scm_dpoly_portion_mono)

  (define-scm-procedure (dpoly-portion-berndc poly a b)
    scm_dpoly_portion_berndc)

  (define-scm-procedure (dpoly-portion-bernh poly a b)
    scm_dpoly_portion_bernh)

  (define-scm-procedure (dpoly-portion-sberndc poly a b)
    scm_dpoly_portion_sberndc)

  (define-scm-procedure (dpoly-portion-sbernh poly a b)
    scm_dpoly_portion_sbernh)

  (define-scm-procedure (dpoly-portion-spower poly a b)
    scm_dpoly_portion_spower)

  (define-scm-procedure (dpoly-subdiv-bern poly t)
    scm_dpoly_subdiv_bern)

  (define-scm-procedure (dpoly-subdiv-sbern poly t)
    scm_dpoly_subdiv_sbern)

  (define f64poly-portion-mono dpoly-portion-mono)
  (define f64poly-portion-berndc dpoly-portion-berndc)
  (define f64poly-portion-bernh dpoly-portion-bernh)
  (define f64poly-portion-sberndc dpoly-portion-sberndc)
  (define f64poly-portion-sbernh dpoly-portion-sbernh)
  (define f64poly-portion-spower dpoly-portion-spower)
  (define f64poly-subdiv-bern dpoly-subdiv-bern)
  (define f64poly-subdiv-sbern dpoly-subdiv-sbern)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (exact-poly-scalarmul poly r)
    "Multiply a real polynomial by a scalar, using exact
arithmetic.

 The same procedure works for any polynomial basis."
    scm_exact_poly_scalarmul)

  (define-scm-procedure (dpoly-scalarmul poly r)
    "Multiply a real polynomial by a scalar, using double precision
arithmetic.

 The same procedure works for any polynomial basis."
    scm_dpoly_scalarmul)

  (define f64poly-scalarmul dpoly-scalarmul)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (budan-0_1 poly)
    "Budan’s 0-1 roots test."
    scm_budan_0_1)

  (define-scm-procedure (isolate-roots-of-square-free-polynomial poly a b)
    "Isolate the roots, between @var{a} and @var{b}, of a square-free
polynomial. Returns a vector, each of whose entries is either an exact
root or a pair of numbers representing an open interval containing a
root. The entries appear in order (FIXME: phrase this better)."
    scm_isolate_roots_of_square_free_polynomial)

  (define*-scm-procedure (dpoly-findroots mono-poly a b #:optional eval-poly)
    "FIXME: Document this."
    scm_dpoly_findroots)

  (define-scm-procedure (dpoly-findroots-bernsv mono-poly a b)
    scm_dpoly_findroots_bernsv)

  (define-scm-procedure (dpoly-findroots-berndc mono-poly a b)
    scm_dpoly_findroots_berndc)

  (define-scm-procedure (dpoly-findroots-sbernsv mono-poly a b)
    scm_dpoly_findroots_sbernsv)

  (define-scm-procedure (dpoly-findroots-sberndc mono-poly a b)
    scm_dpoly_findroots_sberndc)

  (define-scm-procedure (dpoly-findroots-spower mono-poly a b)
    scm_dpoly_findroots_spower)

  (define f64poly-findroots dpoly-findroots)
  (define f64poly-findroots-bernsv dpoly-findroots-bernsv)
  (define f64poly-findroots-berndc dpoly-findroots-berndc)
  (define f64poly-findroots-sbernsv dpoly-findroots-sbernsv)
  (define f64poly-findroots-sberndc dpoly-findroots-sberndc)
  (define f64poly-findroots-spower dpoly-findroots-spower)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (exact-poly-implicitize-mono a b)
    "FIXME: Document this."
    scm_exact_poly_implicitize_mono)

  (define-scm-procedure (dpoly-implicitize-mono a b)
    "FIXME: Document this."
    scm_dpoly_implicitize_mono)

  (define-scm-procedure (dpoly-implicitize-bern a b)
    "FIXME: Document this."
    scm_dpoly_implicitize_bern)

  (define-scm-procedure (dpoly-implicitize-sbern a b)
    "FIXME: Document this."
    scm_dpoly_implicitize_sbern)

  (define-scm-procedure (dpoly-implicitize-spower a b)
    "FIXME: Document this."
    scm_dpoly_implicitize_spower)

  (define f64poly-implicitize-mono dpoly-implicitize-mono)
  (define f64poly-implicitize-bern dpoly-implicitize-bern)
  (define f64poly-implicitize-sbern dpoly-implicitize-sbern)
  (define f64poly-implicitize-spower dpoly-implicitize-spower)

  (define-scm-procedure (exact-poly-plugintoimplicit-mono implicit-eq x y)
    "FIXME: Document this."
    scm_exact_poly_plugintoimplicit_mono)

  (define-scm-procedure (dpoly-plugintoimplicit-mono implicit-eq x y)
    "FIXME: Document this."
    scm_dpoly_plugintoimplicit_mono)

  (define-scm-procedure (dpoly-plugintoimplicit-bern implicit-eq x y)
    "FIXME: Document this."
    scm_dpoly_plugintoimplicit_bern)

  (define-scm-procedure (dpoly-plugintoimplicit-sbern implicit-eq x y)
    "FIXME: Document this."
    scm_dpoly_plugintoimplicit_sbern)

  (define-scm-procedure (dpoly-plugintoimplicit-spower implicit-eq x y)
    "FIXME: Document this."
    scm_dpoly_plugintoimplicit_spower)

  (define f64poly-plugintoimplicit-mono dpoly-plugintoimplicit-mono)
  (define f64poly-plugintoimplicit-bern dpoly-plugintoimplicit-bern)
  (define f64poly-plugintoimplicit-sbern dpoly-plugintoimplicit-sbern)
  (define f64poly-plugintoimplicit-spower dpoly-plugintoimplicit-spower)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (exact-poly-invertdegree1-mono poly-x poly-y x y)
    "FIXME: Document this."
    scm_exact_poly_invertdegree1_mono)

  (define-scm-procedure (dpoly-invert-mono poly-x poly-y x y
                                           ta tb absolute-tolerance)
    "FIXME: Document this."
    scm_dpoly_invert_mono)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (intersect-implicit-with-parametric implicit-a
                                                            bx by t0 t1)
    scm_intersect_implicit_with_parametric)

  (define*-scm-procedure (poly-findintersections-mono-inner
                          ax ay implicit-a ta0 ta1
                          bx by implicit-b tb0 tb1
                          #:optional tolerance)
    scm_poly_findintersections_mono)

  (define poly-findintersections-mono
    (case-lambda
      [(ax ay bx by)
       (poly-findintersections-mono-inner ax ay #f 0 1
                                          bx by #f 0 1)]
      [(ax ay bx by tolerance)
       (poly-findintersections-mono-inner ax ay #f 0 1
                                          bx by #f 0 1 tolerance)]
      [(ax ay implicit-a bx by implicit-b)
       (poly-findintersections-mono-inner ax ay implicit-a 0 1
                                          bx by implicit-b 0 1)]
      [(ax ay implicit-a bx by implicit-b tolerance)
       (poly-findintersections-mono-inner ax ay implicit-a 0 1
                                          bx by implicit-b 0 1 tolerance)]
      [(ax ay implicit-a ta0 ta1 bx by implicit-b tb0 tb1)
       (poly-findintersections-mono-inner ax ay implicit-a ta0 ta1
                                          bx by implicit-b tb0 tb1)]
      [(ax ay implicit-a ta0 ta1 bx by implicit-b tb0 tb1 tolerance)
       (poly-findintersections-mono-inner ax ay implicit-a ta0 ta1
                                          bx by implicit-b tb0 tb1 tolerance)]))

  (define*-scm-procedure (dpoly-findintersections-bern-inner
                          ax ay implicit-a ta0 ta1
                          bx by implicit-b tb0 tb1
                          #:optional tolerance)
    scm_dpoly_findintersections_bern)

  (define dpoly-findintersections-bern
    (case-lambda
      [(ax ay bx by)
       (dpoly-findintersections-bern-inner ax ay #f 0 1
                                           bx by #f 0 1)]
      [(ax ay bx by tolerance)
       (dpoly-findintersections-bern-inner ax ay #f 0 1
                                           bx by #f 0 1 tolerance)]
      [(ax ay implicit-a bx by implicit-b)
       (dpoly-findintersections-bern-inner ax ay implicit-a 0 1
                                           bx by implicit-b 0 1)]
      [(ax ay implicit-a bx by implicit-b tolerance)
       (dpoly-findintersections-bern-inner ax ay implicit-a 0 1
                                           bx by implicit-b 0 1 tolerance)]
      [(ax ay implicit-a ta0 ta1 bx by implicit-b tb0 tb1)
       (dpoly-findintersections-bern-inner ax ay implicit-a ta0 ta1
                                           bx by implicit-b tb0 tb1)]
      [(ax ay implicit-a ta0 ta1 bx by implicit-b tb0 tb1 tolerance)
       (dpoly-findintersections-bern-inner ax ay implicit-a ta0 ta1
                                           bx by implicit-b tb0 tb1 tolerance)]))

  (define f64poly-findintersections-bern dpoly-findintersections-bern)

;;;  (define*-scm-procedure (spoly-findintersections-bernsv ax ay bx by
;;;                                                         #:optional tolerance)
;;;    "FIXME: Document this."
;;;    scm_spoly_findintersections_bernsv)
;;;
;;;  (define*-scm-procedure (spoly-findintersections-berndc ax ay bx by
;;;                                                         #:optional tolerance)
;;;    "FIXME: Document this."
;;;    scm_spoly_findintersections_berndc)
;;;
;;;  (define f32poly-findintersections-bernsv spoly-findintersections-bernsv)
;;;  (define f32poly-findintersections-berndc spoly-findintersections-berndc)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (exact-poly-mono-to-bern a)
    "Tranform polynomial coefficients from monomial to Bernstein
basis."
    scm_exact_poly_mono_to_bern)

  (define-scm-procedure (exact-poly-bern-to-mono a)
    "Tranform polynomial coefficients from Bernstein to monomial
basis."
    scm_exact_poly_bern_to_mono)

  (define-scm-procedure (exact-poly-mono-to-sbern a)
    "Tranform polynomial coefficients from monomial to scaled
Bernstein basis."
    scm_exact_poly_mono_to_sbern)

  (define-scm-procedure (exact-poly-sbern-to-mono a)
    "Tranform polynomial coefficients from scaled Bernstein to
monomial basis."
    scm_exact_poly_sbern_to_mono)

  (define-scm-procedure (exact-poly-bern-to-sbern a)
    "Tranform polynomial coefficients from Bernstein to scaled
Bernstein basis."
    scm_exact_poly_bern_to_sbern)

  (define-scm-procedure (exact-poly-sbern-to-bern a)
    "Tranform polynomial coefficients from scaled Bernstein to
Bernstein basis."
    scm_exact_poly_sbern_to_bern)

  (define-scm-procedure (exact-poly-sbern-to-spower a)
    "Tranform polynomial coefficients from scaled Bernstein to
symmetric power basis."
    scm_exact_poly_sbern_to_spower)

  (define-scm-procedure (exact-poly-spower-to-sbern a)
    "Tranform polynomial coefficients from symmetric power to scaled
Bernstein basis."
    scm_exact_poly_spower_to_sbern)

  (define-scm-procedure (exact-poly-bern-to-spower a)
    "Tranform polynomial coefficients from Bernstein to symmetric
power basis."
    scm_exact_poly_bern_to_spower)

  (define-scm-procedure (exact-poly-spower-to-bern a)
    "Tranform polynomial coefficients from symmetric power to
Bernstein basis."
    scm_exact_poly_spower_to_bern)

  (define-scm-procedure (exact-poly-mono-to-spower a)
    "Tranform polynomial coefficients from monomial to symmetric power
basis."
    scm_exact_poly_mono_to_spower)

  (define-scm-procedure (exact-poly-spower-to-mono a)
    "Tranform polynomial coefficients from symmetric power to monomial
basis."
    scm_exact_poly_spower_to_mono)

  ;;--------------------------------------------------------------------

  (define-scm-procedure (dpoly-mono-to-bern a)
    "Tranform polynomial coefficients from monomial to Bernstein
basis."
    scm_dpoly_mono_to_bern)

  (define-scm-procedure (dpoly-bern-to-mono a)
    "Tranform polynomial coefficients from Bernstein to monomial
basis."
    scm_dpoly_bern_to_mono)

  (define-scm-procedure (dpoly-mono-to-sbern a)
    "Tranform polynomial coefficients from monomial to scaled
Bernstein basis."
    scm_dpoly_mono_to_sbern)

  (define-scm-procedure (dpoly-sbern-to-mono a)
    "Tranform polynomial coefficients from scaled Bernstein to
monomial basis."
    scm_dpoly_sbern_to_mono)

  (define-scm-procedure (dpoly-bern-to-sbern a)
    "Tranform polynomial coefficients from Bernstein to scaled
Bernstein basis."
    scm_dpoly_bern_to_sbern)

  (define-scm-procedure (dpoly-sbern-to-bern a)
    "Tranform polynomial coefficients from scaled Bernstein to
Bernstein basis."
    scm_dpoly_sbern_to_bern)

  (define-scm-procedure (dpoly-sbern-to-spower a)
    "Tranform polynomial coefficients from scaled Bernstein to
symmetric power basis."
    scm_dpoly_sbern_to_spower)

  (define-scm-procedure (dpoly-spower-to-sbern a)
    "Tranform polynomial coefficients from symmetric power to scaled
Bernstein basis."
    scm_dpoly_spower_to_sbern)

  (define-scm-procedure (dpoly-bern-to-spower a)
    "Tranform polynomial coefficients from Bernstein to symmetric
power basis."
    scm_dpoly_bern_to_spower)

  (define-scm-procedure (dpoly-spower-to-bern a)
    "Tranform polynomial coefficients from symmetric power to
Bernstein basis."
    scm_dpoly_spower_to_bern)

  (define-scm-procedure (dpoly-mono-to-spower a)
    "Tranform polynomial coefficients from monomial to symmetric power
basis."
    scm_dpoly_mono_to_spower)

  (define-scm-procedure (dpoly-spower-to-mono a)
    "Tranform polynomial coefficients from symmetric power to monomial
basis."
    scm_dpoly_spower_to_mono)

  (define f64poly-mono-to-bern dpoly-mono-to-bern)
  (define f64poly-bern-to-mono dpoly-bern-to-mono)
  (define f64poly-mono-to-sbern dpoly-mono-to-sbern)
  (define f64poly-sbern-to-mono dpoly-sbern-to-mono)
  (define f64poly-bern-to-sbern dpoly-bern-to-sbern)
  (define f64poly-sbern-to-bern dpoly-sbern-to-bern)
  (define f64poly-sbern-to-spower dpoly-sbern-to-spower)
  (define f64poly-spower-to-sbern dpoly-spower-to-sbern)
  (define f64poly-bern-to-spower dpoly-bern-to-spower)
  (define f64poly-spower-to-bern dpoly-spower-to-bern)
  (define f64poly-mono-to-spower dpoly-mono-to-spower)
  (define f64poly-spower-to-mono dpoly-spower-to-mono)

  ;;--------------------------------------------------------------------

  ) ;; end of library.
