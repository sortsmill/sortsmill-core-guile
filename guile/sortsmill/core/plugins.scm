;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2017 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core plugins)
  (export load-plugin-module)
  (import (guile))

  (define (string->module-name s)
    (map string->symbol
         (filter (lambda (x) (not (string=? x "")))
                 (string-split s #\space))))

  (define (join-module-name module-name)
    (let ([module-name (if (string? module-name)
                           (string->module-name module-name)
                           module-name)])
      (string-join (map symbol->string module-name) "_" 'infix)))

  (define (plugin-name module-name)
    (string-append "scm-module-" (join-module-name module-name)))

  (define (init-function-name module-name)
    (define (name-char->initfunc-char c)
      (cond [(char=? c #\_)       c]
            [(char-alphabetic? c) c]
            [(char-numeric? c)    c]
            [else               #\_]))
    (string-map name-char->initfunc-char
                (string-append "scm_init_"
                               (join-module-name module-name)
                               "_module")))

  (define (load-plugin-module module-name)
    "
The expression

   (load-plugin-module '(aaa bbb_1 ccc.2))

or equivalently

   (load-plugin-module \"aaa bbb_1 ccc.2\")

will load the module

   (aaa bbb_1 ccc.2)

from a dynamically loaded plugin named, for instance (on Elf systems),

   scm-module-aaa_bbb_1_ccc.2.so

The initialization function in this case must have the prototype

   void scm_init_aaa_bbb_1_ccc_2_module (void);

It is the job of the initialization function to create the actual module
(though it can do more than just that, if you want).

A good place to put plugin modules is in the Guile extensions directory,
whose path is returned by a command such as

   pkg-config --variable=extensiondir guile-2.0

(Note that the environment variable GUILE_SYSTEM_EXTENSIONS_PATH
can be used to override the default path for Guile extensions.)
"
    (load-extension (plugin-name module-name)
                    (init-function-name module-name)))

  ) ;; end of library.
