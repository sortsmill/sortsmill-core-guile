;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core hobby)

  (export
   ;; (hobby-tensions-to-control-points bool p0 p3 dir0 dir1 tension0 tension1) → p1, p2
   ;;
   ;; where p0, p1, p2, p3, dir0, dir1 are complex numbers, and
   ;; tension0, tension1 are reals. If the boolean argument is true
   ;; then tensions may be increased to give a ‘nicer’ curve.
   hobby-tensions-to-control-points

   ;; (control-points-to-hobby-tensions p0 p1 p2 p3) → tension0, tension3
   ;;
   ;; where p0, p1, p2, p3 are complex numbers, and tension0, tension1
   ;; are reals.
   control-points-to-hobby-tensions

   ;; FIXME: Document this.
   hobby-guess-directions

   ;; FIXME: Document this.
   hobby-periodic-guess-directions

   ;; FIXME: Document these.
   &hobby-guide-tokens-status
   make-hobby-guide-tokens-status-condition
   hobby-guide-tokens-status-condition?
   hobby-guide-tokens-status-tokens
   hobby-guide-tokens-status-info
   hobby-guide-tokens-status-bad-token-index

   ;; FIXME: Document these.
   &hobby-guide-tokens-message
   make-hobby-guide-tokens-message-condition
   hobby-guide-tokens-message-condition?
   hobby-guide-tokens-message

   ;; FIXME: Document this.
   solve-hobby-guide-tokens

   ;; FIXME: Document these
   enforce-hobby-guide-bounds?
   hobby-guide->f64vector
   f64vector->hobby-guide
   hobby-guide-style-flag
   hobby-guide-style
   solve-hobby-guide)

  (import (rnrs)
          (only (srfi :1) split-at)
          (srfi :4)
          (ice-9 match)
          (except (guile) error)
          (system foreign)
          (sortsmill core machine)
          (sortsmill core uniform-vectors)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_hobby_tensions_to_control_points
                          scm_control_points_to_hobby_tensions
                          scm_hobby_guess_directions
                          scm_hobby_periodic_guess_directions
                          scm_solve_hobby_guide_tokens))

  ;;--------------------------------------------------------------------

  (define-scm-procedure (hobby-tensions-to-control-points perhaps-increase-tensions?
                                                          p0 dir0 tension0
                                                          tension3 dir3 p3)
    "FIXME: Document this."
    scm_hobby_tensions_to_control_points)

  (define-scm-procedure (control-points-to-hobby-tensions p0 p1 p2 p3)
    "FIXME: Document this."
    scm_control_points_to_hobby_tensions)

  (define-scm-procedure (hobby-guess-directions start_dir end_dir
                                                start_curl end_curl
                                                a_tensions b_tensions points)
    "FIXME: Document this."
    scm_hobby_guess_directions)

  (define-scm-procedure (hobby-periodic-guess-directions a_tensions b_tensions
                                                         points)
    "FIXME: Document this."
    scm_hobby_periodic_guess_directions)

  ;;--------------------------------------------------------------------

  (define-condition-type &hobby-guide-tokens-status &condition
    make-hobby-guide-tokens-status-condition
    hobby-guide-tokens-status-condition?
    (tokens hobby-guide-tokens-status-tokens)
    (info hobby-guide-tokens-status-info)
    (bad-token-index hobby-guide-tokens-status-bad-token-index))

  (define-condition-type &hobby-guide-tokens-message &condition
    make-hobby-guide-tokens-message-condition
    hobby-guide-tokens-message-condition?
    (tokens-message hobby-guide-tokens-message))

  (define-scm-procedure (solve-hobby-guide-tokens input-tokens)
    "FIXME: Document this."
    scm_solve_hobby_guide_tokens)

  (define HOBBY_GUIDE_POINT 0)
  (define HOBBY_GUIDE_CYCLE 1)
  (define HOBBY_GUIDE_TENSION 2)
  (define HOBBY_GUIDE_ATLEAST 3)
  (define HOBBY_GUIDE_DIR 4)
  (define HOBBY_GUIDE_CURL 5)
  (define HOBBY_GUIDE_CTRL 6)

  (define HOBBY_GUIDE_TOKEN_ERROR -1)
  (define HOBBY_GUIDE_SOLUTION_NOT_FOUND -2)

  (define MF-INFINITY 4095.99998) ;; plain.mf’s ‘infinity’.
  (define MF-INFINITY-EPS (* MF-INFINITY dbl-epsilon))

  ;; If true, then enforce that tensions be at least 0.75 and curls at
  ;; least zero.
  (define enforce-hobby-guide-bounds? (make-parameter #t))

  (define (hobby-guide->f64vector guide)
    "FIXME: Document this."
    (define (list->complex obj)
      (if (complex? obj)
          obj
          (make-rectangular (car obj) (cadr obj))))
    (define (make-point p)
      (f64vector HOBBY_GUIDE_POINT (real-part p) (imag-part p)))
    (define (make-cycle)
      (f64vector HOBBY_GUIDE_CYCLE 0 0))
    (define (make-tension t1 t2)
      (if (and (enforce-hobby-guide-bounds?)
               (< (min t1 t2) 0.75))
          (assertion-violation
           'hobby-guide->f64vector
           (_ "a tension is less than 0.75 and bounds are being enforced")
           (min t1 t2))
          (f64vector HOBBY_GUIDE_TENSION t1 t2)))
    (define (make-atleast t1 t2)
      (if (and (enforce-hobby-guide-bounds?)
               (< (min t1 t2) 0.75))
          (assertion-violation
           'hobby-guide->f64vector
           (_ "a tension is less than 0.75 and bounds are being enforced")
           (min t1 t2))
          (f64vector HOBBY_GUIDE_ATLEAST t1 t2)))
    (define (make-straight-join)
      ;;
      ;; NOTE: Metafont implements a straight join by the equivalent of
      ;;
      ;;       (f64vector HOBBY_GUIDE_CURL 1 0
      ;;                  HOBBY_GUIDE_TENSION 1 1
      ;;                  HOBBY_GUIDE_CURL 1 0))
      ;;
      ;; But an infinite tension should be easier to manipulate,
      ;; because it is purely a tension rather than a compound
      ;; construct.
      ;;
      (f64vector HOBBY_GUIDE_TENSION +inf.0 +inf.0))
    (define (make-dir p)
      (f64vector HOBBY_GUIDE_DIR (real-part p) (imag-part p)))
    (define (make-curl c)
      (if (and (enforce-hobby-guide-bounds?) (negative? c))
          (assertion-violation
           'hobby-guide->f64vector
           (_ "a curl is less than zero and bounds are being enforced")
           c)
          (f64vector HOBBY_GUIDE_CURL c 0)))
    (define (make-ctrl p1 p2)
      (f64vector HOBBY_GUIDE_CTRL (real-part p1) (imag-part p1)
                 HOBBY_GUIDE_CTRL (real-part p2) (imag-part p2)))
    (define (convert-notation obj)
      (match obj
        [(? (lambda (x) (eq? '.. x)) s) (make-tension 1 1)]
        [(? (lambda (x) (eq? '... x)) s) (make-atleast 1 1)]
        [(? (lambda (x) (eq? '-- x)) s) (make-straight-join)]
        [(? (lambda (x) (eq? '--- x)) s) (make-tension MF-INFINITY MF-INFINITY)]
        [((? real? x) (? real? y)) (make-point (make-rectangular x y))]
        [(? complex? p) (make-point p)]
        [(or 'cycle ('cycle)) (make-cycle)]
        [('tension (? real? t)) (make-tension t t)]
        [(or ('tension (? real? t1) (? real? t2))
             ('tension (? real? t1) 'and (? real? t2)))
         (make-tension t1 t2)]
        [(or ('atleast (? real? t))
             ('tension 'atleast (? real? t)))
         (make-atleast t t)]
        [(or ('atleast (? real? t1) (? real? t2))
             ('tension 'atleast (? real? t1) (? real? t2))
             ('atleast (? real? t1) 'and (? real? t2))
             ('tension 'atleast (? real? t1) 'and (? real? t2)))
         (make-atleast t1 t2)]
        [(or ('dir (? complex? d))
             ('direction (? complex? d)))
         (make-dir d)]
        [(or ('dir ((? real? x) (? real? y)))
             ('direction ((? real? x) (? real? y))))
         (make-dir (make-rectangular x y))]
        [('angle (? real? t)) (make-dir (make-polar 1 (/ (* pi t) 180)))]
        [(or 'left '(left)) (make-dir (make-rectangular -1 0))]
        [(or 'up '(up)) (make-dir (make-rectangular 0 1))]
        [(or 'right '(right)) (make-dir (make-rectangular 1 0))]
        [(or 'down '(down)) (make-dir (make-rectangular 0 -1))]
        [('curl c) (make-curl c)]
        [(or ('controls (or (? complex? p1)
                            (and ((? real? _) (? real? _)) p1))
                        (or (? complex? p2)
                            (and ((? real? _) (? real? _)) p2)))
             ('controls (or (? complex? p1)
                            (and ((? real? _) (? real? _)) p1))
                        'and
                        (or (? complex? p2)
                            (and ((? real? _) (? real? _)) p2))))
         (make-ctrl (list->complex p1) (list->complex p2))]
        [entry (assertion-violation 'hobby-guide->f64vector
                                    (_ "unrecognized Hobby guide entry")
                                    entry)]))
    (f64vector-concatenate (map convert-notation guide)))

  (define-enumeration hobby-guide-style-flag
    (complex-points
     complex-directions
     angles
     direction-names
     tension-shortcuts
     long-names
     use-and
     use-lists)
    hobby-guide-style)

  (define* (f64vector->hobby-guide vec #:key [style (hobby-guide-style)])
    "FIXME: Document this."
    (define complex-points?
      (enum-set-member? (hobby-guide-style-flag complex-points) style))
    (define complex-directions?
      (enum-set-member? (hobby-guide-style-flag complex-directions) style))
    (define angles?
      (enum-set-member? (hobby-guide-style-flag angles) style))
    (define direction-names?
      (enum-set-member? (hobby-guide-style-flag direction-names) style))
    (define tension-shortcuts?
      (enum-set-member? (hobby-guide-style-flag tension-shortcuts) style))
    (define use-lists?
      (enum-set-member? (hobby-guide-style-flag use-lists) style))
    (define dir-notation
      (if (enum-set-member? (hobby-guide-style-flag long-names) style)
          'direction
          'dir))
    (define atleast-notation
      (if (enum-set-member? (hobby-guide-style-flag long-names) style)
          '(tension atleast)
          '(atleast)))
    (define and-notation
      (if (enum-set-member? (hobby-guide-style-flag use-and) style)
          '(and)
          '()))
    (define (make-point-value x y)
      (if complex-points?
          (make-rectangular x y)
          `(,x ,y)))
    (define (make-dir x y)
      (cond [(and direction-names? (= x -1) (zero? y))
             (if use-lists? '(left) 'left)]
            [(and direction-names? (zero? x) (= y +1))
             (if use-lists? '(up) 'up)]
            [(and direction-names? (= x +1) (zero? y))
             (if use-lists? '(right) 'right)]
            [(and direction-names? (zero? x) (= y -1))
             (if use-lists? '(down) 'down)]
            [angles? `(angle ,(/ (* 180 (atan y x)) pi))]
            [complex-directions? `(,dir-notation ,(make-rectangular x y))]
            [else `(,dir-notation (,x ,y))]))
    (define (make-tension atleast? t1 t2)
      (let ([tension-name (if atleast?
                              atleast-notation
                              '(tension))])
        (if tension-shortcuts?
            (cond [(and (not atleast?)
                        (inf? t1) (inf? t2)
                        (positive? t1) (positive? t2))
                   '--]
                  [(and (not atleast?)
                        (<= (abs (- MF-INFINITY t1)) MF-INFINITY-EPS)
                        (<= (abs (- MF-INFINITY t2)) MF-INFINITY-EPS))
                   '---]
                  [(= t1 t2 1) (if atleast? '... '..)]
                  [(= t1 t2) `(,@tension-name ,t1)]
                  [`(,@tension-name ,t1 ,@and-notation ,t2)])
            `(,@tension-name ,t1 ,@and-notation ,t2))))
    (let ([n (f64vector-length vec)])
      (let loop ([lst '()]
                 [i 0])
        (if (<= n i)
            (reverse! lst)
            (let ([token (f64vector-ref vec i)]
                  [value1 (f64vector-ref vec (+ i 1))]
                  [value2 (f64vector-ref vec (+ i 2))])
              (cond [(= token HOBBY_GUIDE_POINT)
                     (loop (cons (make-point-value value1 value2) lst)
                           (+ i 3))]
                    [(= token HOBBY_GUIDE_CYCLE)
                     (loop (cons (if use-lists? '(cycle) 'cycle) lst)
                           (+ i 3))]
                    [(= token HOBBY_GUIDE_TENSION)
                     (loop (cons (make-tension #f value1 value2) lst)
                           (+ i 3))]
                    [(= token HOBBY_GUIDE_ATLEAST)
                     (loop (cons (make-tension #t value1 value2) lst)
                           (+ i 3))]
                    [(= token HOBBY_GUIDE_DIR)
                     (loop (cons (make-dir value1 value2) lst) (+ i 3))]
                    [(= token HOBBY_GUIDE_CURL)
                     (loop (cons `(curl ,value1) lst) (+ i 3))]
                    [(= token HOBBY_GUIDE_CTRL)
                     (if (and (< i (- n 3))
                              (= (f64vector-ref vec (+ i 3)) HOBBY_GUIDE_CTRL))
                         (let ([value3 (f64vector-ref vec (+ i 4))]
                               [value4 (f64vector-ref vec (+ i 5))])
                           (loop (cons `(controls
                                         ,(make-point-value value1 value2)
                                         ,@and-notation
                                         ,(make-point-value value3 value4))
                                       lst)
                                 (+ i 6)))
                         (assertion-violation 'f64vector->hobby-guide
                                              (_ "control point not in a pair")
                                              vec i))]
                    [else
                     (assertion-violation 'f64vector->hobby-guide
                                          (_ "unrecognized token value")
                                          vec i token)]))))))

  (define (token-error->expression-error guide tokens bad-token-index)
    (let loop ([i 0]
               [guide-index 0])
      (if (<= bad-token-index i)
          (let-values ([(before after) (split-at guide guide-index)])
            `(,@before >>>>> ,@after))
          (let ([tok (f64vector-ref tokens (* 3 i))])
            (if (= tok HOBBY_GUIDE_CTRL)
                (begin
                  (assert (= (f64vector-ref tokens (* 3 (1+ i))) HOBBY_GUIDE_CTRL))
                  (loop (+ i 2) (1+ guide-index)))
                (begin
                  (assert (or (= tok HOBBY_GUIDE_POINT)
                              (= tok HOBBY_GUIDE_CYCLE)
                              (= tok HOBBY_GUIDE_TENSION)
                              (= tok HOBBY_GUIDE_ATLEAST)
                              (= tok HOBBY_GUIDE_DIR)
                              (= tok HOBBY_GUIDE_CURL)))
                  (loop (1+ i) (1+ guide-index))))))))

  (define* (solve-hobby-guide guide #:key
                              [return-f64vector? #f]
                              [style (hobby-guide-style)])
    "FIXME: Document this."
    (let ([f64-input (hobby-guide->f64vector guide)])
      (let ([f64-solution
             (guard (exc [(and (hobby-guide-tokens-status-condition? exc)
                               (= (hobby-guide-tokens-status-info exc)
                                  HOBBY_GUIDE_TOKEN_ERROR))
                          (let ([tokens (hobby-guide-tokens-status-tokens exc)]
                                [bad-token-index
                                 (hobby-guide-tokens-status-bad-token-index exc)]
                                [tokens-message (if (message-condition? exc)
                                                    (condition-message exc)
                                                    #f)])
                            (raise
                             (apply condition
                                    `(,(make-error)
                                      ,(make-who-condition 'solve-hobby-guide)
                                      ,(make-message-condition
                                        "Hobby guide expression error")
                                      ,(make-irritants-condition                                   
                                        (token-error->expression-error
                                         guide tokens bad-token-index))
                                      ,(make-hobby-guide-tokens-message-condition
                                        tokens-message)))))])
               (solve-hobby-guide-tokens f64-input))])
        (if return-f64vector?
            f64-solution
            (f64vector->hobby-guide f64-solution #:style style)))))

  ;;--------------------------------------------------------------------

  ) ;; end of library.
