;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2017 Khaled Hosny and Barry Schwartz
;; Copyright (C) 2014, 2015 Free Software Foundation, Inc.
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

;; This code is based on the foreign objects implementation in Guile
;; 2.2.2. Exotic objects, however, are restricted to wrapped pointers
;; or SCM objects not requiring finalization by the wrapper. They are
;; a GOOPS-based alternative to, for instance,
;; define-wrapped-pointer-type.

(define-module (sortsmill core exotic-objects)
  #:use-module (oop goops)
  #:use-module (sortsmill core helpers)
  #:use-module (sortsmill core scm-procedures)
  #:export     (<exotic-object-class>
                make-exotic-object-type
                make-exotic-object
                exotic-object?
                exotic-object-ref
                exotic-object-set!))

(eval-when (compile load eval)
  (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                        scm_make_exotic_object
                        scm_exotic_object_p
                        scm_exotic_object_ref
                        scm_exotic_object_set_x))

(define-class <exotic-object-class> (<class>))

(define (make-exotic-object-type name)
  (unless (symbol? name)
    (error "type name should be a symbol" name))
  (make-class '() '((pointer))
              #:name name
              #:static-slot-allocation? #t
              #:metaclass <exotic-object-class>))

(define-scm-procedure (internal--exotic-object? obj type)
  scm_exotic_object_p)

(define-scm-procedure (internal--make-exotic-object type pointer)
  scm_make_exotic_object)

(define-scm-procedure (internal--exotic-object-ref type obj)
  scm_exotic_object_ref)

(define-scm-procedure (internal--exotic-object-set! type obj pointer)
  scm_exotic_object_set_x)

(define (check-type type proc-name)
  (unless (is-a? type <exotic-object-class>)
    (scm-error 'wrong-type-arg proc-name
               "Wrong type (expecting ~A): ~S"
               (list (class-name <exotic-object-class>) type)
               (list type))))

(define (make-exotic-object type obj)
  "Construct a new instance of an exotic object of given type."
  (check-type type 'make-exotic-object)
  (internal--make-exotic-object type obj))

(define (exotic-object? obj type)
  "Is the first argument an exotic object of the type given as the
second argument?"
  (check-type type 'exotic-object?)
  (internal--exotic-object? obj type))

(define (exotic-object-ref type obj)
  "Return the value of the pointer that is stored in an exotic object,
which must be of the given type."
  (check-type type 'exotic-object-ref)
  (internal--exotic-object-ref type obj))

(define (exotic-object-set! type obj val)
  "Change the value stored in an exotic object, which must be of the
given type."
  (check-type type 'exotic-object-set!)
  (internal--exotic-object-set! type obj val))
