;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core immutable-vectors)

  (export

   any-ivect?
   any-ivect-null?
   any-ivect-length

   ivect-type

   ivect?
   u8ivect? u16ivect? u32ivect? u64ivect?
   s8ivect? s16ivect? s32ivect? s64ivect?
   f32ivect? f64ivect? c32ivect? c64ivect?

   ivect
   u8ivect u16ivect u32ivect u64ivect
   s8ivect s16ivect s32ivect s64ivect
   f32ivect f64ivect c32ivect c64ivect

   ivect-null
   u8ivect-null u16ivect-null u32ivect-null u64ivect-null
   s8ivect-null s16ivect-null s32ivect-null s64ivect-null
   f32ivect-null f64ivect-null c32ivect-null c64ivect-null

   ivect-null?
   u8ivect-null? u16ivect-null? u32ivect-null? u64ivect-null?
   s8ivect-null? s16ivect-null? s32ivect-null? s64ivect-null?
   f32ivect-null? f64ivect-null? c32ivect-null? c64ivect-null?

   ivect-length
   u8ivect-length u16ivect-length u32ivect-length u64ivect-length
   s8ivect-length s16ivect-length s32ivect-length s64ivect-length
   f32ivect-length f64ivect-length c32ivect-length c64ivect-length

   ivect-ref
   u8ivect-ref u16ivect-ref u32ivect-ref u64ivect-ref
   s8ivect-ref s16ivect-ref s32ivect-ref s64ivect-ref
   f32ivect-ref f64ivect-ref c32ivect-ref c64ivect-ref

   ivect-set
   u8ivect-set u16ivect-set u32ivect-set u64ivect-set
   s8ivect-set s16ivect-set s32ivect-set s64ivect-set
   f32ivect-set f64ivect-set c32ivect-set c64ivect-set

   ivect-push
   u8ivect-push u16ivect-push u32ivect-push u64ivect-push
   s8ivect-push s16ivect-push s32ivect-push s64ivect-push
   f32ivect-push f64ivect-push c32ivect-push c64ivect-push

   ivect-pop
   u8ivect-pop u16ivect-pop u32ivect-pop u64ivect-pop
   s8ivect-pop s16ivect-pop s32ivect-pop s64ivect-pop
   f32ivect-pop f64ivect-pop c32ivect-pop c64ivect-pop

   ivect-slice-ref
   u8ivect-slice-ref u16ivect-slice-ref u32ivect-slice-ref u64ivect-slice-ref
   s8ivect-slice-ref s16ivect-slice-ref s32ivect-slice-ref s64ivect-slice-ref
   f32ivect-slice-ref f64ivect-slice-ref c32ivect-slice-ref c64ivect-slice-ref

   ivect-slice-set
   u8ivect-slice-set u16ivect-slice-set u32ivect-slice-set u64ivect-slice-set
   s8ivect-slice-set s16ivect-slice-set s32ivect-slice-set s64ivect-slice-set
   f32ivect-slice-set f64ivect-slice-set c32ivect-slice-set c64ivect-slice-set

   ivect-reverse
   u8ivect-reverse u16ivect-reverse u32ivect-reverse u64ivect-reverse
   s8ivect-reverse s16ivect-reverse s32ivect-reverse s64ivect-reverse
   f32ivect-reverse f64ivect-reverse c32ivect-reverse c64ivect-reverse

   ivect-concatenate
   u8ivect-concatenate u16ivect-concatenate u32ivect-concatenate u64ivect-concatenate
   s8ivect-concatenate s16ivect-concatenate s32ivect-concatenate s64ivect-concatenate
   f32ivect-concatenate f64ivect-concatenate c32ivect-concatenate c64ivect-concatenate
   ivect-append
   u8ivect-append u16ivect-append u32ivect-append u64ivect-append
   s8ivect-append s16ivect-append s32ivect-append s64ivect-append
   f32ivect-append f64ivect-append c32ivect-append c64ivect-append

   ivect->list
   u8ivect->list u16ivect->list u32ivect->list u64ivect->list
   s8ivect->list s16ivect->list s32ivect->list s64ivect->list
   f32ivect->list f64ivect->list c32ivect->list c64ivect->list

   list->ivect
   list->u8ivect list->u16ivect list->u32ivect list->u64ivect
   list->s8ivect list->s16ivect list->s32ivect list->s64ivect
   list->f32ivect list->f64ivect list->c32ivect list->c64ivect

   ivect->vlst
   u8ivect->vlst u16ivect->vlst u32ivect->vlst u64ivect->vlst
   s8ivect->vlst s16ivect->vlst s32ivect->vlst s64ivect->vlst
   f32ivect->vlst f64ivect->vlst c32ivect->vlst c64ivect->vlst

   vlst->ivect
   vlst->u8ivect vlst->u16ivect vlst->u32ivect vlst->u64ivect
   vlst->s8ivect vlst->s16ivect vlst->s32ivect vlst->s64ivect
   vlst->f32ivect vlst->f64ivect vlst->c32ivect vlst->c64ivect

   ivect->vector
   u8ivect->vector u16ivect->vector u32ivect->vector u64ivect->vector
   s8ivect->vector s16ivect->vector s32ivect->vector s64ivect->vector
   f32ivect->vector f64ivect->vector c32ivect->vector c64ivect->vector
   u8ivect->u8vector u16ivect->u16vector u32ivect->u32vector u64ivect->u64vector
   s8ivect->s8vector s16ivect->s16vector s32ivect->s32vector s64ivect->s64vector
   f32ivect->f32vector f64ivect->f64vector c32ivect->c32vector c64ivect->c64vector

   vector->ivect
   vector->u8ivect vector->u16ivect vector->u32ivect vector->u64ivect
   vector->s8ivect vector->s16ivect vector->s32ivect vector->s64ivect
   vector->f32ivect vector->f64ivect vector->c32ivect vector->c64ivect
   u8vector->u8ivect u16vector->u16ivect u32vector->u32ivect u64vector->u64ivect
   s8vector->s8ivect s16vector->s16ivect s32vector->s32ivect s64vector->s64ivect
   f32vector->f32ivect f64vector->f64ivect c32vector->c32ivect c64vector->c64ivect

   ivect-map
   ivect-map-in-order
   )

  (import (except (rnrs)
                  ;; We want to redefine the following with GOOPS;
                  ;; importing them from (rnrs) might cause trouble.
                  write equal?)
          (except (guile) error)
          (oop goops)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs
     (dynamic-link-sortsmill-core-guile)

     scm_any_ivect_p
     scm_any_ivect_null_p
     scm_any_ivect_length

     scm_ivect_type

     scm_ivect_p
     scm_u8ivect_p scm_u16ivect_p scm_u32ivect_p scm_u64ivect_p
     scm_s8ivect_p scm_s16ivect_p scm_s32ivect_p scm_s64ivect_p
     scm_f32ivect_p scm_f64ivect_p scm_c32ivect_p scm_c64ivect_p

     scm_ivect_null_p
     scm_u8ivect_null_p scm_u16ivect_null_p scm_u32ivect_null_p scm_u64ivect_null_p
     scm_s8ivect_null_p scm_s16ivect_null_p scm_s32ivect_null_p scm_s64ivect_null_p
     scm_f32ivect_null_p scm_f64ivect_null_p scm_c32ivect_null_p scm_c64ivect_null_p

     scm_ivect_length
     scm_u8ivect_length scm_u16ivect_length scm_u32ivect_length scm_u64ivect_length
     scm_s8ivect_length scm_s16ivect_length scm_s32ivect_length scm_s64ivect_length
     scm_f32ivect_length scm_f64ivect_length scm_c32ivect_length scm_c64ivect_length

     scm_ivect_ref
     scm_u8ivect_ref scm_u16ivect_ref scm_u32ivect_ref scm_u64ivect_ref
     scm_s8ivect_ref scm_s16ivect_ref scm_s32ivect_ref scm_s64ivect_ref
     scm_f32ivect_ref scm_f64ivect_ref scm_c32ivect_ref scm_c64ivect_ref

     scm_ivect_set
     scm_u8ivect_set scm_u16ivect_set scm_u32ivect_set scm_u64ivect_set
     scm_s8ivect_set scm_s16ivect_set scm_s32ivect_set scm_s64ivect_set
     scm_f32ivect_set scm_f64ivect_set scm_c32ivect_set scm_c64ivect_set

     scm_ivect_push
     scm_u8ivect_push scm_u16ivect_push scm_u32ivect_push scm_u64ivect_push
     scm_s8ivect_push scm_s16ivect_push scm_s32ivect_push scm_s64ivect_push
     scm_f32ivect_push scm_f64ivect_push scm_c32ivect_push scm_c64ivect_push

     scm_ivect_pop
     scm_u8ivect_pop scm_u16ivect_pop scm_u32ivect_pop scm_u64ivect_pop
     scm_s8ivect_pop scm_s16ivect_pop scm_s32ivect_pop scm_s64ivect_pop
     scm_f32ivect_pop scm_f64ivect_pop scm_c32ivect_pop scm_c64ivect_pop

     scm_ivect_slice_ref
     scm_u8ivect_slice_ref scm_u16ivect_slice_ref scm_u32ivect_slice_ref scm_u64ivect_slice_ref
     scm_s8ivect_slice_ref scm_s16ivect_slice_ref scm_s32ivect_slice_ref scm_s64ivect_slice_ref
     scm_f32ivect_slice_ref scm_f64ivect_slice_ref scm_c32ivect_slice_ref scm_c64ivect_slice_ref

     scm_ivect_slice_set
     scm_u8ivect_slice_set scm_u16ivect_slice_set scm_u32ivect_slice_set scm_u64ivect_slice_set
     scm_s8ivect_slice_set scm_s16ivect_slice_set scm_s32ivect_slice_set scm_s64ivect_slice_set
     scm_f32ivect_slice_set scm_f64ivect_slice_set scm_c32ivect_slice_set scm_c64ivect_slice_set

     scm_ivect_reverse
     scm_u8ivect_reverse scm_u16ivect_reverse scm_u32ivect_reverse scm_u64ivect_reverse
     scm_s8ivect_reverse scm_s16ivect_reverse scm_s32ivect_reverse scm_s64ivect_reverse
     scm_f32ivect_reverse scm_f64ivect_reverse scm_c32ivect_reverse scm_c64ivect_reverse

     scm_ivect_concatenate
     scm_u8ivect_concatenate scm_u16ivect_concatenate scm_u32ivect_concatenate scm_u64ivect_concatenate
     scm_s8ivect_concatenate scm_s16ivect_concatenate scm_s32ivect_concatenate scm_s64ivect_concatenate
     scm_f32ivect_concatenate scm_f64ivect_concatenate scm_c32ivect_concatenate scm_c64ivect_concatenate

     scm_ivect_to_list
     scm_u8ivect_to_list scm_u16ivect_to_list scm_u32ivect_to_list scm_u64ivect_to_list
     scm_s8ivect_to_list scm_s16ivect_to_list scm_s32ivect_to_list scm_s64ivect_to_list
     scm_f32ivect_to_list scm_f64ivect_to_list scm_c32ivect_to_list scm_c64ivect_to_list

     scm_list_to_ivect                          
     scm_list_to_u8ivect scm_list_to_u16ivect scm_list_to_u32ivect scm_list_to_u64ivect
     scm_list_to_s8ivect scm_list_to_s16ivect scm_list_to_s32ivect scm_list_to_s64ivect
     scm_list_to_f32ivect scm_list_to_f64ivect scm_list_to_c32ivect scm_list_to_c64ivect

     scm_ivect_to_vlst
     scm_u8ivect_to_vlst scm_u16ivect_to_vlst scm_u32ivect_to_vlst scm_u64ivect_to_vlst
     scm_s8ivect_to_vlst scm_s16ivect_to_vlst scm_s32ivect_to_vlst scm_s64ivect_to_vlst
     scm_f32ivect_to_vlst scm_f64ivect_to_vlst scm_c32ivect_to_vlst scm_c64ivect_to_vlst

     scm_vlst_to_ivect                          
     scm_vlst_to_u8ivect scm_vlst_to_u16ivect scm_vlst_to_u32ivect scm_vlst_to_u64ivect
     scm_vlst_to_s8ivect scm_vlst_to_s16ivect scm_vlst_to_s32ivect scm_vlst_to_s64ivect
     scm_vlst_to_f32ivect scm_vlst_to_f64ivect scm_vlst_to_c32ivect scm_vlst_to_c64ivect

     scm_ivect_to_vector
     scm_u8ivect_to_vector scm_u16ivect_to_vector scm_u32ivect_to_vector scm_u64ivect_to_vector
     scm_s8ivect_to_vector scm_s16ivect_to_vector scm_s32ivect_to_vector scm_s64ivect_to_vector
     scm_f32ivect_to_vector scm_f64ivect_to_vector scm_c32ivect_to_vector scm_c64ivect_to_vector
     scm_u8ivect_to_u8vector scm_u16ivect_to_u16vector scm_u32ivect_to_u32vector scm_u64ivect_to_u64vector
     scm_s8ivect_to_s8vector scm_s16ivect_to_s16vector scm_s32ivect_to_s32vector scm_s64ivect_to_s64vector
     scm_f32ivect_to_f32vector scm_f64ivect_to_f64vector scm_c32ivect_to_c32vector scm_c64ivect_to_c64vector

     scm_vector_to_ivect
     scm_vector_to_u8ivect scm_vector_to_u16ivect scm_vector_to_u32ivect scm_vector_to_u64ivect
     scm_vector_to_s8ivect scm_vector_to_s16ivect scm_vector_to_s32ivect scm_vector_to_s64ivect
     scm_vector_to_f32ivect scm_vector_to_f64ivect scm_vector_to_c32ivect scm_vector_to_c64ivect
     scm_u8vector_to_u8ivect scm_u16vector_to_u16ivect scm_u32vector_to_u32ivect scm_u64vector_to_u64ivect
     scm_s8vector_to_s8ivect scm_s16vector_to_s16ivect scm_s32vector_to_s32ivect scm_s64vector_to_s64ivect
     scm_f32vector_to_f32ivect scm_f64vector_to_f64ivect scm_c32vector_to_c32ivect scm_c64vector_to_c64ivect

     scm_ivect_map
     scm_ivect_map_in_order

     guile_init_smcoreguile_ivects
     guile_ivect_write__
     guile_ivect_equal_p__)

    ((pointer->procedure void guile_init_smcoreguile_ivects '())))

  (define-scm-procedure (internal-ivect-write obj port)
    guile_ivect_write__)

  (define-generic write)
  (define-method (write [obj <ivect>] port)
    (internal-ivect-write obj port))

  (define-scm-procedure (internal-ivect-equal? a b)
    guile_ivect_equal_p__)

  (define-generic equal?)
  (define-method (equal? [a <ivect>] [b <ivect>])
    (internal-ivect-equal? a b))

  ;;------------------------------------------------------------

  (define-scm-procedure (any-ivect? ivect)
    scm_any_ivect_p)

  (define-scm-procedure (any-ivect-null? ivect)
    scm_any_ivect_null_p)

  (define-scm-procedure (any-ivect-length ivect)
    scm_any_ivect_length)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-type ivect)
    scm_ivect_type)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect? obj)
    scm_ivect_p)

  (define-scm-procedure (u8ivect? obj)
    scm_u8ivect_p)

  (define-scm-procedure (u16ivect? obj)
    scm_u16ivect_p)

  (define-scm-procedure (u32ivect? obj)
    scm_u32ivect_p)

  (define-scm-procedure (u64ivect? obj)
    scm_u64ivect_p)

  (define-scm-procedure (s8ivect? obj)
    scm_s8ivect_p)

  (define-scm-procedure (s16ivect? obj)
    scm_s16ivect_p)

  (define-scm-procedure (s32ivect? obj)
    scm_s32ivect_p)

  (define-scm-procedure (s64ivect? obj)
    scm_s64ivect_p)

  (define-scm-procedure (f32ivect? obj)
    scm_f32ivect_p)

  (define-scm-procedure (f64ivect? obj)
    scm_f64ivect_p)

  (define-scm-procedure (c32ivect? obj)
    scm_c32ivect_p)

  (define-scm-procedure (c64ivect? obj)
    scm_c64ivect_p)

  ;;------------------------------------------------------------

  (define (ivect . entries)
    (apply ivect-push ivect-null entries))

  (define (u8ivect . entries)
    (apply u8ivect-push u8ivect-null entries))

  (define (u16ivect . entries)
    (apply u16ivect-push u16ivect-null entries))

  (define (u32ivect . entries)
    (apply u32ivect-push u32ivect-null entries))

  (define (u64ivect . entries)
    (apply u64ivect-push u64ivect-null entries))

  (define (s8ivect . entries)
    (apply s8ivect-push s8ivect-null entries))

  (define (s16ivect . entries)
    (apply s16ivect-push s16ivect-null entries))

  (define (s32ivect . entries)
    (apply s32ivect-push s32ivect-null entries))

  (define (s64ivect . entries)
    (apply s64ivect-push s64ivect-null entries))

  (define (f32ivect . entries)
    (apply f32ivect-push f32ivect-null entries))

  (define (f64ivect . entries)
    (apply f64ivect-push f64ivect-null entries))

  (define (c32ivect . entries)
    (apply c32ivect-push c32ivect-null entries))

  (define (c64ivect . entries)
    (apply c64ivect-push c64ivect-null entries))

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-null? obj)
    scm_ivect_null_p)

  (define-scm-procedure (u8ivect-null? obj)
    scm_u8ivect_null_p)

  (define-scm-procedure (u16ivect-null? obj)
    scm_u16ivect_null_p)

  (define-scm-procedure (u32ivect-null? obj)
    scm_u32ivect_null_p)

  (define-scm-procedure (u64ivect-null? obj)
    scm_u64ivect_null_p)

  (define-scm-procedure (s8ivect-null? obj)
    scm_s8ivect_null_p)

  (define-scm-procedure (s16ivect-null? obj)
    scm_s16ivect_null_p)

  (define-scm-procedure (s32ivect-null? obj)
    scm_s32ivect_null_p)

  (define-scm-procedure (s64ivect-null? obj)
    scm_s64ivect_null_p)

  (define-scm-procedure (f32ivect-null? obj)
    scm_f32ivect_null_p)

  (define-scm-procedure (f64ivect-null? obj)
    scm_f64ivect_null_p)
  
  (define-scm-procedure (c32ivect-null? obj)
    scm_c32ivect_null_p)

  (define-scm-procedure (c64ivect-null? obj)
    scm_c64ivect_null_p)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-length ivect)
    scm_ivect_length)

  (define-scm-procedure (u8ivect-length ivect)
    scm_u8ivect_length)

  (define-scm-procedure (u16ivect-length ivect)
    scm_u16ivect_length)

  (define-scm-procedure (u32ivect-length ivect)
    scm_u32ivect_length)

  (define-scm-procedure (u64ivect-length ivect)
    scm_u64ivect_length)

  (define-scm-procedure (s8ivect-length ivect)
    scm_s8ivect_length)

  (define-scm-procedure (s16ivect-length ivect)
    scm_s16ivect_length)

  (define-scm-procedure (s32ivect-length ivect)
    scm_s32ivect_length)

  (define-scm-procedure (s64ivect-length ivect)
    scm_s64ivect_length)

  (define-scm-procedure (f32ivect-length ivect)
    scm_f32ivect_length)

  (define-scm-procedure (f64ivect-length ivect)
    scm_f64ivect_length)
  
  (define-scm-procedure (c32ivect-length ivect)
    scm_c32ivect_length)

  (define-scm-procedure (c64ivect-length ivect)
    scm_c64ivect_length)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-ref ivect i)
    scm_ivect_ref)

  (define-scm-procedure (u8ivect-ref ivect i)
    scm_u8ivect_ref)

  (define-scm-procedure (u16ivect-ref ivect i)
    scm_u16ivect_ref)

  (define-scm-procedure (u32ivect-ref ivect i)
    scm_u32ivect_ref)

  (define-scm-procedure (u64ivect-ref ivect i)
    scm_u64ivect_ref)

  (define-scm-procedure (s8ivect-ref ivect i)
    scm_s8ivect_ref)

  (define-scm-procedure (s16ivect-ref ivect i)
    scm_s16ivect_ref)

  (define-scm-procedure (s32ivect-ref ivect i)
    scm_s32ivect_ref)

  (define-scm-procedure (s64ivect-ref ivect i)
    scm_s64ivect_ref)

  (define-scm-procedure (f32ivect-ref ivect i)
    scm_f32ivect_ref)

  (define-scm-procedure (f64ivect-ref ivect i)
    scm_f64ivect_ref)
  
  (define-scm-procedure (c32ivect-ref ivect i)
    scm_c32ivect_ref)

  (define-scm-procedure (c64ivect-ref ivect i)
    scm_c64ivect_ref)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-set ivect i x)
    scm_ivect_set)

  (define-scm-procedure (u8ivect-set ivect i x)
    scm_u8ivect_set)

  (define-scm-procedure (u16ivect-set ivect i x)
    scm_u16ivect_set)

  (define-scm-procedure (u32ivect-set ivect i x)
    scm_u32ivect_set)

  (define-scm-procedure (u64ivect-set ivect i x)
    scm_u64ivect_set)

  (define-scm-procedure (s8ivect-set ivect i x)
    scm_s8ivect_set)

  (define-scm-procedure (s16ivect-set ivect i x)
    scm_s16ivect_set)

  (define-scm-procedure (s32ivect-set ivect i x)
    scm_s32ivect_set)

  (define-scm-procedure (s64ivect-set ivect i x)
    scm_s64ivect_set)

  (define-scm-procedure (f32ivect-set ivect i x)
    scm_f32ivect_set)

  (define-scm-procedure (f64ivect-set ivect i x)
    scm_f64ivect_set)
  
  (define-scm-procedure (c32ivect-set ivect i x)
    scm_c32ivect_set)

  (define-scm-procedure (c64ivect-set ivect i x)
    scm_c64ivect_set)

  ;;------------------------------------------------------------

  (define-syntax define-ivect-push
    (syntax-rules ()
      [(_ NAME FUNC)
       (define NAME
         (let ([proc (lambda*-scm-procedure (ivect obj #:optional more-objs)
                       FUNC)])
           (case-lambda
             [(ivect obj) (proc ivect obj)]
             [(ivect obj . more-objs) (proc ivect obj more-objs)]
             [(ivect) ivect])))]))

  (define-ivect-push ivect-push scm_ivect_push)
  (define-ivect-push u8ivect-push scm_u8ivect_push)
  (define-ivect-push u16ivect-push scm_u16ivect_push)
  (define-ivect-push u32ivect-push scm_u32ivect_push)
  (define-ivect-push u64ivect-push scm_u64ivect_push)
  (define-ivect-push s8ivect-push scm_s8ivect_push)
  (define-ivect-push s16ivect-push scm_s16ivect_push)
  (define-ivect-push s32ivect-push scm_s32ivect_push)
  (define-ivect-push s64ivect-push scm_s64ivect_push)
  (define-ivect-push f32ivect-push scm_f32ivect_push)
  (define-ivect-push f64ivect-push scm_f64ivect_push)
  (define-ivect-push c32ivect-push scm_c32ivect_push)
  (define-ivect-push c64ivect-push scm_c64ivect_push)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect-pop ivect #:optional count)
    scm_ivect_pop)

  (define*-scm-procedure (u8ivect-pop ivect #:optional count)
    scm_u8ivect_pop)

  (define*-scm-procedure (u16ivect-pop ivect #:optional count)
    scm_u16ivect_pop)

  (define*-scm-procedure (u32ivect-pop ivect #:optional count)
    scm_u32ivect_pop)

  (define*-scm-procedure (u64ivect-pop ivect #:optional count)
    scm_u64ivect_pop)

  (define*-scm-procedure (s8ivect-pop ivect #:optional count)
    scm_s8ivect_pop)

  (define*-scm-procedure (s16ivect-pop ivect #:optional count)
    scm_s16ivect_pop)

  (define*-scm-procedure (s32ivect-pop ivect #:optional count)
    scm_s32ivect_pop)

  (define*-scm-procedure (s64ivect-pop ivect #:optional count)
    scm_s64ivect_pop)

  (define*-scm-procedure (f32ivect-pop ivect #:optional count)
    scm_f32ivect_pop)

  (define*-scm-procedure (f64ivect-pop ivect #:optional count)
    scm_f64ivect_pop)
  
  (define*-scm-procedure (c32ivect-pop ivect #:optional count)
    scm_c32ivect_pop)

  (define*-scm-procedure (c64ivect-pop ivect #:optional count)
    scm_c64ivect_pop)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect-slice-ref ivect start #:optional end)
    scm_ivect_slice_ref)

  (define*-scm-procedure (u8ivect-slice-ref ivect start #:optional end)
    scm_u8ivect_slice_ref)

  (define*-scm-procedure (u16ivect-slice-ref ivect start #:optional end)
    scm_u16ivect_slice_ref)

  (define*-scm-procedure (u32ivect-slice-ref ivect start #:optional end)
    scm_u32ivect_slice_ref)

  (define*-scm-procedure (u64ivect-slice-ref ivect start #:optional end)
    scm_u64ivect_slice_ref)

  (define*-scm-procedure (s8ivect-slice-ref ivect start #:optional end)
    scm_s8ivect_slice_ref)

  (define*-scm-procedure (s16ivect-slice-ref ivect start #:optional end)
    scm_s16ivect_slice_ref)

  (define*-scm-procedure (s32ivect-slice-ref ivect start #:optional end)
    scm_s32ivect_slice_ref)

  (define*-scm-procedure (s64ivect-slice-ref ivect start #:optional end)
    scm_s64ivect_slice_ref)

  (define*-scm-procedure (f32ivect-slice-ref ivect start #:optional end)
    scm_f32ivect_slice_ref)

  (define*-scm-procedure (f64ivect-slice-ref ivect start #:optional end)
    scm_f64ivect_slice_ref)
  
  (define*-scm-procedure (c32ivect-slice-ref ivect start #:optional end)
    scm_c32ivect_slice_ref)

  (define*-scm-procedure (c64ivect-slice-ref ivect start #:optional end)
    scm_c64ivect_slice_ref)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-slice-set ivect start source)
    scm_ivect_slice_set)

  (define-scm-procedure (u8ivect-slice-set ivect start source)
    scm_u8ivect_slice_set)

  (define-scm-procedure (u16ivect-slice-set ivect start source)
    scm_u16ivect_slice_set)

  (define-scm-procedure (u32ivect-slice-set ivect start source)
    scm_u32ivect_slice_set)

  (define-scm-procedure (u64ivect-slice-set ivect start source)
    scm_u64ivect_slice_set)

  (define-scm-procedure (s8ivect-slice-set ivect start source)
    scm_s8ivect_slice_set)

  (define-scm-procedure (s16ivect-slice-set ivect start source)
    scm_s16ivect_slice_set)

  (define-scm-procedure (s32ivect-slice-set ivect start source)
    scm_s32ivect_slice_set)

  (define-scm-procedure (s64ivect-slice-set ivect start source)
    scm_s64ivect_slice_set)

  (define-scm-procedure (f32ivect-slice-set ivect start source)
    scm_f32ivect_slice_set)

  (define-scm-procedure (f64ivect-slice-set ivect start source)
    scm_f64ivect_slice_set)
  
  (define-scm-procedure (c32ivect-slice-set ivect start source)
    scm_c32ivect_slice_set)

  (define-scm-procedure (c64ivect-slice-set ivect start source)
    scm_c64ivect_slice_set)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect-reverse ivect #:optional start end)
    scm_ivect_reverse)

  (define*-scm-procedure (u8ivect-reverse ivect #:optional start end)
    scm_u8ivect_reverse)

  (define*-scm-procedure (u16ivect-reverse ivect #:optional start end)
    scm_u16ivect_reverse)

  (define*-scm-procedure (u32ivect-reverse ivect #:optional start end)
    scm_u32ivect_reverse)

  (define*-scm-procedure (u64ivect-reverse ivect #:optional start end)
    scm_u64ivect_reverse)

  (define*-scm-procedure (s8ivect-reverse ivect #:optional start end)
    scm_s8ivect_reverse)

  (define*-scm-procedure (s16ivect-reverse ivect #:optional start end)
    scm_s16ivect_reverse)

  (define*-scm-procedure (s32ivect-reverse ivect #:optional start end)
    scm_s32ivect_reverse)

  (define*-scm-procedure (s64ivect-reverse ivect #:optional start end)
    scm_s64ivect_reverse)

  (define*-scm-procedure (f32ivect-reverse ivect #:optional start end)
    scm_f32ivect_reverse)

  (define*-scm-procedure (f64ivect-reverse ivect #:optional start end)
    scm_f64ivect_reverse)
  
  (define*-scm-procedure (c32ivect-reverse ivect #:optional start end)
    scm_c32ivect_reverse)

  (define*-scm-procedure (c64ivect-reverse ivect #:optional start end)
    scm_c64ivect_reverse)

  ;;------------------------------------------------------------

  (define-scm-procedure (ivect-concatenate ivect_list)
    scm_ivect_concatenate)

  (define-scm-procedure (u8ivect-concatenate ivect_list)
    scm_u8ivect_concatenate)

  (define-scm-procedure (u16ivect-concatenate ivect_list)
    scm_u16ivect_concatenate)

  (define-scm-procedure (u32ivect-concatenate ivect_list)
    scm_u32ivect_concatenate)

  (define-scm-procedure (u64ivect-concatenate ivect_list)
    scm_u64ivect_concatenate)

  (define-scm-procedure (s8ivect-concatenate ivect_list)
    scm_s8ivect_concatenate)

  (define-scm-procedure (s16ivect-concatenate ivect_list)
    scm_s16ivect_concatenate)

  (define-scm-procedure (s32ivect-concatenate ivect_list)
    scm_s32ivect_concatenate)

  (define-scm-procedure (s64ivect-concatenate ivect_list)
    scm_s64ivect_concatenate)

  (define-scm-procedure (f32ivect-concatenate ivect_list)
    scm_f32ivect_concatenate)

  (define-scm-procedure (f64ivect-concatenate ivect_list)
    scm_f64ivect_concatenate)
  
  (define-scm-procedure (c32ivect-concatenate ivect_list)
    scm_c32ivect_concatenate)

  (define-scm-procedure (c64ivect-concatenate ivect_list)
    scm_c64ivect_concatenate)

  (define-scm-procedure (ivect-append . ivect_list)
    scm_ivect_concatenate)

  (define-scm-procedure (u8ivect-append . ivect_list)
    scm_u8ivect_concatenate)

  (define-scm-procedure (u16ivect-append . ivect_list)
    scm_u16ivect_concatenate)

  (define-scm-procedure (u32ivect-append . ivect_list)
    scm_u32ivect_concatenate)

  (define-scm-procedure (u64ivect-append . ivect_list)
    scm_u64ivect_concatenate)

  (define-scm-procedure (s8ivect-append . ivect_list)
    scm_s8ivect_concatenate)

  (define-scm-procedure (s16ivect-append . ivect_list)
    scm_s16ivect_concatenate)

  (define-scm-procedure (s32ivect-append . ivect_list)
    scm_s32ivect_concatenate)

  (define-scm-procedure (s64ivect-append . ivect_list)
    scm_s64ivect_concatenate)

  (define-scm-procedure (f32ivect-append . ivect_list)
    scm_f32ivect_concatenate)

  (define-scm-procedure (f64ivect-append . ivect_list)
    scm_f64ivect_concatenate)
  
  (define-scm-procedure (c32ivect-append . ivect_list)
    scm_c32ivect_concatenate)

  (define-scm-procedure (c64ivect-append . ivect_list)
    scm_c64ivect_concatenate)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect->list ivect #:optional start end)
    scm_ivect_to_list)

  (define*-scm-procedure (u8ivect->list ivect #:optional start end)
    scm_u8ivect_to_list)

  (define*-scm-procedure (u16ivect->list ivect #:optional start end)
    scm_u16ivect_to_list)

  (define*-scm-procedure (u32ivect->list ivect #:optional start end)
    scm_u32ivect_to_list)

  (define*-scm-procedure (u64ivect->list ivect #:optional start end)
    scm_u64ivect_to_list)

  (define*-scm-procedure (s8ivect->list ivect #:optional start end)
    scm_s8ivect_to_list)

  (define*-scm-procedure (s16ivect->list ivect #:optional start end)
    scm_s16ivect_to_list)

  (define*-scm-procedure (s32ivect->list ivect #:optional start end)
    scm_s32ivect_to_list)

  (define*-scm-procedure (s64ivect->list ivect #:optional start end)
    scm_s64ivect_to_list)

  (define*-scm-procedure (f32ivect->list ivect #:optional start end)
    scm_f32ivect_to_list)

  (define*-scm-procedure (f64ivect->list ivect #:optional start end)
    scm_f64ivect_to_list)
  
  (define*-scm-procedure (c32ivect->list ivect #:optional start end)
    scm_c32ivect_to_list)

  (define*-scm-procedure (c64ivect->list ivect #:optional start end)
    scm_c64ivect_to_list)

  ;;------------------------------------------------------------

  (define-scm-procedure (list->ivect lst)
    scm_list_to_ivect)

  (define-scm-procedure (list->u8ivect lst)
    scm_list_to_u8ivect)

  (define-scm-procedure (list->u16ivect lst)
    scm_list_to_u16ivect)

  (define-scm-procedure (list->u32ivect lst)
    scm_list_to_u32ivect)

  (define-scm-procedure (list->u64ivect lst)
    scm_list_to_u64ivect)

  (define-scm-procedure (list->s8ivect lst)
    scm_list_to_s8ivect)

  (define-scm-procedure (list->s16ivect lst)
    scm_list_to_s16ivect)

  (define-scm-procedure (list->s32ivect lst)
    scm_list_to_s32ivect)

  (define-scm-procedure (list->s64ivect lst)
    scm_list_to_s64ivect)

  (define-scm-procedure (list->f32ivect lst)
    scm_list_to_f32ivect)

  (define-scm-procedure (list->f64ivect lst)
    scm_list_to_f64ivect)
  
  (define-scm-procedure (list->c32ivect lst)
    scm_list_to_c32ivect)

  (define-scm-procedure (list->c64ivect lst)
    scm_list_to_c64ivect)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect->vlst ivect #:optional start end)
    scm_ivect_to_vlst)

  (define*-scm-procedure (u8ivect->vlst ivect #:optional start end)
    scm_u8ivect_to_vlst)

  (define*-scm-procedure (u16ivect->vlst ivect #:optional start end)
    scm_u16ivect_to_vlst)

  (define*-scm-procedure (u32ivect->vlst ivect #:optional start end)
    scm_u32ivect_to_vlst)

  (define*-scm-procedure (u64ivect->vlst ivect #:optional start end)
    scm_u64ivect_to_vlst)

  (define*-scm-procedure (s8ivect->vlst ivect #:optional start end)
    scm_s8ivect_to_vlst)

  (define*-scm-procedure (s16ivect->vlst ivect #:optional start end)
    scm_s16ivect_to_vlst)

  (define*-scm-procedure (s32ivect->vlst ivect #:optional start end)
    scm_s32ivect_to_vlst)

  (define*-scm-procedure (s64ivect->vlst ivect #:optional start end)
    scm_s64ivect_to_vlst)

  (define*-scm-procedure (f32ivect->vlst ivect #:optional start end)
    scm_f32ivect_to_vlst)

  (define*-scm-procedure (f64ivect->vlst ivect #:optional start end)
    scm_f64ivect_to_vlst)
  
  (define*-scm-procedure (c32ivect->vlst ivect #:optional start end)
    scm_c32ivect_to_vlst)

  (define*-scm-procedure (c64ivect->vlst ivect #:optional start end)
    scm_c64ivect_to_vlst)

  ;;------------------------------------------------------------

  (define-scm-procedure (vlst->ivect lst)
    scm_vlst_to_ivect)

  (define-scm-procedure (vlst->u8ivect lst)
    scm_vlst_to_u8ivect)

  (define-scm-procedure (vlst->u16ivect lst)
    scm_vlst_to_u16ivect)

  (define-scm-procedure (vlst->u32ivect lst)
    scm_vlst_to_u32ivect)

  (define-scm-procedure (vlst->u64ivect lst)
    scm_vlst_to_u64ivect)

  (define-scm-procedure (vlst->s8ivect lst)
    scm_vlst_to_s8ivect)

  (define-scm-procedure (vlst->s16ivect lst)
    scm_vlst_to_s16ivect)

  (define-scm-procedure (vlst->s32ivect lst)
    scm_vlst_to_s32ivect)

  (define-scm-procedure (vlst->s64ivect lst)
    scm_vlst_to_s64ivect)

  (define-scm-procedure (vlst->f32ivect lst)
    scm_vlst_to_f32ivect)

  (define-scm-procedure (vlst->f64ivect lst)
    scm_vlst_to_f64ivect)
  
  (define-scm-procedure (vlst->c32ivect lst)
    scm_vlst_to_c32ivect)

  (define-scm-procedure (vlst->c64ivect lst)
    scm_vlst_to_c64ivect)

  ;;------------------------------------------------------------

  (define*-scm-procedure (ivect->vector ivect #:optional start end)
    scm_ivect_to_vector)

  (define*-scm-procedure (u8ivect->vector ivect #:optional start end)
    scm_u8ivect_to_vector)

  (define*-scm-procedure (u16ivect->vector ivect #:optional start end)
    scm_u16ivect_to_vector)

  (define*-scm-procedure (u32ivect->vector ivect #:optional start end)
    scm_u32ivect_to_vector)

  (define*-scm-procedure (u64ivect->vector ivect #:optional start end)
    scm_u64ivect_to_vector)

  (define*-scm-procedure (s8ivect->vector ivect #:optional start end)
    scm_s8ivect_to_vector)

  (define*-scm-procedure (s16ivect->vector ivect #:optional start end)
    scm_s16ivect_to_vector)

  (define*-scm-procedure (s32ivect->vector ivect #:optional start end)
    scm_s32ivect_to_vector)

  (define*-scm-procedure (s64ivect->vector ivect #:optional start end)
    scm_s64ivect_to_vector)

  (define*-scm-procedure (f32ivect->vector ivect #:optional start end)
    scm_f32ivect_to_vector)

  (define*-scm-procedure (f64ivect->vector ivect #:optional start end)
    scm_f64ivect_to_vector)

  (define*-scm-procedure (c32ivect->vector ivect #:optional start end)
    scm_c32ivect_to_vector)

  (define*-scm-procedure (c64ivect->vector ivect #:optional start end)
    scm_c64ivect_to_vector)

  (define*-scm-procedure (u8ivect->u8vector ivect #:optional start end)
    scm_u8ivect_to_u8vector)

  (define*-scm-procedure (u16ivect->u16vector ivect #:optional start end)
    scm_u16ivect_to_u16vector)

  (define*-scm-procedure (u32ivect->u32vector ivect #:optional start end)
    scm_u32ivect_to_u32vector)

  (define*-scm-procedure (u64ivect->u64vector ivect #:optional start end)
    scm_u64ivect_to_u64vector)

  (define*-scm-procedure (s8ivect->s8vector ivect #:optional start end)
    scm_s8ivect_to_s8vector)

  (define*-scm-procedure (s16ivect->s16vector ivect #:optional start end)
    scm_s16ivect_to_s16vector)

  (define*-scm-procedure (s32ivect->s32vector ivect #:optional start end)
    scm_s32ivect_to_s32vector)

  (define*-scm-procedure (s64ivect->s64vector ivect #:optional start end)
    scm_s64ivect_to_s64vector)

  (define*-scm-procedure (f32ivect->f32vector ivect #:optional start end)
    scm_f32ivect_to_f32vector)

  (define*-scm-procedure (f64ivect->f64vector ivect #:optional start end)
    scm_f64ivect_to_f64vector)

  (define*-scm-procedure (c32ivect->c32vector ivect #:optional start end)
    scm_c32ivect_to_c32vector)

  (define*-scm-procedure (c64ivect->c64vector ivect #:optional start end)
    scm_c64ivect_to_c64vector)

  ;;------------------------------------------------------------

  (define-scm-procedure (vector->ivect vect)
    scm_vector_to_ivect)

  (define-scm-procedure (vector->u8ivect vect)
    scm_vector_to_u8ivect)

  (define-scm-procedure (vector->u16ivect vect)
    scm_vector_to_u16ivect)

  (define-scm-procedure (vector->u32ivect vect)
    scm_vector_to_u32ivect)

  (define-scm-procedure (vector->u64ivect vect)
    scm_vector_to_u64ivect)

  (define-scm-procedure (vector->s8ivect vect)
    scm_vector_to_s8ivect)

  (define-scm-procedure (vector->s16ivect vect)
    scm_vector_to_s16ivect)

  (define-scm-procedure (vector->s32ivect vect)
    scm_vector_to_s32ivect)

  (define-scm-procedure (vector->s64ivect vect)
    scm_vector_to_s64ivect)

  (define-scm-procedure (vector->f32ivect vect)
    scm_vector_to_f32ivect)

  (define-scm-procedure (vector->f64ivect vect)
    scm_vector_to_f64ivect)
  
  (define-scm-procedure (vector->c32ivect vect)
    scm_vector_to_c32ivect)

  (define-scm-procedure (vector->c64ivect vect)
    scm_vector_to_c64ivect)

  (define-scm-procedure (u8vector->u8ivect vect)
    scm_u8vector_to_u8ivect)

  (define-scm-procedure (u16vector->u16ivect vect)
    scm_u16vector_to_u16ivect)

  (define-scm-procedure (u32vector->u32ivect vect)
    scm_u32vector_to_u32ivect)

  (define-scm-procedure (u64vector->u64ivect vect)
    scm_u64vector_to_u64ivect)

  (define-scm-procedure (s8vector->s8ivect vect)
    scm_s8vector_to_s8ivect)

  (define-scm-procedure (s16vector->s16ivect vect)
    scm_s16vector_to_s16ivect)

  (define-scm-procedure (s32vector->s32ivect vect)
    scm_s32vector_to_s32ivect)

  (define-scm-procedure (s64vector->s64ivect vect)
    scm_s64vector_to_s64ivect)

  (define-scm-procedure (f32vector->f32ivect vect)
    scm_f32vector_to_f32ivect)

  (define-scm-procedure (f64vector->f64ivect vect)
    scm_f64vector_to_f64ivect)
  
  (define-scm-procedure (c32vector->c32ivect vect)
    scm_c32vector_to_c32ivect)

  (define-scm-procedure (c64vector->c64ivect vect)
    scm_c64vector_to_c64ivect)

  ;;------------------------------------------------------------

  (define-syntax define-ivect-map
    (syntax-rules ()
      [(_ proc-name c-func)
       (define proc-name
         (let ([map1 (lambda*-scm-procedure (proc ivect #:optional ivect2)
                       c-func)]
               [map2 (lambda-scm-procedure (proc ivect1 more-ivects)
                       c-func)])
           (case-lambda
             [(proc ivect) (map1 proc ivect)]
             [(proc ivect1 ivect2) (map1 proc ivect1 ivect2)]
             [(proc ivect1 . more-ivects) (map2 proc ivect1 more-ivects)])))]))

  (define-ivect-map ivect-map scm_ivect_map)
  (define-ivect-map ivect-map-in-order scm_ivect_map_in_order)

  ;;------------------------------------------------------------

  ) ;; end of library.
