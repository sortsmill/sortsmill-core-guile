;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2014, 2015 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core uniform-vectors)

  (export
   list-map->s8vector  ;; (list-map->s8vector proc pyseq1 pyseq2 ...) → s8vector
   list-map->s16vector ;; etc.
   list-map->s32vector
   list-map->s64vector
   list-map->u8vector
   list-map->u16vector
   list-map->u32vector
   list-map->u64vector
   list-map->f32vector
   list-map->f64vector

   s8vector-append  ;; (s8vector-append s8vector1 s8vector2 ...) → s8vector
   s16vector-append ;; etc.
   s32vector-append
   s64vector-append
   u8vector-append
   u16vector-append
   u32vector-append
   u64vector-append
   f32vector-append
   f64vector-append
   c32vector-append
   c64vector-append

   s8vector-concatenate  ;; (s8vector-concatenate list-of-s8vector) → s8vector
   s16vector-concatenate ;; etc.
   s32vector-concatenate
   s64vector-concatenate
   u8vector-concatenate
   u16vector-concatenate
   u32vector-concatenate
   u64vector-concatenate
   f32vector-concatenate
   f64vector-concatenate
   c32vector-concatenate
   c64vector-concatenate)

  (import (rnrs)
          (except (guile) error)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          scm_list_map_to_s8vector
                          scm_list_map_to_s16vector
                          scm_list_map_to_s32vector
                          scm_list_map_to_s64vector
                          scm_list_map_to_u8vector
                          scm_list_map_to_u16vector
                          scm_list_map_to_u32vector
                          scm_list_map_to_u64vector
                          scm_list_map_to_f32vector
                          scm_list_map_to_f64vector
                          scm_s8vector_concatenate
                          scm_s16vector_concatenate
                          scm_s32vector_concatenate
                          scm_s64vector_concatenate
                          scm_u8vector_concatenate
                          scm_u16vector_concatenate
                          scm_u32vector_concatenate
                          scm_u64vector_concatenate
                          scm_f32vector_concatenate
                          scm_f64vector_concatenate
                          scm_c32vector_concatenate
                          scm_c64vector_concatenate))

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (list-map->s8vector proc . obj-lst)
    "A `map' procedure from one or more lists to s8vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_s8vector)

  (define-scm-procedure (list-map->s16vector proc . obj-lst)
    "A `map' procedure from one or more lists to s16vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_s16vector)

  (define-scm-procedure (list-map->s32vector proc . obj-lst)
    "A `map' procedure from one or more lists to s32vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_s32vector)

  (define-scm-procedure (list-map->s64vector proc . obj-lst)
    "A `map' procedure from one or more lists to s64vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_s64vector)

  (define-scm-procedure (list-map->u8vector proc . obj-lst)
    "A `map' procedure from one or more lists to u8vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_u8vector)

  (define-scm-procedure (list-map->u16vector proc . obj-lst)
    "A `map' procedure from one or more lists to u16vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_u16vector)

  (define-scm-procedure (list-map->u32vector proc . obj-lst)
    "A `map' procedure from one or more lists to u32vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_u32vector)

  (define-scm-procedure (list-map->u64vector proc . obj-lst)
    "A `map' procedure from one or more lists to u64vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_u64vector)

  (define-scm-procedure (list-map->f32vector proc . obj-lst)
    "A `map' procedure from one or more lists to f32vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_f32vector)

  (define-scm-procedure (list-map->f64vector proc . obj-lst)
    "A `map' procedure from one or more lists to f64vector. The order
of application is unspecified. FIXME: Write better documentation for
this procedure."
    scm_list_map_to_f64vector)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (s8vector-append . lst)
    "FIXME: Document this."
    scm_s8vector_concatenate)

  (define-scm-procedure (s16vector-append . lst)
    "FIXME: Document this."
    scm_s16vector_concatenate)

  (define-scm-procedure (s32vector-append . lst)
    "FIXME: Document this."
    scm_s32vector_concatenate)

  (define-scm-procedure (s64vector-append . lst)
    "FIXME: Document this."
    scm_s64vector_concatenate)

  (define-scm-procedure (u8vector-append . lst)
    "FIXME: Document this."
    scm_u8vector_concatenate)

  (define-scm-procedure (u16vector-append . lst)
    "FIXME: Document this."
    scm_u16vector_concatenate)

  (define-scm-procedure (u32vector-append . lst)
    "FIXME: Document this."
    scm_u32vector_concatenate)

  (define-scm-procedure (u64vector-append . lst)
    "FIXME: Document this."
    scm_u64vector_concatenate)

  (define-scm-procedure (f32vector-append . lst)
    "FIXME: Document this."
    scm_f32vector_concatenate)

  (define-scm-procedure (f64vector-append . lst)
    "FIXME: Document this."
    scm_f64vector_concatenate)

  (define-scm-procedure (c32vector-append . lst)
    "FIXME: Document this."
    scm_c32vector_concatenate)

  (define-scm-procedure (c64vector-append . lst)
    "FIXME: Document this."
    scm_c64vector_concatenate)

  ;;-----------------------------------------------------------------------

  (define-scm-procedure (s8vector-concatenate lst)
    "FIXME: Document this."
    scm_s8vector_concatenate)

  (define-scm-procedure (s16vector-concatenate lst)
    "FIXME: Document this."
    scm_s16vector_concatenate)

  (define-scm-procedure (s32vector-concatenate lst)
    "FIXME: Document this."
    scm_s32vector_concatenate)

  (define-scm-procedure (s64vector-concatenate lst)
    "FIXME: Document this."
    scm_s64vector_concatenate)

  (define-scm-procedure (u8vector-concatenate lst)
    "FIXME: Document this."
    scm_u8vector_concatenate)

  (define-scm-procedure (u16vector-concatenate lst)
    "FIXME: Document this."
    scm_u16vector_concatenate)

  (define-scm-procedure (u32vector-concatenate lst)
    "FIXME: Document this."
    scm_u32vector_concatenate)

  (define-scm-procedure (u64vector-concatenate lst)
    "FIXME: Document this."
    scm_u64vector_concatenate)

  (define-scm-procedure (f32vector-concatenate lst)
    "FIXME: Document this."
    scm_f32vector_concatenate)

  (define-scm-procedure (f64vector-concatenate lst)
    "FIXME: Document this."
    scm_f64vector_concatenate)

  (define-scm-procedure (c32vector-concatenate lst)
    "FIXME: Document this."
    scm_c32vector_concatenate)

  (define-scm-procedure (c64vector-concatenate lst)
    "FIXME: Document this."
    scm_c64vector_concatenate)

  ;;-----------------------------------------------------------------------

   ) ;; end of library.
