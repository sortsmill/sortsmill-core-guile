;; Copyright (C) 2014 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core rbmap specialized)

  ;;
  ;; FIXME: Document this.
  ;;

  (export define-rbmap-type
          define-rbmap-set-type
          define-rbmap-record-type)

  (import (rnrs)
          (only (srfi :1) alist-delete! filter-map lset-difference)
          (rename (srfi :9)
                  (define-record-type define-srfi9-record-type))
          (rename (srfi srfi-9 gnu)
                  (set-record-type-printer!
                   set-srfi9-record-type-printer!))
          (srfi :26)
          (srfi :42)
          (ice-9 match)
          (ice-9 format)
          (except (guile) error)
          (system foreign)
          (sortsmill core rbmap generic)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo)
          (sortsmill core helpers)
          (sortsmill core scm-procedures))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs (dynamic-link-sortsmill-core-guile)
                          init_guile_sortsmill_rbmap)
    ((pointer->procedure void init_guile_sortsmill_rbmap '())))

  ;;-----------------------------------------------------------------------

  (define (check-onlys stx who only-list names-list)
    (let ([unrecognized-onlys (lset-difference eq? only-list names-list)])
      (unless (null? unrecognized-onlys)
        (syntax-violation who
                          (format #f (_ "unrecognized `only' elements: ~s")
                                  unrecognized-onlys)
                          (syntax->datum stx)
                          `(only ,@only-list)))))

  (define (check-excepts stx who except-list names-list)
    (let ([unrecognized-excepts (lset-difference eq? except-list names-list)])
      (unless (null? unrecognized-excepts)
        (syntax-violation who
                          (format #f (_ "unrecognized `except' elements: ~s")
                                  unrecognized-excepts)
                          (syntax->datum stx)
                          `(except ,@except-list)))))

  (define (make-enabled-names stx who only-list except-list names)
    (let ([names-list (map cadr names)])
      (check-excepts stx who except-list names-list)
      (lset-difference eq?
                       (cond [(not (null? only-list))
                              (check-onlys stx who only-list names-list)
                              only-list]
                             [else names-list])
                       except-list)))

  (define (make-symbol->name stx names)
    (lambda (symb)
      (datum->syntax stx (cadr (assq symb names)))))

  (define (make-symbol->formatted-name stx names)
    (lambda (symb k)
      (let ([fmt (symbol->string (cadr (assq symb names)))])
        (datum->syntax stx (string->symbol (format #f fmt k))))))

  (define (gensym-formatted fmt . args)
    (gensym (apply format #f fmt (map syntax->datum args))))

  (define (coerce-keyword-to-symbol s)
    (if (keyword? s) (keyword->symbol s) s))

  (define (make-define-if-enabled enabled-names)
    (lambda (name value)
      (if (memq (syntax->datum name) enabled-names)
          #`(define #,name #,value)
          #'*unspecified*)))

  (define (extract-renames-from-clauses clauses)
    (apply append
           (filter-map (lambda (clause)
                         (syntax-case clause (only)
                           [(only name name* ...)
                            (filter-map (lambda (n)
                                          (syntax-case n (rename)
                                            [(rename from-name to-name)
                                             #'(rename from-name to-name)]
                                            [_ #f]))
                                        #'(name name* ...))]
                           [_ #f]))
                       clauses)))

  (define (move-embedded-clauses clauses)
    "Move `rename' clauses that are embedded inside an `only' clause
to the top level."
    (append clauses (extract-renames-from-clauses clauses)))

  (define (remove-renames-from-names names)
    (map (lambda (n)
           (syntax-case n (rename)
             [(rename from-name to-name) #'to-name]
             [other #'other]))
         names))

  (define (make-name . components)
    (define (symb->str s) (if (symbol? s) (symbol->string s) s))
    (string->symbol (apply string-append
                           (map (compose symb->str syntax->datum)
                                components))))

  ;;-----------------------------------------------------------------------

  (define-syntax define-rbmap-type
    (lambda (stx)

      (define (mkname . components)
        (datum->syntax stx (apply make-name components)))

      (syntax-case stx ()
        [(_ rbmap-specific rbmap-generic clause ...)
         (let next-clause
             ([clauses (move-embedded-clauses #'(clause ...))]
              [names
               ;; Initialize @var{names} with the defaults. The
               ;; @code{#f} values indicates the entry can be
               ;; overridden.
               `((type ,(make-name #'rbmap-specific) #f)
                 (type? ,(make-name #'rbmap-specific "?") #f)
                 (make-type ,(make-name "make-" #'rbmap-specific) #f)
                 (type-set! ,(make-name #'rbmap-specific "-set!") #f)
                 (type-delete! ,(make-name #'rbmap-specific "-delete!") #f)
                 (type-ref ,(make-name #'rbmap-specific "-ref") #f)
                 (type-fold-left ,(make-name #'rbmap-specific "-fold-left") #f)
                 (type-fold-right ,(make-name #'rbmap-specific "-fold-right") #f)
                 (alist->type ,(make-name "alist->" #'rbmap-specific) #f)
                 (type->alist ,(make-name #'rbmap-specific "->alist") #f)
                 (plist->type ,(make-name "plist->" #'rbmap-specific) #f)
                 (type->plist ,(make-name #'rbmap-specific "->plist") #f)
                 (type-keys ,(make-name #'rbmap-specific "-keys") #f)
                 (type-values ,(make-name #'rbmap-specific "-values") #f)
                 (type-map->list ,(make-name #'rbmap-specific "-map->list") #f)
                 (type-for-each ,(make-name #'rbmap-specific "-for-each") #f)
                 (type-count ,(make-name #'rbmap-specific "-count") #f)
                 (type-size ,(make-name #'rbmap-specific "-size") #f)
                 (type-null? ,(make-name #'rbmap-specific "-null?") #f))]
              [only-list '()]
              [except-list '()]
              [key-preproc #f]
              [key-check #f]
              [printer-proc #f])
           (syntax-case clauses (rename only except key-preprocessor key-predicate printer)
             [((rename default-name substitute-name) . more-clauses)
              (let ([key
                     (let ([entry
                            (car
                             (or (memp (lambda (e)
                                         (eq? (cadr e)
                                              (make-name #'default-name)))
                                       names)
                                 (syntax-violation 'define-rbmap-type
                                                   (_ "unrecognized name in rename")
                                                   (syntax->datum stx)
                                                   (car (syntax->datum clauses)))))])
                       (when (caddr entry)
                         ;; Attempt to override an entry that
                         ;; contains a @code{#t} value.
                         (syntax-violation 'define-rbmap-type
                                           (_ "multiple rename of the same object")
                                           (syntax->datum stx)
                                           (car (syntax->datum clauses))))
                       (car entry))])
                (next-clause #'more-clauses
                             (cons (list key (make-name #'substitute-name) #t)
                                   (alist-delete! key names eq?))
                             only-list except-list key-preproc key-check printer-proc))]
             [((only name name* ...) . more-clauses)
              (next-clause #'more-clauses names
                           (append only-list
                                   (syntax->datum
                                    (remove-renames-from-names #'(name name* ...))))
                           except-list key-preproc key-check printer-proc)]
             [((except name name* ...) . more-clauses)
              (next-clause #'more-clauses names only-list
                           (append except-list (syntax->datum #'(name name* ...)))
                           key-preproc key-check printer-proc)]
             [((key-preprocessor proc) . more-clauses)
              (when key-preproc
                (syntax-violation 'define-rbmap-type
                                  (_ "multiple key preprocessors specified")
                                  (syntax->datum stx)
                                  (car (syntax->datum clauses))))
              (next-clause #'more-clauses names only-list except-list
                           #'proc key-check printer-proc)]
             [((key-predicate proc) . more-clauses)
              (next-clause #'more-clauses names only-list except-list key-preproc
                           (if key-check
                               (lambda (k) (and (key-check k) (#'proc k)))
                               #'proc)
                           printer-proc)]
             [((printer proc) . more-clauses)
              (when printer-proc
                (syntax-violation 'define-rbmap-type
                                  (_ "multiple printers specified")
                                  (syntax->datum stx)
                                  (car (syntax->datum clauses))))
              (next-clause #'more-clauses names only-list except-list
                           key-preproc key-check #'proc)]
             [()
              (with-syntax
                  ;; rbmap->type and type->rbmap are hidden from the
                  ;; outside world. As far as the programmer using
                  ;; it is concerned, a specialized rbmap is just
                  ;; some walkable map with ordered keys of a
                  ;; uniform type.
                  ([rbmap->type (datum->syntax stx (make-symbol "rbmap->type"))]
                   [type->rbmap (datum->syntax stx (make-symbol "type->rbmap"))]

                   ;; Also hidden, but these symbols have to be able
                   ;; to be compiled, so use gensym instead of
                   ;; make-symbol.
                   [key-preproc%
                    (datum->syntax stx (gensym-formatted " ~a key-preproc% "
                                                         #'rbmap-specific))]
                   [key-check%
                    (datum->syntax stx (gensym-formatted " ~a key-check% "
                                                         #'rbmap-specific))])

                (let* ([symbol->name (make-symbol->name stx names)]
                       [enabled-names (make-enabled-names stx 'define-rbmap-type
                                                          only-list except-list
                                                          names)]
                       [define-if-enabled (make-define-if-enabled enabled-names)]
                       [preprocess-key
                        (if key-preproc
                            (lambda (key-stx) #`(key-preproc% #,key-stx))
                            (lambda (key-stx) key-stx))]
                       [preprocess-alist
                        (if key-preproc
                            (lambda (alist-stx)
                              #`(map-alist-keys key-preproc% #,alist-stx))
                            (lambda (alist-stx) alist-stx))]
                       [preprocess-plist
                        (if key-preproc
                            (lambda (plist-stx)
                              #`(map-plist-keys key-preproc% #,plist-stx))
                            (lambda (plist-stx) plist-stx))]
                       [check-key
                        (if key-check
                            (lambda (key-stx)
                              #`(check-return-key key-check%
                                                  #,(preprocess-key key-stx)
                                                  'rbmap-specific))
                            (lambda (key-stx) (preprocess-key key-stx)))]
                       [check-alist
                        (if key-check
                            (lambda (alist-stx)
                              #`(check-return-alist key-check%
                                                    #,(preprocess-alist alist-stx)
                                                    'rbmap-specific))
                            (lambda (alist-stx) (preprocess-alist alist-stx)))]
                       [check-plist
                        (if key-check
                            (lambda (plist-stx)
                              #`(check-return-plist key-check%
                                                    #,(preprocess-plist plist-stx)
                                                    'rbmap-specific))
                            (lambda (plist-stx) (preprocess-plist plist-stx)))])

                  #`(begin

                      #,(if key-preproc
                            #`(define key-preproc% #,key-preproc)
                            #'*unspecified*)

                      #,(if key-check
                            #`(define key-check% #,key-check)
                            #'*unspecified*)

                      (define-srfi9-record-type #,(symbol->name 'type)
                        (rbmap->type rbmap)
                        #,(symbol->name 'type?)
                        (rbmap type->rbmap))

                      (set-srfi9-record-type-printer!
                       #,(symbol->name 'type)
                       (or #,printer-proc
                           (lambda (obj port)
                             (write-char #\# port)
                             (write-char #\< port)
                             (display 'rbmap-specific port)
                             (write-char #\space port)
                             (display 'rbmap-generic port)
                             (display " 0x" port)
                             (display (number->string (object-address obj) 16) port)
                             (write-char #\> port))))

                      #,(define-if-enabled (symbol->name 'make-type)
                          #`(case-lambda
                              [() (rbmap->type (#,(mkname "make-" #'rbmap-generic)))]
                              [(. arguments)
                               (rbmap->type
                                (#,(mkname "plist->" #'rbmap-generic)
                                 #,(check-plist #'arguments)))]))

                      #,(define-if-enabled (symbol->name 'type-set!)
                          #`(lambda (type key value)
                              (#,(mkname #'rbmap-generic "-set!")
                               (type->rbmap type) #,(check-key #'key) value)))

                      #,(define-if-enabled (symbol->name 'type-delete!)
                          #`(lambda (type key)
                              (#,(mkname #'rbmap-generic "-delete!")
                               (type->rbmap type) #,(preprocess-key #'key))))

                      #,(define-if-enabled (symbol->name 'type-ref)
                          #`(lambda* (type key #:optional [default-value #f])
                              (#,(mkname #'rbmap-generic "-ref")
                               (type->rbmap type) #,(preprocess-key #'key) default-value)))

                      #,(define-if-enabled (symbol->name 'type-fold-left)
                          #`(case-lambda
                              [(proc init type)
                               (#,(mkname #'rbmap-generic "-fold-left")
                                proc init (type->rbmap type))]
                              [(proc init type start-key)
                               (#,(mkname #'rbmap-generic "-fold-left")
                                proc init (type->rbmap type)
                                #,(preprocess-key #'start-key))]))

                      #,(define-if-enabled (symbol->name 'type-fold-right)
                          #`(case-lambda
                              [(proc init type)
                               (#,(mkname #'rbmap-generic "-fold-right")
                                proc init (type->rbmap type))]
                              [(proc init type start-key)
                               (#,(mkname #'rbmap-generic "-fold-right")
                                proc init (type->rbmap type)
                                #,(preprocess-key #'start-key))]))

                      #,(define-if-enabled (symbol->name 'alist->type)
                          #`(lambda (alist)
                              (rbmap->type
                               (#,(mkname "alist->" #'rbmap-generic)
                                #,(check-alist #'alist)))))

                      #,(define-if-enabled (symbol->name 'type->alist)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "->alist")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'plist->type)
                          #`(lambda (plist)
                              (rbmap->type
                               (#,(mkname "plist->" #'rbmap-generic)
                                #,(check-plist #'plist)))))

                      #,(define-if-enabled (symbol->name 'type->plist)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "->plist")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-keys)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-keys")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-values)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-values")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-map->list)
                          #`(lambda (proc type)
                              (#,(mkname #'rbmap-generic "-map->list")
                               proc (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-for-each)
                          #`(lambda (proc type)
                              (#,(mkname #'rbmap-generic "-for-each")
                               proc (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-count)
                          #`(lambda (pred type)
                              (#,(mkname #'rbmap-generic "-count")
                               pred (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-size)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-size")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-null?)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-null?")
                               (type->rbmap type))))

                      )))]
             [((bad-clause . _) . more-clauses)
              (syntax-violation 'define-rbmap-type
                                (_ "unrecognized clause prefix")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))]
             [(bad-clause . more-clauses)
              (syntax-violation 'define-rbmap-type
                                (_ "bad clause")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))] ))] )))

  ;;-----------------------------------------------------------------------

  (define-syntax define-rbmap-set-type
    (lambda (stx)

      (define (mkname . components)
        (datum->syntax stx (apply make-name components)))

      (syntax-case stx ()
        [(_ rbmap-specific rbmap-generic clause ...)
         (let next-clause
             ([clauses (move-embedded-clauses #'(clause ...))]
              [names
               ;; Initialize @var{names} with the defaults. The
               ;; @code{#f} values indicates the entry can be
               ;; overridden.
               `((type ,(make-name #'rbmap-specific) #f)
                 (type? ,(make-name #'rbmap-specific "?") #f)
                 (make-type ,(make-name "make-" #'rbmap-specific) #f)
                 (type-add! ,(make-name #'rbmap-specific "-add!") #f)
                 (type-delete! ,(make-name #'rbmap-specific "-delete!") #f)
                 (type-member? ,(make-name #'rbmap-specific "-member?") #f)
                 (type-fold-left ,(make-name #'rbmap-specific "-fold-left") #f)
                 (type-fold-right ,(make-name #'rbmap-specific "-fold-right") #f)
                 (list->type ,(make-name "list->" #'rbmap-specific) #f)
                 (type->list ,(make-name #'rbmap-specific "->list") #f)
                 (type-map->list ,(make-name #'rbmap-specific "-map->list") #f)
                 (type-for-each ,(make-name #'rbmap-specific "-for-each") #f)
                 (type-count ,(make-name #'rbmap-specific "-count") #f)
                 (type-size ,(make-name #'rbmap-specific "-size") #f)
                 (type-null? ,(make-name #'rbmap-specific "-null?") #f))]
              [only-list '()]
              [except-list '()]
              [key-preproc #f]
              [key-check #f]
              [printer-proc #f])
           (syntax-case clauses (rename only except key-preprocessor key-predicate printer)
             [((rename default-name substitute-name) . more-clauses)
              (let ([key
                     (let ([entry
                            (car
                             (or (memp (lambda (e)
                                         (eq? (cadr e)
                                              (make-name #'default-name)))
                                       names)
                                 (syntax-violation 'define-rbmap-set-type
                                                   (_ "unrecognized name in rename")
                                                   (syntax->datum stx)
                                                   (car (syntax->datum clauses)))))])
                       (when (caddr entry)
                         ;; Attempt to override an entry that
                         ;; contains a @code{#t} value.
                         (syntax-violation 'define-rbmap-set-type
                                           (_ "multiple rename of the same object")
                                           (syntax->datum stx)
                                           (car (syntax->datum clauses))))
                       (car entry))])
                (next-clause #'more-clauses
                             (cons (list key (make-name #'substitute-name) #t)
                                   (alist-delete! key names eq?))
                             only-list except-list key-preproc key-check
                             printer-proc))]
             [((only name name* ...) . more-clauses)
              (next-clause #'more-clauses names
                           (append only-list
                                   (syntax->datum
                                    (remove-renames-from-names #'(name name* ...))))
                           except-list key-preproc key-check printer-proc)]
             [((except name name* ...) . more-clauses)
              (next-clause #'more-clauses names only-list
                           (append except-list (syntax->datum #'(name name* ...)))
                           key-preproc key-check printer-proc)]
             [((key-preprocessor proc) . more-clauses)
              (when key-preproc
                (syntax-violation 'define-rbmap-set-type
                                  (_ "multiple key preprocessors specified")
                                  (syntax->datum stx)
                                  (car (syntax->datum clauses))))
              (next-clause #'more-clauses names only-list except-list
                           #'proc key-check printer-proc)]
             [((key-predicate proc) . more-clauses)
              (next-clause #'more-clauses names only-list except-list key-preproc
                           (if key-check
                               (lambda (k) (and (key-check k) (#'proc k)))
                               #'proc)
                           printer-proc)]
             [((printer proc) . more-clauses)
              (when printer-proc
                (syntax-violation 'define-rbmap-set-type
                                  (_ "multiple printers specified")
                                  (syntax->datum stx)
                                  (car (syntax->datum clauses))))
              (next-clause #'more-clauses names only-list except-list
                           key-preproc key-check #'proc)]
             [()
              (with-syntax
                  ;; rbmap->type and type->rbmap are hidden from the
                  ;; outside world. As far as the programmer using
                  ;; it is concerned, a specialized rbmap is just
                  ;; some walkable map with ordered keys of a
                  ;; uniform type.
                  ([rbmap->type (datum->syntax stx (make-symbol "rbmap->type"))]
                   [type->rbmap (datum->syntax stx (make-symbol "type->rbmap"))]

                   ;; Also hidden, but these symbols have to be able
                   ;; to be compiled, so use gensym instead of
                   ;; make-symbol.
                   [key-preproc%
                    (datum->syntax stx (gensym-formatted " ~a key-preproc% "
                                                         #'rbmap-specific))]
                   [key-check%
                    (datum->syntax stx (gensym-formatted " ~a key-check% "
                                                         #'rbmap-specific))])

                (let* ([symbol->name (make-symbol->name stx names)]
                       [enabled-names (make-enabled-names stx 'define-rbmap-set-type
                                                          only-list except-list
                                                          names)]
                       [define-if-enabled (make-define-if-enabled enabled-names)]
                       [preprocess-key
                        (if key-preproc
                            (lambda (key-stx) #`(key-preproc% #,key-stx))
                            (lambda (key-stx) key-stx))]
                       [preprocess-list
                        (if key-preproc
                            (lambda (list-stx) #`(map key-preproc% #,list-stx))
                            (lambda (list-stx) list-stx))]
                       [check-key
                        (if key-check
                            (lambda (key-stx)
                              #`(check-return-key key-check%
                                                  #,(preprocess-key key-stx)
                                                  'rbmap-specific))
                            (lambda (key-stx) (preprocess-key key-stx)))]
                       [check-list
                        (if key-check
                            (lambda (list-stx)
                              #`(check-return-list key-check%
                                                   #,(preprocess-list list-stx)
                                                   'rbmap-specific))
                            (lambda (list-stx) (preprocess-list list-stx)))])

                  #`(begin

                      #,(if key-preproc
                            #`(define key-preproc% #,key-preproc)
                            #'*unspecified*)

                      #,(if key-check
                            #`(define key-check% #,key-check)
                            #'*unspecified*)

                      (define-srfi9-record-type #,(symbol->name 'type)
                        (rbmap->type rbmap)
                        #,(symbol->name 'type?)
                        (rbmap type->rbmap))

                      (set-srfi9-record-type-printer!
                       #,(symbol->name 'type)
                       (or #,printer-proc
                           (lambda (obj port)
                             (write-char #\# port)
                             (write-char #\< port)
                             (display 'rbmap-specific port)
                             (write-char #\space port)
                             (display 'rbmap-generic port)
                             (display " 0x" port)
                             (display (number->string (object-address obj) 16) port)
                             (write-char #\> port))))

                      #,(define-if-enabled (symbol->name 'make-type)
                          #`(case-lambda
                              [() (rbmap->type (#,(mkname "make-" #'rbmap-generic)))]
                              [(. arguments)
                               (rbmap->type
                                (#,(mkname "alist->" #'rbmap-generic)
                                 (map (lambda (e) (cons e #t))
                                      #,(check-list #'arguments))))]))

                      #,(define-if-enabled (symbol->name 'type-add!)
                          #`(lambda (type . elements)
                              (for-each
                               (lambda (key)
                                 (#,(mkname #'rbmap-generic "-set!")
                                  (type->rbmap type) #,(check-key #'key) #t))
                               elements)))

                      #,(define-if-enabled (symbol->name 'type-delete!)
                          #`(lambda (type . elements)
                              (for-each
                               (lambda (key)
                                 (#,(mkname #'rbmap-generic "-delete!")
                                  (type->rbmap type) #,(preprocess-key #'key)))
                               elements)))

                      #,(define-if-enabled (symbol->name 'type-member?)
                          #`(lambda (type key)
                              (#,(mkname #'rbmap-generic "-ref")
                               (type->rbmap type) #,(preprocess-key #'key))))

                      #,(define-if-enabled (symbol->name 'type-fold-left)
                          #`(case-lambda
                              [(proc init type)
                               (#,(mkname #'rbmap-generic "-fold-left")
                                (lambda (prior key value) (proc prior key))
                                init (type->rbmap type))]
                              [(proc init type start-key)
                               (#,(mkname #'rbmap-generic "-fold-left")
                                (lambda (prior key value) (proc prior key))
                                init (type->rbmap type) #,(preprocess-key #'start-key))]))

                      #,(define-if-enabled (symbol->name 'type-fold-right)
                          #`(case-lambda
                              [(proc init type)
                               (#,(mkname #'rbmap-generic "-fold-right")
                                (lambda (key value prior) (proc key prior))
                                init (type->rbmap type))]
                              [(proc init type start-key)
                               (#,(mkname #'rbmap-generic "-fold-right")
                                (lambda (key value prior) (proc key prior))
                                init (type->rbmap type) #,(preprocess-key #'start-key))]))

                      #,(define-if-enabled (symbol->name 'list->type)
                          #`(lambda (lst)
                              (rbmap->type
                               (#,(mkname "alist->" #'rbmap-generic)
                                (map (lambda (e) (cons e #t))
                                     #,(check-list #'lst))))))

                      #,(define-if-enabled (symbol->name 'type->list)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-keys")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-map->list)
                          #`(lambda (proc type)
                              (#,(mkname #'rbmap-generic "-map->list")
                               (lambda (key value) (proc key))
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-for-each)
                          #`(lambda (proc type)
                              (#,(mkname #'rbmap-generic "-for-each")
                               (lambda (key value) (proc key))
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-count)
                          #`(lambda (pred type)
                              (#,(mkname #'rbmap-generic "-count")
                               (lambda (key value) (pred key))
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-size)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-size")
                               (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-null?)
                          #`(lambda (type)
                              (#,(mkname #'rbmap-generic "-null?")
                               (type->rbmap type))))

                      )))]
             [((bad-clause . _) . more-clauses)
              (syntax-violation 'define-rbmap-set-type
                                (_ "unrecognized clause prefix")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))]
             [(bad-clause . more-clauses)
              (syntax-violation 'define-rbmap-set-type
                                (_ "bad clause")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))] ))] )))

  ;;-----------------------------------------------------------------------

  (define-syntax define-rbmap-record-type
    (lambda (stx)
      (syntax-case stx ()
        [(_ rbmap-specific clause ...)
         (let next-clause
             ([clauses (move-embedded-clauses #'(clause ...))]
              [names
               ;; Initialize @var{names} with the defaults. The
               ;; @code{#f} values indicates the entry can be
               ;; overridden.
               `((type ,(make-name #'rbmap-specific) #f)
                 (type? ,(make-name #'rbmap-specific "?") #f)
                 (make-type ,(make-name "make-" #'rbmap-specific) #f)
                 (type-field ,(make-name #'rbmap-specific "-field") #f)
                 (type-set! ,(make-name #'rbmap-specific "-set!") #f)
                 (type-delete! ,(make-name #'rbmap-specific "-delete!") #f)
                 (type-ref ,(make-name #'rbmap-specific "-ref") #f)
                 (alist->type ,(make-name "alist->" #'rbmap-specific) #f)
                 (type->alist ,(make-name #'rbmap-specific "->alist") #f)
                 (plist->type ,(make-name "plist->" #'rbmap-specific) #f)
                 (type->plist ,(make-name #'rbmap-specific "->plist") #f)
                 (type-keys ,(make-name #'rbmap-specific "-keys") #f)
                 (type-keywords ,(make-name #'rbmap-specific "-keywords") #f)
                 (type-keyword? ,(make-name #'rbmap-specific "-keyword?") #f)
                 (type:~a ,(make-name #'rbmap-specific ":~a") #f)
                 (type:~a-ref ,(make-name #'rbmap-specific ":~a-ref") #f)
                 (type:~a-set! ,(make-name #'rbmap-specific ":~a-set!") #f)
                 (type:~a-delete! ,(make-name #'rbmap-specific ":~a-delete!") #f))]
              [only-list '()]
              [except-list '()]
              [key-list '()]
              [printer-proc #f])
           (syntax-case clauses (rename only except keys printer)
             [((rename default-name substitute-name) . more-clauses)
              (let ([key
                     (let ([entry
                            (car
                             (or (memp (lambda (e)
                                         (eq? (cadr e)
                                              (make-name #'default-name)))
                                       names)
                                 (syntax-violation 'define-rbmap-record-type
                                                   (_ "unrecognized name in rename")
                                                   (syntax->datum stx)
                                                   (car (syntax->datum clauses)))))])
                       (when (caddr entry)
                         ;; Attempt to override an entry that
                         ;; contains a @code{#t} value.
                         (syntax-violation 'define-rbmap-record-type
                                           (_ "multiple rename of the same object")
                                           (syntax->datum stx)
                                           (car (syntax->datum clauses))))
                       (car entry))])
                (next-clause #'more-clauses
                             (cons (list key (make-name #'substitute-name) #t)
                                   (alist-delete! key names eq?))
                             only-list except-list key-list printer-proc))]
             [((only name name* ...) . more-clauses)
              (next-clause #'more-clauses names
                           (append only-list
                                   (syntax->datum
                                    (remove-renames-from-names #'(name name* ...))))
                           except-list key-list printer-proc)]
             [((except name name* ...) . more-clauses)
              (next-clause #'more-clauses names only-list
                           (append except-list (syntax->datum #'(name name* ...)))
                           key-list printer-proc)]
             [((keys kv kv* ...) . more-clauses)
              (next-clause #'more-clauses names only-list except-list
                           (append
                            key-list
                            (map (lambda (kv^)
                                   (syntax-case kv^ ()
                                     [(key value)
                                      ;; A default value was given.
                                      (let ([k (coerce-keyword-to-symbol
                                                (syntax->datum #'key))])
                                        (cons k #'value))]
                                     [key
                                      ;; No default value was
                                      ;; given. Implicitly the default is
                                      ;; @code{#f}.
                                      (let ([k (coerce-keyword-to-symbol
                                                (syntax->datum #'key))])
                                        (cons k (datum->syntax stx #f)))]))
                                 #'(kv kv* ...)))
                           printer-proc)]
             [((printer proc) . more-clauses)
              (when printer-proc
                (syntax-violation 'define-rbmap-record-type
                                  (_ "multiple printers specified")
                                  (syntax->datum stx)
                                  (car (syntax->datum clauses))))
              (next-clause #'more-clauses names only-list except-list
                           key-list #'proc)]
             [()
              (when (null? key-list)
                (syntax-violation 'define-rbmap-record-type
                                  (_ "no keys specified")
                                  (syntax->datum stx)))
              (with-syntax
                  ;; rbmap->type and type->rbmap are hidden from the
                  ;; outside world. As far as the programmer using
                  ;; it is concerned, a specialized rbmap is just
                  ;; some walkable map with ordered keys of a
                  ;; uniform type.
                  ([rbmap->type (datum->syntax stx (make-symbol "rbmap->type"))]
                   [type->rbmap (datum->syntax stx (make-symbol "type->rbmap"))]

                   ;; These symbols also are hidden, but have to be
                   ;; able to be compiled, so use gensym instead of
                   ;; make-symbol.
                   [key-check%
                    (datum->syntax stx (gensym-formatted " ~a key-check% "
                                                         #'rbmap-specific))]
                   [default-value%
                     (datum->syntax stx (gensym-formatted " ~a default-value% "
                                                          #'rbmap-specific))])

                (let* ([symbol->name (make-symbol->name stx names)]
                       [symbol->formatted-name (make-symbol->formatted-name stx names)]
                       [enabled-names (make-enabled-names stx 'define-rbmap-record-type
                                                          only-list except-list
                                                          names)]
                       [define-if-enabled (make-define-if-enabled enabled-names)]
                       [keyword-list (map symbol->keyword (map car key-list))])

                  #`(begin

                      (define key-check%
                        (cute rbmapq-ref
                              (alist->rbmapq '#,(datum->syntax
                                                 stx (map (cut cons <> #t)
                                                          keyword-list)))
                              <>))

                      (define default-value%
                        (cute rbmapq-ref
                              (alist->rbmapq
                               '#,(map (match-lambda
                                        [(k . v)
                                         (cons (datum->syntax
                                                stx (symbol->keyword k)) v)])
                                       key-list))
                              <>))

                      (define-srfi9-record-type #,(symbol->name 'type)
                        (rbmap->type rbmap)
                        #,(symbol->name 'type?)
                        (rbmap type->rbmap))

                      (set-srfi9-record-type-printer!
                       #,(symbol->name 'type)
                       (or #,printer-proc
                           (lambda (obj port)
                             (write-char #\# port)
                             (write-char #\< port)
                             (display 'rbmap-specific port)
                             (write-char #\space port)
                             (display 'rbmapq port)
                             (display " 0x" port)
                             (display (number->string (object-address obj) 16) port)
                             (write-char #\> port))))

                      #,(define-if-enabled (symbol->name 'make-type)
                          #`(case-lambda
                              [() (rbmap->type (make-rbmapq))]
                              [(. arguments)
                               (rbmap->type
                                (plist->rbmapq
                                 (check-return-plist key-check% arguments
                                                     'rbmap-specific)))]))

                      #,(define-if-enabled (symbol->name 'type-field)
                          #`(let ([no-default-given (make-symbol "no-default-given")])
                              (make-procedure-with-setter
                               (lambda* (key type #:optional
                                             [default-value no-default-given])
                                 (if (eq? default-value no-default-given)
                                     (rbmapq-ref (type->rbmap type) key
                                                 (default-value% key))
                                     (rbmapq-ref (type->rbmap type) key
                                                 default-value)))
                               (lambda (key type value)
                                 (rbmapq-set!
                                  (type->rbmap type)
                                  (check-return-key key-check% key 'rbmap-specific)
                                  value)))))

                      #,(define-if-enabled (symbol->name 'type-set!)
                          #`(lambda (type key value)
                              (rbmapq-set!
                               (type->rbmap type)
                               (check-return-key key-check% key 'rbmap-specific)
                               value)))

                      #,(define-if-enabled (symbol->name 'type-delete!)
                          #`(lambda (type key)
                              (rbmapq-delete! (type->rbmap type) key)))

                      #,(define-if-enabled (symbol->name 'type-ref)
                          #`(let ([no-default-given (make-symbol "no-default-given")])
                              (lambda* (type key #:optional
                                             [default-value no-default-given])
                                (if (eq? default-value no-default-given)
                                    (rbmapq-ref (type->rbmap type) key
                                                (default-value% key))
                                    (rbmapq-ref (type->rbmap type) key
                                                default-value)))))

                      #,(define-if-enabled (symbol->name 'alist->type)
                          #`(lambda (alist)
                              (rbmap->type
                               (alist->rbmapq
                                (check-return-alist key-check% alist
                                                    'rbmap-specific)))))

                      #,(define-if-enabled (symbol->name 'type->alist)
                          #`(lambda (type)
                              (rbmapq->alist (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'plist->type)
                          #`(lambda (plist)
                              (rbmap->type
                               (plist->rbmapq
                                (check-return-plist key-check% plist
                                                    'rbmap-specific)))))

                      #,(define-if-enabled (symbol->name 'type->plist)
                          #`(lambda (type)
                              (rbmapq->plist (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-keys)
                          #`(lambda (type)
                              (rbmapq-keys (type->rbmap type))))

                      #,(define-if-enabled (symbol->name 'type-keywords)
                          #`(quote #,keyword-list))

                      #,(define-if-enabled (symbol->name 'type-keyword?)
                          #'key-check%)

                      #,@(if (memq (syntax->datum (symbol->name 'type:~a))
                                   enabled-names)
                             (list-ec
                              (:list k (map car key-list))
                              #`(begin
                                  (define #,(symbol->formatted-name 'type:~a k)
                                    (let ([defval
                                            #,(datum->syntax stx (assq-ref key-list k))])
                                      (make-procedure-with-setter
                                       (lambda* (type #:optional [default-value defval])
                                         (rbmapq-ref
                                          (type->rbmap type)
                                          #,(datum->syntax stx (symbol->keyword k))
                                          default-value))
                                       (lambda (type value)
                                         (rbmapq-set!
                                          (type->rbmap type)
                                          #,(datum->syntax stx (symbol->keyword k))
                                          value)))))))
                             '())

                      #,@(if (memq (syntax->datum (symbol->name 'type:~a-ref))
                                   enabled-names)
                             (list-ec
                              (:list k (map car key-list))
                              #`(begin
                                  (define #,(symbol->formatted-name 'type:~a-ref k)
                                    (let ([defval
                                            #,(datum->syntax stx (assq-ref key-list k))])
                                      (lambda* (type #:optional [default-value defval])
                                        (rbmapq-ref
                                         (type->rbmap type)
                                         #,(datum->syntax stx (symbol->keyword k))
                                         default-value))))))
                             '())

                      #,@(if (memq (syntax->datum (symbol->name 'type:~a-set!))
                                   enabled-names)
                             (list-ec
                              (:list k (map car key-list))
                              #`(begin
                                  (define #,(symbol->formatted-name 'type:~a-set! k)
                                    (lambda (type value)
                                      (rbmapq-set! (type->rbmap type)
                                                   #,(datum->syntax stx (symbol->keyword k))
                                                   value)))))
                             '())

                      #,@(if (memq (syntax->datum (symbol->name 'type:~a-delete!))
                                   enabled-names)
                             (list-ec
                              (:list k (map car key-list))
                              #`(begin
                                  (define #,(symbol->formatted-name 'type:~a-delete! k)
                                    (lambda (type)
                                      (rbmapq-delete! (type->rbmap type)
                                                      #,(datum->syntax
                                                         stx (symbol->keyword k)))))))
                             '())

                      )))]
             [((bad-clause . _) . more-clauses)
              (syntax-violation 'define-rbmap-record-type
                                (_ "unrecognized clause prefix")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))]
             [(bad-clause . more-clauses)
              (syntax-violation 'define-rbmap-record-type
                                (_ "bad clause")
                                (car (syntax->datum clauses))
                                (syntax->datum #'bad-clause))] ))] )))

  ;;-----------------------------------------------------------------------

  ) ;; end of library.
