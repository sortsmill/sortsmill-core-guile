;; -*- mode: scheme; coding: utf-8 -*-

;; Copyright (C) 2015, 2017 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(library (sortsmill core hashmaps)

  ;;
  ;; Immutable hashmaps.
  ;;
  ;; These are implemented as trees rather than arrays. Thus the range
  ;; of hash values is very large, independently of the size of the
  ;; map. Furthermore, the entries can be recovered by walking the
  ;; tree; for this purpose there is an immutable ‘hashmap-cursor’
  ;; type.
  ;;
  ;; (The current implementation uses array mapped tries and can
  ;; benefit from a CPU that has population count instructions, if
  ;; Sorts Mill Core Guile was compiled with compiler options enabling
  ;; such instructions, such as -mpopcnt or -march=native on GCC.)
  ;;

  (export

   hashmap?
   make-hashmap
   make-eq-hashmap
   make-eqv-hashmap
   hashmap-hash-function
   hashmap-equivalence-function
   hashmap-null?
   hashmap-size
   hashmap-contains?
   hashmap-ref
   hashmap-set
   hashmap-remove

   hashmap-cursor?
   make-hashmap-cursor
   hashmap-cursor-null?
   hashmap-cursor-next
   hashmap-cursor-entry
   hashmap-cursor-key
   hashmap-cursor-value

   hashmap->alist
   hashmap-set-from-alist
   alist->hashmap
   alist->eq-hashmap
   alist->eqv-hashmap

   ;; (hashmap-equal? x y [values-equal-proc = equal?])
   ;;
   ;; Two hashmaps are considered equal if they refer to the same
   ;; structure. Otherwise they are considered equal if and only if
   ;; they have the same equivalence function (according to ‘eq?’),
   ;; their key sets are equivalent, and the values corresponding to
   ;; equivalent keys are equal.
   ;;
   ;; If x and y both are hashmaps, then ‘(equal? x y)’ is equivalent
   ;; to ‘(hashmap-equal? x y)’.
   hashmap-equal?

   )

  (import (except (rnrs)
                  ;; We want to be able to redefine the following with
                  ;; GOOPS; importing them from (rnrs) might cause
                  ;; trouble.
                  write equal?)
          (except (guile) error)
          (oop goops)
          (system foreign)
          (sortsmill core helpers)
          (sortsmill core scm-procedures)
          (sortsmill core i18n)
          (sortsmill smcoreguile-pkginfo))

  (define-gettext-for-domain _ SMCOREGUILE_PACKAGE)

  (eval-when (compile load eval)
    (expose-foreign-funcs
     (dynamic-link-sortsmill-core-guile)

     scm_hashmap_p
     scm_make_hashmap
     scm_make_eq_hashmap
     scm_make_eqv_hashmap
     scm_hashmap_hash_function
     scm_hashmap_equivalence_function
     scm_hashmap_null_p
     scm_hashmap_size
     scm_hashmap_contains_p
     scm_hashmap_ref
     scm_hashmap_set
     scm_hashmap_remove

     scm_hashmap_cursor_p
     scm_make_hashmap_cursor
     scm_hashmap_cursor_null_p
     scm_hashmap_cursor_next
     scm_hashmap_cursor_entry
     scm_hashmap_cursor_key
     scm_hashmap_cursor_value

     scm_hashmap_to_alist
     scm_hashmap_set_from_alist
     scm_alist_to_hashmap
     scm_alist_to_eq_hashmap
     scm_alist_to_eqv_hashmap

     scm_hashmap_equal_p

     guile_init_smcoreguile_hashmaps)

    ((pointer->procedure void guile_init_smcoreguile_hashmaps '())))

  (define-generic write)

  (define-method (write (obj <hashmap>) port)
    (display "#<hashmap" port)
    (let loop ([cursor (make-hashmap-cursor obj)])
      (unless (hashmap-cursor-null? cursor)
        (display " " port)
        (write (hashmap-cursor-entry cursor) port)
        (loop (hashmap-cursor-next cursor))))
    (display ">" port))

  (define-method (write (obj <hashmap-cursor>) port)
    (display "#<hashmap-cursor " port)
    (if (hashmap-cursor-null? obj)
        (display "null" port)
        (write (hashmap-cursor-entry obj) port))
    (display ">" port))

  (define-generic equal?)
  (define-method (equal? (x <hashmap>) (y <hashmap>))
    (hashmap-equal? x y))

  ;;------------------------------------------------------------

  (define-scm-procedure (hashmap? obj)
    scm_hashmap_p)

  (define*-scm-procedure (make-hashmap #:optional hash-proc equal-proc)
    scm_make_hashmap)

  (define-scm-procedure (make-eq-hashmap)
    scm_make_eq_hashmap)

  (define-scm-procedure (make-eqv-hashmap)
    scm_make_eqv_hashmap)

  (define-scm-procedure (hashmap-hash-function map)
    scm_hashmap_hash_function)

  (define-scm-procedure (hashmap-equivalence-function map)
    scm_hashmap_equivalence_function)

  (define-scm-procedure (hashmap-null? obj)
    scm_hashmap_null_p)

  (define-scm-procedure (hashmap-size obj)
    scm_hashmap_size)

  (define-scm-procedure (hashmap-contains? map key)
    scm_hashmap_contains_p)

  (define*-scm-procedure (hashmap-ref map key #:optional default-value)
    scm_hashmap_ref)

  (define-scm-procedure (hashmap-set map key value)
    scm_hashmap_set)

  (define-scm-procedure (hashmap-remove map key)
    scm_hashmap_remove)

  ;;------------------------------------------------------------

  (define-scm-procedure (hashmap-cursor? obj)
    scm_hashmap_cursor_p)

  (define-scm-procedure (make-hashmap-cursor map)
    scm_make_hashmap_cursor)

  (define-scm-procedure (hashmap-cursor-null? obj)
    scm_hashmap_cursor_null_p)

  (define-scm-procedure (hashmap-cursor-next cursor)
    scm_hashmap_cursor_next)

  (define-scm-procedure (hashmap-cursor-entry cursor)
    scm_hashmap_cursor_entry)

  (define-scm-procedure (hashmap-cursor-key cursor)
    scm_hashmap_cursor_key)

  (define-scm-procedure (hashmap-cursor-value cursor)
    scm_hashmap_cursor_value)

  ;;------------------------------------------------------------

  (define-scm-procedure (hashmap->alist map)
    scm_hashmap_to_alist)

  (define-scm-procedure (hashmap-set-from-alist map alist)
    scm_hashmap_set_from_alist)

  (define*-scm-procedure (alist->hashmap alist #:optional hash-proc equal-proc)
    scm_alist_to_hashmap)

  (define-scm-procedure (alist->eq-hashmap alist)
    scm_alist_to_eq_hashmap)

  (define-scm-procedure (alist->eqv-hashmap alist)
    scm_alist_to_eqv_hashmap)

  ;;------------------------------------------------------------

  (define*-scm-procedure (hashmap-equal? x y #:optional equal-proc)
    scm_hashmap_equal_p)

  ;;------------------------------------------------------------

  ) ;; end of library.
