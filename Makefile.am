# -*- coding: utf-8; tab-width: 4 -*-
#
# Copyright (C) 2013, 2014, 2015, 2017 Khaled Hosny and Barry Schwartz
#
# This file is part of Sorts Mill Core Guile.
# 
# Sorts Mill Core Guile is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# Sorts Mill Core Guile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

###
### FIXME: ‘make -j9 clean install’ perhaps does not work reliably. I
### am trying making some things ‘DISTCLEANFILES’ rather than more
### zealously cleaned up, to see if that alleviates the problem.
###
### I think it still is not reliable, however. And, if I turn off
### ‘QUICKGOALS’, the command fails.
###

$(eval include library.mk)

# See
# http://www.gnu.org/software/libtool/manual/libtool.html#Versioning
VERSION_INFO = -version-info 0:0:0

ACLOCAL_AMFLAGS = -I m4 --install
SUBDIRS = lib c-from-ats .

AM_DISTCHECK_CONFIGURE_FLAGS = --disable-po-editing --without-ats

# The first 1000 digits of π
PI_DIGITS := $(shell GUILE_AUTO_COMPILE=0 $(GUILE) $(srcdir)/compute-pi 1000 div)

AM_CPPFLAGS =
AM_CFLAGS = $(CFLAG_VISIBILITY)

apiidir = $(libdir)/sortsmill
nodist_apii_DATA =

# An alias for libdir. This exists mainly as an aid in controlling
# installation order of libraries, to work around limitations of
# Automake. See
# http://lists.gnu.org/archive/html/libtool/2011-03/msg00004.html
pysupportlibdir = $(libdir)
#
install_pysupportlibLTLIBRARIES = install-pysupportlibLTLIBRARIES
$(install_pysupportlibLTLIBRARIES): install-libLTLIBRARIES
install_pyexecLTLIBRARIES = install-pyexecLTLIBRARIES
$(install_pyexecLTLIBRARIES): $(install_pysupportlibLTLIBRARIES)

EXTRA_DIST = m4/gnulib-cache.m4 c-from-ats-postprocessing.awk compute-pi
bin_PROGRAMS =
noinst_PROGRAMS =
bin_SCRIPTS =
nodist_bin_SCRIPTS =
noinst_SCRIPTS =
lib_LTLIBRARIES =
pysupportlib_LTLIBRARIES =
noinst_LTLIBRARIES =
nodist_man_MANS =
M4HEADERS =
BUILT_SOURCES =
MOSTLYCLEANFILES =
CLEANFILES = $(nodist_man_MANS)
DISTCLEANFILES = Makefile GNUmakefile lib/Makefile lib/GNUmakefile	\
	library.mk
MAINTAINERCLEANFILES =
PRECIOUSFILES =

M4SUGAR_FLAGS = \
	-I$(srcdir)/m4sugar \
	-I$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR)/sortsmill \
	-I$(srcdir)/$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR)/sortsmill \
	-Iinc/sortsmill \
	-I$(srcdir)/inc/sortsmill
m4sugar_to_script = $(call m4sugar_rule, $(1)); chmod +x $$(@)
$(eval $(call m4sugar_rule, %.h: %.h.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, %.c: %.c.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, %-@GEV@.pc.in: %.pc.in.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, %-@GEV@-py@PYV@.pc.in: %-py.pc.in.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, %.in: %.in.m4 $(M4HEADERS)))
$(eval $(call m4sugar_to_script, %: %.m4 $(M4HEADERS)))
if WITH_ATS
$(eval $(call m4sugar_rule, $(srcdir)/%.cats: %.cats.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, $(srcdir)/%.sats: %.sats.m4 $(M4HEADERS)))
$(eval $(call m4sugar_rule, $(srcdir)/%.dats: %.dats.m4 $(M4HEADERS)))
PRECIOUSFILES += $(srcdir)/%.cats $(srcdir)/%.sats $(srcdir)/%.dats
endif WITH_ATS
DISTCLEANFILES += quadrigraph-tool
EXTRA_DIST += m4sugar/m4citrus.m4
EXTRA_DIST += m4sugar/m4sugar.m4
EXTRA_DIST += m4sugar/foreach.m4

INCLUDEMKFILES =

$(eval include other-packages-info.mk)
DISTCLEANFILES += other-packages-info.mk

AM_CPPFLAGS += \
	-Ilib -I$(top_srcdir)/lib \
	-I$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR) \
	-I$(srcdir)/$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR) \
	-Iinc -I$(top_srcdir)/inc \
	-I$(top_srcdir)/ats-include/ccomp/runtime
AM_CFLAGS += $(WARNING_CFLAGS)

AM_ATSOPTFLAGS = \
	--gline \
	-IATS $(srcdir)/ats-include \
	-IATS $(SORTSMILL_CORE_INCLUDEDIR) \
	-IATS $(builddir)/inc -IATS $(srcdir)/inc \
	$(if $(PATSHOME),-IATS $(PATSHOME))
ATS_DEPGEN = $(PATSOPT) --depgen $(AM_ATSOPTFLAGS) $(ATSOPTFLAGS)
ATS_COMPILE = $(PATSOPT) $(AM_ATSOPTFLAGS) $(ATSOPTFLAGS)
sats_compile = set -e; \
	$(MKDIR_P) $(@D); \
	$(ATS_COMPILE) --static $(<) > $(strip $(@))-tmp; \
	$(AWK) -f $(srcdir)/c-from-ats-postprocessing.awk $(strip $(@))-tmp > $(@); \
	rm $(strip $(@))-tmp
dats_compile = set -e; \
	$(MKDIR_P) $(@D); \
	$(ATS_COMPILE) --dynamic $(<) > $(strip $(@))-tmp; \
	$(AWK) -f $(srcdir)/c-from-ats-postprocessing.awk $(strip $(@))-tmp > $(@); \
	rm $(strip $(@))-tmp
PRECIOUSFILES += $(srcdir)/c-from-ats/%_sats.c $(srcdir)/c-from-ats/%_dats.c
if WITH_ATS
ATS_DEPGEN_MODIFY = sed -e 's|^[ 	]*.*/\([^:]*\).o[ 	]*:|$(srcdir)/c-from-ats/\1.c :|'
.deps/%.sats.d: $(srcdir)/inc/sortsmill/guile/core/SATS/%.sats
	@set -e; \
		$(MKDIR_P) $(@D); \
		rm -f $(@); \
		$(ATS_DEPGEN) --static $(<) > $(@).$$$$; \
		($(ATS_DEPGEN_MODIFY) < $(@).$$$$ || echo) > $(@); \
		rm -f $(@).$$$$
.deps/%.dats.d: $(srcdir)/inc/sortsmill/guile/core/DATS/%.dats
	@set -e; \
		$(MKDIR_P) $(@D); \
		rm -f $(@); \
		$(ATS_DEPGEN) --dynamic $(<) > $(@).$$$$; \
		($(ATS_DEPGEN_MODIFY) < $(@).$$$$ || echo) > $(@); \
		rm $(@).$$$$
$(srcdir)/c-from-ats/%_sats.c: $(srcdir)/inc/sortsmill/guile/core/SATS/%.sats c-from-ats-postprocessing.awk
	$(call v, ATSOPT)$(call sats_compile)
$(srcdir)/c-from-ats/%_dats.c: $(srcdir)/inc/sortsmill/guile/core/DATS/%.dats c-from-ats-postprocessing.awk
	$(call v, ATSOPT)$(call dats_compile)
endif WITH_ATS

DIST_SORTSMILLHEADERS =
NODIST_SORTSMILLHEADERS =

DIST_ATSSOURCES =
NODIST_ATSSOURCES =
ATSSOURCES = $(DIST_ATSSOURCES) $(NODIST_ATSSOURCES)
DIST_SORTSMILLHEADERS += $(DIST_ATSSOURCES)
if WITH_ATS
$(eval -include $(patsubst %,.deps/%.d,$(notdir $(filter %.sats,$(ATSSOURCES)))))
$(eval -include $(patsubst %,.deps/%.d,$(notdir $(filter %.dats,$(ATSSOURCES)))))
endif WITH_ATS

#--------------------------------------------------------------------------

if with_python
CORE_GUILE_COMMAND = sortsmill-core-guile-py@PYV@
SORTSMILL_PYTHON_VERSION=@PYV@
else !with_python
CORE_GUILE_COMMAND = sortsmill-core-guile
SORTSMILL_PYTHON_VERSION=
endif !with_python

GUILE_WARNINGS = \
	-Wunbound-variable \
	-Warity-mismatch \
	-Wduplicate-case-datum \
	-Wbad-case-datum \
	-Wformat

GUILE_ENV = \
	GUILE_AUTO_COMPILE=0 \
	GUILE='$(builddir)/$(CORE_GUILE_COMMAND) --' \
	GUILE_LOAD_PATH="$(abs_top_builddir)/guile:$(abs_top_srcdir)/guile`test -n \"$${GUILE_LOAD_PATH}\" && echo \":$${GUILE_LOAD_PATH}\"`" \
	GUILE_LOAD_COMPILED_PATH="$(abs_top_builddir)/guile`test -n \"$${GUILE_LOAD_COMPILED_PATH}\" && echo \":$${GUILE_LOAD_COMPILED_PATH}\"`" \
	SORTSMILL_PYTHON_VERSION="$(SORTSMILL_PYTHON_VERSION)"

CORE_GUILE_COMPILE = \
	$(GUILE_ENV) $(GUILD) compile $(GUILE_WARNINGS)

GUILE_INTERPRET = $(GUILE_ENV) $(GUILE)
CORE_GUILE_INTERPRET = $(GUILE_ENV) ./$(CORE_GUILE_COMMAND)

#-------------------------------------------------------------------------

include ats-include.am
EXTRA_DIST += make-ats-include.am.sh

#-------------------------------------------------------------------------

bin_PROGRAMS += sortsmill-core-guile
lib_LTLIBRARIES += libsortsmill-core-guile-@GEV@.la
if with_python
bin_PROGRAMS += sortsmill-core-guile-py@PYV@
pysupportlib_LTLIBRARIES += libsortsmill-core-guile-@GEV@-py@PYV@.la
endif with_python

$(eval $(call prefixed-install, inc, include, HEADER))
install-data-local: install-prefixed_inc_include_HEADERZ
uninstall-local: uninstall-prefixed_inc_include_HEADERZ
prefixed_inc_dist_include_HEADERZ = $(DIST_SORTSMILLHEADERS:inc/%=%)
prefixed_inc_nodist_include_HEADERZ = $(NODIST_SORTSMILLHEADERS:inc/%=%)

DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/alists_of_groups.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/arrays.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/atomic_ops.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/bincoef.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/bivariate_polynomials.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/brentroot.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/complex.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/delayed_evaluation.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/eval.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/exotic_objects.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/format.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/gmp.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/hashmaps.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/hobby.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/immutable_vectors.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/linear_systems.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/lists.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/modules.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/polynomials.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/popcount.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/postscript.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/rbmap.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/rnrs_conditions.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/rnrs_enums.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/scm_one_time_initialization.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/uniform_vectors.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/vlists.h
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/wrap.h
if with_python
DIST_SORTSMILLHEADERS += inc/sortsmill/guile/core/python.h
endif with_python

libsortsmill_core_guile_@GEV@_la_SOURCES =
nodist_libsortsmill_core_guile_@GEV@_la_SOURCES =
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_alists_of_groups.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_arrays.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_atomic_ops.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_bincoef.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_bivariate_polynomials.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_brentroot.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_complex.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_delayed_evaluation.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_eval.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_exotic_objects.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_format.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_gmp.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_hashmap_objects.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_hobby.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_immutable_vectors.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_kwargs.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_linear_systems.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_lists.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_machine.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_modules.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_polynomials.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_popcount.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_ps_number.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_rbmap.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_rnrs_conditions.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_rnrs_enums.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_scm_procedures.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_unicode.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_uniform_vectors.c
libsortsmill_core_guile_@GEV@_la_SOURCES += guile_vlists.c
libsortsmill_core_guile_@GEV@_la_SOURCES += scm_one_time_initialization.c
libsortsmill_core_guile_@GEV@_la_LDFLAGS = -export-dynamic -prefer-pic
libsortsmill_core_guile_@GEV@_la_LDFLAGS += $(VERSION_INFO)
libsortsmill_core_guile_@GEV@_la_LIBADD = \
	c-from-ats/libsortsmill-core-guile-@GEV@-ats.la \
	lib/libgnu.la

EXTRA_DIST += guile_gausselimsolve.c
EXTRA_DIST += guile_tridiagsolve.c
EXTRA_DIST += guile_cyclictridiagsolve.c
guile_linear_systems.$(OBJEXT): guile_gausselimsolve.c
guile_linear_systems.$(OBJEXT): guile_tridiagsolve.c
guile_linear_systems.$(OBJEXT): guile_cyclictridiagsolve.c

if with_python
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES =
nodist_libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES =
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyexc.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyfriendlier.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyinitialization.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyiter.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pymapping.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pymethod.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pymodule.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pynumber.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyobject.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyreflection.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyscm.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pysequence.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyset.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pytype.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyversion.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += guile_pyveryhighlevel.c
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += mpz_pylong.c mpz_pylong.h
libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += python_helpers.h
nodist_libsortsmill_core_guile_@GEV@_py@PYV@_la_SOURCES += miscellaneous_data.h
libsortsmill_core_guile_@GEV@_py@PYV@_la_CFLAGS = $(AM_CFLAGS) $(PYTHON_CFLAGS)
libsortsmill_core_guile_@GEV@_py@PYV@_la_LDFLAGS = $(PYTHON_LIBS)
libsortsmill_core_guile_@GEV@_py@PYV@_la_LDFLAGS += -export-dynamic -prefer-pic
libsortsmill_core_guile_@GEV@_py@PYV@_la_LIBADD =	\
	libsortsmill-core-guile-@GEV@.la \
	lib/libgnu.la
endif with_python

pkgconfigdir_default = $(libdir)/pkgconfig
nodist_pkgconfig_DATA = sortsmill-core-guile-@GEV@.pc
CLEANFILES += sortsmill-core-guile-@GEV@.pc
CLEANFILES += sortsmill-core-guile-@GEV@.pc.in
EXTRA_DIST += sortsmill-core-guile.pc.in.m4
if with_python
nodist_pkgconfig_DATA += sortsmill-core-guile-@GEV@-py@PYV@.pc
CLEANFILES += sortsmill-core-guile-@GEV@-py@PYV@.pc
CLEANFILES += sortsmill-core-guile-@GEV@-py@PYV@.pc.in
EXTRA_DIST += sortsmill-core-guile-py.pc.in.m4
endif with_python

sortsmill-core-guile-@GEV@.pc: sortsmill-core-guile-@GEV@.pc.in \
										GNUmakefile Makefile
	$(call v, GEN)( \
		set -e; \
		core_guile_name=`echo sortsmill-core-guile | $(SED) -e '$(transform)'`; \
		$(SED) -e "s&@core_guile_name@&$${core_guile_name}&g" < $(<) > $(@)-tmp; \
		mv $(@)-tmp $(@) \
	)

if with_python
sortsmill-core-guile-@GEV@-py@PYV@.pc: sortsmill-core-guile-@GEV@-py@PYV@.pc.in \
										GNUmakefile Makefile
	$(call v, GEN)( \
		set -e; \
		core_guile_name=`echo sortsmill-core-guile-py@PYV@ | $(SED) -e '$(transform)'`; \
		core_python_name=`echo sortsmill-core-python | $(SED) -e '$(transform)'`; \
		core_python_pyv_name=`echo sortsmill-core-python@PYV@ | $(SED) -e '$(transform)'`; \
		$(SED) \
			-e "s&@PY""V@&@PYV@&g" \
			-e "s&@PY""THON_CFLAGS@&@PYTHON_CFLAGS@&g" \
			-e "s&@PY""THON_LIBS@&@PYTHON_LIBS@&g" \
			-e "s&@core_guile_name@&$${core_guile_name}&g" \
			-e "s&@core_python_name@&$${core_python_name}&g" \
			-e "s&@core_python_pyv_name@&$${core_python_pyv_name}&g" \
			< $(<) > $(@)-tmp; \
		mv $(@)-tmp $(@) \
	)
endif with_python

DLOPENLIBS = $(MY_DLOPENLIBS) $(OTHER_DLOPENLIBS)
MY_DLOPENLIBS = sortsmill-core-guile-@GEV@
OTHER_DLOPENLIBS = \
	sortsmill-core \
	unicodenames guile-$(GUILE_EFFECTIVE_VERSION) \
	unistring \
	gmp
if enable_deprecated
OTHER_DLOPENLIBS += gettextpo
endif enable_deprecated
if with_python
PYTHON_DLOPENLIBS = $(PYTHON_MY_DLOPENLIBS) $(PYTHON_OTHER_DLOPENLIBS)
PYTHON_MY_DLOPENLIBS = sortsmill-core-guile-@GEV@-py@PYV@
PYTHON_OTHER_DLOPENLIBS = python${PYTHON_VERSION_AND_ABI}
endif with_python

expand_dlopenlibs = \
	$(addprefix -dlopen lib,$(addsuffix .la,$(1))) \
	$(addprefix -dlopen -l,$(2))

sortsmill_core_guile_SOURCES = sortsmill-core-guile.c
sortsmill_core_guile_LDFLAGS = \
	-export-dynamic \
	$(call expand_dlopenlibs, $(MY_DLOPENLIBS), $(OTHER_DLOPENLIBS))
sortsmill_core_guile_LDADD = \
	libsortsmill-core-guile-@GEV@.la \
	lib/libgnu.la

if with_python
sortsmill_core_guile_py@PYV@_SOURCES = sortsmill-core-guile.c
sortsmill_core_guile_py@PYV@_CFLAGS = $(AM_CFLAGS) $(PYTHON_CFLAGS)
sortsmill_core_guile_py@PYV@_LDFLAGS = \
	$(PYTHON_LIBS) \
	-export-dynamic \
	$(call expand_dlopenlibs, \
		$(MY_DLOPENLIBS) $(PYTHON_MY_DLOPENLIBS), \
		$(OTHER_DLOPENLIBS) $(PYTHON_OTHER_DLOPENLIBS))
sortsmill_core_guile_py@PYV@_LDADD = \
	libsortsmill-core-guile-@GEV@-py@PYV@.la \
	libsortsmill-core-guile-@GEV@.la \
	lib/libgnu.la
endif with_python

if HAVE_HELP2MAN

nodist_man_MANS += sortsmill-core-guile.1

%.1: %
	$(HELP2MAN) --no-info --output=$@ \
		--name='$(PACKAGE_NAME)' \
	 	--source='Sorts Mill' ./$<

endif HAVE_HELP2MAN

#-------------------------------------------------------------------------

libsortsmill_core_guile_@GEV@_la_SOURCES += ats_runtime_support.c
libsortsmill_core_guile_@GEV@_la_SOURCES += pats_ccomp_memalloc_user.h

C_FROM_ATSSOURCES =
BUILT_SOURCES += $(C_FROM_ATSSOURCES)

C_FROM_ATSSOURCES += $(srcdir)/c-from-ats/guile_sats.c
DIST_ATSSOURCES += inc/sortsmill/guile/core/SATS/guile.sats
DIST_ATSSOURCES += inc/sortsmill/guile/core/CATS/guile.cats

C_FROM_ATSSOURCES += $(srcdir)/c-from-ats/guile_hashmaps_sats.c
C_FROM_ATSSOURCES += $(srcdir)/c-from-ats/guile_hashmaps_dats.c
DIST_ATSSOURCES += inc/sortsmill/guile/core/SATS/guile_hashmaps.sats
DIST_ATSSOURCES += inc/sortsmill/guile/core/DATS/guile_hashmaps.dats
DIST_ATSSOURCES += inc/sortsmill/guile/core/CATS/guile_hashmaps.cats

#-------------------------------------------------------------------------
#
# The Python extension module, and also a Python interpreter that has
# the module built in.

if with_python

pyexec_LTLIBRARIES = sortsmill_core_guile.la
bin_PROGRAMS += sortsmill-core-python@PYV@
nodist_bin_SCRIPTS += sortsmill-core-python
MOSTLYCLEANFILES += sortsmill-core-python
EXTRA_DIST += sortsmill-core-python.m4

sortsmill_core_guile_la_SOURCES = python_module.c
sortsmill_core_guile_la_CFLAGS = $(AM_CFLAGS) $(PYTHON_CFLAGS)
sortsmill_core_guile_la_LDFLAGS = \
	-export-dynamic \
	-prefer-pic	\
	-shared \
	-module \
	-avoid-version \
	$(PYTHON_LIBS)
sortsmill_core_guile_la_LIBADD = \
	libsortsmill-core-guile-@GEV@-py@PYV@.la \
	libsortsmill-core-guile-@GEV@.la \
	lib/libgnu.la

sortsmill_core_python@PYV@_SOURCES = python_interpreter.c python_module.c
sortsmill_core_python@PYV@_CFLAGS = $(AM_CFLAGS) $(PYTHON_CFLAGS)
sortsmill_core_python@PYV@_LDFLAGS = \
	-export-dynamic \
	$(PYTHON_LIBS) \
	$(call expand_dlopenlibs, \
		$(MY_DLOPENLIBS) $(PYTHON_MY_DLOPENLIBS), \
		$(OTHER_DLOPENLIBS) $(PYTHON_OTHER_DLOPENLIBS))
sortsmill_core_python@PYV@_LDADD = \
	libsortsmill-core-guile-@GEV@-py@PYV@.la \
	libsortsmill-core-guile-@GEV@.la \
	lib/libgnu.la

endif with_python

#-------------------------------------------------------------------------
#
# Read APII files to generate Guile code.

# FIXME: Give sortsmill-apii-to-guile options (at least --help and
# --version) and a manual page.
nodist_bin_SCRIPTS += sortsmill-apii-to-guile
DISTCLEANFILES += sortsmill-apii-to-guile
DISTCLEANFILES += sortsmill-apii-to-guile.in
EXTRA_DIST += sortsmill-apii-to-guile.in.m4

sortsmill-apii-to-guile: sortsmill-apii-to-guile.in GNUmakefile Makefile
	$(call v, GEN)(set -e; \
		core_guile_name=`echo sortsmill-core-guile | $(SED) -e '$(transform)'`; \
		$(SED) \
			-e "s&@sortsmill-core-guile@&$${core_guile_name}&g" \
			< $(<) > $(@)-tmp; \
		chmod +x $(@)-tmp; \
		mv $(@)-tmp $(@))

#-------------------------------------------------------------------------
#
# Running the Type Inspector Generator, and running the type
# inspectors that it generates. The results are ‘API Instruction’
# (APII) files.

%_type_inspector.c: %.tig
	$(call v, TIG)$(SORTSMILL_TIG) --include-config --output=$(@)-tmp $(<) \
		&& mv $(@)-tmp $(@)

# <Python.h> is rebellious and needs to come before system header
# files.
python_type_inspector.c: python.tig
	$(call v, TIG)(set -e; \
		$(SORTSMILL_TIG) --include-config --output=$(@)-tmp $(<) && \
			( echo '#include <config.h>'; \
			  echo '#include <Python.h>'; \
			  cat $(@)-tmp              ) > $(@)-tmp2; \
		mv $(@)-tmp2 $(@); \
		rm -f $(@)-tmp)

%.types.apii: %_type_inspector
	$(call v, APII)./$(<) > $(@)-tmp && mv $(@)-tmp $(@)

EXTRA_DIST += python.tig
if with_python
nodist_apii_DATA += python.types.apii
noinst_PROGRAMS += python_type_inspector
python_type_inspector_SOURCES = python_type_inspector.c
python_type_inspector_CFLAGS = $(AM_CFLAGS) $(PYTHON_CFLAGS)
python_type_inspector_LDFLAGS = $(PYTHON_LIBS)
python_type_inspector_LDADD = lib/libgnu.la
python_type_inspector.o: $(filter %.h, $(CFGMK_FILES))
endif with_python

CLEANFILES += \
	python_type_inspector \
	python_type_inspector.c \
	python_type_inspector.o \
	python.types.apii

#-------------------------------------------------------------------------
#
# Automatic reëxportation of objects in a hierarchy of Guile modules.

# FIXME: Install the reëxporter under some ‘sortsmill-%’ name, having
# given it some more interesting user interface, and reüse it in Sorts
# Mill Tools, etc. Also, consider making it a standalone package.

generate_reexporter = \
	$(call v, REEXPORT)(set -e; \
		$(MKDIR_P) $(dir $(strip $(1))); \
		$(GUILE_INTERPRET) generate-reexporter \
			"(lambda (reexporter-name reexports imports) \
			   \`(library ,reexporter-name \
			       (export ,@reexports) \
			       (import ,@imports)))" \
			'($(subst /, ,$(1:guile/%.scm=%)))' \
			'\#t' '$(2:%="%")' > $(strip $(1))-tmp; \
		 mv $(strip $(1))-tmp $(strip $(1)))

reexporter_rule = \
	$(strip $(1)): $(2) generate-reexporter; \
		$$(call generate_reexporter,$(strip $(1)),$$(filter %.scm,$$(^)))

EXTRA_DIST += generate-reexporter.m4
DISTCLEANFILES += generate-reexporter

#-------------------------------------------------------------------------
#
# Automatic dependency tracking for Guile.
#
# If %.scm has changed, write a new %.Pgo file with the latest rule
# for the %.go target. The new %.Pgo file triggers a restart of GNU
# make, so the %.go file is compiled according to the updated rule.

#
# FIXME: Consider making sortsmill-guile-mkdep a standalone package.
#
# FIXME: Add a manpage for sortsmill-guile-mkdep when its --help and
# --version options have been written.
#
nodist_bin_SCRIPTS += sortsmill-guile-mkdep
EXTRA_DIST += sortsmill-guile-mkdep.m4
DISTCLEANFILES += sortsmill-guile-mkdep

$(DEPDIR)/%.Pgo: %.scm $(GUILESRCMODULES) sortsmill-guile-mkdep guile/sortsmill/core.scm
	@$(MKDIR_P) $(dir $@); \
	$(GUILE_INTERPRET) sortsmill-guile-mkdep \
		--vpath='$(VPATH)' \
		--prefix=guile \
		--build=$*.scm \
		$(GUILESRCMODULES) \
	| $(AWK) \
	  '{ print "\n"$$0" $$(CORE_GUILE_COMMAND)\n\t$$(call v, GUILEC)(set -e; $$(MKDIR_P) $$(@D); $$(CORE_GUILE_COMPILE) $$< -o $$@)\n"; \
	     split($$0, sides, /:/); \
	     n = split(sides[2], prereqs, / +/); \
	     for (i in prereqs) \
	       if (prereqs[i] !~ /^$$/) \
	         print prereqs[i]":\n" \
	   }' > $@-tmp \
	&& mv $@-tmp $@

PGOTRACKEDSRCS = $(filter-out $(DIRECT_GUILEOBJMODULES), $(GUILEOBJMODULES))
PGOFILES = $(PGOTRACKEDSRCS:%.go=$(DEPDIR)/%.Pgo)

$(PGOFILES): $(GUILESRCMODULES)
$(PGOTRACKEDSRCS:.scm=.go): $(PGOFILES)

INCLUDEMKFILES += $(PGOFILES)

#-------------------------------------------------------------------------

$(eval include $(srcdir)/guile-default-dirs.mk)
EXTRA_DIST += guile-default-dirs.mk

GUILESRCMODULES = $(DIST_GUILESRCMODULES) $(NODIST_GUILESRCMODULES)
DIST_GUILESRCMODULES =
NODIST_GUILESRCMODULES =
DIRECT_GUILESRCMODULES =
GUILEOBJMODULES = $(GUILESRCMODULES:.scm=.go)
DIRECT_GUILEOBJMODULES = $(DIRECT_GUILESRCMODULES:.scm=.go)

CLEANFILES += $(NODIST_GUILESRCMODULES)
MOSTLYCLEANFILES += $(GUILEOBJMODULES)

all-local: $(GUILEOBJMODULES)
$(GUILEOBJMODULES): sortsmill-core-guile

$(eval $(call prefixed-install, guile, guile, DATA))
install-data-local: install-prefixed_guile_guile_DATAZ
uninstall-local: uninstall-prefixed_guile_guile_DATAZ
prefixed_guile_dist_guile_DATAZ = $(DIST_GUILESRCMODULES:guile/%=%)
prefixed_guile_nodist_guile_DATAZ = $(NODIST_GUILESRCMODULES:guile/%=%)

# Installed Guile bitcode modules need to be timestamped later than
# their equivalent source modules, so we install the bitcodes with
# install-data-hook instead of install-data-local.
$(eval $(call prefixed-install, guile, guileobj, DATA))
install-data-hook: install-prefixed_guile_guileobj_DATAZ
uninstall-local: uninstall-prefixed_guile_guileobj_DATAZ
prefixed_guile_nodist_guileobj_DATAZ = $(GUILEOBJMODULES:guile/%=%)

# Do not re-export any (sortsmill core python<WHATEVER>) or ‘info’
# modules other than (sortsmill *-pkginfo), so we can more easily
# support multiple Python versions and multilib.
NODIST_GUILESRCMODULES += guile/sortsmill/core.scm
$(eval $(call reexporter_rule, guile/sortsmill/core.scm, \
	$(filter-out guile/sortsmill/core/python%.scm, \
		$(call filter-one-dir, guile/sortsmill/core, $(GUILESRCMODULES))) \
	$(filter guile/sortsmill/smcore-pkginfo.scm \
	         guile/sortsmill/smcoreguile-pkginfo.scm, \
		$(call filter-one-dir, guile/sortsmill, $(GUILESRCMODULES)))))

DIST_GUILESRCMODULES += guile/sortsmill/core/api-syntax.scm
DIRECT_GUILESRCMODULES += guile/sortsmill/core/api-syntax.scm
guile/sortsmill/core/api-syntax.go: guile/sortsmill/core/api-syntax.scm $(CORE_GUILE_COMMAND)
	$(call v, GUILEC)$(CORE_GUILE_COMPILE) $(<) -o $(@)

$(eval $(call reexporter_rule, guile/sortsmill/core/rbmap.scm, \
        $(call filter-one-dir, guile/sortsmill/core/rbmap, $(GUILESRCMODULES))))
EXTRA_DIST += guile/sortsmill/core/rbmap/generic.scm.m4
NODIST_GUILESRCMODULES += guile/sortsmill/core/rbmap/generic.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/rbmap/specialized.scm

NODIST_GUILESRCMODULES += guile/sortsmill/core/rbmap.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/alists-of-groups.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/atomic-ops.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/bivariate-polynomials.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/brent-root-finder.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/delayed-evaluation.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/exotic-objects.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/hashmaps.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/helpers.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/higher-order-procedures.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/hobby.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/i18n.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/immutable-vectors.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/kwargs.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/linear-systems.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/lists.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/libunicodenames.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/machine.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/misc-math-functions.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/miscellaneous-macros.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/polynomials.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/postscript.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/procs-with-setters.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/psmat.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/scm-procedures.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/srfi-42.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/plugins.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/unicode.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/uniform-vectors.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/vlists.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/x-alloc.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/xgc.scm

if enable_deprecated

# Cannot be compiled by Guile 2.2.2.
DIST_GUILESRCMODULES += guile/sortsmill/core/extensible-procedures.scm

# The libgettextpo library is not available in 32-bit form on AMD64
# Gentoo. (There is no good reason for this, and it could be remedied
# easily, but let us deprecate po-files, anyway. It can be included in
# one’s own code or made into a separate package.)
DIST_GUILESRCMODULES += guile/sortsmill/core/po-files.scm

endif enable_deprecated

if with_python

NODIST_GUILESRCMODULES += guile/sortsmill/core/python.scm
$(eval $(call reexporter_rule, guile/sortsmill/core/python.scm, \
        $(call filter-one-dir, guile/sortsmill/core/python, $(GUILESRCMODULES))))
DIST_GUILESRCMODULES += guile/sortsmill/core/python/friendlier.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/initialization.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/py-psmat.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pyiters.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pyexcs.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pymappings.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pymethods.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pymodules.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pynumbers.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pyobjects.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pyscms.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pysequences.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pysets.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pytype-maker.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/pytypes.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/reflection.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/shorthand.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/smcoreguile-pymodule.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/version.scm
DIST_GUILESRCMODULES += guile/sortsmill/core/python/very-high-level.scm
#NODIST_GUILESRCMODULES += guile/sortsmill/core/python/python-api.scm
#guile/sortsmill/core/python/python-api.scm: python.types.apii \
#			sortsmill-apii-to-guile sortsmill-core-guile \
#			guile/sortsmill/core/api-syntax.go
#	$(call v, GUILEAPI) \
#		$(CORE_GUILE_INTERPRET) sortsmill-apii-to-guile '(sortsmill core python python-api)' $(<) > $(@)-tmp \
#		&& mv $(@)-tmp $(@)

DIST_GUILESRCMODULES += guile/sortsmill_core_guile/psMat.scm

endif with_python

#--------------------------------------------------------------------------
# Automatic ChangeLog generation at `make dist', if one has
# sortsmill-changelogger.

dist-hook: dist-changelog
@dist_changelog_rules@

#--------------------------------------------------------------------------
# Localization.

$(eval $(call prefixed-install, locale, locale, DATA))
all-local: $(MOFILES)
install-data-local: install-prefixed_locale_locale_DATAZ
uninstall-local: uninstall-prefixed_locale_locale_DATAZ
prefixed_locale_nodist_locale_DATAZ = $(MOFILES:locale/%=%)

EXTRA_DIST += $(POTFILE) $(POFILES)
POTFILE = $(srcdir)/sortsmill-core-guile.pot

POFILES =
POFILES += $(srcdir)/en_US.po
POFILES += $(srcdir)/eo.po

MOFILES = $(POFILES:$(srcdir)/%.po=locale/%/LC_MESSAGES/sortsmill-core-guile.mo)
CLEANFILES += $(MOFILES)

STRINGFILES = \
	$(sortsmill_core_guile_SOURCES) \
	$(libsortsmill_core_guile_@GEV@_la_SOURCES) \
	$(GUILESRCMODULES)

XGETTEXT_FLAGS = \
	--add-location --from-code=UTF-8 \
	--package-name='$(PACKAGE)' \
	--package-version='$(PACKAGE_VERSION)' \
	--msgid-bugs-address='$(PACKAGE_BUGREPORT)' \
	--copyright-holder='Khaled Hosny and Barry Schwartz' \
	--flag=error:3:c-format --flag=error_at_line:5:c-format \
	--keyword=_

if enable_nls
$(eval $(call msgfmt-rule, \
	locale/%/LC_MESSAGES/sortsmill-core-guile.mo: $(srcdir)/%.po, --check))
endif enable_nls

if enable_po_editing

if HAVE_MSGMERGE
$(eval $(call msgmerge-rule, $(POFILES): $(POTFILE), --backup=existing))
endif HAVE_MSGMERGE

if HAVE_XGETTEXT
$(eval $(call xgettext-rule, $(POTFILE): $(STRINGFILES), $(XGETTEXT_FLAGS)))
endif HAVE_XGETTEXT

endif enable_po_editing

#--------------------------------------------------------------------------

CFGMK_FILES =

smcoreguile_configmake_files = \
	$(1:%=$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR)/sortsmill/smcoreguile-%.h) \
	$(1:%=$(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR)/sortsmill/smcoreguile-%.m4)

smcoreguile_configmake_stuff = \
	$(eval $(call smcoreguile_configmake_files,$(1)): Makefile.am) \
	$(eval $(call $(1:%=%-h-rule), $(filter %.h, $(call smcoreguile_configmake_files,$(1))), $(2), $(3), $(4))) \
	$(eval $(call $(1:%=%-m4-rule), $(filter %.m4, $(call smcoreguile_configmake_files,$(1))), $(2), $(3), $(4)))

CFGMK_FILES += $(foreach x, pkginfo dirlayout buildinfo, $(call smcoreguile_configmake_files,$(x)))
#CFGMK_FILES += $(foreach x, dirlayout buildinfo, $(call smcoreguile_configmake_files_py,$(x)))

BUILT_SOURCES += $(CFGMK_FILES)
M4HEADERS += $(filter %.m4, $(CFGMK_FILES))
NODIST_SORTSMILLHEADERS += $(filter %.h %.m4, $(CFGMK_FILES))
DISTCLEANFILES += $(CFGMK_FILES)

$(eval $(call smcoreguile_configmake_stuff,pkginfo, SMCOREGUILE_, \
	$(standard-pkginfo-variables)))

# Include only what is needed. BE SURE TO BUILD ALL PYTHON
# IMPLEMENTATIONS WITH THE SAME SETTING OF THESE VARIABLES.
NEEDED_BUILDINFO_OPT_VARS = enable_threads
NEEDED_BUILDINFO_VARS = $(NEEDED_BUILDINFO_OPT_VARS)
$(eval $(call smcoreguile_configmake_stuff,buildinfo, SMCOREGUILE_, \
	$(NEEDED_BUILDINFO_VARS), $(NEEDED_BUILDINFO_OPT_VARS:%=n_%)))

$(eval $(call smcoreguile_configmake_stuff,dirlayout, SMCOREGUILE_, \
	$(standard-configmake-variables) guiledir guileobjdir hostincludedir))

BUILT_SOURCES += miscellaneous_data.h
MOSTLYCLEANFILES += miscellaneous_data.h
$(eval $(call configmake-h-rule,miscellaneous_data.h,Miscellaneous data.,,PI_DIGITS))
miscellaneous_data.h: Makefile.am

SHELL_COMMAND = $(SHELL)
GUILE_COMMAND = $(GUILE)
DISTCLEANFILES += vars.m4
M4HEADERS += vars.m4
$(eval $(call configmake-m4-rule,vars.m4,A generated file not for installation.,, \
	SHELL_COMMAND GUILE_COMMAND GUILE_EFFECTIVE_VERSION PYTHON_VERSION_AND_ABI))

#--------------------------------------------------------------------------
#
# Scheme versions of Sorts Mill Core Library and Sorts Mill Core Guile
# package info headers.
#
# Nothing fancy or foolproof: just text filtering of the C header
# files without actually parsing them as C. One point worth noting is
# that the `_IN_UTF8' macros, rather than the C string literals, are
# used to derive strings, although the string literals are retained as
# comments.
#

SMCORE_INFO_GUILESOURCES = guile/sortsmill/smcore-pkginfo.scm
SMCOREGUILE_INFO_GUILESOURCES = guile/sortsmill/smcoreguile-pkginfo.scm

NODIST_GUILESRCMODULES += $(SMCORE_INFO_GUILESOURCES)
NODIST_GUILESRCMODULES += $(SMCOREGUILE_INFO_GUILESOURCES)
BUILT_SOURCES += $(SMCORE_INFO_GUILESOURCES)
BUILT_SOURCES += $(SMCOREGUILE_INFO_GUILESOURCES)

EXTRA_DIST += configmake_h_to_scm

guile/sortsmill/smcore-%.scm: $(SORTSMILL_CORE_HOSTINCLUDEDIR)/sortsmill/smcore-%.h \
		Makefile GNUmakefile $(srcdir)/configmake_h_to_scm
	$(call v, CONFMK) \
	( set -e; \
	  $(MKDIR_P) $(@D); \
	  $(AWK) -f $(srcdir)/configmake_h_to_scm 'sortsmill smcore-$(*)' $(<) > $(@)-tmp; \
	  mv $(@)-tmp $(@) )

guile/sortsmill/smcoreguile-%.scm: $(SORTSMILL_CORE_GUILE_HOSTINCLUDEDIR)/sortsmill/smcoreguile-%.h \
		Makefile GNUmakefile $(srcdir)/configmake_h_to_scm
	$(call v, CONFMK) \
	( set -e; \
	  $(MKDIR_P) $(@D); \
	  $(AWK) -f $(srcdir)/configmake_h_to_scm 'sortsmill smcoreguile-$(*)' $(<) > $(@)-tmp; \
	  mv $(@)-tmp $(@) )

#--------------------------------------------------------------------------
#
# $(INCLUDEMKFILES) entries get included here, unless the command line
# asks for one of the ‘QUICKGOALS’. This is where, generally speaking,
# code for compiling Guile modules gets included.

# Command-line goals that do not need dependency tracking or similar
# information and which we would like to be faster.
QUICKGOALS = mostlyclean clean distclean maintainer-clean dist dist-%	\
             distcheck uninstall

$(if $(strip $(filter $(QUICKGOALS),$(MAKECMDGOALS))),,$(eval -include $(INCLUDEMKFILES)))

$(INCLUDEMKFILES): Makefile GNUmakefile

#--------------------------------------------------------------------------

MAINTAINERCLEANFILES += \
  $(GITIGNORE_MAINTAINERCLEANFILES_TOPLEVEL) \
  $(GITIGNORE_MAINTAINERCLEANFILES_M4_LIBTOOL) \
  $(GITIGNORE_MAINTAINERCLEANFILES_MAKEFILE_IN)

-include $(top_srcdir)/git.mk
