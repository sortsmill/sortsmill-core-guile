#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014, 2017 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <frameobject.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>

#include <c-xvasprintf.h>
#include "python_helpers.h"
#include "mpz_pylong.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define _PYOBJECTS_MODULE "sortsmill core python pyobjects"

#ifdef WORDS_BIGENDIAN
#define _UTF32_NATIVE "UTF-32BE"
#else
#define _UTF32_NATIVE "UTF-32LE"
#endif

//-------------------------------------------------------------------------

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

static void
scm_dynwind_decref_handler (void *p)
{
  Py_XDECREF ((PyObject *) p);
}

VISIBLE void
scm_dynwind_decref (PyObject *p)
{
  scm_dynwind_unwind_handler (scm_dynwind_decref_handler, p,
                              SCM_F_WIND_EXPLICITLY);
}

static void
scm_dynwind_decref_clear_handler (void *p)
{
  Py_CLEAR (*(PyObject **) p);
}

VISIBLE void
scm_dynwind_decref_clear (PyObject **p)
{
  scm_dynwind_unwind_handler (scm_dynwind_decref_clear_handler, p,
                              SCM_F_WIND_EXPLICITLY);
}

static void
refcount_nonpositive_assertion_violation (PyObject *obj)
{
  rnrs_raise_condition
    (scm_list_3
     (rnrs_make_assertion_violation (),
      rnrs_c_make_message_condition
      (_("argument has reference count of zero or less")),
      rnrs_make_irritants_condition
      (scm_list_2 (scm_from_pointer (obj, NULL),
                   scm_from_ssize_t (Py_REFCNT (obj))))));
}

static inline void
assert_refcount_positive (PyObject *obj)
{
  if (Py_REFCNT (obj) <= 0)
    refcount_nonpositive_assertion_violation (obj);
}

static inline SCM
scm_make_pointer_without_finalizer (SCM address)
{
  return
    scm_call_1 (scm_c_public_ref ("system foreign", "make-pointer"), address);
}

static inline bool
scm_is_pointer (SCM obj)
{
  return
    scm_is_true (scm_call_1
                 (scm_c_public_ref ("system foreign", "pointer?"), obj));
}

//-------------------------------------------------------------------------

VISIBLE void
py_exc_check_violation (const char *name, SCM irritants)
{
  scm_c_py_exc_rnrs_raise_condition (name, _("Python exception"), irritants);
}

SCM
_scm_pyobject_unary_op (SCM obj, const char *name,
                        PyObject *(*api_func) (PyObject *))
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  PyObject *_result = api_func (_obj);
  py_exc_check ((_result != NULL), name, scm_list_1 (obj));
  return scm_c_make_pyobject (_result);
}

SCM
_scm_pyobject_binary_op (SCM obj1, SCM obj2, const char *name,
                         PyObject *(*api_func) (PyObject *, PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  PyObject *_result = api_func (_obj1, _obj2);
  py_exc_check ((_result != NULL), name, scm_list_2 (obj1, obj2));
  return scm_c_make_pyobject (_result);
}

SCM
_scm_pyobject_ternary_op (SCM obj1, SCM obj2, SCM obj3, const char *name,
                          PyObject *(*api_func) (PyObject *, PyObject *,
                                                 PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  scm_assert_pyobject (obj3);
  PyObject *_obj3 = scm_to_PyObject_ptr (obj3);
  PyObject *_result = api_func (_obj1, _obj2, _obj3);
  py_exc_check ((_result != NULL), name, scm_list_3 (obj1, obj2, obj3));
  return scm_c_make_pyobject (_result);
}

#define _SCM_PYOBJECT_INTTYPE(INTTYPE)                                  \
  INTTYPE                                                               \
  _scm_pyobject_unary_##INTTYPE (SCM obj, const char *name,             \
                                 INTTYPE (*api_func) (PyObject *))      \
  {                                                                     \
    scm_assert_pyobject (obj);                                          \
    PyObject *_obj = scm_to_PyObject_ptr (obj);                         \
    const INTTYPE result = api_func (_obj);                             \
    py_exc_check ((result != -1 || !PyErr_Occurred ()),                 \
                  name, scm_list_1 (obj));                              \
    return result;                                                      \
  }                                                                     \
                                                                        \
  INTTYPE                                                               \
  _scm_pyobject_binary_##INTTYPE (SCM obj1, SCM obj2,                   \
                                  const char *name,                     \
                                  INTTYPE (*api_func) (PyObject *,      \
                                                       PyObject *))     \
  {                                                                     \
    scm_assert_pyobject (obj1);                                         \
    PyObject *_obj1 = scm_to_PyObject_ptr (obj1);                       \
    scm_assert_pyobject (obj2);                                         \
    PyObject *_obj2 = scm_to_PyObject_ptr (obj2);                       \
    const INTTYPE result = api_func (_obj1, _obj2);                     \
    py_exc_check ((result != -1 || !PyErr_Occurred ()),                 \
                  name, scm_list_2 (obj1, obj2));                       \
    return result;                                                      \
  }                                                                     \
                                                                        \
  INTTYPE                                                               \
  _scm_pyobject_ternary_##INTTYPE (SCM obj1, SCM obj2, SCM obj3,        \
                                   const char *name,                    \
                                   INTTYPE (*api_func) (PyObject *,     \
                                                        PyObject *,     \
                                                        PyObject *))    \
  {                                                                     \
    scm_assert_pyobject (obj1);                                         \
    PyObject *_obj1 = scm_to_PyObject_ptr (obj1);                       \
    scm_assert_pyobject (obj2);                                         \
    PyObject *_obj2 = scm_to_PyObject_ptr (obj2);                       \
    scm_assert_pyobject (obj3);                                         \
    PyObject *_obj3 = scm_to_PyObject_ptr (obj3);                       \
    const INTTYPE result = api_func (_obj1, _obj2, _obj3);              \
    py_exc_check ((result != -1 || !PyErr_Occurred ()),                 \
                  name, scm_list_3 (obj1, obj2, obj3));                 \
    return result;                                                      \
  }

_SCM_PYOBJECT_INTTYPE (int);
_SCM_PYOBJECT_INTTYPE (long);
_SCM_PYOBJECT_INTTYPE (ssize_t);

bool
_scm_pyobject_unary_bool (SCM obj, const char *name,
                          int (*api_func) (PyObject *))
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  const int result = api_func (_obj);
  py_exc_check ((result != -1), name, scm_list_1 (obj));
  return result;
}

bool
_scm_pyobject_binary_bool (SCM obj1, SCM obj2, const char *name,
                           int (*api_func) (PyObject *, PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  const int result = api_func (_obj1, _obj2);
  py_exc_check ((result != -1), name, scm_list_2 (obj1, obj2));
  return result;
}

bool
_scm_pyobject_ternary_bool (SCM obj1, SCM obj2, SCM obj3,
                            const char *name,
                            int (*api_func) (PyObject *, PyObject *,
                                             PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  scm_assert_pyobject (obj3);
  PyObject *_obj3 = scm_to_PyObject_ptr (obj3);
  const int result = api_func (_obj1, _obj2, _obj3);
  py_exc_check ((result != -1), name, scm_list_3 (obj1, obj2, obj3));
  return result;
}

SCM
_scm_pyobject_unary_unspec (SCM obj, const char *name,
                            int (*api_func) (PyObject *))
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  const int errval = api_func (_obj);
  py_exc_check ((errval != -1), name, scm_list_1 (obj));
  return SCM_UNSPECIFIED;
}

SCM
_scm_pyobject_binary_unspec (SCM obj1, SCM obj2, const char *name,
                             int (*api_func) (PyObject *, PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  const int errval = api_func (_obj1, _obj2);
  py_exc_check ((errval != -1), name, scm_list_2 (obj1, obj2));
  return SCM_UNSPECIFIED;
}

SCM
_scm_pyobject_ternary_unspec (SCM obj1, SCM obj2, SCM obj3,
                              const char *name,
                              int (*api_func) (PyObject *, PyObject *,
                                               PyObject *))
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);
  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);
  scm_assert_pyobject (obj3);
  PyObject *_obj3 = scm_to_PyObject_ptr (obj3);
  const int errval = api_func (_obj1, _obj2, _obj3);
  py_exc_check ((errval != -1), name, scm_list_3 (obj1, obj2, obj3));
  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------

static inline SCM
scm_from_PyTypeObject_pointer (PyTypeObject *t)
{
  assert (t != NULL);
  return scm_c_incref_make_pyobject ((PyObject *) t);
}

#define _PYTYPE_PROC(L, U)                                      \
  SCM                                                           \
  scm_py##L##_type (void)                                       \
  {                                                             \
    return scm_from_PyTypeObject_pointer (&Py##U##_Type);       \
  }

VISIBLE _PYTYPE_PROC (scm, SCM);
VISIBLE _PYTYPE_PROC (baseobject, BaseObject);
VISIBLE _PYTYPE_PROC (super, Super);
VISIBLE _PYTYPE_PROC (type, Type);
VISIBLE _PYTYPE_PROC (bytes, Bytes);
VISIBLE _PYTYPE_PROC (unicode, Unicode);
VISIBLE _PYTYPE_PROC (bool, Bool);
VISIBLE _PYTYPE_PROC (long, Long);
VISIBLE _PYTYPE_PROC (float, Float);
VISIBLE _PYTYPE_PROC (complex, Complex);
VISIBLE _PYTYPE_PROC (tuple, Tuple);
VISIBLE _PYTYPE_PROC (list, List);
VISIBLE _PYTYPE_PROC (dict, Dict);
VISIBLE _PYTYPE_PROC (dictproxy, DictProxy);
VISIBLE _PYTYPE_PROC (set, Set);
VISIBLE _PYTYPE_PROC (frozenset, FrozenSet);
VISIBLE _PYTYPE_PROC (range, Range);
VISIBLE _PYTYPE_PROC (slice, Slice);
VISIBLE _PYTYPE_PROC (ellipsis, Ellipsis);
VISIBLE _PYTYPE_PROC (code, Code);
VISIBLE _PYTYPE_PROC (frame, Frame);
VISIBLE _PYTYPE_PROC (traceback, TraceBack);
VISIBLE _PYTYPE_PROC (method, Method);
VISIBLE _PYTYPE_PROC (module, Module);
VISIBLE _PYTYPE_PROC (property, Property);
VISIBLE _PYTYPE_PROC (bytearray, ByteArray);
VISIBLE _PYTYPE_PROC (cell, Cell);

#if PY_MAJOR_VERSION <= 2

VISIBLE _PYTYPE_PROC (int, Int);

VISIBLE SCM
scm_pyinstancemethod_type (void)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition ("pyinstancemethod-type"),
      rnrs_c_make_message_condition (_("not supported in Python 2")),
      rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

#else // !(PY_MAJOR_VERSION <= 2)

VISIBLE SCM
scm_pyint_type (void)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition ("pyint-type"),
      rnrs_c_make_message_condition (_("not supported in Python 3 or greater")),
      rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

VISIBLE _PYTYPE_PROC (instancemethod, InstanceMethod);

#endif // !(PY_MAJOR_VERSION <= 2)


#if PY_VERSION_HEX < 0x02070000

VISIBLE SCM
scm_pycapsule_type (void)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition ("pycapsule-type"),
      rnrs_c_make_message_condition (_("not supported in Python 2.6")),
      rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pymemoryview_type (void)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition ("pymemoryview-type"),
      rnrs_c_make_message_condition (_("not supported in Python 2.6")),
      rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

#else // !(PY_VERSION_HEX < 0x02070000)

VISIBLE _PYTYPE_PROC (capsule, Capsule);
VISIBLE _PYTYPE_PROC (memoryview, MemoryView);

#endif // !(PY_VERSION_HEX < 0x02070000)


//-------------------------------------------------------------------------

VISIBLE SCM
scm_py_incref_x (SCM obj)
{
  scm_assert_pyobject (obj);
  Py_INCREF (scm_to_PyObject_ptr (obj));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_py_decref_x (SCM obj)
{
  scm_assert_pyobject (obj);
  Py_DECREF (scm_to_PyObject_ptr (obj));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_py_refcount (SCM obj)
{
  scm_assert_pyobject (obj);
  return scm_from_ssize_t ((scm_to_PyObject_ptr (obj))->ob_refcnt);
}

//-------------------------------------------------------------------------

static void
free_pyobject (void *p)
{
  // Give up our reference to the PyObject, unless the reference count
  // already is zero (in which case we should be in the process of
  // deallocating). Also watch out for NULL objects. (FIXME: Perhaps
  // such NULL objects can be created by Python’s garbage collector?
  // (See Python module ‘gc’.)  Try to understand such matters
  // better.)

  PyObject *obj = (PyObject *) p;
  if (obj != NULL && 0 < Py_REFCNT (obj))
    Py_CLEAR (obj);
}

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _null_string,
                             (_null_string__Value =
                              scm_from_latin1_string ("")));

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _format_string,
                             (_format_string__Value =
                              scm_from_latin1_string ("#<pyobject ~a 0x~x>")));

static void
print_pyobject (SCM pyobj, SCM port)
{
  SCM s = _null_string ();
  PyObject *obj = scm_to_PyObject_ptr (pyobj);
  if (obj != NULL)
    {
      PyObject *repr = PyObject_Repr (obj);
      if (repr == NULL)
        PyErr_Clear ();
      else
        s = scm_pyunicode_to_string (scm_c_make_pyobject (repr),
                                     scm_from_latin1_string ("replace"));
    }
  scm_format (((SCM_UNBNDP (port)) ? (scm_current_output_port ()) : port),
              _format_string (),
              scm_list_2 (s, scm_from_uintmax ((uintptr_t) obj)));
}

static volatile stm_dcl_indicator_t pyobjects_are_initialized = false;
pthread_mutex_t pyobject_initialization_mutex = PTHREAD_MUTEX_INITIALIZER;

STM_SCM_ONE_TIME_INITIALIZE (STM_ATTRIBUTE_PURE static, SCM,
                             scm_pyobject_type__,
                             scm_pyobject_type____Value =
                             scm_c_utf8_make_exotic_object_type ("<pyobject>"));

VISIBLE PyObject *
scm_to_PyObject_ptr (SCM obj)
{
  return (PyObject *)
    scm_to_pointer (scm_exotic_object_ref (scm_pyobject_type__ (), obj));
}

VISIBLE bool
scm_is_pyobject (SCM obj)
{
  return scm_is_exotic_object (obj, scm_pyobject_type__ ());
}

VISIBLE SCM
scm_pyobject_p (SCM obj)
{
  return scm_exotic_object_p (obj, scm_pyobject_type__ ());
}

VISIBLE void
scm_assert_pyobject (SCM pyobj)
{
  scm_assert_exotic_object_type (scm_pyobject_type__ (), pyobj);
  if (scm_to_PyObject_ptr (pyobj) == NULL)
    // FIXME: Probably this never, ever _should_ happen in good
    // code. Thus one might want to put an NDEBUG wrapper around the
    // test. For now, we have not done that.
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("scm_assert_pyobject"),
        rnrs_c_make_message_condition (_("pyobject data is NULL")),
        rnrs_make_irritants_condition (scm_list_1 (pyobj))));
}

static SCM
scm_write_pyobject (SCM pyobj, SCM port)
{
  scm_assert_pyobject (pyobj);
  print_pyobject (pyobj, port);
  return SCM_UNSPECIFIED;
}

void guile_init_pyobjects (void);
//
VISIBLE void
guile_init_pyobjects (void)
{
  scm_c_define ("<pyobject>", scm_pyobject_type__ ());
  scm_c_define_gsubr ("write-pyobject", 1, 1, 0, scm_write_pyobject);
}

static inline SCM
scm_c_make_pyobject__base (PyObject *p)
{
  // Steal a reference to a Python object, turning the reference into
  // an exotic object.
  //
  // As a special case (because it has been found useful), if the
  // argument is @code{NULL} then the return value is @{#f}.

  return ((p == NULL) ?
          SCM_BOOL_F :
          (scm_make_exotic_object (scm_pyobject_type__ (),
                                   scm_from_pointer (p, free_pyobject))));
}

static volatile SCM bitbucket;

static void
initialize_pyobjects (void)
{
  // Load the (sortsmill core python pyobjects) module.
  bitbucket = scm_c_public_ref (_PYOBJECTS_MODULE, "pyobject-getiter");
}

VISIBLE SCM
scm_c_make_pyobject (PyObject *p)
{
  // This assertion has caught programming errors before. I recommend
  // against removing it, should one feel tempted. */
  scm_assert_py_initialized ();

  // Initialize the pyobject smob once, in a thread-safe way (assuming
  // the thread locking is not the fake stuff from Gnulib).
  if (!stm_dcl_load_indicator (&pyobjects_are_initialized))
    {
      scm_dynwind_begin (0);
      pthread_mutex_lock (&pyobject_initialization_mutex);
      scm_dynwind_pthread_mutex_unlock (&pyobject_initialization_mutex);
      if (!pyobjects_are_initialized)
        {
          initialize_pyobjects ();
          stm_dcl_store_indicator (&pyobjects_are_initialized, true);
        }
      scm_dynwind_end ();
    }

  // Steal a reference to a Python object, turning the reference into
  // a smob. The smob data is the address of the PyObject. To use the
  // data, cast it to (PyObject*).
  //
  // As a special case (because it has been found useful), if the
  // argument is @code{NULL} then the return value is @{#f}.
  return (p == NULL) ? SCM_BOOL_F : scm_c_make_pyobject__base (p);
}

VISIBLE inline SCM
scm_c_incref_make_pyobject (PyObject *p)
{
  assert (p != NULL);
  assert_refcount_positive (p);
  Py_XINCREF (p);
  return scm_c_make_pyobject (p);
}

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM,
                             scm_parent_property,
                             (scm_parent_property__Value =
                              scm_call_0
                              (scm_c_public_ref
                               ("guile", "make-object-property"))));

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM,
                             scm_parent_property_setter,
                             (scm_parent_property_setter__Value =
                              scm_setter (scm_parent_property ())));

// Make a pyobject that includes a weak reference to a second
// pyobject. This is meant to be done when making a pyobject from a
// borrowed reference, to make sure the ‘parent’ object does not
// disappear on us.
VISIBLE SCM
scm_c_make_borrowed_pyobject (PyObject *child, SCM parent)
{
  SCM obj = scm_c_incref_make_pyobject (child);
  scm_call_2 (scm_parent_property_setter (), obj, parent);
  return obj;
}

VISIBLE SCM
scm_make_pyobject (SCM address)
{
  // Steal a reference to a Python object, turning the reference into
  // a smob. The smob data is the address of the PyObject. To use the
  // data, cast it to (PyObject*).

  if (scm_is_integer (address))
    address = scm_make_pointer_without_finalizer (address);
  if (!scm_is_pointer (address))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("make-pyobject"),
        rnrs_c_make_message_condition
        (_("expected a pointer or an integer address")),
        rnrs_make_irritants_condition (scm_list_1 (address))));
  return scm_c_make_pyobject (scm_to_pointer (address));
}

VISIBLE SCM
scm_incref_make_pyobject (SCM address)
{
  if (scm_is_integer (address))
    address = scm_make_pointer_without_finalizer (address);
  if (!scm_is_pointer (address))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("make-pyobject"),
        rnrs_c_make_message_condition
        (_("expected a pointer or an integer address")),
        rnrs_make_irritants_condition (scm_list_1 (address))));
  return scm_c_incref_make_pyobject (scm_to_pointer (address));
}

// Make a pyobject that includes a weak reference to a second
// pyobject. This is meant to be done when making a pyobject from a
// borrowed reference, to make sure the ‘parent’ object does not
// disappear on us.
VISIBLE SCM
scm_make_borrowed_pyobject (SCM child, SCM parent)
{
  if (scm_is_integer (child))
    child = scm_make_pointer_without_finalizer (child);
  if (!scm_is_pointer (child))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("make-borrowed-pyobject"),
        rnrs_c_make_message_condition
        (_("expected a pointer or an integer address")),
        rnrs_make_irritants_condition (scm_list_1 (child))));
  return scm_c_make_borrowed_pyobject (scm_to_pointer (child), parent);
}

VISIBLE SCM
scm_pyobject_to_pointer (SCM obj)
{
  // This procedure borrows the reference.
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_from_pointer (_obj, NULL);
}

VISIBLE SCM
scm_incref_pyobject_to_pointer (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  assert_refcount_positive (_obj);
  Py_INCREF (_obj);
  return scm_from_pointer (_obj, NULL);
}

VISIBLE SCM
scm_py_notimplemented (void)
{
  return scm_c_incref_make_pyobject (Py_NotImplemented);
}

VISIBLE bool
scm_is_py_notimplemented (SCM obj)
{
  return (scm_is_pyobject (obj) &&
          scm_to_PyObject_ptr (obj) == Py_NotImplemented);
}

VISIBLE SCM
scm_py_notimplemented_p (SCM obj)
{
  return scm_from_bool (scm_is_py_notimplemented (obj));
}

#define _MY_PYNONE_CHECK(x) ((x) == Py_None)

VISIBLE SCM_PYOBJECT_UNARY_BOOLCHECK (scm_is_pynone, _MY_PYNONE_CHECK);

VISIBLE SCM
scm_pynone_p (SCM obj)
{
  return (scm_is_pynone (obj)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_py_none (void)
{
  return scm_c_incref_make_pyobject (Py_None);
}

SCM_PYOBJECT_UNARY_WRAPCHECKS (pybytes, PyBytes);

VISIBLE SCM
scm_bytevector_to_pybytes (SCM bv)
{
  const char *who = "bytevector->pybytes";
  SCM_ASSERT_TYPE (scm_is_bytevector (bv), bv, SCM_ARG1, who, "bytevector");
  const size_t n = SCM_BYTEVECTOR_LENGTH (bv);
  const char *data = (char *) SCM_BYTEVECTOR_CONTENTS (bv);
  PyObject *obj = PyBytes_FromStringAndSize (data, n);
  if (obj == NULL)
    scm_c_py_exc_check_violation
      (who, _("failed to convert bytevector to pybytes"), scm_list_1 (bv));
  return scm_c_make_pyobject (obj);
}

VISIBLE SCM
scm_pybytes_to_bytevector (SCM pybytes)
{
  const char *who = "pybytes->bytevector";
  scm_assert_pyobject (pybytes);
  PyObject *obj = scm_to_PyObject_ptr (pybytes);
  char *data;
  Py_ssize_t n;
  const int retval = PyBytes_AsStringAndSize (obj, &data, &n);
  if (retval == -1)
    scm_c_py_exc_check_violation
      (who, _("failed to convert pybytes to bytevector"), scm_list_1 (pybytes));
  SCM bv = scm_c_make_bytevector (n);
  char *bvdata = (char *) SCM_BYTEVECTOR_CONTENTS (bv);
  memcpy (bvdata, data, n);
  return bv;
}

SCM_PYOBJECT_UNARY_WRAPCHECKS (pyunicode, PyUnicode);

VISIBLE SCM
scm_string_to_pyunicode (SCM str, SCM errors)
{
  const char *who = "string->pyunicode";

  scm_dynwind_begin (0);

  char *_errors;
  if (SCM_UNBNDP (errors))
    _errors = "strict";
  else
    {
      _errors = scm_to_utf8_stringn (errors, NULL);
      scm_dynwind_free (_errors);
    }

  size_t length;
  int32_t *s = scm_to_utf32_stringn (str, &length);
  scm_dynwind_free (s);

  int byteorder = 0;            // Native byte order.
  PyObject *u =
    PyUnicode_DecodeUTF32 ((char *) s, 4 * length, _errors, &byteorder);
  if (u == NULL)
    scm_c_py_exc_check_violation
      (who, _("failed to convert string to pyunicode"),
       scm_list_2 (str, scm_from_utf8_string (_errors)));

  scm_dynwind_end ();

  return scm_c_make_pyobject (u);
}

VISIBLE SCM
scm_pyunicode_to_string (SCM pyuni, SCM errors)
{
  const char *who = "pyunicode->string";

  scm_assert_pyobject (pyuni);

  SCM str;

  PyObject *u = scm_to_PyObject_ptr (pyuni);
  if (PyBytes_Check (u))
    str = scm_from_locale_stringn (PyBytes_AS_STRING (u), PyBytes_GET_SIZE (u));
  else
    {
      scm_dynwind_begin (0);

      char *_errors;
      if (SCM_UNBNDP (errors))
        _errors = "strict";
      else
        {
          _errors = scm_to_utf8_stringn (errors, NULL);
          scm_dynwind_free (_errors);
        }

      PyObject *b = PyUnicode_AsEncodedString (u, _UTF32_NATIVE, _errors);
      if (b == NULL)
        scm_c_py_exc_check_violation
          (who, _("failed to encode pyunicode object"),
           scm_list_2 (pyuni, scm_from_utf8_string (_errors)));

      scm_dynwind_end ();

      str = scm_from_utf32_stringn ((scm_t_wchar *) PyBytes_AS_STRING (b),
                                    PyBytes_GET_SIZE (b) / 4);

      Py_CLEAR (b);
    }

  return str;
}

// Generate referenceable copies of inline functions.
VISIBLE SCM scm_pyunicode_to_scm (SCM obj);
VISIBLE SCM scm_scm_to_pyunicode (SCM obj);

#if PY_MAJOR_VERSION <= 2

// Convert a Guile string to a Python 2 string, for instance in the
// implementation of a @code{__repr__} method.
VISIBLE SCM
scm_python_repr (SCM string)
{
  SCM_ASSERT_TYPE (scm_is_string (string), string, SCM_ARG1, "python-repr",
                   "string");
  SCM u = scm_string_to_pyunicode (string,
                                   scm_from_latin1_string ("backslashreplace"));
  PyObject *pybytes =
    PyUnicode_AsEncodedString (scm_to_PyObject_ptr (u), "ascii",
                               "backslashreplace");
  py_exc_check ((pybytes != NULL), "python-repr", scm_list_1 (string));
  return scm_c_make_pyobject (pybytes);
}

#else // !(PY_MAJOR_VERSION <= 2)

// Convert a Guile string to a Python 3 string, for instance in the
// implementation of a @code{__repr__} method.
VISIBLE SCM
scm_python_repr (SCM string)
{
  SCM_ASSERT_TYPE (scm_is_string (string), string, SCM_ARG1, "python-repr",
                   "string");
  return scm_string_to_pyunicode (string,
                                  scm_from_latin1_string ("backslashreplace"));
}

#endif // !(PY_MAJOR_VERSION <= 2)

#if PY_MAJOR_VERSION <= 2

// Convert a Guile string to a Python 2 string, for instance in the
// implementation of a @code{__str__} method.
VISIBLE SCM
scm_python_str (SCM string)
{
  SCM_ASSERT_TYPE (scm_is_string (string), string, SCM_ARG1, "python-str",
                   "string");
  SCM bv =
    // FIXME: Are the Guile escapes suitable for a Python function?
    // Note that @code{str()} results are not expected to be readable
    // as Python code (as opposed to @code{repr()} results, which
    // ideally are valid Python code).
    scm_call_3 (scm_c_public_ref ("ice-9 iconv", "string->bytevector"),
                string,
                scm_call_0 (scm_c_public_ref ("ice-9 i18n", "locale-encoding")),
                scm_from_latin1_symbol ("escape"));
  return scm_bytevector_to_pybytes (bv);
}

#else // !(PY_MAJOR_VERSION <= 2)

// Convert a Guile string to a Python 3 string, for instance in the
// implementation of a @code{__str__} method.
VISIBLE SCM
scm_python_str (SCM string)
{
  SCM_ASSERT_TYPE (scm_is_string (string), string, SCM_ARG1, "python-str",
                   "string");
  return scm_string_to_pyunicode (string,
                                  scm_from_latin1_string ("backslashreplace"));
}

#endif // !(PY_MAJOR_VERSION <= 2)

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pybool, PyBool);

VISIBLE SCM
scm_pybool (SCM obj)
{
  PyObject *truth = (scm_is_true (obj)) ? Py_True : Py_False;
  return scm_c_incref_make_pyobject (truth);
}

VISIBLE SCM
scm_pyobject_true_p (SCM pyobj)
{
  scm_assert_pyobject (pyobj);
  PyObject *obj = scm_to_PyObject_ptr (pyobj);
  int truth;
  if (obj == NULL)
    truth = false;
  else
    {
      truth = PyObject_IsTrue (obj);
      if (truth == -1)
        scm_c_py_exc_check_violation
          ("pyobject-true?", _("PyObject_IsTrue() failed"), scm_list_1 (pyobj));
    }
  return (truth) ? SCM_BOOL_T : SCM_BOOL_F;
}

SCM_PYOBJECT_UNARY_WRAPCHECKS (pyfloat, PyFloat);

VISIBLE SCM
scm_real_to_pyfloat (SCM real_number)
{
  double number = scm_to_double (real_number);
  PyObject *pynumber = PyFloat_FromDouble (number);
  if (pynumber == NULL)
    scm_c_py_exc_check_violation
      ("real->pyfloat", _("conversion to Python float failed"),
       scm_list_1 (real_number));
  return scm_c_make_pyobject (pynumber);
}

VISIBLE SCM
scm_pyfloat_to_real (SCM pyfloat)
{
  scm_assert_pyobject (pyfloat);
  PyObject *obj = scm_to_PyObject_ptr (pyfloat);
  double number = PyFloat_AsDouble (obj);
  if (number == -1.0 && PyErr_Occurred ())
    scm_c_py_exc_check_violation
      ("pyfloat->real", _("conversion from Python float failed"),
       scm_list_1 (pyfloat));
  return scm_from_double (number);
}

SCM_PYOBJECT_UNARY_WRAPCHECKS (pycomplex, PyComplex);

VISIBLE SCM
scm_number_to_pycomplex (SCM complex_number)
{
  double real_part = scm_c_real_part (complex_number);
  double imag_part = scm_c_imag_part (complex_number);
  PyObject *pynumber = PyComplex_FromDoubles (real_part, imag_part);
  if (pynumber == NULL)
    scm_c_py_exc_check_violation
      ("number->pycomplex", _("conversion to Python complex failed"),
       scm_list_1 (complex_number));
  return scm_c_make_pyobject (pynumber);
}

VISIBLE SCM
scm_pycomplex_to_number (SCM pycomplex)
{
  scm_assert_pyobject (pycomplex);
  PyObject *obj = scm_to_PyObject_ptr (pycomplex);
  Py_complex number = PyComplex_AsCComplex (obj);
  if (number.real == -1.0 && number.imag == 0.0 && PyErr_Occurred ())
    scm_c_py_exc_check_violation
      ("pycomplex->number", _("conversion from Python complex failed"),
       scm_list_1 (pycomplex));
  return scm_c_make_rectangular (number.real, number.imag);
}

SCM_PYOBJECT_UNARY_WRAPCHECKS (pyinteger, PyIntOrLong);

VISIBLE SCM
scm_integer_to_pyinteger (SCM integer)
{
  PyObject *value;
  if (scm_is_signed_integer (integer, LONG_MIN, LONG_MAX))
    value = PyIntOrLong_FromLong (scm_to_long (integer));
  else
    {
      mpz_t n;
      mpz_init (n);
      scm_to_mpz (integer, n);
      value = mpz_get_PyLong (n);
      mpz_clear (n);            // FIXME: Consider using a dynamic wind.
    }
  if (value == NULL)
    scm_c_py_exc_check_violation
      ("integer->pyinteger", _("conversion to Python integer failed"),
       scm_list_1 (integer));
  return scm_c_make_pyobject (value);
}

#if PY_MAJOR_VERSION <= 2

// Python 2 version.

VISIBLE SCM
scm_pyinteger_to_integer (SCM pyinteger)
{
  scm_assert_pyobject (pyinteger);
  PyObject *obj = scm_to_PyObject_ptr (pyinteger);
  bool problem = false;
  SCM py_exc_condition = SCM_UNDEFINED;
  SCM value;
  if (PyInt_Check (obj))
    {
      const long int m = PyInt_AsLong (obj);
      problem = (m == -1 && PyErr_Occurred ());
      if (problem)
        py_exc_condition = rnrs_fetch_py_exc_info_condition ();
      else
        value = scm_from_long (m);
    }
  else
    {
      mpz_t n;
      mpz_init (n);
      const int retval = mpz_set_PyIntOrLong (n, obj);
      problem = (retval == -1);
      if (problem)
        py_exc_condition = rnrs_fetch_py_exc_info_condition ();
      else
        value = scm_from_mpz (n);
      mpz_clear (n);            // FIXME: Consider using a dynamic wind.
    }
  if (problem)
    {
      rnrs_raise_condition
        (scm_list_5
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition ("pyinteger->integer"),
          rnrs_c_make_message_condition
          (_("conversion from Python integer failed")),
          rnrs_make_irritants_condition (scm_list_1 (pyinteger)),
          py_exc_condition));
    }
  return value;
}

#else

// Python 3 version.

VISIBLE SCM
scm_pyinteger_to_integer (SCM pyinteger)
{
  scm_assert_pyobject (pyinteger);
  PyObject *obj = scm_to_PyObject_ptr (pyinteger);
  bool problem = false;
  SCM py_exc_condition = SCM_UNDEFINED;
  SCM value;
  int overflow;
  const long int m = PyLong_AsLongAndOverflow (obj, &overflow);
  if (overflow == 0)
    {
      problem = (m == -1 && PyErr_Occurred ());
      if (problem)
        py_exc_condition = rnrs_fetch_py_exc_info_condition ();
      else
        value = scm_from_long (m);
    }
  else
    {
      mpz_t n;
      mpz_init (n);
      const int retval = mpz_set_PyIntOrLong (n, obj);
      problem = (retval == -1);
      if (problem)
        py_exc_condition = rnrs_fetch_py_exc_info_condition ();
      else
        value = scm_from_mpz (n);
      mpz_clear (n);            // FIXME: Consider using a dynamic wind.
    }
  if (problem)
    {
      rnrs_raise_condition
        (scm_list_5
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition ("pyinteger->integer"),
          rnrs_c_make_message_condition
          (_("conversion from Python integer failed")),
          rnrs_make_irritants_condition (scm_list_1 (pyinteger)),
          py_exc_condition));
    }
  return value;
}

#endif

VISIBLE SCM
scm_pyinteger_to_scm (SCM obj)
{
  return (scm_is_pyobject (obj)
          && PyIntOrLong_Check (scm_to_PyObject_ptr (obj))) ?
    scm_pyinteger_to_integer (obj) : obj;
}

VISIBLE bool
scm_is_pyreal (SCM obj)
{
  return (scm_is_pyinteger (obj) || scm_is_pyfloat (obj));
}

VISIBLE SCM
scm_pyreal_p (SCM obj)
{
  return scm_from_bool (scm_is_pyreal (obj));
}

VISIBLE SCM
scm_pyreal_to_real (SCM obj)
{
  SCM result;
  if (scm_is_pyinteger (obj))
    result = scm_pyinteger_to_integer (obj);
  else if (scm_is_pyfloat (obj))
    result = scm_pyfloat_to_real (obj);
  else
    {
      SCM_ASSERT_TYPE (false, obj, SCM_ARG1, "pyreal->real",
                       "pyinteger or pyfloat");
      result = SCM_UNDEFINED;
    }
  return result;
}

static PyObject *
empty_PyDict (const char *who)
{
  PyObject *dict = PyDict_New ();
  if (dict == NULL)
    scm_c_py_exc_check_violation
      (who, _("failed trying to create an empty Python dictionary"), SCM_EOL);
  return dict;
}

VISIBLE PyObject *
scm_to_py_globals (SCM globals)
{
  PyObject *_globals;
  if (scm_is_true (globals))
    {
      if (scm_is_bool (globals))
        // globals = #t
        _globals = PyEval_GetBuiltins ();
      else
        {
          scm_assert_pyobject (globals);
          _globals = scm_to_PyObject_ptr (globals);
        }
    }
  else
    // globals = #f
    _globals = empty_PyDict ("scm_to_py_globals");
  return _globals;
}

VISIBLE PyObject *
scm_to_py_locals (SCM locals)
{
  PyObject *_locals;
  if (scm_is_true (locals))
    {
      scm_assert_pyobject (locals);
      _locals = scm_to_PyObject_ptr (locals);
    }
  else
    // locals = #f
    _locals = empty_PyDict ("scm_to_py_locals");
  return _locals;
}

VISIBLE bool
scm_c_pyobject_hasattr (SCM obj, SCM attr_name)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  attr_name = scm_scm_to_pyunicode (attr_name);
  scm_assert_pyobject (attr_name);
  PyObject *_attr_name = scm_to_PyObject_ptr (attr_name);

  return PyObject_HasAttr (_obj, _attr_name);
}

VISIBLE SCM
scm_pyobject_hasattr_p (SCM obj, SCM attr_name)
{
  return (scm_c_pyobject_hasattr (obj, attr_name)) ? SCM_BOOL_T : SCM_BOOL_F;
}

static SCM_PYOBJECT_BINARY_OP ("pyobject-getattr",
                               _scm_pyobject_getattr, PyObject_GetAttr);

VISIBLE SCM
scm_pyobject_getattr (SCM obj, SCM attr_name)
{
  return _scm_pyobject_getattr (obj, scm_scm_to_pyunicode (attr_name));
}

VISIBLE SCM
scm_pyobject_setattr_x (SCM obj, SCM attr_name, SCM v)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  attr_name = scm_scm_to_pyunicode (attr_name);
  scm_assert_pyobject (attr_name);
  PyObject *_attr_name = scm_to_PyObject_ptr (attr_name);

  scm_assert_pyobject (v);
  PyObject *_v = scm_to_PyObject_ptr (v);

  int errval = PyObject_SetAttr (_obj, _attr_name, _v);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pyobject-setattr!",
       _("PyObject_SetAttr raised a Python exception"),
       scm_list_3 (obj, attr_name, v));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyobject_delattr_x (SCM obj, SCM attr_name)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);

  attr_name = scm_scm_to_pyunicode (attr_name);
  scm_assert_pyobject (attr_name);
  PyObject *_attr_name = scm_to_PyObject_ptr (attr_name);

  int errval = PyObject_DelAttr (_obj, _attr_name);
  if (errval == -1)
    scm_c_py_exc_check_violation
      ("pyobject-delattr!",
       _("PyObject_DelAttr raised a Python exception"),
       scm_list_2 (obj, attr_name));

  return SCM_UNSPECIFIED;
}

static bool
pyobj_richcompare (const char *who, SCM obj1, SCM obj2, int op)
{
  scm_assert_pyobject (obj1);
  PyObject *_obj1 = scm_to_PyObject_ptr (obj1);

  scm_assert_pyobject (obj2);
  PyObject *_obj2 = scm_to_PyObject_ptr (obj2);

  int result = PyObject_RichCompareBool (_obj1, _obj2, op);
  if (result == -1)
    scm_c_py_exc_check_violation
      (who, _("PyObject_RichCompareBool raised a Python exception"),
       scm_list_2 (obj1, obj2));

  return result;
}

VISIBLE bool
scm_pyobject_is_less (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject<?", obj1, obj2, Py_LT);
}

VISIBLE bool
scm_pyobject_is_leq (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject<=?", obj1, obj2, Py_LE);
}

VISIBLE bool
scm_pyobject_is_eq (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject=?", obj1, obj2, Py_EQ);
}

VISIBLE bool
scm_pyobject_is_neq (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject<>?", obj1, obj2, Py_NE);
}

VISIBLE bool
scm_pyobject_is_gr (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject>?", obj1, obj2, Py_GT);
}

VISIBLE bool
scm_pyobject_is_geq (SCM obj1, SCM obj2)
{
  return pyobj_richcompare ("pyobject>=?", obj1, obj2, Py_GE);
}

VISIBLE SCM
scm_pyobject_less_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_less (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_pyobject_leq_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_leq (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_pyobject_eq_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_eq (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_pyobject_neq_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_neq (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_pyobject_gr_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_gr (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM
scm_pyobject_geq_p (SCM obj1, SCM obj2)
{
  return (scm_pyobject_is_geq (obj1, obj2)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-repr",
                               scm_pyobject_repr, PyObject_Repr);

#if PY_MAJOR_VERSION <= 2
VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-unicode",
                               scm_pyobject_unicode, PyObject_Unicode);
#else
VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-unicode",
                               scm_pyobject_unicode, PyObject_Str);
#endif

#if PY_MAJOR_VERSION <= 2
VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-ascii",
                               scm_pyobject_ascii, PyObject_Repr);
#else
VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-ascii",
                               scm_pyobject_ascii, PyObject_ASCII);
#endif

#if PY_MAJOR_VERSION <= 2
VISIBLE SCM
scm_pyobject_bytes (SCM obj)
{
  SCM lst = SCM_EOL;
  SCM p = scm_pysequence_reverse_to_list (obj);
  while (!scm_is_null (p))
    {
      lst = scm_cons (scm_pyinteger_to_integer (SCM_CAR (p)), lst);
      p = SCM_CDR (p);
    }
  return scm_bytevector_to_pybytes (scm_u8_list_to_bytevector (lst));
}
#else
VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-bytes",
                               scm_pyobject_bytes, PyObject_Bytes);
#endif

VISIBLE SCM_PYOBJECT_BINARY_BOOL ("pyobject-isinstance?",
                                  scm_c_pyobject_isinstance,
                                  PyObject_IsInstance);

VISIBLE SCM
scm_pyobject_isinstance_p (SCM inst, SCM cls)
{
  return (scm_c_pyobject_isinstance (inst, cls)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM_PYOBJECT_BINARY_BOOL ("pyobject-issubclass?",
                                  scm_c_pyobject_issubclass,
                                  PyObject_IsSubclass);

VISIBLE SCM
scm_pyobject_issubclass_p (SCM inst, SCM cls)
{
  return (scm_c_pyobject_issubclass (inst, cls)) ? SCM_BOOL_T : SCM_BOOL_F;
}

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pycallable, PyCallable);

VISIBLE SCM
scm_pyobject_call (SCM callable_object, SCM args, SCM kw)
{
  scm_dynwind_begin (0);

  scm_assert_pyobject (callable_object);
  PyObject *_callable_object = scm_to_PyObject_ptr (callable_object);

  PyObject *_args;
  if (unbndp_or_false (args))
    {
      _args = PyTuple_New (0);
      if (_args == NULL)
        scm_c_py_exc_check_violation
          ("pyobject-call", _("failed in trying to make an empty tuple"),
           SCM_EOL);
      else
        scm_dynwind_decref (_args);
    }
  else
    {
      scm_assert_pyobject (args);
      _args = scm_to_PyObject_ptr (args);
    }

  PyObject *_kw;
  if (unbndp_or_false (kw))
    _kw = NULL;
  else
    {
      scm_assert_pyobject (kw);
      _kw = scm_to_PyObject_ptr (kw);
    }

  PyObject *_result = PyObject_Call (_callable_object, _args, _kw);
  py_exc_check ((_result != NULL), "pyobject-call",
                scm_list_2 (callable_object, args));

  scm_dynwind_end ();

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pyobject_callobject (SCM callable_object, SCM args)
{
  scm_assert_pyobject (callable_object);
  PyObject *_callable_object = scm_to_PyObject_ptr (callable_object);

  PyObject *_args;
  if (unbndp_or_false (args))
    _args = NULL;
  else
    {
      scm_assert_pyobject (args);
      _args = scm_to_PyObject_ptr (args);
    }

  PyObject *_result = PyObject_CallObject (_callable_object, _args);
  py_exc_check ((_result != NULL), "pyobject-callobject",
                scm_list_2 (callable_object, args));

  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pyobject_callfunctionobjargs (SCM callable, SCM args_lst)
{
  // Similar to (though not based on)
  // PyObject_CallFunctionObjArgs(). Handles keyword arguments.
  SCM pyargs = scm_keyworded_list_to_py_arguments (args_lst);
  return scm_pyobject_call (callable, scm_c_value_ref (pyargs, 0),
                            scm_c_value_ref (pyargs, 1));
}

VISIBLE SCM
scm_pyobject_callmethodobjargs (SCM obj, SCM method_name, SCM args_lst)
{
  // Similar to (though not based on)
  // PyObject_CallMethodObjArgs(). Handles keyword arguments.
  SCM callable = scm_pyobject_getattr (obj, scm_scm_to_pyunicode (method_name));
  SCM pyargs = scm_keyworded_list_to_py_arguments (args_lst);
  return scm_pyobject_call (callable, scm_c_value_ref (pyargs, 0),
                            scm_c_value_ref (pyargs, 1));
}

VISIBLE SCM
scm_pyobject_to_procedure (SCM callable)
{
  return
    scm_call_1 (scm_c_private_ref (_PYOBJECTS_MODULE, "pyobject->procedure"),
                callable);
}

VISIBLE SCM
scm_pyobject_method_to_procedure (SCM obj, SCM method_name)
{
  return scm_call_2 (scm_c_private_ref (_PYOBJECTS_MODULE,
                                        "pyobject-method->procedure"),
                     obj, method_name);
}

static SCM_PYOBJECT_UNARY_INTTYPE (long, "pyobject-hash",
                                   scm_c_pyobject_hash, PyObject_Hash);

VISIBLE SCM
scm_pyobject_hash (SCM obj)
{
  return scm_from_long (scm_c_pyobject_hash (obj));
}

static SCM_PYOBJECT_UNARY_BOOL ("pyobject-not", scm_c_pyobject_not,
                                PyObject_Not);

VISIBLE SCM
scm_pyobject_not (SCM obj)
{
  PyObject *truth = (scm_c_pyobject_not (obj)) ? Py_True : Py_False;
  return scm_c_incref_make_pyobject (truth);
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-type", scm_pyobject_type,
                               PyObject_Type);

VISIBLE SCM_PYOBJECT_UNARY_INTTYPE (ssize_t, "pyobject-length",
                                    scm_c_pyobject_length, PyObject_Length);

VISIBLE SCM
scm_pyobject_length (SCM obj)
{
  return scm_from_ssize_t (scm_c_pyobject_length (obj));
}

VISIBLE SCM_PYOBJECT_BINARY_OP ("pyobject-getitem",
                                scm_pyobject_getitem, PyObject_GetItem);

VISIBLE SCM_PYOBJECT_TERNARY_UNSPEC ("pyobject-setitem!",
                                     scm_pyobject_setitem_x, PyObject_SetItem);

VISIBLE SCM_PYOBJECT_BINARY_UNSPEC ("pyobject-delitem!",
                                    scm_pyobject_delitem_x, PyObject_DelItem);

VISIBLE SCM_PYOBJECT_UNARY_INTTYPE (int, "pyobject-asfiledescriptor",
                                    scm_c_pyobject_asfiledescriptor,
                                    PyObject_AsFileDescriptor);

VISIBLE SCM
scm_pyobject_asfiledescriptor (SCM obj)
{
  return scm_from_int (scm_c_pyobject_asfiledescriptor (obj));
}

VISIBLE SCM
scm_pyobject_dir (SCM obj)
{
  PyObject *_result;
  bool error;
  if (unbndp_or_false (obj))
    {
      _result = PyObject_Dir (NULL);
      error = (_result == NULL && PyErr_Occurred ());
    }
  else
    {
      scm_assert_pyobject (obj);
      PyObject *_obj = scm_to_PyObject_ptr (obj);
      _result = PyObject_Dir (_obj);
      error = (_result == NULL);
    }
  if (error)
    scm_c_py_exc_check_violation
      ("pyobject-dir", _("PyObject_Dir raised a Python exception"),
       scm_list_1 (obj));
  return scm_c_make_pyobject (_result);
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pyobject-getiter",
                               scm_pyobject_getiter, PyObject_GetIter);

//-------------------------------------------------------------------------
