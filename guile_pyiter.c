#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

//-------------------------------------------------------------------------

VISIBLE SCM_PYOBJECT_UNARY_BOOLCHECK (scm_is_pyiter, PyIter_Check);

VISIBLE SCM
scm_pyiter_p (SCM obj)
{
  return scm_from_bool (scm_is_pyiter (obj));
}

VISIBLE SCM
scm_pyiter_next_x (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyIter_Check (_obj), obj, SCM_ARG1, "pyiter-next!",
                   "pyiter");

  PyObject *_next = PyIter_Next (_obj);
  py_exc_check ((_next != NULL || !PyErr_Occurred ()), "pyiter-next!",
                scm_list_1 (obj));

  return (_next == NULL) ? SCM_BOOL_F : scm_c_make_pyobject (_next);
}

//-------------------------------------------------------------------------

typedef struct
{
  // *INDENT-OFF*
  PyObject_HEAD
  // *INDENT-ON*
  SCM var;
} GuileStreamIterObject;

static void
GuileStreamIter__dealloc (PyObject *self)
{
  scm_gc_unprotect_object (((GuileStreamIterObject *) self)->var);
  free (self);
}

static PyObject *
GuileStreamIter__next (PyObject *self)
{
  PyObject *next;
  SCM stream = scm_variable_ref (((GuileStreamIterObject *) self)->var);
  if (scm_is_true (scm_call_1 (scm_c_public_ref ("srfi srfi-41",
                                                 "stream-null?"), stream)))
    {
      next = NULL;
      PyErr_SetNone (PyExc_StopIteration);
    }
  else
    {
      SCM car =
        scm_call_1 (scm_c_public_ref ("srfi srfi-41", "stream-car"), stream);
      scm_assert_pyobject (car);
      next = scm_to_PyObject_ptr (car);
      Py_INCREF (next);

      SCM cdr =
        scm_call_1 (scm_c_public_ref ("srfi srfi-41", "stream-cdr"), stream);
      scm_variable_set_x (((GuileStreamIterObject *) self)->var, cdr);
    }
  return next;
}

static PyTypeObject GuileStreamIter_Type = {
  // *INDENT-OFF*
  PyVarObject_HEAD_INIT (NULL, 0)
  // *INDENT-ON*
  .tp_name = "guile_stream_iter",
  .tp_basicsize = sizeof (GuileStreamIterObject),
  .tp_dealloc = GuileStreamIter__dealloc,
#if PY_MAJOR_VERSION <= 2
  .tp_flags = Py_TPFLAGS_HAVE_ITER,
#else
  .tp_flags = 0,
#endif
  .tp_iter = PyObject_SelfIter,
  .tp_iternext = GuileStreamIter__next
};

// *INDENT-OFF*
STM_SCM_ONE_TIME_INITIALIZE
  (static STM_ATTRIBUTE_PURE, PyTypeObject *, guile_stream_iter_type,
   {
     const int errval = PyType_Ready (&GuileStreamIter_Type);
     py_exc_check ((errval != -1), "PyType_Ready", SCM_EOL);
     guile_stream_iter_type__Value = &GuileStreamIter_Type;
     Py_INCREF (guile_stream_iter_type__Value);
   });
// *INDENT-ON*

static PyObject *
create_GuileStreamIter (SCM stream)
{
  PyObject *obj = scm_malloc (sizeof (GuileStreamIterObject));
  ((PyObject *) obj)->ob_type = guile_stream_iter_type ();
  Py_INCREF (((PyObject *) obj)->ob_type);
  ((PyObject *) obj)->ob_refcnt = 1;
  ((GuileStreamIterObject *) obj)->var = scm_make_variable (stream);
  scm_gc_protect_object (((GuileStreamIterObject *) obj)->var);
  return obj;
}

VISIBLE SCM
scm_stream_to_pyiter (SCM stream)
{
  SCM_ASSERT_TYPE ((scm_call_1 (scm_c_public_ref ("srfi srfi-41", "stream?"),
                                stream)),
                   stream, SCM_ARG1, "stream->pyiter", "stream");
  return scm_c_make_pyobject (create_GuileStreamIter (stream));
}

//-------------------------------------------------------------------------

typedef struct
{
  // *INDENT-OFF*
  PyObject_HEAD
  // *INDENT-ON*
  SCM thunk;
  bool active;
} GuileThunkIterObject;

static void
GuileThunkIter__dealloc (PyObject *self)
{
  scm_gc_unprotect_object (((GuileThunkIterObject *) self)->thunk);
  free (self);
}

static PyObject *
GuileThunkIter__next (PyObject *self)
{
  // The ‘thunk’ procedure returns #f (rather than a Python object) at
  // the end of iteration.

  PyObject *next;
  if (!((GuileThunkIterObject *) self)->active)
    {
      next = NULL;
      PyErr_SetNone (PyExc_StopIteration);
    }
  else
    {
      SCM val = scm_call_0 (((GuileThunkIterObject *) self)->thunk);
      if (scm_is_false (val))
        {
          ((GuileThunkIterObject *) self)->active = false;
          next = NULL;
          PyErr_SetNone (PyExc_StopIteration);
        }
      else
        {
          scm_assert_pyobject (val);
          next = scm_to_PyObject_ptr (val);
          Py_INCREF (next);
        }
    }
  return next;
}

static PyTypeObject GuileThunkIter_Type = {
  // *INDENT-OFF*
  PyVarObject_HEAD_INIT (NULL, 0)
  // *INDENT-ON*
  .tp_name = "guile_thunk_iter",
  .tp_basicsize = sizeof (GuileThunkIterObject),
  .tp_dealloc = GuileThunkIter__dealloc,
#if PY_MAJOR_VERSION <= 2
  .tp_flags = Py_TPFLAGS_HAVE_ITER,
#else
  .tp_flags = 0,
#endif
  .tp_iter = PyObject_SelfIter,
  .tp_iternext = GuileThunkIter__next
};

// *INDENT-OFF*
STM_SCM_ONE_TIME_INITIALIZE
  (static STM_ATTRIBUTE_PURE, PyTypeObject *, guile_thunk_iter_type,
   {
     const int errval = PyType_Ready (&GuileThunkIter_Type);
     py_exc_check ((errval != -1), "PyType_Ready", SCM_EOL);
     guile_thunk_iter_type__Value = &GuileThunkIter_Type;
     Py_INCREF (guile_thunk_iter_type__Value);
   });
// *INDENT-ON*

static PyObject *
create_GuileThunkIter (SCM thunk)
{
  PyObject *obj = scm_malloc (sizeof (GuileThunkIterObject));
  ((PyObject *) obj)->ob_type = guile_thunk_iter_type ();
  Py_INCREF (((PyObject *) obj)->ob_type);
  ((PyObject *) obj)->ob_refcnt = 1;
  ((GuileThunkIterObject *) obj)->thunk = thunk;
  scm_gc_protect_object (((GuileThunkIterObject *) obj)->thunk);
  ((GuileThunkIterObject *) obj)->active = true;
  return obj;
}

VISIBLE SCM
scm_thunk_to_pyiter (SCM thunk)
{
  SCM_ASSERT_TYPE ((scm_thunk_p (thunk)), thunk, SCM_ARG1,
                   "thunk->pyiter", "thunk");
  return scm_c_make_pyobject (create_GuileThunkIter (thunk));
}

//-------------------------------------------------------------------------
