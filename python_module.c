#include <config.h>

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <frameobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#if 3 <= PY_MAJOR_VERSION
PyDoc_STRVAR (smcoreguile_module_docstring, "Foreign interface to GNU Guile.");
#endif

typedef struct
{
  const char *who;
  PyObject *self;
  PyObject *args;
  PyObject *kw;
} _self_args_kw;

static void
pyerr_set_guile_error (void)
{
  // FIXME: Have this include Guile exception information, if
  // possible.
  PyErr_SetString (PyExc_RuntimeError, "Guile error");
}

static inline void
pyerr_set_assert (bool assertion)
{
  if (!assertion && !PyErr_Occurred ())
    pyerr_set_guile_error ();
}

static void *
_guile_module_exists_internal (void *datap)
{
  PyObject *result = Py_False;

  PyObject *args = ((_self_args_kw *) datap)->args;
  PyObject *kw = ((_self_args_kw *) datap)->kw;

  static char *kwlist[] = { "module", NULL };
  char *module;
  const bool parsing_succeeded =
    PyArg_ParseTupleAndKeywords (args, kw, "s:guile_module_exists", kwlist,
                                 &module);

  if (parsing_succeeded)
    {
      SCM pymodule_name = scm_from_utf8_string (module);
      if (scm_is_true (scm_pymodule_pyinit_variable (pymodule_name)))
        result = Py_True;
    }

  Py_INCREF (result);
  return (void *) result;
}

static void
pyerr_set_not_a_module (SCM obj, SCM pymodule_name)
{
  scm_dynwind_begin (0);

  SCM object_string =
    scm_simple_format (SCM_BOOL_F, scm_from_latin1_string ("~S"),
                       scm_list_1 (obj));
  char *_object_string = scm_to_locale_stringn (object_string, NULL);
  scm_dynwind_free (_object_string);

  SCM pyinit_name = scm_pyinit_name (pymodule_name);
  char *_pyinit_name =
    scm_to_locale_stringn (scm_symbol_to_string (pyinit_name), NULL);
  scm_dynwind_free (_pyinit_name);

  PyErr_Format (PyExc_ImportError,
                _("the value %s returned by %s is not a Python module"),
                _object_string, _pyinit_name);

  scm_dynwind_end ();
}

static PyObject *
_guile_module_load_by_name (SCM pymodule_name)
{
  PyObject *result = NULL;

  SCM pyinit_proc = scm_pymodule_pyinit_procedure (pymodule_name);

  if (scm_is_false (pyinit_proc))
    {
      char *name = scm_to_utf8_stringn (pymodule_name, NULL);
      PyErr_Format (PyExc_ImportError, _("module %s was not found"), name);
      free (name);
    }
  else
    {
      SCM new_pymodule = scm_call_0 (pyinit_proc);
      if (scm_is_pyobject (new_pymodule))
        result = scm_to_PyObject_ptr (new_pymodule);
      if (result == NULL || !PyModule_Check (result))
        {
          pyerr_set_not_a_module (new_pymodule, pymodule_name);
          result = NULL;
        }
    }
  return result;
}

static void *
_guile_module_load_internal (void *datap)
{
  PyObject *result = NULL;

  PyObject *args = ((_self_args_kw *) datap)->args;
  PyObject *kw = ((_self_args_kw *) datap)->kw;

  static char *kwlist[] = { "module", NULL };
  char *module;
  const bool parsing_succeeded =
    PyArg_ParseTupleAndKeywords (args, kw, "s:guile_module_load", kwlist,
                                 &module);

  if (parsing_succeeded)
    {
      SCM pymodule_name = scm_from_utf8_string (module);
      result = _guile_module_load_by_name (pymodule_name);
    }

  return (void *) result;
}

static PyObject *
guile_module_exists (PyObject *self, PyObject *args, PyObject *kw)
{
  _self_args_kw data = {
    .who = "guile_module_exists",
    .self = self,
    .args = args,
    .kw = kw
  };
  PyObject *result = (PyObject *)
    scm_c_with_continuation_barrier (_guile_module_exists_internal, &data);
  pyerr_set_assert ((result != NULL));
  return result;
}

static PyObject *
guile_module_load (PyObject *self, PyObject *args, PyObject *kw)
{
  _self_args_kw data = {
    .who = "guile_module_load",
    .self = self,
    .args = args,
    .kw = kw
  };
  PyObject *result = (PyObject *)
    scm_c_with_continuation_barrier (_guile_module_load_internal, &data);
  pyerr_set_assert ((result != NULL));
  return result;
}

static void *
_guile_find_module_internal (void *datap)
{
  PyObject *loader = Py_None;

  PyObject *self = ((_self_args_kw *) datap)->self;
  PyObject *args = ((_self_args_kw *) datap)->args;

  char *pymod_name;
  PyObject *path;
  const bool parsing_succeeded =
    PyArg_ParseTuple (args, "sO:find_module", &pymod_name, &path);
  if (parsing_succeeded)
    {
      SCM pymodule_name = scm_from_utf8_string (pymod_name);
      if (scm_is_true (scm_pymodule_pyinit_variable (pymodule_name)))
        loader = self;
    }

  Py_XINCREF (loader);
  return loader;
}

static PyObject *
guile_find_module (PyObject *self, PyObject *args)
{
  _self_args_kw data = {
    .who = "find_module",
    .self = self,
    .args = args
  };
  PyObject *loader = (PyObject *)
    scm_c_with_continuation_barrier (_guile_find_module_internal, &data);
  pyerr_set_assert ((loader != NULL));
  return loader;
}

static void *
_guile_load_module_internal (void *datap)
{
  PyObject *module = NULL;

  PyObject *self = ((_self_args_kw *) datap)->self;
  PyObject *args = ((_self_args_kw *) datap)->args;

  char *pymod_name;
  const bool parsing_succeeded =
    PyArg_ParseTuple (args, "s:load_module", &pymod_name);
  if (parsing_succeeded)
    {
      SCM pymodule_name = scm_from_utf8_string (pymod_name);
      SCM sys_modules = scm_pyimport_getmoduledict ();
      if (scm_is_true (scm_pymapping_haskeystring_p (sys_modules,
                                                     pymodule_name)))
        {
          SCM m = scm_pymapping_getitemstring (sys_modules, pymodule_name);
          module = scm_to_PyObject_ptr (m);
        }
      else
        {
          module = _guile_module_load_by_name (pymodule_name);
          if (module != NULL)
            {
              SCM pyname;
#if PY_MAJOR_VERSION <= 2
              pyname =
                scm_bytevector_to_pybytes (scm_string_to_utf8 (pymodule_name));
#else
              pyname = scm_string_to_pyunicode (pymodule_name, SCM_UNDEFINED);
#endif
              SCM m = scm_c_incref_make_pyobject (module);
              SCM d = scm_pymodule_getdict (m);
              if (scm_c_pymapping_haskeystring (d, "__name__"))
                scm_c_pymapping_setitemstring (d, "__name__", pyname);
              if (scm_c_pymapping_haskeystring (d, "__package__"))
                scm_c_pymapping_setitemstring (d, "__package__", pyname);
              scm_c_pymapping_setitemstring (d, "__path__",
                                             scm_list_to_pylist (SCM_EOL));
              scm_c_pymapping_setitemstring (d, "__loader__",
                                             scm_c_make_pyobject (self));
              scm_pymapping_setitemstring_x (sys_modules, pymodule_name, m);
            }
        }
    }

  return module;
}

static PyObject *
guile_load_module (PyObject *self, PyObject *args)
{
  _self_args_kw data = {
    .who = "load_module",
    .self = self,
    .args = args
  };
  PyObject *module = (PyObject *)
    scm_c_with_continuation_barrier (_guile_load_module_internal, &data);
  pyerr_set_assert ((module != NULL));
  return module;
}

static void *
run_gc (void *datap STM_MAYBE_UNUSED)
{
  scm_gc ();

  // Return an arbitrary non-NULL pointer.
  return run_gc;
}

static PyObject *
run_guile_gc (PyObject *self STM_MAYBE_UNUSED, PyObject *args STM_MAYBE_UNUSED)
{
  scm_c_with_continuation_barrier (run_gc, NULL);
  Py_INCREF (Py_None);
  return Py_None;
}

static PyMethodDef smcoreguile_meta_finder_loader_methods[] = {
  {.ml_name = "find_module",
   .ml_meth = (PyCFunction) guile_find_module,
   .ml_flags = METH_VARARGS,
   .ml_doc = NULL},             // FIXME: Write a documentation string.
  {.ml_name = "load_module",
   .ml_meth = (PyCFunction) guile_load_module,
   .ml_flags = METH_VARARGS,
   .ml_doc = NULL},             // FIXME: Write a documentation string.
  NULL
};

// *INDENT-OFF*
typedef struct
{
  PyObject_HEAD
} meta_finder_loader_struct;
// *INDENT-ON*

static PyTypeObject meta_finder_loader_Type = {
  // *INDENT-OFF*
  PyVarObject_HEAD_INIT (NULL, 0)
  // *INDENT-ON*
  .tp_name = "sortsmill_core_guile.meta_finder_loader",
  .tp_basicsize = sizeof (meta_finder_loader_struct),
  .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
  .tp_doc = NULL,               // FIXME: Write a documentation string.
  .tp_methods = smcoreguile_meta_finder_loader_methods
};

static PyMethodDef smcoreguile_methods[] = {
  {.ml_name = "guile_module_exists",
   .ml_meth = (PyCFunction) guile_module_exists,
   .ml_flags = METH_VARARGS | METH_KEYWORDS,
   .ml_doc = NULL},             // FIXME: Write a documentation string.
  {.ml_name = "guile_module_load",
   .ml_meth = (PyCFunction) guile_module_load,
   .ml_flags = METH_VARARGS | METH_KEYWORDS,
   .ml_doc = NULL},             // FIXME: Write a documentation string.
  {.ml_name = "run_guile_gc",
   .ml_meth = (PyCFunction) run_guile_gc,
   .ml_flags = METH_NOARGS,
   .ml_doc = NULL},             // FIXME: Write a documentation string.
  NULL
};

static bool
initialize_guile (void)
{
#ifdef HAVE_SCM_INIT_GUILE
  scm_init_guile ();
#endif

  bool initialized;
#ifdef HAVE_SCM_INITIALIZED_P
  initialized = scm_initialized_p;
#else
  initialized = true;
#endif

  if (!initialized)
    // FIXME: Better handling of this situation.
    fprintf (stderr, "Guile initialization failed. You might need to\n"
             "run `sortsmill-core-python%d.%d' instead of `python'.",
             PY_MAJOR_VERSION, PY_MINOR_VERSION);

  SCM module = scm_call_2 (scm_c_public_ref ("guile", "resolve-module"),
                           scm_list_1 (scm_from_latin1_symbol
                                       ("__smcoreguile__python_extension_data__")),
                           SCM_BOOL_F);
  scm_call_3 (scm_c_public_ref ("guile", "module-add!"),
              module, scm_from_latin1_symbol ("python-major-version"),
              scm_make_variable (scm_from_uint (PY_MAJOR_VERSION)));
  scm_call_3 (scm_c_public_ref ("guile", "module-add!"),
              module, scm_from_latin1_symbol ("python-minor-version"),
              scm_make_variable (scm_from_uint (PY_MINOR_VERSION)));

  return initialized;
}

static void *
run_module_hooks (void *vmodule)
{
  PyObject *_module = (PyObject *) vmodule;
  SCM module = scm_c_incref_make_pyobject (_module);
  SCM hooks_parameter =
    scm_c_private_ref ("sortsmill core python smcoreguile-pymodule",
                       "py-sortsmill_core_guile-hooks");
  SCM hooks = scm_call_0 (hooks_parameter);
  for (SCM p = hooks; !scm_is_null (p); p = scm_cdr (p))
    scm_call_1 (scm_car (p), module);
  scm_remember_upto_here_1 (module);
  return &run_module_hooks;     // An arbitrary non-NULL pointer.
}

static void
initialize_module (PyObject **modulep)
{
  if (*modulep != NULL)
    {
      meta_finder_loader_Type.tp_new = PyType_GenericNew;
      int errval = PyType_Ready (&meta_finder_loader_Type);
      if (errval != -1)
        errval = PySCM_Ready ();
      if (errval == -1)
        Py_CLEAR (*modulep);
      else
        {
          // Add a @code{__path__} to the module, to make it a
          // package.
          PyObject *__path__ = PyList_New (0);
          if (__path__ == NULL)
            Py_CLEAR (*modulep);
          else
            {
              PyModule_AddObject (*modulep, "__path__", __path__);

              // Add the @code{scm} type to the module. Coded so as to
              // avoid -Wstrict-aliasing warnings.
              PyObject *PySCM_Type_ptr = (PyObject *) &PySCM_Type;
              Py_INCREF (PySCM_Type_ptr);
              PyModule_AddObject (*modulep, "scm", (PyObject *) &PySCM_Type);

              // Add the @code{meta_finder_loader} class to the
              // module. Coded so as to avoid -Wstrict-aliasing
              // warnings.
              PyObject *mfl_Type_ptr = (PyObject *) &meta_finder_loader_Type;
              Py_INCREF (mfl_Type_ptr);
              PyModule_AddObject (*modulep, "meta_finder_loader",
                                  (PyObject *) &meta_finder_loader_Type);

              // Add an instance of @code{meta_finder_loader} to the end
              // of @code{sys.meta_path}. Thus you can import Python
              // modules directly from Guile modules, once you have
              // imported @code{sortsmill_core_guile}. You install the
              // Guile modules in the usual way for Guile.
              PyObject *meta_path = PySys_GetObject ("meta_path");
              if (meta_path != NULL)
                {
                  PyObject *finder =
                    PyObject_CallObject ((PyObject *) &meta_finder_loader_Type,
                                         NULL);
                  if (finder == NULL)
                    Py_CLEAR (*modulep);
                  else
                    {
                      errval = PyList_Append (meta_path, finder);
                      Py_DECREF (finder);
                      if (errval < -1)
                        Py_CLEAR (*modulep);
                      else
                        {
                          void *errptr =
                            scm_c_with_continuation_barrier (run_module_hooks,
                                                             *modulep);
                          if (errptr == NULL)
                            Py_CLEAR (*modulep);
                        }
                    }
                }
            }
        }
    }
}

static void *
register_atexit_with_barrier (void *datap)
{
  // Run the Guile garbage collector at Python exit.

  SCM scg_module = scm_c_incref_make_pyobject ((PyObject *) datap);
  SCM scg_dict = scm_pymodule_getdict (scg_module);
  SCM gc_name =
    scm_string_to_pyunicode (scm_from_latin1_string ("run_guile_gc"),
                             SCM_UNDEFINED);
  SCM gc_func = scm_pyobject_getitem (scg_dict, gc_name);

  SCM atexit_name =
    scm_string_to_pyunicode (scm_from_latin1_string ("atexit"), SCM_UNDEFINED);
  SCM atexit_module = scm_pyimport_import (atexit_name);
  SCM atexit_dict = scm_pymodule_getdict (atexit_module);
  SCM register_name =
    scm_string_to_pyunicode (scm_from_latin1_string ("register"),
                             SCM_UNDEFINED);
  SCM register_func = scm_pyobject_getitem (atexit_dict, register_name);

  PyObject *_gc_func = scm_to_PyObject_ptr (gc_func);
  Py_INCREF (_gc_func);

  PyObject *_register_func = scm_to_PyObject_ptr (register_func);
  Py_INCREF (_register_func);

  PyObject *retval =
    PyObject_CallFunctionObjArgs (_register_func, _gc_func, NULL);

  Py_DECREF (_register_func);

  if (retval == NULL)
    {
      Py_DECREF (_gc_func);
      py_exc_check (false, "register_atexit_with_barrier", SCM_EOL);
    }

  // Return an arbitrary non-NULL pointer.
  return register_atexit_with_barrier;
}

static void
register_atexit (PyObject *smcoreguile_module)
{
  scm_c_with_continuation_barrier (register_atexit_with_barrier,
                                   smcoreguile_module);
}

#if PY_MAJOR_VERSION <= 2

VISIBLE PyMODINIT_FUNC
initsortsmill_core_guile (void)
{
  PyObject *module = NULL;

  const bool guile_initialized = initialize_guile ();
  if (guile_initialized)
    {
      module = Py_InitModule ("sortsmill_core_guile", smcoreguile_methods);
      initialize_module (&module);
      register_atexit (module);
    }
}

#else // !(PY_MAJOR_VERSION <= 2)

static struct PyModuleDef smcoreguile_module = {
  PyModuleDef_HEAD_INIT,
  .m_name = "sortsmill_core_guile",
  .m_doc = smcoreguile_module_docstring,
  .m_size = -1,
  .m_methods = smcoreguile_methods
};

VISIBLE PyMODINIT_FUNC
PyInit_sortsmill_core_guile (void)
{
  PyObject *module = NULL;

  const bool guile_initialized = initialize_guile ();
  if (guile_initialized)
    {
      module = PyModule_Create (&smcoreguile_module);
      initialize_module (&module);
      register_atexit (module);
    }

  return module;
}

#endif // !(PY_MAJOR_VERSION <= 2)
