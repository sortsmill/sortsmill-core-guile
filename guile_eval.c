#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/guile/core/eval.h>

#define _EVAL_STRING_MODULE "ice-9 eval-string"

VISIBLE SCM
scm_c_eval_locale_string (const char *s)
{
  return scm_eval_string (scm_from_locale_string (s));
}

VISIBLE SCM
scm_c_eval_utf8_string (const char *s)
{
  return scm_eval_string (scm_from_utf8_string (s));
}

VISIBLE SCM
scm_c_eval_locale_string_in_module (const char *s, SCM module)
{
  return scm_eval_string_in_module (scm_from_locale_string (s), module);
}

VISIBLE SCM
scm_c_eval_utf8_string_in_module (const char *s, SCM module)
{
  return scm_eval_string_in_module (scm_from_utf8_string (s), module);
}

VISIBLE SCM
scm_compile_eval_string (SCM s)
{
  return scm_call_3 (scm_c_public_ref (_EVAL_STRING_MODULE, "eval-string"),
                     s, scm_from_latin1_keyword ("compile?"), SCM_BOOL_T);
}

VISIBLE SCM
scm_c_compile_eval_locale_string (const char *s)
{
  return scm_compile_eval_string (scm_from_locale_string (s));
}

VISIBLE SCM
scm_c_compile_eval_utf8_string (const char *s)
{
  return scm_compile_eval_string (scm_from_utf8_string (s));
}

VISIBLE SCM
scm_compile_eval_string_in_module (SCM s, SCM module)
{
  return scm_call_5 (scm_c_public_ref (_EVAL_STRING_MODULE, "eval-string"),
                     s, scm_from_latin1_keyword ("compile?"), SCM_BOOL_T,
                     scm_from_latin1_keyword ("module"), module);
}

VISIBLE SCM
scm_c_compile_eval_locale_string_in_module (const char *s, SCM module)
{
  return scm_compile_eval_string_in_module (scm_from_locale_string (s), module);
}

VISIBLE SCM
scm_c_compile_eval_utf8_string_in_module (const char *s, SCM module)
{
  return scm_compile_eval_string_in_module (scm_from_utf8_string (s), module);
}
