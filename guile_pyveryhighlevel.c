#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <stdio.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>
#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

static void
fclose_FILE (void *p)
{
  fclose ((FILE *) p);
}

static void
scm_dynwind_fclose (FILE *fp)
{
  scm_dynwind_unwind_handler (fclose_FILE, fp, SCM_F_WIND_EXPLICITLY);
}

static SCM
port_or_current_input (const char *who, SCM port)
{
  if (unbndp_or_false (port))
    port = scm_current_input_port ();
  else
    SCM_ASSERT_TYPE (scm_is_true (scm_input_port_p (port)), port,
                     SCM_ARG1, who, "input port");
  return port;
}

static PyCompilerFlags
scm_to_PyCompilerFlags (SCM flags)
{
  PyCompilerFlags _flags;
  _flags.cf_flags = (unbndp_or_false (flags)) ? 0 : scm_to_int (flags);
  return _flags;
}

static void
raise_pyexc_with_info_unavailable (const char *who, SCM irritants)
{
  PyErr_Clear ();
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_assertion_violation (),
      rnrs_c_make_who_condition (who),
      rnrs_c_make_message_condition
      (_("a Python exception was raised (&py-exc-info unavailable)")),
      rnrs_make_irritants_condition (irritants)));
}

static FILE *
open_input_port_as_file (const char *who, SCM port)
{
  const int fd =
    scm_to_int (scm_call_1 (scm_c_public_ref ("guile", "port->fdes"), port));
  FILE *f = fdopen (fd, "r");
  if (f == NULL)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("failed to open Guile port as a (FILE*) for input")),
        rnrs_make_irritants_condition (scm_list_1 (port))));
  return f;
}

static char *
port_file_name (SCM port, SCM file_name)
{
  char *result;
  if (unbndp_or_false (file_name))
    {
      SCM port_fn = scm_port_filename (port);
      if (scm_is_true (port_fn))
        result = scm_to_locale_string (port_fn);
      else
        result = NULL;
    }
  else
    result = scm_to_locale_string (file_name);
  return result;
}

#if PY_MAJOR_VERSION < 3 || (PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION < 2)

static inline PyObject *
Py_CompileStringExFlags (const char *str, const char *filename, int start,
                         PyCompilerFlags *flags, int optimize STM_MAYBE_UNUSED)
{
  // @code{Py_CompileStringExFlags}, with the @var{optimize} argument,
  // is not available before Python 3.2.
  return Py_CompileStringFlags (str, filename, start, flags);
}

#endif

static void
optional_PyObject_array (SCM lst, PyObject ***array, int *count)
{
  if (unbndp_or_false (lst))
    {
      *array = NULL;
      *count = 0;
    }
  else
    {
      const int n = scm_to_int (scm_length (lst));
      PyObject **arr =
        scm_gc_malloc (n * sizeof (PyObject *), "PyObject array");
      SCM p = lst;
      for (int i = 0; i < n; i++)
        {
          scm_assert_pyobject (SCM_CAR (p));
          arr[i] = scm_to_PyObject_ptr (SCM_CAR (p));
          p = SCM_CDR (p);
        }
      *array = arr;
      *count = n;
    }
}

//-------------------------------------------------------------------------

#if PY_MAJOR_VERSION < 3

// scm_py_main() for Python 2.

VISIBLE SCM
scm_py_main (SCM program_params)
{
  const int argc = scm_to_int (scm_length (program_params));

  scm_dynwind_begin (0);

  char **argv = scm_malloc ((argc + 1) * sizeof (char *));
  scm_dynwind_free (argv);

  SCM p = program_params;
  for (int i = 0; i < argc; i++)
    {
      argv[i] = scm_to_locale_string (SCM_CAR (p));
      scm_dynwind_free (argv[i]);
      p = SCM_CDR (p);
    }
  argv[argc] = NULL;

  int retval = Py_Main (argc, argv);

  scm_dynwind_end ();

  return scm_from_int (retval);
}

#else

// scm_py_main() for Python 3.
//
// This is different from the Python 2 version because the Py_Main()
// in Python 3 uses the horrible wchar_t type instead of locale
// encoding.

#include <wchar.h>

VISIBLE SCM
scm_py_main (SCM program_params)
{
  const int argc = scm_to_int (scm_length (program_params));

  scm_dynwind_begin (0);

  wchar_t **argv = scm_malloc ((argc + 1) * sizeof (wchar_t *));
  scm_dynwind_free (argv);

  SCM p = program_params;
  for (int i = 0; i < argc; i++)
    {
      char *s = scm_to_locale_string (SCM_CAR (p));
      const size_t n = strlen (s);
      argv[i] = scm_malloc ((n + 1) * sizeof (wchar_t));
      scm_dynwind_free (argv[i]);
      mbstowcs (argv[i], s, n + 1);
      free (s);
      p = SCM_CDR (p);
    }
  argv[argc] = NULL;

  int retval = Py_Main (argc, argv);

  scm_dynwind_end ();

  return scm_from_int (retval);
}

#endif

VISIBLE SCM
scm_pyrun_simplestring (SCM command, SCM flags)
{
  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);

  scm_dynwind_begin (0);
  char *_command = scm_to_locale_string (command);
  scm_dynwind_free (_command);
  const int retval = PyRun_SimpleStringFlags (_command, &_flags);
  scm_dynwind_end ();

  if (retval == -1)
    raise_pyexc_with_info_unavailable ("pyrun-simplestring",
                                       scm_list_2 (command, flags));

  return scm_from_int (_flags.cf_flags);
}

VISIBLE SCM
scm_pyrun_anyfile (SCM port, SCM flags, SCM file_name)
{
  const char *who = "pyrun-anyfile";

  port = port_or_current_input (who, port);
  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);

  scm_dynwind_begin (0);

  FILE *f = open_input_port_as_file (who, port);
  scm_dynwind_fclose (f);

  char *filename = port_file_name (port, file_name);
  scm_dynwind_free (filename);

  const int retval = PyRun_AnyFileExFlags (f, filename, false, &_flags);

  scm_dynwind_end ();

  if (retval == -1)
    raise_pyexc_with_info_unavailable (who, scm_list_2 (port, flags));

  return scm_from_int (_flags.cf_flags);
}

VISIBLE SCM
scm_pyrun_interactiveone (SCM port, SCM flags, SCM file_name)
{
  const char *who = "pyrun-interactiveone";

  port = port_or_current_input (who, port);
  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);

  scm_dynwind_begin (0);

  FILE *f = open_input_port_as_file (who, port);
  scm_dynwind_fclose (f);

  char *filename = port_file_name (port, file_name);
  scm_dynwind_free (filename);

  const int retval =
    PyRun_InteractiveOneFlags (f, ((filename == NULL) ? "???" : filename),
                               &_flags);

  scm_dynwind_end ();

  if (retval == -1)
    raise_pyexc_with_info_unavailable (who, scm_list_2 (port, flags));

  SCM values[2];
  values[0] = scm_from_int (_flags.cf_flags);
  values[1] = scm_from_int (retval);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_pyrun_string (SCM string, SCM start, SCM globals, SCM locals, SCM flags)
{
  const char *who = "pyrun-string";

  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);
  const int _start = scm_to_py_start_symbol (start);

  PyObject *_globals = scm_to_py_globals (globals);
  PyObject *_locals = scm_to_py_locals (locals);

  scm_dynwind_begin (0);

  char *_string = scm_to_locale_string (string);
  scm_dynwind_free (_string);

  PyObject *pyresult =
    PyRun_StringFlags (_string, _start, _globals, _locals, &_flags);
  if (pyresult == NULL)
    scm_c_py_exc_check_violation
      (who, _("PyRun_StringFlags raised a Python exception"),
       scm_list_5 (string, start, globals, locals, flags));

  scm_dynwind_end ();

  SCM values[2];
  values[0] = scm_c_make_pyobject (pyresult);
  values[1] = scm_from_int (_flags.cf_flags);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_pyrun_file (SCM port, SCM start, SCM globals, SCM locals, SCM flags,
                SCM file_name)
{
  const char *who = "pyrun-file";

  if (unbndp_or_false (port))
    port = scm_current_input_port ();

  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);
  const int _start = scm_to_py_start_symbol (start);

  PyObject *_globals = scm_to_py_globals (globals);
  PyObject *_locals = scm_to_py_locals (locals);

  scm_dynwind_begin (0);

  FILE *f = open_input_port_as_file (who, port);
  scm_dynwind_fclose (f);

  char *filename = port_file_name (port, file_name);
  scm_dynwind_free (filename);

  PyObject *pyresult =
    PyRun_FileExFlags (f, ((filename == NULL) ? "???" : filename),
                       _start, _globals, _locals, false, &_flags);
  if (pyresult == NULL)
    scm_c_py_exc_check_violation
      (who, _("PyRun_FileExFlags raised a Python exception"),
       scm_list_5 (port, start, globals, locals, flags));

  scm_dynwind_end ();

  SCM values[2];
  values[0] = scm_c_make_pyobject (pyresult);
  values[1] = scm_from_int (_flags.cf_flags);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_py_compilestring (SCM string, SCM file_name, SCM start, SCM flags,
                      SCM optimize)
{
  const char *who = "py-compilestring";

  PyCompilerFlags _flags = scm_to_PyCompilerFlags (flags);
  const int _start = scm_to_py_start_symbol (start);
  const int _optimize =
    (unbndp_or_false (optimize)) ? -1 : scm_to_int (optimize);

  scm_dynwind_begin (0);

  char *_string = scm_to_locale_string (string);
  scm_dynwind_free (_string);

  char *filename = scm_to_locale_string (file_name);
  scm_dynwind_free (filename);

  PyObject *pycode =
    Py_CompileStringExFlags (_string, filename, _start, &_flags, _optimize);
  if (pycode == NULL)
    scm_c_py_exc_check_violation
      (who, _("Py_CompileStringExFlags or Py_CompileStringFlags "
              "raised a Python exception"),
       scm_list_4 (string, start, flags, optimize));

  scm_dynwind_end ();

  SCM values[2];
  values[0] = scm_c_make_pyobject (pycode);
  values[1] = scm_from_int (_flags.cf_flags);
  return scm_c_values (values, 2);
}

VISIBLE SCM
scm_pyeval_evalcode (SCM pycode, SCM globals, SCM locals, SCM args,
                     SCM keywords, SCM defaults, SCM closure)
{
  // FIXME: This needs testing with args, keywords, defaults, and/or
  // closure.

  const char *who = "pyeval-evalcode";

  scm_assert_pyobject (pycode);
  PyObject *_pycode = scm_to_PyObject_ptr (pycode);
  SCM_ASSERT_TYPE ((PyCode_Check (_pycode)), pycode,
                   SCM_ARG1, who, "Python code object");

  PyObject *_globals = scm_to_py_globals (globals);
  PyObject *_locals = scm_to_py_locals (locals);

  PyObject **_args;
  int arg_count;
  optional_PyObject_array (args, &_args, &arg_count);

  PyObject **_keywords;
  int keyword_count;
  optional_PyObject_array (keywords, &_keywords, &keyword_count);
  if (keyword_count % 2 != 0)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition
        (_("the keywords list should contain an even number of elements")),
        rnrs_make_irritants_condition (scm_list_1 (keywords))));

  PyObject **_defaults;
  int default_count;
  optional_PyObject_array (defaults, &_defaults, &default_count);

  PyObject *_closure;
  if (unbndp_or_false (closure))
    _closure = NULL;
  else
    {
      scm_assert_pyobject (closure);
      _closure = scm_to_PyObject_ptr (closure);
    }

#if PY_MAJOR_VERSION < 3
  PyObject *pyresult =
    PyEval_EvalCodeEx ((PyCodeObject *) _pycode, _globals, _locals,
                       _args, arg_count, _keywords, keyword_count / 2,
                       _defaults, default_count, _closure);
#else
  PyObject *pyresult = PyEval_EvalCodeEx (_pycode, _globals, _locals,
                                          _args, arg_count, _keywords,
                                          keyword_count / 2,
                                          _defaults, default_count, NULL,
                                          _closure);
#endif
  if (pyresult == NULL)
    scm_c_py_exc_check_violation
      (who, _("PyEval_EvalCodeEx raised a Python exception"),
       scm_list_n (pycode, globals, locals, args,
                   keywords, defaults, closure, SCM_UNDEFINED));

  return scm_c_make_pyobject (pyresult);
}

STM_SCM_ONE_TIME_INITIALIZE (VISIBLE STM_ATTRIBUTE_PURE, SCM,
                             scm_Py_eval_input,
                             (scm_Py_eval_input__Value =
                              scm_from_latin1_symbol ("Py_eval_input")));

STM_SCM_ONE_TIME_INITIALIZE (VISIBLE STM_ATTRIBUTE_PURE, SCM,
                             scm_Py_file_input,
                             (scm_Py_file_input__Value =
                              scm_from_latin1_symbol ("Py_file_input")));

STM_SCM_ONE_TIME_INITIALIZE (VISIBLE STM_ATTRIBUTE_PURE, SCM,
                             scm_Py_single_input,
                             (scm_Py_single_input__Value =
                              scm_from_latin1_symbol ("Py_single_input")));

VISIBLE int
scm_to_py_start_symbol (SCM start_symbol)
{
  int start;
  if (scm_is_eq (start_symbol, scm_Py_eval_input ()))
    start = Py_eval_input;
  else if (scm_is_eq (start_symbol, scm_Py_file_input ()))
    start = Py_file_input;
  else if (scm_is_eq (start_symbol, scm_Py_single_input ()))
    start = Py_single_input;
  else
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("scm_to_py_start_symbol"),
        rnrs_c_make_message_condition
        (_("expected 'Py_eval_input, 'Py_file_input, or 'Py_single_input")),
        rnrs_make_irritants_condition (scm_list_1 (start_symbol))));
  return start;
}

VISIBLE SCM
scm_from_py_start_symbol (int start)
{
  SCM start_symbol;
  switch (start)
    {
    case Py_eval_input:
      break;
    case Py_file_input:
      break;
    case Py_single_input:
      break;
    default:
      rnrs_raise_condition
        (scm_list_4
         (rnrs_make_assertion_violation (),
          rnrs_c_make_who_condition ("scm_from_py_start_symbol"),
          rnrs_c_make_message_condition (_("invalid input")),
          rnrs_make_irritants_condition (scm_list_1 (scm_from_int (start)))));
      break;
    }
  return start_symbol;
}

//-------------------------------------------------------------------------
