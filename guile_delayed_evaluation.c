#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/core.h>
#include <sortsmill/guile/core.h>

#define _DLYEVAL_MODULE "sortsmill core delayed-evaluation"

STM_SCM_DEFINE_COMPILED_SCHEME (static STM_ATTRIBUTE_PURE, _scm_delayed_vtable,
                                "(@@ (" _DLYEVAL_MODULE ") <delayed-vtable>)");

STM_SCM_DEFINE_COMPILED_SCHEME (static STM_ATTRIBUTE_PURE, _scm_null_proc,
                                "(@@ (" _DLYEVAL_MODULE ") null-proc)");

STM_SCM_DEFINE_COMPILED_SCHEME (static STM_ATTRIBUTE_PURE,
                                _scm_proc_make_forcer,
                                "(lambda (obj)"
                                "  (let ([forced^"
                                "         (@@ (" _DLYEVAL_MODULE ") forced)])"
                                "    (lambda () (forced^ obj))))");

static SCM
_scm_make_forcer (SCM obj)
{
  return scm_call_1 (_scm_proc_make_forcer (), obj);
}

static bool
_scm_is_applicable (SCM obj)
{
  return !scm_is_eq (scm_struct_ref (obj, scm_from_int (0)), _scm_null_proc ());
}

VISIBLE bool
scm_is_delayed (SCM obj)
{
  // A Scheme implementation:
  //
  //   (define (delayed? obj)
  //     (and (struct? obj)
  //          (eq? (struct-vtable obj) <delayed-vtable>)))

  return (scm_is_true (scm_struct_p (obj))
          && scm_is_eq (scm_struct_vtable (obj), _scm_delayed_vtable ()));
}

VISIBLE SCM
scm_delayed_p (SCM obj)
{
  return scm_from_bool (scm_is_delayed (obj));
}

VISIBLE bool
scm_is_applicable_delayed (SCM obj)
{
  // A Scheme implementation:
  //
  //   (define (applicable-delayed? obj)
  //     (and (struct? obj)
  //          (eq? (struct-vtable obj) <delayed-vtable>)
  //          (not (eq? (struct-ref obj 0) null-proc))))

  return scm_is_delayed (obj) && _scm_is_applicable (obj);
}

VISIBLE SCM
scm_applicable_delayed_p (SCM obj)
{
  return scm_from_bool (scm_is_applicable_delayed (obj));
}

VISIBLE bool
scm_is_nonapplicable_delayed (SCM obj)
{
  // A Scheme implementation:
  //
  //   (define (nonapplicable-delayed? obj)
  //     (and (struct? obj)
  //          (eq? (struct-vtable obj) <delayed-vtable>)
  //          (eq? (struct-ref obj 0) null-proc)))

  return scm_is_delayed (obj) && !_scm_is_applicable (obj);
}

VISIBLE SCM
scm_nonapplicable_delayed_p (SCM obj)
{
  return scm_from_bool (scm_is_nonapplicable_delayed (obj));
}

VISIBLE SCM
scm_delayed_value (SCM thunk)
{
  // A Scheme implementation:
  //
  //   (define (delayed-value thunk)
  //     (make-struct <delayed-vtable> 0 null-proc
  //                  (make-AO 0) (make-mutex) thunk))

  SCM zero = scm_from_int (0);
  return scm_make_struct (_scm_delayed_vtable (), zero,
                          scm_list_4 (_scm_null_proc (), scm_make_AO (zero),
                                      scm_make_mutex (), thunk));
}

VISIBLE SCM
scm_applicable_delayed_value (SCM thunk)
{
  // A Scheme implementation:
  //
  //   (define (applicable-delayed-value thunk)
  //     (let ([obj (make-struct <delayed-vtable> 0
  //                             *unspecified* (make-AO 0) (make-mutex) thunk)])
  //       (struct-set! obj 0 (lambda () (forced obj)))
  //       obj))

  SCM zero = scm_from_int (0);
  SCM obj = scm_make_struct (_scm_delayed_vtable (), zero,
                             scm_list_4 (SCM_UNSPECIFIED, scm_make_AO (zero),
                                         scm_make_mutex (), thunk));
  scm_struct_set_x (obj, zero, _scm_make_forcer (obj));
  return obj;
}

VISIBLE SCM
scm_undelayed (SCM value)
{
  // A Scheme implementation:
  //
  //   (define (undelayed value)
  //     (make-struct <delayed-vtable> 0 null-proc
  //                  (make-AO 1) *unspecified* value))

  SCM zero = scm_from_int (0);
  SCM one = scm_from_int (1);
  return scm_make_struct (_scm_delayed_vtable (), zero,
                          scm_list_4 (_scm_null_proc (), scm_make_AO (one),
                                      SCM_UNSPECIFIED, value));
}

VISIBLE SCM
scm_applicable_undelayed (SCM value)
{
  // A Scheme implementation:
  //
  //   (define (applicable-delayed value)
  //     (let ([obj (make-struct <delayed-vtable> 0 *unspecified*
  //                             (make-AO 1) *unspecified* value)])
  //       (struct-set! obj 0 (lambda () (forced obj)))
  //       obj))

  SCM zero = scm_from_int (0);
  SCM one = scm_from_int (1);
  SCM obj = scm_make_struct (_scm_delayed_vtable (), zero,
                             scm_list_4 (SCM_UNSPECIFIED, scm_make_AO (one),
                                         SCM_UNSPECIFIED, value));
  scm_struct_set_x (obj, zero, _scm_make_forcer (obj));
  return obj;
}

VISIBLE SCM
scm_make_delayed (SCM value)
{
  return (scm_is_delayed (value)) ? value : (scm_undelayed (value));
}

VISIBLE SCM
scm_make_applicable_delayed (SCM value)
{
  return (scm_is_applicable_delayed (value)) ?
    value : (scm_applicable_undelayed (value));
}

VISIBLE SCM
scm_forced (SCM obj)
{
  // A Scheme implementation:
  //
  //   (define (forced obj)
  //     (let ([initialized (struct-ref obj 1)])
  //       (when (zero? (AO-load-acquire-read initialized))
  //         (let ([mutex (struct-ref obj 2)])
  //           (lock-mutex mutex)
  //           (when (zero? (AO-load initialized))
  //             (struct-set! obj 3 ((struct-ref obj 3)))
  //             (AO-store-release-write! initialized 1))
  //           (unlock-mutex mutex)))
  //       (struct-ref obj 3)))

  SCM value;

  SCM _1 = scm_from_int (1);
  SCM _3 = scm_from_int (3);

  SCM initialized = scm_struct_ref (obj, _1);
  if (scm_to_int (scm_AO_load_acquire_read (initialized)) == 0)
    {
      SCM _2 = scm_from_int (2);
      SCM mutex = scm_struct_ref (obj, _2);
      scm_lock_mutex (mutex);
      if (scm_to_int (scm_AO_load (initialized)) == 0)
        {
          value = scm_call_0 (scm_struct_ref (obj, _3));
          scm_struct_set_x (obj, _3, value);
          scm_AO_store_release_write_x (initialized, _1);
        }
      else
        value = scm_struct_ref (obj, _3);
      scm_unlock_mutex (mutex);
    }
  else
    value = scm_struct_ref (obj, _3);

  return value;
}

VISIBLE bool
scm_is_forced (SCM obj)
{
  // A Scheme implementation:
  //
  //   (define (forced? obj)
  //     (not (zero? (AO-load-acquire-read (struct-ref obj 1)))))

  SCM initialized = scm_struct_ref (obj, scm_from_int (1));
  return (scm_to_int (scm_AO_load_acquire_read (initialized)) != 0);
}

VISIBLE SCM
scm_forced_p (SCM obj)
{
  return scm_from_bool (scm_is_forced (obj));
}
