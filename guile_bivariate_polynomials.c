#include <config.h>

// Copyright (C) 2015 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <libintl.h>
#include <sortsmill/guile/core.h>

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

STM_SCM_ONE_TIME_INITIALIZE (static STM_ATTRIBUTE_PURE, SCM, _symbol_f64,
                             (_symbol_f64__Value =
                              scm_from_latin1_symbol ("f64")));

//----------------------------------------------------------------------
//
// FIXME: This is reusable.

typedef struct
{
  size_t degree;
  void *array;
} mpq_bipoly_clear_handler_data_t_;

static void
mpq_bipoly_clear_handler (void *p)
{
  mpq_bipoly_clear_handler_data_t_ *data = p;
  mpq_bipoly_clear (data->degree, (mpq_t (*)[data->degree + 1]) data->array);
  free (data);
}

static void
scm_dynwind_mpq_bipoly_clear (size_t degree, mpq_t (*a)[degree + 1])
{
  mpq_bipoly_clear_handler_data_t_ *data =
    scm_malloc (sizeof (mpq_bipoly_clear_handler_data_t_));
  data->degree = degree;
  data->array = a;
  scm_dynwind_unwind_handler (mpq_bipoly_clear_handler, data,
                              SCM_F_WIND_EXPLICITLY);
}

//----------------------------------------------------------------------

static inline size_t
szmax (size_t a, size_t b)
{
  return (a < b) ? b : a;
}

// FIXME: This is reusable.
static size_t
bipoly_degree (const char *who, SCM bipoly, unsigned int arg_pos,
               scm_t_array_handle *handlep)
{
  const scm_t_array_dim *dims = scm_array_handle_dims (handlep);
  SCM_ASSERT_TYPE ((scm_array_handle_rank (handlep) == 2
                    && (dims[0].ubnd - dims[0].lbnd
                        == dims[1].ubnd - dims[1].lbnd)),
                   bipoly, arg_pos, who, _("square rank-2 array"));
  return (size_t) (dims[0].ubnd - dims[0].lbnd);
}

//----------------------------------------------------------------------

typedef void mpq_bipoly_add_or_sub_t_ (size_t degree_a,
                                       mpq_t a[degree_a + 1][degree_a + 1],
                                       size_t degree_b,
                                       mpq_t b[degree_b + 1][degree_b + 1],
                                       mpq_t result
                                       [(degree_a < degree_b) ?
                                        (degree_b + 1) : (degree_a + 1)]
                                       [(degree_a < degree_b) ?
                                        (degree_b + 1) : (degree_a + 1)]);

static SCM
scm_exact_bipoly_add_or_sub (const char *who,
                             mpq_bipoly_add_or_sub_t_ *operation, SCM a, SCM b)
{
  const SCM *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  mpq_t (*a_)[degree_a + 1] =
    scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (a_);
  mpq_bipoly_init (degree_a, a_);
  scm_dynwind_mpq_bipoly_clear (degree_a, a_);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], a_[i][j]);

  scm_t_array_handle handle_b;
  scm_array_get_handle (b, &handle_b);
  scm_dynwind_array_handle_release (&handle_b);

  const size_t degree_b = bipoly_degree (who, b, SCM_ARG2, &handle_b);
  elems = scm_array_handle_elements (&handle_b);
  stridex = scm_array_handle_dims (&handle_b)[0].inc;
  stridey = scm_array_handle_dims (&handle_b)[1].inc;
  mpq_t (*b_)[degree_b + 1] =
    scm_malloc ((degree_b + 1) * (degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (b_);
  mpq_bipoly_init (degree_b, b_);
  scm_dynwind_mpq_bipoly_clear (degree_b, b_);
  for (ssize_t i = 0; i <= degree_b; i++)
    for (ssize_t j = 0; j <= degree_b - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], b_[i][j]);

  const size_t degree_max = szmax (degree_a, degree_b);
  mpq_t (*sum_)[degree_max + 1] =
    scm_malloc ((degree_max + 1) * (degree_max + 1) * sizeof (mpq_t));
  scm_dynwind_free (sum_);
  mpq_bipoly_init (degree_max, sum_);
  scm_dynwind_mpq_bipoly_clear (degree_max, sum_);

  operation (degree_a, a_, degree_b, b_, sum_);

  SCM size = scm_from_size_t (degree_max + 1);
  SCM sum = scm_make_array (scm_from_int (0), scm_list_2 (size, size));
  scm_t_array_handle handle_sum;
  scm_array_get_handle (sum, &handle_sum);
  scm_dynwind_array_handle_release (&handle_sum);

  SCM *elems2 = scm_array_handle_writable_elements (&handle_sum);
  for (ssize_t i = 0; i <= degree_max; i++)
    for (ssize_t j = 0; j <= degree_max - i; j++)
      elems2[i * (degree_max + 1) + j] = scm_from_mpq (sum_[i][j]);

  scm_dynwind_end ();

  return sum;
}

VISIBLE SCM
scm_exact_bipoly_add (SCM a, SCM b)
{
  return scm_exact_bipoly_add_or_sub ("exact-bipoly-add", mpq_bipoly_add, a, b);
}

VISIBLE SCM
scm_exact_bipoly_sub (SCM a, SCM b)
{
  return scm_exact_bipoly_add_or_sub ("exact-bipoly-sub", mpq_bipoly_sub, a, b);
}

VISIBLE SCM
scm_exact_bipoly_mul (SCM a, SCM b)
{
  const char *who = "exact-bipoly-mul";

  const SCM *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  mpq_t (*a_)[degree_a + 1] =
    scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (a_);
  mpq_bipoly_init (degree_a, a_);
  scm_dynwind_mpq_bipoly_clear (degree_a, a_);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], a_[i][j]);

  scm_t_array_handle handle_b;
  scm_array_get_handle (b, &handle_b);
  scm_dynwind_array_handle_release (&handle_b);

  const size_t degree_b = bipoly_degree (who, b, SCM_ARG2, &handle_b);
  elems = scm_array_handle_elements (&handle_b);
  stridex = scm_array_handle_dims (&handle_b)[0].inc;
  stridey = scm_array_handle_dims (&handle_b)[1].inc;
  mpq_t (*b_)[degree_b + 1] =
    scm_malloc ((degree_b + 1) * (degree_b + 1) * sizeof (mpq_t));
  scm_dynwind_free (b_);
  mpq_bipoly_init (degree_b, b_);
  scm_dynwind_mpq_bipoly_clear (degree_b, b_);
  for (ssize_t i = 0; i <= degree_b; i++)
    for (ssize_t j = 0; j <= degree_b - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], b_[i][j]);

  const size_t degree_prod = degree_a + degree_b;
  mpq_t (*prod_)[degree_prod + 1] =
    scm_malloc ((degree_prod + 1) * (degree_prod + 1) * sizeof (mpq_t));
  scm_dynwind_free (prod_);
  mpq_bipoly_init (degree_prod, prod_);
  scm_dynwind_mpq_bipoly_clear (degree_prod, prod_);

  mpq_bipoly_mul (degree_a, a_, degree_b, b_, prod_);

  SCM size = scm_from_size_t (degree_prod + 1);
  SCM prod = scm_make_array (scm_from_int (0), scm_list_2 (size, size));
  scm_t_array_handle handle_prod;
  scm_array_get_handle (prod, &handle_prod);
  scm_dynwind_array_handle_release (&handle_prod);

  SCM *elems2 = scm_array_handle_writable_elements (&handle_prod);
  for (ssize_t i = 0; i <= degree_prod; i++)
    for (ssize_t j = 0; j <= degree_prod - i; j++)
      elems2[i * (degree_prod + 1) + j] = scm_from_mpq (prod_[i][j]);

  scm_dynwind_end ();

  return prod;
}

VISIBLE SCM
scm_exact_bipoly_scalarmul (SCM a, SCM r)
{
  const char *who = "exact-bipoly-scalarmul";

  const SCM *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  mpq_t (*a_)[degree_a + 1] =
    scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (a_);
  mpq_bipoly_init (degree_a, a_);
  scm_dynwind_mpq_bipoly_clear (degree_a, a_);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], a_[i][j]);

  mpq_t r_;
  mpq_init (r_);
  scm_dynwind_mpq_clear (r_);
  scm_to_mpq (r, r_);

  mpq_bipoly_scalarmul (degree_a, a_, r_, a_);

  SCM size = scm_from_size_t (degree_a + 1);
  SCM prod = scm_make_array (scm_from_int (0), scm_list_2 (size, size));
  scm_t_array_handle handle_prod;
  scm_array_get_handle (prod, &handle_prod);
  scm_dynwind_array_handle_release (&handle_prod);

  SCM *elems2 = scm_array_handle_writable_elements (&handle_prod);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      elems2[i * (degree_a + 1) + j] = scm_from_mpq (a_[i][j]);

  scm_dynwind_end ();

  return prod;
}

static SCM
_scm_exact_bipoly_partial_deriv (const char *who,
                                 void (*partial_deriv)
                                 (size_t degree_a,
                                  mpq_t a[degree_a + 1][degree_a + 1],
                                  mpq_t deriv
                                  [(degree_a == 0) ? 1 : degree_a]
                                  [(degree_a == 0) ? 1 : degree_a]), SCM a)
{
  const SCM *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  mpq_t (*a_)[degree_a + 1] =
    scm_malloc ((degree_a + 1) * (degree_a + 1) * sizeof (mpq_t));
  scm_dynwind_free (a_);
  mpq_bipoly_init (degree_a, a_);
  scm_dynwind_mpq_bipoly_clear (degree_a, a_);
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      scm_to_mpq (elems[i * stridex + j * stridey], a_[i][j]);

  const size_t degree_deriv = (degree_a == 0) ? 0 : (degree_a - 1);

  mpq_t (*deriv_)[degree_deriv + 1] =
    scm_malloc ((degree_deriv + 1) * (degree_deriv + 1) * sizeof (mpq_t));
  scm_dynwind_free (deriv_);
  mpq_bipoly_init (degree_deriv, deriv_);
  scm_dynwind_mpq_bipoly_clear (degree_deriv, deriv_);

  partial_deriv (degree_a, a_, deriv_);

  SCM size = scm_from_size_t (degree_deriv + 1);
  SCM deriv = scm_make_array (scm_from_int (0), scm_list_2 (size, size));
  scm_t_array_handle handle_deriv;
  scm_array_get_handle (deriv, &handle_deriv);
  scm_dynwind_array_handle_release (&handle_deriv);

  SCM *elems2 = scm_array_handle_writable_elements (&handle_deriv);
  for (ssize_t i = 0; i <= degree_deriv; i++)
    for (ssize_t j = 0; j <= degree_deriv - i; j++)
      elems2[i * (degree_deriv + 1) + j] = scm_from_mpq (deriv_[i][j]);

  scm_dynwind_end ();

  return deriv;
}

VISIBLE SCM
scm_exact_bipoly_partial_deriv_wrt_x (SCM a)
{
  return _scm_exact_bipoly_partial_deriv ("exact-bipoly-partial-deriv-wrt-x",
                                          mpq_bipoly_partial_deriv_wrt_x, a);
}

VISIBLE SCM
scm_exact_bipoly_partial_deriv_wrt_y (SCM a)
{
  return _scm_exact_bipoly_partial_deriv ("exact-bipoly-partial-deriv-wrt-y",
                                          mpq_bipoly_partial_deriv_wrt_y, a);
}

//----------------------------------------------------------------------

typedef void dbipoly_add_or_sub_t_ (size_t degree_a,
                                    double a[degree_a + 1][degree_a + 1],
                                    size_t degree_b,
                                    double b[degree_b + 1][degree_b + 1],
                                    double result
                                    [(degree_a < degree_b) ?
                                     (degree_b + 1) : (degree_a + 1)]
                                    [(degree_a < degree_b) ?
                                     (degree_b + 1) : (degree_a + 1)]);

static SCM
scm_dbipoly_add_or_sub (const char *who, dbipoly_add_or_sub_t_ *operation,
                        SCM a, SCM b)
{
  const double *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_f64_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  double a_[degree_a + 1][degree_a + 1];
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      a_[i][j] = elems[i * stridex + j * stridey];

  scm_t_array_handle handle_b;
  scm_array_get_handle (b, &handle_b);
  scm_dynwind_array_handle_release (&handle_b);

  const size_t degree_b = bipoly_degree (who, b, SCM_ARG2, &handle_b);
  elems = scm_array_handle_f64_elements (&handle_b);
  stridex = scm_array_handle_dims (&handle_b)[0].inc;
  stridey = scm_array_handle_dims (&handle_b)[1].inc;
  double b_[degree_b + 1][degree_b + 1];
  for (ssize_t i = 0; i <= degree_b; i++)
    for (ssize_t j = 0; j <= degree_b - i; j++)
      b_[i][j] = elems[i * stridex + j * stridey];

  const size_t degree_max = szmax (degree_a, degree_b);

  SCM size = scm_from_size_t (degree_max + 1);
  SCM sum = scm_make_typed_array (_symbol_f64 (), SCM_UNSPECIFIED,
                                  scm_list_2 (size, size));
  scm_t_array_handle handle_sum;
  scm_array_get_handle (sum, &handle_sum);
  scm_dynwind_array_handle_release (&handle_sum);

  double *elems2 = scm_array_handle_f64_writable_elements (&handle_sum);
  operation (degree_a, a_, degree_b, b_, (double (*)[degree_max + 1]) elems2);

  scm_dynwind_end ();

  return sum;
}

VISIBLE SCM
scm_dbipoly_add (SCM a, SCM b)
{
  return scm_dbipoly_add_or_sub ("dbipoly-add", dbipoly_add, a, b);
}

VISIBLE SCM
scm_dbipoly_sub (SCM a, SCM b)
{
  return scm_dbipoly_add_or_sub ("dbipoly-sub", dbipoly_sub, a, b);
}

VISIBLE SCM
scm_dbipoly_mul (SCM a, SCM b)
{
  const char *who = "dbipoly-mul";

  const double *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_f64_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  double a_[degree_a + 1][degree_a + 1];
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      a_[i][j] = elems[i * stridex + j * stridey];

  scm_t_array_handle handle_b;
  scm_array_get_handle (b, &handle_b);
  scm_dynwind_array_handle_release (&handle_b);

  const size_t degree_b = bipoly_degree (who, b, SCM_ARG2, &handle_b);
  elems = scm_array_handle_f64_elements (&handle_b);
  stridex = scm_array_handle_dims (&handle_b)[0].inc;
  stridey = scm_array_handle_dims (&handle_b)[1].inc;
  double b_[degree_b + 1][degree_b + 1];
  for (ssize_t i = 0; i <= degree_b; i++)
    for (ssize_t j = 0; j <= degree_b - i; j++)
      b_[i][j] = elems[i * stridex + j * stridey];

  const size_t degree_prod = degree_a + degree_b;

  SCM size = scm_from_size_t (degree_prod + 1);
  SCM prod = scm_make_typed_array (_symbol_f64 (), SCM_UNSPECIFIED,
                                   scm_list_2 (size, size));
  scm_t_array_handle handle_prod;
  scm_array_get_handle (prod, &handle_prod);
  scm_dynwind_array_handle_release (&handle_prod);

  double *elems2 = scm_array_handle_f64_writable_elements (&handle_prod);
  dbipoly_mul (degree_a, a_, degree_b, b_,
               (double (*)[degree_prod + 1]) elems2);

  scm_dynwind_end ();

  return prod;
}

VISIBLE SCM
scm_dbipoly_scalarmul (SCM a, SCM r)
{
  const char *who = "dbipoly-scalarmul";

  const double *elems;
  ssize_t stridex;
  ssize_t stridey;

  scm_dynwind_begin (0);

  scm_t_array_handle handle_a;
  scm_array_get_handle (a, &handle_a);
  scm_dynwind_array_handle_release (&handle_a);

  const size_t degree_a = bipoly_degree (who, a, SCM_ARG1, &handle_a);
  elems = scm_array_handle_f64_elements (&handle_a);
  stridex = scm_array_handle_dims (&handle_a)[0].inc;
  stridey = scm_array_handle_dims (&handle_a)[1].inc;
  double a_[degree_a + 1][degree_a + 1];
  for (ssize_t i = 0; i <= degree_a; i++)
    for (ssize_t j = 0; j <= degree_a - i; j++)
      a_[i][j] = elems[i * stridex + j * stridey];

  const double r_ = scm_to_double (r);

  SCM size = scm_from_size_t (degree_a + 1);
  SCM prod = scm_make_typed_array (_symbol_f64 (), SCM_UNSPECIFIED,
                                   scm_list_2 (size, size));
  scm_t_array_handle handle_prod;
  scm_array_get_handle (prod, &handle_prod);
  scm_dynwind_array_handle_release (&handle_prod);

  double *elems2 = scm_array_handle_f64_writable_elements (&handle_prod);
  dbipoly_scalarmul (degree_a, a_, r_, (double (*)[degree_a + 1]) elems2);

  scm_dynwind_end ();

  return prod;
}

//----------------------------------------------------------------------
