#include <config.h>

// Copyright (C) 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile.
// 
// Sorts Mill Core Guile is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.

#include <sortsmill/guile/core/modules.h>

static SCM
make_module_name (const char *s, SCM scm_from_symbol (const char *name))
{
  // In UTF-8, parts of a multibyte character all start with a ‘1’
  // bit. Thus we can search for NUL and space characters
  // byte-by-byte.

  SCM module_name = SCM_EOL;

  size_t i = 0;
  while (s[i] == ' ')
    i++;

  while (s[i] != '\0')
    {
      size_t j = i + 1;
      while (s[j] != '\0' && s[j] != ' ')
        j++;

      // Store the substring on the stack. We assume module names are
      // not enormous strings.
      char buf[j - i + 1];
      memcpy (buf, &s[i], j - i);
      buf[j - i] = '\0';
      module_name = scm_cons (scm_from_symbol (buf), module_name);

      while (s[j] == ' ')
        j++;
      i = j;
    }

  return scm_reverse_x (module_name, SCM_EOL);
}

VISIBLE SCM
scm_from_locale_module_name (const char *s)
{
  return make_module_name (s, scm_from_locale_symbol);
}

VISIBLE SCM
scm_from_latin1_module_name (const char *s)
{
  return make_module_name (s, scm_from_latin1_symbol);
}

VISIBLE SCM
scm_from_utf8_module_name (const char *s)
{
  return make_module_name (s, scm_from_utf8_symbol);
}
