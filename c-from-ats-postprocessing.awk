BEGIN {
    print "#include <config.h>"
    print "#include <ats-top-extras.h>"
}

# Remove timestamps.
!/^** The starting compilation time is:/ {print}

