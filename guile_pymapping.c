#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <abstract.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------
//
// Referenceable wrappers for functions that in some versions of
// Python are macros.

static inline PyObject *
PYMAPPING_KEYS (PyObject *o)
{
  return PyMapping_Keys (o);
}

static inline PyObject *
PYMAPPING_VALUES (PyObject *o)
{
  return PyMapping_Values (o);
}

static inline PyObject *
PYMAPPING_ITEMS (PyObject *o)
{
  return PyMapping_Items (o);
}

//-------------------------------------------------------------------------

#if PY_VERSION_HEX < 0x03040000
#define _CONST_CHAR_PTR_WORKAROUND(x) ((char *) (x))
#else
#define _CONST_CHAR_PTR_WORKAROUND(x) (x)
#endif

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pymapping, PyMapping);
SCM_PYOBJECT_UNARY_WRAPCHECKS (pydict, PyDict);

VISIBLE SCM_PYOBJECT_UNARY_INTTYPE (ssize_t, "pymapping-length",
                                    scm_c_pymapping_length, PyMapping_Length);

VISIBLE SCM
scm_pymapping_length (SCM obj)
{
  return scm_from_ssize_t (scm_c_pymapping_length (obj));
}

VISIBLE void
scm_c_pymapping_delitemstring (SCM obj, const char *key)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  const int errval =
    PyMapping_DelItemString (_obj, _CONST_CHAR_PTR_WORKAROUND (key));
  py_exc_check ((errval != -1), "pyobject-delitemstring!",
                scm_list_2 (obj, scm_from_locale_string (key)));
}

VISIBLE SCM
scm_pymapping_delitemstring_x (SCM obj, SCM key)
{
  scm_dynwind_begin (0);

  char *k = scm_to_locale_stringn (key, NULL);
  scm_dynwind_free (k);

  scm_c_pymapping_delitemstring (obj, k);

  scm_dynwind_end ();

  return SCM_UNSPECIFIED;
}

VISIBLE bool
scm_c_pymapping_haskeystring (SCM obj, const char *key)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  const int result =
    PyMapping_HasKeyString (_obj, _CONST_CHAR_PTR_WORKAROUND (key));
  py_exc_check ((result != -1), "pyobject-haskeystring",
                scm_list_2 (obj, scm_from_locale_string (key)));
  return result;
}

VISIBLE SCM
scm_pymapping_haskeystring_p (SCM obj, SCM key)
{
  scm_dynwind_begin (0);

  char *k = scm_to_locale_stringn (key, NULL);
  scm_dynwind_free (k);

  const bool result = scm_c_pymapping_haskeystring (obj, k);

  scm_dynwind_end ();

  return (result) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM_PYOBJECT_BINARY_BOOL ("pymapping-haskey?",
                                  scm_c_pymapping_haskey, PyMapping_HasKey);

VISIBLE SCM
scm_pymapping_haskey_p (SCM obj, SCM key)
{
  return (scm_c_pymapping_haskey (obj, key)) ? SCM_BOOL_T : SCM_BOOL_F;
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pymapping-keys",
                               scm_pymapping_keys, PYMAPPING_KEYS);

VISIBLE SCM_PYOBJECT_UNARY_OP ("pymapping-values",
                               scm_pymapping_values, PYMAPPING_VALUES);

VISIBLE SCM_PYOBJECT_UNARY_OP ("pymapping-items",
                               scm_pymapping_items, PYMAPPING_ITEMS);

VISIBLE SCM
scm_c_pymapping_getitemstring (SCM obj, const char *key)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  PyObject *_result =
    PyMapping_GetItemString (_obj, _CONST_CHAR_PTR_WORKAROUND (key));
  py_exc_check ((_result != NULL), "pyobject-getitemstring",
                scm_list_2 (obj, scm_from_locale_string (key)));
  return scm_c_make_pyobject (_result);
}

VISIBLE SCM
scm_pymapping_getitemstring (SCM obj, SCM key)
{
  scm_dynwind_begin (0);

  char *k = scm_to_locale_stringn (key, NULL);
  scm_dynwind_free (k);

  SCM result = scm_c_pymapping_getitemstring (obj, k);

  scm_dynwind_end ();

  return result;
}

VISIBLE void
scm_c_pymapping_setitemstring (SCM obj, const char *key, SCM v)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  scm_assert_pyobject (v);
  PyObject *_v = scm_to_PyObject_ptr (v);
  const int errval =
    PyMapping_SetItemString (_obj, _CONST_CHAR_PTR_WORKAROUND (key), _v);
  py_exc_check ((errval != -1), "pyobject-setitemstring!",
                scm_list_3 (obj, scm_from_locale_string (key), v));
}

VISIBLE SCM
scm_pymapping_setitemstring_x (SCM obj, SCM key, SCM v)
{
  scm_dynwind_begin (0);

  char *k = scm_to_locale_stringn (key, NULL);
  scm_dynwind_free (k);

  scm_c_pymapping_setitemstring (obj, k, v);

  scm_dynwind_end ();

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pymapping_to_alist (SCM obj)
{
  const char *who = "pymapping->alist";

  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyMapping_Check (_obj), obj, SCM_ARG1, who, "pymapping");

  PyObject *contents = PyMapping_Items (_obj);
  py_exc_check ((contents != NULL), who, scm_list_1 (obj));

  PyObject *fast = PySequence_Fast (contents, "");
  py_exc_check ((fast != NULL), who, scm_list_1 (obj));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);
  SCM lst = SCM_EOL;
  for (ssize_t i = n - 1; 0 <= i; i--)
    {
      PyObject *_key = PyTuple_GET_ITEM (items[i], 0);
      SCM key = scm_c_incref_make_pyobject (_key);

      PyObject *_value = PyTuple_GET_ITEM (items[i], 1);
      SCM value = scm_c_incref_make_pyobject (_value);

      lst = scm_acons (key, value, lst);
    }

  scm_dynwind_end ();

  return lst;
}

VISIBLE SCM
scm_alist_to_pydict (SCM lst)
{
  const char *who = "alist->pydict";

  // The following will not work if @var{lst} is cyclic.
  for (SCM p = lst; !scm_is_null (p); p = SCM_CDR (p))
    SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_pair (SCM_CAR (p))
                      && scm_is_pyobject (SCM_CAAR (p))
                      && scm_is_pyobject (SCM_CDAR (p))),
                     lst, SCM_ARG1, who, "association list of pyobject items");

  PyObject *pydict = PyDict_New ();
  if (pydict == NULL)
    scm_c_py_exc_check_violation
      (who, _("PyDict_New raised a Python exception"), SCM_EOL);

  for (SCM p = lst; !scm_is_null (p); p = SCM_CDR (p))
    {
      PyObject *pykey = scm_to_PyObject_ptr (SCM_CAAR (p));
      PyObject *pyvalue = scm_to_PyObject_ptr (SCM_CDAR (p));
      int errval = PyDict_SetItem (pydict, pykey, pyvalue);
      if (errval == -1)
        scm_c_py_exc_check_violation
          (who, _("PyDict_SetItem raised a Python exception"),
           scm_list_1 (lst));
    }

  return scm_c_make_pyobject (pydict);
}

VISIBLE SCM
scm_pydict_clear_x (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyDict_Check (_obj), obj, SCM_ARG1, "pydict-clear!",
                   "pydict");
  PyDict_Clear (_obj);
  return SCM_UNSPECIFIED;
}

VISIBLE SCM_PYOBJECT_UNARY_OP ("pydictproxy-new", scm_pydictproxy_new,
                               PyDictProxy_New);
VISIBLE SCM_PYOBJECT_UNARY_OP ("pydict-copy", scm_pydict_copy, PyDict_Copy);

VISIBLE void
scm_c_pydict_merge (SCM a, SCM b, bool override)
{
  const char *who = "pydict-merge!";

  scm_assert_pyobject (a);
  PyObject *_a = scm_to_PyObject_ptr (a);
  SCM_ASSERT_TYPE (PyDict_Check (_a), a, SCM_ARG1, who, "pydict");

  scm_assert_pyobject (b);
  PyObject *_b = scm_to_PyObject_ptr (b);
  SCM_ASSERT_TYPE (PyDict_Check (_b), b, SCM_ARG1, who, "pydict");

  const int errval = PyDict_Merge (_a, _b, override);
  py_exc_check ((errval != -1), who,
                scm_list_3 (a, b, scm_from_bool (override)));
}

VISIBLE SCM
scm_pydict_merge_x (SCM a, SCM b, SCM override)
{
  scm_c_pydict_merge (a, b, !unbndp_or_false (override));
  return SCM_UNSPECIFIED;
}

VISIBLE void
scm_c_pydict_mergefromseq2 (SCM a, SCM seq2, bool override)
{
  const char *who = "pydict-mergefromseq2!";

  scm_assert_pyobject (a);
  PyObject *_a = scm_to_PyObject_ptr (a);
  SCM_ASSERT_TYPE (PyDict_Check (_a), a, SCM_ARG1, who, "pydict");

  scm_assert_pyobject (seq2);
  PyObject *_seq2 = scm_to_PyObject_ptr (seq2);
  SCM_ASSERT_TYPE (PySequence_Check (_seq2), seq2, SCM_ARG2, who, "pysequence");

  const int errval = PyDict_MergeFromSeq2 (_a, _seq2, override);
  py_exc_check ((errval != -1), who,
                scm_list_3 (a, seq2, scm_from_bool (override)));
}

VISIBLE SCM
scm_pydict_mergefromseq2_x (SCM a, SCM seq2, SCM override)
{
  scm_c_pydict_mergefromseq2 (a, seq2, !unbndp_or_false (override));
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyseq2_to_pydict (SCM obj)
{
  // Convert a generalized Python equivalent of an association list to
  // a Python dictionary.
  SCM dict = scm_alist_to_pydict (SCM_EOL);
  scm_pydict_mergefromseq2_x (dict, obj, SCM_BOOL_F);
  return dict;
}

VISIBLE SCM
scm_pyobject_to_pydict (SCM obj)
{
  // Copies a dictionary or converts a ‘pyseq2’.

  SCM dict;
  if (scm_is_pydict (obj))
    dict = scm_pydict_copy (obj);
  else if (scm_is_pysequence (obj))
    dict = scm_pyseq2_to_pydict (obj);
  else
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_assertion_violation (),
        rnrs_c_make_who_condition ("pyobject->pydict"),
        rnrs_c_make_message_condition
        (_("invalid argument to #:->dict in a `py' expression")),
        rnrs_make_irritants_condition (scm_list_1 (obj))));
  return dict;
}

VISIBLE SCM
scm_c_pymapping_to_keyworded_list (PyObject *_obj)
{
  const char *who = "pymapping->keyworded-list";

  if (!PyMapping_Check (_obj))
    SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1, who,
                     "pymapping");

  PyObject *contents = PyMapping_Items (_obj);
  py_exc_check ((contents != NULL), who,
                scm_list_1 (scm_c_make_pyobject (_obj)));

  PyObject *fast = PySequence_Fast (contents, "");
  py_exc_check ((fast != NULL), who, scm_list_1 (scm_c_make_pyobject (_obj)));

  scm_dynwind_begin (0);
  scm_dynwind_decref (fast);

  ssize_t n = PySequence_Fast_GET_SIZE (fast);
  PyObject **items = PySequence_Fast_ITEMS (fast);
  SCM lst = SCM_EOL;
  for (ssize_t i = n - 1; 0 <= i; i--)
    {
      PyObject *_key = PyTuple_GET_ITEM (items[i], 0);
      PyObject *_bytes;
      if (PyUnicode_Check (_key))
        {
          _bytes = PyUnicode_AsEncodedString (_key, "UTF-8", "strict");
          if (_bytes == NULL)
            scm_c_py_exc_check_violation
              (who, _("failed to encode Python Unicode string"), SCM_EOL);
          scm_dynwind_decref (_bytes);
        }
      else if (PyBytes_Check (_key))
        _bytes = _key;
      else
        SCM_ASSERT_TYPE (false, scm_c_make_pyobject (_obj), SCM_ARG1,
                         who, "string-keyed pymapping");
      SCM s = scm_from_utf8_stringn (PyBytes_AS_STRING (_bytes),
                                     PyBytes_GET_SIZE (_bytes));
      SCM key = scm_symbol_to_keyword (scm_string_to_symbol (s));

      PyObject *_value = PyTuple_GET_ITEM (items[i], 1);
      SCM value = scm_c_incref_make_pyobject (_value);

      lst = scm_cons (key, scm_cons (value, lst));
    }

  scm_dynwind_end ();

  return lst;
}

VISIBLE SCM
scm_pymapping_to_keyworded_list (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  return scm_c_pymapping_to_keyworded_list (_obj);
}

VISIBLE SCM
scm_keyworded_list_to_pydict (SCM lst)
{
  const char *who = "keyworded-list->pydict";

  // The following will not work if @var{lst} is cyclic.
  for (SCM p = lst; !scm_is_null (p); p = SCM_CDDR (p))
    SCM_ASSERT_TYPE ((scm_is_pair (p) && scm_is_keyword (SCM_CAR (p))
                      && scm_is_pair (SCM_CDR (p))
                      && scm_is_pyobject (SCM_CADR (p))),
                     lst, SCM_ARG1, who, "keyworded list of pyobject items");

  PyObject *pydict = PyDict_New ();
  if (pydict == NULL)
    scm_c_py_exc_check_violation
      (who, _("PyDict_New raised a Python exception"), SCM_EOL);

  for (SCM p = lst; !scm_is_null (p); p = SCM_CDDR (p))
    {
      SCM key =
        scm_string_to_pyunicode
        (scm_symbol_to_string (scm_keyword_to_symbol (SCM_CAR (p))),
         SCM_UNDEFINED);
      PyObject *pykey = scm_to_PyObject_ptr (key);
      PyObject *pyvalue = scm_to_PyObject_ptr (SCM_CADR (p));
      int errval = PyDict_SetItem (pydict, pykey, pyvalue);
      if (errval == -1)
        scm_c_py_exc_check_violation
          (who, _("PyDict_SetItem raised a Python exception"),
           scm_list_1 (lst));
    }

  return scm_c_make_pyobject (pydict);
}

VISIBLE SCM
scm_keyworded_list_to_py_arguments (SCM lst)
{
  const char *who = "keyworded-list->py-arguments";

  // The following will not work if @var{lst} is cyclic.
  SCM reversed_args = SCM_EOL;
  SCM keyworded_lst = SCM_EOL;
  for (SCM p = lst;
       scm_is_null (keyworded_lst) && !scm_is_null (p); p = SCM_CDR (p))
    {
      SCM_ASSERT_TYPE (scm_is_pair (p), lst, SCM_ARG1, who, "argument list");
      if (scm_is_keyword (SCM_CAR (p)))
        keyworded_lst = p;
      else
        {
          SCM_ASSERT_TYPE (scm_is_pyobject (SCM_CAR (p)), lst, SCM_ARG1, who,
                           "argument list");
          reversed_args = scm_cons (SCM_CAR (p), reversed_args);
        }
    }

  SCM values[2];
  if (scm_is_null (keyworded_lst))
    {
      values[0] = scm_list_reverse_to_pytuple (reversed_args);
      values[1] = SCM_BOOL_F;
    }
  else
    {
      values[0] = scm_list_reverse_to_pytuple (reversed_args);
      values[1] = scm_keyworded_list_to_pydict (keyworded_lst);
    }
  return scm_c_values (values, 2);
}

//-------------------------------------------------------------------------
