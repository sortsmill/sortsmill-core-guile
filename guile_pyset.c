#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>
#include <abstract.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

//-------------------------------------------------------------------------

static inline bool
unbndp_or_false (SCM x)
{
  return (SCM_UNBNDP (x) || scm_is_false (x));
}

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECK_ONLY (pyset, PySet);
SCM_PYOBJECT_UNARY_WRAPCHECKS (pyfrozenset, PyFrozenSet);
SCM_PYOBJECT_UNARY_WRAPCHECKS (pyanyset, PyAnySet);

static SCM
_scm_c_pyanyset_new (SCM pyiterable, const char *who,
                     PyObject *(*make_new_set) (PyObject *iterable))
{
  PyObject *_pyiterable;
  if (unbndp_or_false (pyiterable))
    _pyiterable = NULL;
  else
    {
      scm_assert_pyobject (pyiterable);
      _pyiterable = scm_to_PyObject_ptr (pyiterable);
    }
  PyObject *set = make_new_set (_pyiterable);
  py_exc_check ((set != NULL), who, scm_list_1 (pyiterable));
  return scm_c_make_pyobject (set);
}

VISIBLE SCM
scm_pyset_new (SCM pyiterable)
{
  return _scm_c_pyanyset_new (pyiterable, "pyset-new", PySet_New);
}

VISIBLE SCM
scm_pyfrozenset_new (SCM pyiterable)
{
  return _scm_c_pyanyset_new (pyiterable, "pyfrozenset-new", PyFrozenSet_New);
}

VISIBLE SCM
scm_list_to_pyset (SCM lst)
{
  return scm_pyset_new (scm_list_to_pytuple (lst));
}

VISIBLE SCM
scm_list_to_pyfrozenset (SCM lst)
{
  return scm_pyfrozenset_new (scm_list_to_pytuple (lst));
}

VISIBLE SCM
scm_pyset_size (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyAnySet_Check (_obj), obj, SCM_ARG1, "pyset-size",
                   "pyanyset");
  const ssize_t size = PySet_Size (_obj);
  py_exc_check ((size != -1), "pyset-size", scm_list_1 (obj));
  return scm_from_ssize_t (size);
}

VISIBLE bool
scm_c_pyset_contains (SCM anyset, SCM key)
{
  scm_assert_pyobject (anyset);
  PyObject *_anyset = scm_to_PyObject_ptr (anyset);
  SCM_ASSERT_TYPE (PyAnySet_Check (_anyset), anyset, SCM_ARG1,
                   "pyset-contains?", "pyanyset");

  scm_assert_pyobject (key);
  PyObject *_key = scm_to_PyObject_ptr (key);

  const int truth = PySet_Contains (_anyset, _key);
  py_exc_check ((truth != -1), "pyset-contains?", scm_list_2 (anyset, key));

  return truth;
}

VISIBLE SCM
scm_pyset_contains_p (SCM anyset, SCM key)
{
  return scm_from_bool (scm_c_pyset_contains (anyset, key));
}

VISIBLE SCM
scm_pyset_add_x (SCM anyset, SCM key)
{
  scm_assert_pyobject (anyset);
  PyObject *_anyset = scm_to_PyObject_ptr (anyset);
  SCM_ASSERT_TYPE (PyAnySet_Check (_anyset), anyset, SCM_ARG1, "pyset-add!",
                   "pyanyset");

  scm_assert_pyobject (key);
  PyObject *_key = scm_to_PyObject_ptr (key);

  const int errval = PySet_Add (_anyset, _key);
  py_exc_check ((errval != -1), "pyset-add!", scm_list_2 (anyset, key));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyset_discard_x (SCM set, SCM key)
{
  scm_assert_pyobject (set);
  PyObject *_set = scm_to_PyObject_ptr (set);
  SCM_ASSERT_TYPE (PySet_Check (_set), set, SCM_ARG1, "pyset-discard!",
                   "pyset");

  scm_assert_pyobject (key);
  PyObject *_key = scm_to_PyObject_ptr (key);

  const int errval = PySet_Discard (_set, _key);
  py_exc_check ((errval != -1), "pyset-discard!", scm_list_2 (set, key));

  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pyset_pop_x (SCM set)
{
  scm_assert_pyobject (set);
  PyObject *_set = scm_to_PyObject_ptr (set);
  SCM_ASSERT_TYPE (PySet_Check (_set), set, SCM_ARG1, "pyset-pop!", "pyset");

  PyObject *result = PySet_Pop (_set);
  py_exc_check ((result != NULL), "pyset-pop!", scm_list_1 (set));

  return scm_c_make_pyobject (result);
}

VISIBLE SCM
scm_pyset_clear_x (SCM set)
{
  scm_assert_pyobject (set);
  PyObject *_set = scm_to_PyObject_ptr (set);
  SCM_ASSERT_TYPE (PySet_Check (_set), set, SCM_ARG1, "pyset-clear!", "pyset");

  const int errval = PySet_Clear (_set);
  py_exc_check ((errval != -1), "pyset-clear!", scm_list_1 (set));

  return SCM_UNSPECIFIED;
}

//-------------------------------------------------------------------------
