#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>
#include <libguile/gettext.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"
#include "mpz_pylong.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

// FIXME: The following macros assume two’s-complement arithmetic.
#define _MY_SSIZE_MAX ((ssize_t) (((size_t) -1) >> 1))
#define _MY_SSIZE_MIN ((-_MY_SSIZE_MAX) - 1)

//-------------------------------------------------------------------------

static void
bad_attr_key (SCM key)
{
  const char *s = "expected pyobject or string";
  SCM message =
    scm_call_2 (scm_c_public_ref ("ice-9 format", "format"), SCM_BOOL_F,
                scm_gettext (scm_from_utf8_string (s),
                             scm_from_utf8_string (SMCOREGUILE_PACKAGE),
                             SCM_UNDEFINED));
  rnrs_raise_condition (scm_list_3
                        (rnrs_make_error (),
                         rnrs_make_message_condition (message),
                         rnrs_make_irritants_condition (scm_list_1 (key))));
}

static inline bool
attr_key_good (PyObject *key)
{
  bool good;
#if PY_MAJOR_VERSION <= 2
  good = (PyBytes_Check (key) || PyUnicode_Check (key));
#else
  good = (PyUnicode_Check (key));
#endif
  return good;
}

static SCM
scm_prepare_python_attr_key (SCM key)
{
  SCM result;
  if (scm_is_pyobject (key))
    {
      PyObject *_key = scm_to_PyObject_ptr (key);
      if (attr_key_good (_key))
        result = key;
      else
        {
          result = SCM_UNDEFINED;
          bad_attr_key (key);
        }
    }
  else if (scm_is_string (key))
    result = scm_string_to_pyunicode (key, SCM_UNDEFINED);
  else
    {
      result = SCM_UNDEFINED;
      bad_attr_key (key);
    }
  return result;
}

//-------------------------------------------------------------------------

static void
bad_item_key (SCM key)
{
  const char *s = "expected pyobject, exact integer in [~a,~a], or string";
  SCM message =
    scm_call_4 (scm_c_public_ref ("ice-9 format", "format"), SCM_BOOL_F,
                scm_gettext (scm_from_utf8_string (s),
                             scm_from_utf8_string (SMCOREGUILE_PACKAGE),
                             SCM_UNDEFINED),
                scm_from_ssize_t (_MY_SSIZE_MIN),
                scm_from_ssize_t (_MY_SSIZE_MAX));
  rnrs_raise_condition (scm_list_3
                        (rnrs_make_error (),
                         rnrs_make_message_condition (message),
                         rnrs_make_irritants_condition (scm_list_1 (key))));
}

static SCM
scm_prepare_python_item_key (SCM key)
{
  SCM result;
  if (scm_is_pyobject (key))
    result = key;
  else if (scm_is_signed_integer (key, _MY_SSIZE_MIN, _MY_SSIZE_MAX))
    {
      const ssize_t i = scm_to_ssize_t (key);
      PyObject *pyobj = PyIntOrLong_FromSsize_t (i);
      py_exc_check ((pyobj != NULL),
                    "static SCM _scm_prepare_key (SCM key)", scm_list_1 (key));
      result = scm_c_make_pyobject (pyobj);
    }
  else if (scm_is_string (key))
    result = scm_string_to_pyunicode (key, SCM_UNDEFINED);
  else
    {
      result = SCM_UNDEFINED;
      bad_item_key (key);
    }
  return result;
}

static inline SCM
scm_prepare_python_item_string_key (SCM key)
{
  return scm_prepare_python_attr_key (key);
}

//-------------------------------------------------------------------------

VISIBLE SCM
scm_py_item_ref (SCM obj, SCM key)
{
  SCM k = scm_prepare_python_item_key (key);
  return scm_pyobject_getitem (obj, k);
}

VISIBLE SCM
scm_py_item_set_x (SCM obj, SCM key, SCM value)
{
  SCM k = scm_prepare_python_item_key (key);
  return scm_pyobject_setitem_x (obj, k, value);
}

VISIBLE SCM
scm_py_item_del_x (SCM obj, SCM key)
{
  SCM k = scm_prepare_python_item_key (key);
  return scm_pyobject_delitem_x (obj, k);
}

//-------------------------------------------------------------------------

static SCM
scm_c_py_lookup_ref (const char *who, PyObject *mapping, SCM key)
{
  assert (mapping != NULL);

  SCM k = scm_prepare_python_attr_key (key);
  PyObject *_k = scm_to_PyObject_ptr (k);
  PyObject *_result = PyObject_GetItem (mapping, _k);
  py_exc_check ((_result != NULL), who, scm_list_1 (key));
  return scm_c_make_pyobject (_result);
}

static SCM
scm_c_py_lookup_ref_p (const char *who, PyObject *mapping, SCM key)
{
  SCM result;
  if (mapping != NULL)
    {
      SCM k = scm_prepare_python_attr_key (key);
      PyObject *_k = scm_to_PyObject_ptr (k);
      if (PyMapping_HasKey (mapping, _k))
        {
          PyObject *_result = PyObject_GetItem (mapping, _k);
          py_exc_check ((_result != NULL), who, scm_list_1 (key));
          result = scm_c_make_pyobject (_result);
        }
      else
        result = SCM_BOOL_F;
    }
  else
    result = SCM_BOOL_F;
  return result;
}

static SCM
scm_c_c_py_lookup_ref_p (const char *who, PyObject *mapping, PyObject *key)
{
  assert (key != NULL);

  SCM result;
  if (mapping != NULL)
    {
      SCM_ASSERT_TYPE (PyUnicode_Check (key), scm_c_incref_make_pyobject (key),
                       SCM_ARG1, who, "string or pyunicode");
      if (PyMapping_HasKey (mapping, key))
        {
          PyObject *_result = PyObject_GetItem (mapping, key);
          py_exc_check ((_result != NULL), who,
                        scm_list_1 (scm_c_incref_make_pyobject (key)));
          result = scm_c_make_pyobject (_result);
        }
      else
        result = SCM_BOOL_F;
    }
  else
    result = SCM_BOOL_F;
  return result;
}

VISIBLE SCM
scm_py_builtin_ref (SCM key)
{
  return scm_c_py_lookup_ref ("py-builtin-ref", PyEval_GetBuiltins (), key);
}

VISIBLE SCM
scm_py_builtin_ref_p (SCM key)
{
  return scm_c_py_lookup_ref_p ("py-builtin-ref?", PyEval_GetBuiltins (), key);
}

static SCM
no_execution_frame_message (void)
{
  return rnrs_c_make_message_condition (_("no execution frame is active"));
}

static SCM
scm_raise_no_execution_frame_error (SCM who)
{
  rnrs_raise_condition
    (scm_list_4
     (rnrs_make_error (),
      rnrs_make_who_condition (who),
      no_execution_frame_message (), rnrs_make_irritants_condition (SCM_EOL)));
  return SCM_UNSPECIFIED;
}

static SCM
key_not_found_message (void)
{
  return rnrs_c_make_message_condition
    (_("key not found in locals, globals, or builtins"));
}

VISIBLE SCM
scm_py_global_ref (SCM key)
{
  PyObject *globals = PyEval_GetGlobals ();
  if (globals == NULL)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition ("py-global-ref"),
        no_execution_frame_message (),
        rnrs_make_irritants_condition (SCM_EOL)));
  return scm_c_py_lookup_ref ("py-global-ref", globals, key);
}

VISIBLE SCM
scm_py_global_ref_p (SCM key)
{
  return scm_c_py_lookup_ref_p ("py-global-ref?", PyEval_GetGlobals (), key);
}

VISIBLE SCM
scm_py_local_ref (SCM key)
{
  PyObject *locals = PyEval_GetLocals ();
  if (locals == NULL)
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition ("py-local-ref"),
        no_execution_frame_message (),
        rnrs_make_irritants_condition (SCM_EOL)));
  return scm_c_py_lookup_ref ("py-local-ref", locals, key);
}

VISIBLE SCM
scm_py_local_ref_p (SCM key)
{
  return scm_c_py_lookup_ref_p ("py-local-ref?", PyEval_GetLocals (), key);
}

static SCM
_scm_py_variable_ref_p_internal (const char *who, SCM key)
{
  SCM k = scm_prepare_python_attr_key (key);
  PyObject *_k = scm_to_PyObject_ptr (k);
  SCM result = scm_c_c_py_lookup_ref_p (who, PyEval_GetLocals (), _k);
  if (scm_is_false (result))
    {
      result = scm_c_c_py_lookup_ref_p (who, PyEval_GetGlobals (), _k);
      if (scm_is_false (result))
        result = scm_c_c_py_lookup_ref_p (who, PyEval_GetBuiltins (), _k);
    }
  return result;
}

VISIBLE SCM
scm_py_var_ref (SCM key)
{
  SCM result = _scm_py_variable_ref_p_internal ("py-var-ref", key);
  if (scm_is_false (result))
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition ("py-var-ref"),
        key_not_found_message (),
        rnrs_make_irritants_condition (scm_list_1 (key))));
  return result;
}

VISIBLE SCM
scm_py_var_ref_p (SCM key)
{
  return _scm_py_variable_ref_p_internal ("py-var-ref?", key);
}

VISIBLE SCM
scm_py_module_var_ref (SCM module_key, SCM variable_key)
{
  SCM value;
  const char *who = "py-module-var-ref";

  scm_dynwind_begin (0);

  PyObject *mkey =
    scm_to_PyObject_ptr (scm_prepare_python_item_string_key (module_key));
  PyObject *sys_modules = PyImport_GetModuleDict ();
  if (sys_modules == NULL)
    // FIXME: Is this error possible?
    rnrs_raise_condition
      (scm_list_4
       (rnrs_make_error (),
        rnrs_c_make_who_condition (who),
        rnrs_c_make_message_condition (_("sys.modules is unavailable")),
        rnrs_make_irritants_condition (SCM_EOL)));
  if (PyMapping_HasKey (sys_modules, mkey))
    {
      PyObject *module = PyObject_GetItem (sys_modules, mkey);
      py_exc_check ((module != NULL), who, scm_list_1 (module_key));
      scm_dynwind_decref (module);
      PyObject *vkey =
        scm_to_PyObject_ptr (scm_prepare_python_attr_key (variable_key));
      PyObject *_value = PyObject_GetAttr (module, vkey);
      py_exc_check ((_value != NULL), who,
                    scm_list_2 (module_key, variable_key));
      value = scm_c_make_pyobject (_value);
    }
  else
    {
      PyObject *m = PyImport_Import (mkey);
      scm_dynwind_decref (m);
      py_exc_check ((m != NULL), who, scm_list_1 (module_key));
      if (PyMapping_HasKey (sys_modules, mkey))
        // Now that the module has been ‘autoloaded’, try again to
        // dereference the variable.
        value = scm_py_module_var_ref (module_key, variable_key);
      else
        // The module now should be in sys.modules, but it is possible
        // the module was not imported correctly. If it is not found
        // in sys.modules, raise the following exception.
        rnrs_raise_condition
          (scm_list_4
           (rnrs_make_error (),
            rnrs_c_make_who_condition (who),
            rnrs_c_make_message_condition
            (_("Python module was not imported correctly")),
            rnrs_make_irritants_condition (scm_list_1 (module_key))));
    }

  scm_dynwind_end ();

  return value;
}

//-------------------------------------------------------------------------

void init_guile_pyfriendlier (void);

VISIBLE void
init_guile_pyfriendlier (void)
{
  scm_c_define_gsubr ("prepare-item-key", 1, 0, 0, scm_prepare_python_item_key);
  scm_c_define_gsubr ("prepare-attr-key", 1, 0, 0, scm_prepare_python_attr_key);
  scm_c_define_gsubr ("raise-no-execution-frame-error", 1, 0, 0,
                      scm_raise_no_execution_frame_error);
}

//-------------------------------------------------------------------------
