#include <config.h>             // -*- coding: utf-8 -*-

// Copyright (C) 2013, 2014 Khaled Hosny and Barry Schwartz
//
// This file is part of Sorts Mill Core Guile
// 
// Sorts Mill Core Guile is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
// 
// Sorts Mill Core Guile is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with Sorts Mill Core Guile.  If not, see
// <http://www.gnu.org/licenses/>.

#include <Python.h>

#include <assert.h>
#include <stddef.h>
#include <stdbool.h>
#include <libintl.h>
#include <libguile.h>

#include <sortsmill/guile/core.h>
#include <sortsmill/guile/core/python.h>
#include "python_helpers.h"

#undef _
#define _(string) dgettext (SMCOREGUILE_PACKAGE, string)

#define _PYTYPES_MODULE "sortsmill core python pytypes"

#if PY_VERSION_HEX < 0x03020000 || !defined Py_LIMITED_API
#define _PYTYPE_GETFLAGS(t) ((t)->tp_flags)
#else
#define _PYTYPE_GETFLAGS(t) (PyType_GetFlags (t))
#endif

//-------------------------------------------------------------------------

SCM_PYOBJECT_UNARY_WRAPCHECKS (pytype, PyType);

VISIBLE SCM
scm_pytype_clearcache_x (void)
{
  return scm_from_uint (PyType_ClearCache ());
}

VISIBLE SCM
scm_pytype_modified_x (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyType_Check (_obj), obj, SCM_ARG1, "pytype-modified!",
                   "pytype");
  PyType_Modified ((PyTypeObject *) _obj);
  return SCM_UNSPECIFIED;
}

VISIBLE SCM
scm_pytype_getflags (SCM obj)
{
  scm_assert_pyobject (obj);
  PyObject *_obj = scm_to_PyObject_ptr (obj);
  SCM_ASSERT_TYPE (PyType_Check (_obj), obj, SCM_ARG1, "pytype-modified!",
                   "pytype");
  const long int flags = _PYTYPE_GETFLAGS ((PyTypeObject *) _obj);
  return scm_integer_to_tpflags_symbols (scm_from_long (flags));
}

VISIBLE SCM
scm_tpflags_symbols_to_integer (SCM symbol_lst)
{
  return
    scm_call_1 (scm_c_private_ref (_PYTYPES_MODULE, "tpflags-symbols->integer"),
                symbol_lst);
}

VISIBLE SCM
scm_integer_to_tpflags_symbols (SCM flag_bits)
{
  return
    scm_call_1 (scm_c_private_ref (_PYTYPES_MODULE, "integer->tpflags-symbols"),
                flag_bits);
}

VISIBLE SCM
scm_pytype_hasfeature_p (SCM pytype, SCM features)
{
  scm_assert_pyobject (pytype);
  PyObject *_pytype = scm_to_PyObject_ptr (pytype);
  SCM_ASSERT_TYPE (PyType_Check (_pytype), pytype, SCM_ARG1, "pytype-modified!",
                   "pytype");
  const long int flags =
    scm_to_long ((scm_is_integer (features)) ? features :
                 scm_tpflags_symbols_to_integer (features));
  const bool truth = PyType_HasFeature ((PyTypeObject *) _pytype, flags);
  return scm_from_bool (truth);
}

VISIBLE bool
scm_pytype_is_issubtype (SCM a, SCM b)
{
  scm_assert_pyobject (a);
  PyObject *_a = scm_to_PyObject_ptr (a);
  scm_assert_pyobject (b);
  PyObject *_b = scm_to_PyObject_ptr (b);
  return PyType_IsSubtype ((PyTypeObject *) _a, (PyTypeObject *) _b);
}

VISIBLE SCM
scm_pytype_issubtype_p (SCM a, SCM b)
{
  return scm_from_bool (scm_pytype_is_issubtype (a, b));
}

//-------------------------------------------------------------------------

void init_guile_pytype (void);

#if PY_MAJOR_VERSION < 3
#define _SCM_FROM_LONG_IF_PY2(n) scm_from_long (n)
#else
#define _SCM_FROM_LONG_IF_PY2(n) scm_from_int (0)
#endif

VISIBLE void
init_guile_pytype (void)
{
  scm_c_define ("Py_TPFLAGS_HAVE_GETCHARBUFFER",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_GETCHARBUFFER));
  scm_c_define ("Py_TPFLAGS_HAVE_SEQUENCE_IN",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_SEQUENCE_IN));
  scm_c_define ("Py_TPFLAGS_HAVE_INPLACEOPS",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_INPLACEOPS));
  scm_c_define ("Py_TPFLAGS_CHECKTYPES",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_CHECKTYPES));
  scm_c_define ("Py_TPFLAGS_HAVE_RICHCOMPARE",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_RICHCOMPARE));
  scm_c_define ("Py_TPFLAGS_HAVE_WEAKREFS",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_WEAKREFS));
  scm_c_define ("Py_TPFLAGS_HAVE_ITER",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_ITER));
  scm_c_define ("Py_TPFLAGS_HAVE_CLASS",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_CLASS));
  scm_c_define ("Py_TPFLAGS_HEAPTYPE", scm_from_long (Py_TPFLAGS_HEAPTYPE));
  scm_c_define ("Py_TPFLAGS_BASETYPE", scm_from_long (Py_TPFLAGS_BASETYPE));
  scm_c_define ("Py_TPFLAGS_READY", scm_from_long (Py_TPFLAGS_READY));
  scm_c_define ("Py_TPFLAGS_READYING", scm_from_long (Py_TPFLAGS_READYING));
  scm_c_define ("Py_TPFLAGS_HAVE_GC", scm_from_long (Py_TPFLAGS_HAVE_GC));
  scm_c_define ("Py_TPFLAGS_HAVE_STACKLESS_EXTENSION",
                scm_from_long (Py_TPFLAGS_HAVE_STACKLESS_EXTENSION));
  scm_c_define ("Py_TPFLAGS_HAVE_INDEX",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_INDEX));
  scm_c_define ("Py_TPFLAGS_HAVE_VERSION_TAG",
                scm_from_long (Py_TPFLAGS_HAVE_VERSION_TAG));
  scm_c_define ("Py_TPFLAGS_VALID_VERSION_TAG",
                scm_from_long (Py_TPFLAGS_VALID_VERSION_TAG));
  scm_c_define ("Py_TPFLAGS_IS_ABSTRACT",
                scm_from_long (Py_TPFLAGS_IS_ABSTRACT));
  scm_c_define ("Py_TPFLAGS_HAVE_NEWBUFFER",
                _SCM_FROM_LONG_IF_PY2 (Py_TPFLAGS_HAVE_NEWBUFFER));
#if PY_VERSION_HEX < 0x03040000
  // FIXME: Should we use _SCM_FROM_LONG_IF_PY2 here instead of the
  // special handling for Python 3.4+? Was this flag left in earlier
  // Python 3.x by mistake?
  scm_c_define ("Py_TPFLAGS_INT_SUBCLASS",
                scm_from_long (Py_TPFLAGS_INT_SUBCLASS));
#else
  scm_c_define ("Py_TPFLAGS_INT_SUBCLASS", scm_from_int (0));
#endif
  scm_c_define ("Py_TPFLAGS_LONG_SUBCLASS",
                scm_from_long (Py_TPFLAGS_LONG_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_LIST_SUBCLASS",
                scm_from_long (Py_TPFLAGS_LIST_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_TUPLE_SUBCLASS",
                scm_from_long (Py_TPFLAGS_TUPLE_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_BYTES_SUBCLASS",
                scm_from_long (Py_TPFLAGS_BYTES_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_UNICODE_SUBCLASS",
                scm_from_long (Py_TPFLAGS_UNICODE_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_DICT_SUBCLASS",
                scm_from_long (Py_TPFLAGS_DICT_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_BASE_EXC_SUBCLASS",
                scm_from_long (Py_TPFLAGS_BASE_EXC_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_TYPE_SUBCLASS",
                scm_from_long (Py_TPFLAGS_TYPE_SUBCLASS));
  scm_c_define ("Py_TPFLAGS_DEFAULT", scm_from_long (Py_TPFLAGS_DEFAULT));
}

//-------------------------------------------------------------------------
