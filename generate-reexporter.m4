#!/bin/sh
# -*- mode: scheme; coding: utf-8 -*-
include(`m4citrus.m4')dnl
m4_include([vars.m4])dnl
GUILE_AUTO_COMPILE=0 exec GUILE_COMMAND -s "$0" ${1+"$@"}
!#

;; Copyright (C) 2013 Khaled Hosny and Barry Schwartz
;;
;; This file is part of Sorts Mill Core Guile.
;; 
;; Sorts Mill Core Guile is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;; 
;; Sorts Mill Core Guile is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, see <http://www.gnu.org/licenses/>.

(import (rnrs)
        (only (srfi :1) delete-duplicates)
        (ice-9 match)
        (ice-9 pretty-print))

(define (find-exports library-name expression)
  "Find exports in the R⁶RS-style library @var{expression}, or in a
@code{define-module} expression, if @var{library-name} is @code{#t} or
matches the library’s name. Otherwise return @code{#f}."
  (define (find-exports-in-define-module options)
    (match options
      (() '())
      (((or #:export #:re-export #:replace) lst . more-options)
       (append (map (lambda (e) (if (pair? e) (cdr e) e)) lst)
               (find-exports-in-define-module more-options)))
      (((or #:use-module #:version #:duplicates) _ . more-options)
       (find-exports-in-define-module more-options))
      ((#:autoload _ _ . more-options)
       (find-exports-in-define-module more-options))
      ((#:pure . more-options)
       (find-exports-in-define-module more-options))
      ((_ . more-options)
       (find-exports-in-define-module more-options))))
  (match expression
    (('library lib-name ('export . exports-list) . imports-and-body)
     (if (or (eq? library-name #t) (equal? library-name lib-name))
         (cons lib-name exports-list)
         #f))
    (('define-module lib-name . options)
     (if (or (eq? library-name #t) (equal? library-name lib-name))
         (cons lib-name (find-exports-in-define-module options))
         #f))
    (_ #f)))

(define* (find-exports-in-input library-name #:optional
                                (port (current-input-port)))
  "Read expressions from @var{port}, looking for R⁶RS-style libraries
matching @var{library-name} according to the rules in
@code{find-exports}, and collecting their exports."
  (let find-them ((prior '()))
    (let ((expression (read port)))
      (if (eof-object? expression)
          prior
          (let ((exports (find-exports library-name expression)))
            (if exports
                (find-them (append prior (list exports)))
                (find-them prior)))))))

(define (find-exports-in-files library-name encoding . file-names)
  "Read expressions from files, collecting exports by calling
@code{find-exports-in-input} on the opened files. The open ports are
set to the given encoding if @var{encoding} is not @code{#f}."
  (let ((find-in-one-file
         (lambda (f)
           (with-input-from-file f
             (lambda ()
               (when encoding
                 (set-port-encoding! (current-input-port) encoding))
               (find-exports-in-input library-name))))))
    (fold-left (lambda (prior file-name)
                 (let ((new-exports (find-in-one-file file-name)))
                   (if new-exports
                       (append prior new-exports)
                       prior)))
               '() file-names)))

(define (generate-reexporter template reexporter-name library-names
                             encoding file-names)
  (let ((reexports
         (fold-left
          (lambda (prior lib-name)
            (append
             prior (apply find-exports-in-files lib-name encoding
                          file-names)))
          '() library-names)))
    (template reexporter-name
              (delete-duplicates (fold-left append '()
                                            (map cdr reexports)))
              (delete-duplicates (map car reexports)))))

(define (pretty-print-reexporter template reexporter-name library-names
                                 encoding file-names)
  (pretty-print (generate-reexporter template reexporter-name
                                     library-names encoding file-names)))

(define (main-program commands)
  ;;(enable-hash-guillemet-strings) <-- FIXME: Make this optional.
  (set-port-encoding! (current-output-port) "UTF-8") ;;; FIXME: Make
                                                     ;;; this
                                                     ;;; optional.
  (match commands
    ((_ template reexporter-name library-names file-names)
     (pretty-print-reexporter
      (primitive-eval (call-with-input-string template read))
      (call-with-input-string reexporter-name read)
      (call-with-input-string (string-append "(" library-names ")") read)
      "UTF-8" ;; FIXME: Make this encoding optional.
      (call-with-input-string (string-append "(" file-names ")")  read)))))

(main-program (command-line))

